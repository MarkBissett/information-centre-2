namespace WoodPlan5
{
    partial class frm_Core_Contractor_Team_Member_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Contractor_Team_Member_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.GritJobAllocateByDefaultCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.sp04024GCSubContractorTeamMemberEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_DataEntry = new WoodPlan5.DataSet_GC_DataEntry();
            this.btnCreateGUID = new DevExpress.XtraEditors.SimpleButton();
            this.PDALoginTokenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PDAPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PDAUserNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RoleIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04330CoreTeamMemberRolesListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GenderIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04331CoreTeamMemberGendersListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colItemOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PostcodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine5TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine4TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine3TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AddressLine1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.TeamMemberIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TelephoneTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MobileTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FaxTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.strRemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SubContractorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp04026ContractorListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalContractor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInternalCOntractorDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colTelephone1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserDefined3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVatReg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebsite = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EmailMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.TextMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dateAddedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.NatInsuranceNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForTeamMemberID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTelephone = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobile = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFax = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForText = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAddressLine1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAddressLine5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPostcode = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPDAUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDAPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDALoginToken = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGritJobAllocateByDefault = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForstrRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForGenderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRoleID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDateAdded = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNatInsuranceNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp04024_GC_SubContractor_Team_Member_EditTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04024_GC_SubContractor_Team_Member_EditTableAdapter();
            this.sp04026_Contractor_List_With_BlankTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04026_Contractor_List_With_BlankTableAdapter();
            this.sp04330_Core_Team_Member_Roles_ListTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04330_Core_Team_Member_Roles_ListTableAdapter();
            this.sp04331_Core_Team_Member_Genders_ListTableAdapter = new WoodPlan5.DataSet_GC_DataEntryTableAdapters.sp04331_Core_Team_Member_Genders_ListTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GritJobAllocateByDefaultCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04024GCSubContractorTeamMemberEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDALoginTokenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDAPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDAUserNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoleIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04330CoreTeamMemberRolesListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GenderIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04331CoreTeamMemberGendersListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine5TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine4TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine3TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamMemberIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaxTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04026ContractorListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateAddedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateAddedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NatInsuranceNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMemberID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDAUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDAPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDALoginToken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritJobAllocateByDefault)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGenderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoleID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNatInsuranceNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 509);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 483);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 483);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // colID
            // 
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colContractorID
            // 
            this.colContractorID.Caption = "Contractor ID";
            this.colContractorID.FieldName = "ContractorID";
            this.colContractorID.Name = "colContractorID";
            this.colContractorID.OptionsColumn.AllowEdit = false;
            this.colContractorID.OptionsColumn.AllowFocus = false;
            this.colContractorID.OptionsColumn.ReadOnly = true;
            this.colContractorID.Width = 87;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 509);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(628, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 483);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 483);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.GritJobAllocateByDefaultCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.btnCreateGUID);
            this.dataLayoutControl1.Controls.Add(this.PDALoginTokenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PDAPasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PDAUserNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RoleIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.GenderIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.PostcodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine5TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine4TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine3TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AddressLine1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.TeamMemberIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TelephoneTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MobileTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FaxTextEdit);
            this.dataLayoutControl1.Controls.Add(this.strRemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.EmailMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.TextMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dateAddedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.NatInsuranceNumberTextEdit);
            this.dataLayoutControl1.DataSource = this.sp04024GCSubContractorTeamMemberEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTeamMemberID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(821, 348, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 483);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // GritJobAllocateByDefaultCheckEdit
            // 
            this.GritJobAllocateByDefaultCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "GritJobAllocateByDefault", true));
            this.GritJobAllocateByDefaultCheckEdit.EditValue = 0;
            this.GritJobAllocateByDefaultCheckEdit.Location = new System.Drawing.Point(150, 296);
            this.GritJobAllocateByDefaultCheckEdit.MenuManager = this.barManager1;
            this.GritJobAllocateByDefaultCheckEdit.Name = "GritJobAllocateByDefaultCheckEdit";
            this.GritJobAllocateByDefaultCheckEdit.Properties.Caption = "(Tick if Yes  -  Only 1 Per Team. Flag must also be set on Team Gritting Info scr" +
    "een)";
            this.GritJobAllocateByDefaultCheckEdit.Properties.ValueChecked = 1;
            this.GritJobAllocateByDefaultCheckEdit.Properties.ValueUnchecked = 0;
            this.GritJobAllocateByDefaultCheckEdit.Size = new System.Drawing.Size(442, 19);
            this.GritJobAllocateByDefaultCheckEdit.StyleController = this.dataLayoutControl1;
            this.GritJobAllocateByDefaultCheckEdit.TabIndex = 28;
            // 
            // sp04024GCSubContractorTeamMemberEditBindingSource
            // 
            this.sp04024GCSubContractorTeamMemberEditBindingSource.DataMember = "sp04024_GC_SubContractor_Team_Member_Edit";
            this.sp04024GCSubContractorTeamMemberEditBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // dataSet_GC_DataEntry
            // 
            this.dataSet_GC_DataEntry.DataSetName = "DataSet_GC_DataEntry";
            this.dataSet_GC_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnCreateGUID
            // 
            this.btnCreateGUID.Location = new System.Drawing.Point(513, 367);
            this.btnCreateGUID.Name = "btnCreateGUID";
            this.btnCreateGUID.Size = new System.Drawing.Size(79, 22);
            this.btnCreateGUID.StyleController = this.dataLayoutControl1;
            this.btnCreateGUID.TabIndex = 27;
            this.btnCreateGUID.Text = "Create Token";
            this.btnCreateGUID.Click += new System.EventHandler(this.btnCreateGUID_Click);
            // 
            // PDALoginTokenTextEdit
            // 
            this.PDALoginTokenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "PDALoginToken", true));
            this.PDALoginTokenTextEdit.Location = new System.Drawing.Point(150, 367);
            this.PDALoginTokenTextEdit.MenuManager = this.barManager1;
            this.PDALoginTokenTextEdit.Name = "PDALoginTokenTextEdit";
            this.PDALoginTokenTextEdit.Properties.MaxLength = 50;
            this.PDALoginTokenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PDALoginTokenTextEdit, true);
            this.PDALoginTokenTextEdit.Size = new System.Drawing.Size(359, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PDALoginTokenTextEdit, optionsSpelling1);
            this.PDALoginTokenTextEdit.StyleController = this.dataLayoutControl1;
            this.PDALoginTokenTextEdit.TabIndex = 26;
            // 
            // PDAPasswordTextEdit
            // 
            this.PDAPasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "PDAPassword", true));
            this.PDAPasswordTextEdit.Location = new System.Drawing.Point(150, 343);
            this.PDAPasswordTextEdit.MenuManager = this.barManager1;
            this.PDAPasswordTextEdit.Name = "PDAPasswordTextEdit";
            this.PDAPasswordTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PDAPasswordTextEdit, true);
            this.PDAPasswordTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PDAPasswordTextEdit, optionsSpelling2);
            this.PDAPasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.PDAPasswordTextEdit.TabIndex = 25;
            // 
            // PDAUserNameTextEdit
            // 
            this.PDAUserNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "PDAUserName", true));
            this.PDAUserNameTextEdit.Location = new System.Drawing.Point(150, 319);
            this.PDAUserNameTextEdit.MenuManager = this.barManager1;
            this.PDAUserNameTextEdit.Name = "PDAUserNameTextEdit";
            this.PDAUserNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PDAUserNameTextEdit, true);
            this.PDAUserNameTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PDAUserNameTextEdit, optionsSpelling3);
            this.PDAUserNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PDAUserNameTextEdit.TabIndex = 24;
            // 
            // RoleIDGridLookUpEdit
            // 
            this.RoleIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "RoleID", true));
            this.RoleIDGridLookUpEdit.Location = new System.Drawing.Point(126, 143);
            this.RoleIDGridLookUpEdit.MenuManager = this.barManager1;
            this.RoleIDGridLookUpEdit.Name = "RoleIDGridLookUpEdit";
            this.RoleIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RoleIDGridLookUpEdit.Properties.DataSource = this.sp04330CoreTeamMemberRolesListBindingSource;
            this.RoleIDGridLookUpEdit.Properties.DisplayMember = "Value";
            this.RoleIDGridLookUpEdit.Properties.NullText = "";
            this.RoleIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit2View;
            this.RoleIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.RoleIDGridLookUpEdit.Size = new System.Drawing.Size(490, 20);
            this.RoleIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RoleIDGridLookUpEdit.TabIndex = 23;
            // 
            // sp04330CoreTeamMemberRolesListBindingSource
            // 
            this.sp04330CoreTeamMemberRolesListBindingSource.DataMember = "sp04330_Core_Team_Member_Roles_List";
            this.sp04330CoreTeamMemberRolesListBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridLookUpEdit2View
            // 
            this.gridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit2View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit2View.Name = "gridLookUpEdit2View";
            this.gridLookUpEdit2View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Order";
            this.gridColumn2.FieldName = "ItemOrder";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Description";
            this.gridColumn3.FieldName = "Value";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 220;
            // 
            // GenderIDGridLookUpEdit
            // 
            this.GenderIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "GenderID", true));
            this.GenderIDGridLookUpEdit.Location = new System.Drawing.Point(126, 119);
            this.GenderIDGridLookUpEdit.MenuManager = this.barManager1;
            this.GenderIDGridLookUpEdit.Name = "GenderIDGridLookUpEdit";
            this.GenderIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GenderIDGridLookUpEdit.Properties.DataSource = this.sp04331CoreTeamMemberGendersListBindingSource;
            this.GenderIDGridLookUpEdit.Properties.DisplayMember = "Value";
            this.GenderIDGridLookUpEdit.Properties.NullText = "";
            this.GenderIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.GenderIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.GenderIDGridLookUpEdit.Size = new System.Drawing.Size(490, 20);
            this.GenderIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.GenderIDGridLookUpEdit.TabIndex = 22;
            // 
            // sp04331CoreTeamMemberGendersListBindingSource
            // 
            this.sp04331CoreTeamMemberGendersListBindingSource.DataMember = "sp04331_Core_Team_Member_Genders_List";
            this.sp04331CoreTeamMemberGendersListBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colItemOrder,
            this.colValue});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colID;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colItemOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colItemOrder
            // 
            this.colItemOrder.Caption = "Order";
            this.colItemOrder.FieldName = "ItemOrder";
            this.colItemOrder.Name = "colItemOrder";
            this.colItemOrder.OptionsColumn.AllowEdit = false;
            this.colItemOrder.OptionsColumn.AllowFocus = false;
            this.colItemOrder.OptionsColumn.ReadOnly = true;
            // 
            // colValue
            // 
            this.colValue.Caption = "Description";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.OptionsColumn.AllowEdit = false;
            this.colValue.OptionsColumn.AllowFocus = false;
            this.colValue.OptionsColumn.ReadOnly = true;
            this.colValue.Visible = true;
            this.colValue.VisibleIndex = 0;
            this.colValue.Width = 220;
            // 
            // PostcodeTextEdit
            // 
            this.PostcodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Postcode", true));
            this.PostcodeTextEdit.Location = new System.Drawing.Point(150, 416);
            this.PostcodeTextEdit.MenuManager = this.barManager1;
            this.PostcodeTextEdit.Name = "PostcodeTextEdit";
            this.PostcodeTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PostcodeTextEdit, true);
            this.PostcodeTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PostcodeTextEdit, optionsSpelling4);
            this.PostcodeTextEdit.StyleController = this.dataLayoutControl1;
            this.PostcodeTextEdit.TabIndex = 20;
            // 
            // AddressLine5TextEdit
            // 
            this.AddressLine5TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "AddressLine5", true));
            this.AddressLine5TextEdit.Location = new System.Drawing.Point(150, 392);
            this.AddressLine5TextEdit.MenuManager = this.barManager1;
            this.AddressLine5TextEdit.Name = "AddressLine5TextEdit";
            this.AddressLine5TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine5TextEdit, true);
            this.AddressLine5TextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine5TextEdit, optionsSpelling5);
            this.AddressLine5TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine5TextEdit.TabIndex = 19;
            // 
            // AddressLine4TextEdit
            // 
            this.AddressLine4TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "AddressLine4", true));
            this.AddressLine4TextEdit.Location = new System.Drawing.Point(150, 368);
            this.AddressLine4TextEdit.MenuManager = this.barManager1;
            this.AddressLine4TextEdit.Name = "AddressLine4TextEdit";
            this.AddressLine4TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine4TextEdit, true);
            this.AddressLine4TextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine4TextEdit, optionsSpelling6);
            this.AddressLine4TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine4TextEdit.TabIndex = 18;
            // 
            // AddressLine3TextEdit
            // 
            this.AddressLine3TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "AddressLine3", true));
            this.AddressLine3TextEdit.Location = new System.Drawing.Point(150, 344);
            this.AddressLine3TextEdit.MenuManager = this.barManager1;
            this.AddressLine3TextEdit.Name = "AddressLine3TextEdit";
            this.AddressLine3TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine3TextEdit, true);
            this.AddressLine3TextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine3TextEdit, optionsSpelling7);
            this.AddressLine3TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine3TextEdit.TabIndex = 17;
            // 
            // AddressLine2TextEdit
            // 
            this.AddressLine2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "AddressLine2", true));
            this.AddressLine2TextEdit.Location = new System.Drawing.Point(150, 320);
            this.AddressLine2TextEdit.MenuManager = this.barManager1;
            this.AddressLine2TextEdit.Name = "AddressLine2TextEdit";
            this.AddressLine2TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine2TextEdit, true);
            this.AddressLine2TextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine2TextEdit, optionsSpelling8);
            this.AddressLine2TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine2TextEdit.TabIndex = 16;
            // 
            // AddressLine1TextEdit
            // 
            this.AddressLine1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "AddressLine1", true));
            this.AddressLine1TextEdit.Location = new System.Drawing.Point(150, 296);
            this.AddressLine1TextEdit.MenuManager = this.barManager1;
            this.AddressLine1TextEdit.Name = "AddressLine1TextEdit";
            this.AddressLine1TextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AddressLine1TextEdit, true);
            this.AddressLine1TextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AddressLine1TextEdit, optionsSpelling9);
            this.AddressLine1TextEdit.StyleController = this.dataLayoutControl1;
            this.AddressLine1TextEdit.TabIndex = 15;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp04024GCSubContractorTeamMemberEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(127, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 14;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // TeamMemberIDTextEdit
            // 
            this.TeamMemberIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "TeamMemberID", true));
            this.TeamMemberIDTextEdit.Location = new System.Drawing.Point(96, 85);
            this.TeamMemberIDTextEdit.MenuManager = this.barManager1;
            this.TeamMemberIDTextEdit.Name = "TeamMemberIDTextEdit";
            this.TeamMemberIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TeamMemberIDTextEdit, true);
            this.TeamMemberIDTextEdit.Size = new System.Drawing.Size(520, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TeamMemberIDTextEdit, optionsSpelling10);
            this.TeamMemberIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TeamMemberIDTextEdit.TabIndex = 4;
            // 
            // NameTextEdit
            // 
            this.NameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Name", true));
            this.NameTextEdit.Location = new System.Drawing.Point(126, 62);
            this.NameTextEdit.MenuManager = this.barManager1;
            this.NameTextEdit.Name = "NameTextEdit";
            this.NameTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NameTextEdit, true);
            this.NameTextEdit.Size = new System.Drawing.Size(490, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NameTextEdit, optionsSpelling11);
            this.NameTextEdit.StyleController = this.dataLayoutControl1;
            this.NameTextEdit.TabIndex = 6;
            this.NameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.strContactNameTextEdit_Validating);
            // 
            // TelephoneTextEdit
            // 
            this.TelephoneTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Telephone", true));
            this.TelephoneTextEdit.Location = new System.Drawing.Point(150, 296);
            this.TelephoneTextEdit.MenuManager = this.barManager1;
            this.TelephoneTextEdit.Name = "TelephoneTextEdit";
            this.TelephoneTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TelephoneTextEdit, true);
            this.TelephoneTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TelephoneTextEdit, optionsSpelling12);
            this.TelephoneTextEdit.StyleController = this.dataLayoutControl1;
            this.TelephoneTextEdit.TabIndex = 7;
            // 
            // MobileTextEdit
            // 
            this.MobileTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Mobile", true));
            this.MobileTextEdit.Location = new System.Drawing.Point(150, 320);
            this.MobileTextEdit.MenuManager = this.barManager1;
            this.MobileTextEdit.Name = "MobileTextEdit";
            this.MobileTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MobileTextEdit, true);
            this.MobileTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MobileTextEdit, optionsSpelling13);
            this.MobileTextEdit.StyleController = this.dataLayoutControl1;
            this.MobileTextEdit.TabIndex = 8;
            this.MobileTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.MobileTextEdit_Validating);
            // 
            // FaxTextEdit
            // 
            this.FaxTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Fax", true));
            this.FaxTextEdit.Location = new System.Drawing.Point(150, 344);
            this.FaxTextEdit.MenuManager = this.barManager1;
            this.FaxTextEdit.Name = "FaxTextEdit";
            this.FaxTextEdit.Properties.MaxLength = 20;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FaxTextEdit, true);
            this.FaxTextEdit.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FaxTextEdit, optionsSpelling14);
            this.FaxTextEdit.StyleController = this.dataLayoutControl1;
            this.FaxTextEdit.TabIndex = 9;
            // 
            // strRemarksMemoEdit
            // 
            this.strRemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Remarks", true));
            this.strRemarksMemoEdit.Location = new System.Drawing.Point(36, 296);
            this.strRemarksMemoEdit.MenuManager = this.barManager1;
            this.strRemarksMemoEdit.Name = "strRemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.strRemarksMemoEdit, true);
            this.strRemarksMemoEdit.Size = new System.Drawing.Size(556, 140);
            this.scSpellChecker.SetSpellCheckerOptions(this.strRemarksMemoEdit, optionsSpelling15);
            this.strRemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.strRemarksMemoEdit.TabIndex = 11;
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Active", true));
            this.ActiveCheckEdit.Location = new System.Drawing.Point(126, 96);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(490, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 12;
            // 
            // SubContractorIDGridLookUpEdit
            // 
            this.SubContractorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "SubContractorID", true));
            this.SubContractorIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SubContractorIDGridLookUpEdit.Location = new System.Drawing.Point(126, 36);
            this.SubContractorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SubContractorIDGridLookUpEdit.Name = "SubContractorIDGridLookUpEdit";
            editorButtonImageOptions1.Image = global::WoodPlan5.Properties.Resources.Edit_16x16;
            editorButtonImageOptions2.Image = global::WoodPlan5.Properties.Resources.Refresh2_16x16;
            this.SubContractorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.SubContractorIDGridLookUpEdit.Properties.DataSource = this.sp04026ContractorListWithBlankBindingSource;
            this.SubContractorIDGridLookUpEdit.Properties.DisplayMember = "ContractorName";
            this.SubContractorIDGridLookUpEdit.Properties.NullText = "";
            this.SubContractorIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.SubContractorIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.SubContractorIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SubContractorIDGridLookUpEdit.Properties.ValueMember = "ContractorID";
            this.SubContractorIDGridLookUpEdit.Size = new System.Drawing.Size(490, 22);
            this.SubContractorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorIDGridLookUpEdit.TabIndex = 5;
            this.SubContractorIDGridLookUpEdit.TabStop = false;
            this.SubContractorIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.intContractorIDSpinEdit_ButtonClick);
            this.SubContractorIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.intContractorIDGridLookUpEdit_Validating);
            // 
            // sp04026ContractorListWithBlankBindingSource
            // 
            this.sp04026ContractorListWithBlankBindingSource.DataMember = "sp04026_Contractor_List_With_Blank";
            this.sp04026ContractorListWithBlankBindingSource.DataSource = this.dataSet_GC_DataEntry;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5,
            this.colContractorCode,
            this.colContractorID,
            this.colContractorName,
            this.colDisabled,
            this.colEmailPassword,
            this.colInternalContractor,
            this.colInternalCOntractorDescription,
            this.colMobile,
            this.colPostcode,
            this.colRemarks,
            this.colTelephone1,
            this.colTelephone2,
            this.colTypeDescription,
            this.colTypeID,
            this.colUserDefined1,
            this.colUserDefined2,
            this.colUserDefined3,
            this.colVatReg,
            this.colWebsite});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colContractorID;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridLookUpEdit1View.GroupCount = 1;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInternalCOntractorDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContractorName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 2;
            this.colAddressLine1.Width = 144;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Width = 91;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Width = 91;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Width = 91;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Width = 91;
            // 
            // colContractorCode
            // 
            this.colContractorCode.Caption = "Contractor Code";
            this.colContractorCode.FieldName = "ContractorCode";
            this.colContractorCode.Name = "colContractorCode";
            this.colContractorCode.OptionsColumn.AllowEdit = false;
            this.colContractorCode.OptionsColumn.AllowFocus = false;
            this.colContractorCode.OptionsColumn.ReadOnly = true;
            this.colContractorCode.Width = 101;
            // 
            // colContractorName
            // 
            this.colContractorName.Caption = "Contractor Name";
            this.colContractorName.FieldName = "ContractorName";
            this.colContractorName.Name = "colContractorName";
            this.colContractorName.OptionsColumn.AllowEdit = false;
            this.colContractorName.OptionsColumn.AllowFocus = false;
            this.colContractorName.OptionsColumn.ReadOnly = true;
            this.colContractorName.Visible = true;
            this.colContractorName.VisibleIndex = 0;
            this.colContractorName.Width = 273;
            // 
            // colDisabled
            // 
            this.colDisabled.Caption = "Disabled";
            this.colDisabled.FieldName = "Disabled";
            this.colDisabled.Name = "colDisabled";
            this.colDisabled.OptionsColumn.AllowEdit = false;
            this.colDisabled.OptionsColumn.AllowFocus = false;
            this.colDisabled.OptionsColumn.ReadOnly = true;
            this.colDisabled.Visible = true;
            this.colDisabled.VisibleIndex = 3;
            this.colDisabled.Width = 61;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Width = 94;
            // 
            // colInternalContractor
            // 
            this.colInternalContractor.Caption = "Internal Contractor ID";
            this.colInternalContractor.FieldName = "InternalContractor";
            this.colInternalContractor.Name = "colInternalContractor";
            this.colInternalContractor.OptionsColumn.AllowEdit = false;
            this.colInternalContractor.OptionsColumn.AllowFocus = false;
            this.colInternalContractor.OptionsColumn.ReadOnly = true;
            this.colInternalContractor.Width = 118;
            // 
            // colInternalCOntractorDescription
            // 
            this.colInternalCOntractorDescription.Caption = "Status";
            this.colInternalCOntractorDescription.FieldName = "InternalCOntractorDescription";
            this.colInternalCOntractorDescription.Name = "colInternalCOntractorDescription";
            this.colInternalCOntractorDescription.OptionsColumn.AllowEdit = false;
            this.colInternalCOntractorDescription.OptionsColumn.AllowFocus = false;
            this.colInternalCOntractorDescription.OptionsColumn.ReadOnly = true;
            this.colInternalCOntractorDescription.Width = 52;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Width = 51;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Width = 65;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Width = 62;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colTelephone1
            // 
            this.colTelephone1.Caption = "Telephone 1";
            this.colTelephone1.FieldName = "Telephone1";
            this.colTelephone1.Name = "colTelephone1";
            this.colTelephone1.OptionsColumn.AllowEdit = false;
            this.colTelephone1.OptionsColumn.AllowFocus = false;
            this.colTelephone1.OptionsColumn.ReadOnly = true;
            this.colTelephone1.Width = 80;
            // 
            // colTelephone2
            // 
            this.colTelephone2.Caption = "Telephone 2";
            this.colTelephone2.FieldName = "Telephone2";
            this.colTelephone2.Name = "colTelephone2";
            this.colTelephone2.OptionsColumn.AllowEdit = false;
            this.colTelephone2.OptionsColumn.AllowFocus = false;
            this.colTelephone2.OptionsColumn.ReadOnly = true;
            this.colTelephone2.Width = 80;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Type";
            this.colTypeDescription.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 1;
            this.colTypeDescription.Width = 111;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            this.colTypeID.Width = 49;
            // 
            // colUserDefined1
            // 
            this.colUserDefined1.Caption = "User Defined 1";
            this.colUserDefined1.FieldName = "UserDefined1";
            this.colUserDefined1.Name = "colUserDefined1";
            this.colUserDefined1.OptionsColumn.AllowEdit = false;
            this.colUserDefined1.OptionsColumn.AllowFocus = false;
            this.colUserDefined1.OptionsColumn.ReadOnly = true;
            this.colUserDefined1.Width = 92;
            // 
            // colUserDefined2
            // 
            this.colUserDefined2.Caption = "User Defined 2";
            this.colUserDefined2.FieldName = "UserDefined2";
            this.colUserDefined2.Name = "colUserDefined2";
            this.colUserDefined2.OptionsColumn.AllowEdit = false;
            this.colUserDefined2.OptionsColumn.AllowFocus = false;
            this.colUserDefined2.OptionsColumn.ReadOnly = true;
            this.colUserDefined2.Width = 92;
            // 
            // colUserDefined3
            // 
            this.colUserDefined3.Caption = "User Defined 3";
            this.colUserDefined3.FieldName = "UserDefined3";
            this.colUserDefined3.Name = "colUserDefined3";
            this.colUserDefined3.OptionsColumn.AllowEdit = false;
            this.colUserDefined3.OptionsColumn.AllowFocus = false;
            this.colUserDefined3.OptionsColumn.ReadOnly = true;
            this.colUserDefined3.Width = 92;
            // 
            // colVatReg
            // 
            this.colVatReg.Caption = "VAT Reg";
            this.colVatReg.FieldName = "VatReg";
            this.colVatReg.Name = "colVatReg";
            this.colVatReg.OptionsColumn.AllowEdit = false;
            this.colVatReg.OptionsColumn.AllowFocus = false;
            this.colVatReg.OptionsColumn.ReadOnly = true;
            this.colVatReg.Width = 62;
            // 
            // colWebsite
            // 
            this.colWebsite.Caption = "Website";
            this.colWebsite.FieldName = "Website";
            this.colWebsite.Name = "colWebsite";
            this.colWebsite.OptionsColumn.AllowEdit = false;
            this.colWebsite.OptionsColumn.AllowFocus = false;
            this.colWebsite.OptionsColumn.ReadOnly = true;
            this.colWebsite.Width = 60;
            // 
            // EmailMemoEdit
            // 
            this.EmailMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Email", true));
            this.EmailMemoEdit.Location = new System.Drawing.Point(150, 368);
            this.EmailMemoEdit.MenuManager = this.barManager1;
            this.EmailMemoEdit.Name = "EmailMemoEdit";
            this.EmailMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmailMemoEdit, true);
            this.EmailMemoEdit.Size = new System.Drawing.Size(442, 32);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmailMemoEdit, optionsSpelling16);
            this.EmailMemoEdit.StyleController = this.dataLayoutControl1;
            this.EmailMemoEdit.TabIndex = 10;
            // 
            // TextMemoEdit
            // 
            this.TextMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "Text", true));
            this.TextMemoEdit.Location = new System.Drawing.Point(150, 404);
            this.TextMemoEdit.MenuManager = this.barManager1;
            this.TextMemoEdit.Name = "TextMemoEdit";
            this.TextMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TextMemoEdit, true);
            this.TextMemoEdit.Size = new System.Drawing.Size(442, 32);
            this.scSpellChecker.SetSpellCheckerOptions(this.TextMemoEdit, optionsSpelling17);
            this.TextMemoEdit.StyleController = this.dataLayoutControl1;
            this.TextMemoEdit.TabIndex = 21;
            // 
            // dateAddedDateEdit
            // 
            this.dateAddedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "DateAdded", true));
            this.dateAddedDateEdit.EditValue = null;
            this.dateAddedDateEdit.Location = new System.Drawing.Point(126, 167);
            this.dateAddedDateEdit.Name = "dateAddedDateEdit";
            this.dateAddedDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateAddedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateAddedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateAddedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateAddedDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.dateAddedDateEdit.Size = new System.Drawing.Size(153, 20);
            this.dateAddedDateEdit.StyleController = this.dataLayoutControl1;
            this.dateAddedDateEdit.TabIndex = 21;
            // 
            // NatInsuranceNumberTextEdit
            // 
            this.NatInsuranceNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp04024GCSubContractorTeamMemberEditBindingSource, "NatInsuranceNumber", true));
            this.NatInsuranceNumberTextEdit.Location = new System.Drawing.Point(126, 191);
            this.NatInsuranceNumberTextEdit.MenuManager = this.barManager1;
            this.NatInsuranceNumberTextEdit.Name = "NatInsuranceNumberTextEdit";
            this.NatInsuranceNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.NatInsuranceNumberTextEdit, true);
            this.NatInsuranceNumberTextEdit.Size = new System.Drawing.Size(150, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.NatInsuranceNumberTextEdit, optionsSpelling18);
            this.NatInsuranceNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.NatInsuranceNumberTextEdit.TabIndex = 7;
            // 
            // ItemForTeamMemberID
            // 
            this.ItemForTeamMemberID.Control = this.TeamMemberIDTextEdit;
            this.ItemForTeamMemberID.CustomizationFormText = "Contact ID:";
            this.ItemForTeamMemberID.Location = new System.Drawing.Point(0, 73);
            this.ItemForTeamMemberID.Name = "ItemForTeamMemberID";
            this.ItemForTeamMemberID.Size = new System.Drawing.Size(608, 24);
            this.ItemForTeamMemberID.Text = "Contact ID:";
            this.ItemForTeamMemberID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 483);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSubContractorID,
            this.ItemForName,
            this.layoutControlGroup3,
            this.layoutControlItem1,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.ItemForActive,
            this.emptySpaceItem5,
            this.ItemForGenderID,
            this.ItemForRoleID,
            this.emptySpaceItem1,
            this.ItemForDateAdded,
            this.ItemForNatInsuranceNumber,
            this.emptySpaceItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 452);
            // 
            // ItemForSubContractorID
            // 
            this.ItemForSubContractorID.Control = this.SubContractorIDGridLookUpEdit;
            this.ItemForSubContractorID.CustomizationFormText = "Team:";
            this.ItemForSubContractorID.Location = new System.Drawing.Point(0, 24);
            this.ItemForSubContractorID.Name = "ItemForSubContractorID";
            this.ItemForSubContractorID.Size = new System.Drawing.Size(608, 26);
            this.ItemForSubContractorID.Text = "Team:";
            this.ItemForSubContractorID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForName
            // 
            this.ItemForName.Control = this.NameTextEdit;
            this.ItemForName.CustomizationFormText = "Team Member Name:";
            this.ItemForName.Location = new System.Drawing.Point(0, 50);
            this.ItemForName.Name = "ItemForName";
            this.ItemForName.Size = new System.Drawing.Size(608, 24);
            this.ItemForName.Text = "Team Member Name:";
            this.ItemForName.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Details";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 214);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(608, 238);
            this.layoutControlGroup3.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(584, 192);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Contact Details";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTelephone,
            this.ItemForMobile,
            this.ItemForFax,
            this.ItemForEmail,
            this.ItemForText});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(560, 144);
            this.layoutControlGroup4.Text = "Contact Details";
            // 
            // ItemForTelephone
            // 
            this.ItemForTelephone.Control = this.TelephoneTextEdit;
            this.ItemForTelephone.CustomizationFormText = "Telephone:";
            this.ItemForTelephone.Location = new System.Drawing.Point(0, 0);
            this.ItemForTelephone.Name = "ItemForTelephone";
            this.ItemForTelephone.Size = new System.Drawing.Size(560, 24);
            this.ItemForTelephone.Text = "Telephone:";
            this.ItemForTelephone.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForMobile
            // 
            this.ItemForMobile.Control = this.MobileTextEdit;
            this.ItemForMobile.CustomizationFormText = "Mobile:";
            this.ItemForMobile.Location = new System.Drawing.Point(0, 24);
            this.ItemForMobile.Name = "ItemForMobile";
            this.ItemForMobile.Size = new System.Drawing.Size(560, 24);
            this.ItemForMobile.Text = "Mobile:";
            this.ItemForMobile.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForFax
            // 
            this.ItemForFax.Control = this.FaxTextEdit;
            this.ItemForFax.CustomizationFormText = "Fax:";
            this.ItemForFax.Location = new System.Drawing.Point(0, 48);
            this.ItemForFax.Name = "ItemForFax";
            this.ItemForFax.Size = new System.Drawing.Size(560, 24);
            this.ItemForFax.Text = "Fax:";
            this.ItemForFax.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForEmail
            // 
            this.ItemForEmail.Control = this.EmailMemoEdit;
            this.ItemForEmail.CustomizationFormText = "Email:";
            this.ItemForEmail.Location = new System.Drawing.Point(0, 72);
            this.ItemForEmail.Name = "ItemForEmail";
            this.ItemForEmail.Size = new System.Drawing.Size(560, 36);
            this.ItemForEmail.Text = "Email:";
            this.ItemForEmail.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForText
            // 
            this.ItemForText.Control = this.TextMemoEdit;
            this.ItemForText.CustomizationFormText = "Text:";
            this.ItemForText.Location = new System.Drawing.Point(0, 108);
            this.ItemForText.Name = "ItemForText";
            this.ItemForText.Size = new System.Drawing.Size(560, 36);
            this.ItemForText.Text = "Text:";
            this.ItemForText.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup6.CaptionImageOptions.Image")));
            this.layoutControlGroup6.CustomizationFormText = "Address";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAddressLine1,
            this.ItemForAddressLine2,
            this.ItemForAddressLine3,
            this.ItemForAddressLine4,
            this.ItemForAddressLine5,
            this.ItemForPostcode});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(560, 144);
            this.layoutControlGroup6.Text = "Address";
            // 
            // ItemForAddressLine1
            // 
            this.ItemForAddressLine1.Control = this.AddressLine1TextEdit;
            this.ItemForAddressLine1.CustomizationFormText = "Address Line 1:";
            this.ItemForAddressLine1.Location = new System.Drawing.Point(0, 0);
            this.ItemForAddressLine1.Name = "ItemForAddressLine1";
            this.ItemForAddressLine1.Size = new System.Drawing.Size(560, 24);
            this.ItemForAddressLine1.Text = "Address Line 1:";
            this.ItemForAddressLine1.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForAddressLine2
            // 
            this.ItemForAddressLine2.Control = this.AddressLine2TextEdit;
            this.ItemForAddressLine2.CustomizationFormText = "Address Line 2:";
            this.ItemForAddressLine2.Location = new System.Drawing.Point(0, 24);
            this.ItemForAddressLine2.Name = "ItemForAddressLine2";
            this.ItemForAddressLine2.Size = new System.Drawing.Size(560, 24);
            this.ItemForAddressLine2.Text = "Address Line 2:";
            this.ItemForAddressLine2.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForAddressLine3
            // 
            this.ItemForAddressLine3.Control = this.AddressLine3TextEdit;
            this.ItemForAddressLine3.CustomizationFormText = "Address Line 3:";
            this.ItemForAddressLine3.Location = new System.Drawing.Point(0, 48);
            this.ItemForAddressLine3.Name = "ItemForAddressLine3";
            this.ItemForAddressLine3.Size = new System.Drawing.Size(560, 24);
            this.ItemForAddressLine3.Text = "Address Line 3:";
            this.ItemForAddressLine3.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForAddressLine4
            // 
            this.ItemForAddressLine4.Control = this.AddressLine4TextEdit;
            this.ItemForAddressLine4.CustomizationFormText = "Address Line 4:";
            this.ItemForAddressLine4.Location = new System.Drawing.Point(0, 72);
            this.ItemForAddressLine4.Name = "ItemForAddressLine4";
            this.ItemForAddressLine4.Size = new System.Drawing.Size(560, 24);
            this.ItemForAddressLine4.Text = "Address Line 4:";
            this.ItemForAddressLine4.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForAddressLine5
            // 
            this.ItemForAddressLine5.Control = this.AddressLine5TextEdit;
            this.ItemForAddressLine5.CustomizationFormText = "Address Line 5:";
            this.ItemForAddressLine5.Location = new System.Drawing.Point(0, 96);
            this.ItemForAddressLine5.Name = "ItemForAddressLine5";
            this.ItemForAddressLine5.Size = new System.Drawing.Size(560, 24);
            this.ItemForAddressLine5.Text = "Address Line 5:";
            this.ItemForAddressLine5.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForPostcode
            // 
            this.ItemForPostcode.Control = this.PostcodeTextEdit;
            this.ItemForPostcode.CustomizationFormText = "Postcode:";
            this.ItemForPostcode.Location = new System.Drawing.Point(0, 120);
            this.ItemForPostcode.Name = "ItemForPostcode";
            this.ItemForPostcode.Size = new System.Drawing.Size(560, 24);
            this.ItemForPostcode.Text = "Postcode:";
            this.ItemForPostcode.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Device Connection";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPDAUserName,
            this.ItemForPDAPassword,
            this.ItemForPDALoginToken,
            this.layoutControlItem2,
            this.ItemForGritJobAllocateByDefault});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(560, 144);
            this.layoutControlGroup7.Text = "Device Connection";
            // 
            // ItemForPDAUserName
            // 
            this.ItemForPDAUserName.Control = this.PDAUserNameTextEdit;
            this.ItemForPDAUserName.CustomizationFormText = "Device User Name:";
            this.ItemForPDAUserName.Location = new System.Drawing.Point(0, 23);
            this.ItemForPDAUserName.Name = "ItemForPDAUserName";
            this.ItemForPDAUserName.Size = new System.Drawing.Size(560, 24);
            this.ItemForPDAUserName.Text = "Device User Name:";
            this.ItemForPDAUserName.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForPDAPassword
            // 
            this.ItemForPDAPassword.Control = this.PDAPasswordTextEdit;
            this.ItemForPDAPassword.CustomizationFormText = "Device Password:";
            this.ItemForPDAPassword.Location = new System.Drawing.Point(0, 47);
            this.ItemForPDAPassword.Name = "ItemForPDAPassword";
            this.ItemForPDAPassword.Size = new System.Drawing.Size(560, 24);
            this.ItemForPDAPassword.Text = "Device Password:";
            this.ItemForPDAPassword.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForPDALoginToken
            // 
            this.ItemForPDALoginToken.Control = this.PDALoginTokenTextEdit;
            this.ItemForPDALoginToken.CustomizationFormText = "Device Login Token:";
            this.ItemForPDALoginToken.Location = new System.Drawing.Point(0, 71);
            this.ItemForPDALoginToken.Name = "ItemForPDALoginToken";
            this.ItemForPDALoginToken.Size = new System.Drawing.Size(477, 73);
            this.ItemForPDALoginToken.Text = "Device Login Token:";
            this.ItemForPDALoginToken.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnCreateGUID;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(477, 71);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(83, 73);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForGritJobAllocateByDefault
            // 
            this.ItemForGritJobAllocateByDefault.Control = this.GritJobAllocateByDefaultCheckEdit;
            this.ItemForGritJobAllocateByDefault.CustomizationFormText = "Default Jobs Allocated:";
            this.ItemForGritJobAllocateByDefault.Location = new System.Drawing.Point(0, 0);
            this.ItemForGritJobAllocateByDefault.Name = "ItemForGritJobAllocateByDefault";
            this.ItemForGritJobAllocateByDefault.Size = new System.Drawing.Size(560, 23);
            this.ItemForGritJobAllocateByDefault.Text = "Default Jobs Allocated:";
            this.ItemForGritJobAllocateByDefault.TextSize = new System.Drawing.Size(111, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImageOptions.Image")));
            this.layoutControlGroup5.CustomizationFormText = "Remarks";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForstrRemarks});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(560, 144);
            this.layoutControlGroup5.Text = "Remarks";
            // 
            // ItemForstrRemarks
            // 
            this.ItemForstrRemarks.Control = this.strRemarksMemoEdit;
            this.ItemForstrRemarks.CustomizationFormText = "Remarks:";
            this.ItemForstrRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForstrRemarks.Name = "ItemForstrRemarks";
            this.ItemForstrRemarks.Size = new System.Drawing.Size(560, 144);
            this.ItemForstrRemarks.Text = "Remarks:";
            this.ItemForstrRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForstrRemarks.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(115, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(115, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(115, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(115, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(292, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(316, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForActive
            // 
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.CustomizationFormText = "Active:";
            this.ItemForActive.Location = new System.Drawing.Point(0, 84);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(608, 23);
            this.ItemForActive.Text = "Active:";
            this.ItemForActive.TextSize = new System.Drawing.Size(111, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 74);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForGenderID
            // 
            this.ItemForGenderID.Control = this.GenderIDGridLookUpEdit;
            this.ItemForGenderID.CustomizationFormText = "Gender:";
            this.ItemForGenderID.Location = new System.Drawing.Point(0, 107);
            this.ItemForGenderID.Name = "ItemForGenderID";
            this.ItemForGenderID.Size = new System.Drawing.Size(608, 24);
            this.ItemForGenderID.Text = "Gender:";
            this.ItemForGenderID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForRoleID
            // 
            this.ItemForRoleID.Control = this.RoleIDGridLookUpEdit;
            this.ItemForRoleID.CustomizationFormText = "Role:";
            this.ItemForRoleID.Location = new System.Drawing.Point(0, 131);
            this.ItemForRoleID.Name = "ItemForRoleID";
            this.ItemForRoleID.Size = new System.Drawing.Size(608, 24);
            this.ItemForRoleID.Text = "Role:";
            this.ItemForRoleID.TextSize = new System.Drawing.Size(111, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 203);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(608, 11);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(608, 11);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(608, 11);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDateAdded
            // 
            this.ItemForDateAdded.Control = this.dateAddedDateEdit;
            this.ItemForDateAdded.CustomizationFormText = "Date Added:";
            this.ItemForDateAdded.Location = new System.Drawing.Point(0, 155);
            this.ItemForDateAdded.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForDateAdded.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForDateAdded.Name = "ItemForDateAdded";
            this.ItemForDateAdded.Size = new System.Drawing.Size(608, 24);
            this.ItemForDateAdded.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForDateAdded.Text = "Date Added:";
            this.ItemForDateAdded.TextSize = new System.Drawing.Size(111, 13);
            // 
            // ItemForNatInsuranceNumber
            // 
            this.ItemForNatInsuranceNumber.Control = this.NatInsuranceNumberTextEdit;
            this.ItemForNatInsuranceNumber.CustomizationFormText = "NI Number:";
            this.ItemForNatInsuranceNumber.Location = new System.Drawing.Point(0, 179);
            this.ItemForNatInsuranceNumber.Name = "ItemForNatInsuranceNumber";
            this.ItemForNatInsuranceNumber.Size = new System.Drawing.Size(268, 24);
            this.ItemForNatInsuranceNumber.Text = "NI Number:";
            this.ItemForNatInsuranceNumber.TextSize = new System.Drawing.Size(111, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(268, 179);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(340, 24);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 452);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 11);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp04024_GC_SubContractor_Team_Member_EditTableAdapter
            // 
            this.sp04024_GC_SubContractor_Team_Member_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp04026_Contractor_List_With_BlankTableAdapter
            // 
            this.sp04026_Contractor_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp04330_Core_Team_Member_Roles_ListTableAdapter
            // 
            this.sp04330_Core_Team_Member_Roles_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04331_Core_Team_Member_Genders_ListTableAdapter
            // 
            this.sp04331_Core_Team_Member_Genders_ListTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Contractor_Team_Member_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 537);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Contractor_Team_Member_Edit";
            this.Text = "Edit Team Member";
            this.Activated += new System.EventHandler(this.frm_Core_Contractor_Team_Member_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Contractor_Team_Member_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Contractor_Team_Member_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GritJobAllocateByDefaultCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04024GCSubContractorTeamMemberEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDALoginTokenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDAPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDAUserNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoleIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04330CoreTeamMemberRolesListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GenderIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04331CoreTeamMemberGendersListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostcodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine5TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine4TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine3TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AddressLine1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TeamMemberIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelephoneTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FaxTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.strRemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04026ContractorListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmailMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateAddedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateAddedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NatInsuranceNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMemberID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTelephone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAddressLine5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPostcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDAUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDAPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDALoginToken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGritJobAllocateByDefault)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForstrRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGenderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRoleID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateAdded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNatInsuranceNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit TeamMemberIDTextEdit;
        private DevExpress.XtraEditors.TextEdit NameTextEdit;
        private DevExpress.XtraEditors.TextEdit TelephoneTextEdit;
        private DevExpress.XtraEditors.TextEdit MobileTextEdit;
        private DevExpress.XtraEditors.TextEdit FaxTextEdit;
        private DevExpress.XtraEditors.MemoEdit strRemarksMemoEdit;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamMemberID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForstrRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTelephone;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobile;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFax;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmail;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.GridLookUpEdit SubContractorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorCode;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorID;
        private DevExpress.XtraGrid.Columns.GridColumn colContractorName;
        private DevExpress.XtraGrid.Columns.GridColumn colDisabled;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalContractor;
        private DevExpress.XtraGrid.Columns.GridColumn colInternalCOntractorDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone1;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone2;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined1;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined2;
        private DevExpress.XtraGrid.Columns.GridColumn colUserDefined3;
        private DevExpress.XtraGrid.Columns.GridColumn colVatReg;
        private DevExpress.XtraGrid.Columns.GridColumn colWebsite;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp04024GCSubContractorTeamMemberEditBindingSource;
        private DataSet_GC_DataEntry dataSet_GC_DataEntry;
        private DataSet_GC_DataEntryTableAdapters.sp04024_GC_SubContractor_Team_Member_EditTableAdapter sp04024_GC_SubContractor_Team_Member_EditTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraEditors.TextEdit PostcodeTextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine5TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine4TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine3TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine2TextEdit;
        private DevExpress.XtraEditors.TextEdit AddressLine1TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAddressLine5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPostcode;
        private DevExpress.XtraEditors.MemoEdit EmailMemoEdit;
        private DevExpress.XtraEditors.MemoEdit TextMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForText;
        private System.Windows.Forms.BindingSource sp04026ContractorListWithBlankBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04026_Contractor_List_With_BlankTableAdapter sp04026_Contractor_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit RoleIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit2View;
        private DevExpress.XtraEditors.GridLookUpEdit GenderIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGenderID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRoleID;
        private System.Windows.Forms.BindingSource sp04330CoreTeamMemberRolesListBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04330_Core_Team_Member_Roles_ListTableAdapter sp04330_Core_Team_Member_Roles_ListTableAdapter;
        private System.Windows.Forms.BindingSource sp04331CoreTeamMemberGendersListBindingSource;
        private DataSet_GC_DataEntryTableAdapters.sp04331_Core_Team_Member_Genders_ListTableAdapter sp04331_Core_Team_Member_Genders_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colItemOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.TextEdit PDAPasswordTextEdit;
        private DevExpress.XtraEditors.TextEdit PDAUserNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDAUserName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDAPassword;
        private DevExpress.XtraEditors.TextEdit PDALoginTokenTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDALoginToken;
        private DevExpress.XtraEditors.SimpleButton btnCreateGUID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.CheckEdit GritJobAllocateByDefaultCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGritJobAllocateByDefault;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.DateEdit dateAddedDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateAdded;
        private DevExpress.XtraEditors.TextEdit NatInsuranceNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNatInsuranceNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
    }
}

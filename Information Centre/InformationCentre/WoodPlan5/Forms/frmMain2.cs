using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Xml;
using System.Threading;
using System.Reflection;
using System.Runtime.InteropServices;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.Skins;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Ribbon.Gallery;
using DevExpress.Utils.Drawing;
using DevExpress.Utils;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraNavBar;

using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts.Wizard;
using DevExpress.XtraPivotGrid;

using System.Text;
using WoodPlan5.Properties;
using BaseObjects;

using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace WoodPlan5
{
    struct stcDatabase
    {
        public string strServerName;
        public string strDatabaseName;
        public string strFriendlyName;
        public int intDefault;
    }

    public partial class frmMain2 : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        [DllImport("user32.dll")]
        private extern static IntPtr SetActiveWindow(IntPtr handle);

        [DllImport("User32.dll")]
        protected static extern int SetClipboardViewer(int hWndNewViewer);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove,IntPtr hWndNewNext);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);


        #region Instance Variables //
        private string strPassedDatabase = "";

        private object objTest;
        private NavBarItem nbiSelected;

        private Boolean blLoad = false;
        private Boolean blFirstTime = true;

        private Boolean blAbortLoad = false;

        /// <summary>
        /// 
        /// </summary>
        public GlobalSettings GlobalSettings; 
        private frmSplash fSplash;
        private Thread t;
        private int intStaffID;
        private int intUserID;
        private string strUserType;
        private int intSystemID;
        private int intApplicationConfigID;
        private Settings set = Settings.Default;
        private ArrayList alNames = new ArrayList();
        private string strConnectionString = "";
        private string strServerName = "";
        private string strDatabaseName = "";
        private string strFriendlyName = "";
        private Thread tGallery;
        private string strUserName = "";
        private string strActualUserName = "";
        private ArrayList alExcludedMenuItems = new ArrayList();
        private Thread tMagnifier = null;

        /// <summary>
        /// 
        /// </summary>
        public Boolean blPanelHasFocus = false;

        private Boolean ib_LoadLayout = false;
        private Boolean ib_LoadLayout_Delete = false;
        private Boolean ib_SaveLayout = false;
        private Boolean ib_SaveLayoutAs = false;
        private Boolean ib_LoadFilter = false;
        private Boolean ib_LoadFilter_Delete = false; 
        private Boolean ib_CreateFilter = false;
        private Boolean ib_SaveFilter = false;
        private Boolean ib_SaveFilterAs = false;
        private Boolean ib_ToDoList = false;
        private Boolean ib_FastFind = false;
        private Boolean ib_Calculator = false;
        private Boolean ib_Clipboard = false;
        private Boolean ib_CommentBank = false;

        public ChartAppearanceManager chartAppearanceManager = null;
        #endregion


        public frmMain2(Thread t)
        {
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);
            this.t = t;

            intSystemID = 1;
            this.StartPosition = FormStartPosition.CenterScreen;

            // Required for Windows Form Designer support
            InitializeComponent();
            //this.notifyIcon1.Visible = false;
            
            Skin skin = CommonSkins.GetSkin(DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveLookAndFeel);
            skin.ColorizationMode = SkinColorizationMode.Color;
            
            FillExcludedMenuItemsArray();

            // StartNotificationTimer();

            //tNotifications = new Thread(new ThreadStart(StartNotificationTimer));
            //tNotifications.Start();

            CreateColorPopup(popupControlContainer1);

            //this.HotKeyPressed +=new KeyPressEventHandler(frmMain2_HotKeyPressed);

            //BaseObjects.HotKeys hkCtrlC = new HotKeys(Keys.C, HotKeys.KeyModifiers.Control, frmMain2_HotKeyPressed);
            //Application.AddMessageFilter(hkCtrlC);
            //BaseObjects.HotKeys hkCtrlV = new HotKeys(Keys.V, HotKeys.KeyModifiers.Control, frmMain2_HotKeyPressed);
            //Application.AddMessageFilter(hkCtrlV);
        }

        public frmMain2(Thread t, string strDatabase)
        {
            this.SetStyle(ControlStyles.DoubleBuffer | ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint, true);

            this.t = t;

            strPassedDatabase = strDatabase;
            intSystemID = 1;
            this.StartPosition = FormStartPosition.CenterScreen;

            // Required for Windows Form Designer support
            InitializeComponent();
            this.notifyIcon1.Visible = false;

            Skin skin = CommonSkins.GetSkin(DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveLookAndFeel);
            skin.ColorizationMode = SkinColorizationMode.Color;

            FillExcludedMenuItemsArray();

            // StartNotificationTimer();

            //tNotifications = new Thread(new ThreadStart(StartNotificationTimer));
            //tNotifications.Start();

            CreateColorPopup(popupControlContainer1);


            //this.HotKeyPressed +=new KeyPressEventHandler(frmMain2_HotKeyPressed);

            //BaseObjects.HotKeys hkCtrlC = new HotKeys(Keys.C, HotKeys.KeyModifiers.Control, frmMain2_HotKeyPressed);
            //Application.AddMessageFilter(hkCtrlC);
            //BaseObjects.HotKeys hkCtrlV = new HotKeys(Keys.V, HotKeys.KeyModifiers.Control, frmMain2_HotKeyPressed);
            //Application.AddMessageFilter(hkCtrlV);
        }

        //void frmMain2_HotKeyPressed(object sender, KeyPressEventArgs e)
        //{
        //    DevExpress.XtraBars.Docking.DockPanel dpPanel;
        //    ctrlBase ctrlClip;

        //    if (e.KeyChar == 'C')
        //    {
        //        blCopyFunction = true;
        //        blPasteFunction = false;
        //    }
        //    else if (e.KeyChar == 'V')
        //    {
        //        blCopyFunction = false;
        //        blPasteFunction = true;
        //    }
        //    else
        //    {
        //        blCopyFunction = false;
        //        blPasteFunction = false;
        //    }
        //}

//        public event KeyPressEventHandler HotKeyPressed;

        private void InitializeModules()
        {
            string strPath;

            fSplash = new frmSplash(1);
            strPath = Application.StartupPath;

            // --- ADD EACH MODULE IN HERE --- //
            stcItems newItems = new stcItems();
            
            newItems.iItemID = 1;
            newItems.strItemName = "Amenity Trees";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 2;
            newItems.strItemName = "Asset Management";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 6;
            newItems.strItemName = "CRM";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 5;
            newItems.strItemName = "Tender Register";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 4;
            newItems.strItemName = "Winter Maintenance";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 7;
            newItems.strItemName = "Summer Maintenance";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 8;
            newItems.strItemName = "Utility Arb";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 9;
            newItems.strItemName = "HR";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 10;
            newItems.strItemName = "Training";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 11;
            newItems.strItemName = "Equipment";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 9000;
            newItems.strItemName = "Core";
            alNames.Add(newItems);

            newItems = new stcItems();
            newItems.iItemID = 9999;
            newItems.strItemName = "System Administration";
            alNames.Add(newItems);
            // ------------------------------- //


            fSplash.BeginStartupProcess(alNames, intStaffID, intSystemID);
            fSplash.strPassedDatabase = strPassedDatabase;
            fSplash.SetUpProgressBar(alNames.Count);
            fSplash.Height = 285;
            fSplash.Width = 368;

            fSplash.StartPosition = FormStartPosition.CenterScreen;
            fSplash.frmParent = this;
            fSplash.TopMost = true;
            fSplash.aThread = t;
            DialogResult drResult = fSplash.ShowDialog();

            if (drResult == DialogResult.Yes)
            {
                strServerName = fSplash.strServerName;
                strDatabaseName = fSplash.strDatabaseName;
                strFriendlyName = fSplash.strFriendlyName;
                strUserName = fSplash.strUsername;
                intUserID = fSplash.intUserID;
                strUserType = fSplash.strUserType;
                strActualUserName = fSplash.strActualID;
                strConnectionString = fSplash.strConnectionString;

                fSplash.Close();
                fSplash = null;
                this.WindowState = FormWindowState.Maximized;
                this.notifyIcon1.Visible = true;
                openGiroToolStripMenuItem.Text = "Hide Information Centre";
                blLoad = true;
            }
            else if (drResult == DialogResult.Abort)
            {
                blAbortLoad = true;
                blLoad = false;
                this.Close();
            }
            else
            {
                blAbortLoad = true;
                blLoad = false;
                this.Close();
            }
        }

        ColorPopup cp;
        private void CreateColorPopup(PopupControlContainer container)
        {
            cp = new ColorPopup(container, iFontColour, this);
        }

        private void iFont_ItemClick(object sender, ItemClickEventArgs e)
        {
 //           ShowFontDialog();
        }

        private void iFontColor_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
         //   if (CurrentRichTextBox == null) return;
         //   CurrentRichTextBox.SelectionColor = cp.ResultColor;
        }

        #region SkinGallery
        
        void InitSkinGallery()
        {
            DevExpress.XtraBars.Helpers.SkinHelper.InitSkinGallery(rgbiSkins, true);
            rgbiSkins.Enabled = true;
        }

        void LookAndFeel_StyleChanged(object sender, EventArgs e)
        {
            var name = defaultLookAndFeel1.LookAndFeel.SkinName;
            if (name.ToLower() == "the bezier")
            {
                using (var dialog = new DevExpress.Customization.SvgSkinPaletteSelector(this))
                {
                    dialog.ShowDialog();
                }
            }
            if (this.MdiChildren.Length <= 0) return;
            foreach (frmBase fbBase in this.MdiChildren)
            {
                fbBase.Set_Grid_Highlighter_Transparent(fbBase.Controls);
            }
        }

        #endregion

        #region FontGallery
        
        Image GetFontImage(int width, int height, string fontName, int fontSize)
        {
            Rectangle rect = new Rectangle(0, 0, width, height);
            Image fontImage = new Bitmap(width, height);
            try
            {
                using (Font fontSample = new Font(fontName, fontSize))
                {
                    Graphics g = Graphics.FromImage(fontImage);
                    g.FillRectangle(Brushes.White, rect);
                    using (StringFormat fs = new StringFormat())
                    {
                        fs.Alignment = StringAlignment.Center;
                        fs.LineAlignment = StringAlignment.Center;
                        g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                        g.DrawString("Aa", fontSample, Brushes.Black, rect, fs);
                        g.Dispose();
                    }
                }
            }
            catch { }
            return fontImage;
        }

        void InitFont(GalleryItemGroup groupDropDown)
        {
            for (int i = 0; i < FontFamily.Families.Length; i++)
            {
                string fontName = FontFamily.Families[i].Name;
                GalleryItem item = new GalleryItem();
                item.Caption = fontName;
                item.Image = GetFontImage(32, 28, fontName, 12);
                item.HoverImage = item.Image;
                item.Description = fontName;
                item.Hint = fontName;
                try
                {
                    item.Tag = new Font(fontName, 9);
                }
                catch
                {
                    continue;
                }
                groupDropDown.Items.Add(item);
            }
        }

        void InitFontGallery()
        {
            InitFont(gddFont.Gallery.Groups[0]);
            beiFontSize.EditValue = 8;
            iFont.Enabled = true;
         }

        private void gddFont_Gallery_CustomDrawItemText(object sender, GalleryItemCustomDrawEventArgs e)
        {
            //DevExpress.XtraBars.Ribbon.ViewInfo.GalleryItemViewInfo itemInfo = e.ItemInfo as DevExpress.XtraBars.Ribbon.ViewInfo.GalleryItemViewInfo;
            //itemInfo.PaintAppearance.ItemDescription.DrawString(e.Cache, e.Item.Description, itemInfo.DescriptionBounds);
            //AppearanceObject app = itemInfo.PaintAppearance.ItemCaption.Clone() as AppearanceObject;
            //app.Font = (Font)e.Item.Tag;
            //app.DrawString(e.Cache, e.Item.Caption, itemInfo.CaptionBounds);
            //e.Handled = true;
        }

        #endregion

        #region ColorGallery
        
        void InitColorGallery()
        {
            gddFontColour.BeginUpdate();
            foreach (Color color in DevExpress.XtraEditors.Popup.ColorListBoxViewInfo.WebColors)
            {
                if (color == Color.Transparent) continue;
                GalleryItem item = new GalleryItem();
                item.Caption = color.Name;
                item.Tag = color;
                item.Hint = color.Name;
                gddFontColour.Gallery.Groups[0].Items.Add(item);
                //rgbiFontColour.Gallery.Groups[0].Items.Add(item);
            }
            foreach (Color color in DevExpress.XtraEditors.Popup.ColorListBoxViewInfo.SystemColors)
            {
                GalleryItem item = new GalleryItem();
                item.Caption = color.Name;
                item.Tag = color;
                gddFontColour.Gallery.Groups[1].Items.Add(item);
            }
            gddFontColour.EndUpdate();

            iFontColour.Enabled = true;
        }
        
        private void gddFontColor_Gallery_CustomDrawItemImage(object sender, GalleryItemCustomDrawEventArgs e)
        {
            Color clr = (Color)e.Item.Tag;
            using (Brush brush = new SolidBrush(clr))
            {
                e.Cache.FillRectangle(brush, e.Bounds);
                e.Handled = true;
            }
        }
        
        void SetResultColor(Color color, GalleryItem item)
        {
            //if (CurrentRichTextBox == null) return;
            cp.ResultColor = color;
            //CurrentRichTextBox.SelectionColor = cp.ResultColor;
            if (item != null) CurrentColorItem = item;
        }
        
        private void gddFontColor_Gallery_ItemClick(object sender, DevExpress.XtraBars.Ribbon.GalleryItemClickEventArgs e)
        {
            SetResultColor((Color)e.Item.Tag, e.Item);
        }

        private void rgbiFontColor_Gallery_ItemClick(object sender, DevExpress.XtraBars.Ribbon.GalleryItemClickEventArgs e)
        {
            SetResultColor((Color)e.Item.Tag, e.Item);
        }

        #endregion

        #region DatePeriods
        
        void InitDatePeriods()
        {
            // FOLLOWING IS TEMPORARY MEASURE FOR TESTING PURPOSES //
            string strConnectionString = "";
            strConnectionString = set.WoodPlanConnectionString;

            sp00022GetDatePeriodsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00022GetDatePeriodsTableAdapter.Fill(this.woodPlanDataSet.sp00022GetDatePeriods, 1, 1, 1);
            beiLivePeriod.EditValue = this.GlobalSettings.LivePeriodID;
            beiViewedPeriod.EditValue = this.GlobalSettings.ViewedPeriodID;
        }

        #endregion
        
        #region GalleryItemsChecked

        GalleryItem fCurrentFontItem, fCurrentColorItem, fCurrentStaffItem;
        GalleryItem GetColorItemByColor(Color color, BaseGallery gallery)
        {
            foreach (GalleryItemGroup group in gallery.Groups)
                foreach (GalleryItem item in group.Items)
                    if (item.Caption == color.Name)
                        return item;
            return null;
        }
        GalleryItem GetFontItemByFont(string fontName, BaseGallery gallery)
        {
            foreach (GalleryItemGroup group in gallery.Groups)
                foreach (GalleryItem item in group.Items)
                    if (item.Caption == fontName)
                        return item;
            return null;
        }
        GalleryItem GetStaffItemByStaffName(string staffName, BaseGallery gallery)
        {
            foreach (GalleryItemGroup group in gallery.Groups)
                foreach (GalleryItem item in group.Items)
                    if (item.Caption == staffName)
                        return item;
            return null;
        }
        GalleryItem CurrentFontItem
        {
            get { return fCurrentFontItem; }
            set
            {
                if (fCurrentFontItem == value) return;
                if (fCurrentFontItem != null) fCurrentFontItem.Checked = false;
                fCurrentFontItem = value;
                if (fCurrentFontItem != null)
                {
                    fCurrentFontItem.Checked = true;
                    MakeFontVisible(fCurrentFontItem);
                }
            }
        }
        GalleryItem CurrentColorItem
        {
            get { return fCurrentColorItem; }
            set
            {
                if (fCurrentColorItem == value) return;
                if (fCurrentColorItem != null) fCurrentColorItem.Checked = false;
                fCurrentColorItem = value;
                if (fCurrentColorItem != null)
                {
                    fCurrentColorItem.Checked = true;
                    MakeColorVisible(fCurrentColorItem);
                }
            }
        }
        GalleryItem CurrentStaffItem
        {
            get { return fCurrentStaffItem; }
            set
            {
                if (fCurrentStaffItem == value) return;
                if (fCurrentStaffItem != null) fCurrentStaffItem.Checked = false;
                fCurrentStaffItem = value;
                if (fCurrentStaffItem != null)
                {
                    fCurrentStaffItem.Checked = true;
                    MakeStaffVisible(fCurrentStaffItem);
                }
            }
        }

        void MakeFontVisible(GalleryItem item)
        {
            gddFont.Gallery.MakeVisible(fCurrentFontItem);
            //rgbiFont.Gallery.MakeVisible(fCurrentFontItem);
        }
        void MakeColorVisible(GalleryItem item)
        {
            gddFontColour.Gallery.MakeVisible(fCurrentColorItem);
            //rgbiFontColour.Gallery.MakeVisible(fCurrentColorItem);
        }
        void MakeStaffVisible(GalleryItem item)
        {
            gddStaff.Gallery.MakeVisible(fCurrentStaffItem);
         }

        private void gddFont_Popup(object sender, System.EventArgs e)
        {
            MakeFontVisible(CurrentFontItem);
        }

        private void gddFontColor_Popup(object sender, System.EventArgs e)
        {
            MakeColorVisible(CurrentColorItem);
        }

        private void gddStaff_Popup(object sender, System.EventArgs e)
        {
            MakeStaffVisible(CurrentStaffItem);
        }

        private void gddFont_Gallery_ItemClick(object sender, DevExpress.XtraBars.Ribbon.GalleryItemClickEventArgs e)
        {
            //SetFont(e.Item.Caption, e.Item);
        }


        #endregion

        private void frmMain2_Load(object sender, EventArgs e)
        {
            ribbon.Minimized = true;

            sbiAdd.Enabled = false;
            sbiEdit.Enabled = false;
            iDelete.Enabled = false;
            iSave.Enabled = false;

            iCut.Enabled = false;
            iCopy.Enabled = false;
            iPaste.Enabled = false;
            iUndo.Enabled = false;
            iRedo.Enabled = false;

            iPreview.Enabled = false;
            iPrint.Enabled = false;

            panelContainer1.Height = this.Height - 275;
            InitializeModules();

            if (blLoad == true)
            {
                // Scan through the directory and get our available modules...
                this.GlobalSettings = new GlobalSettings();
                this.GlobalSettings.ConnectionString = strConnectionString;

                // Check System_Setting to see if ApplicationIntentReadOnly is switched on //
                string strApplicationIntentReadOnly = "Off";
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    strApplicationIntentReadOnly = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "ApplicationIntentReadOnly").ToString();
                }
                catch (Exception) { }
                this.GlobalSettings.ConnectionStringREADONLY = strConnectionString + (strApplicationIntentReadOnly.ToLower() == "on" ? " ApplicationIntent=ReadOnly;" : "");

                /*if (strUserName != Environment.UserName)
                {
                    iCurrentUser.Caption = Environment.UserName + " as " + strUserName;
                }
                else
                {
                    iCurrentUser.Caption = Environment.UserName;
                }*/
                iCurrentUser.Caption = strUserName;


                sp00019GetUserDetailsForStartUpTableAdapter.Connection.ConnectionString = strConnectionString;
                sp00019GetUserDetailsForStartUpTableAdapter.Fill(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp, strUserName);

                try
                {
                    intStaffID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
                }
                catch (Exception)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Account unavailable - your username [" + strUserName + "] does not have an entry in the Staff Table. Please contact your System Administrator.", "Information Centre Login - User Account Unavailable", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    blAbortLoad = true;
                    this.Close();
                }

                if (strUserType == "")
                {
                    GlobalSettings.UserID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
                    GlobalSettings.UserType1 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType1"].ToString();
                }
                else
                {
                    GlobalSettings.UserID = intUserID;
                    GlobalSettings.UserType1 = strUserType;
                }

                GlobalSettings.UserSurname = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserSurname"].ToString();
                GlobalSettings.UserForename = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserForename"].ToString();
                GlobalSettings.Username = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUsername"].ToString().ToLower();
                GlobalSettings.ToDoList = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intToDoList"]);
                GlobalSettings.CommentBank = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intCommentBank"]);
                GlobalSettings.ShowTipsOnStart = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowTipsOnStart"]);
                GlobalSettings.ShowFormOnStart = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intDefaultForm"]);
                GlobalSettings.UserScreenConfig = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserConfig"]);
                GlobalSettings.ToolTips = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowToolTips"]);
                GlobalSettings.ContactGroups = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intContactGroups"]);
                GlobalSettings.ShowConfirmations = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowConfirmations"]);
                GlobalSettings.UserType2 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType2"].ToString();
                GlobalSettings.UserType3 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType3"].ToString();
                GlobalSettings.LiveStartDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtLivePeriodStart"]);
                GlobalSettings.LiveEndDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtLivePeriodEnd"]);
                GlobalSettings.ViewedStartDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtViewedPeriodStart"]);
                GlobalSettings.ViewedEndDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtViewedPeriodEnd"]);
                GlobalSettings.PersonType = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intPersonType"]);
                GlobalSettings.LivePeriodID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intLivePeriodID"]);
                GlobalSettings.ViewedPeriodID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intViewedPeriodID"]);
                GlobalSettings.LiveDBName = Convert.ToString(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strLiveDBName"]);
                GlobalSettings.SystemDataTransferMode = Convert.ToString(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["SystemDataTransferMode"]);

                int intFirstScreenToLoad = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["FirstScreenLoadedID"]);
                int intShowEstatePlanInspectionControlPanel = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["EstatePlanInspectionControlPanel"]);

                lcNonLiveDB.Visible = (GlobalSettings.LiveDBName.ToLower() != set.DatabaseName.ToLower() ? true : false);
                lcDataTransferMode.Visible = (GlobalSettings.SystemDataTransferMode.ToLower() != "desktop" ? true : false);

                GlobalSettings.DataEntryOff = (GlobalSettings.LivePeriodID == GlobalSettings.ViewedPeriodID ? false : true);

                if (GlobalSettings.UserType1.ToUpper() != "SUPER")
                {
                    bbiChangeCurrentUser.Enabled = false;
                    bbiChangeCurrentUserSuper.Enabled = false;
                    bbiShowLoggedInUsers.Enabled = false;
                }
                else
                {
                    bbiChangeCurrentUser.Enabled = true;
                    bbiChangeCurrentUserSuper.Enabled = true;
                    bbiShowLoggedInUsers.Enabled = true;
                }

                GlobalSettings.StudentPhotoPath = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strStudentPhotoPath"].ToString();
                GlobalSettings.StaffPhotoPath = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strStaffPhotoPath"].ToString();
                GlobalSettings.DefaultPhoto = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strDefaultPhoto"].ToString();
                GlobalSettings.MinimiseToTaskBar = Convert.ToBoolean(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["MinimiseToTaskBar"]);
                GlobalSettings.ShowPhotoPopup = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["ShowPhotoPopup"]);
                
                GlobalSettings.HRSecurityKey = "";
                // Check if the Security Key if valid [for viewing employee pay data] //
                string strSecurityKey = "";
                string strCompanyName = "";
                bool boolKeyValid = true;
                try
                {
                    strSecurityKey = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["HRSecurityKey"].ToString();
                    strCompanyName = GlobalSettings.Username;
                    if (string.IsNullOrEmpty(strSecurityKey) || string.IsNullOrEmpty(strCompanyName))
                    {
                        boolKeyValid = false;
                    }
                    else
                    {
                        // Decrypt key to see if it is valid //
                        SimplerAES Encryptor = new SimplerAES();
                        string strDecryptedCompanyName = Encryptor.Decrypt(strSecurityKey);
                        if (strCompanyName.ToLower().Trim() != strDecryptedCompanyName.ToLower().Trim())
                        {
                            boolKeyValid = false;  // Licence Key is invalid or expired //
                        }
                        else GlobalSettings.HRSecurityKey = strSecurityKey;
                    }
                }
                catch (Exception Ex)
                {
                    boolKeyValid = false;  // Error occurred so key is treated as invalid //
                }


                iDatabase.Caption = set.DatabaseName + " on " + set.ServerName;

                this.sp00020GetUserApplicationSettingsTableAdapter.Connection.ConnectionString = strConnectionString;
                this.sp00020GetUserApplicationSettingsTableAdapter.Fill(this.woodPlanDataSet.sp00020GetUserApplicationSettings, intStaffID);

                if (this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows.Count > 0)
                {
                    switch (Convert.ToInt32(this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ActiveLookAndFeel"]))
                    {
                        case 0:
                            defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
                            break;
                        case 1:
                            defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
                            break;
                        case 2:
                            defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
                            break;
                        case 3:
                            defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
                            break;
                        case 4:
                            defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                            break;
                    }

                    // Valentines day //
 /*                   if (DateTime.Today.Day == 14 && DateTime.Today.Month == 2)
                    {
                        this.defaultLookAndFeel1.LookAndFeel.SkinName = "Valentine";
                    }
                    else if (DateTime.Today.Day == 1 && DateTime.Today.Month == 4)
                    {
                        // Happy Apple Day!
                        this.defaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
                    }
                    else if (DateTime.Today.Month == 12 && DateTime.Today.Day >= 13)
                    {
                        // Christmas tree, oh Christmas tree, how lovely are your branches...
                        this.defaultLookAndFeel1.LookAndFeel.SkinName = "Xmas 2008 Blue";
                    }
                    else if (DateTime.Today.Month == 10 && DateTime.Today.Day >= 28)
                    {
                        // They did the mash, they did the monster mash...
                        this.defaultLookAndFeel1.LookAndFeel.SkinName = "Halloween";
                    }
                    else if ((DateTime.Today.Month >= 7 && DateTime.Today.Day >= 4) && (DateTime.Today.Month <= 8 && DateTime.Today.Day <= 4))
                    {
                        this.defaultLookAndFeel1.LookAndFeel.SkinName = "SummerTime";
                    }
                    else
                    {*/
                        // Boring and dull I know, but this is the line you should use all the time! :-)
                        this.defaultLookAndFeel1.LookAndFeel.SkinName = this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ActiveSkinName"].ToString();

                        int intColour1 = Convert.ToInt32(woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["SkinMaskColor1"]);
                        int intColour2 = Convert.ToInt32(woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["SkinMaskColor2"]);
                        if (intColour1 != 0)
                        {
                            Color color1 = Color.FromArgb(intColour1);
                            Color color2 = Color.FromArgb(intColour2);
                            this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor = color1;
                            if (intColour2 != 0) this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor2 = color2;
                        }
                    //}
                    intApplicationConfigID = Convert.ToInt32(this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ApplicationConfigID"]);
                }

                //if (strFriendlyName != "Training Database")
                //{
                //    // bsiTraining.Dispose();
                //}

                InitDatePeriods();

                LoadChartTypes(); // Loads list of chart types into Chart Gallery on RibbonBar //

                SkinContainerCollection scc = SkinManager.Default.Skins;
                tGallery = new Thread(new ParameterizedThreadStart(InitGalleries));
                tGallery.Start(scc);

                //tGallery = new Thread(new ThreadStart(InitStaffGallery));
                //tGallery.Start();
                //tFont = new Thread(new ThreadStart(InitFontGallery));
                //tFont.Start();
                /*tSkins = new Thread(new ThreadStart(InitSkinGallery));
                tSkins.Start();*/

                InitSkinGallery();
                defaultLookAndFeel1.LookAndFeel.StyleChanged += LookAndFeel_StyleChanged;  // Hook in event (can't be done in designer for this object) to trap when style changes so we can update any open grids semi-transparent highlighter bar //

                //InitColorGallery();

                sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
                sp00039GetFormPermissionsForUserTableAdapter.Fill(this.woodPlanDataSet.sp00039GetFormPermissionsForUser, 0, intStaffID, 0,
                                                                  this.GlobalSettings.ViewedPeriodID);

                ProcessPermissionsForForm();  // Load Permissions for menu items (Filters and Layouts) //

                dpActiveForm.Height = 300;

                sp00001PopulateSwitchboardTableAdapter.Connection.ConnectionString = strConnectionString;

                // Following Commented out by Mark 25/02/2009 //
                //StartNotificationTimer();
                //RestartUpdateTimer();

                // Ensure the folder holding the dictionary for the spell checker is present - if not, create it //
                if (!System.IO.File.Exists(Environment.CurrentDirectory + "\\CustomEnglish.dic"))
                {
                    System.IO.File.Create(Environment.CurrentDirectory + "\\CustomEnglish.dic");
                }

                iMyFilters.Enabled = false;
                iAvailableFilters.Enabled = false;
                iLoadFilter.Enabled = false;
                iFilterCreate.Enabled = false;
                iSaveFilter.Enabled = false;
                iSaveFilterAs.Enabled = false;
                iLoadLayout.Enabled = false;
                iSaveLayout.Enabled = false;
                iSaveLayoutAs.Enabled = false;
                blFirstTime = false;
                bbiMagnifier.Visibility = BarItemVisibility.Always;

                sbiToDoList.Enabled = false;
                iToDoList.Enabled = false;

                iFastFind.Enabled = false;
                bbiCalculator.Enabled = false;
                iClipboard.Enabled = false;
                sbiCommentBank.Enabled = false;
                iCommentBank.Enabled = false;

                rgbiChartTypes.Enabled = false;
                bbiChartPalette.Enabled = false;
                rgbiChartAppearance.Enabled = false;
                bbiRotateAxis.Enabled = false;
                bbiChartWizard.Enabled = false;
                bbiLayoutFlip.Enabled = false;
                bbiLayoutRotate.Enabled = false;
                
                bbiColourMixer.Enabled = true;

                OnShowPhotoPopupChanged(this, GlobalSettings.ShowPhotoPopup);
                pePersonsPhoto.Size = new Size(20, 20);
                pePersonsPhoto.Visible = false;

                // Control Visibilty of Asset Management Site Inspection Control Panel //
                dockPanelSiteInspection.Visibility = (intShowEstatePlanInspectionControlPanel == 1 ? DevExpress.XtraBars.Docking.DockVisibility.Visible : DevExpress.XtraBars.Docking.DockVisibility.Hidden);

                ribbon.SelectedPage = ribbonPage1;  // Set to Core Page Tab //

                // Open first form if user has a preference set //
                if (intFirstScreenToLoad != 0) Open_Screen(intFirstScreenToLoad);

                // Check Version //
                int intCurrentDBVersion = 0;
                try
                {
                    DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                    GetSetting.ChangeConnectionString(strConnectionString);
                    intCurrentDBVersion = Convert.ToInt32(GetSetting.sp00043_RetrieveSingleSystemSetting(0, "DatabaseVersion").ToString().Replace(".", "").PadRight(5, '0'));  // strip out dots //

                    System.Reflection.Assembly oAssembly = System.Reflection.Assembly.GetExecutingAssembly();
                    Version ver = oAssembly.GetName().Version;
                    int intApplicationVersion = Convert.ToInt32(ver.ToString().Replace(".", "").PadRight(5, '0'));  // strip out dots //
                    if (intApplicationVersion < intCurrentDBVersion)
                    {
                        string strMessage = "<color=red>WARNING!\n\nYou are not using the latest version of the application.\n\nIt is still possible to use this version, however it may be unstable.\n\nPlease upgrade your version as soon as possible or contact the ICT Department to assist.</color>";
                        XtraMessageBox.Show(strMessage, "Outdated Version", MessageBoxButtons.OK, MessageBoxIcon.Warning, DefaultBoolean.True);
                    }
                }
                catch (Exception)
                {
                }
            }
            else
            {
                blAbortLoad = true;
                this.Close();
            }
        }

        #region AnalysisChartsStyling

        ViewType viewType = ViewType.Line;
        public Chart currentChart = null;
        public ChartControl currentChartControl = null;
        public ViewType ViewType
        {
            get { return viewType; }
            set
            {
                if (ViewType == value) return;
                viewType = value;
                InitChartAppearanceGallery(rgbiChartAppearance, currentChart.PaletteName);
            }
        }
        public bool AllowDataSeriesPagesOnWizard = false;
        public PivotGridControl currentPivotGridControl = null;

        public void Set_Chart(ChartControl chartControl, PivotGridControl pivotGridControl)
        {
            // Hooks up the passed in chart to the RibbonBars Chart Styling Gallery //
            currentChartControl = chartControl;
            Chart chart = ((IChartContainer)chartControl).Chart;
            currentChart = chart;
            currentPivotGridControl = pivotGridControl;

            rgbiChartTypes.Enabled = true;
            bbiChartPalette.Enabled = true;
            rgbiChartAppearance.Enabled = true;
            bbiRotateAxis.Enabled = true;
            bbiChartWizard.Enabled = true;
            bbiLayoutFlip.Enabled = true;
            bbiLayoutRotate.Enabled = true;
        }

        public void Kill_Chart_Styling()
        {
            // Removes current chart from RibbonBars Chart Styling Gallery in [effect clearing the chart styling gallery] //
            currentChart = null;
            currentChartControl = null;
            rgbiChartAppearance.Gallery.DestroyItems();
            rgbiChartAppearance.Gallery.Groups.Clear();
            gddChartPalette.Gallery.DestroyItems();
            currentPivotGridControl = null;
        }

        private void gddChartPalette_GalleryItemClick(object sender, GalleryItemClickEventArgs e)
        {
            currentChart.PaletteName = e.Item.Caption;
            UpdateStyle(true);
        }


        private void rgbiChartAppearance_GalleryItemClick(object sender, GalleryItemClickEventArgs e)
        {
            if (currentChartControl == null) return;
            UpdateStyle();
        }

        private void rgbiChartAppearance_GalleryInitDropDownGallery(object sender, InplaceGalleryEventArgs e)
        {
            e.PopupGallery.ItemCheckMode = DevExpress.XtraBars.Ribbon.Gallery.ItemCheckMode.SingleRadio;
            e.PopupGallery.ColumnCount = 7;
            e.PopupGallery.DrawImageBackground = true;
            e.PopupGallery.ShowGroupCaption = true;
            e.PopupGallery.SynchWithInRibbonGallery = true;
            e.PopupGallery.AutoSize = GallerySizeMode.Vertical;
        }

        private void rgbiChartAppearance_GalleryPopupClose(object sender, InplaceGalleryEventArgs e)
        {
            List<GalleryItem> items = e.Item.Gallery.GetCheckedItems();
            if (items.Count > 0)
            {
                e.Item.Gallery.MakeVisible(items[0]);
                if (currentChartControl == null) return;
                UpdateStyle();
            }
        }


        public void InitChartAppearanceGallery(RibbonGalleryBarItem galleryBarItem, string paletteName)
        {
            GalleryItem checkedItem = null;
            galleryBarItem.Gallery.DestroyItems();
            galleryBarItem.Gallery.Groups.Clear();
            Palette palette = currentChart.PaletteRepository[paletteName];
            galleryBarItem.Gallery.BeginUpdate();
            foreach (ChartAppearance appearance in currentChart.AppearanceRepository)
            {
                if (currentChart.Palette.Predefined && appearance != currentChart.Appearance && !String.IsNullOrEmpty(appearance.PaletteName) && appearance.PaletteName != currentChart.Palette.Name) continue;
                GalleryItemGroup group = new GalleryItemGroup();
                group.Caption = appearance.Name;
                galleryBarItem.Gallery.Groups.Add(group);
                for (int i = 0; i <= palette.Count; i++)
                {
                    GalleryItem gAppearanceItem = new GalleryItem();
                    gAppearanceItem.Caption = appearance.Name;
                    gAppearanceItem.Hint = GetStyleName(appearance, i);
                    gAppearanceItem.Tag = i;
                    gAppearanceItem.Image = AppearanceImageHelper.CreateImage(ViewType, appearance, palette, i);
                    group.Items.Add(gAppearanceItem);
                    if (currentChart.AppearanceName == appearance.Name && currentChart.PaletteBaseColorNumber == i)
                    {
                        checkedItem = gAppearanceItem;
                    }
                }
            }
            galleryBarItem.Gallery.EndUpdate();
            galleryBarItem.Tag = palette.Count;
            galleryBarItem.Gallery.SetItemCheck(checkedItem, true, true);
            RibbonControl ribbon = ((RibbonBarManager)galleryBarItem.Manager).Ribbon;
            ribbon.UpdateViewInfo();
            checkedItem.MakeVisible();
        }

        static string GetStyleName(ChartAppearance appearance, int index)
        {
            if (index == 0) return string.Format("{0} (All colors)", appearance.Name);
            return string.Format("{0} (Color {1})", appearance.Name, index);
        }

        public void InitChartPaletteGallery(GalleryDropDown gallery)
        {
            gallery.Gallery.DestroyItems();
            string[] paletteNames = currentChartControl.GetPaletteNames();
            gallery.Gallery.BeginUpdate();
            for (int i = 0; i < paletteNames.Length; i++)
            {
                GalleryItem gPaletteItem = new GalleryItem();
                gPaletteItem.Caption = paletteNames[i];
                gPaletteItem.Hint = paletteNames[i];
                gPaletteItem.Image = CreateEditorImage(currentChartControl.PaletteRepository[paletteNames[i]]);
                gallery.Gallery.Groups[0].Items.Add(gPaletteItem);
            }
            gallery.Gallery.EndUpdate();
            UpdateCurrentPalette(gallery);
        }

        public void UpdateCurrentPalette(GalleryDropDown gallery)
        {
            List<GalleryItem> innerList = gallery.Gallery.GetAllItems();
            foreach (GalleryItem item in innerList)
            {
                if (item.Caption == currentChart.PaletteName)
                {
                    gallery.Gallery.SetItemCheck(item, true, true);
                    bbiChartPalette.Hint = currentChart.PaletteName;
                    string.Format("Specify a colour palette ({0})", currentChart.PaletteName); 
                    return;
                }
            }
        }

        public static Image CreateEditorImage(Palette palette)
        {
            const int imageSize = 10;
            Bitmap image = null;
            try
            {
                image = new Bitmap(palette.Count * (imageSize + 1) - 1, imageSize);
                using (Graphics g = Graphics.FromImage(image))
                {
                    Rectangle rect = new Rectangle(Point.Empty, new Size(imageSize, imageSize));
                    for (int i = 0; i < palette.Count; i++, rect.X += 11)
                    {
                        using (Brush brush = new SolidBrush(palette[i].Color))
                            g.FillRectangle(brush, rect);
                        Rectangle penRect = rect;
                        penRect.Width--;
                        penRect.Height--;
                        using (Pen pen = new Pen(Color.Gray))
                            g.DrawRectangle(pen, penRect);
                    }
                }
            }
            catch
            {
                if (image != null)
                {
                    image.Dispose();
                    image = null;
                }
            }
            return image;
        }

        static int GetPaletteColor(Palette palette, int value)
        {
            if (value < 0) return 0;
            if (value > palette.Count) return palette.Count;
            return value;
        }

        void UpdateStyle()
        {
            UpdateStyle(false);
        }

        void UpdateStyle(bool updateAppearance)
        {
            string currentPalette = currentChart.PaletteName;
            currentChart.AppearanceName = CurrentAppearanceName;
            currentChart.PaletteBaseColorNumber = GetPaletteColor(currentChart.Palette, CurrentPaletteColor);
            if (currentPalette != currentChart.PaletteName || updateAppearance)
            {
                InitChartAppearanceGallery(rgbiChartAppearance, currentChart.PaletteName);
            }
            UpdateCurrentPalette();
            RaiseStyleChanged();
        }

        public void UpdateCurrentPalette()
        {
            List<GalleryItem> innerList = gddChartPalette.Gallery.GetAllItems();
            foreach (GalleryItem item in innerList)
            {
                if (item.Caption == currentChart.PaletteName)
                {
                    gddChartPalette.Gallery.SetItemCheck(item, true, true);
                    bbiChartPalette.Hint = string.Format("Specify a colour palette ({0})", currentChart.PaletteName);
                    return;
                }
            }
        }

        void RaiseStyleChanged()
        {
            //if (StyleChanged != null) StyleChanged(this, new ChartAppearanceEventArgs(PaletteName, AppearanceName, ColorIndex));
        }
        
        public string PaletteName
        {
            get { return currentChart.PaletteName; }
            set { currentChart.PaletteName = value; }
        }
        public string AppearanceName
        {
            get { return currentChart.AppearanceName; }
            set { currentChart.AppearanceName = value; }
        }
        public int ColorIndex
        {
            get { return currentChart.PaletteBaseColorNumber; }
            set { currentChart.PaletteBaseColorNumber = value; }
        }
        
        #region CurrentStyle
        string CurrentAppearanceName
        {
            get
            {
                List<GalleryItem> innerCheckedList = rgbiChartAppearance.Gallery.GetCheckedItems();
                return innerCheckedList[0].Caption;
            }
        }
        int CurrentPaletteColor
        {
            get
            {
                List<GalleryItem> innerCheckedList = rgbiChartAppearance.Gallery.GetCheckedItems();
                return (int)innerCheckedList[0].Tag;
            }
        }
        #endregion

        public void LoadChartTypes()
        {
            //rgbiChartTypes
            rgbiChartTypes.Gallery.DestroyItems();
            rgbiChartTypes.Gallery.BeginUpdate();
            Assembly ass = Assembly.GetAssembly(typeof(SeriesViewFactory));
            
            GalleryItemGroup group = new GalleryItemGroup();
            group.Caption = "Chart Types";
            rgbiChartTypes.Gallery.Groups.Add(group);
           
            foreach (ViewType viewType in SeriesViewFactory.ViewTypes)
            {
                GalleryItem gPaletteItem = new GalleryItem();
                gPaletteItem.Caption = viewType.ToString();
                gPaletteItem.Hint = viewType.ToString();
                //gPaletteItem.Image = ResourceImageHelper.CreateBitmapFromResources("DevExpress.XtraCharts.Design.Wizard.Images." + viewType.ToString().ToLower() + ".png", ass);
                imageCollectionChartTypes.AddImage(ResourceImageHelper.CreateBitmapFromResources("DevExpress.XtraCharts.Design.Wizard.Images." + viewType.ToString().ToLower() + ".png", ass));
                gPaletteItem.Image = imageCollectionChartTypes.Images[imageCollectionChartTypes.Images.Count - 1];
                                              
                rgbiChartTypes.Gallery.Groups[0].Items.Add(gPaletteItem);
            }
            rgbiChartTypes.Gallery.EndUpdate();
        }

        private void rgbiChartTypes_GalleryInitDropDownGallery(object sender, InplaceGalleryEventArgs e)
        {
            e.PopupGallery.ItemCheckMode = DevExpress.XtraBars.Ribbon.Gallery.ItemCheckMode.SingleRadio;
            e.PopupGallery.ColumnCount = 7;
            e.PopupGallery.DrawImageBackground = true;
            e.PopupGallery.ShowGroupCaption = true;
            e.PopupGallery.SynchWithInRibbonGallery = true;
            e.PopupGallery.AutoSize = GallerySizeMode.Vertical;
        }

        private void rgbiChartTypes_GalleryItemClick(object sender, GalleryItemClickEventArgs e)
        {
            if (currentChartControl == null) return;
            string strViewType = e.Item.Caption;

            ViewType vType = ChartGetViewType(strViewType);
            try
            {
                currentChartControl.SeriesTemplate.ChangeView(vType);
                if (currentChartControl.Diagram is Diagram3D)
                {
                    Diagram3D diagram = (Diagram3D)currentChartControl.Diagram;
                    diagram.RuntimeRotation = true;
                    diagram.RuntimeZooming = true;
                    diagram.RuntimeScrolling = true;
                }
                ViewType = vType;  // Update Main Forms Ribbon (Chart Styling) //
            }
            catch {}
        }

        private void rgbiChartTypes_GalleryPopupClose(object sender, InplaceGalleryEventArgs e)
        {
            List<GalleryItem> items = e.Item.Gallery.GetCheckedItems();
            if (items.Count > 0)
            {
                e.Item.Gallery.MakeVisible(items[0]);
                if (currentChartControl == null) return;
            }
        }

        private ViewType ChartGetViewType(string strViewType)
        {
            ViewType viewType;
            //strViewType = strViewType.Replace(" ", "");
            switch (strViewType)
            {
                case "Bar":
                    viewType = ViewType.Bar;
                    break;
                case "StackedBar":
                    viewType = ViewType.StackedBar;
                    break;
                case "FullStackedBar":
                    viewType = ViewType.FullStackedBar;
                    break;
                case "SideBySideStackedBar":
                    viewType = ViewType.SideBySideStackedBar;
                    break;
                case "SideBySideFullStackedBar":
                    viewType = ViewType.SideBySideFullStackedBar;
                    break;
                case "Pie":
                    viewType = ViewType.Pie;
                    break;
                case "Doughnut":
                    viewType = ViewType.Doughnut;
                    break;
                case "Funnel":
                    viewType = ViewType.Funnel;
                    break;
                case "Bubble":
                    viewType = ViewType.Bubble;
                    break;
                case "Line":
                    viewType = ViewType.Line;
                    break;
                case "StepLine":
                    viewType = ViewType.StepLine;
                    break;
                case "Spline":
                    viewType = ViewType.Spline;
                    break;
                case "ScatterLine":
                    viewType = ViewType.ScatterLine;
                    break;
                case "SwiftPlot":
                    viewType = ViewType.SwiftPlot;
                    break;
                case "Area":
                    viewType = ViewType.Area;
                    break;
                case "SplineArea":
                    viewType = ViewType.SplineArea;
                    break;
                case "StackedArea":
                    viewType = ViewType.StackedArea;
                    break;
                case "StackedSplineArea":
                    viewType = ViewType.StackedSplineArea;
                    break;
                case "FullStackedArea":
                    viewType = ViewType.FullStackedArea;
                    break;
                case "FullStackedSplineArea":
                    viewType = ViewType.FullStackedSplineArea;
                    break;
                case "Stock":
                    viewType = ViewType.Stock;
                    break;
                case "CandleStick":
                    viewType = ViewType.CandleStick;
                    break;
                case "SideBySideRangeBar":
                    viewType = ViewType.SideBySideRangeBar;
                    break;
                case "RangeBar":
                    viewType = ViewType.RangeBar;
                    break;
                case "SideBySideGantt":
                    viewType = ViewType.SideBySideGantt;
                    break;
                case "Gantt":
                    viewType = ViewType.Gantt;
                    break;
                case "PolarPoint":
                    viewType = ViewType.PolarPoint;
                    break;
                case "PolarLine":
                    viewType = ViewType.PolarLine;
                    break;
                case "PolarArea":
                    viewType = ViewType.PolarArea;
                    break;
                case "RadarPoint":
                    viewType = ViewType.RadarPoint;
                    break;
                case "RadarLine":
                    viewType = ViewType.RadarLine;
                    break;
                case "RadarArea":
                    viewType = ViewType.RadarArea;
                    break;
                case "Bar3D":
                    viewType = ViewType.Bar3D;
                    break;
                case "StackedBar3D":
                    viewType = ViewType.StackedBar3D;
                    break;
                case "FullStackedBar3D":
                    viewType = ViewType.FullStackedBar3D;
                    break;
                case "ManhattanBar":
                    viewType = ViewType.ManhattanBar;
                    break;
                case "SideBySideStackedBar3D":
                    viewType = ViewType.SideBySideStackedBar3D;
                    break;
                case "SideBySideFullStackedBar3D":
                    viewType = ViewType.SideBySideFullStackedBar3D;
                    break;
                case "Pie3D":
                    viewType = ViewType.Pie3D;
                    break;
                case "Doughnut3D":
                    viewType = ViewType.Doughnut3D;
                    break;
                case "Funnel3D":
                    viewType = ViewType.Funnel3D;
                    break;
                case "Line3D":
                    viewType = ViewType.Line3D;
                    break;
                case "StepLine3D":
                    viewType = ViewType.StepLine3D;
                    break;
                case "Area3D":
                    viewType = ViewType.Area3D;
                    break;
                case "StackedArea3D":
                    viewType = ViewType.StackedArea3D;
                    break;
                case "FullStackedArea3D":
                    viewType = ViewType.FullStackedArea3D;
                    break;
                case "Spline3D":
                    viewType = ViewType.Spline3D;
                    break;
                case "SplineArea3D":
                    viewType = ViewType.SplineArea3D;
                    break;
                case "StackedSplineArea3D":
                    viewType = ViewType.StackedSplineArea3D;
                    break;
                case "FullStackedSplineArea3D":
                    viewType = ViewType.FullStackedSplineArea3D;
                    break;
                default:
                    viewType = ViewType.Line;
                    break;
            }
            return viewType;
        }

        private void bbiChartWizard_ItemClick(object sender, ItemClickEventArgs e)
        {
            RunChartWizard();
        }

        public void RunChartWizard()
        {
            if (currentChartControl == null) return;
            // Create a new Wizard.
            ChartWizard wizard = new ChartWizard(currentChartControl);

            // Obtain a Data page.
            WizardDataPage page = wizard.DataPage;

            // Hide datasource-related tabs on the Data page.
            if (!AllowDataSeriesPagesOnWizard)
            {
                page.HiddenPageTabs.Add(DataPageTab.AutoCreatedSeries);
                page.HiddenPageTabs.Add(DataPageTab.SeriesBinding);
            }

            // Invoke the Wizard window.
            wizard.ShowDialog();
            ChartPreserveUserInteraction(currentChartControl);  // Ensure end-user rotation still working if user converted from 2D to 3D chart //

            ViewType = DevExpress.XtraCharts.Native.SeriesViewFactory.GetViewType(currentChartControl.Series[0].View);  // Update Main Forms Ribbon (Chart Styling) //
        }

        private void ChartPreserveUserInteraction(ChartControl chart)
        {
            // Ensure end-user rotation working if 3D chart //
            try
            {
                SimpleDiagram3D diagram = (SimpleDiagram3D)currentChart.Diagram;
                if (diagram != null)
                {
                    diagram.RuntimeRotation = true;
                    diagram.RuntimeScrolling = true;
                    diagram.RuntimeZooming = true;
                }
            }
            catch
            {
            }
        }

        private void bbiRotateAxis_ItemClick(object sender, ItemClickEventArgs e)
        {
            RotateChartAxis();
        }

        public void RotateChartAxis()
        {
            try
            {
                if (currentPivotGridControl == null) return;
                currentPivotGridControl.OptionsChartDataSource.ProvideDataByColumns = !currentPivotGridControl.OptionsChartDataSource.ProvideDataByColumns;
            }
            catch { }
        }

        private void bbiLayoutFlip_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (currentChartControl == null || currentPivotGridControl == null) return;
            
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            ctrlBase cbBase;
            DevExpress.XtraBars.Docking.DockPanel dpActive;
            if (objTest.GetType().Name == "DockPanel")
            {
                dpActive = dockManager1.ActivePanel;
                if (dpActive != null && dpActive.Name == "dpToDoList")
                {
                    foreach (Control ctrl in dpActive.Controls[0].Controls)
                    {
                        cbBase = (ctrlBase)ctrl;
                        MethodInfo method = typeof(ctrlBase).GetMethod("OnLayoutFlipEvent");
                        if (method != null)
                        {
                            method.Invoke(cbBase, new object[] { this, ea });
                        }
                        break;
                    }
                }
            }
            else
            {
                fbBase = (frmBase)this.ActiveMdiChild;
                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnLayoutFlipEvent");
                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea });
                    }
                }
            }
        }

        private void bbiLayoutRotate_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (currentChartControl == null || currentPivotGridControl == null) return;

            EventArgs ea = new EventArgs();
            frmBase fbBase;
            ctrlBase cbBase;
            DevExpress.XtraBars.Docking.DockPanel dpActive;
            if (objTest.GetType().Name == "DockPanel")
            {
                dpActive = dockManager1.ActivePanel;
                if (dpActive != null && dpActive.Name == "dpToDoList")
                {
                    foreach (Control ctrl in dpActive.Controls[0].Controls)
                    {
                        cbBase = (ctrlBase)ctrl;
                        MethodInfo method = typeof(ctrlBase).GetMethod("OnLayoutRotateEvent");
                        if (method != null)
                        {
                            method.Invoke(cbBase, new object[] { this, ea });
                        }
                        break;
                    }
                }
            }
            else
            {
                fbBase = (frmBase)this.ActiveMdiChild;
                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnLayoutRotateEvent");
                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea });
                    }
                }
            }
        }

        #endregion


        private void InitGalleries(object scc)
        {
            iFont.Enabled = false;
            iFontColour.Enabled = false;
//            iShareLayout.Enabled = false;

            InitColorGallery();
            InitFontGallery();
//            InitStaffGallery();
        }
        
        private void ProcessPermissionsForForm()
        {
            sbiToDoList.Enabled = false;
            iToDoList.Enabled = false;
            iLinkRecordToDoList.Enabled = false;
         
            iFastFind.Enabled = false;
            bbiCalculator.Enabled = false;
            iClipboard.Enabled = false;

            sbiCommentBank.Enabled = false;
            bbiEditComments.Enabled = false;
            iCommentBankCodes.Enabled = false;
            iCommentBank.Enabled = false;

            for (int i = 0; i < this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                if (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]) == 6)  // Filters //
                {
                    switch (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]))
                    { 
                        case 1:  // Save Filter //
                            if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]))
                            {
                                ib_SaveFilter = true;
                            }
                            break;
                        case 2:  // Load Filter  //
                            if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]))
                            {
                                ib_LoadFilter = true;
                            }
                            if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]))
                            {
                                ib_LoadFilter_Delete = true;
                                this.GlobalSettings.DeleteScreenFilter = true;
                            }
                            break;
                        case 3:  // Save Filter As //
                            if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]))
                            {
                                ib_SaveFilterAs = true;
                            }
                            break;
                        case 4:  // Create Filter //
                            if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]))
                            {
                                ib_CreateFilter = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
                else if (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]) == 8)  // Layouts //
                {
                    switch (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]))
                    { 
                        case 1:  // Load Layout //
                            if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]))
                            {
                                ib_LoadLayout = true;
                            }
                             if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]))
                            {
                                ib_LoadLayout_Delete = true;
                                this.GlobalSettings.DeleteScreenLayout = true;
                            }
                           break;
                        case 2:  // Save Layout  //
                            if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]))
                            {
                                ib_SaveLayout = true;
                            }
                            break;
                        case 3:  // Save Layout As //
                            if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]))
                            {
                                ib_SaveLayoutAs = true;
                            }
                            break;
                        default:
                            break;
                    }
                }

                else if (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]) == 12002)  // ToDoList //
                {
                    if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]))
                    {
                        ib_ToDoList = true;
                        sbiToDoList.Enabled = true;
                        iToDoList.Enabled = true;
                        //iLinkRecordToDoList.Enabled = true;
                    }
                }
                else if (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]) == 12003)  // FastFind //
                {
                    if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]))
                    {
                        ib_FastFind = true;
                        iFastFind.Enabled = true;
                  }
                }
                else if (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]) == 12004)  // Calculator //
                {
                    if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]))
                    {
                        ib_Calculator = true;
                        bbiCalculator.Enabled = true;
                   }
                }
                else if (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]) == 12005)  // Clipboard //
                {
                    if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]))
                    {
                        ib_Clipboard = true;
                        iClipboard.Enabled = true;
                   }
                }
                else if (Convert.ToInt32(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]) == 12006)  // CommentBank //
                {
                    if (Convert.ToBoolean(this.woodPlanDataSet.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]))
                    {
                        ib_CommentBank = true;
                        sbiCommentBank.Enabled = true;
                        bbiEditComments.Enabled = true;
                        iCommentBankCodes.Enabled = true;
                        iCommentBank.Enabled = true;

                    }
                }
            }
        }

        private void StartNotificationTimer()
        {
            //tmrNotification.Interval = (GlobalSettings.ToDoPopupMessageTimer * 60) * 1000;
            //tmrNotification.Start();
        }

        private void nbcReports_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            stcLinkItemIDs sliIDs = (stcLinkItemIDs)e.Link.Item.Tag;
            Open_Screen(sliIDs.intFormID);
        }

        void nbcSwitchboard_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            stcLinkItemIDs sliIDs = (stcLinkItemIDs)e.Link.Item.Tag;
            Open_Screen(sliIDs.intFormID);
        }

        internal void Open_Screen(int intScreenID)
        {
            stcUpdatedGlobals stcGlobals;
            if (intScreenID == 3)
            {
                // Open My Application Settings dialog //
                frmUserSettings fusSettings = new frmUserSettings();
                fusSettings.GlobalSettings = this.GlobalSettings;
                fusSettings.Owner = this;
                fusSettings.ShowDialog();
                stcGlobals = fusSettings.stcGlobals;
                // Now get any changed settngs and apply then for this run of the app //
                this.GlobalSettings.ShowTipsOnStart = Convert.ToInt32(stcGlobals.blShowTipsOnStartup);
                this.GlobalSettings.ToolTips = Convert.ToInt32(stcGlobals.blShowToolTips);
                this.GlobalSettings.ShowConfirmations = Convert.ToInt32(stcGlobals.blShowConfirmations);
                this.GlobalSettings.MinimiseToTaskBar = stcGlobals.blMinimiseToTaskBar;
                this.GlobalSettings.ShowPhotoPopup = stcGlobals.intShowPhotoPopup;
                return;
            }
            else if (intScreenID == 7)
            {
                // Open My Application Settings dialog //
                frmSystemSettings fSystemSettings = new frmSystemSettings();
                fSystemSettings.GlobalSettings = this.GlobalSettings;
                fSystemSettings.Owner = this;
                if (fSystemSettings.ShowDialog() == DialogResult.OK)
                {
                    Boolean boolUpdateChildSheets = false;
                    int intLivePeriod = fSystemSettings.intLivePeriod;
                    Boolean boolDataEntryOff = fSystemSettings.boolDataEntryOff;
                    if (intLivePeriod != GlobalSettings.LivePeriodID)
                    {
                        boolUpdateChildSheets = true;
                        GlobalSettings.LivePeriodID = intLivePeriod;
                        GlobalSettings.LiveStartDate = fSystemSettings.dtLiveStartDate;
                        GlobalSettings.LiveEndDate = fSystemSettings.dtLiveEndDate;
                        beiLivePeriod.EditValue = intLivePeriod;
                        // Check if Live and Viewed periods match - if not disable data entry... //
                        if (GlobalSettings.LivePeriodID != GlobalSettings.ViewedPeriodID)
                        {
                            boolUpdateChildSheets = true;
                            GlobalSettings.DataEntryOff = true;
                        }
                    }
                    if (boolDataEntryOff != GlobalSettings.DataEntryOff)  // May not have been trapped by prior condition, so set here //
                    {
                        boolUpdateChildSheets = true;
                        GlobalSettings.DataEntryOff = boolDataEntryOff;
                    }
                    if (GlobalSettings.SystemDataTransferMode != fSystemSettings.strSystemDataTransferMode)  // Check if we need to update SystemDataTransferMode //
                    {
                        GlobalSettings.SystemDataTransferMode = fSystemSettings.strSystemDataTransferMode;
                        lcDataTransferMode.Visible = (GlobalSettings.SystemDataTransferMode.ToLower() != "desktop" ? true : false);
                        boolUpdateChildSheets = true;
                    }

                    if (boolUpdateChildSheets)
                    {
                        // Update any open sheet's GlobalSettings //
                        EventArgs ea = new EventArgs();
                        frmBase fbBase;
                        fbBase = (frmBase)this.ActiveMdiChild;
                        if (fbBase != null)
                        {
                            MethodInfo method = typeof(frmBase).GetMethod("OnViewedPeriodEvent");
                            if (method != null) method.Invoke(fbBase, new object[] { this, ea, GlobalSettings });
                        }
                    }
                }
                fSystemSettings.Dispose();
                return;
            }
            else if (intScreenID == 2)
            {
                if (this.MdiChildren.Length != 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Please close all forms before editing the system periods.", "Edit System Periods", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    frmSystemPeriods fSystemPeriods = new frmSystemPeriods();
                    fSystemPeriods.GlobalSettings = this.GlobalSettings;
                    fSystemPeriods.Owner = this;
                    if (fSystemPeriods.ShowDialog() == DialogResult.OK)
                    {
                        this.GlobalSettings.ViewedPeriodID = fSystemPeriods.intViewedPeriodID;
                        this.GlobalSettings.ViewedStartDate = fSystemPeriods.dtViewedStartDate;
                        this.GlobalSettings.ViewedEndDate = fSystemPeriods.dtViewedEndDate;
                        beiViewedPeriod.EditValue = this.GlobalSettings.ViewedPeriodID;
                        this.GlobalSettings.LivePeriodID = fSystemPeriods.intLivePeriodID;
                        this.GlobalSettings.LiveStartDate = fSystemPeriods.dtLiveStartDate;
                        this.GlobalSettings.LiveEndDate = fSystemPeriods.dtLiveEndDate;
                        beiLivePeriod.EditValue = this.GlobalSettings.LivePeriodID;

                        // Update any open sheet's GlobalSettings //
                        EventArgs ea = new EventArgs();
                        frmBase fbBase;
                        fbBase = (frmBase)this.ActiveMdiChild;
                        if (fbBase != null)
                        {
                            MethodInfo method = typeof(frmBase).GetMethod("OnViewedPeriodEvent");
                            if (method != null)
                            {
                                method.Invoke(fbBase, new object[] { this, ea, GlobalSettings });
                            }
                        }
                    }
                    fSystemPeriods.Dispose();
                }
                return;
            }
            frmBase frmInstance = null;
            Boolean blChildActivated = false;
            try
            {
                foreach (frmBase frmChild in this.MdiChildren)
                {
                    if (frmChild.FormID == intScreenID && System.Windows.Forms.Control.ModifierKeys != Keys.Control)  // Hold CTRL key down to open multiple instances //
                    {
                        //fmMain.AddViewMenuItems(intWindowID, intStaffID);
                        frmChild.Activate();
                        blChildActivated = true;
                        this.ActivePanelActiveForm(1, 0, false);
                        break;
                    }
                }
                if (blChildActivated == false)
                {
                    switch (intScreenID)
                    {
                        // ------------------------ SCREENS ----------------------------- //
                        #region Core
                        case 1:
                            frmInstance = new frmPermissionsManager();
                            break;
                        case 4:
                            frmInstance = new frm_Core_Picklist_Manager();
                            break;
                        case 11:
                            frmInstance = new frmGroupsManager();
                            break;
                        case 30:
                            frmInstance = new frm_Core_Staff_Manager();
                            break;
                        case 8003:
                            frmInstance = new frm_Core_Accident_Manager();
                            break;
                        case 9227:
                            frmInstance = new frm_Core_Package_Configuration_Manager();
                            break;
                        #endregion

                        #region WoodPlan
                        case 2001:
                            frmInstance = new frm_AT_Tree_Manager();
                            break;
                        //case 2002:
                        //    frmInstance = new frm_AT_Data_Transfer_GBM_Mobile();
                        //    break;
                        case 2003:
                            {
                                if (this.MdiChildren.Length > 0)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("Please close all open screens before starting the Amenity ARB Data Synchronisation Process.", "Amenity ARB Data Synchronisation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                var fChild = new frm_AT_Data_Sync();
                                fChild.GlobalSettings = this.GlobalSettings;
                                fChild.Owner = this;
                                fChild.ShowDialog();
                                return;
                            }
                        case 2004:  // Tree Picker Mapping //
                            {
                                int intWorkSpaceID = 0;
                                int intWorkspaceOwner = 0;
                                string strWorkspaceName = "";
                                Mapping_Functions MapFunctions = new Mapping_Functions();
                                MapFunctions.Get_Mapping_Workspace(this.GlobalSettings, ref intWorkSpaceID, ref intWorkspaceOwner, ref strWorkspaceName);  // Pass by ref so values are updated in called function //
                                if (intWorkSpaceID == 0) return;
                                frmInstance = new frm_AT_Mapping_Tree_Picker(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);
                                break;
                            }
                        case 2005:
                            frm_AT_Mapping_Utilities fMappingUtilities = new frm_AT_Mapping_Utilities();
                            fMappingUtilities.GlobalSettings = this.GlobalSettings;
                            fMappingUtilities.Owner = this;
                            fMappingUtilities.ShowDialog();
                            return;
                        case 2006:
                            frm_AT_Data_Transfer_Tablet fDataTransferTablet = new frm_AT_Data_Transfer_Tablet();
                            fDataTransferTablet.GlobalSettings = this.GlobalSettings;
                            fDataTransferTablet.Owner = this;
                            fDataTransferTablet.ShowDialog();
                            return;
                        case 2008:
                            frmInstance = new frm_AT_Inspection_Manager();
                            break;
                        case 2009:
                            frmInstance = new frm_AT_Action_Manager();
                            break;
                        case 2010:
                            frmInstance = new frm_AT_Monitor_Outstanding_Actions();
                            break;
                        case 2012:
                            frmInstance = new frm_AT_Budget_Manager();
                            break;
                        case 2013:
                            frmInstance = new frm_AT_WorkOrder_Manager();
                            break;
                        case 2014:
                            frmInstance = new frm_AT_Incident_Manager();
                            break;
                        case 2015:
                            frmInstance = new frm_AT_Utilities();
                            break;
                        case 2016:
                            frmInstance = new frm_Mail_Merge_Manager();
                            break;
                        case 2017:
                            frmInstance = new frm_AT_Staff_Client_Access();
                            break;
                        #endregion

                        #region WoodPlan - Assets
                        case 3001:
                            frmInstance = new frm_Core_Client_Manager();
                            break;
                        case 3002:
                            frmInstance = new frm_Core_Site_Manager();
                            break;
                        case 3003:
                            frmInstance = new frm_EP_Asset_Manager();
                            break;
                        case 3004:
                            frmInstance = new frm_EP_Action_Manager();
                            break;
                        case 3005:
                            frmInstance = new frm_EP_Site_Inspection_Manager();
                            break;
                        case 3006:  // Tree Picker Mapping //
                            {
                                int intWorkSpaceID = 0;
                                int intWorkspaceOwner = 0;
                                string strWorkspaceName = "";
                                Mapping_Functions MapFunctions = new Mapping_Functions();
                                MapFunctions.Get_Mapping_Workspace(this.GlobalSettings, ref intWorkSpaceID, ref intWorkspaceOwner, ref strWorkspaceName);  // Pass by ref so values are updated in called function //
                                if (intWorkSpaceID == 0) return;
                                frmInstance = new frm_AT_Mapping_Tree_Picker(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 2);
                                break;
                            }
                        case 3007:
                            frmInstance = new frm_EP_Scheduler();
                            break;
                        #endregion

                        #region Winter Maintenance
                        case 4001:
                            frmInstance = new frm_Core_Client_Manager();
                            break;
                        case 4002:
                            frmInstance = new frm_Core_Site_Manager();
                            break;
                        case 4003:
                            frmInstance = new frm_GC_Company_Manager();
                            break;
                        case 4004:
                            frmInstance = new frm_Core_Contractor_Manager();
                            break;
                        case 4005:
                            frmInstance = new frm_GC_Gritting_Contract_Manager();
                            break;
                        case 4006:
                            frmInstance = new frm_GC_Import_Weather_Forecasts();
                            break;
                        case 4007:
                            frmInstance = new frm_GC_Callout_Manager();
                            break;
                        case 4008:
                            frmInstance = new frm_GC_Team_Self_Billing_Invoicing();
                            break;
                        case 4009:
                            frmInstance = new frm_GC_Snow_Contract_Manager();
                            break;
                        case 4010:
                            frmInstance = new frm_GC_Snow_Callout_Manager();
                            break;
                        case 4011:
                            frmInstance = new frm_GC_Snow_Authorise_Callouts();
                            break;
                        case 4012:
                            frmInstance = new frm_GC_Salt_Order_Manager();
                            break;
                        case 4013:
                            frmInstance = new frm_GC_Salt_Transfer_Manager();
                            break;
                        case 4014:
                            frmInstance = new frm_GC_Salt_Supplier_Manager();
                            break;
                        case 4015:
                            frmInstance = new frm_GC_Salt_Depot_Manager();
                            break;
                        case 4016:
                            frmInstance = new frm_GC_Finance_Invoice_Jobs();
                            break;
                        case 4017:
                            frmInstance = new frm_GC_Job_Sheet_Numbers();
                            break;
                        case 4018:
                            frmInstance = new frm_GC_Utilities();
                            break;
                        case 4019:
                            frmInstance = new frm_GC_Dashboard();
                            break;
                        #endregion

                        #region CRM 
                        case 6001:
                            frmInstance = new frm_Core_CRM_Manager();
                            break;
                        #endregion

                        #region Summer Maintenance
                        case 7001:
                            frmInstance = new frm_GC_SUM_Issue_Manager();
                            break;
                        case 7002:
                            frmInstance = new frm_GC_SUM_Permit_Manager();
                            break;
                        case 7003:
                            frmInstance = new frm_Operatives_Location();
                            break;
                        case 7004:
                            frmInstance = new frm_OM_Job_Manager();
                            break;
                        case 7005:
                            frmInstance = new frm_OM_Linked_Document_Manager();
                            break;
                        case 7006:
                            frmInstance = new frm_OM_Client_Contract_Manager();
                            break;
                        case 7007:
                            frmInstance = new frm_OM_Site_Contract_Manager();
                            break;
                        case 7008:
                            frmInstance = new frm_OM_Template_Billing_Profile_Manager();
                            break;
                        case 7009:
                            frmInstance = new frm_OM_Visit_Manager();
                            break;
                        case 7010:
                            frmInstance = new frm_OM_Visit_Template_Manager();
                            break;
                        case 7011:
                            frmInstance = new frm_OM_Job_Collection_Template_Manager();
                            break;
                        case 7012:
                            frmInstance = new frm_OM_Client_PO_Manager();
                            break;
                        case 7013:
                            frmInstance = new frm_OM_Waste_Disposal_Centre_Manager();
                            break;
                        case 7015:
                            frmInstance = new frm_OM_Schedule_Manager();
                            break;
                        case 7016:
                            frmInstance = new frm_OM_Client_PO_Manager();
                            break;
                        case 7017:
                            frmInstance = new frm_OM_Client_Billing_Manager();
                            break;
                        case 7018:
                            frmInstance = new frm_Core_Billing_Requirement_Template_Manager();
                            break;
                        case 7019:
                            frmInstance = new frm_OM_Waste_Document_Manager();
                            break;
                        case 7020:
                            frmInstance = new frm_OM_Team_Self_Billing_Manager();
                            break;
                        case 7021:
                            frmInstance = new frm_OM_Team_Self_Billing_Finance_Manager();
                            break;
                        case 7022:
                            frmInstance = new frm_OM_Tender_Manager();
                            break;
                        case 7023:
                            frmInstance = new frm_OM_Tender_Group_Manager();
                            break;
                        case 7030:
                            frmInstance = new frm_OM_Points_Team_Redemption_Manager();
                            break;
                         
                        #endregion

                        #region Core
                        case 8005:
                            frmInstance = new frm_Core_Audit_Trail_Viewer();
                            break;
                        case 8006:
                            frmInstance = new frm_Core_Web_Service_Queue_Viewer();
                            break;
                        #endregion

                        #region Utilities
                        case 10001:
                            frmInstance = new frm_UT_Survey_Manager();
                            break;
                        case 10002:
                            frmInstance = new frm_UT_WorkOrder_Manager();
                            break;
                        case 10003:  // Tree Picker Mapping //
                            {
                                int intWorkSpaceID = 0;
                                int intWorkspaceOwner = 0;
                                string strWorkspaceName = "";
                                Mapping_Functions MapFunctions = new Mapping_Functions();
                                MapFunctions.Get_Mapping_Workspace(this.GlobalSettings, ref intWorkSpaceID, ref intWorkspaceOwner, ref strWorkspaceName);  // Pass by ref so values are updated in called function //
                                if (intWorkSpaceID == 0) return;
                                frmInstance = new frm_UT_Mapping(intWorkSpaceID, intWorkspaceOwner, strWorkspaceName, 1);
                                break;
                            }
                        case 10004:
                            frmInstance = new frm_UT_Circuit_Manager();
                            break;
                        case 10005:
                            frmInstance = new frm_UT_Region_Manager();
                            break;
                        case 10006:
                            frmInstance = new frm_UT_SubArea_Manager();
                            break;
                        case 10007:
                            frmInstance = new frm_UT_Primary_Manager();
                            break;
                        case 10008:
                            frmInstance = new frm_UT_Feeder_Manager();
                            break;
                        case 10009:
                            frmInstance = new frm_UT_Pole_Manager();
                            break;
                        case 10015:
                            frmInstance = new frm_UT_Permission_Document_Manager();
                            break;
                        case 10016:  // Data Sync Manager//
                            {
                                if (this.MdiChildren.Length > 0)
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("Please close all open screens before starting the Utility ARB Data Synchronisation Process.", "Utility ARB Data Synchronisation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    return;
                                }
                                frm_UT_Data_Sync fChild = new frm_UT_Data_Sync();
                                fChild.GlobalSettings = this.GlobalSettings;
                                fChild.Owner = this;
                                fChild.ShowDialog();
                                return;
                            }
                        case 10018:
                            frmInstance = new frm_UT_Team_WorkOrder_Manager();
                            break;
                        case 10020:
                            frmInstance = new frm_UT_Dashboard();
                            break;
                        case 10021:
                            frmInstance = new frm_UT_Operational_Days_Manager();
                            break;
                        case 10022:
                            frmInstance = new frm_UT_Surveyed_Pole_Manager();
                            break;
                        case 10023:
                            frmInstance = new frm_UT_Action_Manager();
                            break;
                        case 10024:  // Utilities //
                            frm_UT_Utilities fUtilities = new frm_UT_Utilities();
                            fUtilities.GlobalSettings = this.GlobalSettings;
                            fUtilities.Owner = this;
                            fUtilities.ShowDialog();
                            return;
                        case 10025:
                            frmInstance = new frm_UT_Shutdown_Manager();
                            break;

                        case 10026:
                            frmInstance = new frm_UT_Resource_Rate_Manager();
                            frmInstance.GlobalSettings = this.GlobalSettings;
                            frmInstance.Owner = this;
                            break;
                        case 10027:
                            frmInstance = new frm_UT_Picklist_Manager();
                            frmInstance.GlobalSettings = this.GlobalSettings;
                            frmInstance.Owner = this;
                            break;
                        case 10028:
                            frmInstance = new frm_UT_Utility_Units_Manager();
                            frmInstance.GlobalSettings = this.GlobalSettings;
                            frmInstance.Owner = this;
                            break;
                        #endregion

                        #region HR
                        case 9901:
                            frmInstance = new frm_HR_Employee_Manager();
                            break;
                        case 9902:
                            frmInstance = new frm_HR_Master_Holiday_Manager();
                            break;
                        case 9903:
                            frmInstance = new frm_HR_Business_Area_Manager();
                            break;
                        case 9904:
                            frmInstance = new frm_HR_Holiday_Manager();
                            break;
                        case 9905:
                            frmInstance = new frm_HR_Sanction_Manager();
                            break;
                        case 9906:
                            frmInstance = new frm_HR_Pension_Manager();
                            break;
                        case 9909:
                            frmInstance = new frm_HR_Vetting_Manager();
                            break;
                        case 9910:
                            frmInstance = new frm_HR_Master_Shift_Pattern_Manager();
                            break;
                        case 9911:
                            frmInstance = new frm_HR_Work_Schedule_Manager();
                            break;
                        case 9912:
                            frmInstance = new frm_HR_Bonus_Manager();
                            break;
                        case 9913:
                            frmInstance = new frm_HR_Linked_Document_Manager();
                            break;
                        case 9914:
                            frmInstance = new frm_HR_CRM_Manager();
                            break;
                        case 9915:
                            frmInstance = new frm_HR_Dashboard();
                            break;
                        #endregion

                        #region Training
                        case 9100:
                            frmInstance = new frm_HR_Qualifications_Manager();
                            break;
                        case 9110:
                            frmInstance = new frm_HR_Training_Manager();
                            break;
                        case 9120:
                            frmInstance = new frm_TR_Team_Training_Manager();
                            break;
                        case 9130:
                            frmInstance = new frm_TR_Allocation_Manager();
                            break;

                        case 9140:
                            frmInstance = new frm_TR_Qualification_Override_Manager();
                            break;
                        #endregion
                        
                        #region Fleet
                        case 1101:
                            frmInstance = new frm_AS_Equipment_Manager();
                            break;
                        case 1102:
                            frmInstance = new frm_AS_Supplier_Manager();
                            frmInstance.GlobalSettings = this.GlobalSettings;
                            frmInstance.Owner = this;
                            break;
                        case 1103:
                            frmInstance = new frm_AS_Billing_Centre_Manager();
                            break;
                        case 1104:
                            frmInstance = new frm_AS_Depreciation_Setting_Manager();
                            break;
                        case 1105:
                            frmInstance = new frm_AS_Fleet_Manager();
                            break;
                        case 1112://Makes and models wizard
                            frm_Makes_Model_Wizard wChildForm = new frm_Makes_Model_Wizard();
                            wChildForm.GlobalSettings = this.GlobalSettings;
                            wChildForm.strRecordIDs = "";
                            wChildForm.strFormMode = "add";
                            wChildForm.strCaller = "frmMain2";
                            //mChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            wChildForm.splashScreenManager = splashScreenManager2;
                            wChildForm.splashScreenManager.ShowWaitForm();
                            wChildForm.ShowDialog();
                            break;
                        case 1118://Self Biller
                            frm_Self_Biller sbChildForm = new frm_Self_Biller();
                            sbChildForm.GlobalSettings = this.GlobalSettings;
                            sbChildForm.strRecordIDs = "";
                            sbChildForm.strFormMode = "add";
                            sbChildForm.strCaller = "frmMain2";
                            //mChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerSB1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            sbChildForm.splashScreenManager = splashScreenManagerSB1;
                            sbChildForm.splashScreenManager.ShowWaitForm();
                            sbChildForm.ShowDialog();
                            break;
                        case 1119://KAM Report
                            frm_KAM_Report_Wizard krChildForm = new frm_KAM_Report_Wizard();
                            krChildForm.GlobalSettings = this.GlobalSettings;
                            krChildForm.strRecordIDs = "";
                            krChildForm.strFormMode = "add";
                            krChildForm.strCaller = "frmMain2";
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerKR1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            krChildForm.splashScreenManager = splashScreenManagerKR1;
                            krChildForm.splashScreenManager.ShowWaitForm();
                            krChildForm.ShowDialog();
                            break;
                        case 1120://Import wizard
                            frm_Import_Wizard iChildForm = new frm_Import_Wizard();
                            iChildForm.GlobalSettings = this.GlobalSettings;
                            iChildForm.strRecordIDs = "";
                            iChildForm.strFormMode = "add";
                            iChildForm.strCaller = "frmMain2";
                            //mChildForm.FormPermissions = this.FormPermissions;
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManagerImport = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            iChildForm.splashScreenManager = splashScreenManagerImport;
                            iChildForm.splashScreenManager.ShowWaitForm();
                            iChildForm.ShowDialog();
                            break;
                        case 1106:
                            frmInstance = new frm_AS_Office_Manager();
                            break;
                        case 1107:
                            frmInstance = new frm_AS_Gadget_Manager();
                            break;
                        case 1108:
                            frmInstance = new frm_AS_Hardware_Manager();
                            break;
                        case 1109:
                            frmInstance = new frm_AS_Rental_Manager();
                            break;
                        case 1110:
                            frmInstance = new frm_AS_Picklist_Manager();
                            frmInstance.GlobalSettings = this.GlobalSettings;
                            frmInstance.Owner = this;
                            break;
                        case 1111:
                            frmInstance = new frm_AS_Notification_Manager();
                            frmInstance.GlobalSettings = this.GlobalSettings;
                            frmInstance.Owner = this;
                            break;
                        case 1113:
                            frmInstance = new frm_AS_Keeper_Manager();
                            break;
                        case 1114:
                            frmInstance = new frm_AS_Sim_Card_Manager();
                            frmInstance.GlobalSettings = this.GlobalSettings;
                            frmInstance.Owner = this;
                            break;
                        case 1115:
                            frmInstance = new frm_AS_Software_Manager();
                            break;
                        case 1116:
                            frmInstance = new frm_AS_Specifications_Manager();
                            frmInstance.GlobalSettings = this.GlobalSettings;
                            frmInstance.Owner = this;
                            break;
                        case 1117:
                            frmInstance = new frm_AS_Email_Schedule_Manager();
                            break;
                        case 1121:
                            frmInstance = new frm_AS_P11D_Manager();
                            break;
                        case 91103:
                            frmInstance = new frm_AS_Report_P11D_Export();
                            break;
                        #endregion

                        // ------------------------ REPORTING ----------------------------- //
                        #region Utilities
                        case 910001:
                            frmInstance = new frm_UT_Analysis_Poles();
                            break;
                        case 910002:
                            frmInstance = new frm_UT_Analysis_Actions();
                            break;
                        case 910003:
                            frmInstance = new frm_UT_Analysis_Permissioned_Work();
                            break;
                        #endregion

                        #region WoodPlan
                        case 92001:
                            frmInstance = new frm_AT_Report_Tree_Listing();
                            break;
                        case 92002:
                            //rmInstance = new frm_AT_Report_trees_with_last_inspection();
                            break;
                        case 92003:
                            frmInstance = new frm_AT_WorkOrder_Print();
                            break;
                        case 92004:
                            frmInstance = new frm_AT_WorkOrder_Analysis_1();
                            break;
                        case 92005:  // Amenity Trees Data Analysis //
                            frmInstance = new frm_AT_Analysis();
                            break;
                        #endregion

                        #region Winter Maintenance
                        case 92006:
                            frmInstance = new frm_GC_Report_Client_Gritting_Breakdown();
                            break;
                        case 92007:
                            frmInstance = new frm_GC_Report_Site_Listing();
                            break;
                        case 92008:
                            frmInstance = new frm_GC_Report_Site_Gritting_Contract_Listing();
                            break;
                        case 92009:
                            frmInstance = new frm_GC_Report_Site_Snow_Contract_Listing();
                            break;
                        case 92010:
                            frmInstance = new frm_GC_Report_Team_Listing();
                            break;
                        case 92011:
                            frmInstance = new frm_GC_Report_Gritting_Callout_Listing();
                            break;
                        case 92012:
                            frmInstance = new frm_GC_Report_Snow_Callout_Listing();
                            break;
                        case 92013:
                            frmInstance = new frm_GC_Analysis_Gritting_Callouts();
                            break;
                        case 92014:
                            frmInstance = new frm_GC_Analysis_Snow_Callouts();
                            break;
                        case 92015:
                            frmInstance = new frm_GC_Analysis_Gritting_And_Snow_Callouts();
                            break;
                        #endregion

                        #region HR
                        case 99001:
                            frmInstance = new frm_HR_Report_Payroll_Export_Absences();
                            break;
                        case 99002:
                            frmInstance = new frm_HR_Report_Employee_Personal_Details();
                            break;
                        case 99003:
                            frmInstance = new frm_HR_Report_Pay_Rises();
                            break;
                        case 99004:
                            frmInstance = new frm_HR_Bonus_Manager();
                            break;
                        #endregion

                        #region Fleet
                        case 91101:
                            frmInstance = new frm_AS_Report_Equipment_Expense();
                            break;
                        case 91102:
                            frmInstance = new frm_AS_Report_Equipment_Allocation();
                            break;
                        #endregion

                        default:
                            DevExpress.XtraEditors.XtraMessageBox.Show("Not Available Yet.");
                            break;
                    }
                    if (frmInstance != null)
                    {
                        // Following statement controls which loading screen is used for the forms //
                        switch (intScreenID)
                        {
                            case 1:  // Permissions Manager //
                            case 4:  // Picklist Manager //
                            case 11:  // Permission Groups Manager //
                            case 30:  // Staff Manager //
                            //case 2001:  // Tree Manager //
                            case 2002:  // GBM Mobile Data Transfer //
                            case 2003:
                            case 2004:  // Tree Picker Mapping //
                            case 2005:  // Mapping Utilities dialog //                           
                            case 2006:  // Tablet Data Transfer dialog //                          
                            case 2007:  // Locality Manager //                            
                            //case 2008:  // Inspection Manager //                          
                            //case 2009:  // Action Manager //                           
                            //case 2010:  // Monitor Outstanding Work //
                            //case 2011:  // Job Rate Manager //
                            //case 2012:  // Budget \ Ownership \ Cost Centre Manager //
                            //case 2013:  // Work Order Manager //
                            //case 2014:  // Incident Manager //
                            //case 2015:  // Amenity Trees Utilities //
                            //case 2016:  // Mail Merge Manager //
                            //case 3001:  // Client Manager //
                            //case 3002:  // Site Manager //
                            //case 3003:  // Asset Manager //
                            //case 3004:  // Asset Action Manager //
                            //case 3005:  // Site Inspection Manager //
                            case 3006:  // Tree Picker Mapping //
                            //case 3007:  // Scheduler //
                            //case 4001:  // Client Manager //
                            //case 4002:  // Site Manager //
                            case 4003:  // Company Manager //
                            case 4004:  // Team Manager //
                            case 4005:  // Gritting Contract Manager //
                            case 4006:  // Import Weather Forecast and Client Authorisation //
                            case 4007:  // Callout Manager //
                            case 4008:  // Callout Authorisation \ Self Billing Invoicing //
                            case 4009:  // Snow Clearance Site Contract Manager //
                            case 4010:  // Snow Clearance Callout Manager //
                            case 4011:  // Snow Clearance Authorisation //
                            case 4012:  // Salt Order Manager //
                            case 4013:  // Salt Transfer Manager //
                            case 4014:  // Salt Supplier Manager //
                            case 4015:  // Salt Depot Manager //
                            case 4016:  // Finance - Invoice Jobs //
                            case 4017:  // Job Sheet Numbers (Missing) //
                            case 4018:  // Winter Maintenance - Utilities //

                            case 1102://Manage Supplier //
                            case 1110:  // Picklist Manager form //
                            case 1111:  // Notification Manager form //
                            case 1114:  // Sim Card Manager Manager form //
                            case 1116:  // Specifiations Manager form //
                            case 1121:  // P11D Manager
                            
                            case 10026: // Manage Resource Rate //
                            case 10027: // Pick List Manager //
                            case 10028: // Utility Units Manager //

                            //case 92001:  // Listing Reports //
                            case 92002:
                            case 92003:  // Work Order Printing //
                            //case 92004:  // Work Order Analysis //
                            //case 92005:  // Amenity Trees Data Analysis //
                            case 92006:  // Winter Maintenance - Client Job Breakdown Report //
                            case 92007:  // Winter Maintenance - Site Listing Report //
                            case 92008:  // Winter Maintenance - Site Gritting Contract Listing Report //
                            case 92009:  // Winter Maintenance - Site Snow Clearance Contract Listing Report //
                            case 92010:  // Winter Maintenance - Team Listing Report //
                            case 92011:  // Winter Maintenance - Gritting Callout Listing Report //
                            case 92012:  // Winter Maintenance - Snow Clearance Callout Listing Report //
                                frmProgress fProgress = new frmProgress(10);
                                this.AddOwnedForm(fProgress);
                                fProgress.Show();  // ***** Closed in PostOpen event ***** //
                                Application.DoEvents();
                                frmInstance.fProgress = fProgress;
                                break;
                            default:  // All New forms... //
                                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                                frmInstance.splashScreenManager = splashScreenManager1;
                                frmInstance.splashScreenManager.ShowWaitForm();
                                break;
                        }
                        frmInstance.GlobalSettings = this.GlobalSettings;
                        frmInstance.StaffID = intStaffID;
                        frmInstance.FormID = intScreenID;
                        frmInstance.MdiParent = this;
                        //frmInstance.WindowState = FormWindowState.Maximized;  // Removed otherwise RibbonBar does not display!!! //
                        frmInstance.Show();
                        // Invoke Post Open event on Base form //
                        MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null)
                        {
                            method.Invoke(frmInstance, new object[] { null });
                        }
                        this.ActivePanelActiveForm(1, 0, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        #region RibbonGroupMenuItemsClicked

        private void ribbonPageGroup2_CaptionButtonClick(object sender, DevExpress.XtraBars.Ribbon.RibbonPageGroupEventArgs e)
        {
            DataEditMenu1.ShowPopup(ribbon.Manager, MousePosition);
        }

        private void ribbonPageGroup3_CaptionButtonClick(object sender, DevExpress.XtraBars.Ribbon.RibbonPageGroupEventArgs e)
        {
            ToolsMenu1.ShowPopup(ribbon.Manager, MousePosition);
        }

        private void ribbonPageGroup1_CaptionButtonClick(object sender, DevExpress.XtraBars.Ribbon.RibbonPageGroupEventArgs e)
        {
            DataMenu1.ShowPopup(ribbon.Manager, MousePosition);
        }

        private void ribbonPageGroup5_CaptionButtonClick(object sender, RibbonPageGroupEventArgs e)
        {
            PrintingMenu1.ShowPopup(ribbon.Manager, MousePosition);
        }

        private void ribbonPageGroup6_CaptionButtonClick(object sender, RibbonPageGroupEventArgs e)
        {
            SystemPeriodsMenu1.ShowPopup(ribbon.Manager, MousePosition);
        }

        private void ribbonPageGroup10_CaptionButtonClick(object sender, RibbonPageGroupEventArgs e)
        {
            WindowMenu1.ShowPopup(ribbon.Manager, MousePosition);
        }

        #endregion

        private void frmMain2_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //if (tNotifications != null)
                //{
                //    tNotifications.Abort();
                //}

                if (tMagnifier != null)
                {
                    tMagnifier.Abort();
                }
            }
            catch (Exception ex)
            {
                // Aborting threads throws an exception, even though it's a natural thing to do!
                // So catch the exception and continue on quietly!
                Console.WriteLine(ex.Message);
            }
        }

        private void frmMain2_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (blAbortLoad == true)
                {

                }
                else
                {
                    if (e.Cancel == false)
                    {
                        if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to <b>close</b> the application?", "Application Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Question, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            //this.notifyIcon1.Visible = false;
                            if (tGallery != null)
                            {
                                tGallery.Abort();
                            }
                            try
                            {
                                /*if ((DateTime.Today.Day == 14 && DateTime.Today.Month == 2) || (DateTime.Today.Day == 1 && DateTime.Today.Month == 4) ||
                                    (DateTime.Today.Month == 12 && DateTime.Today.Day >= 13) || (DateTime.Today.Month == 10 && DateTime.Today.Day >= 28) ||
                                    ((DateTime.Today.Month >= 7 && DateTime.Today.Day >= 4) && (DateTime.Today.Month <= 8 && DateTime.Today.Day <= 4)))
                                {
                                    // Do not save the current skin as this is all part of the jolly jappery!!!
                                    // Just hope no one changes their skin during these dates, or there'll be trouble!!! ;-P
                                }
                                else
                                {*/
                                    int intColour1 = this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor.ToArgb();
                                    int intColour2 = this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor2.ToArgb();
                                    sp00020GetUserApplicationSettingsTableAdapter.sp00021StoreUserApplicationSettings(intApplicationConfigID, intStaffID, Convert.ToInt32(defaultLookAndFeel1.LookAndFeel.Style), defaultLookAndFeel1.LookAndFeel.SkinName, intColour1, intColour2);
                                //}
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            defaultLookAndFeel1.LookAndFeel.StyleChanged -= LookAndFeel_StyleChanged;  // Unhook //

                            this.notifyIcon1.Visible = false;
                            this.notifyIcon1.Dispose();
                        }
                        else
                        {
                            e.Cancel = true;
                            if (fSplash != null)
                            {
                                InitializeModules();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Same as for FormClosed event...
                Console.WriteLine(ex.Message);
            }
        }

        private void frmMain2_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized && this.GlobalSettings.MinimiseToTaskBar == true)
            {
                //this.Hide();
                openGiroToolStripMenuItem.Text = "Show Information Centre";
            }
            else
            {
                openGiroToolStripMenuItem.Text = "Hide Information Centre";
            }
        }

        private void openGiroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openGiroToolStripMenuItem.Text == "Hide Information Centre")
            {
                this.WindowState = FormWindowState.Minimized;
                this.Hide();
            }
            else
            {
                this.Show();
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to close Information Centre?", "Close Information Centre", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }


        #region Button Events...

        private void sbiAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            ctrlBase cbBase;
            DevExpress.XtraBars.Docking.DockPanel dpActive;
            if (objTest != null && objTest.GetType().Name == "DockPanel")
            {
                dpActive = dockManager1.ActivePanel;
                if (dpActive != null && dpActive.Name == "dpToDoList")
                {
                    foreach (Control ctrl in dpActive.Controls[0].Controls)
                    {
                        cbBase = (ctrlBase)ctrl;
                        MethodInfo method = typeof(ctrlBase).GetMethod("OnAddEvent");
                        if (method != null)
                        {
                            method.Invoke(cbBase, new object[] { this, ea });
                        }
                        break;
                    }
                }
            }
            else
            {
                fbBase = (frmBase)this.ActiveMdiChild;
                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnAddEvent");
                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea });
                    }
                }
            }
        }

        private void iBlockAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnBlockAddEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void sbiEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            ctrlBase cbBase;
            DevExpress.XtraBars.Docking.DockPanel dpActive;
            if (objTest.GetType().Name == "DockPanel")
            {
                dpActive = dockManager1.ActivePanel;
                if (dpActive != null && dpActive.Name == "dpToDoList")
                {
                    foreach (Control ctrl in dpActive.Controls[0].Controls)
                    {
                        cbBase = (ctrlBase)ctrl;
                        MethodInfo method = typeof(ctrlBase).GetMethod("OnEditEvent");
                        if (method != null)
                        {
                            method.Invoke(cbBase, new object[] { this, ea });
                        }
                        break;
                    }
                }
            }
            else
            {
                fbBase = (frmBase)this.ActiveMdiChild;
                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnEditEvent");
                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea });
                    }
                }
            }
        }

        private void iBlockEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnBlockEditEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            ctrlBase cbBase;
            DevExpress.XtraBars.Docking.DockPanel dpActive;
            if (objTest != null && objTest.GetType().Name == "DockPanel")
            {
                dpActive = dockManager1.ActivePanel;
                if (dpActive != null && dpActive.Name == "dpToDoList")
                {
                    foreach (Control ctrl in dpActive.Controls[0].Controls)
                    {
                        cbBase = (ctrlBase)ctrl;
                        MethodInfo method = typeof(ctrlBase).GetMethod("OnDeleteEvent");
                        if (method != null)
                        {
                            method.Invoke(cbBase, new object[] { this, ea });
                        }
                        break;
                    }
                }
            }
            else
            {
                fbBase = (frmBase)this.ActiveMdiChild;
                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnDeleteEvent");
                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea });
                    }
                }
            }
        }

        private void iSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            ctrlBase cbBase;
            DevExpress.XtraBars.Docking.DockPanel dpActive;
            if (objTest != null && objTest.GetType().Name == "DockPanel")
            {
                dpActive = dockManager1.ActivePanel;
                if (dpActive != null && dpActive.Name == "dpToDoList")
                {
                    foreach (Control ctrl in dpActive.Controls[0].Controls)
                    {
                        cbBase = (ctrlBase)ctrl;
                        MethodInfo method = typeof(ctrlBase).GetMethod("OnSaveEvent");
                        if (method != null)
                        {
                            method.Invoke(cbBase, new object[] { this, ea });
                        }
                        break;
                    }
                }
            }
            else
            {
                fbBase = (frmBase)this.ActiveMdiChild;
                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnSaveEvent");
                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea });
                    }
                }
            }
        }

        private void iCut_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnCutEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iCopy_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnCopyEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iPaste_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnPasteEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iUndo_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnUndoEvent");

                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iRedo_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnRedoEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                WaitDialogForm loadingForm = new WaitDialogForm("Loading Preview...", "Print Preview");
                loadingForm.Show();


                frmPrintPreview fPrintPreview = new frmPrintPreview();
                fPrintPreview.loadingForm = loadingForm;
                fPrintPreview.objObject = fbBase;
                fPrintPreview.MdiParent = this;
                fPrintPreview.GlobalSettings = this.GlobalSettings;
                fPrintPreview.StaffID = intStaffID;
                //fPrintPreview.WindowState = FormWindowState.Maximized;  // Commented out otherwise RibbonBar doesn't display!!! //
                fPrintPreview.Show();
                // Invoke Post Open event on Base form //
                MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                if (method != null)
                {
                    method.Invoke(fPrintPreview, new object[] { null });
                }
            }
        }

        private void iPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnPrintEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iBold_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnBoldEvent");

                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iItalic_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnItalicEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iUnderline_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnUnderlineEvent");

                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iAlignLeft_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnAlignLeftEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iAlignCentre_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnAlignCentreEvent");

                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iAlignRight_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnAlignRightEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iBullet_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnBulletEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iImage_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnImageEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iSpellCheck_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnSpellCheckEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iAdd_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            ctrlBase cbBase;
            DevExpress.XtraBars.Docking.DockPanel dpActive;
            if (objTest.GetType().Name == "DockPanel")
            {
                dpActive = dockManager1.ActivePanel;
                if (dpActive != null && dpActive.Name == "dpToDoList")
                {
                    foreach (Control ctrl in dpActive.Controls[0].Controls)
                    {
                        cbBase = (ctrlBase)ctrl;
                        MethodInfo method = typeof(ctrlBase).GetMethod("OnAddEvent");
                        if (method != null)
                        {
                            method.Invoke(cbBase, new object[] { this, ea });
                        }
                        break;
                    }
                }
            }
            else
            {
                fbBase = (frmBase)this.ActiveMdiChild;

                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnAddEvent");

                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea });
                    }
                }
            }
        }

        private void iEdit_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            ctrlBase cbBase;
            DevExpress.XtraBars.Docking.DockPanel dpActive;
            if (objTest.GetType().Name == "DockPanel")
            {
                dpActive = dockManager1.ActivePanel;
                if (dpActive != null && dpActive.Name == "dpToDoList")
                {
                    foreach (Control ctrl in dpActive.Controls[0].Controls)
                    {
                        cbBase = (ctrlBase)ctrl;
                        MethodInfo method = typeof(ctrlBase).GetMethod("OnEditEvent");
                        if (method != null)
                        {
                            method.Invoke(cbBase, new object[] { this, ea });
                        }
                        break;
                    }
                }
            }
            else
            {
                fbBase = (frmBase)this.ActiveMdiChild;

                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnEditEvent");
                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea });
                    }
                }
            }
        }

        private void iPrintSetup_ItemClick(object sender, ItemClickEventArgs e)
        {
            PrintDialog dlg = new PrintDialog();
            //dlg.Document = printDoc;
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                //printDoc.Print();
            }

            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnPrintSetupEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnClearEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iSelectAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnSelectAllEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iMyOptions_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnMyOptionsEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iLoadLayout_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmBase fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                frmUserScreenSettingsLoad fChildForm = new frmUserScreenSettingsLoad(fbBase);
                this.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    fbBase.LoadedViewID = fChildForm.intLayoutID;
                    fbBase.LoadedViewCreatedByID = fChildForm.intLayoutCreatedByID;
                    if (this.GlobalSettings.ShowConfirmations == 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Screen Layout Loaded Successfully.", "Load Screen Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void iSaveLayout_ItemClick(object sender, ItemClickEventArgs e)
        {
            int intLoadedViewID = 0;
            int intLoadedViewCreatedByID = 0;
            frmBase fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                intLoadedViewID = fbBase.LoadedViewID;
                intLoadedViewCreatedByID = fbBase.LoadedViewCreatedByID;
                if (intLoadedViewID <= 0)  // Open Save As Form to get a description etc for the layout //
                {
                    frmUserScreenSettingsSave fChildForm = new frmUserScreenSettingsSave(fbBase);
                    this.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    if (fChildForm.ShowDialog() == DialogResult.Yes)
                    {
                        fbBase.LoadedViewID = fChildForm.intLayoutID;
                        fbBase.LoadedViewCreatedByID = fChildForm.intLayoutCreatedByID;
                    }
                }
                else
                {
                    if (intLoadedViewCreatedByID != this.GlobalSettings.UserID && this.GlobalSettings.PersonType != 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("You cannot save changes to the currently selected view as you did not create it.", "Save Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Ok to proceed so update saved layout. //
                    Load_Saved_Layout SaveLayout = new Load_Saved_Layout();
                    SaveLayout.Store_Screen_Object_Settings(strConnectionString, intLoadedViewID, fbBase, null);
                }
            }
        }

        private void iSaveLayoutAs_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmBase fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                frmUserScreenSettingsSave fChildForm = new frmUserScreenSettingsSave(fbBase);
                this.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                if (fChildForm.ShowDialog() == DialogResult.Yes)
                {
                    fbBase.LoadedViewID = fChildForm.intLayoutID;
                    fbBase.LoadedViewCreatedByID = fChildForm.intLayoutCreatedByID;
                    if (this.GlobalSettings.ShowConfirmations == 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Screen Layout Saved.", "Save Screen Layout As", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                //FillLayoutMenu(fbBase.FormID);  // Update Available Layout menu for this form //
            }
        }

        private void iSystemOptions_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnSystemOptionsEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnExportEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnExitEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
            this.Close();
        }

        private void iGrammerCheck_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnGrammerCheckEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iScratchPad_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnScratchPadEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iClipboard_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnClipboardEvent");
                method.Invoke(fbBase, new object[] { this, ea });
            }
         }

        private void beiFontSize_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnFontSizeEvent");

                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iFilterManager_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Check Permissions...
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnFilterManagerEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iFilterCreate_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            // Check Permissions...
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnCreateFilterEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iDefaultView_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnDefaultViewEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iFind_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnFindEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iReplace_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnReplaceEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iToDoList_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.ShowUtilityPanel(12, 12002, this, stcCrypto, intStaffID, this.GlobalSettings);
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnToDoListEvent");
                method.Invoke(fbBase, new object[] { this, ea });
            }
        }

        private void sbiToDoList_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.ShowUtilityPanel(12, 12002, this, stcCrypto, intStaffID, this.GlobalSettings);
        }

        private void iLinkRecordToDoList_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnLinkRecordToDoListEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iCommentBank_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.ShowUtilityPanel(12, 12008, this, stcCrypto, intStaffID, this.GlobalSettings);
        }

        private void iCommentBankCodes_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.ShowUtilityPanel(12, 12007, this, stcCrypto, intStaffID, this.GlobalSettings);
        }

        private void beiLivePeriod_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            fbBase = (frmBase)this.ActiveMdiChild;
            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnLivePeriodEvent");
                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void beiViewedPeriod_EditValueChanged(object sender, EventArgs e)
        {
            int intViewedPeriod = Convert.ToInt32(beiViewedPeriod.EditValue);
            if (intViewedPeriod != GlobalSettings.ViewedPeriodID)
            {
                // Update Global Setting then pass message to all open form sheets... //
                object row = ViewedPeriodLookUpEdit1.GetDataSourceRowByKeyValue(intViewedPeriod);
                DateTime dtViewedStartDate = Convert.ToDateTime((row as DataRowView)["dtFromDate"]);
                DateTime dtViewedEndDate = Convert.ToDateTime((row as DataRowView)["dtToDate"]);
                GlobalSettings.ViewedPeriodID = intViewedPeriod;
                GlobalSettings.ViewedStartDate = dtViewedStartDate;
                GlobalSettings.ViewedEndDate = dtViewedEndDate;
                if (GlobalSettings.ViewedPeriodID != GlobalSettings.LivePeriodID)
                {
                    GlobalSettings.DataEntryOff = true;  // Disable data entry as viewed and live periods don't match //
                }
                else
                {
                    GlobalSettings.DataEntryOff = false;  // Enable data entry as viewed and live periods match //
                }

                EventArgs ea = new EventArgs();
                frmBase fbBase;
                fbBase = (frmBase)this.ActiveMdiChild;
                if (fbBase != null)
                {
                    MethodInfo method = typeof(frmBase).GetMethod("OnViewedPeriodEvent");
                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { this, ea, GlobalSettings });
                    }
                }
            }
        }

        private void iEditSystemPeriods_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.MdiChildren.Length != 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Please close all forms before editing the system periods.", "Edit system Periods", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                frmSystemPeriods fSystemPeriods = new frmSystemPeriods();
                fSystemPeriods.GlobalSettings = this.GlobalSettings;
                if (fSystemPeriods.ShowDialog() == DialogResult.OK)
                {
                    this.GlobalSettings.ViewedPeriodID = fSystemPeriods.intViewedPeriodID;
                    this.GlobalSettings.ViewedStartDate = fSystemPeriods.dtViewedStartDate;
                    this.GlobalSettings.ViewedEndDate = fSystemPeriods.dtViewedEndDate;
                    beiViewedPeriod.EditValue = this.GlobalSettings.ViewedPeriodID;
                    this.GlobalSettings.LivePeriodID = fSystemPeriods.intLivePeriodID;
                    this.GlobalSettings.LiveStartDate = fSystemPeriods.dtLiveStartDate;
                    this.GlobalSettings.LiveEndDate = fSystemPeriods.dtLiveEndDate;
                    beiLivePeriod.EditValue = this.GlobalSettings.LivePeriodID;
                }
            }

            //EventArgs ea = new EventArgs();
            //frmBase fbBase;

            //fbBase = (frmBase)this.ActiveMdiChild;

            //if (fbBase != null)
            //{
            //    MethodInfo method = typeof(frmBase).GetMethod("OnEditSystemPeriodsEvent");

            //    if (method != null)
            //    {
            //        method.Invoke(fbBase, new object[] { this, ea });
            //    }
            //}
        }

        private void barSubItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;

            fbBase = (frmBase)this.ActiveMdiChild;

            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnFilterManagerEvent");

                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        private void iSelectGroup_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;

            fbBase = (frmBase)this.ActiveMdiChild;

            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnSelectGroupEvent");

                if (method != null)
                {
                    method.Invoke(fbBase, new object[] { this, ea });
                }
            }
        }

        #endregion 


        #region Site Control Panel

        int intCurrentTimeDeductionID = 0;  // Instance var //
        public void SetControlPanelStatus(string status, string strParameter1)
        {
            switch (status.ToLower())
            {
                case "start":
                    {
                        // Add inspection //
                        this.GlobalSettings.CurrentSiteInspectionID = -1;  // Flag to tell Edit Inspection screen on save to set the flag //

                        frmProgress fProgress = null;
                        fProgress = new frmProgress(10);
                        this.AddOwnedForm(fProgress);
                        fProgress.Show();  // ***** Closed in PostOpen event ***** //
                        Application.DoEvents();

                        frm_EP_Site_Inspection_Edit fChildForm = new frm_EP_Site_Inspection_Edit();
                        fChildForm.MdiParent = this;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = "frmMain2";
                        fChildForm.intRecordCount = 0;
                        //fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.fProgress = fProgress;
                        fChildForm.Show();
                        MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case "first_saved":
                    {
                        btnStart.Enabled = false;
                        btnFinish.Enabled = true;
                        btnPause.Enabled = true;
                        btnResume.Enabled = false;
                        this.GlobalSettings.CurrentSiteInspectionDescription = strParameter1;
                        labelControlSelectedInspection.Text = "Inspection: <b>" + this.GlobalSettings.CurrentSiteInspectionDescription + "</b>";
                    }
                    break;
                case "finish":
                    {
                        // End inspection //
                        if (XtraMessageBox.Show("You are about to complete the linked inspection.\r\nIf you proceed the current inspection will be marked as completed, the current date and time will be recorded against the inspection and if the inspection record is open, it will be closed.\r\n\r\nAre you sure you wish to proceed?", "Complete Inspection", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No) return;
                        // OK to proceed - check if the inspection is currently open //
                        DateTime CurrentDateTime = DateTime.Now;
                        bool boolRecordUpdated = false;
                        foreach (Form frmChild in this.MdiChildren)
                        {
                            switch (frmChild.Name)
                            {
                                case "frm_EP_Site_Inspection_Edit":
                                    frm_EP_Site_Inspection_Edit fForm;
                                    fForm = (frm_EP_Site_Inspection_Edit)frmChild;
                                    if (fForm.Finish_Inspection(this.GlobalSettings.CurrentSiteInspectionID, CurrentDateTime))
                                    {
                                        // Finish DateTime set on form so close it now - prompting the user to save it //
                                        fForm.Close();
                                        boolRecordUpdated = true;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (!boolRecordUpdated)  // Record wasn't currently open so try and update it in the back-end DB //
                        {
                            SqlConnection SQlConn = new SqlConnection(strConnectionString);
                            SQlConn.Open();
                            SqlCommand cmd = null;
                            cmd = new SqlCommand("sp03056_EP_Update_Inspection_End_Time", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@intInspectionID", this.GlobalSettings.CurrentSiteInspectionID));
                            cmd.Parameters.Add(new SqlParameter("@dtFinishDateTime", CurrentDateTime));
                            try
                            {
                                cmd.ExecuteScalar();
                            }
                            catch (Exception ex)
                            {
                                XtraMessageBox.Show("An error occurred while attempting to update the inspection with the End Date Time and Elapsed Time!\n\nError: [" + ex.Message + "]!\n\nTry Completing the Inspection again. If the problem persists contact Technical Support.", "Finish Inspection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                SQlConn.Close();
                                SQlConn.Dispose();
                                return;
                            }
                            SQlConn.Close();
                            SQlConn.Dispose();
                            Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                            Broadcast.Site_Inspection_Immediate_Refresh(this, this.Name, this.GlobalSettings.CurrentSiteInspectionID.ToString() + ";", ""); // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        }

                        btnStart.Enabled = true;
                        btnFinish.Enabled = false;
                        btnPause.Enabled = false;
                        btnResume.Enabled = false;
                        this.GlobalSettings.CurrentSiteInspectionID = 0;
                        this.GlobalSettings.CurrentSiteInspectionDescription = "";
                        labelControlSelectedInspection.Text = "No Inspection, <b>Click Start</b> to Proceed";
                    }
                    break;
                case "pause":
                    {
                        // Pause inspection //
                        frm_EP_Site_Inspection_Edit_Pause fChildForm = new frm_EP_Site_Inspection_Edit_Pause();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intInspectionID = this.GlobalSettings.CurrentSiteInspectionID;
                        fChildForm.strInspectionDescription = this.GlobalSettings.CurrentSiteInspectionDescription;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;

                        DateTime dtStartTime = fChildForm.dtStartDate;
                        string strDescription = fChildForm.strReason;
                        // Add new Time Deduction record //
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SQlConn.Open();
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp03064_EP_Insert_Inspection_Time_Deduction", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@InspectionID", this.GlobalSettings.CurrentSiteInspectionID));
                        cmd.Parameters.Add(new SqlParameter("@StartTime", dtStartTime));
                        cmd.Parameters.Add(new SqlParameter("@Description", strDescription));
                        try
                        {
                            intCurrentTimeDeductionID = Convert.ToInt32(cmd.ExecuteScalar());
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show("An error occurred while attempting to add the Time Deduction record!\n\nError: [" + ex.Message + "]!\n\nTry Pausing the Inspection again. If the problem persists contact Technical Support.", "Pause Inspection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            SQlConn.Close();
                            SQlConn.Dispose();
                            return;
                        }
                        SQlConn.Close();
                        SQlConn.Dispose();
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        Broadcast.Site_Inspection_Time_Deduction_Refresh(this, this.Name, intCurrentTimeDeductionID.ToString() + ";", "");  // Notify any open forms which reference this data that they will need to refresh their data on activating //
                        Broadcast.Site_Inspection_Time_Deduction_Immediate_Refresh(this, this.Name, intCurrentTimeDeductionID.ToString() + ";", "");  // Notify any open forms which reference this data that they will need to refresh their data immediatly //

                        btnStart.Enabled = false;
                        btnFinish.Enabled = false;
                        btnPause.Enabled = false;
                        btnResume.Enabled = true;
                        labelControlSelectedInspection.Text = "Paused, <b>Click Resume</b> to continue";
                    }
                    break;
                case "resume":
                    {
                        // Resume inspection //
                        DateTime CurrentDateTime = DateTime.Now;
                        SqlConnection SQlConn = new SqlConnection(strConnectionString);
                        SQlConn.Open();
                        SqlCommand cmd = null;
                        cmd = new SqlCommand("sp03065_EP_Complete_Inspection_Time_Deduction", SQlConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@DeductionID", intCurrentTimeDeductionID));
                        cmd.Parameters.Add(new SqlParameter("@EndTime", CurrentDateTime));
                        try
                        {
                            cmd.ExecuteScalar();
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show("An error occurred while attempting to update the current Time Deduction record with the End Date Time!\n\nError: [" + ex.Message + "]!\n\nTry Resuming the Inspection again. If the problem persists contact Technical Support.", "Resume Inspection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            SQlConn.Close();
                            SQlConn.Dispose();
                            return;
                        }
                        // New Time Deduction completed so now attempt to update parent site inspection's Total Time Deductions... //
                        Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                        bool boolRecordUpdated = false;
                        foreach (Form frmChild in this.MdiChildren)
                        {
                            switch (frmChild.Name)
                            {
                                case "frm_EP_Site_Inspection_Edit":
                                    frm_EP_Site_Inspection_Edit fForm;
                                    fForm = (frm_EP_Site_Inspection_Edit)frmChild;
                                    if (fForm.Check_For_Matching_Inspection_And_Calc_Total_Deducted_Time(this.GlobalSettings.CurrentSiteInspectionID)) boolRecordUpdated = true;
                                    // Above line updates total time deductions then elapsed time //
                                    break;
                                default:
                                    break;
                            }
                        }
                        if (!boolRecordUpdated)  // Record wasn't currently open so try and update it in the back-end DB //
                        {
                            // update Total Time Deductions first //
                            cmd = null;
                            cmd = new SqlCommand("sp03066_EP_Inspection_Update_Total_Time_Deductions", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@InspectionID", this.GlobalSettings.CurrentSiteInspectionID));
                            try
                            {
                                cmd.ExecuteScalar();
                            }
                            catch (Exception ex)
                            {
                                XtraMessageBox.Show("An error occurred while attempting to update the inspection with the Total Time Deduction!\n\nError: [" + ex.Message + "]!\n\nTry editing the inspection and clicking the Calculate button on the Total Time Deductions field or contact Technical Support for further information.", "Update Inspection Total Time Deductions", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            // update Total Elapsed Time second //
                            DateTime dtEndTime = Convert.ToDateTime("01/01/1900 00:00:00.000");  // No value in it.
                            cmd = null;
                            cmd = new SqlCommand("sp03056_EP_Update_Inspection_End_Time", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@intInspectionID", this.GlobalSettings.CurrentSiteInspectionID));
                            cmd.Parameters.Add(new SqlParameter("@dtFinishDateTime", dtEndTime));
                            try
                            {
                                cmd.ExecuteScalar();
                            }
                            catch (Exception ex)
                            {
                                XtraMessageBox.Show("An error occurred while attempting to update the inspection with the Elapsed Time!\n\nError: [" + ex.Message + "]!\n\nTry editing the Inspection and clicking the Calculate button on the Total Time Taken field or contact Technical Support for further information.", "Update Inspection Total Time Taken", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            }
                            Broadcast.Site_Inspection_Immediate_Refresh(this, this.Name, this.GlobalSettings.CurrentSiteInspectionID.ToString() + ";", ""); // Notify any open forms which reference this data that they will need to refresh their data immediatly //
                        }
                        SQlConn.Close();
                        SQlConn.Dispose();
                        Broadcast.Site_Inspection_Time_Deduction_Immediate_Refresh(this, this.Name, intCurrentTimeDeductionID.ToString() + ";", "");  // Notify any open forms which reference this data that they will need to refresh their data immediatly //

                        intCurrentTimeDeductionID = 0;  // Clear Instance Var //
                        btnStart.Enabled = false;
                        btnFinish.Enabled = true;
                        btnPause.Enabled = true;
                        btnResume.Enabled = false;
                        labelControlSelectedInspection.Text = "Inspection: <b>" + this.GlobalSettings.CurrentSiteInspectionDescription + "</b>";
                    }
                    break;
                case "unlink":
                    {
                        // Kill link to current inspection //
                        btnStart.Enabled = true;
                        btnFinish.Enabled = false;
                        btnPause.Enabled = false;
                        btnResume.Enabled = false;
                        this.GlobalSettings.CurrentSiteInspectionID = 0;
                        this.GlobalSettings.CurrentSiteInspectionDescription = "";
                        labelControlSelectedInspection.Text = "No Inspection, <b>Click Start</b> to Proceed";
                    }
                    break;
                default:
                    break;
            }

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            SetControlPanelStatus("start", "");
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            SetControlPanelStatus("finish", "");
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            SetControlPanelStatus("pause", "");
        }

        private void btnResume_Click(object sender, EventArgs e)
        {
            SetControlPanelStatus("resume", "");
        }


        #endregion


        public void PermissionsHandler(ArrayList alPermissions)
        {
            try
            {
                foreach (DevExpress.XtraBars.BarItem bbiItem in ribbon.Items)
                {
                    if (!alExcludedMenuItems.Contains(bbiItem.Name)) 
                    {
                        bbiItem.Enabled = false;
                    }
                }           
                foreach (string strName in alPermissions)
                {
                    if (strName.StartsWith("-"))
                    {
                        ribbon.Items[strName.Substring(1, strName.Length - 1)].Enabled = false;
                    }
                    else
                    {
                        ribbon.Items[strName].Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        void Gallery_ItemClick(object sender, GalleryItemClickEventArgs e)
        {
            frmBase fbBase = (frmBase)this.ActiveMdiChild;
            int intStaffID = Convert.ToInt32(e.Item.Tag);

            if (fbBase != null)
            {
                stcSharedLayouts sslLayouts = new stcSharedLayouts();

                sslLayouts.fbBase = fbBase;
                sslLayouts.intFormID = fbBase.FormID;
                sslLayouts.intUserID = intStaffID;
                //psPlugins.StartPlugin(9999, 8, this, stcCrypto, this.GlobalSettings.UserID, this.GlobalSettings, sslLayouts);
            }
        }

        private void FillFilterMenu(int intFormID)
        {
            // Commented out by Mark: 24/02/2009 //
      /*      BarButtonItem bbiFilterItem;
            Boolean blDefaultViewPresent = false;
      
            sp00061LoadUserFiltersTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00061LoadUserFiltersTableAdapter.Fill(this.woodPlanDataSet.sp00061LoadUserFilters, 0, intFormID, this.GlobalSettings.UserID, 0);

            iMyFilters.ClearLinks();
            iAvailableFilters.ClearLinks();

            for (int i = 0; i < this.woodPlanDataSet.sp00061LoadUserFilters.Rows.Count; i++)
            {
                if (Convert.ToInt32(this.woodPlanDataSet.sp00061LoadUserFilters.Rows[i]["FilterOwnerID"]) != intStaffID)
                {
                    bbiFilterItem = new BarButtonItem();

                   bbiFilterItem.Tag = Convert.ToInt32(this.woodPlanDataSet.sp00061LoadUserFilters.Rows[i]["FilterID"]);
                    bbiFilterItem.Caption = this.woodPlanDataSet.sp00061LoadUserFilters.Rows[i]["FilterDescription"].ToString();
                    bbiFilterItem.Name = "iFilterItem";
                    bbiFilterItem.Caption = "[" + this.woodPlanDataSet.sp00061LoadUserFilters.Rows[i]["UserName"].ToString() + "]  " + bbiFilterItem.Caption;
                    iAvailableFilters.AddItem(bbiFilterItem);
                    bbiFilterItem.ItemClick += new ItemClickEventHandler(bbiFilterItem_ItemClick);
                }
                else
                {
                    bbiFilterItem = new BarButtonItem();

                    bbiFilterItem.Tag = Convert.ToInt32(this.woodPlanDataSet.sp00061LoadUserFilters.Rows[i]["FilterID"]);
                    bbiFilterItem.Caption = this.woodPlanDataSet.sp00061LoadUserFilters.Rows[i]["FilterDescription"].ToString();
                    bbiFilterItem.Name = "iFilterItem";
                    iMyFilters.AddItem(bbiFilterItem);
                    bbiFilterItem.ItemClick += new ItemClickEventHandler(bbiFilterItem_ItemClick);
                }
            }

            if (!blDefaultViewPresent)
            {
                bbiDefaultView.Enabled = false;
            }
       * */
        }

        void bbiFilterItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmBase fbBase = (frmBase)this.ActiveMdiChild;
            BarButtonItem bbiFilterItem = (BarButtonItem)e.Item;

            if (fbBase != null)
            {
                //psPlugins.LoadFilter(9999, Convert.ToInt32(bbiFilterItem.Tag), stcCrypto, this.GlobalSettings.UserID, this.GlobalSettings, fbBase);
            }
        }

        void bbiLayoutItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmBase fbBase = (frmBase)this.ActiveMdiChild;
            BarButtonItem bbiLayoutItem = (BarButtonItem)e.Item;

            if (fbBase != null)
            {

            }
        }

        private void OnPanelActivated(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.XtraMessageBox.Show("Stage 1 Completed!");
        }

        private void frmMain2_MdiChildActivate(object sender, EventArgs e)
        {
            BaseObjects.AppMessenger am = new AppMessenger();
            am.SendMessage(1, 0, false);
        }

        public void ActivePanelActiveForm(int intFormID, int intPanelID, Boolean blLoseFocus)
        {
            if (intFormID == 0 && blLoseFocus == false)
            {
                if (this.dockManager1.ActivePanel != null)
                {
                    objTest = this.dockManager1.ActivePanel;
                }
            }
            else if((intFormID == 0 && blLoseFocus == true) || (intFormID > 0 && blLoseFocus == false))
            {
                if (this.ActiveMdiChild != null)
                {
                    objTest = this.ActiveMdiChild;
                }
            }
        }

        private void dockManager1_ClosedPanel(object sender, DevExpress.XtraBars.Docking.DockPanelEventArgs e)
        {
            DevExpress.XtraBars.Docking.DockPanel dpPanel;

            if (e.Panel.Name == "dpCommentEdit")
            {
                // dpPanel = dockManager1.Panels["dpCommentCodes"];

                for (int i = dockManager1.Panels.Count - 1; i > 0; i-- )
                {
                    dpPanel = dockManager1.Panels[i];

                    if (dpPanel.Name == "dpCommentCodes" || dpPanel.Name == "dpCommentEdit")
                    {
                        dockManager1.RemovePanel(dpPanel);
                        dpPanel.Close();
                        dpPanel.Dispose();
                    }
                }
            }
            else
            {
                if (e.Panel.Name == "dpClipboard" || e.Panel.Name == "dpToDoList" || e.Panel.Name == "dpCommentBank"
                    || e.Panel.Name == "dpCommentEdit" || e.Panel.Name == "dpCommentBankSelection" || e.Panel.Name == "dpScriptBuilder")
                {
                    dockManager1.RemovePanel(e.Panel);
                    e.Panel.Dispose();
                }
            }

            if (this.ActiveMdiChild != null)
            {
                MethodInfo method = this.ActiveMdiChild.GetType().GetMethod("SetMenuStatus");

                if (method != null)
                {
                    method.Invoke(this.ActiveMdiChild, new object[] { });
                }

                objTest = this.ActiveMdiChild;
            }
            else
            {
                ProcessPermissionsForForm();
            }
        }

        private void SetMicroHelpText(string strHelpText)
        {
            // Send a blank string to reset the caption to "Ready"...
            if (strHelpText == "")
            {
                MicroHelpStaticItem.Caption = "Ready";
            }
            else
            {
                MicroHelpStaticItem.Caption = strHelpText;
            }
        }

        private void ProgressBarProcessor(int intMaximum, int intMinimum,int intStep, Boolean blProgress)
        {
            if (blProgress == false)
            {
                if (intMaximum == 0 && intMinimum == 0 && intStep == 0)
                {
                    iProgressBar.Reset();
                }
                else
                {
                    rpiProgressBar.Maximum = intMaximum;
                    rpiProgressBar.Minimum = intMinimum;
                    rpiProgressBar.Step = intStep;
                }
            }
            else
            {
                iProgressBar.EditValue = (int)iProgressBar.EditValue + rpiProgressBar.Step;
            }
        }

        public void OnNewToDoList(object sender, EventArgs e)
        {
            BaseObjects.stcToDoList stdlToDoList = (BaseObjects.stcToDoList)sender;
            //psPlugins.ShowUtilityPanel(12, 12003, this, stcCrypto, intStaffID, this.GlobalSettings, stdlToDoList);            
        }

        public void OnUpdateActiveFormPanel(ArrayList alActionsAndIDs, int intFormID, string strFormName)
        {
            DevExpress.XtraNavBar.NavBarGroup nbgFormGroup;
            DevExpress.XtraNavBar.NavBarItem nbiItem;

            nbgFormGroup = new DevExpress.XtraNavBar.NavBarGroup(strFormName);
            nbgFormGroup.Tag = intFormID;

            nbcActiveForm.Groups.Add(nbgFormGroup);

            if (alActionsAndIDs.Count == 0)
            {
                if (Convert.ToInt32(nbcActiveForm.Groups[0].Tag) == intFormID)
                {
                    nbcActiveForm.Groups[0].ItemLinks.Clear();
                    nbcActiveForm.Groups.Clear();
                }
            }
            else
            {
                foreach (stcActiveForm safForm in alActionsAndIDs)
                {
                    nbiItem = new DevExpress.XtraNavBar.NavBarItem(safForm.strActionText);
                    nbiItem.Tag = safForm;
                    //                strBitmap = row["ImageName"].ToString() + ".gif";
                    //                strBitmap = strBitmap.ToLower();
                    //                nbiItem.SmallImage = frmParent.imageList1.Images[strBitmap];
                    // frmParent.nbcSwitchboard.Groups[i].ItemLinks.Add(nbiItem);

                    nbcActiveForm.Groups[0].ItemLinks.Add(nbiItem);
                }
            }

            if (nbcActiveForm.Groups.Count > 0)
            {
                nbcActiveForm.Groups[0].Expanded = true;
            }

        }

        private void nbcActiveForm_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            frmBase fbActiveForm = (frmBase)this.ActiveMdiChild;
            if (fbActiveForm != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnProcessActiveFormAction");
                if (method != null)
                {
                    method.Invoke(fbActiveForm, new object[] { ((stcActiveForm)e.Link.Item.Tag).intActionID, ((stcActiveForm)e.Link.Item.Tag).intRecordID });
                }
            }
        }

        private void nbcSwitchboard_NavDragOver(object sender, DevExpress.XtraNavBar.ViewInfo.NavBarDragDropEventArgs e)
        {
            NavBarItemLink nbSourceLink;
            NavBarGroup nbgSourceGroup;
            NavBarGroup nbgTargetGroup;
            nbSourceLink = (NavBarItemLink)e.Data.GetData(typeof(NavBarItemLink));
            if (nbSourceLink == null)
            {
                return;
            }       
            nbgSourceGroup = nbSourceLink.Group;
            nbgTargetGroup = e.Group;
            if(nbgSourceGroup != nbgTargetGroup)
            {
                if (e.Group.Caption != "Favourites")
                {
                    e.Effect = DragDropEffects.None;
                }
                else
                {
                    bool boolExists = false;
                    for (int i = 0; i < nbgTargetGroup.ItemLinks.Count; i++) 
                    {
                       if (nbgTargetGroup.ItemLinks[i].Caption == nbSourceLink.Caption) boolExists = true;
                    }
                    if (boolExists)
                    {
                        e.Effect = DragDropEffects.None;
                    }
                    else
                    {
                        e.Effect = DragDropEffects.Copy;
                    }
                }
            }
        }

        private void nbcReports_NavDragOver(object sender, DevExpress.XtraNavBar.ViewInfo.NavBarDragDropEventArgs e)
        {
            NavBarItemLink nbSourceLink;
            NavBarGroup nbgSourceGroup;
            NavBarGroup nbgTargetGroup;
            nbSourceLink = (NavBarItemLink)e.Data.GetData(typeof(NavBarItemLink));
            if (nbSourceLink == null)
            {
                return;
            }
            nbgSourceGroup = nbSourceLink.Group;
            nbgTargetGroup = e.Group;
            if (nbgSourceGroup != nbgTargetGroup)
            {
                if (e.Group.Caption != "Favourites")
                {
                    e.Effect = DragDropEffects.None;
                }
                else
                {
                    bool boolExists = false;
                    for (int i = 0; i < nbgTargetGroup.ItemLinks.Count; i++)
                    {
                        if (nbgTargetGroup.ItemLinks[i].Caption == nbSourceLink.Caption) boolExists = true;
                    }
                    if (boolExists)
                    {
                        e.Effect = DragDropEffects.None;
                    }
                    else
                    {
                        e.Effect = DragDropEffects.Copy;
                    }
                }
            }
        }

        private void nbcSwitchboard_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraNavBar.NavBarHitInfo hi = nbcSwitchboard.CalcHitInfo(new Point(e.X, e.Y));
            if (hi.InLink)
            {
                nbiSelected = hi.Link.Item;
                if (e.Button == MouseButtons.Right)
                {
                    SwitchboardMenu.ShowPopup(Control.MousePosition);
                }
            }
        }

        void biItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            // *** NOT SURE IF THIS IS STILL USED - MB ***** //
            if (e.Item.Caption == "Select")
            {
                stcLinkItemIDs sliIDs = (stcLinkItemIDs)nbiSelected.Tag;

                //psPlugins.StartPlugin(sliIDs.intModuleID, sliIDs.intFormID, this, stcCrypto, intStaffID, this.GlobalSettings);
            }
            else if (e.Item.Caption == "Delete")
            {
                nbcSwitchboard.Groups["Favourites"].ItemLinks.Remove(nbiSelected);
            }
        }

        private void nbcReports_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraNavBar.NavBarHitInfo hi = nbcReports.CalcHitInfo(new Point(e.X, e.Y));
            if (hi.InLink)
            {
                nbiSelected = hi.Link.Item;         
                if (e.Button == MouseButtons.Right)
                {
                    ReportsMenu.ShowPopup(Control.MousePosition);
                }
            }
        }

        private void bbiSelect_ItemClick(object sender, ItemClickEventArgs e)
        {
            stcLinkItemIDs sliIDs = (stcLinkItemIDs)nbiSelected.Tag;
            //psPlugins.StartPlugin(sliIDs.intModuleID, sliIDs.intFormID, this, stcCrypto, intStaffID, this.GlobalSettings);
        }

        private void bbiDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            stcLinkItemIDs sliIDs;
            for (int i = 0; i < nbcSwitchboard.Groups.Count; i++)
            {
                if (nbcSwitchboard.Groups[i].Caption == "Favourites")
                {
                    sliIDs = (stcLinkItemIDs)nbiSelected.Tag;
                    sp00001PopulateSwitchboardTableAdapter.sp00044_RemoveSwitchboardFavourite(sliIDs.intFormID, sliIDs.intModuleID, sliIDs.intType);
                    nbcSwitchboard.Groups[i].ItemLinks.Remove(nbiSelected);
                    break;
                }
            }
        }

        private void bbiReportSelect_ItemClick(object sender, ItemClickEventArgs e)
        {
            stcLinkItemIDs sliIDs = (stcLinkItemIDs)nbiSelected.Tag;
            //psPlugins.OpenPluginReport(sliIDs.intModuleID, sliIDs.intFormID, this, stcCrypto, GlobalSettings);
        }

        private void bbiReportDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            stcLinkItemIDs sliIDs;
            for (int i = 0; i < nbcReports.Groups.Count; i++)
            {
                if (nbcReports.Groups[i].Caption == "Favourites")
                {
                    sliIDs = (stcLinkItemIDs)nbiSelected.Tag;
                    sp00001PopulateSwitchboardTableAdapter.sp00044_RemoveSwitchboardFavourite(sliIDs.intFormID, sliIDs.intModuleID, sliIDs.intType);
                    nbcReports.Groups[i].ItemLinks.Remove(nbiSelected);
                    break;
                }
            }
        }

        private void nbcSwitchboard_DragDrop(object sender, DragEventArgs e)
        {
            NavBarItemLink nbiLink;
            stcLinkItemIDs sliIDs;
            try
            {
                nbiLink = (NavBarItemLink)e.Data.GetData("DevExpress.XtraNavBar.NavBarItemLink", true);

                sliIDs = (stcLinkItemIDs)nbiLink.Item.Tag;
                int intNewID = Convert.ToInt32(this.sp00001PopulateSwitchboardTableAdapter.sp00046_UpdateSwitchboardFavourite(0, this.GlobalSettings.UserID, sliIDs.intModuleID, sliIDs.intFormID, nbiLink.Caption, sliIDs.intType));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void nbcReports_DragDrop(object sender, DragEventArgs e)
        {
            NavBarItemLink nbiLink;
            stcLinkItemIDs sliIDs;
            try
            {
                nbiLink = (NavBarItemLink)e.Data.GetData("DevExpress.XtraNavBar.NavBarItemLink", true);

                sliIDs = (stcLinkItemIDs)nbiLink.Item.Tag;
                int intNewID = Convert.ToInt32(this.sp00001PopulateSwitchboardTableAdapter.sp00046_UpdateSwitchboardFavourite(0, this.GlobalSettings.UserID, sliIDs.intModuleID, sliIDs.intFormID, nbiLink.Caption, sliIDs.intType));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void iLoadFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            /*EventArgs ea = new EventArgs();
            MethodInfo method;
            frmBase fbBase;
            int intFormID = 0;
            stcGridFilters sgfFilter = new stcGridFilters();

            // Check Permissions...

            fbBase = (frmBase)this.ActiveMdiChild;
            
            if (fbBase != null)
            {
                // Get Active Form ID...
                intFormID = fbBase.FormID;
            
                // Retrieve Available Filters...
                sgfFilter = (stcGridFilters)psPlugins.StartPlugin(9999, 6, this, stcCrypto, intStaffID, this.GlobalSettings, intFormID);

                if (sgfFilter.intFilterID > 0)
                {
                    method = typeof(frmBase).GetMethod("OnLoadFilterEvent");

                    if (method != null)
                    {
                        method.Invoke(fbBase, new object[] { sgfFilter, ea });
                    }
                }
            }
            */
        }

        private void iSaveFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
    /*        EventArgs ea = new EventArgs();
            frmBase fbBase;
//            int intFormID = 0;
            stcGridFilters sgfFilter = new stcGridFilters();
            
            // Check Permissions...

            fbBase = (frmBase)this.ActiveMdiChild;

            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnSaveFilterEvent");

                if (method != null)
                {
                    sgfFilter = (stcGridFilters)method.Invoke(fbBase, new object[] { this, ea });

                    if (sgfFilter.intFilterID > 0)
                    {
                        psPlugins.SaveFilter(9999, sgfFilter.intFilterID, stcCrypto, this.GlobalSettings.UserID, this.GlobalSettings, fbBase, sgfFilter);
                    }
                    else
                    {
                        if (sgfFilter.strFilterText!= null && sgfFilter.strFilterText != "")
                        {
                            // Store filter...
                            psPlugins.StartPlugin(9999, 5, this, stcCrypto, this.GlobalSettings.UserID, this.GlobalSettings, sgfFilter);
                        }
                    }
                }

                FillFilterMenu(fbBase.FormID);
            }
            */
        }

        private void iSaveFilterAs_ItemClick(object sender, ItemClickEventArgs e)
        {
            EventArgs ea = new EventArgs();
            frmBase fbBase;
            //            int intFormID = 0;
            stcGridFilters sgfFilter = new stcGridFilters();

            // Check Permissions...

            fbBase = (frmBase)this.ActiveMdiChild;

            if (fbBase != null)
            {
                MethodInfo method = typeof(frmBase).GetMethod("OnSaveFilterEvent");

                if (method != null)
                {
                    sgfFilter = (stcGridFilters)method.Invoke(fbBase, new object[] { this, ea });
                }

                if (sgfFilter.intFormID != 0 && sgfFilter.strFilterText != "")
                {
                    // Store filter...
                    //psPlugins.StartPlugin(9999, 5, this, stcCrypto, intStaffID, this.GlobalSettings, sgfFilter);
                }
            }

            // Profit!!!
        }

        private void bbiDefaultView_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmBase fbBase = (frmBase)this.ActiveMdiChild;
            BarButtonItem bbiLayoutItem = (BarButtonItem)e.Item;

            if (fbBase != null)
            {
                // Load Layout here...
                //psPlugins.LoadLayout(9999, Convert.ToInt32(bbiLayoutItem.Tag), stcCrypto, this.GlobalSettings.UserID, this.GlobalSettings, fbBase);
            }
        }

        private void ribbonPageGroup11_CaptionButtonClick(object sender, RibbonPageGroupEventArgs e)
        {
            FilterMenu.ShowPopup(ribbon.Manager, MousePosition);
        }

        private void ribbonPageGroup9_CaptionButtonClick(object sender, RibbonPageGroupEventArgs e)
        {
            LayoutMenu.ShowPopup(ribbon.Manager, MousePosition);
        }

        private void iLayoutManager_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.StartPlugin(9999, 8, this, stcCrypto, this.GlobalSettings.UserID, this.GlobalSettings);
        }

        private void bbiCloseAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strCount = this.MdiChildren.Length.ToString();

            if (this.MdiChildren.Length == 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you want to close the current form?", "Close All Forms", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (frmBase fbBase in this.MdiChildren)
                    {
                        fbBase.Close();
                    }
                }
            }
            else if(this.MdiChildren.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you want to close " + strCount + " forms?", "Close All Forms", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (frmBase fbBase in this.MdiChildren)
                    {
                        fbBase.Close();
                    }
                }
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Show();
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Minimized;
                this.Hide();   
            }
        }

        private void iCurrentUser_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Replace with a menu to give several options...
            pmCurrentUserMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
        }

        private void Switch_User()
        {
            // Check if user is a Super user...
            if (this.GlobalSettings.UserType1.ToUpper() == "SUPER")
            {
                // If they are, allow them to login as some one else...
                frmLogin2 flLogin = new frmLogin2(alNames, intStaffID, intSystemID);
                flLogin.frmParent = this;

                if (flLogin.ShowDialog() == DialogResult.OK)
                {
                    tmrNotification.Stop();

                    strConnectionString = set.WoodPlanConnectionString;
                    GlobalSettings.ConnectionString = strConnectionString;

                    sp00019GetUserDetailsForStartUpTableAdapter.Connection.ConnectionString = strConnectionString;
                    sp00019GetUserDetailsForStartUpTableAdapter.Fill(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp, set.UserName);
                    
                    // Changing login account...
                    intStaffID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
                    GlobalSettings.UserID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
                    GlobalSettings.UserType1 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType1"].ToString();
                    GlobalSettings.UserSurname = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserSurname"].ToString();
                    GlobalSettings.UserForename = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserForename"].ToString();
                    GlobalSettings.Username = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUsername"].ToString().ToLower();
                    GlobalSettings.ToDoList = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intToDoList"]);
                    GlobalSettings.CommentBank = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intCommentBank"]);
                    GlobalSettings.ShowTipsOnStart = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowTipsOnStart"]);
                    GlobalSettings.ShowFormOnStart = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intDefaultForm"]);
                    GlobalSettings.UserScreenConfig = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserConfig"]);
                    GlobalSettings.ToolTips = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowToolTips"]);
                    GlobalSettings.ContactGroups = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intContactGroups"]);
                    GlobalSettings.ShowConfirmations = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowConfirmations"]);
                    GlobalSettings.UserType2 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType2"].ToString();
                    GlobalSettings.UserType3 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType3"].ToString();
                    GlobalSettings.LiveStartDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtLivePeriodStart"]);
                    GlobalSettings.LiveEndDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtLivePeriodEnd"]);
                    GlobalSettings.ViewedStartDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtViewedPeriodStart"]);
                    GlobalSettings.ViewedEndDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtViewedPeriodEnd"]);
                    GlobalSettings.PersonType = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intPersonType"]);
                    GlobalSettings.LivePeriodID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intLivePeriodID"]);
                    GlobalSettings.ViewedPeriodID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intViewedPeriodID"]);

                    if (GlobalSettings.LivePeriodID == GlobalSettings.ViewedPeriodID)
                    {
                        GlobalSettings.DataEntryOff = false;
                    }
                    else
                    {
                        GlobalSettings.DataEntryOff = true;
                    }

                    if (strActualUserName.ToLower() != set.UserName.ToLower())
                    {
                        iCurrentUser.Caption = strActualUserName + " as " + set.UserName;
                        iCurrentUser.Appearance.Options.UseFont = true;
                    }
                    else
                    {
                        iCurrentUser.Caption = strActualUserName;
                        iCurrentUser.Appearance.Options.UseFont = false;
                    }
                   
                    beiLivePeriod.EditValue = this.GlobalSettings.LivePeriodID;
                    beiViewedPeriod.EditValue = this.GlobalSettings.ViewedPeriodID;

                    this.sp00020GetUserApplicationSettingsTableAdapter.Connection.ConnectionString = strConnectionString;
                    this.sp00020GetUserApplicationSettingsTableAdapter.Fill(this.woodPlanDataSet.sp00020GetUserApplicationSettings, intStaffID);

                    if (this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows.Count > 0)
                    {
                        switch (Convert.ToInt32(this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ActiveLookAndFeel"]))
                        {
                            case 0:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
                                break;
                            case 1:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
                                break;
                            case 2:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
                                break;
                            case 3:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
                                break;
                            case 4:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                                break;
                        }

                        this.defaultLookAndFeel1.LookAndFeel.SkinName = this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ActiveSkinName"].ToString();

                        int intColour1 = Convert.ToInt32(woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["SkinMaskColor1"]);
                        int intColour2 = Convert.ToInt32(woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["SkinMaskColor2"]);
                        if (intColour1 != 0)
                        {
                            Color color1 = Color.FromArgb(intColour1);
                            Color color2 = Color.FromArgb(intColour2);
                            this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor = color1;
                            if (intColour2 != 0) this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor2 = color2;
                        }

                        intApplicationConfigID = Convert.ToInt32(this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ApplicationConfigID"]);
                    }

                    if (strFriendlyName != "Training Database")
                    {
                        // bsiTraining.Dispose();
                    }

                    sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
                    sp00039GetFormPermissionsForUserTableAdapter.Fill(this.woodPlanDataSet.sp00039GetFormPermissionsForUser, 0, intStaffID, 0,
                                                                      this.GlobalSettings.ViewedPeriodID);

                    ProcessPermissionsForForm();

                    dpActiveForm.Height = 300;

                    sp00001PopulateSwitchboardTableAdapter.Connection.ConnectionString = strConnectionString;

                    //StartNotificationTimer();
                }
            }
        }

        private void bbiShowLoggedInUsers_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmConnectedUsers fcuUsers = new frmConnectedUsers();
            fcuUsers.Owner = this;
            fcuUsers.GlobalSettings = this.GlobalSettings;
            fcuUsers.Show();
        }

        private void bbiChangeCurrentUserSuper_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.MdiChildren.Length > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more forms are open. Please close all forms before proceeding.", "Change User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                Switch_User();
            }
        }

        private void bbiChangeCurrentUser_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmAvailableUsers fauUsers = new frmAvailableUsers(alNames, intStaffID, intSystemID);
            fauUsers.UserType = this.GlobalSettings.UserType1;
            fauUsers.frmParent = this;
            fauUsers.Owner = this;

            if (this.MdiChildren.Length > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more forms are open. Please close all forms before proceeding.", "Change User", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                if (fauUsers.ShowDialog() == DialogResult.OK)
                {
                    tmrNotification.Stop();

                    strConnectionString = set.WoodPlanConnectionString;
                    GlobalSettings.ConnectionString = strConnectionString;

                    sp00019GetUserDetailsForStartUpTableAdapter.Connection.ConnectionString = strConnectionString;
                    sp00019GetUserDetailsForStartUpTableAdapter.Fill(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp, set.UserName);

                    // Changing login account...
                    intStaffID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
                    GlobalSettings.UserID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
                    GlobalSettings.UserType1 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType1"].ToString();
                    GlobalSettings.UserSurname = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserSurname"].ToString();
                    GlobalSettings.UserForename = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserForename"].ToString();
                    GlobalSettings.Username = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUsername"].ToString().ToLower();
                    GlobalSettings.ToDoList = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intToDoList"]);
                    GlobalSettings.CommentBank = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intCommentBank"]);
                    GlobalSettings.ShowTipsOnStart = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowTipsOnStart"]);
                    GlobalSettings.ShowFormOnStart = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intDefaultForm"]);
                    GlobalSettings.UserScreenConfig = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserConfig"]);
                    GlobalSettings.ToolTips = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowToolTips"]);
                    GlobalSettings.ContactGroups = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intContactGroups"]);
                    GlobalSettings.ShowConfirmations = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowConfirmations"]);
                    GlobalSettings.UserType2 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType2"].ToString();
                    GlobalSettings.UserType3 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType3"].ToString();
                    GlobalSettings.LiveStartDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtLivePeriodStart"]);
                    GlobalSettings.LiveEndDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtLivePeriodEnd"]);
                    GlobalSettings.ViewedStartDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtViewedPeriodStart"]);
                    GlobalSettings.ViewedEndDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtViewedPeriodEnd"]);
                    GlobalSettings.PersonType = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intPersonType"]);
                    GlobalSettings.LivePeriodID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intLivePeriodID"]);
                    GlobalSettings.ViewedPeriodID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intViewedPeriodID"]);

                    if (GlobalSettings.LivePeriodID == GlobalSettings.ViewedPeriodID)
                    {
                        GlobalSettings.DataEntryOff = false;
                    }
                    else
                    {
                        GlobalSettings.DataEntryOff = true;
                    }

                    if (strActualUserName.ToLower() != set.UserName.ToLower())
                    {
                        iCurrentUser.Caption = strActualUserName + " as " + set.UserName;
                        iCurrentUser.Appearance.Options.UseFont = true;
                    }
                    else
                    {
                        iCurrentUser.Caption = strActualUserName;
                        iCurrentUser.Appearance.Options.UseFont = false;
                    }

                    beiLivePeriod.EditValue = this.GlobalSettings.LivePeriodID;
                    beiViewedPeriod.EditValue = this.GlobalSettings.ViewedPeriodID;

                    this.sp00020GetUserApplicationSettingsTableAdapter.Connection.ConnectionString = strConnectionString;
                    this.sp00020GetUserApplicationSettingsTableAdapter.Fill(this.woodPlanDataSet.sp00020GetUserApplicationSettings, intStaffID);

                    if (this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows.Count > 0)
                    {
                        switch (Convert.ToInt32(this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ActiveLookAndFeel"]))
                        {
                            case 0:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
                                break;
                            case 1:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
                                break;
                            case 2:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
                                break;
                            case 3:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
                                break;
                            case 4:
                                defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                                break;
                        }

                        this.defaultLookAndFeel1.LookAndFeel.SkinName = this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ActiveSkinName"].ToString();

                        int intColour1 = Convert.ToInt32(woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["SkinMaskColor1"]);
                        int intColour2 = Convert.ToInt32(woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["SkinMaskColor2"]);
                        if (intColour1 != 0)
                        {
                            Color color1 = Color.FromArgb(intColour1);
                            Color color2 = Color.FromArgb(intColour2);
                            this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor = color1;
                            if (intColour2 != 0) this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor2 = color2;
                        }

                        intApplicationConfigID = Convert.ToInt32(this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ApplicationConfigID"]);
                    }

                    if (strFriendlyName != "Training Database")
                    {
                        // bsiTraining.Dispose();
                    }

                    sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
                    sp00039GetFormPermissionsForUserTableAdapter.Fill(this.woodPlanDataSet.sp00039GetFormPermissionsForUser, 0, intStaffID, 0,
                                                                      this.GlobalSettings.ViewedPeriodID);

                    ProcessPermissionsForForm();

                    dpActiveForm.Height = 300;

                    sp00001PopulateSwitchboardTableAdapter.Connection.ConnectionString = strConnectionString;

                    //StartNotificationTimer();
                }
            }
        }

        private void tabbedView1_DocumentAdded(object sender, DevExpress.XtraBars.Docking2010.Views.DocumentEventArgs e)
        {
            frmBase fbBase = (frmBase)e.Document.Control;

            if (dockManager1.Panels.Contains(dpPersonImage))
            {
                ClearImageInImagePanel("");
            }

            //iLoadFilter.Enabled = ib_LoadFilter;
            //iFilterCreate.Enabled = ib_CreateFilter;
            //iSaveFilter.Enabled = ib_SaveFilter;
            //iSaveFilterAs.Enabled = ib_SaveFilterAs;
            iLoadFilter.Enabled = false;
            iFilterCreate.Enabled = false;
            iSaveFilter.Enabled = false;
            iSaveFilterAs.Enabled = false;

            iLoadLayout.Enabled = ib_LoadLayout;
            iSaveLayout.Enabled = ib_SaveLayout;
            iSaveLayoutAs.Enabled = ib_SaveLayoutAs;

            //sbiToDoList.Enabled = ib_ToDoList;
            //iToDoList.Enabled = ib_ToDoList;
            sbiToDoList.Enabled = false;
            iToDoList.Enabled = false;

            //iFastFind.Enabled = ib_FastFind;
            iFastFind.Enabled = false;

            //bbiCalculator.Enabled = ib_Calculator;
            bbiCalculator.Enabled = false;

            //iClipboard.Enabled = ib_Clipboard;
            iClipboard.Enabled = false;

            //sbiCommentBank.Enabled = ib_CommentBank;
            //iCommentBank.Enabled = ib_CommentBank;
            sbiCommentBank.Enabled = false;
            iCommentBank.Enabled = false;

            rgbiChartTypes.Enabled = false;
            bbiChartPalette.Enabled = false;
            rgbiChartAppearance.Enabled = false;
            bbiRotateAxis.Enabled = false;
            bbiChartWizard.Enabled = false;
            bbiLayoutFlip.Enabled = false;
            bbiLayoutRotate.Enabled = false;

            DevExpress.XtraBars.Docking2010.Views.BaseDocument bd = (DevExpress.XtraBars.Docking2010.Views.BaseDocument)e.Document;
            if (bd.Form.ShowIcon && bd.Form.Icon.Size == new Size(16, 16))  // Default VS icon is 32x32 //
            {
                e.Document.Image = bd.Form.Icon.ToBitmap();
            }
        }

        private void tabbedView1_DocumentActivated(object sender, DevExpress.XtraBars.Docking2010.Views.DocumentEventArgs e)
        {
            try
            {
                frmBase fbBase = (frmBase)e.Document.Control;
                if (fbBase != null)
                {
                    FillFilterMenu(fbBase.FormID);

                    if (tabbedView1.Documents.Count > 0)
                    {
                        //beiLivePeriod.Enabled = false;
                        beiViewedPeriod.Enabled = false;
                    }
                    else
                    {
                        foreach (DevExpress.XtraBars.BarItem bbiItem in ribbon.Items)
                        {
                            if (!alExcludedMenuItems.Contains(bbiItem.Name))
                            {
                                bbiItem.Enabled = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                foreach (DevExpress.XtraBars.BarItem bbiItem in ribbon.Items)
                {
                    if (!alExcludedMenuItems.Contains(bbiItem.Name))
                    {
                        bbiItem.Enabled = true;
                    }
                }
            }
        }

        private void tabbedView1_DocumentClosed(object sender, DevExpress.XtraBars.Docking2010.Views.DocumentEventArgs e)
        {
            if (tabbedView1.Documents.Count == 0)
            {
                foreach (DevExpress.XtraBars.BarItem bbiItem in ribbon.Items)
                {
                    if (!alExcludedMenuItems.Contains(bbiItem.Name) && !string.IsNullOrEmpty(bbiItem.Name))
                    {
                        bbiItem.Enabled = true;
                    }
                }
                sbiAdd.Enabled = false;
                sbiEdit.Enabled = false;
                iDelete.Enabled = false;
                iSave.Enabled = false;

                iCut.Enabled = false;
                iCopy.Enabled = false;
                iPaste.Enabled = false;
                iUndo.Enabled = false;
                iRedo.Enabled = false;

                iPreview.Enabled = false;
                iPrint.Enabled = false;

                iLoadLayout.Enabled = false;
                iSaveLayout.Enabled = false;
                iSaveLayoutAs.Enabled = false;

                rgbiChartTypes.Enabled = false;
                bbiChartPalette.Enabled = false;
                rgbiChartAppearance.Enabled = false;
                bbiRotateAxis.Enabled = false;
                bbiChartWizard.Enabled = false;
                bbiLayoutFlip.Enabled = false;
                bbiLayoutRotate.Enabled = false;
            }
        }

        private void FillExcludedMenuItemsArray()
        {
            alExcludedMenuItems.Add("iSpellCheck");
            alExcludedMenuItems.Add("iExit");
            alExcludedMenuItems.Add("MicroHelpStaticItem");
            alExcludedMenuItems.Add("iProgressBar");
            alExcludedMenuItems.Add("iCurrentUser");
            alExcludedMenuItems.Add("iDatabase");
            alExcludedMenuItems.Add("iGrammerCheck");
            alExcludedMenuItems.Add("sbiCommentBank");
            alExcludedMenuItems.Add("bbiEditComments");
            alExcludedMenuItems.Add("iCommentBankCodes");
            alExcludedMenuItems.Add("iCommentBank");
            alExcludedMenuItems.Add("iScratchPad");
            alExcludedMenuItems.Add("iClipboard");
            alExcludedMenuItems.Add("sbiToDoList");
            alExcludedMenuItems.Add("iFastFind");
            alExcludedMenuItems.Add("rgbiSkins");
            //alExcludedMenuItems.Add("beiLivePeriod");
            //alExcludedMenuItems.Add("beiViewedPeriod");
            alExcludedMenuItems.Add("iEditSystemPeriods");
            alExcludedMenuItems.Add("iTabbedInterface");
            alExcludedMenuItems.Add("iCascade");
            alExcludedMenuItems.Add("iTileHorizontal");
            alExcludedMenuItems.Add("iTileVertical");
            alExcludedMenuItems.Add("barMdiChildrenListItem1");
            alExcludedMenuItems.Add("iLoadLayout");
            alExcludedMenuItems.Add("iSaveLayout");
            alExcludedMenuItems.Add("iSaveLayoutAs");
            alExcludedMenuItems.Add("sbiOptions");
            alExcludedMenuItems.Add("iMyOptions");
            alExcludedMenuItems.Add("iSystemOptions");
            alExcludedMenuItems.Add("iShareLayout");
            alExcludedMenuItems.Add("iToDoList");
            alExcludedMenuItems.Add("iLinkRecordToDoList");
            alExcludedMenuItems.Add("iFilterCreate");
            alExcludedMenuItems.Add("iFilterManager");
            alExcludedMenuItems.Add("iSaveFilter");
            alExcludedMenuItems.Add("iLoadFilter");
            alExcludedMenuItems.Add("sbiFilter");
            alExcludedMenuItems.Add("iLayoutItem");
            alExcludedMenuItems.Add("sbiLayout");
            alExcludedMenuItems.Add("bbiDefaultView");
            alExcludedMenuItems.Add("bsiMyLayouts");
            alExcludedMenuItems.Add("bsiAvailableLayouts");
            alExcludedMenuItems.Add("iMyFilters");
            alExcludedMenuItems.Add("iAvailableFilters");
            alExcludedMenuItems.Add("iSaveFilterAs");
            alExcludedMenuItems.Add("iFilterItem");
            alExcludedMenuItems.Add("bbiCloseAll");
            alExcludedMenuItems.Add("bbiSelect");
            alExcludedMenuItems.Add("bbiDelete");
            alExcludedMenuItems.Add("bbiShowLoggedInUsers");
            alExcludedMenuItems.Add("bbiChangeCurrentUserSuper");
            alExcludedMenuItems.Add("bbiChangeCurrentUser");
            alExcludedMenuItems.Add("bbiCalculator");
            alExcludedMenuItems.Add("bbiCloseThisForm");
            alExcludedMenuItems.Add("bbiCloseAllForms");
            alExcludedMenuItems.Add("bbiCloseAllButThisForm");
            alExcludedMenuItems.Add("bbiMagnifier");
            alExcludedMenuItems.Add("iAbout");
            alExcludedMenuItems.Add("bbiColourMixer");
        }

        private void sbiCommentBank_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.strMode = "";
            //psPlugins.ShowUtilityPanel(12, 12008, this, stcCrypto, intStaffID, this.GlobalSettings);
        }

        private void iFastFind_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.strMode = "";
            //psPlugins.ShowUtilityPanel(12, 12011, this, stcCrypto, intStaffID, this.GlobalSettings);

        }
        
        private void bbiEditComments_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.strMode = "Edit";
            //psPlugins.ShowUtilityPanel(12, 12006, this, stcCrypto, intStaffID, this.GlobalSettings);
        }

        private void bbiCalculator_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.ShowUtilityPanel(12, 12009, this, stcCrypto, intStaffID, this.GlobalSettings);
        }

        private void bbiScriptBuilder_ItemClick(object sender, ItemClickEventArgs e)
        {
            //psPlugins.ShowUtilityPanel(12, 12010, this, stcCrypto, intStaffID, this.GlobalSettings);
        }

        public void OnScriptBuilder(object sender, object objDataset, string strScript)
        {
            //ArrayList alItems = new ArrayList();

            //// Using a bit of trickery discovered whilst trying to get another bit of trickery to work...
            //alItems.Add(objDataset);
            //alItems.Add(strScript);

            //psPlugins.ShowUtilityPanel(12, 12010, this, stcCrypto, intStaffID, this.GlobalSettings, alItems);
        }

        public void OnScriptBuilderReturning(object sender, object objScript)
        {
            /*foreach (frmBase frmChild in this.MdiChildren)
            {
                if (frmChild.FormID == 2005 && frmChild.Name == "frmAssessmentTemplate")
                {
                    MethodInfo method = frmChild.GetType().GetMethod("OnScriptBuilderReturning");
                    if (method != null)
                    {
                        method.Invoke(frmChild, new object[] { sender, objScript });
                    }
                    frmChild.Activate();
                    this.ActivePanelActiveForm(1, 0, false);
                    break;
                }
            }*/
        }

        [DllImport("User32.dll")]
        public static extern Int32 SetForegroundWindow(int hWnd);


        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
          /*  // defined in winuser.h
            //const int WM_DRAWCLIPBOARD = 0x308;
            //const int WM_CHANGECBCHAIN = 0x030D;
            const int WM_KEYDOWN = 0x0100;

            switch (m.Msg)
            {
                case WM_KEYDOWN:
                    if (m.Msg == WM_KEYDOWN && (Keys)m.WParam.ToInt32() == Keys.C && Control.ModifierKeys == Keys.Control)
                    {
                        //MessageBox.Show("Test Ctrl + C!");
                    }
                    else if (m.Msg == WM_KEYDOWN && (Keys)m.WParam.ToInt32() == Keys.V && Control.ModifierKeys == Keys.Control)
                    {
                        //MessageBox.Show("Test Ctrl + V!");
                    }
                    break;

                //case WM_DRAWCLIPBOARD:
                //    if (blFirstTime == false)
                //    {
                //        GetClipboardData();
                //    }
                //    SendMessage(nextClipboardViewer, m.Msg, m.WParam, m.LParam);
                //    break;

                //case WM_CHANGECBCHAIN:
                //    if (m.WParam == nextClipboardViewer)
                //        nextClipboardViewer = m.LParam;
                //    else
                //        SendMessage(nextClipboardViewer, m.Msg, m.WParam, m.LParam);
                //    break;
                default:
                    base.WndProc(ref m);
                    break;
            }*/


            // Listen for Message from WoodPlan Version 4 //
            //const int WM_COPYDATA = 0x4A;

            switch (m.Msg)
            {
                case 1030:  // Refresh Map //
                    // Called by WoodPlan V.4 f_Save() event from AT_tree_edit Screen triggered this via the gf_update_woodplan_5 global Function //
                    SetForegroundWindow(this.Handle.ToInt32());

                    // Make sure Mapping Form is Active - if not focus it //
                    frm_AT_Mapping_Tree_Picker child = null;
                    foreach (Form f in this.MdiChildren)
                    {
                        if (f.Text == "Mapping")
                        {
                            child = f as frm_AT_Mapping_Tree_Picker;
                            child.UpdateMapAfterChangesFromWoodplanVersion4(1);
                            break;

                        }
                    }
                    if (child != null) child.Focus(); // Make sure Mapping Form is active sheet //
                    break;
                case 1031:  // Open Map //
                    // Called by WoodPlan V.4 gf_generate_map_from_x_and_y() function //
                    SetForegroundWindow(this.Handle.ToInt32());

                    string strRegistryValue = "";
                    Microsoft.Win32.RegistryKey registry = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\\WoodPlan");
                    if (registry == null) return;
                    strRegistryValue = Convert.ToString(registry.GetValue("V4toV5ParameterString"));
                    registry.Close();
                    if (string.IsNullOrEmpty(strRegistryValue))
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to display on the map before proceeding.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    try
                    {
                        Mapping_Functions MapFunctions = new Mapping_Functions();
                        MapFunctions.Show_Map_From_Screen(this.GlobalSettings, strConnectionString, this, strRegistryValue, "tree");
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to open the map [" + ex.Message + "].\n\nTry opening the map again. If the problem persists contact Technical Support.", "Show Map", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    break;
                case 1032:  // Open WorkOrder //
                    // Called by WoodPlan V.4 gf_generate_map_from_x_and_y() function //
                    SetForegroundWindow(this.Handle.ToInt32());

                    string strRegistryValue2 = "";
                    Microsoft.Win32.RegistryKey registry2 = Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\\WoodPlan");
                    if (registry2 == null) return;
                    strRegistryValue2 = Convert.ToString(registry2.GetValue("V4toV5ParameterString"));
                    registry2.Close();

                    // Attempt to activate Print Work Orders screen is alreay open otherwise open it //
                    frm_AT_WorkOrder_Print frmWorkOrderPrint = null;
                    try
                    {
                        foreach (frmBase frmChild in this.MdiChildren)
                        {
                            if (frmChild.FormID == 92003)
                            {
                                frmChild.Activate();
                                frmWorkOrderPrint = (frm_AT_WorkOrder_Print)frmChild;
                                frmWorkOrderPrint.LoadWorkOrder_ExternalCall(strRegistryValue2);
                                return;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    // If we are here then the form was NOT already open, so open it //
                    frmWorkOrderPrint = new frm_AT_WorkOrder_Print();
                    frmProgress fProgress = new frmProgress(10);
                    this.AddOwnedForm(fProgress);
                    fProgress.Show();  // ***** Closed in PostOpen event ***** //
                    Application.DoEvents();
                    frmWorkOrderPrint.fProgress = fProgress;
                    frmWorkOrderPrint.MdiParent = this;
                    frmWorkOrderPrint.GlobalSettings = this.GlobalSettings;
                    frmWorkOrderPrint.Show();

                    frmWorkOrderPrint.LoadWorkOrder_ExternalCall(strRegistryValue2);
                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(frmWorkOrderPrint, new object[] { null });
                    break;
            }

            base.WndProc(ref m);
        }    
        
        private void GetClipboardData()
        {
            /*DevExpress.XtraBars.Docking.DockPanel dpPanel;
            ctrlBase cbBase;
                psPlugins.ShowUtilityPanel(12, 12001, this, stcCrypto, intStaffID, this.GlobalSettings);
                dpPanel = dockManager1.ActivePanel;
                cbBase = (ctrlBase)dpPanel.Controls[0].Controls["ctrlClipboard"];
                cbBase.OnCopyEvent(null, new EventArgs());*/
        }

        public void OnPasteClipItems(object sender, object objTextToPaste)
        {
           /* // Find the active window/panel(?) and send the text to it...
            if (this.ActiveMdiChild != null)
            {
                frmBase frmActiveWindow = (frmBase)this.ActiveMdiChild;
                MethodInfo method = frmActiveWindow.GetType().GetMethod("PasteTextToControl");
                if (method != null)
                {
                    method.Invoke(frmActiveWindow, new object[] { this, objTextToPaste });
                }
            }*/
        }

        public void OnClipboardCaptureChanged(object sender, object objStatus)
        {
            /*objClipboardCaller = sender;

            // This also needs to be saved as an option...
            Boolean blStatus = Convert.ToBoolean(objStatus);

            if (blStatus == true)
            {
                GlobalSettings.ClipboardCapture = true;
                nextClipboardViewer = (IntPtr)SetClipboardViewer((int)this.Handle);
                sp00019GetUserDetailsForStartUpTableAdapter.sp00063StoreUserApplicationSettings("ClipboardCapture", 1, GlobalSettings.UserID, intUserType);
            }
            else
            {
                GlobalSettings.ClipboardCapture = false;
                ChangeClipboardChain(this.Handle, nextClipboardViewer);
                sp00019GetUserDetailsForStartUpTableAdapter.sp00063StoreUserApplicationSettings("ClipboardCapture", 0, GlobalSettings.UserID, intUserType);
                Clipboard.Clear();
            }*/
        }

        private void bbiCloseThisForm_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this.ActiveMdiChild != null)
            {
                this.ActiveMdiChild.Close();
            }
        }

        private void bbiCloseAllForms_ItemClick(object sender, ItemClickEventArgs e)
        {
            foreach (Form frmChild in this.MdiChildren)
            {
                frmChild.Close();
            }
        }

        private void bbiCloseAllButThisForm_ItemClick(object sender, ItemClickEventArgs e)
        {
            foreach (Form frmChild in this.MdiChildren)
            {
                if (frmChild != this.ActiveMdiChild)
                {
                    frmChild.Close();
                }
            }
        }

        public void OnShowPhotoPopupChanged(object sender, int intValue)
        {
            if (intValue == 0)
            {
                if (dockManager1.Panels.Contains(dpPersonImage))
                {
                    dpPersonImage.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
                }
            }
            else
            {
                dpPersonImage.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
                dpPersonImage.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
                dpPersonImage.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
        }

        public void ShowPersonsPhoto(object sender, string strPhotoPath)
        {
            /*           Image imgLoaded;
                       Bitmap bmpResized;

                       if (dockManager1.Panels.Contains(dpPersonImage))
                       {
                           if (File.Exists(strPhotoPath) != false)
                           {
                               imgLoaded = Image.FromFile(strPhotoPath);
                               bmpResized = new Bitmap(imgLoaded,new Size(150,225));

                               pePersonsPhoto.Image = bmpResized;
                           }
                           else
                           {
                               imgLoaded = Image.FromFile(GlobalSettings.StudentPhotoPath + "\\default.jpg");
                               bmpResized = new Bitmap(imgLoaded, new Size(150, 225));
                    
                               pePersonsPhoto.Image = bmpResized;
                           }
                       }*/
        }

        public void ShowImageInImagePanel(Image PassedImage, string PassedSize, string PassedShowScrollbars)
        {
            if (dockManager1.Panels.Contains(dpPersonImage))
            {
                try
                {
                    if (PassedSize == "scale")
                    {
                        float floatWidthScale;
                        float floatHeightScale;
                        floatWidthScale = (float)xtraScrollableControl1.Width / (float)PassedImage.Width;
                        floatHeightScale = (float)xtraScrollableControl1.Height / (float)PassedImage.Height;

                        if (PassedShowScrollbars == "no")
                        {
                            floatWidthScale = (floatWidthScale <= floatHeightScale ? floatWidthScale : floatHeightScale);
                            floatHeightScale = (floatHeightScale <= floatWidthScale ? floatHeightScale : floatWidthScale);
                        }
                        pePersonsPhoto.Image = PassedImage.GetThumbnailImage(Convert.ToInt32((float)PassedImage.Width * floatWidthScale), Convert.ToInt32((float)PassedImage.Height * floatHeightScale), null, new IntPtr());
                        pePersonsPhoto.Size = new Size(Convert.ToInt32((float)PassedImage.Width * floatWidthScale), Convert.ToInt32((float)PassedImage.Height * floatHeightScale));
                    }
                    else
                    {
                        pePersonsPhoto.Image = PassedImage;
                        pePersonsPhoto.Size = PassedImage.Size;
                    }
                    pePersonsPhoto.Visible = true;

                }
                catch (Exception)
                {
                    pePersonsPhoto.Visible = false;
                    pePersonsPhoto.Image = null;
                    pePersonsPhoto.Size = new Size(20, 20);
                }
            }
        }

        public void ClearImageInImagePanel(string strDummy)
        {
            pePersonsPhoto.Visible = false;
            pePersonsPhoto.Image = null;
            pePersonsPhoto.Size = new Size(20, 20);
        }

        struct stcIDAndLookAndFeel
        {
            public DevExpress.LookAndFeel.ActiveLookAndFeelStyle lfsStyle;
            public string strLookAndFeelSkin;
        }

        private void bbiMagnifier_ItemClick(object sender, ItemClickEventArgs e)
        {
            stcIDAndLookAndFeel siflStruct = new stcIDAndLookAndFeel();

            siflStruct.strLookAndFeelSkin = this.LookAndFeel.ActiveSkinName;
            siflStruct.lfsStyle = this.LookAndFeel.ActiveStyle;

            try
            {
                if (tMagnifier != null)
                {
                    tMagnifier.Abort();
                    tMagnifier = null;
                }
                else
                {
                    tMagnifier = new Thread(new ParameterizedThreadStart(ShowMagnifier));
                    tMagnifier.Start(siflStruct);
                }
            }
            catch
            {
                if (tMagnifier != null)
                {
                    tMagnifier = null;
                }
            }
        }

        private void ShowMagnifier(object objIDAndLookAndFeel)
        {
            stcIDAndLookAndFeel siflStruct = (stcIDAndLookAndFeel)objIDAndLookAndFeel;

            MagnifierConfiguration magConfig = new MagnifierConfiguration();

            sp00068_Get_Magnifier_SettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00068_Get_Magnifier_SettingsTableAdapter.Fill(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings, 0, GlobalSettings.UserID, 0);

            if (this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows.Count > 0)
            {
                magConfig.LocationX = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["LocationX"]);
                magConfig.LocationY = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["LocationY"]);
                magConfig.CloseOnMouseUp = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["CloseOnMouseUp"]);
                magConfig.DoubleBuffered = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["DoubleBuffered"]);
                magConfig.HideMouseCursor = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["HideMouseCursor"]);
                magConfig.RememberLastPoint = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["RememberLastPoint"]);
                magConfig.ReturnToOrigin = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["ReturnToOrigin"]);
                magConfig.TopMostWindow = Convert.ToBoolean(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["TopMostWindow"]);
                magConfig.MagnifierWidth = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["MagnifierWidth"]);
                magConfig.MagnifierHeight = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["MagnifierHeight"]);
                magConfig.ZoomFactor = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["ZoomFactor"]);
                magConfig.SpeedFactor = Convert.ToDouble(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["SpeedFactor"]);
                magConfig.MagnifierShape = Convert.ToInt32(this.woodPlanDataSet.sp00068_Get_Magnifier_Settings.Rows[0]["MagnifierShape"]);
            }
            else
            {
                magConfig.LocationX = 0;
                magConfig.LocationY = 0;
                magConfig.CloseOnMouseUp = false;
                magConfig.DoubleBuffered = true;
                magConfig.HideMouseCursor = true;
                magConfig.RememberLastPoint = true;
                magConfig.ReturnToOrigin = false;
                magConfig.TopMostWindow = true;
                magConfig.MagnifierWidth = 150;
                magConfig.MagnifierHeight = 150;
                magConfig.ZoomFactor = 3;
                magConfig.SpeedFactor = 0.35f;
                magConfig.MagnifierShape = 0;
            }

            //BaseObjects.frmMagnifierController fmcController = new frmMagnifierController(magConfig, new Point(MousePosition.X, MousePosition.Y));
            //fmcController.ShowDialog();
            BaseObjects.frmMagnifyView fmvMagnify = new frmMagnifyView();
            fmvMagnify.LookAndFeel.SetStyle((DevExpress.LookAndFeel.LookAndFeelStyle)siflStruct.lfsStyle, false, false, siflStruct.strLookAndFeelSkin);
            fmvMagnify.TopMost = true;
            fmvMagnify.ShowDialog();
           
            // If we get here, then we should kill the thread...
            EndMagnifierThreadCallback d = new EndMagnifierThreadCallback(EndMagnifierThread);
            this.Invoke(d, new object[] { });
        }

        delegate void EndMagnifierThreadCallback();

        void EndMagnifierThread()
        {
            try
            {
                if (tMagnifier != null)
                {
                    tMagnifier.Abort();
                    tMagnifier = null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void ChangeLoggedInUser()
        {
            strConnectionString = set.WoodPlanConnectionString;

            sp00019GetUserDetailsForStartUpTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00019GetUserDetailsForStartUpTableAdapter.Fill(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp, set.UserName);

            // Changing login account...
            intStaffID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
            GlobalSettings.UserID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserID"]);
            GlobalSettings.UserType1 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType1"].ToString();
            GlobalSettings.UserSurname = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserSurname"].ToString();
            GlobalSettings.UserForename = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserForename"].ToString();
            GlobalSettings.Username = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUsername"].ToString().ToLower();
            GlobalSettings.ToDoList = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intToDoList"]);
            GlobalSettings.CommentBank = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intCommentBank"]);
            GlobalSettings.ShowTipsOnStart = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowTipsOnStart"]);
            GlobalSettings.ShowFormOnStart = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intDefaultForm"]);
            GlobalSettings.UserScreenConfig = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intUserConfig"]);
            GlobalSettings.ToolTips = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowToolTips"]);
            GlobalSettings.ContactGroups = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intContactGroups"]);
            GlobalSettings.ShowConfirmations = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intShowConfirmations"]);
            GlobalSettings.UserType2 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType2"].ToString();
            GlobalSettings.UserType3 = this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["strUserType3"].ToString();
            GlobalSettings.LiveStartDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtLivePeriodStart"]);
            GlobalSettings.LiveEndDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtLivePeriodEnd"]);
            GlobalSettings.ViewedStartDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtViewedPeriodStart"]);
            GlobalSettings.ViewedEndDate = Convert.ToDateTime(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["dtViewedPeriodEnd"]);
            GlobalSettings.PersonType = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intPersonType"]);
            GlobalSettings.LivePeriodID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intLivePeriodID"]);
            GlobalSettings.ViewedPeriodID = Convert.ToInt32(this.woodPlanDataSet.sp00019GetUserDetailsForStartUp.Rows[0]["intViewedPeriodID"]);

            if (GlobalSettings.LivePeriodID == GlobalSettings.ViewedPeriodID)
            {
                GlobalSettings.DataEntryOff = false;
            }
            else
            {
                GlobalSettings.DataEntryOff = true;
            }

            iCurrentUser.Caption = GlobalSettings.Username;

            if (iCurrentUser.Caption != Environment.UserName)
            {
                iCurrentUser.Appearance.Options.UseFont = true;
            }
            else
            {
                iCurrentUser.Appearance.Options.UseFont = false;
            }

            beiLivePeriod.EditValue = this.GlobalSettings.LivePeriodID;
            beiViewedPeriod.EditValue = this.GlobalSettings.ViewedPeriodID;

            this.sp00020GetUserApplicationSettingsTableAdapter.Connection.ConnectionString = strConnectionString;
            this.sp00020GetUserApplicationSettingsTableAdapter.Fill(this.woodPlanDataSet.sp00020GetUserApplicationSettings, intStaffID);

            if (this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows.Count > 0)
            {
                switch (Convert.ToInt32(this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ActiveLookAndFeel"]))
                {
                    case 0:
                        defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
                        break;
                    case 1:
                        defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
                        break;
                    case 2:
                        defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
                        break;
                    case 3:
                        defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
                        break;
                    case 4:
                        defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Skin;
                        break;
                }

                this.defaultLookAndFeel1.LookAndFeel.SkinName = this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ActiveSkinName"].ToString();

                int intColour1 = Convert.ToInt32(woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["SkinMaskColor1"]);
                int intColour2 = Convert.ToInt32(woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["SkinMaskColor2"]);
                if (intColour1 != 0)
                {
                    Color color1 = Color.FromArgb(intColour1);
                    Color color2 = Color.FromArgb(intColour2);
                    this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor = color1;
                    if (intColour2 != 0) this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor2 = color2;
                }

                intApplicationConfigID = Convert.ToInt32(this.woodPlanDataSet.sp00020GetUserApplicationSettings.Rows[0]["ApplicationConfigID"]);
            }

            if (strFriendlyName != "Training Database")
            {
                // bsiTraining.Dispose();
            }

            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.woodPlanDataSet.sp00039GetFormPermissionsForUser, 0, intStaffID, 0,
                                                              this.GlobalSettings.ViewedPeriodID);

            ProcessPermissionsForForm();

            dpActiveForm.Height = 300;

            sp00001PopulateSwitchboardTableAdapter.Connection.ConnectionString = strConnectionString;
        }
        
        public void UpdateFontSettings(string strFontName, int intFontSize)
        {
            if (strFontName != "")
            {
                if (DevExpress.Utils.AppearanceObject.DefaultFont.Name != strFontName ||
                DevExpress.Utils.AppearanceObject.DefaultFont.Size != intFontSize)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("A change to Font Settings requires Information Centre to be restarted.", "Information Centre Settings", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                DevExpress.Utils.AppearanceObject.DefaultFont = new Font(strFontName, intFontSize);
            }
            else
            {
                DevExpress.Utils.AppearanceObject.DefaultFont = new Font("Tahoma", 8);
            }

            beiLivePeriod.Edit.AppearanceDisabled.Font = new Font(strFontName, intFontSize);
            beiViewedPeriod.Edit.AppearanceDisabled.Font = new Font(strFontName, intFontSize);
            beiLivePeriod.Edit.AppearanceFocused.Font = new Font(strFontName, intFontSize);
            beiViewedPeriod.Edit.AppearanceFocused.Font = new Font(strFontName, intFontSize);
            beiLivePeriod.Edit.AppearanceReadOnly.Font = new Font(strFontName, intFontSize);
            beiViewedPeriod.Edit.AppearanceReadOnly.Font = new Font(strFontName, intFontSize);
            beiLivePeriod.Edit.Appearance.Font = new Font(strFontName, intFontSize);
            beiViewedPeriod.Edit.Appearance.Font = new Font(strFontName, intFontSize);
        }

        private void nbcSwitchboard_CustomDrawGroupCaption(object sender, DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventArgs e)
        {
            e.Appearance.Options.UseFont = true;
            e.Appearance.Font = DevExpress.Utils.AppearanceObject.DefaultFont;
        }

        private void nbcSwitchboard_CustomDrawLink(object sender, DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventArgs e)
        {
            e.Appearance.Options.UseFont = true;
            e.Appearance.Font = DevExpress.Utils.AppearanceObject.DefaultFont;               
        }

        private void nbcActiveForm_CustomDrawLink(object sender, DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventArgs e)
        {
            e.Appearance.Options.UseFont = true;
            e.Appearance.Font = DevExpress.Utils.AppearanceObject.DefaultFont;
        }

        private void nbcActiveForm_CustomDrawGroupCaption(object sender, DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventArgs e)
        {
            e.Appearance.Options.UseFont = true;
            e.Appearance.Font = DevExpress.Utils.AppearanceObject.DefaultFont;
        }

        private void nbcReports_CustomDrawLink(object sender, DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventArgs e)
        {
            e.Appearance.Options.UseFont = true;
            e.Appearance.Font = DevExpress.Utils.AppearanceObject.DefaultFont;
        }

        private void nbcReports_CustomDrawGroupCaption(object sender, DevExpress.XtraNavBar.ViewInfo.CustomDrawNavBarElementEventArgs e)
        {
            e.Appearance.Options.UseFont = true;
            e.Appearance.Font = DevExpress.Utils.AppearanceObject.DefaultFont;
        }

        private void dockManager1_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            /*if (e.Panel.Name == "dpClipboard")
            {
                MethodInfo method = e.Panel.Controls[0].Controls[0].GetType().GetMethod("SaveClips");
                if (method != null)
                {
                    method.Invoke(e.Panel.Controls[0].Controls[0], new object[] { });
                }
            }*/
        }

        private void iAbout_ItemClick(object sender, ItemClickEventArgs e)
        {
            frmAbout frmChild = new frmAbout();
            frmChild.ShowDialog();
        }

        private void bbiVisitWebSite_ItemClick(object sender, ItemClickEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.ground-control.co.uk/");
        }

        private void bbiColourMixer_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (DevExpress.XtraEditors.ColorWheel.ColorWheelForm fColorMixer = new DevExpress.XtraEditors.ColorWheel.ColorWheelForm())
            {
                fColorMixer.ShowDialog(this);
            }
        }



    }
}

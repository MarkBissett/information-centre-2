using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frmGroupsManager : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        WaitDialogForm loadingForm;
        private int i_int_FocusedGrid = 1;
        public bool UpdateRefreshStatus = false; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        public RefreshGridState RefreshGridViewState;  // Used by Grid View State Facilities //

        //BaseObjects.GridCheckMarksSelection selection1;
        #endregion

        public frmGroupsManager()
        {
            InitializeComponent();
        }

        private void frmGroupsManager_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            this.LockThisWindow();  // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_Groups.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00073_permission_group_listTableAdapter.Connection.ConnectionString = strConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            sp00074_permission_group_membersTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            sp00075_permission_group_non_membersTableAdapter.Connection.ConnectionString = strConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            ProcessPermissionsForForm();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            RefreshGridViewState = new RefreshGridState(gridView1, "GroupID");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            gridControl1.BeginUpdate();
            LoadData();
            gridControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
        }


        public void UpdateFormRefreshStatus(bool status)
        {
            UpdateRefreshStatus = status;
        }

        private void LoadData()
        {
            if (UpdateRefreshStatus) UpdateRefreshStatus = false;
            sp00073_permission_group_listTableAdapter.Fill(this.dataSet_Groups.sp00073_permission_group_list, 0);  // 0 = All Groups //
            this.RefreshGridViewState.LoadViewInfo();  // Reload any expanded groups and selected rows //
        }

        private void frmGroupsManager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus)
            {
                gridControl1.BeginUpdate();
                LoadData();
                gridControl1.EndUpdate();
            }
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;


            alItems.AddRange(new string[] { "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            if (i_int_FocusedGrid == 1)
            {
                GridView view = (GridView)gridControl1.MainView;
                int[] intRowHandles;
                intRowHandles = view.GetSelectedRows();

                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length > 0)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length > 1)
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }
                else
                {
                    bbiBlockEdit.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length > 0)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            if (!iBool_AllowAdd) return;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Groups - so open Group Edit screen //
                    this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State //
                    frmGroupManagerEditGroup fChildForm = new frmGroupManagerEditGroup();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = "frmGroupsManager";
                    fChildForm.intRecordCount = 0;
                    fChildForm.boolAllowEdit = true;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.Show();
                    break;
                default:
                    break;
            }
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();
            if (!iBool_AllowAdd) return;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Groups - so open Group Edit screen //
                    string strRecordIDs = "";
                    int[] intRowHandles;
                    int intCount = 0;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 1)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GroupID")) + ',';
                    }
                    this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State //
                    frmGroupManagerEditGroup fChildForm = new frmGroupManagerEditGroup();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "blockedit";
                    fChildForm.strCaller = "frmGroupsManager";
                    fChildForm.intRecordCount = intCount;
                    fChildForm.boolAllowEdit = true;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.Show();
                    break;
                default:
                    break;
            }
        }
        
        public override void OnEditEvent(object sender, EventArgs e)
        {
            EditRecords();
        }

        private void EditRecords()
        {
            GridView view = null;
            if (!iBool_AllowAdd) return;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Groups - so open Group Edit screen //
                    view = (GridView)gridControl1.MainView;
                    //view.PostEditor();
                    string strRecordIDs = "";
                    int[] intRowHandles;
                    int intCount = 0;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    loadingForm = new WaitDialogForm("Loading Data...", "Edit");

                    foreach (int intRowHandle in intRowHandles)
                    {
                        strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GroupID")) + ',';
                    }
                    this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State //
                    frmGroupManagerEditGroup fChildForm = new frmGroupManagerEditGroup();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs;
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = "frmGroupsManager";
                    fChildForm.intRecordCount = intCount;
                    fChildForm.boolAllowEdit = true;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.loadingForm = loadingForm;
                    fChildForm.Show();
                    break;
                default:
                    break;
            }
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            if (i_int_FocusedGrid != 1 || !iBool_AllowDelete) return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Permision Groups to delete by clicking on them then try again.", "Delete Permission Group", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            string strMessage = "";
            if (intCount == 1)
            {
                strMessage = "You have 1 Permission Group selected for delete.\n\nAll staff in the selected group will be removed from their group during this process - this may effect permissions for these staff!\n\nProceed?";
            }
                else
            {
                strMessage = "You have " + Convert.ToString(intRowHandles.Length) + " Permission Groups selected for delete.\n\nAll staff in the selected groups will be removed from their group during this process - this may effect permissions for these staff!\n\nProceed?";
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete Permission Group", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                string strGroupIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    strGroupIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "GroupID")) + ",";
                }
                DataSet_GroupsTableAdapters.QueriesTableAdapter RemoveGroups = new DataSet_GroupsTableAdapters.QueriesTableAdapter();
                RemoveGroups.ChangeConnectionString(strConnectionString);
                this.RefreshGridViewState.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                gridControl1.BeginUpdate();
                RemoveGroups.sp00077_permission_group_delete("permissiongroup", strGroupIDs);  // Remove the records from the DB in one go //
                LoadData();
                gridControl1.EndUpdate();
                if (this.GlobalSettings.ShowConfirmations == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete Permission Group", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Permission Groups Available";
                    break;
                case "gridView2":
                    message = "No Staff Available";
                    break;
                case "gridView3":
                    message = "No Staff Available";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadGroupMembers();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region PermissionGroupsGrid

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit) EditRecords();
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void LoadGroupMembers()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedGroups = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedGroups += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GroupID"])) + ',';
            }
            gridControl2.MainView.BeginUpdate();
            gridControl3.MainView.BeginUpdate();
            sp00075_permission_group_non_membersTableAdapter.Fill(this.dataSet_Groups.sp00075_permission_group_non_members, strSelectedGroups);
            sp00074_permission_group_membersTableAdapter.Fill(this.dataSet_Groups.sp00074_permission_group_members, strSelectedGroups);
            gridControl2.MainView.EndUpdate();
            gridControl3.MainView.EndUpdate();
        }

        #endregion


        #region SelectedMembersGrid

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            RemoveOne();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region AvailableMembersGrid

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            AddOne();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void btnAddOne1_Click(object sender, EventArgs e)
        {
            AddOne();
        }
 
        private void btnAddAll1_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int intCount = view.RowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("There are no staff to add to the selected group(s).", "Add All Staff to Selected Group(s)" , MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedStaff = "";
            for (int i = 0; i < intCount; i++)
            {
                strSelectedStaff += Convert.ToString(view.GetRowCellValue(i, view.Columns["StaffID"])) + ',';

            }
            view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more permission groups to add to before proceeding.", "Add All Staff to Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedGroups = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedGroups += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GroupID"])) + ',';
            }
            AddGroupMembers(strSelectedStaff, strSelectedGroups);
        }

        private void AddOne()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more staff to add to the selected group(s) before proceeding.", "Add Selected Staff to Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedStaff = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedStaff += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["StaffID"])) + ',';
            }
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more permission groups to add to before proceeding.", "Add Selected Staff to Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedGroups = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedGroups += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GroupID"])) + ',';
            }
            AddGroupMembers(strSelectedStaff, strSelectedGroups);
        }
        
        private void AddGroupMembers(string strSelectedStaff, string strSelectedGroups)
        {
            using (DataSet_GroupsTableAdapters.QueriesTableAdapter AddGroupMembers = new DataSet_GroupsTableAdapters.QueriesTableAdapter())
            {
                // Add the selected staff to the selected groups //
                AddGroupMembers.ChangeConnectionString(strConnectionString);
                AddGroupMembers.sp00076_permission_group_add_members(strSelectedGroups, strSelectedStaff);
            }
            gridControl2.MainView.BeginUpdate();   // Relaod Available and Selected Group Members lists
            gridControl3.MainView.BeginUpdate();
            sp00075_permission_group_non_membersTableAdapter.Fill(this.dataSet_Groups.sp00075_permission_group_non_members, strSelectedGroups);
            sp00074_permission_group_membersTableAdapter.Fill(this.dataSet_Groups.sp00074_permission_group_members, strSelectedGroups);
            gridControl2.MainView.EndUpdate();
            gridControl3.MainView.EndUpdate();
        }

        private void btnRemoveOne1_Click(object sender, EventArgs e)
        {
            RemoveOne();
        }

        private void btnRemoveAll1_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            int intCount = view.RowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("There are no staff to remove from the selected group(s).", "Remove All Staff from Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedStaff = "";
            for (int i = 0; i < intCount; i++)
            {
                strSelectedStaff += Convert.ToString(view.GetRowCellValue(i, view.Columns["GroupMemberID"])) + ',';

            }
            RemoveGroupMembers(strSelectedStaff);
        }

        private void RemoveOne()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more staff to remove from the selected group(s) before proceeding.", "Remove Selected Staff from Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedStaff = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedStaff += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GroupMemberID"])) + ',';
            }
            RemoveGroupMembers(strSelectedStaff);
        }

        private void RemoveGroupMembers(string strSelectedGroupMembers)
        {
            using (DataSet_GroupsTableAdapters.QueriesTableAdapter RemoveGroupMembers = new DataSet_GroupsTableAdapters.QueriesTableAdapter())
            {
                // Add the selected staff to the selected groups //
                RemoveGroupMembers.ChangeConnectionString(strConnectionString);
                RemoveGroupMembers.sp00077_permission_group_delete("permissiongroupmember", strSelectedGroupMembers);
            }
            LoadGroupMembers();   // Relaod Available and Selected Group Members lists
        }







    }
}


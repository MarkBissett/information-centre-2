using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_AT_Linked_Document_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public int intRecordTypeID = 0;  // 1 = Districts, 2 = Localities, 3 = Trees, 4 = Inspections, 5 = Actions, 6 = Incidents, 7 = Work Orders //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //
        #endregion

        public frm_AT_Linked_Document_Edit()
        {
            InitializeComponent();
        }

        private void frm_AT_Linked_Document_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 20033;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(1, "AmenityTreesLinkedDocumentsPath").ToString();
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                } 
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            try
            {
                sp00224_Linked_Document_Linked_Record_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp00224_Linked_Document_Linked_Record_TypesTableAdapter.Fill(this.dataSet_AT.sp00224_Linked_Document_Linked_Record_Types);

                sp01016_Core_Linked_Document_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp01016_Core_Linked_Document_TypesTableAdapter.Fill(this.dataSet_Common_Functionality.sp01016_Core_Linked_Document_Types, 1);
            }
            catch (Exception) { };

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            // Populate Main Dataset //
            this.sp00221_Linked_Document_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT.sp00221_Linked_Document_Item.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["LinkedDocumentID"] = 0;
                        drNewRow["LinkedToRecordID"] = intLinkedToRecordID;
                        drNewRow["LinkedRecordDescription"] = strLinkedToRecordDesc;
                        drNewRow["LinkedToRecordTypeID"] = intRecordTypeID;
                        drNewRow["AddedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["DocumentTypeID"] = 0;
                        string strPath = "";
                        try
                        {
                            DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter GetSetting = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                            GetSetting.ChangeConnectionString(strConnectionString);
                            strPath = GetSetting.sp01015_Get_Linked_Document_Folder_Path(intRecordTypeID).ToString();
                            drNewRow["FolderLocation"] = strPath;
                        }
                        catch (Exception)
                        {
                        }

                        this.dataSet_AT.sp00221_Linked_Document_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT.sp00221_Linked_Document_Item.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["LinkedDocumentID"] = 0;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedRecordDescription"] = "Not Applicable - Block Adding";
                        drNewRow["LinkedToRecordTypeID"] = intRecordTypeID;
                        drNewRow["AddedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["DocumentTypeID"] = 0;
                        string strPath = "";
                        try
                        {
                            DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter GetSetting = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                            GetSetting.ChangeConnectionString(strConnectionString);
                            strPath = GetSetting.sp01015_Get_Linked_Document_Folder_Path(intRecordTypeID).ToString();
                            drNewRow["FolderLocation"] = strPath;
                        }
                        catch (Exception)
                        {
                        }

                        this.dataSet_AT.sp00221_Linked_Document_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_AT.sp00221_Linked_Document_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["LinkedDocumentID"] = 0;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedToRecordTypeID"] = 0;
                        this.dataSet_AT.sp00221_Linked_Document_Item.Rows.Add(drNewRow);
                        this.dataSet_AT.sp00221_Linked_Document_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                    try
                    {
                        sp00221_Linked_Document_ItemTableAdapter.Fill(this.dataSet_AT.sp00221_Linked_Document_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (this.dataSet_AT.sp00221_Linked_Document_Item.Rows.Count == 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        buttonEdit1.Focus();

                        buttonEdit1.Properties.ReadOnly = false;
                        buttonEdit1.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        buttonEdit1.Focus();

                        buttonEdit1.Properties.ReadOnly = true;
                        buttonEdit1.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        DescriptionTextEdit.Focus();

                        buttonEdit1.Properties.ReadOnly = false;
                        buttonEdit1.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        DescriptionTextEdit.Focus();

                        buttonEdit1.Properties.ReadOnly = true;
                        buttonEdit1.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_AT.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_AT_Linked_Document_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_AT_Linked_Document_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp00221LinkedDocumentItemBindingSource.EndEdit();
            try
            {
                this.sp00221_Linked_Document_ItemTableAdapter.Update(dataSet_AT);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["LinkedDocumentID"]) + ";";
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                switch (strCaller)
                {
                    case "frm_AT_Linked_Document_Manager":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_AT_Linked_Document_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_AT_Linked_Document_Manager")
                                {
                                    fParentForm = (frm_AT_Linked_Document_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_AT_Tree_Manager":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_AT_Tree_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_AT_Tree_Manager")
                                {
                                    fParentForm = (frm_AT_Tree_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_AT_Tree_Edit":
                    case "frm_AT_Inspection_Edit":
                        {
                            // Notify any open forms which reference this data that they will need to refresh their data on activating //
                            Broadcast_Data_Refresh Broadcast = new Broadcast_Data_Refresh();
                            Broadcast.LinkedDocument_Refresh(this.ParentForm, this.Name, strNewIDs);
                        }
                        break;
                    case "frm_AT_Inspection_Manager":
                        {
                            frm_AT_Inspection_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_AT_Inspection_Manager")
                                {
                                    fParentForm = (frm_AT_Inspection_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_AT_Action_Manager":
                        {
                            frm_AT_Action_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_AT_Action_Manager")
                                {
                                    fParentForm = (frm_AT_Action_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_AT_WorkOrder_Manager":
                        {
                            frm_AT_WorkOrder_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_AT_WorkOrder_Manager")
                                {
                                    fParentForm = (frm_AT_WorkOrder_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_AT_Incident_Manager":
                        {
                            frm_AT_Incident_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_AT_Incident_Manager")
                                {
                                    fParentForm = (frm_AT_Incident_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;

                    case "frm_Core_Client_Manager":
                        {
                            frm_Core_Client_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_Core_Client_Manager")
                                {
                                    fParentForm = (frm_Core_Client_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs, "", "", "", "", "", "");
                                }
                            }
                        }
                        break;
                    case "frm_Core_Site_Manager":
                        {
                            frm_Core_Site_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_Core_Site_Manager")
                                {
                                    fParentForm = (frm_Core_Site_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs, "", "", "");
                                }
                            }
                        }
                        break;
                    case "frm_EP_Asset_Manager":
                        {
                            frm_EP_Asset_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_EP_Asset_Manager")
                                {
                                    fParentForm = (frm_EP_Asset_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(3, "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_EP_Action_Manager":
                        {
                            frm_EP_Action_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_EP_Action_Manager")
                                {
                                    fParentForm = (frm_EP_Action_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_EP_Action_Edit":
                        {
                            frm_EP_Action_Edit fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_EP_Action_Edit")
                                {
                                    fParentForm = (frm_EP_Action_Edit)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_EP_Site_Inspection_Manager":
                        {
                            frm_EP_Site_Inspection_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_EP_Site_Inspection_Manager")
                                {
                                    fParentForm = (frm_EP_Site_Inspection_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_GC_Company_Manager":
                        {
                            frm_GC_Company_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_Company_Manager")
                                {
                                    fParentForm = (frm_GC_Company_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_GC_Gritting_Contract_Manager":
                        {
                            frm_GC_Gritting_Contract_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_Gritting_Contract_Manager")
                                {
                                    fParentForm = (frm_GC_Gritting_Contract_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs, "", "");
                                }
                            }
                        }
                        break;
                    case "frm_GC_Snow_Contract_Manager":
                        {
                            frm_GC_Snow_Contract_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_Snow_Contract_Manager")
                                {
                                    fParentForm = (frm_GC_Snow_Contract_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs, "", "", "");
                                }
                            }
                        }
                        break;
                    case "frm_GC_Salt_Supplier_Manager":
                        {
                            frm_GC_Salt_Supplier_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_Salt_Supplier_Manager")
                                {
                                    fParentForm = (frm_GC_Salt_Supplier_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_GC_Salt_Depot_Manager":
                        {
                            frm_GC_Salt_Depot_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_Salt_Depot_Manager")
                                {
                                    fParentForm = (frm_GC_Salt_Depot_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_GC_Salt_Transfer_Manager":
                        {
                            frm_GC_Salt_Transfer_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_Salt_Transfer_Manager")
                                {
                                    fParentForm = (frm_GC_Salt_Transfer_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_GC_Salt_Order_Manager":
                        {
                            frm_GC_Salt_Order_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_Salt_Order_Manager")
                                {
                                    fParentForm = (frm_GC_Salt_Order_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs, "");
                                }
                            }
                        }
                        break;
                    case "frm_Core_CRM_Manager":
                        {
                            frm_Core_CRM_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_Core_CRM_Manager")
                                {
                                    fParentForm = (frm_Core_CRM_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_GC_SUM_Issue_Manager":
                        {
                            frm_GC_SUM_Issue_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_SUM_Issue_Manager")
                                {
                                    fParentForm = (frm_GC_SUM_Issue_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_GC_SUM_Permit_Manager":
                        {
                            frm_GC_SUM_Permit_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_GC_SUM_Permit_Manager")
                                {
                                    fParentForm = (frm_GC_SUM_Permit_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_UT_Circuit_Manager":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_UT_Circuit_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Circuit_Manager")
                                {
                                    fParentForm = (frm_UT_Circuit_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_UT_Pole_Manager":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_UT_Pole_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Pole_Manager")
                                {
                                    fParentForm = (frm_UT_Pole_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", strNewIDs, "", "", "", "", "", "");
                                }
                            }
                        }
                        break;
                    case "frm_UT_Survey_Manager":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_UT_Survey_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Survey_Manager")
                                {
                                    fParentForm = (frm_UT_Survey_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs, "");
                                }
                            }
                        }
                        break;
                    case "frm_UT_Survey_Pole2":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_UT_Survey_Pole2 fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Survey_Pole2")
                                {
                                    fParentForm = (frm_UT_Survey_Pole2)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(20, "", "", "", "", "", "", "", "", "", "", "", "", "", "", strNewIDs, "", "");
                                }
                            }
                        }
                        break;
                    case "frm_UT_Permission_Document_Manager":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_UT_Permission_Document_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Permission_Document_Manager")
                                {
                                    fParentForm = (frm_UT_Permission_Document_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs, "");
                                }
                            }
                        }
                        break;
                    case "frm_UT_Permission_Doc_Edit_WPD":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_UT_Permission_Doc_Edit_WPD fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Permission_Doc_Edit_WPD")
                                {
                                    fParentForm = (frm_UT_Permission_Doc_Edit_WPD)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(4, "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_UT_Permission_Doc_Edit_UKPN":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_UT_Permission_Doc_Edit_UKPN fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_Permission_Doc_Edit_UKPN")
                                {
                                    fParentForm = (frm_UT_Permission_Doc_Edit_UKPN)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(4, "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_UT_WorkOrder_Manager":
                        {  // Outer braces to allow fParentForm to be declared in each case statement without generating compiler errors //
                            frm_UT_WorkOrder_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_UT_WorkOrder_Manager")
                                {
                                    fParentForm = (frm_UT_WorkOrder_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", strNewIDs, "", "");
                                }
                            }
                        }
                        break;
                    case "frm_Core_Staff_Manager":
                        {
                            frm_Core_Staff_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_Core_Staff_Manager")
                                {
                                    fParentForm = (frm_Core_Staff_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", "", "", strNewIDs);
                                }
                            }
                        }
                        break;
                    case "frm_Core_Contractor_Manager":
                        {
                            frm_Core_Contractor_Manager fParentForm;
                            foreach (Form frmChild in this.ParentForm.MdiChildren)
                            {
                                if (frmChild.Name == "frm_Core_Contractor_Manager")
                                {
                                    fParentForm = (frm_Core_Contractor_Manager)frmChild;
                                    fParentForm.UpdateFormRefreshStatus(2, "", "", "", "", "", "", "", "", "", "", "", strNewIDs);
                                }
                            }
                        }
                        break;

                    case "frm_AS_Equipment_Manager":
                        {
                            if (Application.OpenForms[strCaller] != null)
                            {
                                (Application.OpenForms[strCaller] as frm_AS_Equipment_Manager).UpdateFormRefreshStatus(2, "", strRecordIDs, "");
                            }
                        }
                        break;
                    case "frm_AS_Fleet_Manager":
                        {
                            if (Application.OpenForms[strCaller] != null)
                            {
                                (Application.OpenForms[strCaller] as frm_AS_Fleet_Manager).UpdateFormRefreshStatus(2, "", strRecordIDs, "");
                            }
                        }
                        break;
                    case "frm_AS_Office_Manager":
                        {
                            if (Application.OpenForms[strCaller] != null)
                            {
                                (Application.OpenForms[strCaller] as frm_AS_Office_Manager).UpdateFormRefreshStatus(2, "", strRecordIDs, "");
                            }
                        }
                        break;
                    case "frm_AS_Gadget_Manager":
                        {
                            if (Application.OpenForms[strCaller] != null)
                            {
                                (Application.OpenForms[strCaller] as frm_AS_Gadget_Manager).UpdateFormRefreshStatus(2, "", strRecordIDs, "");
                            }
                        }
                        break;
                    case "frm_AS_Hardware_Manager":
                        {
                            if (Application.OpenForms[strCaller] != null)
                            {
                                (Application.OpenForms[strCaller] as frm_AS_Hardware_Manager).UpdateFormRefreshStatus(2, "", strRecordIDs, "");
                            }
                        }
                        break;
                    case "frm_AS_Rental_Manager":
                        {
                            if (Application.OpenForms[strCaller] != null)
                            {
                                (Application.OpenForms[strCaller] as frm_AS_Rental_Manager).UpdateFormRefreshStatus(2, "", strRecordIDs, "");
                            }
                        }
                        break;
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_AT.sp00221_Linked_Document_Item.Rows.Count; i++)
            {
                switch (this.dataSet_AT.sp00221_Linked_Document_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void buttonEdit1_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit glue = (ButtonEdit)sender;
            string strValue = glue.EditValue.ToString();
            if (this.strFormMode != "blockadd" && this.strFormMode != "blockedit" && String.IsNullOrEmpty(strValue))
            {
                dxErrorProvider1.SetError(buttonEdit1, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(buttonEdit1, "");
            }
        }
        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            switch (intRecordTypeID)
            {
                case 1:  // Clients // 
                    {
                        var fChildForm1 = new frm_EP_Select_Client();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedClientID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedClientName;
                            }
                        }
                    }
                    break;
                case 2:  // Sites //     
                    {
                        var fChildForm = new frm_Core_Select_Site();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                            }
                        }
                    }
                    break;
                case 3:  // Trees //          
                    {
                        frm_AT_Select_Tree fChildForm3 = new frm_AT_Select_Tree();
                        fChildForm3.GlobalSettings = this.GlobalSettings;
                        fChildForm3.intFilterUtilityArbClient = 1;  // Show only clients with UtilityARB ticked //
                        if (fChildForm3.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm3.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm3.strSelectedValue;
                            }
                        }
                    }
                    break;
                case 4:  // Inspections //   
                    {
                        frm_AT_Select_Inspection fChildForm4 = new frm_AT_Select_Inspection();
                        fChildForm4.GlobalSettings = this.GlobalSettings;
                        fChildForm4.intAmenityArbClient = 1;
                        if (fChildForm4.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm4.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm4.strSelectedValue;
                            }
                        }
                    }
                    break;
                case 5:  // Actions // 
                    {
                        frm_AT_Select_Action fChildForm5 = new frm_AT_Select_Action();
                        fChildForm5.GlobalSettings = this.GlobalSettings;
                        fChildForm5.intAmenityArbClient = 1;
                        if (fChildForm5.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm5.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm5.strSelectedValue;
                            }
                        }
                    }
                    break;
                case 6:  // Incidents //
                    {
                        frm_AT_Select_Incident fChildForm6 = new frm_AT_Select_Incident();
                        fChildForm6.GlobalSettings = this.GlobalSettings;
                        if (fChildForm6.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm6.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm6.strSelectedValue;
                            }
                        }
                    }
                    break;
                case 7:  // Work Orders //
                    {
                        frm_AT_Select_Work_Order fChildForm7 = new frm_AT_Select_Work_Order();
                        fChildForm7.GlobalSettings = this.GlobalSettings;
                        if (fChildForm7.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm7.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm7.strSelectedValue;
                            }
                        }
                    }
                    break;
                    
                case 8:  // Clients //
                    {
                        DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                        frm_EP_Select_Client fChildForm = new frm_EP_Select_Client();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (currentRow != null)
                        {
                            int intOriginalValue = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
                            fChildForm.intOriginalClientID = intOriginalValue;
                        }
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedClientID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedClientName;
                            }
                        }
                    }
                    break;
                case 9:  // Site //
                    {
                        var fChildForm = new frm_Core_Select_Site();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                            }
                        }
                    }
                    break;
                case 10:  // Asset //
                    {
                        frm_EP_Select_Asset fChildForm = new frm_EP_Select_Asset();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                            }
                        }
                    }
                    break;
                case 11:  // Asset Action //
                    {
                        frm_EP_Select_Asset_Action fChildForm = new frm_EP_Select_Asset_Action();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                            }
                        }
                    }
                    break;
                case 12:  // Site Inspection //
                    {
                        DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                        frm_EP_Select_Inspection fChildForm = new frm_EP_Select_Inspection();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (currentRow != null)
                        {
                            int intOriginalValue = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
                            fChildForm.intOriginalInspectionID = intOriginalValue;
                        }
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedInspectionID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedInspectionNumber;
                            }
                        }
                    }
                    break;
                 case 13:  // Companies // 
                    {
                        frm_GC_Core_Select_Company fChildForm1 = new frm_GC_Core_Select_Company();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 14:  // Site Gritting Contracts // 
                    {
                        frm_GC_Select_Site_Gritting_Contract fChildForm1 = new frm_GC_Select_Site_Gritting_Contract();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 15:  // Site Snow Clearance Contracts // 
                    {
                        frm_GC_Select_Snow_Site_Contract fChildForm1 = new frm_GC_Select_Snow_Site_Contract();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 16:  // Grit Supplier // 
                    {
                        frm_GC_Select_Salt_Supplier fChildForm1 = new frm_GC_Select_Salt_Supplier();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 17:  // Grit Depot // 
                    {
                        frm_GC_Select_Salt_Depot fChildForm1 = new frm_GC_Select_Salt_Depot();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.intIncludeTeams = 0;
                        if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 18:  // Grit Transfer // 
                    {
                        frm_GC_Select_Salt_Transfer fChildForm1 = new frm_GC_Select_Salt_Transfer();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 19:  // Grit Order // 
                    {
                        frm_GC_Select_Salt_Order fChildForm1 = new frm_GC_Select_Salt_Order();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.Yes)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 23:  // CRM // 
                    {
                        frm_Core_Select_CRM fChildForm1 = new frm_Core_Select_CRM();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 25:  // Work Issue // 
                    {
                        frm_GC_Sum_Select_Issue fChildForm1 = new frm_GC_Sum_Select_Issue();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 26:  // Work Permit // 
                    {
                        frm_GC_SUM_Select_Permit fChildForm1 = new frm_GC_SUM_Select_Permit();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm1.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm1.strSelectedValue;
                            }
                        }
                    }
                    break;


                 case 50:  // Utilties Circuit //
                    {
                        frm_UT_Select_Circuit fChildForm = new frm_UT_Select_Circuit();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intFilterUtilityArbClient = 1;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 51:  // Utilties Pole //
                    {
                        frm_UT_Select_Pole fChildForm = new frm_UT_Select_Pole();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intFilterUtilityArbClient = 1;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 52:  // Utilties Survey //
                    {
                        frm_UT_Select_Survey fChildForm = new frm_UT_Select_Survey();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedSurveyID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 53:  // Utilties Permission Document //
                    {
                        frm_UT_Select_Permission_Document fChildForm = new frm_UT_Select_Permission_Document();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            int intOriginalValue = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
                            fChildForm.intOriginalSelectedID = intOriginalValue;
                        }
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                        }
                    }
                    break;
                 case 54:  // Utilties Work Order //
                    {
                        frm_UT_Select_Work_Order fChildForm = new frm_UT_Select_Work_Order();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            int intOriginalValue = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
                            fChildForm.intOriginalSelectedValue = intOriginalValue;
                        }
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                        }
                    }
                    break;
                    

                 case 100:  // Staff // 
                    {
                        frm_HR_Select_Staff fChildForm = new frm_HR_Select_Staff();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedStaffID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedStaffName;
                            }
                        }
                    }
                    break;
                 case 101:  // Team // 
                    {
                        frm_Core_Select_Contractor fChildForm = new frm_Core_Select_Contractor();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                            if (currentRow != null)
                            {
                                currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                                currentRow["LinkedRecordDescription"] = fChildForm.strSelectedValue;
                            }
                        }
                    }
                    break;
                 case 200:  // Fleet //
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("This functionality is not available when called from Fleet.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                 default:
                    break;
            }
        }

        private void DescriptionTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "");
            }

        }

        private void DocumentPathButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DocumentPathButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DocumentPathButtonEdit, "");
            }

        }
        private void DocumentPathButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
            if (currentRow == null) return;
            string strLinkedDocumentRecordTypePath = currentRow["FolderLocation"].ToString().ToLower();
            if (string.IsNullOrWhiteSpace(strLinkedDocumentRecordTypePath)) strLinkedDocumentRecordTypePath = strLinkedDocumentRecordTypePath.ToLower();
            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    {
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "tab";
                            dlg.Filter = "";
                            if (strLinkedDocumentRecordTypePath != "") dlg.InitialDirectory = strLinkedDocumentRecordTypePath;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                string strExtension = "";
                                
                                
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strLinkedDocumentRecordTypePath != "")
                                    {
                                        if (!filename.ToLower().StartsWith(strLinkedDocumentRecordTypePath.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the Linked Document Record Type table] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    }
                                    strTempFileName = filename.Substring(strLinkedDocumentRecordTypePath.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                    // Update Extension Field with extension from filename //
                                    strExtension = System.IO.Path.GetExtension(filename);
                                    if (!string.IsNullOrEmpty(strExtension))
                                    {
                                        currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                                        if (currentRow != null)
                                        {
                                            currentRow["DocumentExtension"] = strExtension.Substring(1).ToUpper();  // Ignore the "." at the start of the extension //
                                        }
                                    }
                                }
                                DocumentPathButtonEdit.Text = strTempResult;
                            }
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter so linked file can be viewed if required //
                        }
                    }
                    break;
                case "view file":
                    {
                        currentRow = (DataRowView)sp00221LinkedDocumentItemBindingSource.Current;
                        if (currentRow == null) return;
                        string strFile = currentRow["DocumentPath"].ToString();
                        string strExtension = currentRow["DocumentExtension"].ToString();
                                 
                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            if (strExtension.ToLower() != "pdf")
                            {
                                System.Diagnostics.Process.Start(Path.Combine(strLinkedDocumentRecordTypePath + strFile));
                            }
                            else
                            {
                                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                                {
                                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                    fChildForm.strPDFFile = Path.Combine(strLinkedDocumentRecordTypePath + strFile);
                                    fChildForm.MdiParent = this.MdiParent;
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.Show();
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(Path.Combine(strLinkedDocumentRecordTypePath + strFile));
                                }
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strLinkedDocumentRecordTypePath + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }

        #endregion






   
    }
}


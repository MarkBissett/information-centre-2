﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Linked_Document_Email : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        public string _SubjectLine = "";
        public string _MessageBody = "";
        public List<string> _FilesToEmail = new List<string>();
        public List<string> _EmailAddresses = new List<string>();
        public List<string> _EmailCCAddresses = new List<string>();
        public string _FromEmailAddress = "";
        #endregion

        public frm_Core_Linked_Document_Email()
        {
            InitializeComponent();
        }

        private void frm_Core_Linked_Document_Email_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 500113;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            foreach (string strEmailAddress in _EmailAddresses)
            {
                //EmailToMemoEdit.Text += strEmailAddress + ";\n";
                EmailToMemoEdit.Text += strEmailAddress + ";";
            }

            foreach (string strEmailAddress in _EmailCCAddresses)
            {
                //EmailCCToMemoEdit.Text += strEmailAddress + ";\n";
                EmailCCToMemoEdit.Text += strEmailAddress + ";";
            }

            EmailSubjectTextEdit.Text = _SubjectLine;
            MessageBodyRichEditControl.HtmlText = _MessageBody;

            foreach (string strFilename in _FilesToEmail)
            {
                LinkedDocumentsMemoEdit.Text += strFilename + "\n";
            }

            ribbonControl1.SelectedPage = homeRibbonPage1;

            PostOpen(null);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void bbiEmail_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(EmailToMemoEdit.Text.Trim()))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter the recipient for the email in the Email To box before proceeding.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Get DB Settings for generated Emails //
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp07216_UT_Get_Email_Settings", SQlConn);   // This SP called just so we can get the SMTP values - anything else is ignored //
            cmd.CommandType = CommandType.StoredProcedure;
            int intParameter = 0;  // 0 = Customer, 1 = Client //
            cmd.Parameters.Add(new SqlParameter("@RecipientTypeID", intParameter));
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
            DataSet dsSettings = new DataSet("NewDataSet");
            try
            {
                sdaSettings.Fill(dsSettings, "Table");
            }
            catch (Exception ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Email Settings (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Email Settings (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataRow dr1 = dsSettings.Tables[0].Rows[0];
            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);

            if (string.IsNullOrEmpty(strSMTPMailServerAddress))
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the email settings (SMTP Mail Server Name etc) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                msg.From = new System.Net.Mail.MailAddress(_FromEmailAddress);
                msg.Subject = EmailSubjectTextEdit.Text;

                char[] delimiters = new char[] { ';' };
                string[] strEmailTo = EmailToMemoEdit.Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strEmailTo.Length > 0)
                {
                    foreach (string strEmailAddress in strEmailTo)
                    {
                        if (!string.IsNullOrWhiteSpace(strEmailAddress.Trim())) msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress.Trim()));
                    }
                }
                else
                {
                    msg.To.Add(new System.Net.Mail.MailAddress(EmailToMemoEdit.Text.Trim()));  // Original value wouldn't split as no commas so it's just one email address so use it //
                }

                string[] strCCTo = EmailCCToMemoEdit.Text.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (strCCTo.Length > 0)
                {
                    foreach (string strEmailAddress in strCCTo)
                    {
                        if (!string.IsNullOrWhiteSpace(strEmailAddress.Trim())) msg.CC.Add(new System.Net.Mail.MailAddress(strEmailAddress.Trim()));
                    }
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(EmailCCToMemoEdit.Text)) msg.CC.Add(new System.Net.Mail.MailAddress(EmailCCToMemoEdit.Text.Trim()));  // Original value wouldn't split as no commas so it's just one email address so use it //
                }

                // Create a new attachment or each linked document ot be sent //
                foreach (string strFilename in _FilesToEmail)
                {
                    if (!File.Exists(strFilename))
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Unable to email linked document(s)...\n\nFile: " + strFilename + " does not exist in the specified location.\n\nEmail aborted. Please correct the linked document record before proceeding.", "Check Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    Attachment attachment = new Attachment(strFilename);
                    ContentDisposition disposition = attachment.ContentDisposition;
                    disposition.CreationDate = File.GetCreationTime(strFilename);
                    disposition.ModificationDate = File.GetLastWriteTime(strFilename);
                    disposition.ReadDate = File.GetLastAccessTime(strFilename);
                    disposition.FileName = Path.GetFileName(strFilename);
                    disposition.Size = new FileInfo(strFilename).Length;
                    disposition.DispositionType = DispositionTypeNames.Attachment;
                    msg.Attachments.Add(attachment);   
                }

                msg.Priority = System.Net.Mail.MailPriority.High;
                msg.IsBodyHtml = true;

                string strBody = MessageBodyRichEditControl.HtmlText;
                System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");
                msg.AlternateViews.Add(plainView);
                msg.AlternateViews.Add(htmlView);

                object userState = msg;
                System.Net.Mail.SmtpClient emailClient = null;
                if (intSMTPMailServerPort != 0)
                {
                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                }
                else
                {
                    emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                }
                if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                {
                    System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                    emailClient.UseDefaultCredentials = false;
                    emailClient.Credentials = basicCredential;
                }
                emailClient.SendAsync(msg, userState);
            }
            catch (Exception Ex)
            {
                if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Email Linked Documents - an error occurred [" + Ex.Message + "].\n\nTry again. If problems persists - contact Technical Support.", "Email Linked Documents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            DevExpress.XtraEditors.XtraMessageBox.Show("Linked Document(s) Emailed Successfully.", "Email Linked Documents", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }





    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;  // Mouse down Args for GridView Mouse Down //

namespace WoodPlan5
{
 
    public partial class frm_Core_Sequence_Selection : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        private Settings set = Settings.Default;

        public string PassedInTableName = "";
        public string PassedInFieldName = "";

        public int PassedInClientID = 0;
        public int PassedInSiteID = 0;

        public int SelectedSequenceID = 0;
        public string SelectedSequence = "";

        #endregion

        public frm_Core_Sequence_Selection()
        {
            InitializeComponent();
        }

        private void frmSequenceSelection_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            Set_Grid_Highlighter_Transparent(this.Controls);

            sp01247_AT_Sequence_List_FilteredTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01247_AT_Sequence_List_FilteredTableAdapter.Fill(dataSet_AT.sp01247_AT_Sequence_List_Filtered, PassedInTableName, PassedInFieldName);

            string strAllowedValues = ",AssetNumber,ActionNumber,InspectionNumber,";  // Values from stored proc: SP00165 - column strField //
            if (strAllowedValues.Contains("," + PassedInFieldName + ","))  // Show gridview2 (with Client and Site Name) //
            {
                gridControl1.MainView = gridView2;
            }
            else
            {
                gridControl1.MainView = gridView1;  // Show Gridview2 (No Client and Site Name) //
            }
            gridControl1.ForceInitialize();

            // Attempt to get a best match sequence [only works for Asset Management sequences] //
            GridView view = (GridView)gridControl1.DefaultView;
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            DevExpress.XtraGrid.Columns.GridColumn[] cols = new DevExpress.XtraGrid.Columns.GridColumn[] { colClientID, colSiteID };  // Array of columns to search for a match on //
            object[] values = new object[] { PassedInClientID, PassedInSiteID };  // Array of objects [values] to look for //
            int intFoundRow = extGridViewFunction.LocateRowByMultipleValues(view, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //
            if (intFoundRow != GridControl.InvalidRowHandle)
            {
                view.FocusedRowHandle = intFoundRow;
                view.MakeRowVisible(intFoundRow, false);
            }
            else  // No match so attempt a match just on Client and Generic Site (site equal to 0) //
            {
                PassedInSiteID = 0;
                values = new object[] { PassedInClientID, PassedInSiteID };  // Array of objects [values] to look for //
                intFoundRow = extGridViewFunction.LocateRowByMultipleValues(view, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
                else  // No match so attempt a match just on Generic Client and Generic Site (both equal to 0) //
                {
                    PassedInClientID = 0;
                    PassedInSiteID = 0;
                    values = new object[] { PassedInClientID, PassedInSiteID };  // Array of objects [values] to look for //
                    intFoundRow = extGridViewFunction.LocateRowByMultipleValues(view, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Sequence to use from the list before proceeding!", "Select Sequence", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LengthOfStringPart")) <= 0)
            {
                SelectedSequence = "";
            }
            else
            {
                SelectedSequence = view.GetRowCellValue(view.FocusedRowHandle, "Sequence").ToString();
            }
            SelectedSequenceID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SequenceID"));
            this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails validation test earlier in this process //
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No sequences available - use the Sequence Manager screen to create first";
                    break;
                case "gridView2":
                    message = "No sequences available - use the Sequence Manager screen to create first";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


 

    }
}


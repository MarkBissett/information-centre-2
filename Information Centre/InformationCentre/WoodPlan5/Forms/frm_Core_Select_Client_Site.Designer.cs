namespace WoodPlan5
{
    partial class frm_Core_Select_Client_Site
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Select_Client_Site));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActiveClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEditAllowSiteSelection = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp06021OMSelectClientFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Summer_Core = new WoodPlan5.DataSet_GC_Summer_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06022OMSelectSiteFilterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveClient1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiRefreshSites = new DevExpress.XtraBars.BarButtonItem();
            this.beiAllowSiteSelection = new DevExpress.XtraBars.BarEditItem();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barEditItemDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefreshClients = new DevExpress.XtraBars.BarButtonItem();
            this.sp06021_OM_Select_Client_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06021_OM_Select_Client_FilterTableAdapter();
            this.sp06022_OM_Select_Site_FilterTableAdapter = new WoodPlan5.DataSet_GC_Summer_CoreTableAdapters.sp06022_OM_Select_Site_FilterTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditAllowSiteSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06021OMSelectClientFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06022OMSelectSiteFilterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(678, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 516);
            this.barDockControlBottom.Size = new System.Drawing.Size(678, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 490);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(678, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 490);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beiAllowSiteSelection,
            this.bbiRefreshSites,
            this.barEditItemDateRange,
            this.bbiRefreshClients});
            this.barManager1.MaxItemId = 35;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActiveClient
            // 
            this.colActiveClient.Caption = "Active";
            this.colActiveClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActiveClient.FieldName = "ActiveClient";
            this.colActiveClient.Name = "colActiveClient";
            this.colActiveClient.OptionsColumn.AllowEdit = false;
            this.colActiveClient.OptionsColumn.AllowFocus = false;
            this.colActiveClient.OptionsColumn.ReadOnly = true;
            this.colActiveClient.Visible = true;
            this.colActiveClient.VisibleIndex = 2;
            this.colActiveClient.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEditAllowSiteSelection
            // 
            this.repositoryItemCheckEditAllowSiteSelection.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditAllowSiteSelection.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditAllowSiteSelection.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditAllowSiteSelection.AutoHeight = false;
            this.repositoryItemCheckEditAllowSiteSelection.Caption = "Allow Site Selection:";
            this.repositoryItemCheckEditAllowSiteSelection.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditAllowSiteSelection.LookAndFeel.SkinName = "Blue";
            this.repositoryItemCheckEditAllowSiteSelection.Name = "repositoryItemCheckEditAllowSiteSelection";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Clients";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel2.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Sites";
            this.splitContainerControl1.Size = new System.Drawing.Size(678, 455);
            this.splitContainerControl1.SplitterPosition = 195;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.DataSource = this.sp06021OMSelectClientFilterBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(653, 192);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp06021OMSelectClientFilterBindingSource
            // 
            this.sp06021OMSelectClientFilterBindingSource.DataMember = "sp06021_OM_Select_Client_Filter";
            this.sp06021OMSelectClientFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // dataSet_GC_Summer_Core
            // 
            this.dataSet_GC_Summer_Core.DataSetName = "DataSet_GC_Summer_Core";
            this.dataSet_GC_Summer_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName,
            this.colClientType,
            this.colActiveClient});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActiveClient;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            this.colClientID.Visible = true;
            this.colClientID.VisibleIndex = 3;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 291;
            // 
            // colClientType
            // 
            this.colClientType.Caption = "Client Type";
            this.colClientType.FieldName = "ClientType";
            this.colClientType.Name = "colClientType";
            this.colClientType.OptionsColumn.AllowEdit = false;
            this.colClientType.OptionsColumn.AllowFocus = false;
            this.colClientType.OptionsColumn.ReadOnly = true;
            this.colClientType.Visible = true;
            this.colClientType.VisibleIndex = 1;
            this.colClientType.Width = 158;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(115, 110);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(195, 107);
            this.popupContainerControlDateRange.TabIndex = 17;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 5);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(189, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date Range  [Contracts]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(144, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(144, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 84);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(657, 25);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06022OMSelectSiteFilterBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 26);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit2});
            this.gridControl2.Size = new System.Drawing.Size(655, 224);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06022OMSelectSiteFilterBindingSource
            // 
            this.sp06022OMSelectSiteFilterBindingSource.DataMember = "sp06022_OM_Select_Site_Filter";
            this.sp06022OMSelectSiteFilterBindingSource.DataSource = this.dataSet_GC_Summer_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteID,
            this.colSiteName,
            this.colSiteType,
            this.colSiteOrder,
            this.colClientID1,
            this.colClientName1,
            this.colClientType1,
            this.colActiveClient1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.BestFitMaxRowCount = 10;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 3;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 309;
            // 
            // colSiteType
            // 
            this.colSiteType.Caption = "Site Type";
            this.colSiteType.FieldName = "SiteType";
            this.colSiteType.Name = "colSiteType";
            this.colSiteType.OptionsColumn.AllowEdit = false;
            this.colSiteType.OptionsColumn.AllowFocus = false;
            this.colSiteType.OptionsColumn.ReadOnly = true;
            this.colSiteType.Visible = true;
            this.colSiteType.VisibleIndex = 1;
            this.colSiteType.Width = 167;
            // 
            // colSiteOrder
            // 
            this.colSiteOrder.Caption = "Site Order";
            this.colSiteOrder.FieldName = "SiteOrder";
            this.colSiteOrder.Name = "colSiteOrder";
            this.colSiteOrder.OptionsColumn.AllowEdit = false;
            this.colSiteOrder.OptionsColumn.AllowFocus = false;
            this.colSiteOrder.OptionsColumn.ReadOnly = true;
            this.colSiteOrder.Visible = true;
            this.colSiteOrder.VisibleIndex = 2;
            this.colSiteOrder.Width = 83;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            this.colClientID1.Visible = true;
            this.colClientID1.VisibleIndex = 4;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 3;
            this.colClientName1.Width = 306;
            // 
            // colClientType1
            // 
            this.colClientType1.Caption = "Client Type";
            this.colClientType1.FieldName = "ClientType";
            this.colClientType1.Name = "colClientType1";
            this.colClientType1.OptionsColumn.AllowEdit = false;
            this.colClientType1.OptionsColumn.AllowFocus = false;
            this.colClientType1.OptionsColumn.ReadOnly = true;
            this.colClientType1.Width = 185;
            // 
            // colActiveClient1
            // 
            this.colActiveClient1.Caption = "Active Client";
            this.colActiveClient1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colActiveClient1.FieldName = "ActiveClient";
            this.colActiveClient1.Name = "colActiveClient1";
            this.colActiveClient1.OptionsColumn.AllowEdit = false;
            this.colActiveClient1.OptionsColumn.AllowFocus = false;
            this.colActiveClient1.OptionsColumn.ReadOnly = true;
            this.colActiveClient1.Visible = true;
            this.colActiveClient1.VisibleIndex = 5;
            this.colActiveClient1.Width = 81;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(597, 487);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(516, 487);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(656, 398);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshSites, true)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 3";
            // 
            // bbiRefreshSites
            // 
            this.bbiRefreshSites.Caption = "Load Linked Sites";
            this.bbiRefreshSites.Id = 31;
            this.bbiRefreshSites.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefreshSites.ImageOptions.Image")));
            this.bbiRefreshSites.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefreshSites.ImageOptions.LargeImage")));
            this.bbiRefreshSites.Name = "bbiRefreshSites";
            this.bbiRefreshSites.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshSites.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshSites_ItemClick);
            // 
            // beiAllowSiteSelection
            // 
            this.beiAllowSiteSelection.Caption = "Allow Site Selection";
            this.beiAllowSiteSelection.Edit = this.repositoryItemCheckEditAllowSiteSelection;
            this.beiAllowSiteSelection.EditValue = 0;
            this.beiAllowSiteSelection.EditWidth = 117;
            this.beiAllowSiteSelection.Id = 30;
            this.beiAllowSiteSelection.ItemAppearance.Disabled.BackColor = System.Drawing.Color.Transparent;
            this.beiAllowSiteSelection.ItemAppearance.Disabled.Options.UseBackColor = true;
            this.beiAllowSiteSelection.ItemAppearance.Hovered.BackColor = System.Drawing.Color.Transparent;
            this.beiAllowSiteSelection.ItemAppearance.Hovered.Options.UseBackColor = true;
            this.beiAllowSiteSelection.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.beiAllowSiteSelection.ItemAppearance.Normal.Options.UseBackColor = true;
            this.beiAllowSiteSelection.ItemAppearance.Pressed.BackColor = System.Drawing.Color.Transparent;
            this.beiAllowSiteSelection.ItemAppearance.Pressed.Options.UseBackColor = true;
            this.beiAllowSiteSelection.Name = "beiAllowSiteSelection";
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 4";
            this.bar2.DockCol = 1;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshClients, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DisableClose = true;
            this.bar2.OptionsBar.DisableCustomization = true;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.Text = "Custom 4";
            // 
            // barEditItemDateRange
            // 
            this.barEditItemDateRange.Caption = "Date Range:";
            this.barEditItemDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.barEditItemDateRange.EditWidth = 228;
            this.barEditItemDateRange.Id = 33;
            this.barEditItemDateRange.Name = "barEditItemDateRange";
            this.barEditItemDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.popupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefreshClients
            // 
            this.bbiRefreshClients.Caption = "Load Clients";
            this.bbiRefreshClients.Id = 34;
            this.bbiRefreshClients.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefreshClients.ImageOptions.Image")));
            this.bbiRefreshClients.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefreshClients.ImageOptions.LargeImage")));
            this.bbiRefreshClients.Name = "bbiRefreshClients";
            this.bbiRefreshClients.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefreshClients.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshClients_ItemClick);
            // 
            // sp06021_OM_Select_Client_FilterTableAdapter
            // 
            this.sp06021_OM_Select_Client_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // sp06022_OM_Select_Site_FilterTableAdapter
            // 
            this.sp06022_OM_Select_Site_FilterTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Select_Client_Site
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(678, 516);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_Core_Select_Client_Site";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Client and \\ or Site";
            this.Load += new System.EventHandler(this.frm_Core_Select_Client_Site_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditAllowSiteSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06021OMSelectClientFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Summer_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06022OMSelectSiteFilterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem beiAllowSiteSelection;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshSites;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarEditItem barEditItemDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshClients;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditAllowSiteSelection;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private System.Windows.Forms.BindingSource sp06021OMSelectClientFilterBindingSource;
        private DataSet_GC_Summer_Core dataSet_GC_Summer_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientType;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveClient;
        private DataSet_GC_Summer_CoreTableAdapters.sp06021_OM_Select_Client_FilterTableAdapter sp06021_OM_Select_Client_FilterTableAdapter;
        private System.Windows.Forms.BindingSource sp06022OMSelectSiteFilterBindingSource;
        private DataSet_GC_Summer_CoreTableAdapters.sp06022_OM_Select_Site_FilterTableAdapter sp06022_OM_Select_Site_FilterTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteType;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientType1;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveClient1;
    }
}

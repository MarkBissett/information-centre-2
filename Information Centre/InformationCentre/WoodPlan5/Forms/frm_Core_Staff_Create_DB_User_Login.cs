using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Staff_Create_DB_User_Login : BaseObjects.frmBase
    {
        #region Instance Variables

        Settings set = Settings.Default;
        private string strConnectionString = "";

        public string strNetworkID = "";
        #endregion

        public frm_Core_Staff_Create_DB_User_Login()
        {
            InitializeComponent();
        }


        private void frm_Core_Staff_Create_DB_User_Login_Load(object sender, EventArgs e)
        {
            this.FormID = 302;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            this.Text = "Create Database User Login [" + strNetworkID + "]";

            textEdit1.EditValue = "";
            textEdit2.EditValue = "";
            this.ValidateChildren();  // Force Validation Message on any controls containing them //
        }


        private void frm_Core_Staff_Create_DB_User_Login_Activated(object sender, EventArgs e)
        {
        }


        #region Editors

        private void textEdit1_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            string strPassword = te.EditValue.ToString();
            string strConfirmedPassword = textEdit2.EditValue.ToString();
            if (string.IsNullOrEmpty(strPassword))
            {
                dxErrorProvider1.SetError(textEdit1, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else if (strPassword.Length < 6)
            {
                dxErrorProvider1.SetError(textEdit1, "Value entered must be [6 - 50] digits in length.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(textEdit1, "");
            }

            if (strConfirmedPassword != strPassword)
            {
                dxErrorProvider1.SetError(textEdit2, "Password and Password Confirmation must match.");
                //e.Cancel = true;  // Show stop icon as field is invalid //
                //return;
            }
            else
            {
                dxErrorProvider1.SetError(textEdit2, "");
            }
        }


        private void textEdit2_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            string strPassword = textEdit1.EditValue.ToString();
            string strConfirmedPassword = te.EditValue.ToString();
            if (string.IsNullOrEmpty(strConfirmedPassword))
            {
                dxErrorProvider1.SetError(textEdit2, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else if (strConfirmedPassword != strPassword)
            {
                dxErrorProvider1.SetError(textEdit2, "Password and Password Confirmation must match.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(textEdit2, "");
            }

        }

        #endregion


        private void simpleButton1_Click(object sender, EventArgs e)
        {
            // Create Login //
            if (dxErrorProvider1.HasErrors)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Ensure a Password is present and that the Password and Confirmed Password match!\n\n" + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Create Login", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            WoodPlanDataSetTableAdapters.QueriesTableAdapter CreateLogin = new WoodPlan5.WoodPlanDataSetTableAdapters.QueriesTableAdapter();
            CreateLogin.ChangeConnectionString(strConnectionString);
            int intReturnValue = 0;
            try
            {
                intReturnValue = Convert.ToInt32(CreateLogin.sp00200_Staff_Create_User_DB_Login(strNetworkID, textEdit1.Text.ToString(), set.DatabaseName));
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the user login [" + ex.Message + "].\n\nYou may not have sufficient access rights to perform this action or the login may already exists.\n\nTry creating the login again - if the problem persists, contact Technical Support.", "Create Login", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (intReturnValue != 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to create the user login.\n\nYou may not have sufficient access rights to perform this action or the login may already exists.\n\nTry creating the login again - if the problem persists, contact Technical Support.", "Create Login", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DevExpress.XtraEditors.XtraMessageBox.Show("Login created successfully.\n\nThe new user should use Login ID: " + strNetworkID + " and the password: " + textEdit1.Text.ToString() + " to login.\n\nRemember to assign the new user to the appropriate user access groups using the Permissions Manager screen.", "Create Login", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;                        
            this.Close();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel; 
            this.Close();
        }




    }
}


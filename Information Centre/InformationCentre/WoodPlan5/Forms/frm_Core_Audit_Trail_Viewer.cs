using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //
using System.Xml;
using System.Linq;
using System.Xml.Linq;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraBars.Docking;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;
using WoodPlan5.Classes.Operations;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_Core_Audit_Trail_Viewer : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private string strConnectionStringREADONLY = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;

        bool iBool_ImportButtonEnabled = false;
        bool iBool_EnableGridColumnChooser = true;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateTransaction;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateTransactionItem;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateTransactionSQL;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_selected_ChangeType_ids = "";
        string i_str_selected_ChangeType_descriptions = "";
        BaseObjects.GridCheckMarksSelection selection1;

        string i_str_selected_TableName = "";
        string i_str_selected_FriendlyTableName = "";
        BaseObjects.GridCheckMarksSelection selection3;

        string i_str_selected_ColumnName = "";
        string i_str_selected_FriendlyColumnName = "";
        BaseObjects.GridCheckMarksSelection selection5;

        string i_str_selected_Users = "";
        string i_str_selected_Applications = "";
        string i_str_selected_Hosts = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        public string strPassedInDrillDownIDs = "";
        public string strPassedInDrillDownTypeID = "";

        Bitmap bmpBlank = null;
        Bitmap bmpInsert = null;
        Bitmap bmpUpdate = null;
        Bitmap bmpDelete = null;
        
        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        SuperToolTip superToolTipUserFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsUserFilter = null;
        
        SuperToolTip superToolTipApplicationFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsApplicationFilter = null;

        SuperToolTip superToolTipHostFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsHostFilter = null;

        SuperToolTip superToolTipTableFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsTableFilter = null;

        SuperToolTip superToolTipColumnFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsColumnFilter = null;

        // Extra Permissions to control the types of data the user can view //
        private bool boolCreateRewindSQL = false;
        private bool boolAmenityTreesData = false;
        private bool boolAssetManagementData = false;
        private bool boolWinterMaintenanceData = false;
        private bool boolTenderRegisterData = false;
        private bool boolCRMData = false;
        private bool boolSummerMaintenanceData = false;
        private bool boolUtilityArbData = false;
        private bool boolHRData = false;
        private bool boolTrainingData = false;
        private bool boolEquipmentData = false;

        #endregion


        public frm_Core_Audit_Trail_Viewer()
        {
            InitializeComponent();
        }

        private void frm_Core_Audit_Trail_Viewer_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 8005;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;
            strConnectionStringREADONLY = GlobalSettings.ConnectionStringREADONLY;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            bmpBlank = new Bitmap(16, 16);
            bmpInsert = new Bitmap(WoodPlan5.Properties.Resources.Add_16x16, 16, 16);
            bmpUpdate = new Bitmap(WoodPlan5.Properties.Resources.Edit_16x16, 16, 16);
            bmpDelete = new Bitmap(WoodPlan5.Properties.Resources.Delete_16x16, 16, 16);

            sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateTransaction = new RefreshGridState(gridViewHeader, "TransactionID");

            sp01006_Core_Audit_Trail_Viewer_Transaction_Details_ListTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateTransactionItem = new RefreshGridState(gridViewDetail, "RecordID");

            sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQLTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            RefreshGridViewStateTransactionSQL = new RefreshGridState(gridViewDetail, "TransactionID");

            i_dtStart = DateTime.Today.AddMonths(-1);
            i_dtEnd = DateTime.Today.AddDays(1).AddSeconds(-1);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            popupContainerEditDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            memoExEditOldValue.EditValue = "";
            memoExEditNewValue.EditValue = "";

            // Add record selection checkboxes to popup Change Type Filter grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            sp01010_Core_Audit_Trail_Viewer_Change_Types_ListTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp01010_Core_Audit_Trail_Viewer_Change_Types_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01010_Core_Audit_Trail_Viewer_Change_Types_List);
            gridControl1.ForceInitialize();

            // Add record selection checkboxes to popup Table Filter grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;
            sp01008_Core_Audit_Trail_Viewer_Unique_Table_NamesTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp01008_Core_Audit_Trail_Viewer_Unique_Table_NamesTableAdapter.Fill(dataSet_Common_Functionality.sp01008_Core_Audit_Trail_Viewer_Unique_Table_Names, "");
            gridControl3.ForceInitialize();

            // Add record selection checkboxes to popup Column Filter grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;
            sp01009_Core_Audit_Trail_Viewer_Unique_Column_NamesTableAdapter.Connection.ConnectionString = strConnectionStringREADONLY;
            sp01009_Core_Audit_Trail_Viewer_Unique_Column_NamesTableAdapter.Fill(dataSet_Common_Functionality.sp01009_Core_Audit_Trail_Viewer_Unique_Column_Names, "");
            gridControl5.ForceInitialize();

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEditDateRange.EditValue = "Custom Filter";
                buttonEditUserFilter.EditValue = "Custom Filter";
                popupContainerEditTableFilter.Text = "Custom Filter";
                popupContainerEditColumnFilter.Text = "Custom Filter";
                popupContainerEditChangeTypeFilter.Text = "Custom Filter";
                buttonEditApplicationFilter.Text = "Custom Filter";
                buttonEditHostNameFilter.Text = "Custom Filter";
                Load_Data();  // Load records //
            }

            popupContainerControlDateRange.Size = new System.Drawing.Size(200, 107);
            popupContainerControlTableNameFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlColumnFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlChangeTypeFilter.Size = new System.Drawing.Size(270, 400);

            // Create a SuperToolTip //
            superToolTipUserFilter = new SuperToolTip();
            superToolTipSetupArgsUserFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsUserFilter.Title.Text = "User Filter - Information";
            superToolTipSetupArgsUserFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsUserFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsUserFilter.Footer.Text = "";
            superToolTipUserFilter.Setup(superToolTipSetupArgsUserFilter);
            superToolTipUserFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditUserFilter.SuperTip = superToolTipUserFilter;

            // Create a SuperToolTip //
            superToolTipApplicationFilter = new SuperToolTip();
            superToolTipSetupArgsApplicationFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsApplicationFilter.Title.Text = "Application Filter - Information";
            superToolTipSetupArgsApplicationFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsApplicationFilter.Contents.Text = "I store the currently applied Application Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsApplicationFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsApplicationFilter.Footer.Text = "";
            superToolTipApplicationFilter.Setup(superToolTipSetupArgsApplicationFilter);
            superToolTipUserFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditApplicationFilter.SuperTip = superToolTipApplicationFilter;

            // Create a SuperToolTip //
            superToolTipHostFilter = new SuperToolTip();
            superToolTipSetupArgsHostFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsHostFilter.Title.Text = "Host Filter - Information";
            superToolTipSetupArgsHostFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsHostFilter.Contents.Text = "I store the currently applied Host Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsHostFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsHostFilter.Footer.Text = "";
            superToolTipHostFilter.Setup(superToolTipSetupArgsHostFilter);
            superToolTipHostFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditHostNameFilter.SuperTip = superToolTipHostFilter;

            // Create a SuperToolTip //
            superToolTipTableFilter = new SuperToolTip();
            superToolTipSetupArgsTableFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsTableFilter.Title.Text = "Table Filter - Information";
            superToolTipSetupArgsTableFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsTableFilter.Contents.Text = "I store the currently applied Table Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsTableFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsTableFilter.Footer.Text = "";
            superToolTipTableFilter.Setup(superToolTipSetupArgsTableFilter);
            superToolTipTableFilter.AllowHtmlText = DefaultBoolean.True;
            popupContainerEditTableFilter.SuperTip = superToolTipTableFilter;

            // Create a SuperToolTip //
            superToolTipColumnFilter = new SuperToolTip();
            superToolTipSetupArgsColumnFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsColumnFilter.Title.Text = "Column Filter - Information";
            superToolTipSetupArgsColumnFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsColumnFilter.Contents.Text = "I store the currently applied Column Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsColumnFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsColumnFilter.Footer.Text = "";
            superToolTipColumnFilter.Setup(superToolTipSetupArgsColumnFilter);
            superToolTipColumnFilter.AllowHtmlText = DefaultBoolean.True;
            popupContainerEditColumnFilter.SuperTip = superToolTipColumnFilter;

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_Core_Audit_Trail_Viewer_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Audit_Trail_Viewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientContractFilter", i_str_selected_ClientContract_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "TableFilter", i_str_selected_TableName);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ColumnFilter", i_str_selected_FriendlyColumnName);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "OldValueFilter", memoExEditOldValue.EditValue.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "NewValueFilter", memoExEditNewValue.EditValue.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ApplyFilterToDetail",(checkEditApplyFilterToDetail.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ChangeTypeFilter", i_str_selected_ChangeType_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "UserFilter", i_str_selected_Users);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ApplicationFilter", i_str_selected_Applications);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "HostFilter", i_str_selected_Hosts);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "OldValueProceedingWildcard", (checkEditOldValueProceedingWildcard.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "OldValueSucceedingWildcard", (checkEditOldValueSucceedingWildcard.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "NewValueProceedingWildcard", (checkEditNewValueProceedingWildcard.Checked ? "1" : "0"));
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "NewValueSucceedingWildcard", (checkEditNewValueSucceedingWildcard.Checked ? "1" : "0"));
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //
                string strTooltipText = "";
                int intFoundRow = 0;
                string strItemFilter = "";

                // Change Type Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ChangeTypeFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl1.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditChangeTypeFilter.Text = PopupContainerEditChangeTypeFilter_Get_Selected();
                }

                // Table Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("TableFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["TableName"], strElement);
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditTableFilter.Text = PopupContainerEditTableFilter_Get_Selected();
                    strTooltipText = popupContainerEditTableFilter.Text.Replace(", ", "\n");
                    superToolTipSetupArgsTableFilter.Contents.Text = "I store the currently applied Table Filter.\n\n" + strTooltipText;
                }

                // Column Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ColumnFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItemsOuter = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    
                    ExtendedGridViewFunctions extGridViewFunction;  // Required for search for grid row using multiple columns //
                    extGridViewFunction = new ExtendedGridViewFunctions();
                    
                    foreach (string strElement in arrayItemsOuter)
                    {
                        if (strElement == "") break;
                        string[] arrayItems = strElement.Split(':');  // Single quotes because char expected for delimeter //
                        if (arrayItems.Length != 2) continue;

                        // Search on Multiple columns for matching gridview row //
                        GridColumn[] cols = new GridColumn[] { colFriendlyTableName3, colColumnName1 };  // Array of columns to search for a match on //
                        object[] values = new object[] { arrayItems[0].Trim(), arrayItems[1].Trim() };
                        intFoundRow = extGridViewFunction.LocateRowByMultipleValues(viewFilter, cols, values, 0);  // Function declared in BaseObjects... Classes... ExtendedGridViewFunctions //

                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditColumnFilter.Text = PopupContainerEditColumnFilter_Get_Selected();
                    strTooltipText = popupContainerEditColumnFilter.Text.Replace(", ", "\n");
                    superToolTipSetupArgsColumnFilter.Contents.Text = "I store the currently applied Column Filter.\n\n" + strTooltipText;
                }

                strItemFilter = default_screen_settings.RetrieveSetting("OldValueFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    memoExEditOldValue.EditValue = strItemFilter;
                }

                strItemFilter = default_screen_settings.RetrieveSetting("NewValueFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    memoExEditNewValue.EditValue = strItemFilter;
                }

                strItemFilter = default_screen_settings.RetrieveSetting("ApplyFilterToDetail");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditApplyFilterToDetail.Checked = (strItemFilter == "1");
                }

                // User Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("UserFilter");
                i_str_selected_Users = strItemFilter;
                buttonEditUserFilter.EditValue = i_str_selected_Users;
                strTooltipText = i_str_selected_Users.Replace(", ", "\n");
                superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\n" + strTooltipText;
                
                // Application Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ApplicationFilter");
                i_str_selected_Applications = strItemFilter;
                buttonEditApplicationFilter.EditValue = i_str_selected_Applications;
                strTooltipText = i_str_selected_Applications.Replace(", ", "\n");
                superToolTipSetupArgsApplicationFilter.Contents.Text = "I store the currently applied Application Filter.\n\n" + strTooltipText;
                
                // Host Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("HostFilter");
                i_str_selected_Hosts = strItemFilter;
                buttonEditHostNameFilter.EditValue = i_str_selected_Hosts;
                strTooltipText = i_str_selected_Hosts.Replace(", ", "\n");
                superToolTipSetupArgsHostFilter.Contents.Text = "I store the currently applied Host Filter.\n\n" + strTooltipText;

                strItemFilter = default_screen_settings.RetrieveSetting("OldValueProceedingWildcard");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditOldValueProceedingWildcard.Checked = (strItemFilter == "1");
                }

                strItemFilter = default_screen_settings.RetrieveSetting("OldValueSucceedingWildcard");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditOldValueSucceedingWildcard.Checked = (strItemFilter == "1");
                }

                strItemFilter = default_screen_settings.RetrieveSetting("NewValueProceedingWildcard");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditNewValueProceedingWildcard.Checked = (strItemFilter == "1");
                }

                strItemFilter = default_screen_settings.RetrieveSetting("NewValueSucceedingWildcard");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditNewValueSucceedingWildcard.Checked = (strItemFilter == "1");
                }

                Load_Data();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                Point location = dockPanelFilters.PointToScreen(Point.Empty);

                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Transactions...");
            }

            if (i_str_selected_ChangeType_ids == null) i_str_selected_ChangeType_ids = "";
            RefreshGridViewStateTransaction.SaveViewInfo();
            GridView view = (GridView)gridControlHeader.MainView;
            view.BeginUpdate();
            try
            {
                if (buttonEditUserFilter.EditValue.ToString() == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Callouts //
                {
                    sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_List, strPassedInDrillDownIDs, 
                                                                                                                                                                                    strPassedInDrillDownTypeID, 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    i_dtStart, 
                                                                                                                                                                                    i_dtEnd, 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    0, 
                                                                                                                                                                                    0, 
                                                                                                                                                                                    0, 
                                                                                                                                                                                    0, 
                                                                                                                                                                                    (boolAmenityTreesData ? 1 : 0), 
                                                                                                                                                                                    (boolAssetManagementData ? 1 : 0), 
                                                                                                                                                                                    (boolWinterMaintenanceData ? 1 : 0), 
                                                                                                                                                                                    (boolTenderRegisterData ? 1 : 0), 
                                                                                                                                                                                    (boolCRMData ? 1 : 0), 
                                                                                                                                                                                    (boolSummerMaintenanceData ? 1 : 0), 
                                                                                                                                                                                    (boolUtilityArbData ? 1 : 0), 
                                                                                                                                                                                    (boolHRData ? 1 : 0), 
                                                                                                                                                                                    (boolTrainingData ? 1 : 0), 
                                                                                                                                                                                    (boolEquipmentData ? 1 : 0));
                    this.RefreshGridViewStateTransaction.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    view.ExpandAllGroups();
                }
                else // Load users selection //
                {
                    string  strOldValue = (string.IsNullOrWhiteSpace(memoExEditOldValue.EditValue.ToString()) ? "" : memoExEditOldValue.EditValue.ToString());
                    string  strNewValue = (string.IsNullOrWhiteSpace(memoExEditNewValue.EditValue.ToString()) ? "" : memoExEditNewValue.EditValue.ToString());

                    sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01005_Core_Audit_Trail_Viewer_Unique_Transactions_List, "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    "", 
                                                                                                                                                                                    i_str_selected_TableName, 
                                                                                                                                                                                    i_dtStart, 
                                                                                                                                                                                    i_dtEnd, 
                                                                                                                                                                                    strOldValue, 
                                                                                                                                                                                    strNewValue, 
                                                                                                                                                                                    i_str_selected_ColumnName, 
                                                                                                                                                                                    i_str_selected_Users, 
                                                                                                                                                                                    i_str_selected_Applications, 
                                                                                                                                                                                    i_str_selected_Hosts, 
                                                                                                                                                                                    i_str_selected_ChangeType_ids, 
                                                                                                                                                                                    (checkEditOldValueProceedingWildcard.Checked ? 1 : 0), 
                                                                                                                                                                                    (checkEditOldValueSucceedingWildcard.Checked ? 1 : 0), 
                                                                                                                                                                                    (checkEditNewValueProceedingWildcard.Checked ? 1 : 0), 
                                                                                                                                                                                    (checkEditNewValueSucceedingWildcard.Checked ? 1 : 0),
                                                                                                                                                                                    (boolAmenityTreesData ? 1 : 0), 
                                                                                                                                                                                    (boolAssetManagementData ? 1 : 0), 
                                                                                                                                                                                    (boolWinterMaintenanceData ? 1 : 0), 
                                                                                                                                                                                    (boolTenderRegisterData ? 1 : 0), 
                                                                                                                                                                                    (boolCRMData ? 1 : 0), 
                                                                                                                                                                                    (boolSummerMaintenanceData ? 1 : 0), 
                                                                                                                                                                                    (boolUtilityArbData ? 1 : 0), 
                                                                                                                                                                                    (boolHRData ? 1 : 0), 
                                                                                                                                                                                    (boolTrainingData ? 1 : 0), 
                                                                                                                                                                                    (boolEquipmentData ? 1 : 0));
                    this.RefreshGridViewStateTransaction.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
            }
            catch (Exception ex)
            {
                 DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Transactions.\n\nMessage = [" + ex.Message + "].\n\nIf the error is a result of a query timeout, try adjusting the parameters at the top of the screen to reduce the amount of data returned then try again. If the problem persists, contact Technical Support.", "Load Transactions", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            bool boolSplashScreenVisibile = splashScreenManager.IsSplashFormVisible;
            if (!boolSplashScreenVisibile)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Linked Data...");
            }
            
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlHeader.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["TransactionID"])) + ',';
            }

            gridControlDetail.MainView.BeginUpdate();
            RefreshGridViewStateTransactionItem.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_Common_Functionality.sp01006_Core_Audit_Trail_Viewer_Transaction_Details_List.Clear();
            }
            else
            {
                string strOldValue = (string.IsNullOrWhiteSpace(memoExEditOldValue.EditValue.ToString()) ? "" : memoExEditOldValue.EditValue.ToString());
                string strNewValue = (string.IsNullOrWhiteSpace(memoExEditNewValue.EditValue.ToString()) ? "" : memoExEditNewValue.EditValue.ToString());

                sp01006_Core_Audit_Trail_Viewer_Transaction_Details_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01006_Core_Audit_Trail_Viewer_Transaction_Details_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), (checkEditApplyFilterToDetail.Checked ? 1 : 0), strOldValue, strNewValue, i_str_selected_ColumnName, (checkEditOldValueProceedingWildcard.Checked ? 1 : 0), (checkEditOldValueSucceedingWildcard.Checked ? 1 : 0), (checkEditNewValueProceedingWildcard.Checked ? 1 : 0), (checkEditNewValueSucceedingWildcard.Checked ? 1 : 0));
                RefreshGridViewStateTransactionItem.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlDetail.MainView.EndUpdate();

            gridControlSQL.MainView.BeginUpdate();
            RefreshGridViewStateTransactionSQL.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_Common_Functionality.sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQL.Clear();
            }
            else
            {
                sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQLTableAdapter.Fill(dataSet_Common_Functionality.sp01007_Core_Audit_Trail_Viewer_Transaction_Details_SQL, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateTransactionSQL.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlSQL.MainView.EndUpdate();

            if (!boolSplashScreenVisibile && splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data();
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string newIds)
        {
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1000:
                        if (sfpPermissions.blCreate)
                        {
                            boolCreateRewindSQL = true;
                        }
                        break;
                    case 1:
                        if (sfpPermissions.blRead)
                        {
                            boolAmenityTreesData = true;
                        }
                        break;
                    case 2:
                        if (sfpPermissions.blRead)
                        {
                            boolAssetManagementData = true;
                        }
                        break;
                    case 4:
                        if (sfpPermissions.blRead)
                        {
                            boolWinterMaintenanceData = true;
                        }
                        break;
                    case 5:
                        if (sfpPermissions.blRead)
                        {
                            boolTenderRegisterData = true;
                        }
                        break;
                    case 6:
                        if (sfpPermissions.blRead)
                        {
                            boolCRMData = true;
                        }
                        break;
                    case 7:
                        if (sfpPermissions.blRead)
                        {
                            boolSummerMaintenanceData = true;
                        }
                        break;
                    case 8:
                        if (sfpPermissions.blRead)
                        {
                            boolUtilityArbData = true;
                        }
                        break;
                    case 9:
                        if (sfpPermissions.blRead)
                        {
                            boolHRData = true;
                        }
                        break;
                    case 10:
                        if (sfpPermissions.blRead)
                        {
                            boolTrainingData = true;
                        }
                        break;
                    case 11:
                        if (sfpPermissions.blRead)
                        {
                            boolEquipmentData = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControlHeader.MainView;
            GridView ParentView = (GridView)gridControlHeader.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    view = (GridView)gridControlHeader.MainView;
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    view = (GridView)gridControlDetail.MainView;
                    break;
                default: 
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();
            int[] intParentRowHandles = ParentView.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                case Utils.enmFocusedGrid.JobRate:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        bbiBlockAdd.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        bbiBlockAdd.Enabled = (iBool_AllowAdd && intParentRowHandles.Length > 1);
                    }
                    break;
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set Enabled Status of Main Toolbar Buttons //
            bsiCreateRollbackSQL.Enabled = (boolCreateRewindSQL && intParentRowHandles.Length > 0);
            bbiRollbackSQLFile.Enabled = bsiCreateRollbackSQL.Enabled;
            bbiRollbackSQLClipboard.Enabled = bsiCreateRollbackSQL.Enabled;
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
        }

        private void Block_Add()
        {
        }

        private void Block_Edit()
        {
        }

        private void Edit_Record()
        {
        }

        private void Delete_Record()
        {
        }

        private void View_Record()
        {
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewHeader":
                    message = "No Transactions Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewDetail":
                    message = "No Linked Transaction Details Available - Select one or more Transactions to view Linked Transaction Details";
                    break;
                case "gridViewSQL":
                    message = "No Linked Transaction Details Available - Select one or more Transactions to view Linked Transaction Details";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewHeader":
                    LoadLinkedRecords();
                    view = (GridView)gridControlDetail.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlSQL.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView - Transactions

        private void gridControlHeader_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            /*
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
            */
        }

        private void gridViewHeader_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewHeader_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedVisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedVisitCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewHeader_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewHeader_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            SetMenuStatus();
        }

        private void gridViewHeader_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewHeader_CustomUnboundColumnData(object sender, CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "IconStatus" && e.IsGetData)
            {
                e.Value = bmpBlank;
                GridView view = (GridView)sender;
                int rHandle = (sender as GridView).GetRowHandle(e.ListSourceRowIndex);
                switch (view.GetRowCellValue(rHandle, "ChangeTypeDescription").ToString())
                {
                    case "Insert":
                        e.Value = bmpInsert;
                        break;
                    case "Update":
                        e.Value = bmpUpdate;
                        break;
                    case "Delete":
                        e.Value = bmpDelete;
                        break;
                    default:
                        e.Value = bmpBlank;
                        break;
                }
            }
        }

        #endregion


        #region GridView - Transaction Details

        private void gridControlDetail_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            /*
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
            */
        }

        private void gridViewDetail_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewDetail_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.JobRate;
            SetMenuStatus();
        }

        private void gridViewDetail_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.JobRate;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Transaction SQL

        private void gridControlSQL_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            /*
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
            */
        }

        private void gridViewSQL_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewSQL_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.JobRate;
            SetMenuStatus();
        }

        private void gridViewSQL_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.JobRate;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Other Filters

        private void buttonEditUserFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString().ToLower())
            {
                case "clear":
                    {
                        i_str_selected_Users = "";
                        buttonEditUserFilter.EditValue = "No User Filter";

                        // Update Filter control's tooltip //
                        superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\nNo Filter Set.";
                    }
                    break;
                case "choose":
                    {
                        var fChildForm = new frm_Core_Audit_Trail_Filter_Data();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._DateType = "user";
                        fChildForm._Mode = "multiple";
                        fChildForm.strOriginalValue = i_str_selected_Users;
                        fChildForm._Mode = "multiple";
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (i_str_selected_Users == fChildForm.strSelectedValue) return;
                            i_str_selected_Users = fChildForm.strSelectedValue;
                            buttonEditUserFilter.EditValue = i_str_selected_Users;

                            // Update Filter control's tooltip //
                            string strTooltipText = i_str_selected_Users.Replace(", ", "\n");
                            superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\n" + strTooltipText;

                            Clear_Custom_Filter_Labels("buttonEditUserFilter", false);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void buttonEditApplicationFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString().ToLower())
            {
                case "clear":
                    {
                        i_str_selected_Users = "";
                        buttonEditApplicationFilter.EditValue = "No Application Filter";

                        // Update Filter control's tooltip //
                        superToolTipSetupArgsApplicationFilter.Contents.Text = "I store the currently applied Application Filter.\n\nNo Filter Set.";
                    }
                    break;
                case "choose":
                    {
                        var fChildForm = new frm_Core_Audit_Trail_Filter_Data();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._DateType = "application";
                        fChildForm._Mode = "multiple";
                        fChildForm.strOriginalValue = i_str_selected_Applications;
                        fChildForm._Mode = "multiple";
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (i_str_selected_Users == fChildForm.strSelectedValue) return;
                            i_str_selected_Applications = fChildForm.strSelectedValue;
                            buttonEditApplicationFilter.EditValue = i_str_selected_Applications;

                            // Update Filter control's tooltip //
                            string strTooltipText = i_str_selected_Applications.Replace(", ", "\n");
                            superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\n" + strTooltipText;

                            Clear_Custom_Filter_Labels("buttonEditApplicationFilter", false);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void buttonEditHostNameFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString().ToLower())
            {
                case "clear":
                    {
                        i_str_selected_Hosts = "";
                        buttonEditHostNameFilter.EditValue = "No Host Filter";

                        // Update Filter control's tooltip //
                        superToolTipSetupArgsHostFilter.Contents.Text = "I store the currently applied Host Filter.\n\nNo Filter Set.";
                    }
                    break;
                case "choose":
                    {
                        var fChildForm = new frm_Core_Audit_Trail_Filter_Data();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._DateType = "hostname";
                        fChildForm._Mode = "multiple";
                        fChildForm.strOriginalValue = i_str_selected_Hosts;
                        fChildForm._Mode = "multiple";
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (i_str_selected_Hosts == fChildForm.strSelectedValue) return;
                            i_str_selected_Hosts = fChildForm.strSelectedValue;
                            buttonEditHostNameFilter.EditValue = i_str_selected_Hosts;

                            // Update Filter control's tooltip //
                            string strTooltipText = i_str_selected_Hosts.Replace(", ", "\n");
                            superToolTipSetupArgsHostFilter.Contents.Text = "I store the currently applied Host Filter.\n\n" + strTooltipText;

                            Clear_Custom_Filter_Labels("buttonEditHostNameFilter", false);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void memoExEditOldValue_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString().ToLower())
            {
                case "clear":
                    {
                        memoExEditOldValue.EditValue = "";

                        // Update Filter control's tooltip //
                        //superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\nNo Filter Set.";
                    }
                    break;
                case "choose":
                    {
                        var fChildForm = new frm_Core_Audit_Trail_Filter_Data();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._DateType = "oldvalue";
                        fChildForm._Mode = "multiple";
                        fChildForm.strOriginalValue = memoExEditOldValue.EditValue.ToString();
                        fChildForm._Mode = "multiple";
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (memoExEditOldValue.EditValue.ToString() == fChildForm.strSelectedValue) return;
                            memoExEditOldValue.EditValue = fChildForm.strSelectedValue;

                            // Update Filter control's tooltip //
                            //string strTooltipText = i_str_selected_Users.Replace(", ", "\n");
                            //superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\n" + strTooltipText;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void memoExEditNewValue_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString().ToLower())
            {
                case "clear":
                    {
                        memoExEditNewValue.EditValue = "";

                        // Update Filter control's tooltip //
                        //superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\nNo Filter Set.";
                    }
                    break;
                case "choose":
                    {
                        var fChildForm = new frm_Core_Audit_Trail_Filter_Data();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._DateType = "newvalue";
                        fChildForm._Mode = "multiple";
                        fChildForm.strOriginalValue = memoExEditNewValue.EditValue.ToString();
                        fChildForm._Mode = "multiple";
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (memoExEditNewValue.EditValue.ToString() == fChildForm.strSelectedValue) return;
                            memoExEditNewValue.EditValue = fChildForm.strSelectedValue;

                            // Update Filter control's tooltip //
                            //string strTooltipText = i_str_selected_Users.Replace(", ", "\n");
                            //superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\n" + strTooltipText;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Callout Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();

            Clear_Custom_Filter_Labels("popupContainerEditDateRange", false);
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        #region Table Filter Panel

        private void popupContainerEditTableFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditTableFilter_Get_Selected();

            Clear_Custom_Filter_Labels("popupContainerEditTableFilter", false);
        }

        private void btnTableFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditTableFilter_Get_Selected()
        {
            i_str_selected_TableName = "";    // Reset any prior values first //
            i_str_selected_FriendlyTableName = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                superToolTipSetupArgsTableFilter.Contents.Text = "I store the currently applied Table Filter.\n\nNo Filter Set.";
                i_str_selected_TableName = "";
                return "No Table Filter";
            }
            else if (selection3.SelectedCount <= 0)
            {
                superToolTipSetupArgsTableFilter.Contents.Text = "I store the currently applied Table Filter.\n\nNo Filter Set.";
                i_str_selected_TableName = "";
                return "No Table Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_TableName += Convert.ToString(view.GetRowCellValue(i, "TableName")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_FriendlyTableName = Convert.ToString(view.GetRowCellValue(i, "FriendlyTableName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_FriendlyTableName += ", " + Convert.ToString(view.GetRowCellValue(i, "FriendlyTableName"));
                        }
                        intCount++;
                    }
                }
                // Update Filter control's tooltip //
                string strTooltipText = i_str_selected_FriendlyTableName.Replace(", ", "\n");
                superToolTipSetupArgsTableFilter.Contents.Text = "I store the currently applied Table Filter.\n\n" + strTooltipText;
            }
            return i_str_selected_FriendlyTableName;
        }

        #endregion


        #region Column Filter Panel

        private void popupContainerEditColumnFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditColumnFilter_Get_Selected();

            Clear_Custom_Filter_Labels("PopupContainerEditColumnFilter", false);
        }

        private void btnColumnFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditColumnFilter_Get_Selected()
        {
            i_str_selected_ColumnName = "";    // Reset any prior values first //
            i_str_selected_FriendlyColumnName = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                superToolTipSetupArgsColumnFilter.Contents.Text = "I store the currently applied Column Filter.\n\nNo Filter Set.";
                i_str_selected_ColumnName = "";
                return "No Column Filter";
            }
            else if (selection5.SelectedCount <= 0)
            {
                superToolTipSetupArgsColumnFilter.Contents.Text = "I store the currently applied Column Filter.\n\nNo Filter Set.";
                i_str_selected_ColumnName = "";
                return "No Column Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_ColumnName += Convert.ToString(view.GetRowCellValue(i, "ColumnName")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_FriendlyColumnName = Convert.ToString(view.GetRowCellValue(i, "FriendlyTableName")) + ": " + Convert.ToString(view.GetRowCellValue(i, "ColumnName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_FriendlyColumnName += ", " + Convert.ToString(view.GetRowCellValue(i, "FriendlyTableName")) + ": " + Convert.ToString(view.GetRowCellValue(i, "ColumnName"));
                        }
                        intCount++;
                    }
                }
                // Update Filter control's tooltip //
                string strTooltipText = i_str_selected_FriendlyColumnName.Replace(", ", "\n");
                superToolTipSetupArgsColumnFilter.Contents.Text = "I store the currently applied Column Filter.\n\n" + strTooltipText;

            }
            return i_str_selected_FriendlyColumnName;
        }

        #endregion


        #region Change Type Filter Panel

        private void popupContainerEditChangeTypeFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditChangeTypeFilter_Get_Selected();

            Clear_Custom_Filter_Labels("PopupContainerEditChangeTypeFilter", false);
        }

        private void btnChangeTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditChangeTypeFilter_Get_Selected()
        {
            i_str_selected_ChangeType_ids = "";    // Reset any prior values first //
            i_str_selected_ChangeType_descriptions = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl1.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_ChangeType_ids = "";
                return "No Change Type Filter";
            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_ChangeType_ids = "";
                return "No Change Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_ChangeType_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_ChangeType_descriptions = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_ChangeType_descriptions += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_ChangeType_descriptions;
        }

        #endregion


        private void btnClearAllFilters_Click(object sender, EventArgs e)
        {
            Clear_Custom_Filter_Labels("", true);
        }

        private void Clear_Custom_Filter_Labels(string strCurrentControl, bool boolClearAll)
        {
            if (strCurrentControl != "popupContainerEditDateRange" && (popupContainerEditDateRange.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                popupContainerEditDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();
            }
            if (strCurrentControl != "buttonEditUserFilter" && (buttonEditUserFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                i_str_selected_Users = "";
                buttonEditUserFilter.Text = "No User Filter";
                superToolTipSetupArgsUserFilter.Contents.Text = "I store the currently applied User Filter.\n\nNo Filter Set.";
            }
            if (strCurrentControl != "buttonEditApplicationFilter" && (buttonEditApplicationFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                i_str_selected_Applications = "";
                buttonEditApplicationFilter.Text = "No Application Filter";
                superToolTipSetupArgsApplicationFilter.Contents.Text = "I store the currently applied Application Filter.\n\nNo Filter Set.";
            }
            if (strCurrentControl != "buttonEditHostNameFilter" && (buttonEditHostNameFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                i_str_selected_Hosts = "";
                buttonEditHostNameFilter.EditValue = "No Host Filter";
                superToolTipSetupArgsHostFilter.Contents.Text = "I store the currently applied Host Filter.\n\nNo Filter Set.";
            }
            if (strCurrentControl != "popupContainerEditChangeTypeFilter" && (popupContainerEditChangeTypeFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                selection1.ClearSelection();
                popupContainerEditChangeTypeFilter.Text = PopupContainerEditChangeTypeFilter_Get_Selected();
            }
            if (strCurrentControl != "popupContainerEditTableFilter" && (popupContainerEditTableFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                selection3.ClearSelection();
                popupContainerEditTableFilter.Text = PopupContainerEditTableFilter_Get_Selected();
                superToolTipSetupArgsTableFilter.Contents.Text = "I store the currently applied Table Filter.\n\nNo Filter Set.";
            }
            if (strCurrentControl != "popupContainerEditColumnFilter" && (popupContainerEditColumnFilter.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                selection5.ClearSelection();
                popupContainerEditColumnFilter.Text = PopupContainerEditColumnFilter_Get_Selected();
                superToolTipSetupArgsColumnFilter.Contents.Text = "I store the currently applied Column Filter.\n\nNo Filter Set.";
            }
            if (strCurrentControl != "memoExEditOldValue" && (memoExEditOldValue.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                memoExEditOldValue.EditValue = "";
            }
            if (strCurrentControl != "memoExEditNewValue" && (memoExEditNewValue.EditValue.ToString() == "Custom Filter" || boolClearAll))
            {
                memoExEditNewValue.EditValue = "";
            }
            if (strCurrentControl != "checkEditApplyFilterToDetail" && (memoExEditNewValue.EditValue == "Custom Filter" || boolClearAll))
            {
                checkEditApplyFilterToDetail.Checked = false;
            }
        }

        private void btnLoad_Click_1(object sender, EventArgs e)
        {
            Load_Data();
        }


        #region Buttons


        private void bbiViewSchedule_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void bbiSendSchedule_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        #endregion


        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }

        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo {Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip};

            toolTipController1.ShowHint(Info);
        }

        private void bbiRollbackSQLFile_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strSQL = Create_Rollback_SQL();

            string strFilename = "SQL_Rollback_Script_" + String.Format("{0:yyyy-MM-dd_HH-mm-ss}", DateTime.Now);
            using (var sfd = new SaveFileDialog())
            {
                sfd.FileName = strFilename;
                sfd.Filter = "sql script files (*.sql)|*.sql|txt files (*.txt)|*.txt|All files (*.*)|*.*";
                sfd.FilterIndex = 1;

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        File.WriteAllText(sfd.FileName, strSQL);
                        DevExpress.XtraEditors.XtraMessageBox.Show("SQL Rollback Script Created Successfully.", "Create Rollback SQL", MessageBoxButtons.OK);
                        System.Diagnostics.Process.Start(sfd.FileName);
                    }
                    catch (Exception ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("There was a problem creating the file - an error occurred:\n\n" + ex.Message + "\n\nPlease try again.If the problem persists, please contact Technical Support.", "Create Rollback SQL", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        throw;
                    }
                }
            }
        }

        private void bbiRollbackSQLClipboard_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strSQL = Create_Rollback_SQL();
            
            System.Windows.Forms.Clipboard.SetText(strSQL);
            if (!string.IsNullOrWhiteSpace(strSQL)) XtraMessageBox.Show("SQL copied to clipboard.\n\nUse Ctrl + V to paste the SQL into a suitable editor.", "Create Rollback SQL", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private string Create_Rollback_SQL()
        {
            string strSQL = "";
            int[] intRowHandles;
            GridView view = (GridView)gridControlHeader.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to generate Rollback SQL Code for before proceeding.", "Create Rollback SQL", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "";
            }
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Creating Rollback SQL...");

            string strTransactionID = "";
            string strTableName = "";
            string strAuditTableWhereClause = "";
            string strDestinationTableName = "";
            string strDestinationTableSchema = "";
            int intSqlToTable = 1;  // 0 directly executes the SQL and rolls back the changes. ! puts it intot a temp table which we can loop round and extract to build a string for output to the user [recommended]. //

            try
            {
                SqlDataAdapter sdaData = null;
                DataSet dsData = null;
                using (var conn = new SqlConnection(strConnectionString))
                {
                    conn.Open();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        using (var cmd = new SqlCommand())
                        {
                            strTransactionID = Convert.ToString(view.GetRowCellValue(intRowHandle, "TransactionID"));
                            strTableName = Convert.ToString(view.GetRowCellValue(intRowHandle, "TableName"));
                            strAuditTableWhereClause = "operationIdentifier = '" + strTransactionID + "'";
                            strDestinationTableName = Convert.ToString(view.GetRowCellValue(intRowHandle, "FriendlyTableName"));
                            strDestinationTableSchema = "dbo";

                            cmd.CommandText = "sp01012_Core_Audit_Trail_Viewer_Generate_Rollback_SQL";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@tableName", strTableName));
                            cmd.Parameters.Add(new SqlParameter("@auditTableWhereClause", strAuditTableWhereClause));
                            cmd.Parameters.Add(new SqlParameter("@destinationTableName", strDestinationTableName));
                            cmd.Parameters.Add(new SqlParameter("@destinationTableSchema", strDestinationTableSchema));
                            cmd.Parameters.Add(new SqlParameter("@sqlToTable", intSqlToTable));
                            cmd.Connection = conn;
                            sdaData = new SqlDataAdapter(cmd);
                            dsData = new DataSet("NewDataSet");
                            sdaData.Fill(dsData, "Table");
                            foreach (DataRow dr in dsData.Tables[0].Rows)
                            {
                                strSQL += dr["text"].ToString() + Environment.NewLine;
                            }
                            dsData.Clear();
                            strSQL += Environment.NewLine;
                        }
                    }
                    conn.Close();
                }
            }
            catch (Exception ex) { }

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            return strSQL;
        }











 





    }
}



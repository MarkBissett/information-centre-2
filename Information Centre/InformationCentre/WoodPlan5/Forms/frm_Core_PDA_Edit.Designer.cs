namespace WoodPlan5
{
    partial class frm_Core_PDA_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_PDA_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.LinkedToPersonTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp00242PDAEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.LinkedToPersonTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubContractorNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubContractorIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PDACodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PDALoginTokenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PDAPasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PDAUserNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SoftwareVersionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.IMEITextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MobileTelNoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DatePurchasedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.FirmwareVersionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SerialNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ModelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MakeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.PDA_IDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForSubContractorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDA_ID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMake = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForModel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForFirmwareVersion = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDatePurchased = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMobileTelNo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIMEI = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSoftwareVersion = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDAUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDAPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDALoginToken = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPDACode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubContractorName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLinkedToPersonType = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp00242_PDA_EditTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp00242_PDA_EditTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00242PDAEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDACodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDALoginTokenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDAPasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDAUserNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareVersionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMEITextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTelNoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatePurchasedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatePurchasedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirmwareVersionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SerialNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MakeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDA_IDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDA_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFirmwareVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDatePurchased)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobileTelNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIMEI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareVersion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDAUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDAPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDALoginToken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDACode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 502);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 502);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(628, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 476);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SubContractorIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PDACodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PDALoginTokenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PDAPasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PDAUserNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SoftwareVersionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.IMEITextEdit);
            this.dataLayoutControl1.Controls.Add(this.MobileTelNoTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DatePurchasedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.FirmwareVersionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SerialNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ModelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MakeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.PDA_IDTextEdit);
            this.dataLayoutControl1.DataSource = this.sp00242PDAEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSubContractorID,
            this.ItemForPDA_ID,
            this.ItemForLinkedToPersonTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1292, 171, 254, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 476);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // LinkedToPersonTypeTextEdit
            // 
            this.LinkedToPersonTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "LinkedToPersonType", true));
            this.LinkedToPersonTypeTextEdit.Location = new System.Drawing.Point(126, 403);
            this.LinkedToPersonTypeTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeTextEdit.Name = "LinkedToPersonTypeTextEdit";
            this.LinkedToPersonTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonTypeTextEdit, true);
            this.LinkedToPersonTypeTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonTypeTextEdit, optionsSpelling1);
            this.LinkedToPersonTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonTypeTextEdit.TabIndex = 47;
            // 
            // sp00242PDAEditBindingSource
            // 
            this.sp00242PDAEditBindingSource.DataMember = "sp00242_PDA_Edit";
            this.sp00242PDAEditBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LinkedToPersonTypeIDTextEdit
            // 
            this.LinkedToPersonTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "LinkedToPersonTypeID", true));
            this.LinkedToPersonTypeIDTextEdit.Location = new System.Drawing.Point(126, 398);
            this.LinkedToPersonTypeIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeIDTextEdit.Name = "LinkedToPersonTypeIDTextEdit";
            this.LinkedToPersonTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonTypeIDTextEdit, true);
            this.LinkedToPersonTypeIDTextEdit.Size = new System.Drawing.Size(461, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonTypeIDTextEdit, optionsSpelling2);
            this.LinkedToPersonTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonTypeIDTextEdit.TabIndex = 46;
            // 
            // SubContractorNameTextEdit
            // 
            this.SubContractorNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "SubContractorName", true));
            this.SubContractorNameTextEdit.Location = new System.Drawing.Point(126, 379);
            this.SubContractorNameTextEdit.MenuManager = this.barManager1;
            this.SubContractorNameTextEdit.Name = "SubContractorNameTextEdit";
            this.SubContractorNameTextEdit.Properties.MaxLength = 50;
            this.SubContractorNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubContractorNameTextEdit, true);
            this.SubContractorNameTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubContractorNameTextEdit, optionsSpelling3);
            this.SubContractorNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorNameTextEdit.TabIndex = 45;
            // 
            // SubContractorIDTextEdit
            // 
            this.SubContractorIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "SubContractorID", true));
            this.SubContractorIDTextEdit.Location = new System.Drawing.Point(147, 348);
            this.SubContractorIDTextEdit.MenuManager = this.barManager1;
            this.SubContractorIDTextEdit.Name = "SubContractorIDTextEdit";
            this.SubContractorIDTextEdit.Properties.MaxLength = 50;
            this.SubContractorIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubContractorIDTextEdit, true);
            this.SubContractorIDTextEdit.Size = new System.Drawing.Size(452, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubContractorIDTextEdit, optionsSpelling4);
            this.SubContractorIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SubContractorIDTextEdit.TabIndex = 45;
            // 
            // PDACodeTextEdit
            // 
            this.PDACodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "PDACode", true));
            this.PDACodeTextEdit.Location = new System.Drawing.Point(126, 345);
            this.PDACodeTextEdit.MenuManager = this.barManager1;
            this.PDACodeTextEdit.Name = "PDACodeTextEdit";
            this.PDACodeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PDACodeTextEdit, true);
            this.PDACodeTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PDACodeTextEdit, optionsSpelling5);
            this.PDACodeTextEdit.StyleController = this.dataLayoutControl1;
            this.PDACodeTextEdit.TabIndex = 45;
            // 
            // PDALoginTokenTextEdit
            // 
            this.PDALoginTokenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "PDALoginToken", true));
            this.PDALoginTokenTextEdit.Location = new System.Drawing.Point(126, 321);
            this.PDALoginTokenTextEdit.MenuManager = this.barManager1;
            this.PDALoginTokenTextEdit.Name = "PDALoginTokenTextEdit";
            this.PDALoginTokenTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PDALoginTokenTextEdit, true);
            this.PDALoginTokenTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PDALoginTokenTextEdit, optionsSpelling6);
            this.PDALoginTokenTextEdit.StyleController = this.dataLayoutControl1;
            this.PDALoginTokenTextEdit.TabIndex = 45;
            // 
            // PDAPasswordTextEdit
            // 
            this.PDAPasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "PDAPassword", true));
            this.PDAPasswordTextEdit.Location = new System.Drawing.Point(126, 297);
            this.PDAPasswordTextEdit.MenuManager = this.barManager1;
            this.PDAPasswordTextEdit.Name = "PDAPasswordTextEdit";
            this.PDAPasswordTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PDAPasswordTextEdit, true);
            this.PDAPasswordTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PDAPasswordTextEdit, optionsSpelling7);
            this.PDAPasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.PDAPasswordTextEdit.TabIndex = 45;
            // 
            // PDAUserNameTextEdit
            // 
            this.PDAUserNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "PDAUserName", true));
            this.PDAUserNameTextEdit.Location = new System.Drawing.Point(126, 273);
            this.PDAUserNameTextEdit.MenuManager = this.barManager1;
            this.PDAUserNameTextEdit.Name = "PDAUserNameTextEdit";
            this.PDAUserNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PDAUserNameTextEdit, true);
            this.PDAUserNameTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PDAUserNameTextEdit, optionsSpelling8);
            this.PDAUserNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PDAUserNameTextEdit.TabIndex = 45;
            // 
            // SoftwareVersionTextEdit
            // 
            this.SoftwareVersionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "SoftwareVersion", true));
            this.SoftwareVersionTextEdit.Location = new System.Drawing.Point(126, 239);
            this.SoftwareVersionTextEdit.MenuManager = this.barManager1;
            this.SoftwareVersionTextEdit.Name = "SoftwareVersionTextEdit";
            this.SoftwareVersionTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SoftwareVersionTextEdit, true);
            this.SoftwareVersionTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SoftwareVersionTextEdit, optionsSpelling9);
            this.SoftwareVersionTextEdit.StyleController = this.dataLayoutControl1;
            this.SoftwareVersionTextEdit.TabIndex = 44;
            // 
            // IMEITextEdit
            // 
            this.IMEITextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "IMEI", true));
            this.IMEITextEdit.Location = new System.Drawing.Point(126, 215);
            this.IMEITextEdit.MenuManager = this.barManager1;
            this.IMEITextEdit.Name = "IMEITextEdit";
            this.IMEITextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.IMEITextEdit, true);
            this.IMEITextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.IMEITextEdit, optionsSpelling10);
            this.IMEITextEdit.StyleController = this.dataLayoutControl1;
            this.IMEITextEdit.TabIndex = 43;
            // 
            // MobileTelNoTextEdit
            // 
            this.MobileTelNoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "MobileTelNo", true));
            this.MobileTelNoTextEdit.Location = new System.Drawing.Point(126, 191);
            this.MobileTelNoTextEdit.MenuManager = this.barManager1;
            this.MobileTelNoTextEdit.Name = "MobileTelNoTextEdit";
            this.MobileTelNoTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MobileTelNoTextEdit, true);
            this.MobileTelNoTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MobileTelNoTextEdit, optionsSpelling11);
            this.MobileTelNoTextEdit.StyleController = this.dataLayoutControl1;
            this.MobileTelNoTextEdit.TabIndex = 42;
            // 
            // DatePurchasedDateEdit
            // 
            this.DatePurchasedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "DatePurchased", true));
            this.DatePurchasedDateEdit.EditValue = null;
            this.DatePurchasedDateEdit.Location = new System.Drawing.Point(126, 167);
            this.DatePurchasedDateEdit.MenuManager = this.barManager1;
            this.DatePurchasedDateEdit.Name = "DatePurchasedDateEdit";
            this.DatePurchasedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DatePurchasedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DatePurchasedDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DatePurchasedDateEdit.Size = new System.Drawing.Size(478, 20);
            this.DatePurchasedDateEdit.StyleController = this.dataLayoutControl1;
            this.DatePurchasedDateEdit.TabIndex = 42;
            // 
            // FirmwareVersionTextEdit
            // 
            this.FirmwareVersionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "FirmwareVersion", true));
            this.FirmwareVersionTextEdit.Location = new System.Drawing.Point(126, 143);
            this.FirmwareVersionTextEdit.MenuManager = this.barManager1;
            this.FirmwareVersionTextEdit.Name = "FirmwareVersionTextEdit";
            this.FirmwareVersionTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.FirmwareVersionTextEdit, true);
            this.FirmwareVersionTextEdit.Size = new System.Drawing.Size(478, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.FirmwareVersionTextEdit, optionsSpelling12);
            this.FirmwareVersionTextEdit.StyleController = this.dataLayoutControl1;
            this.FirmwareVersionTextEdit.TabIndex = 41;
            // 
            // SerialNumberTextEdit
            // 
            this.SerialNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "SerialNumber", true));
            this.SerialNumberTextEdit.Location = new System.Drawing.Point(114, 83);
            this.SerialNumberTextEdit.MenuManager = this.barManager1;
            this.SerialNumberTextEdit.Name = "SerialNumberTextEdit";
            this.SerialNumberTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SerialNumberTextEdit, true);
            this.SerialNumberTextEdit.Size = new System.Drawing.Size(502, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SerialNumberTextEdit, optionsSpelling13);
            this.SerialNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.SerialNumberTextEdit.TabIndex = 40;
            // 
            // ModelTextEdit
            // 
            this.ModelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "Model", true));
            this.ModelTextEdit.Location = new System.Drawing.Point(114, 59);
            this.ModelTextEdit.MenuManager = this.barManager1;
            this.ModelTextEdit.Name = "ModelTextEdit";
            this.ModelTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ModelTextEdit, true);
            this.ModelTextEdit.Size = new System.Drawing.Size(502, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ModelTextEdit, optionsSpelling14);
            this.ModelTextEdit.StyleController = this.dataLayoutControl1;
            this.ModelTextEdit.TabIndex = 39;
            this.ModelTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ModelTextEdit_Validating);
            // 
            // MakeTextEdit
            // 
            this.MakeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "Make", true));
            this.MakeTextEdit.Location = new System.Drawing.Point(114, 35);
            this.MakeTextEdit.MenuManager = this.barManager1;
            this.MakeTextEdit.Name = "MakeTextEdit";
            this.MakeTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.MakeTextEdit, true);
            this.MakeTextEdit.Size = new System.Drawing.Size(502, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.MakeTextEdit, optionsSpelling15);
            this.MakeTextEdit.StyleController = this.dataLayoutControl1;
            this.MakeTextEdit.TabIndex = 38;
            this.MakeTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.MakeTextEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(24, 143);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(580, 280);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling16);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 37;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00242PDAEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(115, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // PDA_IDTextEdit
            // 
            this.PDA_IDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00242PDAEditBindingSource, "PDA_ID", true));
            this.PDA_IDTextEdit.Location = new System.Drawing.Point(133, 35);
            this.PDA_IDTextEdit.MenuManager = this.barManager1;
            this.PDA_IDTextEdit.Name = "PDA_IDTextEdit";
            this.PDA_IDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PDA_IDTextEdit, true);
            this.PDA_IDTextEdit.Size = new System.Drawing.Size(466, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PDA_IDTextEdit, optionsSpelling17);
            this.PDA_IDTextEdit.StyleController = this.dataLayoutControl1;
            this.PDA_IDTextEdit.TabIndex = 4;
            // 
            // ItemForSubContractorID
            // 
            this.ItemForSubContractorID.Control = this.SubContractorIDTextEdit;
            this.ItemForSubContractorID.Location = new System.Drawing.Point(0, 240);
            this.ItemForSubContractorID.Name = "ItemForSubContractorID";
            this.ItemForSubContractorID.Size = new System.Drawing.Size(591, 24);
            this.ItemForSubContractorID.Text = "Allocated To Contractor ID:";
            this.ItemForSubContractorID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForPDA_ID
            // 
            this.ItemForPDA_ID.Control = this.PDA_IDTextEdit;
            this.ItemForPDA_ID.CustomizationFormText = "Device ID:";
            this.ItemForPDA_ID.Location = new System.Drawing.Point(0, 23);
            this.ItemForPDA_ID.Name = "ItemForPDA_ID";
            this.ItemForPDA_ID.Size = new System.Drawing.Size(591, 24);
            this.ItemForPDA_ID.Text = "Device ID:";
            this.ItemForPDA_ID.TextSize = new System.Drawing.Size(118, 13);
            // 
            // ItemForLinkedToPersonTypeID
            // 
            this.ItemForLinkedToPersonTypeID.Control = this.LinkedToPersonTypeIDTextEdit;
            this.ItemForLinkedToPersonTypeID.Location = new System.Drawing.Point(0, 260);
            this.ItemForLinkedToPersonTypeID.Name = "ItemForLinkedToPersonTypeID";
            this.ItemForLinkedToPersonTypeID.Size = new System.Drawing.Size(567, 24);
            this.ItemForLinkedToPersonTypeID.Text = "Labour Type ID:";
            this.ItemForLinkedToPersonTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 476);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.ItemForMake,
            this.ItemForModel});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 71);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(103, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(103, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(103, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(280, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(328, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(103, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForMake
            // 
            this.ItemForMake.Control = this.MakeTextEdit;
            this.ItemForMake.Location = new System.Drawing.Point(0, 23);
            this.ItemForMake.Name = "ItemForMake";
            this.ItemForMake.Size = new System.Drawing.Size(608, 24);
            this.ItemForMake.Text = "Make:";
            this.ItemForMake.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForModel
            // 
            this.ItemForModel.Control = this.ModelTextEdit;
            this.ItemForModel.Location = new System.Drawing.Point(0, 47);
            this.ItemForModel.Name = "ItemForModel";
            this.ItemForModel.Size = new System.Drawing.Size(608, 24);
            this.ItemForModel.Text = "Model:";
            this.ItemForModel.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3,
            this.layoutControlItem2,
            this.tabbedControlGroup2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 71);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(608, 385);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 356);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(608, 29);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.SerialNumberTextEdit;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(608, 24);
            this.layoutControlItem2.Text = "Serial Number:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(99, 13);
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 24);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(608, 332);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup5});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForFirmwareVersion,
            this.ItemForDatePurchased,
            this.ItemForMobileTelNo,
            this.ItemForIMEI,
            this.ItemForSoftwareVersion,
            this.ItemForPDAUserName,
            this.ItemForPDAPassword,
            this.ItemForPDALoginToken,
            this.ItemForPDACode,
            this.ItemForSubContractorName,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.ItemForLinkedToPersonType});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(584, 284);
            this.layoutControlGroup4.Text = "Details";
            // 
            // ItemForFirmwareVersion
            // 
            this.ItemForFirmwareVersion.Control = this.FirmwareVersionTextEdit;
            this.ItemForFirmwareVersion.Location = new System.Drawing.Point(0, 0);
            this.ItemForFirmwareVersion.Name = "ItemForFirmwareVersion";
            this.ItemForFirmwareVersion.Size = new System.Drawing.Size(584, 24);
            this.ItemForFirmwareVersion.Text = "Firmware Version:";
            this.ItemForFirmwareVersion.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForDatePurchased
            // 
            this.ItemForDatePurchased.Control = this.DatePurchasedDateEdit;
            this.ItemForDatePurchased.Location = new System.Drawing.Point(0, 24);
            this.ItemForDatePurchased.Name = "ItemForDatePurchased";
            this.ItemForDatePurchased.Size = new System.Drawing.Size(584, 24);
            this.ItemForDatePurchased.Text = "Date Purchased:";
            this.ItemForDatePurchased.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForMobileTelNo
            // 
            this.ItemForMobileTelNo.Control = this.MobileTelNoTextEdit;
            this.ItemForMobileTelNo.Location = new System.Drawing.Point(0, 48);
            this.ItemForMobileTelNo.Name = "ItemForMobileTelNo";
            this.ItemForMobileTelNo.Size = new System.Drawing.Size(584, 24);
            this.ItemForMobileTelNo.Text = "Mobile Tel No:";
            this.ItemForMobileTelNo.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForIMEI
            // 
            this.ItemForIMEI.Control = this.IMEITextEdit;
            this.ItemForIMEI.Location = new System.Drawing.Point(0, 72);
            this.ItemForIMEI.Name = "ItemForIMEI";
            this.ItemForIMEI.Size = new System.Drawing.Size(584, 24);
            this.ItemForIMEI.Text = "IMEI:";
            this.ItemForIMEI.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForSoftwareVersion
            // 
            this.ItemForSoftwareVersion.Control = this.SoftwareVersionTextEdit;
            this.ItemForSoftwareVersion.Location = new System.Drawing.Point(0, 96);
            this.ItemForSoftwareVersion.Name = "ItemForSoftwareVersion";
            this.ItemForSoftwareVersion.Size = new System.Drawing.Size(584, 24);
            this.ItemForSoftwareVersion.Text = "Software Version:";
            this.ItemForSoftwareVersion.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForPDAUserName
            // 
            this.ItemForPDAUserName.Control = this.PDAUserNameTextEdit;
            this.ItemForPDAUserName.Location = new System.Drawing.Point(0, 130);
            this.ItemForPDAUserName.Name = "ItemForPDAUserName";
            this.ItemForPDAUserName.Size = new System.Drawing.Size(584, 24);
            this.ItemForPDAUserName.Text = "Device User Name:";
            this.ItemForPDAUserName.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForPDAPassword
            // 
            this.ItemForPDAPassword.Control = this.PDAPasswordTextEdit;
            this.ItemForPDAPassword.Location = new System.Drawing.Point(0, 154);
            this.ItemForPDAPassword.Name = "ItemForPDAPassword";
            this.ItemForPDAPassword.Size = new System.Drawing.Size(584, 24);
            this.ItemForPDAPassword.Text = "Device Password:";
            this.ItemForPDAPassword.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForPDALoginToken
            // 
            this.ItemForPDALoginToken.Control = this.PDALoginTokenTextEdit;
            this.ItemForPDALoginToken.Location = new System.Drawing.Point(0, 178);
            this.ItemForPDALoginToken.Name = "ItemForPDALoginToken";
            this.ItemForPDALoginToken.Size = new System.Drawing.Size(584, 24);
            this.ItemForPDALoginToken.Text = "Device Login Token:";
            this.ItemForPDALoginToken.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForPDACode
            // 
            this.ItemForPDACode.Control = this.PDACodeTextEdit;
            this.ItemForPDACode.Location = new System.Drawing.Point(0, 202);
            this.ItemForPDACode.Name = "ItemForPDACode";
            this.ItemForPDACode.Size = new System.Drawing.Size(584, 24);
            this.ItemForPDACode.Text = "Device Code:";
            this.ItemForPDACode.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForSubContractorName
            // 
            this.ItemForSubContractorName.Control = this.SubContractorNameTextEdit;
            this.ItemForSubContractorName.Location = new System.Drawing.Point(0, 236);
            this.ItemForSubContractorName.Name = "ItemForSubContractorName";
            this.ItemForSubContractorName.Size = new System.Drawing.Size(584, 24);
            this.ItemForSubContractorName.Text = "Allocated To Labour:";
            this.ItemForSubContractorName.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 226);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(584, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(584, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLinkedToPersonType
            // 
            this.ItemForLinkedToPersonType.Control = this.LinkedToPersonTypeTextEdit;
            this.ItemForLinkedToPersonType.Location = new System.Drawing.Point(0, 260);
            this.ItemForLinkedToPersonType.Name = "ItemForLinkedToPersonType";
            this.ItemForLinkedToPersonType.Size = new System.Drawing.Size(584, 24);
            this.ItemForLinkedToPersonType.Text = "Labour Type:";
            this.ItemForLinkedToPersonType.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CaptionImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup5.CaptionImageOptions.Image")));
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(584, 284);
            this.layoutControlGroup5.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(584, 284);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00242_PDA_EditTableAdapter
            // 
            this.sp00242_PDA_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_PDA_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 530);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_PDA_Edit";
            this.Text = "Edit Device";
            this.Activated += new System.EventHandler(this.frm_Core_PDA_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_PDA_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_PDA_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00242PDAEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubContractorIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDACodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDALoginTokenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDAPasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDAUserNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareVersionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IMEITextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobileTelNoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatePurchasedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DatePurchasedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirmwareVersionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SerialNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ModelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MakeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PDA_IDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDA_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForModel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFirmwareVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDatePurchased)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMobileTelNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIMEI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSoftwareVersion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDAUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDAPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDALoginToken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPDACode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubContractorName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit PDA_IDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDA_ID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private System.Windows.Forms.BindingSource sp00242PDAEditBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DataSet_Common_FunctionalityTableAdapters.sp00242_PDA_EditTableAdapter sp00242_PDA_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit MakeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMake;
        private DevExpress.XtraEditors.TextEdit ModelTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForModel;
        private DevExpress.XtraEditors.TextEdit SerialNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit FirmwareVersionTextEdit;
        private DevExpress.XtraEditors.DateEdit DatePurchasedDateEdit;
        private DevExpress.XtraEditors.TextEdit MobileTelNoTextEdit;
        private DevExpress.XtraEditors.TextEdit IMEITextEdit;
        private DevExpress.XtraEditors.TextEdit SoftwareVersionTextEdit;
        private DevExpress.XtraEditors.TextEdit SubContractorNameTextEdit;
        private DevExpress.XtraEditors.TextEdit SubContractorIDTextEdit;
        private DevExpress.XtraEditors.TextEdit PDACodeTextEdit;
        private DevExpress.XtraEditors.TextEdit PDALoginTokenTextEdit;
        private DevExpress.XtraEditors.TextEdit PDAPasswordTextEdit;
        private DevExpress.XtraEditors.TextEdit PDAUserNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorID;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFirmwareVersion;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDatePurchased;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMobileTelNo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIMEI;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSoftwareVersion;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDAUserName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDAPassword;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDALoginToken;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPDACode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubContractorName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonTypeTextEdit;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonType;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

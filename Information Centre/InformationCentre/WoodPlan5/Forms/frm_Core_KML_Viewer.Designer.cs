﻿namespace WoodPlan5
{
    partial class frm_Core_KML_Viewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_KML_Viewer));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.popupContainerControlMapType = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControlMapType = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.btnOKMapType = new DevExpress.XtraEditors.SimpleButton();
            this.mapControl1 = new BaseObjects.ExtendedMapControl();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.barEditItemMapType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditMapType = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.imageCollection16x16 = new DevExpress.Utils.ImageCollection(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bciNavigation = new DevExpress.XtraBars.BarCheckItem();
            this.bciMiniMap = new DevExpress.XtraBars.BarCheckItem();
            this.bsiMapObjectCount = new DevExpress.XtraBars.BarStaticItem();
            this.bbiMapPrintPreview = new DevExpress.XtraBars.BarButtonItem();
            this.pmMapControl = new DevExpress.XtraBars.PopupMenu(this.components);
            this.imageCollection32x32 = new DevExpress.Utils.ImageCollection(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiOpenKMLFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiZoomToFile = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClearKML = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPrint = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveImage = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMapType)).BeginInit();
            this.popupContainerControlMapType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMapType)).BeginInit();
            this.groupControlMapType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditMapType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection16x16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection32x32)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(891, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 624);
            this.barDockControlBottom.Size = new System.Drawing.Size(891, 42);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 582);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(891, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 582);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar1});
            this.barManager1.DockManager = this.dockManager1;
            this.barManager1.Images = this.imageCollection32x32;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemMapType,
            this.bsiMapObjectCount,
            this.bbiMapPrintPreview,
            this.bciMiniMap,
            this.bciNavigation,
            this.bbiOpenKMLFile,
            this.bbiZoomToFile,
            this.bbiClearKML,
            this.bbiPrint,
            this.bbiSaveImage});
            this.barManager1.MaxItemId = 58;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRadioGroup1,
            this.repositoryItemPopupContainerEditMapType});
            this.barManager1.StatusBar = this.bar2;
            // 
            // popupContainerControlMapType
            // 
            this.popupContainerControlMapType.Controls.Add(this.groupControlMapType);
            this.popupContainerControlMapType.Controls.Add(this.btnOKMapType);
            this.popupContainerControlMapType.Location = new System.Drawing.Point(456, 72);
            this.popupContainerControlMapType.Name = "popupContainerControlMapType";
            this.popupContainerControlMapType.Size = new System.Drawing.Size(129, 157);
            this.popupContainerControlMapType.TabIndex = 9;
            // 
            // groupControlMapType
            // 
            this.groupControlMapType.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlMapType.Controls.Add(this.checkEdit4);
            this.groupControlMapType.Controls.Add(this.checkEdit3);
            this.groupControlMapType.Controls.Add(this.checkEdit2);
            this.groupControlMapType.Controls.Add(this.checkEdit1);
            this.groupControlMapType.Location = new System.Drawing.Point(3, 4);
            this.groupControlMapType.Name = "groupControlMapType";
            this.groupControlMapType.Size = new System.Drawing.Size(123, 123);
            this.groupControlMapType.TabIndex = 3;
            this.groupControlMapType.Text = "Map Type";
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(5, 99);
            this.checkEdit4.MenuManager = this.barManager1;
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "Open Street Map";
            this.checkEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit4.Properties.RadioGroupIndex = 1;
            this.checkEdit4.Size = new System.Drawing.Size(110, 19);
            this.checkEdit4.TabIndex = 3;
            this.checkEdit4.TabStop = false;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(5, 74);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "Bing Road";
            this.checkEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit3.Properties.RadioGroupIndex = 1;
            this.checkEdit3.Size = new System.Drawing.Size(110, 19);
            this.checkEdit3.TabIndex = 2;
            this.checkEdit3.TabStop = false;
            // 
            // checkEdit2
            // 
            this.checkEdit2.EditValue = true;
            this.checkEdit2.Location = new System.Drawing.Point(5, 49);
            this.checkEdit2.MenuManager = this.barManager1;
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "Bing Satellite";
            this.checkEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit2.Properties.RadioGroupIndex = 1;
            this.checkEdit2.Size = new System.Drawing.Size(110, 19);
            this.checkEdit2.TabIndex = 1;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(5, 24);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "Bing Hybrid";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(110, 19);
            this.checkEdit1.TabIndex = 0;
            this.checkEdit1.TabStop = false;
            // 
            // btnOKMapType
            // 
            this.btnOKMapType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOKMapType.Location = new System.Drawing.Point(3, 131);
            this.btnOKMapType.Name = "btnOKMapType";
            this.btnOKMapType.Size = new System.Drawing.Size(38, 23);
            this.btnOKMapType.TabIndex = 2;
            this.btnOKMapType.Text = "OK";
            this.btnOKMapType.Click += new System.EventHandler(this.btnOKMapType_Click);
            // 
            // mapControl1
            // 
            this.mapControl1.CenterPoint = new DevExpress.XtraMap.GeoPoint(51.63060168D, 0.41083011D);
            this.mapControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapControl1.Location = new System.Drawing.Point(0, 42);
            this.mapControl1.Name = "mapControl1";
            this.mapControl1.Size = new System.Drawing.Size(891, 582);
            this.mapControl1.TabIndex = 0;
            this.mapControl1.ToolTipController = this.toolTipController1;
            this.mapControl1.ZoomLevel = 12D;
            this.mapControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.mapControl1_MouseUp);
            // 
            // toolTipController1
            // 
            this.toolTipController1.AllowHtmlText = true;
            // 
            // barEditItemMapType
            // 
            this.barEditItemMapType.Caption = "Map Type";
            this.barEditItemMapType.Edit = this.repositoryItemPopupContainerEditMapType;
            this.barEditItemMapType.EditValue = "Bing Satellite";
            this.barEditItemMapType.EditWidth = 105;
            this.barEditItemMapType.Id = 33;
            this.barEditItemMapType.Name = "barEditItemMapType";
            // 
            // repositoryItemPopupContainerEditMapType
            // 
            this.repositoryItemPopupContainerEditMapType.AutoHeight = false;
            this.repositoryItemPopupContainerEditMapType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditMapType.Name = "repositoryItemPopupContainerEditMapType";
            this.repositoryItemPopupContainerEditMapType.PopupControl = this.popupContainerControlMapType;
            this.repositoryItemPopupContainerEditMapType.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditMapType_QueryResultValue);
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Bing Hybid"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "Bing Satellite"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Bing Road"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "Open Street Map")});
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // dockManager1
            // 
            this.dockManager1.DockingOptions.HideImmediatelyOnAutoHide = true;
            this.dockManager1.DockModeVS2005FadeFramesCount = 1;
            this.dockManager1.DockModeVS2005FadeSpeed = 1;
            this.dockManager1.Form = this;
            this.dockManager1.MenuManager = this.barManager1;
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // imageCollection16x16
            // 
            this.imageCollection16x16.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection16x16.ImageStream")));
            this.imageCollection16x16.InsertGalleryImage("geopoint_16x16.png", "images/maps/geopoint_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopoint_16x16.png"), 0);
            this.imageCollection16x16.Images.SetKeyName(0, "geopoint_16x16.png");
            this.imageCollection16x16.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection16x16.Images.SetKeyName(1, "refresh_16x16");
            this.imageCollection16x16.InsertGalleryImage("next_16x16.png", "images/navigation/next_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/next_16x16.png"), 2);
            this.imageCollection16x16.Images.SetKeyName(2, "next_16x16.png");
            this.imageCollection16x16.InsertGalleryImage("previous_16x16.png", "images/navigation/previous_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/navigation/previous_16x16.png"), 3);
            this.imageCollection16x16.Images.SetKeyName(3, "previous_16x16.png");
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemMapType),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciNavigation, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciMiniMap, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiMapObjectCount, true)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 3";
            // 
            // bciNavigation
            // 
            this.bciNavigation.BindableChecked = true;
            this.bciNavigation.Caption = "Navigation Pane";
            this.bciNavigation.Checked = true;
            this.bciNavigation.Glyph = ((System.Drawing.Image)(resources.GetObject("bciNavigation.Glyph")));
            this.bciNavigation.Id = 51;
            this.bciNavigation.Name = "bciNavigation";
            toolTipTitleItem1.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem1.Text = "Navigation Pane - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to Show \\ Hide the Navigation Pane.\r\n\r\nThe Navigation Pane shows you the" +
    " current Latitude and Longitude of the mouse pointer when over the map and allow" +
    "s you to pan and zoom the map.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bciNavigation.SuperTip = superToolTip1;
            this.bciNavigation.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciNavigation_CheckedChanged);
            // 
            // bciMiniMap
            // 
            this.bciMiniMap.BindableChecked = true;
            this.bciMiniMap.Caption = "Mini Map";
            this.bciMiniMap.Checked = true;
            this.bciMiniMap.Glyph = ((System.Drawing.Image)(resources.GetObject("bciMiniMap.Glyph")));
            this.bciMiniMap.Id = 49;
            this.bciMiniMap.Name = "bciMiniMap";
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Mini Map - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to Show \\ Hide the Mini Map.\r\n\r\nThe Mini Map provides overview navigatio" +
    "n of the map.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bciMiniMap.SuperTip = superToolTip2;
            this.bciMiniMap.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciMiniMap_CheckedChanged);
            // 
            // bsiMapObjectCount
            // 
            this.bsiMapObjectCount.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bsiMapObjectCount.Caption = "Map Object Count: 0";
            this.bsiMapObjectCount.Id = 35;
            this.bsiMapObjectCount.Name = "bsiMapObjectCount";
            this.bsiMapObjectCount.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // bbiMapPrintPreview
            // 
            this.bbiMapPrintPreview.Caption = "Print Preview";
            this.bbiMapPrintPreview.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiMapPrintPreview.Glyph")));
            this.bbiMapPrintPreview.Id = 40;
            this.bbiMapPrintPreview.Name = "bbiMapPrintPreview";
            this.bbiMapPrintPreview.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMapPrintPreview_ItemClick);
            // 
            // pmMapControl
            // 
            this.pmMapControl.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiMapPrintPreview)});
            this.pmMapControl.Manager = this.barManager1;
            this.pmMapControl.MenuCaption = "Map Menu";
            this.pmMapControl.Name = "pmMapControl";
            this.pmMapControl.ShowCaption = true;
            // 
            // imageCollection32x32
            // 
            this.imageCollection32x32.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection32x32.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection32x32.ImageStream")));
            this.imageCollection32x32.InsertGalleryImage("info_32x32.png", "images/support/info_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/info_32x32.png"), 0);
            this.imageCollection32x32.Images.SetKeyName(0, "info_32x32.png");
            this.imageCollection32x32.InsertGalleryImage("exporttoxls_32x32.png", "images/export/exporttoxls_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/export/exporttoxls_32x32.png"), 1);
            this.imageCollection32x32.Images.SetKeyName(1, "exporttoxls_32x32.png");
            this.imageCollection32x32.InsertGalleryImage("geopointmap_32x32.png", "images/maps/geopointmap_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/maps/geopointmap_32x32.png"), 2);
            this.imageCollection32x32.Images.SetKeyName(2, "geopointmap_32x32.png");
            this.imageCollection32x32.InsertGalleryImage("merge_32x32.png", "images/actions/merge_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/merge_32x32.png"), 3);
            this.imageCollection32x32.Images.SetKeyName(3, "merge_32x32.png");
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 4";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiOpenKMLFile),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiZoomToFile, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiClearKML, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiPrint, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveImage, true)});
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 4";
            // 
            // bbiOpenKMLFile
            // 
            this.bbiOpenKMLFile.Caption = "Open KML File";
            this.bbiOpenKMLFile.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiOpenKMLFile.Glyph")));
            this.bbiOpenKMLFile.Id = 52;
            this.bbiOpenKMLFile.Name = "bbiOpenKMLFile";
            this.bbiOpenKMLFile.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiOpenKMLFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiOpenKMLFile_ItemClick);
            // 
            // bbiZoomToFile
            // 
            this.bbiZoomToFile.Caption = "Zoom to KML";
            this.bbiZoomToFile.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiZoomToFile.Glyph")));
            this.bbiZoomToFile.Id = 53;
            this.bbiZoomToFile.Name = "bbiZoomToFile";
            this.bbiZoomToFile.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiZoomToFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiZoomToFile_ItemClick);
            // 
            // bbiClearKML
            // 
            this.bbiClearKML.Caption = "Clear KML File";
            this.bbiClearKML.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiClearKML.Glyph")));
            this.bbiClearKML.Id = 54;
            this.bbiClearKML.Name = "bbiClearKML";
            this.bbiClearKML.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiClearKML.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiClearKML_ItemClick);
            // 
            // bbiPrint
            // 
            this.bbiPrint.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiPrint.Caption = "Print";
            this.bbiPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiPrint.Glyph")));
            this.bbiPrint.Id = 55;
            this.bbiPrint.Name = "bbiPrint";
            this.bbiPrint.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPrint_ItemClick);
            // 
            // bbiSaveImage
            // 
            this.bbiSaveImage.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiSaveImage.Caption = "Save Image";
            this.bbiSaveImage.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveImage.Glyph")));
            this.bbiSaveImage.Id = 57;
            this.bbiSaveImage.Name = "bbiSaveImage";
            this.bbiSaveImage.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiSaveImage.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveImage_ItemClick);
            // 
            // frm_Core_KML_Viewer
            // 
            this.ClientSize = new System.Drawing.Size(891, 666);
            this.Controls.Add(this.popupContainerControlMapType);
            this.Controls.Add(this.mapControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_KML_Viewer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "KML File Viewer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_KML_Viewer_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_KML_Viewer_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.mapControl1, 0);
            this.Controls.SetChildIndex(this.popupContainerControlMapType, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlMapType)).EndInit();
            this.popupContainerControlMapType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlMapType)).EndInit();
            this.groupControlMapType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mapControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditMapType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection16x16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmMapControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection32x32)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseObjects.ExtendedMapControl mapControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraBars.BarEditItem barEditItemMapType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditMapType;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlMapType;
        private DevExpress.XtraEditors.GroupControl groupControlMapType;
        private DevExpress.XtraEditors.SimpleButton btnOKMapType;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarStaticItem bsiMapObjectCount;
        private DevExpress.XtraBars.BarButtonItem bbiMapPrintPreview;
        private DevExpress.XtraBars.PopupMenu pmMapControl;
        private DevExpress.Utils.ImageCollection imageCollection32x32;
        private DevExpress.XtraBars.BarCheckItem bciMiniMap;
        private DevExpress.Utils.ImageCollection imageCollection16x16;
        private DevExpress.XtraBars.BarCheckItem bciNavigation;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiOpenKMLFile;
        private DevExpress.XtraBars.BarButtonItem bbiZoomToFile;
        private DevExpress.XtraBars.BarButtonItem bbiClearKML;
        private DevExpress.XtraBars.BarButtonItem bbiPrint;
        private DevExpress.XtraBars.BarButtonItem bbiSaveImage;

    }
}

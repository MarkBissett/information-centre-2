namespace WoodPlan5
{
    partial class frm_HR_Select_Department_Worked
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00153EmployeeDeptsWorkedBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeDeptWorkedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPositionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportsToStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBaseType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurnameForename2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeForename2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportsToStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00153_Employee_Depts_WorkedTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00153_Employee_Depts_WorkedTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00153EmployeeDeptsWorkedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(711, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(711, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(711, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 5;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00153EmployeeDeptsWorkedBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(710, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00153EmployeeDeptsWorkedBindingSource
            // 
            this.spHR00153EmployeeDeptsWorkedBindingSource.DataMember = "sp_HR_00153_Employee_Depts_Worked";
            this.spHR00153EmployeeDeptsWorkedBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeDeptWorkedID,
            this.colEmployeeID,
            this.colDepartmentLocationID,
            this.colRegionID,
            this.colBaseTypeID,
            this.colPositionID,
            this.colReportsToStaffID,
            this.colTimePercentage,
            this.colFromDate1,
            this.colToDate,
            this.colActive,
            this.colRemarks,
            this.colBaseType,
            this.colEmployeeSurnameForename2,
            this.colEmployeeSurname3,
            this.colEmployeeForename2,
            this.colEmployeeNumber6,
            this.colContractNumber2,
            this.colReportsToStaff,
            this.colDepartmentName,
            this.colLocationName,
            this.colBusinessAreaName,
            this.colRegionName,
            this.colCostCentreCode,
            this.colEmployeeID6});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colEmployeeDeptWorkedID
            // 
            this.colEmployeeDeptWorkedID.Caption = "Employee Department Worked ID";
            this.colEmployeeDeptWorkedID.FieldName = "EmployeeDeptWorkedID";
            this.colEmployeeDeptWorkedID.Name = "colEmployeeDeptWorkedID";
            this.colEmployeeDeptWorkedID.OptionsColumn.AllowEdit = false;
            this.colEmployeeDeptWorkedID.OptionsColumn.AllowFocus = false;
            this.colEmployeeDeptWorkedID.OptionsColumn.ReadOnly = true;
            this.colEmployeeDeptWorkedID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeDeptWorkedID.Width = 181;
            // 
            // colEmployeeID
            // 
            this.colEmployeeID.Caption = "Employee ID";
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.Name = "colEmployeeID";
            this.colEmployeeID.OptionsColumn.AllowEdit = false;
            this.colEmployeeID.OptionsColumn.AllowFocus = false;
            this.colEmployeeID.OptionsColumn.ReadOnly = true;
            this.colEmployeeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID.Width = 126;
            // 
            // colDepartmentLocationID
            // 
            this.colDepartmentLocationID.Caption = "Department Location ID";
            this.colDepartmentLocationID.FieldName = "DepartmentLocationID";
            this.colDepartmentLocationID.Name = "colDepartmentLocationID";
            this.colDepartmentLocationID.OptionsColumn.AllowEdit = false;
            this.colDepartmentLocationID.OptionsColumn.AllowFocus = false;
            this.colDepartmentLocationID.OptionsColumn.ReadOnly = true;
            this.colDepartmentLocationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentLocationID.Width = 135;
            // 
            // colRegionID
            // 
            this.colRegionID.Caption = "Region ID";
            this.colRegionID.FieldName = "RegionID";
            this.colRegionID.Name = "colRegionID";
            this.colRegionID.OptionsColumn.AllowEdit = false;
            this.colRegionID.OptionsColumn.AllowFocus = false;
            this.colRegionID.OptionsColumn.ReadOnly = true;
            this.colRegionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colBaseTypeID
            // 
            this.colBaseTypeID.Caption = "Base Type ID";
            this.colBaseTypeID.FieldName = "BaseTypeID";
            this.colBaseTypeID.Name = "colBaseTypeID";
            this.colBaseTypeID.OptionsColumn.AllowEdit = false;
            this.colBaseTypeID.OptionsColumn.AllowFocus = false;
            this.colBaseTypeID.OptionsColumn.ReadOnly = true;
            this.colBaseTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBaseTypeID.Width = 85;
            // 
            // colPositionID
            // 
            this.colPositionID.Caption = "Position ID";
            this.colPositionID.FieldName = "PositionID";
            this.colPositionID.Name = "colPositionID";
            this.colPositionID.OptionsColumn.AllowEdit = false;
            this.colPositionID.OptionsColumn.AllowFocus = false;
            this.colPositionID.OptionsColumn.ReadOnly = true;
            this.colPositionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colReportsToStaffID
            // 
            this.colReportsToStaffID.Caption = "Report to Staff ID";
            this.colReportsToStaffID.FieldName = "ReportsToStaffID";
            this.colReportsToStaffID.Name = "colReportsToStaffID";
            this.colReportsToStaffID.OptionsColumn.AllowEdit = false;
            this.colReportsToStaffID.OptionsColumn.AllowFocus = false;
            this.colReportsToStaffID.OptionsColumn.ReadOnly = true;
            this.colReportsToStaffID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportsToStaffID.Width = 108;
            // 
            // colTimePercentage
            // 
            this.colTimePercentage.Caption = "Time Percentage";
            this.colTimePercentage.FieldName = "TimePercentage";
            this.colTimePercentage.Name = "colTimePercentage";
            this.colTimePercentage.OptionsColumn.AllowEdit = false;
            this.colTimePercentage.OptionsColumn.AllowFocus = false;
            this.colTimePercentage.OptionsColumn.ReadOnly = true;
            this.colTimePercentage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTimePercentage.Visible = true;
            this.colTimePercentage.VisibleIndex = 9;
            this.colTimePercentage.Width = 101;
            // 
            // colFromDate1
            // 
            this.colFromDate1.Caption = "From Date";
            this.colFromDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colFromDate1.FieldName = "FromDate";
            this.colFromDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colFromDate1.Name = "colFromDate1";
            this.colFromDate1.OptionsColumn.AllowEdit = false;
            this.colFromDate1.OptionsColumn.AllowFocus = false;
            this.colFromDate1.OptionsColumn.ReadOnly = true;
            this.colFromDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colFromDate1.Visible = true;
            this.colFromDate1.VisibleIndex = 6;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colToDate
            // 
            this.colToDate.Caption = "To Date";
            this.colToDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colToDate.FieldName = "ToDate";
            this.colToDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colToDate.Name = "colToDate";
            this.colToDate.OptionsColumn.AllowEdit = false;
            this.colToDate.OptionsColumn.AllowFocus = false;
            this.colToDate.OptionsColumn.ReadOnly = true;
            this.colToDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colToDate.Visible = true;
            this.colToDate.VisibleIndex = 7;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colBaseType
            // 
            this.colBaseType.Caption = "Base Type";
            this.colBaseType.FieldName = "BaseType";
            this.colBaseType.Name = "colBaseType";
            this.colBaseType.OptionsColumn.AllowEdit = false;
            this.colBaseType.OptionsColumn.AllowFocus = false;
            this.colBaseType.OptionsColumn.ReadOnly = true;
            this.colBaseType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBaseType.Visible = true;
            this.colBaseType.VisibleIndex = 4;
            this.colBaseType.Width = 71;
            // 
            // colEmployeeSurnameForename2
            // 
            this.colEmployeeSurnameForename2.Caption = "Linked To Employee";
            this.colEmployeeSurnameForename2.FieldName = "EmployeeSurnameForename";
            this.colEmployeeSurnameForename2.Name = "colEmployeeSurnameForename2";
            this.colEmployeeSurnameForename2.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurnameForename2.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurnameForename2.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurnameForename2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurnameForename2.Width = 206;
            // 
            // colEmployeeSurname3
            // 
            this.colEmployeeSurname3.Caption = "Surname";
            this.colEmployeeSurname3.FieldName = "EmployeeSurname";
            this.colEmployeeSurname3.Name = "colEmployeeSurname3";
            this.colEmployeeSurname3.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname3.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname3.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEmployeeForename2
            // 
            this.colEmployeeForename2.Caption = "Forename";
            this.colEmployeeForename2.FieldName = "EmployeeForename";
            this.colEmployeeForename2.Name = "colEmployeeForename2";
            this.colEmployeeForename2.OptionsColumn.AllowEdit = false;
            this.colEmployeeForename2.OptionsColumn.AllowFocus = false;
            this.colEmployeeForename2.OptionsColumn.ReadOnly = true;
            this.colEmployeeForename2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEmployeeNumber6
            // 
            this.colEmployeeNumber6.Caption = "Employee #";
            this.colEmployeeNumber6.FieldName = "EmployeeNumber";
            this.colEmployeeNumber6.Name = "colEmployeeNumber6";
            this.colEmployeeNumber6.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber6.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber6.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber6.Width = 78;
            // 
            // colContractNumber2
            // 
            this.colContractNumber2.Caption = "Contract #";
            this.colContractNumber2.FieldName = "ContractNumber";
            this.colContractNumber2.Name = "colContractNumber2";
            this.colContractNumber2.OptionsColumn.AllowEdit = false;
            this.colContractNumber2.OptionsColumn.AllowFocus = false;
            this.colContractNumber2.OptionsColumn.ReadOnly = true;
            this.colContractNumber2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colReportsToStaff
            // 
            this.colReportsToStaff.Caption = "Reports To";
            this.colReportsToStaff.FieldName = "ReportsToStaff";
            this.colReportsToStaff.Name = "colReportsToStaff";
            this.colReportsToStaff.OptionsColumn.AllowEdit = false;
            this.colReportsToStaff.OptionsColumn.AllowFocus = false;
            this.colReportsToStaff.OptionsColumn.ReadOnly = true;
            this.colReportsToStaff.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReportsToStaff.Visible = true;
            this.colReportsToStaff.VisibleIndex = 8;
            this.colReportsToStaff.Width = 141;
            // 
            // colDepartmentName
            // 
            this.colDepartmentName.Caption = "Dept Name";
            this.colDepartmentName.FieldName = "DepartmentName";
            this.colDepartmentName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDepartmentName.Name = "colDepartmentName";
            this.colDepartmentName.OptionsColumn.AllowEdit = false;
            this.colDepartmentName.OptionsColumn.AllowFocus = false;
            this.colDepartmentName.OptionsColumn.ReadOnly = true;
            this.colDepartmentName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentName.Visible = true;
            this.colDepartmentName.VisibleIndex = 1;
            this.colDepartmentName.Width = 117;
            // 
            // colLocationName
            // 
            this.colLocationName.Caption = "Location";
            this.colLocationName.FieldName = "LocationName";
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.OptionsColumn.AllowEdit = false;
            this.colLocationName.OptionsColumn.AllowFocus = false;
            this.colLocationName.OptionsColumn.ReadOnly = true;
            this.colLocationName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLocationName.Visible = true;
            this.colLocationName.VisibleIndex = 3;
            this.colLocationName.Width = 111;
            // 
            // colBusinessAreaName
            // 
            this.colBusinessAreaName.Caption = "Business Area";
            this.colBusinessAreaName.FieldName = "BusinessAreaName";
            this.colBusinessAreaName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBusinessAreaName.Name = "colBusinessAreaName";
            this.colBusinessAreaName.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBusinessAreaName.Visible = true;
            this.colBusinessAreaName.VisibleIndex = 0;
            this.colBusinessAreaName.Width = 100;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 2;
            this.colRegionName.Width = 102;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.Caption = "Cost Centre Code";
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 10;
            this.colCostCentreCode.Width = 107;
            // 
            // colEmployeeID6
            // 
            this.colEmployeeID6.Caption = "Employee ID";
            this.colEmployeeID6.FieldName = "EmployeeID";
            this.colEmployeeID6.Name = "colEmployeeID6";
            this.colEmployeeID6.OptionsColumn.AllowEdit = false;
            this.colEmployeeID6.OptionsColumn.AllowFocus = false;
            this.colEmployeeID6.OptionsColumn.ReadOnly = true;
            this.colEmployeeID6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID6.Width = 81;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(543, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(624, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(710, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00153_Employee_Depts_WorkedTableAdapter
            // 
            this.sp_HR_00153_Employee_Depts_WorkedTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Department_Worked
            // 
            this.ClientSize = new System.Drawing.Size(711, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Department_Worked";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Department Worked";
            this.Load += new System.EventHandler(this.frm_HR_Select_Department_Worked_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00153EmployeeDeptsWorkedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private System.Windows.Forms.BindingSource spHR00153EmployeeDeptsWorkedBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeDeptWorkedID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPositionID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportsToStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colFromDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseType;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurnameForename2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname3;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeForename2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber6;
        private DevExpress.XtraGrid.Columns.GridColumn colContractNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colReportsToStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID6;
        private DataSet_HR_CoreTableAdapters.sp_HR_00153_Employee_Depts_WorkedTableAdapter sp_HR_00153_Employee_Depts_WorkedTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
    }
}

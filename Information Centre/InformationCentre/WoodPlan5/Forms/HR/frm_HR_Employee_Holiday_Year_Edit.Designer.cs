namespace WoodPlan5
{
    partial class frm_HR_Employee_Holiday_Year_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Holiday_Year_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.btnAdjustHolidays = new DevExpress.XtraEditors.SimpleButton();
            this.ActiveHolidayYearCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.spHR00010GetEmployeeHolidayYearItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TotalHolidaysRemainingTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalHolidaysTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalHolidayEntitlementTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BankHolidaysRemainingTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HolidaysRemainingTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AbsenceUnauthorisedTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AbsenceAuthorisedTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BankHolidaysTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HolidaysTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeForenameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeSurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MustTakeCertainHolidaysCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.HoursPerHolidayDaySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.HolidayUnitDescriptorIdGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00082HolidayUnitDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PurchasedHolidayEntitlementSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.BankHolidayEntitlementSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.BasicHolidayEntitlementSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ActualStartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.HolidayYearEndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.HolidayYearStartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.IdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.EmployeeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.EmployeeNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeForename = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForBasicHolidayEntitlement = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBankHolidayEntitlement = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalHolidayEntitlement = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAdjustHolidaysButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPurchasedHolidayEntitlement = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForHolidayUnitDescriptorId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHoursPerHolidayDay = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMustTakeCertainHolidays = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHolidaysTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBankHolidaysTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceAuthorisedTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceUnauthorisedTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForTotalHolidaysTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHolidaysRemaining = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBankHolidaysRemaining = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForTotalHolidaysRemaining = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForActiveHolidayYear = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHolidayYearStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForHolidayYearEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActualStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem14 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.TableAdapterManager();
            this.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter();
            this.sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveHolidayYearCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00010GetEmployeeHolidayYearItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidaysRemainingTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidaysTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidayEntitlementTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidaysRemainingTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidaysRemainingTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceUnauthorisedTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceAuthorisedTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidaysTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidaysTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeForenameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MustTakeCertainHolidaysCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursPerHolidayDaySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayUnitDescriptorIdGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00082HolidayUnitDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchasedHolidayEntitlementSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidayEntitlementSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BasicHolidayEntitlementSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualStartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualStartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearEndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearEndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearStartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearStartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeForename)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBasicHolidayEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidayEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidayEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdjustHolidaysButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchasedHolidayEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayUnitDescriptorId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursPerHolidayDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMustTakeCertainHolidays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidaysTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidaysTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceAuthorisedTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceUnauthorisedTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidaysTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidaysRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidaysRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidaysRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActiveHolidayYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayYearStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayYearEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(851, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 580);
            this.barDockControlBottom.Size = new System.Drawing.Size(851, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 554);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(851, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 554);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(851, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 580);
            this.barDockControl2.Size = new System.Drawing.Size(851, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 554);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(851, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 554);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.btnAdjustHolidays);
            this.dataLayoutControl1.Controls.Add(this.ActiveHolidayYearCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalHolidaysRemainingTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalHolidaysTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalHolidayEntitlementTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BankHolidaysRemainingTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidaysRemainingTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceUnauthorisedTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceAuthorisedTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BankHolidaysTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidaysTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeForenameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeSurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.MustTakeCertainHolidaysCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.HoursPerHolidayDaySpinEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidayUnitDescriptorIdGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.PurchasedHolidayEntitlementSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.BankHolidayEntitlementSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.BasicHolidayEntitlementSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActualStartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidayYearEndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidayYearStartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.IdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.spHR00010GetEmployeeHolidayYearItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForId,
            this.ItemForEmployeeSurname,
            this.ItemForEmployeeForename,
            this.ItemForEmployeeNumber,
            this.ItemForEmployeeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1069, 193, 524, 756);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(851, 554);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // btnAdjustHolidays
            // 
            this.btnAdjustHolidays.Location = new System.Drawing.Point(48, 402);
            this.btnAdjustHolidays.Name = "btnAdjustHolidays";
            this.btnAdjustHolidays.Size = new System.Drawing.Size(225, 22);
            this.btnAdjustHolidays.StyleController = this.dataLayoutControl1;
            this.btnAdjustHolidays.TabIndex = 53;
            this.btnAdjustHolidays.Text = "Adjust Holidays By Actual Start Date";
            this.btnAdjustHolidays.ToolTip = "Click to Adjust Holidays Using Actual Start Date";
            this.btnAdjustHolidays.Click += new System.EventHandler(this.btnAdjustHolidays_Click);
            // 
            // ActiveHolidayYearCheckEdit
            // 
            this.ActiveHolidayYearCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "ActiveHolidayYear", true));
            this.ActiveHolidayYearCheckEdit.Location = new System.Drawing.Point(428, 237);
            this.ActiveHolidayYearCheckEdit.MenuManager = this.barManager1;
            this.ActiveHolidayYearCheckEdit.Name = "ActiveHolidayYearCheckEdit";
            this.ActiveHolidayYearCheckEdit.Properties.Caption = "(Ticked If Yes, Calculated - Read Only]";
            this.ActiveHolidayYearCheckEdit.Properties.ReadOnly = true;
            this.ActiveHolidayYearCheckEdit.Properties.ValueChecked = 1;
            this.ActiveHolidayYearCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveHolidayYearCheckEdit.Size = new System.Drawing.Size(374, 19);
            this.ActiveHolidayYearCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveHolidayYearCheckEdit.TabIndex = 52;
            // 
            // spHR00010GetEmployeeHolidayYearItemBindingSource
            // 
            this.spHR00010GetEmployeeHolidayYearItemBindingSource.DataMember = "sp_HR_00010_Get_Employee_Holiday_Year_Item";
            this.spHR00010GetEmployeeHolidayYearItemBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // TotalHolidaysRemainingTextEdit
            // 
            this.TotalHolidaysRemainingTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "TotalHolidaysRemaining", true));
            this.TotalHolidaysRemainingTextEdit.Location = new System.Drawing.Point(698, 378);
            this.TotalHolidaysRemainingTextEdit.MenuManager = this.barManager1;
            this.TotalHolidaysRemainingTextEdit.Name = "TotalHolidaysRemainingTextEdit";
            this.TotalHolidaysRemainingTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalHolidaysRemainingTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalHolidaysRemainingTextEdit.Properties.Mask.EditMask = "####0.00";
            this.TotalHolidaysRemainingTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalHolidaysRemainingTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalHolidaysRemainingTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalHolidaysRemainingTextEdit, true);
            this.TotalHolidaysRemainingTextEdit.Size = new System.Drawing.Size(92, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalHolidaysRemainingTextEdit, optionsSpelling1);
            this.TotalHolidaysRemainingTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalHolidaysRemainingTextEdit.TabIndex = 51;
            // 
            // TotalHolidaysTakenTextEdit
            // 
            this.TotalHolidaysTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "TotalHolidaysTaken", true));
            this.TotalHolidaysTakenTextEdit.Location = new System.Drawing.Point(440, 378);
            this.TotalHolidaysTakenTextEdit.MenuManager = this.barManager1;
            this.TotalHolidaysTakenTextEdit.Name = "TotalHolidaysTakenTextEdit";
            this.TotalHolidaysTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalHolidaysTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalHolidaysTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.TotalHolidaysTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalHolidaysTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalHolidaysTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalHolidaysTakenTextEdit, true);
            this.TotalHolidaysTakenTextEdit.Size = new System.Drawing.Size(91, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalHolidaysTakenTextEdit, optionsSpelling2);
            this.TotalHolidaysTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalHolidaysTakenTextEdit.TabIndex = 50;
            // 
            // TotalHolidayEntitlementTextEdit
            // 
            this.TotalHolidayEntitlementTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "TotalHolidayEntitlement", true));
            this.TotalHolidayEntitlementTextEdit.Location = new System.Drawing.Point(181, 378);
            this.TotalHolidayEntitlementTextEdit.MenuManager = this.barManager1;
            this.TotalHolidayEntitlementTextEdit.Name = "TotalHolidayEntitlementTextEdit";
            this.TotalHolidayEntitlementTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalHolidayEntitlementTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalHolidayEntitlementTextEdit.Properties.Mask.EditMask = "####0.00";
            this.TotalHolidayEntitlementTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalHolidayEntitlementTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalHolidayEntitlementTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalHolidayEntitlementTextEdit, true);
            this.TotalHolidayEntitlementTextEdit.Size = new System.Drawing.Size(92, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalHolidayEntitlementTextEdit, optionsSpelling3);
            this.TotalHolidayEntitlementTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalHolidayEntitlementTextEdit.TabIndex = 49;
            // 
            // BankHolidaysRemainingTextEdit
            // 
            this.BankHolidaysRemainingTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "BankHolidaysRemaining", true));
            this.BankHolidaysRemainingTextEdit.Location = new System.Drawing.Point(698, 352);
            this.BankHolidaysRemainingTextEdit.MenuManager = this.barManager1;
            this.BankHolidaysRemainingTextEdit.Name = "BankHolidaysRemainingTextEdit";
            this.BankHolidaysRemainingTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.BankHolidaysRemainingTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BankHolidaysRemainingTextEdit.Properties.Mask.EditMask = "####0.00";
            this.BankHolidaysRemainingTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.BankHolidaysRemainingTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BankHolidaysRemainingTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BankHolidaysRemainingTextEdit, true);
            this.BankHolidaysRemainingTextEdit.Size = new System.Drawing.Size(92, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BankHolidaysRemainingTextEdit, optionsSpelling4);
            this.BankHolidaysRemainingTextEdit.StyleController = this.dataLayoutControl1;
            this.BankHolidaysRemainingTextEdit.TabIndex = 47;
            // 
            // HolidaysRemainingTextEdit
            // 
            this.HolidaysRemainingTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "HolidaysRemaining", true));
            this.HolidaysRemainingTextEdit.Location = new System.Drawing.Point(698, 328);
            this.HolidaysRemainingTextEdit.MenuManager = this.barManager1;
            this.HolidaysRemainingTextEdit.Name = "HolidaysRemainingTextEdit";
            this.HolidaysRemainingTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.HolidaysRemainingTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.HolidaysRemainingTextEdit.Properties.Mask.EditMask = "####0.00";
            this.HolidaysRemainingTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.HolidaysRemainingTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HolidaysRemainingTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HolidaysRemainingTextEdit, true);
            this.HolidaysRemainingTextEdit.Size = new System.Drawing.Size(92, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HolidaysRemainingTextEdit, optionsSpelling5);
            this.HolidaysRemainingTextEdit.StyleController = this.dataLayoutControl1;
            this.HolidaysRemainingTextEdit.TabIndex = 46;
            // 
            // AbsenceUnauthorisedTakenTextEdit
            // 
            this.AbsenceUnauthorisedTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "AbsenceUnauthorisedTaken", true));
            this.AbsenceUnauthorisedTakenTextEdit.Location = new System.Drawing.Point(440, 436);
            this.AbsenceUnauthorisedTakenTextEdit.MenuManager = this.barManager1;
            this.AbsenceUnauthorisedTakenTextEdit.Name = "AbsenceUnauthorisedTakenTextEdit";
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AbsenceUnauthorisedTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceUnauthorisedTakenTextEdit, true);
            this.AbsenceUnauthorisedTakenTextEdit.Size = new System.Drawing.Size(91, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceUnauthorisedTakenTextEdit, optionsSpelling6);
            this.AbsenceUnauthorisedTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceUnauthorisedTakenTextEdit.TabIndex = 45;
            // 
            // AbsenceAuthorisedTakenTextEdit
            // 
            this.AbsenceAuthorisedTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "AbsenceAuthorisedTaken", true));
            this.AbsenceAuthorisedTakenTextEdit.Location = new System.Drawing.Point(440, 412);
            this.AbsenceAuthorisedTakenTextEdit.MenuManager = this.barManager1;
            this.AbsenceAuthorisedTakenTextEdit.Name = "AbsenceAuthorisedTakenTextEdit";
            this.AbsenceAuthorisedTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AbsenceAuthorisedTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AbsenceAuthorisedTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.AbsenceAuthorisedTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AbsenceAuthorisedTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AbsenceAuthorisedTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceAuthorisedTakenTextEdit, true);
            this.AbsenceAuthorisedTakenTextEdit.Size = new System.Drawing.Size(91, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceAuthorisedTakenTextEdit, optionsSpelling7);
            this.AbsenceAuthorisedTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceAuthorisedTakenTextEdit.TabIndex = 44;
            // 
            // BankHolidaysTakenTextEdit
            // 
            this.BankHolidaysTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "BankHolidaysTaken", true));
            this.BankHolidaysTakenTextEdit.Location = new System.Drawing.Point(440, 352);
            this.BankHolidaysTakenTextEdit.MenuManager = this.barManager1;
            this.BankHolidaysTakenTextEdit.Name = "BankHolidaysTakenTextEdit";
            this.BankHolidaysTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.BankHolidaysTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BankHolidaysTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.BankHolidaysTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.BankHolidaysTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BankHolidaysTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BankHolidaysTakenTextEdit, true);
            this.BankHolidaysTakenTextEdit.Size = new System.Drawing.Size(91, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BankHolidaysTakenTextEdit, optionsSpelling8);
            this.BankHolidaysTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.BankHolidaysTakenTextEdit.TabIndex = 42;
            // 
            // HolidaysTakenTextEdit
            // 
            this.HolidaysTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "HolidaysTaken", true));
            this.HolidaysTakenTextEdit.Location = new System.Drawing.Point(440, 328);
            this.HolidaysTakenTextEdit.MenuManager = this.barManager1;
            this.HolidaysTakenTextEdit.Name = "HolidaysTakenTextEdit";
            this.HolidaysTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.HolidaysTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.HolidaysTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.HolidaysTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.HolidaysTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HolidaysTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HolidaysTakenTextEdit, true);
            this.HolidaysTakenTextEdit.Size = new System.Drawing.Size(91, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HolidaysTakenTextEdit, optionsSpelling9);
            this.HolidaysTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.HolidaysTakenTextEdit.TabIndex = 41;
            // 
            // EmployeeNumberTextEdit
            // 
            this.EmployeeNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "EmployeeNumber", true));
            this.EmployeeNumberTextEdit.Location = new System.Drawing.Point(166, 143);
            this.EmployeeNumberTextEdit.MenuManager = this.barManager1;
            this.EmployeeNumberTextEdit.Name = "EmployeeNumberTextEdit";
            this.EmployeeNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNumberTextEdit, true);
            this.EmployeeNumberTextEdit.Size = new System.Drawing.Size(563, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNumberTextEdit, optionsSpelling10);
            this.EmployeeNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNumberTextEdit.TabIndex = 40;
            // 
            // EmployeeForenameTextEdit
            // 
            this.EmployeeForenameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "EmployeeForename", true));
            this.EmployeeForenameTextEdit.Location = new System.Drawing.Point(166, 143);
            this.EmployeeForenameTextEdit.MenuManager = this.barManager1;
            this.EmployeeForenameTextEdit.Name = "EmployeeForenameTextEdit";
            this.EmployeeForenameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeForenameTextEdit, true);
            this.EmployeeForenameTextEdit.Size = new System.Drawing.Size(563, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeForenameTextEdit, optionsSpelling11);
            this.EmployeeForenameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeForenameTextEdit.TabIndex = 39;
            // 
            // EmployeeSurnameTextEdit
            // 
            this.EmployeeSurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "EmployeeSurname", true));
            this.EmployeeSurnameTextEdit.Location = new System.Drawing.Point(166, 143);
            this.EmployeeSurnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeSurnameTextEdit.Name = "EmployeeSurnameTextEdit";
            this.EmployeeSurnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeSurnameTextEdit, true);
            this.EmployeeSurnameTextEdit.Size = new System.Drawing.Size(563, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeSurnameTextEdit, optionsSpelling12);
            this.EmployeeSurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeSurnameTextEdit.TabIndex = 38;
            // 
            // MustTakeCertainHolidaysCheckEdit
            // 
            this.MustTakeCertainHolidaysCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "MustTakeCertainHolidays", true));
            this.MustTakeCertainHolidaysCheckEdit.Location = new System.Drawing.Point(169, 237);
            this.MustTakeCertainHolidaysCheckEdit.MenuManager = this.barManager1;
            this.MustTakeCertainHolidaysCheckEdit.Name = "MustTakeCertainHolidaysCheckEdit";
            this.MustTakeCertainHolidaysCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.MustTakeCertainHolidaysCheckEdit.Properties.ValueChecked = 1;
            this.MustTakeCertainHolidaysCheckEdit.Properties.ValueUnchecked = 0;
            this.MustTakeCertainHolidaysCheckEdit.Size = new System.Drawing.Size(122, 19);
            this.MustTakeCertainHolidaysCheckEdit.StyleController = this.dataLayoutControl1;
            this.MustTakeCertainHolidaysCheckEdit.TabIndex = 37;
            // 
            // HoursPerHolidayDaySpinEdit
            // 
            this.HoursPerHolidayDaySpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "HoursPerHolidayDay", true));
            this.HoursPerHolidayDaySpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.HoursPerHolidayDaySpinEdit.Location = new System.Drawing.Point(428, 213);
            this.HoursPerHolidayDaySpinEdit.MenuManager = this.barManager1;
            this.HoursPerHolidayDaySpinEdit.Name = "HoursPerHolidayDaySpinEdit";
            this.HoursPerHolidayDaySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HoursPerHolidayDaySpinEdit.Properties.Mask.EditMask = "f2";
            this.HoursPerHolidayDaySpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HoursPerHolidayDaySpinEdit.Properties.MaxValue = new decimal(new int[] {
            2400,
            0,
            0,
            131072});
            this.HoursPerHolidayDaySpinEdit.Size = new System.Drawing.Size(74, 20);
            this.HoursPerHolidayDaySpinEdit.StyleController = this.dataLayoutControl1;
            this.HoursPerHolidayDaySpinEdit.TabIndex = 36;
            // 
            // HolidayUnitDescriptorIdGridLookUpEdit
            // 
            this.HolidayUnitDescriptorIdGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "HolidayUnitDescriptorId", true));
            this.HolidayUnitDescriptorIdGridLookUpEdit.Location = new System.Drawing.Point(169, 213);
            this.HolidayUnitDescriptorIdGridLookUpEdit.MenuManager = this.barManager1;
            this.HolidayUnitDescriptorIdGridLookUpEdit.Name = "HolidayUnitDescriptorIdGridLookUpEdit";
            this.HolidayUnitDescriptorIdGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HolidayUnitDescriptorIdGridLookUpEdit.Properties.DataSource = this.spHR00082HolidayUnitDescriptorsWithBlankBindingSource;
            this.HolidayUnitDescriptorIdGridLookUpEdit.Properties.DisplayMember = "Description";
            this.HolidayUnitDescriptorIdGridLookUpEdit.Properties.NullText = "";
            this.HolidayUnitDescriptorIdGridLookUpEdit.Properties.ValueMember = "ID";
            this.HolidayUnitDescriptorIdGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.HolidayUnitDescriptorIdGridLookUpEdit.Size = new System.Drawing.Size(122, 20);
            this.HolidayUnitDescriptorIdGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.HolidayUnitDescriptorIdGridLookUpEdit.TabIndex = 35;
            this.HolidayUnitDescriptorIdGridLookUpEdit.EditValueChanged += new System.EventHandler(this.HolidayUnitDescriptorIdGridLookUpEdit_EditValueChanged);
            this.HolidayUnitDescriptorIdGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.HolidayUnitDescriptorIdGridLookUpEdit_Validating);
            // 
            // spHR00082HolidayUnitDescriptorsWithBlankBindingSource
            // 
            this.spHR00082HolidayUnitDescriptorsWithBlankBindingSource.DataMember = "sp_HR_00082_Holiday_Unit_Descriptors_With_Blank";
            this.spHR00082HolidayUnitDescriptorsWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Holiday Unit Descriptor";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // PurchasedHolidayEntitlementSpinEdit
            // 
            this.PurchasedHolidayEntitlementSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "PurchasedHolidayEntitlement", true));
            this.PurchasedHolidayEntitlementSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.PurchasedHolidayEntitlementSpinEdit.Location = new System.Drawing.Point(181, 304);
            this.PurchasedHolidayEntitlementSpinEdit.MenuManager = this.barManager1;
            this.PurchasedHolidayEntitlementSpinEdit.Name = "PurchasedHolidayEntitlementSpinEdit";
            this.PurchasedHolidayEntitlementSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PurchasedHolidayEntitlementSpinEdit.Properties.Mask.EditMask = "####0.00";
            this.PurchasedHolidayEntitlementSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PurchasedHolidayEntitlementSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            131072});
            this.PurchasedHolidayEntitlementSpinEdit.Properties.MinValue = new decimal(new int[] {
            99999,
            0,
            0,
            -2147352576});
            this.PurchasedHolidayEntitlementSpinEdit.Size = new System.Drawing.Size(92, 20);
            this.PurchasedHolidayEntitlementSpinEdit.StyleController = this.dataLayoutControl1;
            this.PurchasedHolidayEntitlementSpinEdit.TabIndex = 34;
            this.PurchasedHolidayEntitlementSpinEdit.EditValueChanged += new System.EventHandler(this.PurchasedHolidayEntitlementSpinEdit_EditValueChanged);
            // 
            // BankHolidayEntitlementSpinEdit
            // 
            this.BankHolidayEntitlementSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "BankHolidayEntitlement", true));
            this.BankHolidayEntitlementSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.BankHolidayEntitlementSpinEdit.Location = new System.Drawing.Point(181, 352);
            this.BankHolidayEntitlementSpinEdit.MenuManager = this.barManager1;
            this.BankHolidayEntitlementSpinEdit.Name = "BankHolidayEntitlementSpinEdit";
            this.BankHolidayEntitlementSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BankHolidayEntitlementSpinEdit.Properties.Mask.EditMask = "####0.00";
            this.BankHolidayEntitlementSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BankHolidayEntitlementSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            131072});
            this.BankHolidayEntitlementSpinEdit.Properties.MinValue = new decimal(new int[] {
            99999,
            0,
            0,
            -2147352576});
            this.BankHolidayEntitlementSpinEdit.Size = new System.Drawing.Size(92, 20);
            this.BankHolidayEntitlementSpinEdit.StyleController = this.dataLayoutControl1;
            this.BankHolidayEntitlementSpinEdit.TabIndex = 33;
            this.BankHolidayEntitlementSpinEdit.EditValueChanged += new System.EventHandler(this.BankHolidayEntitlementSpinEdit_EditValueChanged);
            // 
            // BasicHolidayEntitlementSpinEdit
            // 
            this.BasicHolidayEntitlementSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "BasicHolidayEntitlement", true));
            this.BasicHolidayEntitlementSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.BasicHolidayEntitlementSpinEdit.Location = new System.Drawing.Point(181, 328);
            this.BasicHolidayEntitlementSpinEdit.MenuManager = this.barManager1;
            this.BasicHolidayEntitlementSpinEdit.Name = "BasicHolidayEntitlementSpinEdit";
            this.BasicHolidayEntitlementSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BasicHolidayEntitlementSpinEdit.Properties.Mask.EditMask = "####0.00";
            this.BasicHolidayEntitlementSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BasicHolidayEntitlementSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            131072});
            this.BasicHolidayEntitlementSpinEdit.Properties.MinValue = new decimal(new int[] {
            99999,
            0,
            0,
            -2147352576});
            this.BasicHolidayEntitlementSpinEdit.Size = new System.Drawing.Size(92, 20);
            this.BasicHolidayEntitlementSpinEdit.StyleController = this.dataLayoutControl1;
            this.BasicHolidayEntitlementSpinEdit.TabIndex = 32;
            this.BasicHolidayEntitlementSpinEdit.EditValueChanged += new System.EventHandler(this.BasicHolidayEntitlementSpinEdit_EditValueChanged);
            // 
            // ActualStartDateDateEdit
            // 
            this.ActualStartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "ActualStartDate", true));
            this.ActualStartDateDateEdit.EditValue = null;
            this.ActualStartDateDateEdit.Location = new System.Drawing.Point(145, 107);
            this.ActualStartDateDateEdit.MenuManager = this.barManager1;
            this.ActualStartDateDateEdit.Name = "ActualStartDateDateEdit";
            this.ActualStartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualStartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActualStartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActualStartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ActualStartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ActualStartDateDateEdit.Size = new System.Drawing.Size(146, 20);
            this.ActualStartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ActualStartDateDateEdit.TabIndex = 31;
            this.ActualStartDateDateEdit.EditValueChanged += new System.EventHandler(this.ActualStartDateDateEdit_EditValueChanged);
            this.ActualStartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ActualStartDateDateEdit_Validating);
            // 
            // HolidayYearEndDateDateEdit
            // 
            this.HolidayYearEndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "HolidayYearEndDate", true));
            this.HolidayYearEndDateDateEdit.EditValue = null;
            this.HolidayYearEndDateDateEdit.Location = new System.Drawing.Point(145, 83);
            this.HolidayYearEndDateDateEdit.MenuManager = this.barManager1;
            this.HolidayYearEndDateDateEdit.Name = "HolidayYearEndDateDateEdit";
            this.HolidayYearEndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HolidayYearEndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HolidayYearEndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HolidayYearEndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.HolidayYearEndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.HolidayYearEndDateDateEdit.Size = new System.Drawing.Size(146, 20);
            this.HolidayYearEndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.HolidayYearEndDateDateEdit.TabIndex = 30;
            this.HolidayYearEndDateDateEdit.EditValueChanged += new System.EventHandler(this.HolidayYearEndDateDateEdit_EditValueChanged);
            this.HolidayYearEndDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.HolidayYearEndDateDateEdit_Validating);
            // 
            // HolidayYearStartDateDateEdit
            // 
            this.HolidayYearStartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "HolidayYearStartDate", true));
            this.HolidayYearStartDateDateEdit.EditValue = null;
            this.HolidayYearStartDateDateEdit.Location = new System.Drawing.Point(145, 59);
            this.HolidayYearStartDateDateEdit.MenuManager = this.barManager1;
            this.HolidayYearStartDateDateEdit.Name = "HolidayYearStartDateDateEdit";
            this.HolidayYearStartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HolidayYearStartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HolidayYearStartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HolidayYearStartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.HolidayYearStartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.HolidayYearStartDateDateEdit.Size = new System.Drawing.Size(146, 20);
            this.HolidayYearStartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.HolidayYearStartDateDateEdit.TabIndex = 29;
            this.HolidayYearStartDateDateEdit.EditValueChanged += new System.EventHandler(this.HolidayYearStartDateDateEdit_EditValueChanged);
            this.HolidayYearStartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.HolidayYearStartDateDateEdit_Validating);
            // 
            // IdTextEdit
            // 
            this.IdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "Id", true));
            this.IdTextEdit.Location = new System.Drawing.Point(142, 59);
            this.IdTextEdit.MenuManager = this.barManager1;
            this.IdTextEdit.Name = "IdTextEdit";
            this.IdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.IdTextEdit, true);
            this.IdTextEdit.Size = new System.Drawing.Size(587, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.IdTextEdit, optionsSpelling13);
            this.IdTextEdit.StyleController = this.dataLayoutControl1;
            this.IdTextEdit.TabIndex = 19;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00010GetEmployeeHolidayYearItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(144, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(186, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 17;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // EmployeeIDTextEdit
            // 
            this.EmployeeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "EmployeeID", true));
            this.EmployeeIDTextEdit.Location = new System.Drawing.Point(166, 131);
            this.EmployeeIDTextEdit.MenuManager = this.barManager1;
            this.EmployeeIDTextEdit.Name = "EmployeeIDTextEdit";
            this.EmployeeIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.EmployeeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIDTextEdit, true);
            this.EmployeeIDTextEdit.Size = new System.Drawing.Size(663, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIDTextEdit, optionsSpelling14);
            this.EmployeeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIDTextEdit.TabIndex = 4;
            this.EmployeeIDTextEdit.TabStop = false;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 213);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(766, 275);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling15);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 27;
            // 
            // EmployeeNameButtonEdit
            // 
            this.EmployeeNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00010GetEmployeeHolidayYearItemBindingSource, "EmployeeName", true));
            this.EmployeeNameButtonEdit.EditValue = "";
            this.EmployeeNameButtonEdit.Location = new System.Drawing.Point(145, 35);
            this.EmployeeNameButtonEdit.MenuManager = this.barManager1;
            this.EmployeeNameButtonEdit.Name = "EmployeeNameButtonEdit";
            this.EmployeeNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Select Employee\'s screen", "choose", null, true)});
            this.EmployeeNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EmployeeNameButtonEdit.Size = new System.Drawing.Size(410, 20);
            this.EmployeeNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameButtonEdit.TabIndex = 28;
            this.EmployeeNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EmployeeNameButtonEdit_ButtonClick);
            this.EmployeeNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EmployeeNameButtonEdit_Validating);
            // 
            // ItemForId
            // 
            this.ItemForId.Control = this.IdTextEdit;
            this.ItemForId.CustomizationFormText = "Employee Holiday Year ID:";
            this.ItemForId.Location = new System.Drawing.Point(0, 47);
            this.ItemForId.Name = "ItemForId";
            this.ItemForId.Size = new System.Drawing.Size(721, 24);
            this.ItemForId.Text = "Employee Holiday Year ID:";
            this.ItemForId.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeSurname
            // 
            this.ItemForEmployeeSurname.Control = this.EmployeeSurnameTextEdit;
            this.ItemForEmployeeSurname.CustomizationFormText = "Employee Surname:";
            this.ItemForEmployeeSurname.Location = new System.Drawing.Point(0, 131);
            this.ItemForEmployeeSurname.Name = "ItemForEmployeeSurname";
            this.ItemForEmployeeSurname.Size = new System.Drawing.Size(721, 24);
            this.ItemForEmployeeSurname.Text = "Employee Surname:";
            this.ItemForEmployeeSurname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeForename
            // 
            this.ItemForEmployeeForename.Control = this.EmployeeForenameTextEdit;
            this.ItemForEmployeeForename.CustomizationFormText = "Employee Forename:";
            this.ItemForEmployeeForename.Location = new System.Drawing.Point(0, 131);
            this.ItemForEmployeeForename.Name = "ItemForEmployeeForename";
            this.ItemForEmployeeForename.Size = new System.Drawing.Size(721, 24);
            this.ItemForEmployeeForename.Text = "Employee Forename:";
            this.ItemForEmployeeForename.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeNumber
            // 
            this.ItemForEmployeeNumber.Control = this.EmployeeNumberTextEdit;
            this.ItemForEmployeeNumber.CustomizationFormText = "Employee Number:";
            this.ItemForEmployeeNumber.Location = new System.Drawing.Point(0, 131);
            this.ItemForEmployeeNumber.Name = "ItemForEmployeeNumber";
            this.ItemForEmployeeNumber.Size = new System.Drawing.Size(721, 24);
            this.ItemForEmployeeNumber.Text = "Employee Number:";
            this.ItemForEmployeeNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeID
            // 
            this.ItemForEmployeeID.Control = this.EmployeeIDTextEdit;
            this.ItemForEmployeeID.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeID.Location = new System.Drawing.Point(0, 119);
            this.ItemForEmployeeID.Name = "ItemForEmployeeID";
            this.ItemForEmployeeID.Size = new System.Drawing.Size(821, 24);
            this.ItemForEmployeeID.Text = "Employee ID:";
            this.ItemForEmployeeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(851, 554);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.ItemForEmployeeName,
            this.ItemForHolidayYearStartDate,
            this.emptySpaceItem13,
            this.ItemForHolidayYearEndDate,
            this.ItemForActualStartDate,
            this.emptySpaceItem14,
            this.emptySpaceItem15,
            this.emptySpaceItem16});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(831, 534);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 131);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(818, 373);
            this.layoutControlGroup5.Text = "Details";
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(794, 327);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details:";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.emptySpaceItem4,
            this.ItemForHolidayUnitDescriptorId,
            this.ItemForHoursPerHolidayDay,
            this.ItemForMustTakeCertainHolidays,
            this.layoutControlGroup7,
            this.layoutControlGroup8,
            this.ItemForActiveHolidayYear,
            this.splitterItem1,
            this.splitterItem2,
            this.emptySpaceItem17,
            this.emptySpaceItem2});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(770, 279);
            this.layoutControlGroup4.Text = "Details";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Holiday Entitlement";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForBasicHolidayEntitlement,
            this.ItemForBankHolidayEntitlement,
            this.ItemForTotalHolidayEntitlement,
            this.simpleSeparator1,
            this.emptySpaceItem7,
            this.ItemForAdjustHolidaysButton,
            this.ItemForPurchasedHolidayEntitlement});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 57);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(253, 212);
            this.layoutControlGroup6.Text = "Entitlement";
            // 
            // ItemForBasicHolidayEntitlement
            // 
            this.ItemForBasicHolidayEntitlement.Control = this.BasicHolidayEntitlementSpinEdit;
            this.ItemForBasicHolidayEntitlement.CustomizationFormText = "Holidays Entitlement:";
            this.ItemForBasicHolidayEntitlement.Location = new System.Drawing.Point(0, 24);
            this.ItemForBasicHolidayEntitlement.Name = "ItemForBasicHolidayEntitlement";
            this.ItemForBasicHolidayEntitlement.Size = new System.Drawing.Size(229, 24);
            this.ItemForBasicHolidayEntitlement.Text = "Holidays:";
            this.ItemForBasicHolidayEntitlement.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForBankHolidayEntitlement
            // 
            this.ItemForBankHolidayEntitlement.Control = this.BankHolidayEntitlementSpinEdit;
            this.ItemForBankHolidayEntitlement.CustomizationFormText = "Bank Holidays Entitlement:";
            this.ItemForBankHolidayEntitlement.Location = new System.Drawing.Point(0, 48);
            this.ItemForBankHolidayEntitlement.Name = "ItemForBankHolidayEntitlement";
            this.ItemForBankHolidayEntitlement.Size = new System.Drawing.Size(229, 24);
            this.ItemForBankHolidayEntitlement.Text = "Bank Holidays:";
            this.ItemForBankHolidayEntitlement.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForTotalHolidayEntitlement
            // 
            this.ItemForTotalHolidayEntitlement.Control = this.TotalHolidayEntitlementTextEdit;
            this.ItemForTotalHolidayEntitlement.CustomizationFormText = "Total Holidays Entitlement:";
            this.ItemForTotalHolidayEntitlement.Location = new System.Drawing.Point(0, 74);
            this.ItemForTotalHolidayEntitlement.Name = "ItemForTotalHolidayEntitlement";
            this.ItemForTotalHolidayEntitlement.Size = new System.Drawing.Size(229, 24);
            this.ItemForTotalHolidayEntitlement.Text = "Total Holidays:";
            this.ItemForTotalHolidayEntitlement.TextSize = new System.Drawing.Size(130, 13);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(229, 2);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 124);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(229, 42);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAdjustHolidaysButton
            // 
            this.ItemForAdjustHolidaysButton.Control = this.btnAdjustHolidays;
            this.ItemForAdjustHolidaysButton.CustomizationFormText = "Adjust Holidays Button";
            this.ItemForAdjustHolidaysButton.Location = new System.Drawing.Point(0, 98);
            this.ItemForAdjustHolidaysButton.Name = "ItemForAdjustHolidaysButton";
            this.ItemForAdjustHolidaysButton.Size = new System.Drawing.Size(229, 26);
            this.ItemForAdjustHolidaysButton.Text = "Adjust Holidays Button";
            this.ItemForAdjustHolidaysButton.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForAdjustHolidaysButton.TextVisible = false;
            // 
            // ItemForPurchasedHolidayEntitlement
            // 
            this.ItemForPurchasedHolidayEntitlement.Control = this.PurchasedHolidayEntitlementSpinEdit;
            this.ItemForPurchasedHolidayEntitlement.CustomizationFormText = "Purchased Holidays Entitlement:";
            this.ItemForPurchasedHolidayEntitlement.Location = new System.Drawing.Point(0, 0);
            this.ItemForPurchasedHolidayEntitlement.Name = "ItemForPurchasedHolidayEntitlement";
            this.ItemForPurchasedHolidayEntitlement.Size = new System.Drawing.Size(229, 24);
            this.ItemForPurchasedHolidayEntitlement.Text = "Purchased Holidays:";
            this.ItemForPurchasedHolidayEntitlement.TextSize = new System.Drawing.Size(130, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(770, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForHolidayUnitDescriptorId
            // 
            this.ItemForHolidayUnitDescriptorId.AllowHide = false;
            this.ItemForHolidayUnitDescriptorId.Control = this.HolidayUnitDescriptorIdGridLookUpEdit;
            this.ItemForHolidayUnitDescriptorId.CustomizationFormText = "Holiday Unit Descriptor:";
            this.ItemForHolidayUnitDescriptorId.Location = new System.Drawing.Point(0, 0);
            this.ItemForHolidayUnitDescriptorId.MaxSize = new System.Drawing.Size(259, 24);
            this.ItemForHolidayUnitDescriptorId.MinSize = new System.Drawing.Size(259, 24);
            this.ItemForHolidayUnitDescriptorId.Name = "ItemForHolidayUnitDescriptorId";
            this.ItemForHolidayUnitDescriptorId.Size = new System.Drawing.Size(259, 24);
            this.ItemForHolidayUnitDescriptorId.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHolidayUnitDescriptorId.Text = "Holiday Unit Descriptor:";
            this.ItemForHolidayUnitDescriptorId.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForHoursPerHolidayDay
            // 
            this.ItemForHoursPerHolidayDay.AllowHide = false;
            this.ItemForHoursPerHolidayDay.Control = this.HoursPerHolidayDaySpinEdit;
            this.ItemForHoursPerHolidayDay.CustomizationFormText = "Hours Per Holiday Day:";
            this.ItemForHoursPerHolidayDay.Location = new System.Drawing.Point(259, 0);
            this.ItemForHoursPerHolidayDay.MaxSize = new System.Drawing.Size(211, 24);
            this.ItemForHoursPerHolidayDay.MinSize = new System.Drawing.Size(211, 24);
            this.ItemForHoursPerHolidayDay.Name = "ItemForHoursPerHolidayDay";
            this.ItemForHoursPerHolidayDay.Size = new System.Drawing.Size(211, 24);
            this.ItemForHoursPerHolidayDay.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHoursPerHolidayDay.Text = "Hours Per Holiday Day:";
            this.ItemForHoursPerHolidayDay.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForMustTakeCertainHolidays
            // 
            this.ItemForMustTakeCertainHolidays.Control = this.MustTakeCertainHolidaysCheckEdit;
            this.ItemForMustTakeCertainHolidays.CustomizationFormText = "Must Take Mandatory Holidays:";
            this.ItemForMustTakeCertainHolidays.Location = new System.Drawing.Point(0, 24);
            this.ItemForMustTakeCertainHolidays.Name = "ItemForMustTakeCertainHolidays";
            this.ItemForMustTakeCertainHolidays.Size = new System.Drawing.Size(259, 23);
            this.ItemForMustTakeCertainHolidays.Text = "Takes Mandatory Holidays:";
            this.ItemForMustTakeCertainHolidays.TextSize = new System.Drawing.Size(130, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.AllowHtmlStringInCaption = true;
            this.layoutControlGroup7.CustomizationFormText = "Taken [Read Only]";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHolidaysTaken,
            this.ItemForBankHolidaysTaken,
            this.ItemForAbsenceAuthorisedTaken,
            this.ItemForAbsenceUnauthorisedTaken,
            this.simpleSeparator2,
            this.ItemForTotalHolidaysTaken,
            this.emptySpaceItem8,
            this.emptySpaceItem10,
            this.emptySpaceItem11});
            this.layoutControlGroup7.Location = new System.Drawing.Point(259, 57);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(252, 212);
            this.layoutControlGroup7.Text = "Taken  [<b>Calculated</b> - Read Only]";
            // 
            // ItemForHolidaysTaken
            // 
            this.ItemForHolidaysTaken.Control = this.HolidaysTakenTextEdit;
            this.ItemForHolidaysTaken.CustomizationFormText = "Holidays:";
            this.ItemForHolidaysTaken.Location = new System.Drawing.Point(0, 24);
            this.ItemForHolidaysTaken.Name = "ItemForHolidaysTaken";
            this.ItemForHolidaysTaken.Size = new System.Drawing.Size(228, 24);
            this.ItemForHolidaysTaken.Text = "Holidays:";
            this.ItemForHolidaysTaken.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForBankHolidaysTaken
            // 
            this.ItemForBankHolidaysTaken.Control = this.BankHolidaysTakenTextEdit;
            this.ItemForBankHolidaysTaken.CustomizationFormText = "Bank Holidays Taken:";
            this.ItemForBankHolidaysTaken.Location = new System.Drawing.Point(0, 48);
            this.ItemForBankHolidaysTaken.Name = "ItemForBankHolidaysTaken";
            this.ItemForBankHolidaysTaken.Size = new System.Drawing.Size(228, 24);
            this.ItemForBankHolidaysTaken.Text = "Bank Holidays:";
            this.ItemForBankHolidaysTaken.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForAbsenceAuthorisedTaken
            // 
            this.ItemForAbsenceAuthorisedTaken.Control = this.AbsenceAuthorisedTakenTextEdit;
            this.ItemForAbsenceAuthorisedTaken.CustomizationFormText = "Authorised Absences Taken:";
            this.ItemForAbsenceAuthorisedTaken.Location = new System.Drawing.Point(0, 108);
            this.ItemForAbsenceAuthorisedTaken.Name = "ItemForAbsenceAuthorisedTaken";
            this.ItemForAbsenceAuthorisedTaken.Size = new System.Drawing.Size(228, 24);
            this.ItemForAbsenceAuthorisedTaken.Text = "Authorised Absences:";
            this.ItemForAbsenceAuthorisedTaken.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForAbsenceUnauthorisedTaken
            // 
            this.ItemForAbsenceUnauthorisedTaken.Control = this.AbsenceUnauthorisedTakenTextEdit;
            this.ItemForAbsenceUnauthorisedTaken.CustomizationFormText = "Unauthorised Absences Taken:";
            this.ItemForAbsenceUnauthorisedTaken.Location = new System.Drawing.Point(0, 132);
            this.ItemForAbsenceUnauthorisedTaken.Name = "ItemForAbsenceUnauthorisedTaken";
            this.ItemForAbsenceUnauthorisedTaken.Size = new System.Drawing.Size(228, 24);
            this.ItemForAbsenceUnauthorisedTaken.Text = "Unauthorised Absences:";
            this.ItemForAbsenceUnauthorisedTaken.TextSize = new System.Drawing.Size(130, 13);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.CustomizationFormText = "simpleSeparator2";
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(228, 2);
            // 
            // ItemForTotalHolidaysTaken
            // 
            this.ItemForTotalHolidaysTaken.Control = this.TotalHolidaysTakenTextEdit;
            this.ItemForTotalHolidaysTaken.CustomizationFormText = "Total Holidays Taken:";
            this.ItemForTotalHolidaysTaken.Location = new System.Drawing.Point(0, 74);
            this.ItemForTotalHolidaysTaken.Name = "ItemForTotalHolidaysTaken";
            this.ItemForTotalHolidaysTaken.Size = new System.Drawing.Size(228, 24);
            this.ItemForTotalHolidaysTaken.Text = "Total Holidays:";
            this.ItemForTotalHolidaysTaken.TextSize = new System.Drawing.Size(130, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 156);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(228, 10);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem10.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem10.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(228, 10);
            this.emptySpaceItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem11.MaxSize = new System.Drawing.Size(0, 24);
            this.emptySpaceItem11.MinSize = new System.Drawing.Size(10, 24);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(228, 24);
            this.emptySpaceItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.AllowHtmlStringInCaption = true;
            this.layoutControlGroup8.CustomizationFormText = "Remaining [Read Only]";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHolidaysRemaining,
            this.ItemForBankHolidaysRemaining,
            this.simpleSeparator3,
            this.ItemForTotalHolidaysRemaining,
            this.emptySpaceItem9,
            this.emptySpaceItem12});
            this.layoutControlGroup8.Location = new System.Drawing.Point(517, 57);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(253, 212);
            this.layoutControlGroup8.Text = "Remaining  [<b>Calculated</b> - Read Only]";
            // 
            // ItemForHolidaysRemaining
            // 
            this.ItemForHolidaysRemaining.Control = this.HolidaysRemainingTextEdit;
            this.ItemForHolidaysRemaining.CustomizationFormText = "Holidays Remaining:";
            this.ItemForHolidaysRemaining.Location = new System.Drawing.Point(0, 24);
            this.ItemForHolidaysRemaining.Name = "ItemForHolidaysRemaining";
            this.ItemForHolidaysRemaining.Size = new System.Drawing.Size(229, 24);
            this.ItemForHolidaysRemaining.Text = "Holidays:";
            this.ItemForHolidaysRemaining.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForBankHolidaysRemaining
            // 
            this.ItemForBankHolidaysRemaining.Control = this.BankHolidaysRemainingTextEdit;
            this.ItemForBankHolidaysRemaining.CustomizationFormText = "Bank Holidays Remaining:";
            this.ItemForBankHolidaysRemaining.Location = new System.Drawing.Point(0, 48);
            this.ItemForBankHolidaysRemaining.Name = "ItemForBankHolidaysRemaining";
            this.ItemForBankHolidaysRemaining.Size = new System.Drawing.Size(229, 24);
            this.ItemForBankHolidaysRemaining.Text = "Bank Holidays:";
            this.ItemForBankHolidaysRemaining.TextSize = new System.Drawing.Size(130, 13);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator3";
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(229, 2);
            // 
            // ItemForTotalHolidaysRemaining
            // 
            this.ItemForTotalHolidaysRemaining.Control = this.TotalHolidaysRemainingTextEdit;
            this.ItemForTotalHolidaysRemaining.CustomizationFormText = "Total Holidays Remaining:";
            this.ItemForTotalHolidaysRemaining.Location = new System.Drawing.Point(0, 74);
            this.ItemForTotalHolidaysRemaining.Name = "ItemForTotalHolidaysRemaining";
            this.ItemForTotalHolidaysRemaining.Size = new System.Drawing.Size(229, 24);
            this.ItemForTotalHolidaysRemaining.Text = "Total Holidays:";
            this.ItemForTotalHolidaysRemaining.TextSize = new System.Drawing.Size(130, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(229, 68);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 24);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(229, 24);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForActiveHolidayYear
            // 
            this.ItemForActiveHolidayYear.Control = this.ActiveHolidayYearCheckEdit;
            this.ItemForActiveHolidayYear.CustomizationFormText = "Active Holiday Year:";
            this.ItemForActiveHolidayYear.Location = new System.Drawing.Point(259, 24);
            this.ItemForActiveHolidayYear.Name = "ItemForActiveHolidayYear";
            this.ItemForActiveHolidayYear.Size = new System.Drawing.Size(511, 23);
            this.ItemForActiveHolidayYear.Text = "Active Holiday Year:";
            this.ItemForActiveHolidayYear.TextSize = new System.Drawing.Size(130, 13);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(253, 57);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 212);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(511, 57);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(6, 212);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(470, 0);
            this.emptySpaceItem17.MaxSize = new System.Drawing.Size(300, 0);
            this.emptySpaceItem17.MinSize = new System.Drawing.Size(300, 10);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(300, 24);
            this.emptySpaceItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks:";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(770, 279);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.MaxSize = new System.Drawing.Size(0, 279);
            this.ItemForRemarks.MinSize = new System.Drawing.Size(14, 279);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(770, 279);
            this.ItemForRemarks.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 119);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(831, 12);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(132, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(190, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(132, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(132, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(132, 23);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(322, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(509, 23);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.AllowHide = false;
            this.ItemForEmployeeName.Control = this.EmployeeNameButtonEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Employee Contract:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 23);
            this.ItemForEmployeeName.MaxSize = new System.Drawing.Size(547, 24);
            this.ItemForEmployeeName.MinSize = new System.Drawing.Size(547, 24);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(547, 24);
            this.ItemForEmployeeName.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEmployeeName.Text = "Employee:";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForHolidayYearStartDate
            // 
            this.ItemForHolidayYearStartDate.AllowHide = false;
            this.ItemForHolidayYearStartDate.Control = this.HolidayYearStartDateDateEdit;
            this.ItemForHolidayYearStartDate.CustomizationFormText = "Holiday Year Start Date:";
            this.ItemForHolidayYearStartDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForHolidayYearStartDate.MaxSize = new System.Drawing.Size(283, 24);
            this.ItemForHolidayYearStartDate.MinSize = new System.Drawing.Size(283, 24);
            this.ItemForHolidayYearStartDate.Name = "ItemForHolidayYearStartDate";
            this.ItemForHolidayYearStartDate.Size = new System.Drawing.Size(283, 24);
            this.ItemForHolidayYearStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHolidayYearStartDate.Text = "Holiday Year Start Date:";
            this.ItemForHolidayYearStartDate.TextSize = new System.Drawing.Size(130, 13);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.Location = new System.Drawing.Point(283, 47);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(548, 72);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForHolidayYearEndDate
            // 
            this.ItemForHolidayYearEndDate.AllowHide = false;
            this.ItemForHolidayYearEndDate.Control = this.HolidayYearEndDateDateEdit;
            this.ItemForHolidayYearEndDate.CustomizationFormText = "Holiday Year End Date:";
            this.ItemForHolidayYearEndDate.Location = new System.Drawing.Point(0, 71);
            this.ItemForHolidayYearEndDate.Name = "ItemForHolidayYearEndDate";
            this.ItemForHolidayYearEndDate.Size = new System.Drawing.Size(283, 24);
            this.ItemForHolidayYearEndDate.Text = "Holiday Year End Date:";
            this.ItemForHolidayYearEndDate.TextSize = new System.Drawing.Size(130, 13);
            // 
            // ItemForActualStartDate
            // 
            this.ItemForActualStartDate.AllowHide = false;
            this.ItemForActualStartDate.Control = this.ActualStartDateDateEdit;
            this.ItemForActualStartDate.CustomizationFormText = "Actual Start Date:";
            this.ItemForActualStartDate.Location = new System.Drawing.Point(0, 95);
            this.ItemForActualStartDate.Name = "ItemForActualStartDate";
            this.ItemForActualStartDate.Size = new System.Drawing.Size(283, 24);
            this.ItemForActualStartDate.Text = "Actual Start Date:";
            this.ItemForActualStartDate.TextSize = new System.Drawing.Size(130, 13);
            // 
            // emptySpaceItem14
            // 
            this.emptySpaceItem14.AllowHotTrack = false;
            this.emptySpaceItem14.Location = new System.Drawing.Point(818, 131);
            this.emptySpaceItem14.Name = "emptySpaceItem14";
            this.emptySpaceItem14.Size = new System.Drawing.Size(13, 403);
            this.emptySpaceItem14.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(547, 23);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(284, 24);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(0, 504);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(818, 30);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_HR_00002_Employee_Depts_Worked_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00013_Get_Employee_Address_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00017_Pension_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00019_Get_Employee_Next_Of_Kin_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00023_Master_Salary_Bandings_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00040_Employee_Bonus_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00085_Master_Holiday_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00093_Business_Area_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00095_Location_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00097_Department_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00100_Region_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00102_Department_Location_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00132_Employee_Vetting_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00137_Employee_Vetting_Restriction_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00143_Master_Vetting_Types_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00145_Master_Vetting_SubType_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00148_Master_Vetting_Restriction_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00155_Employee_Working_Pattern_Headers_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00164_Master_Working_Pattern_Headers_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00166_Master_Working_Pattern_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00175_Employee_Pension_Contribution_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00182_Linked_Document_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00194_Employee_CRM_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00201_Employee_Reference_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00214_Employee_PayRise_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00226_Employee_Shares_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00232_Master_Qualification_Types_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00250_Employee_Administer_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00255_Business_Area_Insurance_Requirement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00258_Employee_Subsistence_EditTableAdapter = null;
            this.tableAdapterManager.sp09002_HR_Employee_ItemTableAdapter = null;
            this.tableAdapterManager.sp09105_HR_Qualification_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_HR_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter
            // 
            this.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter
            // 
            this.sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 269);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(770, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frm_HR_Employee_Holiday_Year_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(851, 610);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Holiday_Year_Edit";
            this.Text = "Edit Employee Holiday Year";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Holiday_Year_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Holiday_Year_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Holiday_Year_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ActiveHolidayYearCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00010GetEmployeeHolidayYearItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidaysRemainingTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidaysTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidayEntitlementTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidaysRemainingTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidaysRemainingTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceUnauthorisedTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceAuthorisedTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidaysTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidaysTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeForenameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MustTakeCertainHolidaysCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursPerHolidayDaySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayUnitDescriptorIdGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00082HolidayUnitDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchasedHolidayEntitlementSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidayEntitlementSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BasicHolidayEntitlementSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualStartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActualStartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearEndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearEndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearStartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearStartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeForename)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBasicHolidayEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidayEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidayEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdjustHolidaysButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchasedHolidayEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayUnitDescriptorId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursPerHolidayDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMustTakeCertainHolidays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidaysTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidaysTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceAuthorisedTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceUnauthorisedTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidaysTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidaysRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidaysRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidaysRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActiveHolidayYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayYearStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayYearEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActualStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeID;
        private DevExpress.XtraEditors.TextEdit EmployeeIDTextEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private WoodPlanDataSet woodPlanDataSet;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit IdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForId;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DataSet_HR_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraEditors.ButtonEdit EmployeeNameButtonEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00010GetEmployeeHolidayYearItemBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter;
        private DevExpress.XtraEditors.DateEdit HolidayYearEndDateDateEdit;
        private DevExpress.XtraEditors.DateEdit HolidayYearStartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidayYearStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidayYearEndDate;
        private DevExpress.XtraEditors.DateEdit ActualStartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActualStartDate;
        private DevExpress.XtraEditors.SpinEdit BankHolidayEntitlementSpinEdit;
        private DevExpress.XtraEditors.SpinEdit BasicHolidayEntitlementSpinEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBasicHolidayEntitlement;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBankHolidayEntitlement;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.SpinEdit PurchasedHolidayEntitlementSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPurchasedHolidayEntitlement;
        private DevExpress.XtraEditors.GridLookUpEdit HolidayUnitDescriptorIdGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidayUnitDescriptorId;
        private System.Windows.Forms.BindingSource spHR00082HolidayUnitDescriptorsWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.SpinEdit HoursPerHolidayDaySpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHoursPerHolidayDay;
        private DevExpress.XtraEditors.CheckEdit MustTakeCertainHolidaysCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMustTakeCertainHolidays;
        private DevExpress.XtraEditors.TextEdit EmployeeForenameTextEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeSurnameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeSurname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeForename;
        private DevExpress.XtraEditors.TextEdit EmployeeNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeNumber;
        private DevExpress.XtraEditors.TextEdit HolidaysTakenTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidaysTaken;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraEditors.TextEdit BankHolidaysTakenTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBankHolidaysTaken;
        private DevExpress.XtraEditors.TextEdit AbsenceAuthorisedTakenTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceAuthorisedTaken;
        private DevExpress.XtraEditors.TextEdit AbsenceUnauthorisedTakenTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceUnauthorisedTaken;
        private DevExpress.XtraEditors.TextEdit HolidaysRemainingTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidaysRemaining;
        private DevExpress.XtraEditors.TextEdit BankHolidaysRemainingTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBankHolidaysRemaining;
        private DevExpress.XtraEditors.TextEdit TotalHolidayEntitlementTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalHolidayEntitlement;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraEditors.TextEdit TotalHolidaysTakenTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalHolidaysTaken;
        private DevExpress.XtraEditors.TextEdit TotalHolidaysRemainingTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalHolidaysRemaining;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraEditors.CheckEdit ActiveHolidayYearCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActiveHolidayYear;
        private DevExpress.XtraEditors.SimpleButton btnAdjustHolidays;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAdjustHolidaysButton;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}

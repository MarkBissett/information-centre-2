namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Reference
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00206SelectEmployeeReferenceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReferenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCommunicationMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuppliedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuppliedByContactTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuppliedByContactEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelationshipTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRequested = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRequestedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceSatisfactory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRemarks9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCommunicationMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestedByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelationshipType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00206_Select_Employee_ReferenceTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00206_Select_Employee_ReferenceTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00206SelectEmployeeReferenceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00206SelectEmployeeReferenceBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00206SelectEmployeeReferenceBindingSource
            // 
            this.spHR00206SelectEmployeeReferenceBindingSource.DataMember = "sp_HR_00206_Select_Employee_Reference";
            this.spHR00206SelectEmployeeReferenceBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReferenceID,
            this.colEmployeeID11,
            this.colReferenceTypeID,
            this.colCommunicationMethodID,
            this.colSuppliedByName,
            this.colSuppliedByContactTel,
            this.colSuppliedByContactEmail,
            this.colCompanyName,
            this.colRelationshipTypeID,
            this.colDateRequested,
            this.colRequestedByPersonID,
            this.colDateReceived,
            this.colReferenceSatisfactory,
            this.colRemarks9,
            this.colEmployeeName7,
            this.colEmployeeSurname8,
            this.colEmployeeFirstname6,
            this.colEmployeeNumber11,
            this.colCommunicationMethod,
            this.colRequestedByPerson,
            this.colReferenceType,
            this.colRelationshipType});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName7, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRequested, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colReferenceID
            // 
            this.colReferenceID.Caption = "Reference ID";
            this.colReferenceID.FieldName = "ReferenceID";
            this.colReferenceID.Name = "colReferenceID";
            this.colReferenceID.OptionsColumn.AllowEdit = false;
            this.colReferenceID.OptionsColumn.AllowFocus = false;
            this.colReferenceID.OptionsColumn.ReadOnly = true;
            this.colReferenceID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceID.Width = 85;
            // 
            // colEmployeeID11
            // 
            this.colEmployeeID11.Caption = "Employee ID";
            this.colEmployeeID11.FieldName = "EmployeeID";
            this.colEmployeeID11.Name = "colEmployeeID11";
            this.colEmployeeID11.OptionsColumn.AllowEdit = false;
            this.colEmployeeID11.OptionsColumn.AllowFocus = false;
            this.colEmployeeID11.OptionsColumn.ReadOnly = true;
            this.colEmployeeID11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID11.Width = 81;
            // 
            // colReferenceTypeID
            // 
            this.colReferenceTypeID.Caption = "Reference Type ID";
            this.colReferenceTypeID.FieldName = "ReferenceTypeID";
            this.colReferenceTypeID.Name = "colReferenceTypeID";
            this.colReferenceTypeID.OptionsColumn.AllowEdit = false;
            this.colReferenceTypeID.OptionsColumn.AllowFocus = false;
            this.colReferenceTypeID.OptionsColumn.ReadOnly = true;
            this.colReferenceTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceTypeID.Width = 112;
            // 
            // colCommunicationMethodID
            // 
            this.colCommunicationMethodID.Caption = "Communication Method ID";
            this.colCommunicationMethodID.FieldName = "CommunicationMethodID";
            this.colCommunicationMethodID.Name = "colCommunicationMethodID";
            this.colCommunicationMethodID.OptionsColumn.AllowEdit = false;
            this.colCommunicationMethodID.OptionsColumn.AllowFocus = false;
            this.colCommunicationMethodID.OptionsColumn.ReadOnly = true;
            this.colCommunicationMethodID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCommunicationMethodID.Width = 146;
            // 
            // colSuppliedByName
            // 
            this.colSuppliedByName.Caption = "Supplied By Name";
            this.colSuppliedByName.FieldName = "SuppliedByName";
            this.colSuppliedByName.Name = "colSuppliedByName";
            this.colSuppliedByName.OptionsColumn.AllowEdit = false;
            this.colSuppliedByName.OptionsColumn.AllowFocus = false;
            this.colSuppliedByName.OptionsColumn.ReadOnly = true;
            this.colSuppliedByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSuppliedByName.Visible = true;
            this.colSuppliedByName.VisibleIndex = 4;
            this.colSuppliedByName.Width = 145;
            // 
            // colSuppliedByContactTel
            // 
            this.colSuppliedByContactTel.Caption = "Supplied By Contact Tel";
            this.colSuppliedByContactTel.FieldName = "SuppliedByContactTel";
            this.colSuppliedByContactTel.Name = "colSuppliedByContactTel";
            this.colSuppliedByContactTel.OptionsColumn.AllowEdit = false;
            this.colSuppliedByContactTel.OptionsColumn.AllowFocus = false;
            this.colSuppliedByContactTel.OptionsColumn.ReadOnly = true;
            this.colSuppliedByContactTel.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSuppliedByContactTel.Visible = true;
            this.colSuppliedByContactTel.VisibleIndex = 7;
            this.colSuppliedByContactTel.Width = 134;
            // 
            // colSuppliedByContactEmail
            // 
            this.colSuppliedByContactEmail.Caption = "Supplied By Contact Email";
            this.colSuppliedByContactEmail.FieldName = "SuppliedByContactEmail";
            this.colSuppliedByContactEmail.Name = "colSuppliedByContactEmail";
            this.colSuppliedByContactEmail.OptionsColumn.AllowEdit = false;
            this.colSuppliedByContactEmail.OptionsColumn.AllowFocus = false;
            this.colSuppliedByContactEmail.OptionsColumn.ReadOnly = true;
            this.colSuppliedByContactEmail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSuppliedByContactEmail.Visible = true;
            this.colSuppliedByContactEmail.VisibleIndex = 8;
            this.colSuppliedByContactEmail.Width = 144;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 5;
            this.colCompanyName.Width = 126;
            // 
            // colRelationshipTypeID
            // 
            this.colRelationshipTypeID.Caption = "Relationship Type ID";
            this.colRelationshipTypeID.FieldName = "RelationshipTypeID";
            this.colRelationshipTypeID.Name = "colRelationshipTypeID";
            this.colRelationshipTypeID.OptionsColumn.AllowEdit = false;
            this.colRelationshipTypeID.OptionsColumn.AllowFocus = false;
            this.colRelationshipTypeID.OptionsColumn.ReadOnly = true;
            this.colRelationshipTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRelationshipTypeID.Width = 120;
            // 
            // colDateRequested
            // 
            this.colDateRequested.Caption = "Date Requested";
            this.colDateRequested.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateRequested.FieldName = "DateRequested";
            this.colDateRequested.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRequested.Name = "colDateRequested";
            this.colDateRequested.OptionsColumn.AllowEdit = false;
            this.colDateRequested.OptionsColumn.AllowFocus = false;
            this.colDateRequested.OptionsColumn.ReadOnly = true;
            this.colDateRequested.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRequested.Visible = true;
            this.colDateRequested.VisibleIndex = 1;
            this.colDateRequested.Width = 112;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colRequestedByPersonID
            // 
            this.colRequestedByPersonID.Caption = "Requested By Person ID";
            this.colRequestedByPersonID.FieldName = "RequestedByPersonID";
            this.colRequestedByPersonID.Name = "colRequestedByPersonID";
            this.colRequestedByPersonID.OptionsColumn.AllowEdit = false;
            this.colRequestedByPersonID.OptionsColumn.AllowFocus = false;
            this.colRequestedByPersonID.OptionsColumn.ReadOnly = true;
            this.colRequestedByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRequestedByPersonID.Width = 138;
            // 
            // colDateReceived
            // 
            this.colDateReceived.Caption = "Date Received";
            this.colDateReceived.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateReceived.FieldName = "DateReceived";
            this.colDateReceived.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateReceived.Name = "colDateReceived";
            this.colDateReceived.OptionsColumn.AllowEdit = false;
            this.colDateReceived.OptionsColumn.AllowFocus = false;
            this.colDateReceived.OptionsColumn.ReadOnly = true;
            this.colDateReceived.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateReceived.Visible = true;
            this.colDateReceived.VisibleIndex = 2;
            this.colDateReceived.Width = 99;
            // 
            // colReferenceSatisfactory
            // 
            this.colReferenceSatisfactory.Caption = "Satisfactory";
            this.colReferenceSatisfactory.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReferenceSatisfactory.FieldName = "ReferenceSatisfactory";
            this.colReferenceSatisfactory.Name = "colReferenceSatisfactory";
            this.colReferenceSatisfactory.OptionsColumn.AllowEdit = false;
            this.colReferenceSatisfactory.OptionsColumn.AllowFocus = false;
            this.colReferenceSatisfactory.OptionsColumn.ReadOnly = true;
            this.colReferenceSatisfactory.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceSatisfactory.Visible = true;
            this.colReferenceSatisfactory.VisibleIndex = 3;
            this.colReferenceSatisfactory.Width = 79;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colRemarks9
            // 
            this.colRemarks9.Caption = "Remarks";
            this.colRemarks9.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks9.FieldName = "Remarks";
            this.colRemarks9.Name = "colRemarks9";
            this.colRemarks9.OptionsColumn.ReadOnly = true;
            this.colRemarks9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks9.Visible = true;
            this.colRemarks9.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colEmployeeName7
            // 
            this.colEmployeeName7.Caption = "Linked To Employee";
            this.colEmployeeName7.FieldName = "EmployeeName";
            this.colEmployeeName7.Name = "colEmployeeName7";
            this.colEmployeeName7.OptionsColumn.AllowEdit = false;
            this.colEmployeeName7.OptionsColumn.AllowFocus = false;
            this.colEmployeeName7.OptionsColumn.ReadOnly = true;
            this.colEmployeeName7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName7.Visible = true;
            this.colEmployeeName7.VisibleIndex = 9;
            this.colEmployeeName7.Width = 277;
            // 
            // colEmployeeSurname8
            // 
            this.colEmployeeSurname8.Caption = "Employee Surname";
            this.colEmployeeSurname8.FieldName = "EmployeeSurname";
            this.colEmployeeSurname8.Name = "colEmployeeSurname8";
            this.colEmployeeSurname8.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname8.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname8.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname8.Width = 112;
            // 
            // colEmployeeFirstname6
            // 
            this.colEmployeeFirstname6.Caption = "Employee Forename";
            this.colEmployeeFirstname6.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname6.Name = "colEmployeeFirstname6";
            this.colEmployeeFirstname6.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname6.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname6.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname6.Width = 118;
            // 
            // colEmployeeNumber11
            // 
            this.colEmployeeNumber11.Caption = "Employee #";
            this.colEmployeeNumber11.FieldName = "EmployeeNumber";
            this.colEmployeeNumber11.Name = "colEmployeeNumber11";
            this.colEmployeeNumber11.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber11.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber11.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber11.Width = 78;
            // 
            // colCommunicationMethod
            // 
            this.colCommunicationMethod.Caption = "Communication Method";
            this.colCommunicationMethod.FieldName = "CommunicationMethod";
            this.colCommunicationMethod.Name = "colCommunicationMethod";
            this.colCommunicationMethod.OptionsColumn.AllowEdit = false;
            this.colCommunicationMethod.OptionsColumn.AllowFocus = false;
            this.colCommunicationMethod.OptionsColumn.ReadOnly = true;
            this.colCommunicationMethod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCommunicationMethod.Visible = true;
            this.colCommunicationMethod.VisibleIndex = 9;
            this.colCommunicationMethod.Width = 132;
            // 
            // colRequestedByPerson
            // 
            this.colRequestedByPerson.Caption = "Requested By Person";
            this.colRequestedByPerson.FieldName = "RequestedByPerson";
            this.colRequestedByPerson.Name = "colRequestedByPerson";
            this.colRequestedByPerson.OptionsColumn.AllowEdit = false;
            this.colRequestedByPerson.OptionsColumn.AllowFocus = false;
            this.colRequestedByPerson.OptionsColumn.ReadOnly = true;
            this.colRequestedByPerson.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRequestedByPerson.Visible = true;
            this.colRequestedByPerson.VisibleIndex = 10;
            this.colRequestedByPerson.Width = 124;
            // 
            // colReferenceType
            // 
            this.colReferenceType.Caption = "Reference Type";
            this.colReferenceType.FieldName = "ReferenceType";
            this.colReferenceType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceType.Name = "colReferenceType";
            this.colReferenceType.OptionsColumn.AllowEdit = false;
            this.colReferenceType.OptionsColumn.AllowFocus = false;
            this.colReferenceType.OptionsColumn.ReadOnly = true;
            this.colReferenceType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceType.Visible = true;
            this.colReferenceType.VisibleIndex = 0;
            this.colReferenceType.Width = 154;
            // 
            // colRelationshipType
            // 
            this.colRelationshipType.Caption = "Relationship Type";
            this.colRelationshipType.FieldName = "RelationshipType";
            this.colRelationshipType.Name = "colRelationshipType";
            this.colRelationshipType.OptionsColumn.AllowEdit = false;
            this.colRelationshipType.OptionsColumn.AllowFocus = false;
            this.colRelationshipType.OptionsColumn.ReadOnly = true;
            this.colRelationshipType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRelationshipType.Visible = true;
            this.colRelationshipType.VisibleIndex = 6;
            this.colRelationshipType.Width = 109;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00206_Select_Employee_ReferenceTableAdapter
            // 
            this.sp_HR_00206_Select_Employee_ReferenceTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_Reference
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Reference";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Reference";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Reference_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00206SelectEmployeeReferenceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private System.Windows.Forms.BindingSource spHR00206SelectEmployeeReferenceBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID11;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCommunicationMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colSuppliedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colSuppliedByContactTel;
        private DevExpress.XtraGrid.Columns.GridColumn colSuppliedByContactEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colRelationshipTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRequested;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceSatisfactory;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks9;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName7;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname8;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname6;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber11;
        private DevExpress.XtraGrid.Columns.GridColumn colCommunicationMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceType;
        private DevExpress.XtraGrid.Columns.GridColumn colRelationshipType;
        private DataSet_HR_CoreTableAdapters.sp_HR_00206_Select_Employee_ReferenceTableAdapter sp_HR_00206_Select_Employee_ReferenceTableAdapter;
    }
}

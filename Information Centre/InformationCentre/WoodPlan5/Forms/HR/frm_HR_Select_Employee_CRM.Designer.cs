namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_CRM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00198SelectEmployeeCRMBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCRMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContactMadeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactWithEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactWithEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00198_Select_Employee_CRMTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00198_Select_Employee_CRMTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00198SelectEmployeeCRMBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00198SelectEmployeeCRMBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime,
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00198SelectEmployeeCRMBindingSource
            // 
            this.spHR00198SelectEmployeeCRMBindingSource.DataMember = "sp_HR_00198_Select_Employee_CRM";
            this.spHR00198SelectEmployeeCRMBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCRMID,
            this.colEmployeeID10,
            this.colContactDueDate,
            this.colContactMadeDate,
            this.colContactMethodID,
            this.colContactWithEmployeeID,
            this.colRecordedByEmployeeID,
            this.colDateRecorded,
            this.colDescription2,
            this.colContactDirectionID,
            this.colStatusID2,
            this.colRemarks8,
            this.colEmployeeName6,
            this.colEmployeeSurname7,
            this.colEmployeeFirstname5,
            this.colEmployeeNumber10,
            this.colContactWithEmployeeName,
            this.colRecordedByEmployeeName,
            this.colContactDirection,
            this.colStatus2,
            this.colContactMethod});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurname7, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeFirstname5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContactDueDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colCRMID
            // 
            this.colCRMID.Caption = "CRM ID";
            this.colCRMID.FieldName = "CRMID";
            this.colCRMID.Name = "colCRMID";
            this.colCRMID.OptionsColumn.AllowEdit = false;
            this.colCRMID.OptionsColumn.AllowFocus = false;
            this.colCRMID.OptionsColumn.ReadOnly = true;
            this.colCRMID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCRMID.Width = 57;
            // 
            // colEmployeeID10
            // 
            this.colEmployeeID10.Caption = "Employee ID";
            this.colEmployeeID10.FieldName = "EmployeeID";
            this.colEmployeeID10.Name = "colEmployeeID10";
            this.colEmployeeID10.OptionsColumn.AllowEdit = false;
            this.colEmployeeID10.OptionsColumn.AllowFocus = false;
            this.colEmployeeID10.OptionsColumn.ReadOnly = true;
            this.colEmployeeID10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID10.Width = 81;
            // 
            // colContactDueDate
            // 
            this.colContactDueDate.Caption = "Contact Due";
            this.colContactDueDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colContactDueDate.FieldName = "ContactDueDate";
            this.colContactDueDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactDueDate.Name = "colContactDueDate";
            this.colContactDueDate.OptionsColumn.AllowEdit = false;
            this.colContactDueDate.OptionsColumn.AllowFocus = false;
            this.colContactDueDate.OptionsColumn.ReadOnly = true;
            this.colContactDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactDueDate.Visible = true;
            this.colContactDueDate.VisibleIndex = 3;
            this.colContactDueDate.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colContactMadeDate
            // 
            this.colContactMadeDate.Caption = "Contact Made";
            this.colContactMadeDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colContactMadeDate.FieldName = "ContactMadeDate";
            this.colContactMadeDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactMadeDate.Name = "colContactMadeDate";
            this.colContactMadeDate.OptionsColumn.AllowEdit = false;
            this.colContactMadeDate.OptionsColumn.AllowFocus = false;
            this.colContactMadeDate.OptionsColumn.ReadOnly = true;
            this.colContactMadeDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactMadeDate.Visible = true;
            this.colContactMadeDate.VisibleIndex = 4;
            this.colContactMadeDate.Width = 100;
            // 
            // colContactMethodID
            // 
            this.colContactMethodID.Caption = "Contact Method ID";
            this.colContactMethodID.FieldName = "ContactMethodID";
            this.colContactMethodID.Name = "colContactMethodID";
            this.colContactMethodID.OptionsColumn.AllowEdit = false;
            this.colContactMethodID.OptionsColumn.AllowFocus = false;
            this.colContactMethodID.OptionsColumn.ReadOnly = true;
            this.colContactMethodID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContactMethodID.Width = 112;
            // 
            // colContactWithEmployeeID
            // 
            this.colContactWithEmployeeID.Caption = "Contact With Employee ID";
            this.colContactWithEmployeeID.FieldName = "ContactWithEmployeeID";
            this.colContactWithEmployeeID.Name = "colContactWithEmployeeID";
            this.colContactWithEmployeeID.OptionsColumn.AllowEdit = false;
            this.colContactWithEmployeeID.OptionsColumn.AllowFocus = false;
            this.colContactWithEmployeeID.OptionsColumn.ReadOnly = true;
            this.colContactWithEmployeeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContactWithEmployeeID.Width = 147;
            // 
            // colRecordedByEmployeeID
            // 
            this.colRecordedByEmployeeID.Caption = "Recorded By Employee ID";
            this.colRecordedByEmployeeID.FieldName = "RecordedByEmployeeID";
            this.colRecordedByEmployeeID.Name = "colRecordedByEmployeeID";
            this.colRecordedByEmployeeID.OptionsColumn.AllowEdit = false;
            this.colRecordedByEmployeeID.OptionsColumn.AllowFocus = false;
            this.colRecordedByEmployeeID.OptionsColumn.ReadOnly = true;
            this.colRecordedByEmployeeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRecordedByEmployeeID.Width = 145;
            // 
            // colDateRecorded
            // 
            this.colDateRecorded.Caption = "Date Recorded";
            this.colDateRecorded.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateRecorded.FieldName = "DateRecorded";
            this.colDateRecorded.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRecorded.Name = "colDateRecorded";
            this.colDateRecorded.OptionsColumn.AllowEdit = false;
            this.colDateRecorded.OptionsColumn.AllowFocus = false;
            this.colDateRecorded.OptionsColumn.ReadOnly = true;
            this.colDateRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRecorded.Visible = true;
            this.colDateRecorded.VisibleIndex = 5;
            this.colDateRecorded.Width = 100;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Description";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 6;
            this.colDescription2.Width = 249;
            // 
            // colContactDirectionID
            // 
            this.colContactDirectionID.Caption = "Contact Direction ID";
            this.colContactDirectionID.FieldName = "ContactDirectionID";
            this.colContactDirectionID.Name = "colContactDirectionID";
            this.colContactDirectionID.OptionsColumn.AllowEdit = false;
            this.colContactDirectionID.OptionsColumn.AllowFocus = false;
            this.colContactDirectionID.OptionsColumn.ReadOnly = true;
            this.colContactDirectionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContactDirectionID.Width = 118;
            // 
            // colStatusID2
            // 
            this.colStatusID2.Caption = "Status ID";
            this.colStatusID2.FieldName = "StatusID";
            this.colStatusID2.Name = "colStatusID2";
            this.colStatusID2.OptionsColumn.AllowEdit = false;
            this.colStatusID2.OptionsColumn.AllowFocus = false;
            this.colStatusID2.OptionsColumn.ReadOnly = true;
            this.colStatusID2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colRemarks8
            // 
            this.colRemarks8.Caption = "Remarks";
            this.colRemarks8.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks8.FieldName = "Remarks";
            this.colRemarks8.Name = "colRemarks8";
            this.colRemarks8.OptionsColumn.ReadOnly = true;
            this.colRemarks8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks8.Visible = true;
            this.colRemarks8.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colEmployeeName6
            // 
            this.colEmployeeName6.Caption = "Linked To Employee";
            this.colEmployeeName6.FieldName = "EmployeeName";
            this.colEmployeeName6.Name = "colEmployeeName6";
            this.colEmployeeName6.OptionsColumn.AllowEdit = false;
            this.colEmployeeName6.OptionsColumn.AllowFocus = false;
            this.colEmployeeName6.OptionsColumn.ReadOnly = true;
            this.colEmployeeName6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName6.Width = 247;
            // 
            // colEmployeeSurname7
            // 
            this.colEmployeeSurname7.Caption = "Employee Surname";
            this.colEmployeeSurname7.FieldName = "EmployeeSurname";
            this.colEmployeeSurname7.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeSurname7.Name = "colEmployeeSurname7";
            this.colEmployeeSurname7.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname7.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname7.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname7.Visible = true;
            this.colEmployeeSurname7.VisibleIndex = 0;
            this.colEmployeeSurname7.Width = 125;
            // 
            // colEmployeeFirstname5
            // 
            this.colEmployeeFirstname5.Caption = "Employee Forename";
            this.colEmployeeFirstname5.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname5.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeFirstname5.Name = "colEmployeeFirstname5";
            this.colEmployeeFirstname5.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname5.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname5.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname5.Visible = true;
            this.colEmployeeFirstname5.VisibleIndex = 1;
            this.colEmployeeFirstname5.Width = 131;
            // 
            // colEmployeeNumber10
            // 
            this.colEmployeeNumber10.Caption = "Employee #";
            this.colEmployeeNumber10.FieldName = "EmployeeNumber";
            this.colEmployeeNumber10.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeNumber10.Name = "colEmployeeNumber10";
            this.colEmployeeNumber10.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber10.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber10.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber10.Visible = true;
            this.colEmployeeNumber10.VisibleIndex = 2;
            this.colEmployeeNumber10.Width = 78;
            // 
            // colContactWithEmployeeName
            // 
            this.colContactWithEmployeeName.Caption = "Contact with Person";
            this.colContactWithEmployeeName.FieldName = "ContactWithEmployeeName";
            this.colContactWithEmployeeName.Name = "colContactWithEmployeeName";
            this.colContactWithEmployeeName.OptionsColumn.AllowEdit = false;
            this.colContactWithEmployeeName.OptionsColumn.AllowFocus = false;
            this.colContactWithEmployeeName.OptionsColumn.ReadOnly = true;
            this.colContactWithEmployeeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContactWithEmployeeName.Visible = true;
            this.colContactWithEmployeeName.VisibleIndex = 7;
            this.colContactWithEmployeeName.Width = 135;
            // 
            // colRecordedByEmployeeName
            // 
            this.colRecordedByEmployeeName.Caption = "Recorded By";
            this.colRecordedByEmployeeName.FieldName = "RecordedByEmployeeName";
            this.colRecordedByEmployeeName.Name = "colRecordedByEmployeeName";
            this.colRecordedByEmployeeName.OptionsColumn.AllowEdit = false;
            this.colRecordedByEmployeeName.OptionsColumn.AllowFocus = false;
            this.colRecordedByEmployeeName.OptionsColumn.ReadOnly = true;
            this.colRecordedByEmployeeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRecordedByEmployeeName.Visible = true;
            this.colRecordedByEmployeeName.VisibleIndex = 8;
            this.colRecordedByEmployeeName.Width = 135;
            // 
            // colContactDirection
            // 
            this.colContactDirection.Caption = "Contact Direction";
            this.colContactDirection.FieldName = "ContactDirection";
            this.colContactDirection.Name = "colContactDirection";
            this.colContactDirection.OptionsColumn.AllowEdit = false;
            this.colContactDirection.OptionsColumn.AllowFocus = false;
            this.colContactDirection.OptionsColumn.ReadOnly = true;
            this.colContactDirection.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContactDirection.Visible = true;
            this.colContactDirection.VisibleIndex = 10;
            this.colContactDirection.Width = 130;
            // 
            // colStatus2
            // 
            this.colStatus2.Caption = "Status";
            this.colStatus2.FieldName = "Status";
            this.colStatus2.Name = "colStatus2";
            this.colStatus2.OptionsColumn.AllowEdit = false;
            this.colStatus2.OptionsColumn.AllowFocus = false;
            this.colStatus2.OptionsColumn.ReadOnly = true;
            this.colStatus2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus2.Visible = true;
            this.colStatus2.VisibleIndex = 9;
            // 
            // colContactMethod
            // 
            this.colContactMethod.Caption = "Contact Method";
            this.colContactMethod.FieldName = "ContactMethod";
            this.colContactMethod.Name = "colContactMethod";
            this.colContactMethod.OptionsColumn.AllowEdit = false;
            this.colContactMethod.OptionsColumn.AllowFocus = false;
            this.colContactMethod.OptionsColumn.ReadOnly = true;
            this.colContactMethod.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContactMethod.Visible = true;
            this.colContactMethod.VisibleIndex = 11;
            this.colContactMethod.Width = 98;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00198_Select_Employee_CRMTableAdapter
            // 
            this.sp_HR_00198_Select_Employee_CRMTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_CRM
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_CRM";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee CRM";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_CRM_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00198SelectEmployeeCRMBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private System.Windows.Forms.BindingSource spHR00198SelectEmployeeCRMBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCRMID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID10;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMadeDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactWithEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID2;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName6;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname7;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname5;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber10;
        private DevExpress.XtraGrid.Columns.GridColumn colContactWithEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirection;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus2;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethod;
        private DataSet_HR_CoreTableAdapters.sp_HR_00198_Select_Employee_CRMTableAdapter sp_HR_00198_Select_Employee_CRMTableAdapter;
    }
}

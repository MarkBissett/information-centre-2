using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class frm_HR_Sanction_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        public string strLinkedToRecordDesc = "";
        public string strLinkedToRecordDesc2 = "";
        public string strLinkedToRecordDesc3 = "";
        public string strLinkedToRecordDesc4 = "";
        public int intLinkedToRecordID = 0;

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        bool iBool_AllowAdd = true;
        bool iBool_AllowEdit = true;
        bool iBool_AllowDelete = true;

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        //private enmFocusedGrids _enmFocusedGrid = enmFocusedGrids.enmSanctionProgress;


        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        string _strLastUsedReferencePrefix = "";

        #endregion

        public frm_HR_Sanction_Edit()
        {
            InitializeComponent();
        }

        private void frm_HR_Sanction_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 990109;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00073_Sanction_Types_List_With_Blank);

            sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00074_Sanction_Statuses_List_With_Blank);

            sp_HR_00075_Sanction_Duration_Units_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00075_Sanction_Duration_Units_Descriptors_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00075_Sanction_Duration_Units_Descriptors_With_Blank);

            sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00039_Appeal_Outcomes_List_With_Blank);

            sp_HR_00129_Discipline_Investigation_Outcomes_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00129_Discipline_Investigation_Outcomes_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00129_Discipline_Investigation_Outcomes_With_Blank);

            sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00130_Sanction_Hearing_Outcome_List_With_Blank);

            RefreshGridViewState1 = new RefreshGridState(gridView2, "SanctionProgressId");

            LoadLastSavedUserScreenSettings();  // Get any used sequence number prefix //

            // Populate Main Dataset //          
            sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter.Connection.ConnectionString = strConnectionString;

            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        
                        var drNewRow =(DataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_ItemsRow)dataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_Items.NewRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordIds = strRecordIDs;
                        drNewRow.EmployeeID = intLinkedToRecordID;
                        drNewRow.EmployeeName = strLinkedToRecordDesc;
                        drNewRow.EmployeeSurname = strLinkedToRecordDesc2;
                        drNewRow.EmployeeFirstname = strLinkedToRecordDesc3;
                        drNewRow.EmployeeNumber = strLinkedToRecordDesc4;
                        drNewRow.ERIssueTypeID = 0;
                        drNewRow.DateRaised = DateTime.Now;
                        drNewRow.InvestigatedByPersonID = 0;
                        drNewRow.InvestigationOutcomeID = 0;
                        drNewRow.HeardByPersonID = 0;
                        drNewRow.RaisedByPersonID = 0;
                        drNewRow.HearingOutcomeID = 0;
                        drNewRow.HearingOutcomeIssuedByPersonID = 0;
                        drNewRow.AppealHeardByPersonID = 0;
                        drNewRow.AppealOutcomeID = 0;
                        drNewRow.ExpiryDurationUnits = 0;
                        drNewRow.ExpiryDurationUnitDescriptorID = 0;
                        drNewRow.HRManagerID = 0;
                        drNewRow.StatusID = 0;

                        dataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_Items.Rows.Add(drNewRow);
                        //if (strFormMode == "add" && !string.IsNullOrEmpty(_strLastUsedReferencePrefix))  // Add Suffix number to Reference Number //
                        //{
                        //    Get_Suffix_Number();  // Get next value in sequence //
                        //}
                        gridControl1.Enabled = false;

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        var drNewRow = (DataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_ItemsRow)dataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_Items.Newsp_HR_00055_Get_Employee_Sanction_ItemsRow();
                        drNewRow.Mode = "blockedit";
                        drNewRow.RecordIds = strRecordIDs;
                        dataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_Items.Addsp_HR_00055_Get_Employee_Sanction_ItemsRow(drNewRow);
                        drNewRow.AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_Items, strFormMode, strRecordIDs);
                    }
                    catch (Exception)
                    {
                    }                 
                    if (strFormMode == "view")
                    {
                        iBool_AllowAdd = false;
                        iBool_AllowEdit = false;
                        iBool_AllowDelete = false;
                    }
                    break;
            }

            if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit")) LoadLinkedRecords();

            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_Items.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Employee Discipline \\ Grievance", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "")
            {
                i_str_AddedRecordIDs1 = strNewIDs1;
                //iBool_AllowAdd = false;     //Only one holiday record ever allowed for the current year.
            }
            if (strFormMode == "add" || strFormMode == "edit")
            {
                LoadLinkedRecords();
                FilterGrids();
            }
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ERIssueTypeIDGridLookUpEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = false;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ERIssueTypeIDGridLookUpEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = true;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ERIssueTypeIDGridLookUpEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = false;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ERIssueTypeIDGridLookUpEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = true;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DefaultBoolean.True;
            }
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            AppealOutcomeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            AppealOutcomeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            AppealOutcomeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            AppealOutcomeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            HearingOutcomeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            HearingOutcomeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            HearingOutcomeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            HearingOutcomeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "917,9178,9179, 9195", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9177:  // Sanction Types //    
                        {
                            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9178:  // Sanction Statuses //    
                        {
                            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            ERIssueTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9179:  // Sanction Resolution Types //    
                        {
                            AppealOutcomeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            AppealOutcomeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            AppealOutcomeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            AppealOutcomeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;                         
                        }
                        break;
                    case 9195:  // Sanction HEaring Outcomes //    
                        {
                            HearingOutcomeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            HearingOutcomeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            HearingOutcomeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            HearingOutcomeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                btnSave.Enabled = bbiSave.Enabled;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                btnSave.Enabled = bbiSave.Enabled;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            if (!iBool_AllowAdd || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["SanctionID"] == null ? 0 : Convert.ToInt32(currentRow["SanctionID"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of Linked Documents button //

            if (i_int_FocusedGrid == 1)  // Sanction Actions //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit" && intID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && strFormMode != "blockedit" && intRowHandles.Length > 0 && intID > 0)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intID > 0 && strFormMode != "blockedit");
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 && strFormMode != "blockedit");
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
        }


        private void frm_HR_Sanction_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Sanction_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;

            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                if (!string.IsNullOrEmpty(_strLastUsedReferencePrefix))
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "PoleNumberPrefix", _strLastUsedReferencePrefix);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }

            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Reference Number Prefix //
                _strLastUsedReferencePrefix = default_screen_settings.RetrieveSetting("PoleNumberPrefix");
            }
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (this.strFormMode == "blockadd" || this.strFormMode == "blockedit") return;

            // Actions //
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
            sp_HR_00057_Get_Employee_Sanction_ProgressTableAdapter.Fill(dataSet_HR_Core.sp_HR_00057_Get_Employee_Sanction_Progress, strRecordIDs);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["Id"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
            try
            {
                this.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter.Update(dataSet_HR_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            if (strFormMode == "add" || strFormMode == "edit")
            {
                gridControl1.Enabled = true;  // Child Grid now enabled //
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["SanctionID"]) + ";";
                    strRecordIDs = Convert.ToInt32(currentRow["SanctionID"]) + ",";  // This is required so the linked child records can be loaded into the grid //
                }
                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["Mode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }

            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        var fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Utils.enmMainGrids.Discipline, strNewIDs);
                    }
                    if (frmChild.Name == "frm_HR_Sanction_Manager")
                    {
                        var fParentForm = (frm_HR_Sanction_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                    }
                    if (frmChild.Name == "frm_HR_Employee_Edit")
                    {
                        var fParentForm = (frm_HR_Employee_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(4, Utils.enmMainGrids.Discipline, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_Items.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_Items.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0) //|| intAccessIssuesNew > 0 || intAccessIssuesModified > 0 || intAccessIssuesDeleted > 0 || intEnvironmentalIssuesNew > 0 || intEnvironmentalIssuesModified > 0 || intEnvironmentalIssuesDeleted > 0 || intSiteHazardsNew > 0 || intSiteHazardsModified > 0 || intSiteHazardsDeleted > 0 || intElecticalHazardsNew > 0 || intElecticalHazardsModified > 0 || intElecticalHazardsDeleted > 0
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    FilterGrids();
                    GridView view = (GridView)gridControl1.MainView;
                    view.ExpandAllGroups();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region GridControl1

        private void gridView2_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            string message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Sanction Actions NOT Shown When Block Adding and Block Editing" : "No Linked Sanction Actions Available - Click the Add button to Create Sanction Actions");
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView2_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            //_enmFocusedGrid = enmFocusedGrids.enmSanctionProgress;

            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Editors

        private void RaisedByPersonNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["RaisedByPersonID"].ToString()) ? 0 : Convert.ToInt32(currentRow["RaisedByPersonID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["RaisedByPersonID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["RaisedByPersonName"] = fChildForm.strSelectedEmployeeName;
                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }

        private void HeardByPersonNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["HeardByPersonID"].ToString()) ? 0 : Convert.ToInt32(currentRow["HeardByPersonID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["HeardByPersonID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["HeardByPersonName"] = fChildForm.strSelectedEmployeeName;
                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }

        private void HRManagerNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["HRManagerID"].ToString()) ? 0 : Convert.ToInt32(currentRow["HRManagerID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["HRManagerID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["HRManagerName"] = fChildForm.strSelectedEmployeeName;
                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private void EmployeeNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["EmployeeId"] = fChildForm.intSelectedEmployeeID;
                    currentRow["EmployeeName"] = fChildForm.strSelectedEmployeeName;
                    currentRow["EmployeeSurname"] = fChildForm.strSelectedSurname;
                    currentRow["EmployeeFirstname"] = fChildForm.strSelectedForename;
                    currentRow["EmployeeNumber"] = fChildForm.strSelectedEmployeeNumber;

                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }
        private void EmployeeNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(EmployeeNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(EmployeeNameButtonEdit, "");
            }
        }

        private void SanctionTypeIdGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 315, "HR Sanction Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00073_Sanction_Types_List_With_Blank);
                }
            }
        }

        private void StatusIdGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 316, "HR Sanction Statuses");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00074_Sanction_Statuses_List_With_Blank);
                }
            }
        }

        private void InvestigatedByPersonNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["InvestigatedByPersonID"].ToString()) ? 0 : Convert.ToInt32(currentRow["InvestigatedByPersonID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["InvestigatedByPersonID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["InvestigatedByPersonName"] = fChildForm.strSelectedEmployeeName;

                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }
        private void InvestigationOutcomeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void InvestigationOutcomeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            Calculate_Sanction_Outcome(glue.Name, (glue.EditValue == null ? 0 : Convert.ToInt32(glue.EditValue)), glue.Text);
        }

        private void AppealOutcomeIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 317, "HR Sanction Resolution Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00039_Appeal_Outcomes_List_With_Blank);
                }
            }
        }
        private void AppealOutcomeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void AppealOutcomeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            Calculate_Sanction_Outcome(glue.Name, (glue.EditValue == null ? 0 : Convert.ToInt32(glue.EditValue)), glue.Text);
        }

        private void IssuedByEmployeeNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["IssuedByID"].ToString()) ? 0 : Convert.ToInt32(currentRow["IssuedByID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["IssuedByID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["IssuedByEmployeeName"] = fChildForm.strSelectedEmployeeName;
                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }

        private void AppealHeardByPersonNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["AppealHeardByPersonID"].ToString()) ? 0 : Convert.ToInt32(currentRow["AppealHeardByPersonID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["AppealHeardByPersonID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["AppealHeardByPersonName"] = fChildForm.strSelectedEmployeeName;
                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }

        private void DateRaisedDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void DateRaisedDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Expiry_Date();
        }

        private void ExpiryDurationUnitsSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ExpiryDurationUnitsSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Expiry_Date();
        }

        private void ExpiryDurationUnitDescriptorIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ExpiryDurationUnitDescriptorIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Expiry_Date();
        }

        private void ExpiryDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ExpiryDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Expiry_Date();
        }


        private void Calculate_Expiry_Date()
        {
            if (strFormMode != "add" || strFormMode != "edit") return;
            DateTime dtCalculatedDate = DateTime.MinValue;

            var currentRowView = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            if (currentRowView == null) return;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_ItemsRow)currentRowView.Row;
            if (currentRow == null) return;
            currentRow.Live = 0;  // Set it to Not Live here - will be updated later id sufficient info present //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            if (currentRow.DateRaised == null || currentRow.DateRaised == DateTime.MinValue) return;

            if (currentRow.ExpiryDurationUnits == 0) return;
            if (currentRow.ExpiryDurationUnitDescriptorID == 0) return;

            switch (currentRow.ExpiryDurationUnitDescriptorID)
            {
                case 1:  // Days //
                    dtCalculatedDate = currentRow.DateRaised.AddDays(currentRow.ExpiryDurationUnits);
                    break;
                case 2:  // Weeks //
                    dtCalculatedDate = currentRow.DateRaised.AddDays(currentRow.ExpiryDurationUnits * 7);
                    break;
                case 3:  // Months //
                    dtCalculatedDate = currentRow.DateRaised.AddMonths(currentRow.ExpiryDurationUnits);
                    break;
                case 4:  // Years //
                    dtCalculatedDate = currentRow.DateRaised.AddYears(currentRow.ExpiryDurationUnits);
                    break;
                default:
                    return;
            }
            currentRow.ExpiryDate = dtCalculatedDate;
            if (dtCalculatedDate > DateTime.Today) currentRow.Live = 1;  // Live //

            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
        }

        private void HearingOutcomeIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 331, "HR Sanction Hearing Outcomes");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00130_Sanction_Hearing_Outcome_List_With_Blank);
                }
            }
        }
        private void HearingOutcomeIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void HearingOutcomeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;   
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            Calculate_Sanction_Outcome(glue.Name, (glue.EditValue == null ? 0 : Convert.ToInt32(glue.EditValue)), glue.Text);
        }

        private void HearingOutcomeIssuedByPersonNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["HearingOutcomeIssuedByPersonID"].ToString()) ? 0 : Convert.ToInt32(currentRow["HearingOutcomeIssuedByPersonID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["HearingOutcomeIssuedByPersonID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["HearingOutcomeIssuedByPersonName"] = fChildForm.strSelectedEmployeeName;
                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }

        private void HearingNoteTakerNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["HearingNoteTakerPersonID"].ToString()) ? 0 : Convert.ToInt32(currentRow["HearingNoteTakerPersonID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["HearingNoteTakerPersonID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["HearingNoteTakerName"] = fChildForm.strSelectedEmployeeName;
                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }

        private void AppealNoteTakerNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Employee();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = (string.IsNullOrEmpty(currentRow["AppealNoteTakerPersonID"].ToString()) ? 0 : Convert.ToInt32(currentRow["AppealNoteTakerPersonID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["AppealNoteTakerPersonID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["AppealNoteTakerName"] = fChildForm.strSelectedEmployeeName;
                    spHR00055GetEmployeeSanctionItemsBindingSource.EndEdit();
                }
            }
        }


        private void Calculate_Sanction_Outcome(string strGridName, int intValue, string strValue)
        {
            if (strFormMode != "add" || strFormMode != "edit") return;
            var currentRowView = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            if (currentRowView == null) return;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_ItemsRow)currentRowView.Row;
            if (currentRow == null) return;

            int intAppealOutcomeID = (strGridName == "AppealOutcomeIDGridLookUpEdit" ? intValue : (currentRow.AppealOutcomeID == null ? 0 : currentRow.AppealOutcomeID));
            int intHearingOutcomeID = (strGridName == "HearingOutcomeIDGridLookUpEdit" ? intValue : (currentRow.HearingOutcomeID == null ? 0 : currentRow.HearingOutcomeID));
            int intInvestigationOutcomeID = (strGridName == "InvestigationOutcomeIDGridLookUpEdit" ? intValue : (currentRow.InvestigationOutcomeID == null ? 0 : currentRow.InvestigationOutcomeID));

            string strAppealOutcome = "";
            if (intAppealOutcomeID > 0)
            {
                strAppealOutcome = (strGridName == "AppealOutcomeIDGridLookUpEdit" ? strValue : AppealOutcomeIDGridLookUpEdit.Text.ToString());
            }
            else if (intHearingOutcomeID > 0)
            {
                strAppealOutcome = (strGridName == "HearingOutcomeIDGridLookUpEdit" ? strValue : HearingOutcomeIDGridLookUpEdit.Text.ToString());
            }
            else if (intInvestigationOutcomeID == 1)
            {
                strAppealOutcome = "No Further Action";
            }
            currentRow.SanctionOutcome = strAppealOutcome;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
            DataRowView currentRowView = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_ItemsRow)currentRowView.Row;
            if (currentRow == null) return;
            int intSanctionId = currentRow.SanctionID;
            if (intSanctionId <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Add Sanction Action - Save changes to this sanction first then try again.", "Add Sanction Action", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Sanction_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.ParentForm = this;
                        fChildForm.PassedInId = intSanctionId;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        // Invoke Post Open event on Base form //
                         System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add_Record()
        {
        }

        private void Edit_Record()
        {
            var currentRowView = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            if (currentRowView == null) return;

            var sanctionId = 0;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_ItemsRow)currentRowView.Row;
            sanctionId = currentRow.SanctionID;

            switch (i_int_FocusedGrid)
            {
                case 1:  // Sanction Progress //
                    {
                        var view = (GridView)gridControl1.MainView;
                        var intRowHandles = view.GetSelectedRows();
                        var intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Sanction Action records to edit by clicking on them then try again.", "Edit Linked Sanction Action Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var recordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            recordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Sanction_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = recordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.ParentForm = this;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        // Invoke Post Open event on Base form //
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void Block_Edit_Record()
        {
            var currentRowView = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            if (currentRowView == null) return;

            var sanctionId = 0;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_ItemsRow)currentRowView.Row;
            sanctionId = currentRow.SanctionID;

            switch (i_int_FocusedGrid)
            {
                case 1:  // Sanction Progress //
                    {
                        var view = (GridView)gridControl1.MainView;
                        var intRowHandles = view.GetSelectedRows();
                        var intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Sanction Action records to block edit by clicking on them then try again.", "Block Edit Linked Sanction Action Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var recordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            recordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Sanction_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = recordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.ParentForm = this;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        // Invoke Post Open event on Base form //
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void View_Record()
        {
            var currentRowView = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            if (currentRowView == null) return;

            var sanctionId = 0;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00055_Get_Employee_Sanction_ItemsRow)currentRowView.Row;
            sanctionId = currentRow.SanctionID;

            switch (i_int_FocusedGrid)
            {
                case 1:  // Sanction Progress //
                    {
                        var view = (GridView)gridControl1.MainView;
                        var intRowHandles = view.GetSelectedRows();
                        var intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Sanction Action records to view by clicking on them then try again.", "View Linked Sanction Action Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var recordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            recordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Sanction_Action_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = recordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.ParentForm = this;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        // Invoke Post Open event on Base form //
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Sanction Action //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Sanction Actions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Sanction Action" : Convert.ToString(intRowHandles.Length) + " Sanction Actions") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Sanction Action" : "these Sanction Actions") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "Id")) + ",";
                            }

                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            RemoveRecords.sp09000_HR_Delete("employee_sanction_progress", strRecordIDs);  // Remove the records from the DB in one go //
                            LoadLinkedRecords();
                            FilterGrids();

                            RemoveRecords.Dispose();

                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }

        }

        private void FilterGrids()
        {
            string strID = "";
            DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            if (currentRow != null)
            {
                strID = (currentRow["SanctionID"] == null ? "0" : currentRow["SanctionID"].ToString());
            }

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[SanctionId] = " + Convert.ToString(strID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }


        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)spHR00055GetEmployeeSanctionItemsBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["SanctionID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SanctionID"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 3;  // Sanction //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = currentRow["EmployeeName"].ToString() + " - " + Convert.ToDateTime(currentRow["DateRaised"]).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date"; 
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }



    }
}


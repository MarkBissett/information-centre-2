using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Repository;

namespace WoodPlan5
{
    public partial class frm_HR_Select_Employee_Holiday_Year : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;
    
        public int intOriginalSelectedValue = 0;
        public int intSelectedID = 0;
        public string strSelectedValue = "";
        public int intEmployeeID = 0;
        public string HolidayUnitDescriptor = "";
        public int HolidayUnitDescriptorId = 0;
        public decimal HolidaysTaken = (decimal)0.00;
        public decimal BankHolidaysTaken = (decimal)0.00;
        public decimal PurchasedHolidaysTaken = (decimal)0.00;
        public decimal AbsenceAuthorisedTaken = (decimal)0.00;
        public decimal AbsenceUnauthorisedTaken = (decimal)0.00;
        public decimal BasicHolidayEntitlement = (decimal)0.00;
        public decimal BankHolidayEntitlement = (decimal)0.00;
        public decimal PurchasedHolidayEntitlement = (decimal)0.00;
        public decimal TotalHolidayEntitlement = (decimal)0.00;
        public decimal HolidaysRemaining = (decimal)0.00;
        public decimal BankHolidaysRemaining = (decimal)0.00;
        public decimal PurchasedHolidaysRemaining = (decimal)0.00;
        public decimal TotalHolidaysTaken = (decimal)0.00;
        public decimal TotalHolidaysRemaining = (decimal)0.00;
        public decimal HoursPerHolidayDay = (decimal)0.00;
        #endregion
    
        public frm_HR_Select_Employee_Holiday_Year()
        {
            InitializeComponent();
        }

        private void frm_HR_Select_Employee_Holiday_Year_Load(object sender, EventArgs e)
        {
            this.FormID = 990112;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            beiActiveHolidayYearOnly.EditValue = 1;
            sp_HR_00030_Select_Employee_Holiday_YearsTableAdapter.Connection.ConnectionString = strConnectionString;
            Load_Data();
            gridControl1.ForceInitialize();

            if (intOriginalSelectedValue != 0)  // Rate selected so try to find and highlight //
            {
                GridView view = (GridView)gridControl1.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["Id"], intOriginalSelectedValue);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
        }

        private void Load_Data()
        {
            try
            {
                sp_HR_00030_Select_Employee_Holiday_YearsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00030_Select_Employee_Holiday_Years, Convert.ToInt32(beiActiveHolidayYearOnly.EditValue));
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Employee Holiday Years list.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Holiday Years Available");
        }

        bool internalRowFocusing;

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                //case "HolidaysTaken":
                //case "BankHolidaysTaken":
                //case "AbsenceAuthorisedTaken":
                //case "AbsenceUnauthorisedTaken":
                //case "BasicHolidayEntitlement":
                //case "BankHolidayEntitlement":
                //case "PurchasedHolidayEntitlement":
                //case "TotalHolidayEntitlement":
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                //case "TotalHolidaysTaken":
                case "TotalHolidaysRemaining":
                    if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, e.Column.FieldName)) <= (decimal)0.00)
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "HolidaysTaken":
                case "BankHolidaysTaken":
                case "PurchasedHolidaysTaken":
                case "AbsenceAuthorisedTaken":
                case "AbsenceUnauthorisedTaken":
                case "BasicHolidayEntitlement":
                case "BankHolidayEntitlement":
                case "PurchasedHolidayEntitlement":
                case "TotalHolidayEntitlement":
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "PurchasedHolidaysRemaining":
                case "TotalHolidaysTaken":
                case "TotalHolidaysRemaining":
                    e.RepositoryItem = (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptorId")) == 0 ? repositoryItemTextEditDummyHours : repositoryItemTextEditDummyDays);
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedValue))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedID = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Id"));
                strSelectedValue = gridView1.GetFocusedRowCellValue("EmployeeSurnameForename").ToString();
                intEmployeeID = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "EmployeeID"));
                HolidayUnitDescriptor = gridView1.GetFocusedRowCellValue("HolidayUnitDescriptor").ToString();
                HolidayUnitDescriptorId = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HolidayUnitDescriptorId"));
                HolidaysTaken = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HolidaysTaken"));
                BankHolidaysTaken = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "BankHolidaysTaken"));
                PurchasedHolidaysTaken = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "PurchasedHolidaysTaken"));
                AbsenceAuthorisedTaken = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "AbsenceAuthorisedTaken"));
                AbsenceUnauthorisedTaken = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "AbsenceUnauthorisedTaken"));
                BasicHolidayEntitlement = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "BasicHolidayEntitlement"));
                BankHolidayEntitlement = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "BankHolidayEntitlement"));
                PurchasedHolidayEntitlement = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "PurchasedHolidayEntitlement"));
                TotalHolidayEntitlement = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TotalHolidayEntitlement"));
                HolidaysRemaining = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HolidaysRemaining"));
                BankHolidaysRemaining = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "BankHolidaysRemaining"));
                PurchasedHolidaysRemaining = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "PurchasedHolidaysRemaining"));
                TotalHolidaysTaken = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TotalHolidaysTaken"));
                TotalHolidaysRemaining = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "TotalHolidaysRemaining"));
                HoursPerHolidayDay = Convert.ToDecimal(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "HoursPerHolidayDay"));            
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

 
    }
}


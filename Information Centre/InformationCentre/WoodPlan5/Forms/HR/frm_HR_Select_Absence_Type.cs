using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_HR_Select_Absence_Type : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int intPassedInFilterID = 0;
        public int intPassedInTypeId = 0;
        public int intPassedInSubTypeId = 0;
        public int intSelectedTypeId = 0;
        public int intSelectedSubTypeId = 0;
        public string strSelectedTypeName = "";
        public string strSelectedSubTypeName = "";

        private ExtendedGridMenu egmMenu;  // Used to extend Grid Column Menu //
        GridHitInfo downHitInfo = null;

        #endregion

        public frm_HR_Select_Absence_Type()
        {
            InitializeComponent();
        }

        private void frm_HR_Select_Absence_Type_Load(object sender, EventArgs e)
        {
            this.FormID = 990110;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            sp_HR_00035_Get_Absence_Types_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00036_Get_Absence_Sub_Types_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            
            try
            {
                sp_HR_00035_Get_Absence_Types_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00035_Get_Absence_Types_List_With_Blank, intPassedInFilterID);
            }
            catch (Exception)
            {
                XtraMessageBox.Show("An error occurred while loading the absence type list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInTypeId != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ID"], intPassedInTypeId);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            if (intPassedInSubTypeId != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ID"], intPassedInSubTypeId);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

        }

        bool internalRowFocusing;


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Absence Types Available";
                    break;
                case "gridView2":
                    message = "No Absence Sub-Types Available For Selection - Select an Absence Type to see Related Sub-Types";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            var intSelectedID = 0;

            if (intCount == 1)
            {
                //foreach (int intRowHandle in intRowHandles)
                //{
                intSelectedID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], view.Columns["ID"]));
                //}
            }

            //Populate Linked sub types //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00036_Get_Absence_Sub_Types_List_With_Blank.Clear();
            }
            else
            {
                try
                {
                    sp_HR_00036_Get_Absence_Sub_Types_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00036_Get_Absence_Sub_Types_List_With_Blank, intSelectedID);
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related absence sub-types.\n\nTry selecting an absence type again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedTypeId == 0)
            {
                XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            intSelectedTypeId = 0;
            strSelectedTypeName = "";

            intSelectedSubTypeId = 0;
            strSelectedSubTypeName = "";

            var view1 = (GridView)gridControl1.MainView;
            if (view1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedTypeId = Convert.ToInt32(view1.GetRowCellValue(view1.FocusedRowHandle, "ID"));
                strSelectedTypeName = (String.IsNullOrEmpty(Convert.ToString(view1.GetRowCellValue(view1.FocusedRowHandle, "Description"))) ? "Unknown Absence Type" : Convert.ToString(view1.GetRowCellValue(view1.FocusedRowHandle, "Description")));
            }

            var view2 = (GridView)gridControl2.MainView;
            if (view2.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedSubTypeId = Convert.ToInt32(view2.GetRowCellValue(view2.FocusedRowHandle, "ID"));
                strSelectedSubTypeName = (String.IsNullOrEmpty(Convert.ToString(view2.GetRowCellValue(view2.FocusedRowHandle, "Description"))) ? "Unknown Absence Sub-Type" : Convert.ToString(view2.GetRowCellValue(view2.FocusedRowHandle, "Description")));
            }
        }



    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_HR_Select_Qualification_Type : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public bool boolShowSubTypes = true;

        public int intIncludeBlankSubType = 1;
        public int intPassedInQualificationTypeID = 0;
        public int intPassedInQualificationSubTypeID = 0;
        
        public int intSelectedQualificiationTypeID = 0;
        public int intSelectedQualificationSubTypeID = 0;

        public string strSelectedQualificationType = "";
        public string strSelectedQualificationSubType = "";

        GridHitInfo downHitInfo = null;

        #endregion

        public frm_HR_Select_Qualification_Type()
        {
            InitializeComponent();
        }

        private void frm_HR_Select_Qualification_Type_Load(object sender, EventArgs e)
        {
            this.FormID = 500123;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            sp_HR_00238_Qualification_Sub_Types_SelectTableAdapter.Connection.ConnectionString = strConnectionString;

            try
            {
                sp_HR_00237_Qualification_Types_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
                sp_HR_00237_Qualification_Types_SelectTableAdapter.Fill(dataSet_HR_Core.sp_HR_00237_Qualification_Types_Select);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Business Areas list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInQualificationTypeID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["QualificationTypeID"], intPassedInQualificationTypeID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            if (boolShowSubTypes)
            {
                gridControl2.ForceInitialize();
                view = (GridView)gridControl2.MainView;
                if (intPassedInQualificationSubTypeID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["QualificationSubTypeID"], intPassedInQualificationSubTypeID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            else
            {
                splitContainerControl1.Collapsed = true;
            }
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        bool internalRowFocusing;


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Qualification Types Available";
                    break;
                case "gridView2":
                    message = "No Qualification Sub-Types Available For Selection - Select a Type to see Related Sub-Types";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["QualificationTypeID"])) + ',';
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_HR_Core.sp_HR_00238_Qualification_Sub_Types_Select.Clear();
            }
            else
            {
                try
                {
                    sp_HR_00238_Qualification_Sub_Types_SelectTableAdapter.Fill(dataSet_HR_Core.sp_HR_00238_Qualification_Sub_Types_Select, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intIncludeBlankSubType);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Sub-Types.\n\nTry selecting a Type again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedQualificiationTypeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (boolShowSubTypes && intSelectedQualificationSubTypeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a Sub-Type record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedQualificiationTypeID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationTypeID"));
                strSelectedQualificationType = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description"))) ? "Unknown Qualification Type" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description")));
            }
            if (boolShowSubTypes)
            {
                view = (GridView)gridControl2.MainView;
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    intSelectedQualificationSubTypeID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationSubTypeID"));
                    strSelectedQualificationSubType = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description"))) ? "No Qualification Sub-Type" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description")));
                }
            }
        }



    }
}


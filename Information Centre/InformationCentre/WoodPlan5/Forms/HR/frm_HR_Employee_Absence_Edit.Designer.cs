namespace WoodPlan5
{
    partial class frm_HR_Employee_Absence_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Absence_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling18 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling19 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling20 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling21 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling22 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling23 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling24 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling25 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling26 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling27 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling28 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling29 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling30 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling31 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.SickPayPercentage2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.spHR00051GetEmployeeAbsenceItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.SickPayPercentage1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SickPayUnitDescriptor2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SickPayUnitDescriptor1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SickPayUnits2TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SickPayUnits1TextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AbsenceCommentMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.UnitsPaidOtherSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.UnitsPaidHalfPaySpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.UnitsPaidCSPSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EmployeeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AdditionalDaysPaidSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.HalfDayEndCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.HalfDayStartCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.EWCDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.TotalHolidaysRemainingTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalHolidaysTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BankHolidaysRemainingTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HolidaysRemainingTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TotalHolidayEntitlementTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HoursPerHolidayDayTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PurchasedHolidayEntitlementTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BankHolidayEntitlementTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CancelledByIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ApproverIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AbsenceSubTypeIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AbsenceTypeIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StatusIdGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00080AbsenceStatusListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PayCategoryIdGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00079AbsencePaymentTypesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HolidayUnitDescriptorTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RecordTypeIdGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00078AbsenceRecordTypesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.IdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.EmployeeHolidayYearIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HolidayYearDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.DeclaredDisabilityCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.StartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ExpectedReturnDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.UnitsUsedSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.IsWorkRelatedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ApprovedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.CancelledDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.CommentsMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ApprovedByPersonButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.CancelledByPersonButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AbsenceTypeDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AbsenceSubTypeDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HolidaysTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BankHolidaysTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AbsenceAuthorisedTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AbsenceUnauthorisedTakenTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BasicHolidayEntitlementTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.UnitsPaidUnpaidSpinEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeHolidayYearId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceTypeId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceSubTypeId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForApproverId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCancelledById = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAbsenceTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitsUsed = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIsWorkRelated = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpectedReturnDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPayCategoryId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDeclaredDisability = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHolidayUnitDescriptor = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEWCDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHalfDayStart = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHalfDayEnd = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAdditionalDaysPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitsPaidCSP = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitsPaidHalfPay = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitsPaidOther = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitsPaidUnpaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceComment = new DevExpress.XtraLayout.LayoutControlItem();
            this.layGrpStatus = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCancelledByPerson = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCancelledDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForApprovedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForApprovedByPerson = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStatusId = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForComments = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForBankHolidayEntitlement = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalHolidayEntitlement = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHoursPerHolidayDay = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.ItemForBasicHolidayEntitlement = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPurchasedHolidayEntitlement = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHolidaysTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBankHolidaysTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceAuthorisedTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceUnauthorisedTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalHolidaysTaken = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator2 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layGrpEntitlements = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForBankHolidaysRemaining = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHolidaysRemaining = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTotalHolidaysRemaining = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem11 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.simpleSeparator3 = new DevExpress.XtraLayout.SimpleSeparator();
            this.emptySpaceItem12 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSickPayUnits1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSickPayUnitDescriptor1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSickPayPercentage1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSickPayUnits2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSickPayUnitDescriptor2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSickPayPercentage2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem3 = new DevExpress.XtraLayout.SplitterItem();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForHolidayYearDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRecordId = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter();
            this.sp_HR_00078_Absence_Record_Types_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00078_Absence_Record_Types_List_With_BlankTableAdapter();
            this.sp_HR_00079_Absence_Payment_Types_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00079_Absence_Payment_Types_List_With_BlankTableAdapter();
            this.sp_HR_00080_Absence_Status_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00080_Absence_Status_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayPercentage2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00051GetEmployeeAbsenceItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayPercentage1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayUnitDescriptor2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayUnitDescriptor1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayUnits2TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayUnits1TextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceCommentMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsPaidOtherSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsPaidHalfPaySpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsPaidCSPSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdditionalDaysPaidSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HalfDayEndCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HalfDayStartCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EWCDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EWCDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidaysRemainingTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidaysTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidaysRemainingTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidaysRemainingTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidayEntitlementTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursPerHolidayDayTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchasedHolidayEntitlementTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidayEntitlementTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledByIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApproverIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceSubTypeIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceTypeIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIdGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00080AbsenceStatusListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayCategoryIdGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00079AbsencePaymentTypesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayUnitDescriptorTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordTypeIdGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00078AbsenceRecordTypesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeHolidayYearIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeclaredDisabilityCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedReturnDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedReturnDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsUsedSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsWorkRelatedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentsMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedByPersonButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledByPersonButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceTypeDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceSubTypeDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidaysTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidaysTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceAuthorisedTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceUnauthorisedTakenTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BasicHolidayEntitlementTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsPaidUnpaidSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeHolidayYearId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceTypeId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceSubTypeId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApproverId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledById)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsWorkRelated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedReturnDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPayCategoryId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeclaredDisability)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayUnitDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEWCDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHalfDayStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHalfDayEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdditionalDaysPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsPaidCSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsPaidHalfPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsPaidOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsPaidUnpaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledByPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApprovedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApprovedByPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidayEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidayEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursPerHolidayDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBasicHolidayEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchasedHolidayEntitlement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidaysTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidaysTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceAuthorisedTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceUnauthorisedTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidaysTaken)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpEntitlements)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidaysRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidaysRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidaysRemaining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayUnits1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayUnitDescriptor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayPercentage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayUnits2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayUnitDescriptor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayYearDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(875, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 466);
            this.barDockControlBottom.Size = new System.Drawing.Size(875, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 440);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(875, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 440);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.MaxItemId = 43;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ID";
            this.gridColumn2.FieldName = "ID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(1417, 164);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormCancel.ImageOptions.Image")));
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.ImageOptions.Image")));
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(875, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 466);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(875, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 440);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(875, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 440);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.SickPayPercentage2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SickPayPercentage1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SickPayUnitDescriptor2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SickPayUnitDescriptor1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SickPayUnits2TextEdit);
            this.dataLayoutControl1.Controls.Add(this.SickPayUnits1TextEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceCommentMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitsPaidOtherSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitsPaidHalfPaySpinEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitsPaidCSPSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AdditionalDaysPaidSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.HalfDayEndCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.HalfDayStartCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.EWCDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalHolidaysRemainingTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalHolidaysTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BankHolidaysRemainingTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidaysRemainingTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TotalHolidayEntitlementTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HoursPerHolidayDayTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PurchasedHolidayEntitlementTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BankHolidayEntitlementTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CancelledByIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ApproverIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceSubTypeIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceTypeIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIdGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.PayCategoryIdGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidayUnitDescriptorTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RecordTypeIdGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.IdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.EmployeeHolidayYearIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidayYearDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.DeclaredDisabilityCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpectedReturnDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitsUsedSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.IsWorkRelatedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ApprovedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.CancelledDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.CommentsMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.ApprovedByPersonButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.CancelledByPersonButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceTypeDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceSubTypeDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HolidaysTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BankHolidaysTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceAuthorisedTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AbsenceUnauthorisedTakenTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BasicHolidayEntitlementTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitsPaidUnpaidSpinEdit);
            this.dataLayoutControl1.DataSource = this.spHR00051GetEmployeeAbsenceItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForId,
            this.ItemForEmployeeHolidayYearId,
            this.ItemForAbsenceTypeId,
            this.ItemForAbsenceSubTypeId,
            this.ItemForApproverId,
            this.ItemForCancelledById,
            this.ItemForEmployeeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(15, 79, 371, 558);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(875, 440);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // SickPayPercentage2TextEdit
            // 
            this.SickPayPercentage2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "SickPayPercentage2", true));
            this.SickPayPercentage2TextEdit.Location = new System.Drawing.Point(564, 815);
            this.SickPayPercentage2TextEdit.MenuManager = this.barManager1;
            this.SickPayPercentage2TextEdit.Name = "SickPayPercentage2TextEdit";
            this.SickPayPercentage2TextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SickPayPercentage2TextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SickPayPercentage2TextEdit.Properties.Mask.EditMask = "P";
            this.SickPayPercentage2TextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SickPayPercentage2TextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SickPayPercentage2TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SickPayPercentage2TextEdit, true);
            this.SickPayPercentage2TextEdit.Size = new System.Drawing.Size(246, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SickPayPercentage2TextEdit, optionsSpelling1);
            this.SickPayPercentage2TextEdit.StyleController = this.dataLayoutControl1;
            this.SickPayPercentage2TextEdit.TabIndex = 66;
            // 
            // spHR00051GetEmployeeAbsenceItemBindingSource
            // 
            this.spHR00051GetEmployeeAbsenceItemBindingSource.DataMember = "sp_HR_00051_Get_Employee_Absence_Item";
            this.spHR00051GetEmployeeAbsenceItemBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SickPayPercentage1TextEdit
            // 
            this.SickPayPercentage1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "SickPayPercentage1", true));
            this.SickPayPercentage1TextEdit.Location = new System.Drawing.Point(164, 815);
            this.SickPayPercentage1TextEdit.MenuManager = this.barManager1;
            this.SickPayPercentage1TextEdit.Name = "SickPayPercentage1TextEdit";
            this.SickPayPercentage1TextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SickPayPercentage1TextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SickPayPercentage1TextEdit.Properties.Mask.EditMask = "P";
            this.SickPayPercentage1TextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SickPayPercentage1TextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SickPayPercentage1TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SickPayPercentage1TextEdit, true);
            this.SickPayPercentage1TextEdit.Size = new System.Drawing.Size(250, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SickPayPercentage1TextEdit, optionsSpelling2);
            this.SickPayPercentage1TextEdit.StyleController = this.dataLayoutControl1;
            this.SickPayPercentage1TextEdit.TabIndex = 65;
            // 
            // SickPayUnitDescriptor2TextEdit
            // 
            this.SickPayUnitDescriptor2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "SickPayUnitDescriptor2", true));
            this.SickPayUnitDescriptor2TextEdit.Location = new System.Drawing.Point(564, 791);
            this.SickPayUnitDescriptor2TextEdit.MenuManager = this.barManager1;
            this.SickPayUnitDescriptor2TextEdit.Name = "SickPayUnitDescriptor2TextEdit";
            this.SickPayUnitDescriptor2TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SickPayUnitDescriptor2TextEdit, true);
            this.SickPayUnitDescriptor2TextEdit.Size = new System.Drawing.Size(246, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SickPayUnitDescriptor2TextEdit, optionsSpelling3);
            this.SickPayUnitDescriptor2TextEdit.StyleController = this.dataLayoutControl1;
            this.SickPayUnitDescriptor2TextEdit.TabIndex = 67;
            // 
            // SickPayUnitDescriptor1TextEdit
            // 
            this.SickPayUnitDescriptor1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "SickPayUnitDescriptor1", true));
            this.SickPayUnitDescriptor1TextEdit.Location = new System.Drawing.Point(164, 791);
            this.SickPayUnitDescriptor1TextEdit.MenuManager = this.barManager1;
            this.SickPayUnitDescriptor1TextEdit.Name = "SickPayUnitDescriptor1TextEdit";
            this.SickPayUnitDescriptor1TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SickPayUnitDescriptor1TextEdit, true);
            this.SickPayUnitDescriptor1TextEdit.Size = new System.Drawing.Size(250, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SickPayUnitDescriptor1TextEdit, optionsSpelling4);
            this.SickPayUnitDescriptor1TextEdit.StyleController = this.dataLayoutControl1;
            this.SickPayUnitDescriptor1TextEdit.TabIndex = 66;
            // 
            // SickPayUnits2TextEdit
            // 
            this.SickPayUnits2TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "SickPayUnits2", true));
            this.SickPayUnits2TextEdit.Location = new System.Drawing.Point(564, 767);
            this.SickPayUnits2TextEdit.MenuManager = this.barManager1;
            this.SickPayUnits2TextEdit.Name = "SickPayUnits2TextEdit";
            this.SickPayUnits2TextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SickPayUnits2TextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SickPayUnits2TextEdit.Properties.Mask.EditMask = "f2";
            this.SickPayUnits2TextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SickPayUnits2TextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SickPayUnits2TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SickPayUnits2TextEdit, true);
            this.SickPayUnits2TextEdit.Size = new System.Drawing.Size(246, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SickPayUnits2TextEdit, optionsSpelling5);
            this.SickPayUnits2TextEdit.StyleController = this.dataLayoutControl1;
            this.SickPayUnits2TextEdit.TabIndex = 65;
            // 
            // SickPayUnits1TextEdit
            // 
            this.SickPayUnits1TextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "SickPayUnits1", true));
            this.SickPayUnits1TextEdit.Location = new System.Drawing.Point(164, 767);
            this.SickPayUnits1TextEdit.MenuManager = this.barManager1;
            this.SickPayUnits1TextEdit.Name = "SickPayUnits1TextEdit";
            this.SickPayUnits1TextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.SickPayUnits1TextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.SickPayUnits1TextEdit.Properties.Mask.EditMask = "f2";
            this.SickPayUnits1TextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SickPayUnits1TextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SickPayUnits1TextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SickPayUnits1TextEdit, true);
            this.SickPayUnits1TextEdit.Size = new System.Drawing.Size(250, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SickPayUnits1TextEdit, optionsSpelling6);
            this.SickPayUnits1TextEdit.StyleController = this.dataLayoutControl1;
            this.SickPayUnits1TextEdit.TabIndex = 64;
            // 
            // AbsenceCommentMemoEdit
            // 
            this.AbsenceCommentMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "AbsenceComment", true));
            this.AbsenceCommentMemoEdit.Location = new System.Drawing.Point(547, 308);
            this.AbsenceCommentMemoEdit.MenuManager = this.barManager1;
            this.AbsenceCommentMemoEdit.Name = "AbsenceCommentMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceCommentMemoEdit, true);
            this.AbsenceCommentMemoEdit.Size = new System.Drawing.Size(275, 92);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceCommentMemoEdit, optionsSpelling7);
            this.AbsenceCommentMemoEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceCommentMemoEdit.TabIndex = 63;
            // 
            // UnitsPaidOtherSpinEdit
            // 
            this.UnitsPaidOtherSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "UnitsPaidOther", true));
            this.UnitsPaidOtherSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitsPaidOtherSpinEdit.Location = new System.Drawing.Point(152, 356);
            this.UnitsPaidOtherSpinEdit.MenuManager = this.barManager1;
            this.UnitsPaidOtherSpinEdit.Name = "UnitsPaidOtherSpinEdit";
            this.UnitsPaidOtherSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnitsPaidOtherSpinEdit.Properties.Mask.EditMask = "n2";
            this.UnitsPaidOtherSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitsPaidOtherSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.UnitsPaidOtherSpinEdit.Size = new System.Drawing.Size(275, 20);
            this.UnitsPaidOtherSpinEdit.StyleController = this.dataLayoutControl1;
            this.UnitsPaidOtherSpinEdit.TabIndex = 30;
            this.UnitsPaidOtherSpinEdit.EditValueChanged += new System.EventHandler(this.UnitsPaidOtherSpinEdit_EditValueChanged);
            this.UnitsPaidOtherSpinEdit.Validated += new System.EventHandler(this.UnitsPaidOtherSpinEdit_Validated);
            // 
            // UnitsPaidHalfPaySpinEdit
            // 
            this.UnitsPaidHalfPaySpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "UnitsPaidHalfPay", true));
            this.UnitsPaidHalfPaySpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitsPaidHalfPaySpinEdit.Location = new System.Drawing.Point(152, 332);
            this.UnitsPaidHalfPaySpinEdit.MenuManager = this.barManager1;
            this.UnitsPaidHalfPaySpinEdit.Name = "UnitsPaidHalfPaySpinEdit";
            this.UnitsPaidHalfPaySpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnitsPaidHalfPaySpinEdit.Properties.Mask.EditMask = "n2";
            this.UnitsPaidHalfPaySpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitsPaidHalfPaySpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.UnitsPaidHalfPaySpinEdit.Size = new System.Drawing.Size(275, 20);
            this.UnitsPaidHalfPaySpinEdit.StyleController = this.dataLayoutControl1;
            this.UnitsPaidHalfPaySpinEdit.TabIndex = 30;
            this.UnitsPaidHalfPaySpinEdit.EditValueChanged += new System.EventHandler(this.UnitsPaidHalfPaySpinEdit_EditValueChanged);
            this.UnitsPaidHalfPaySpinEdit.Validated += new System.EventHandler(this.UnitsPaidHalfPaySpinEdit_Validated);
            // 
            // UnitsPaidCSPSpinEdit
            // 
            this.UnitsPaidCSPSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "UnitsPaidCSP", true));
            this.UnitsPaidCSPSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitsPaidCSPSpinEdit.Location = new System.Drawing.Point(152, 308);
            this.UnitsPaidCSPSpinEdit.MenuManager = this.barManager1;
            this.UnitsPaidCSPSpinEdit.Name = "UnitsPaidCSPSpinEdit";
            this.UnitsPaidCSPSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnitsPaidCSPSpinEdit.Properties.Mask.EditMask = "n2";
            this.UnitsPaidCSPSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitsPaidCSPSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.UnitsPaidCSPSpinEdit.Size = new System.Drawing.Size(275, 20);
            this.UnitsPaidCSPSpinEdit.StyleController = this.dataLayoutControl1;
            this.UnitsPaidCSPSpinEdit.TabIndex = 30;
            this.UnitsPaidCSPSpinEdit.EditValueChanged += new System.EventHandler(this.UnitsPaidCSPSpinEdit_EditValueChanged);
            this.UnitsPaidCSPSpinEdit.Validated += new System.EventHandler(this.UnitsPaidCSPSpinEdit_Validated);
            // 
            // EmployeeIDTextEdit
            // 
            this.EmployeeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "EmployeeID", true));
            this.EmployeeIDTextEdit.Location = new System.Drawing.Point(128, 83);
            this.EmployeeIDTextEdit.MenuManager = this.barManager1;
            this.EmployeeIDTextEdit.Name = "EmployeeIDTextEdit";
            this.EmployeeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIDTextEdit, true);
            this.EmployeeIDTextEdit.Size = new System.Drawing.Size(718, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIDTextEdit, optionsSpelling8);
            this.EmployeeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIDTextEdit.TabIndex = 62;
            // 
            // AdditionalDaysPaidSpinEdit
            // 
            this.AdditionalDaysPaidSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "AdditionalDaysPaid", true));
            this.AdditionalDaysPaidSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.AdditionalDaysPaidSpinEdit.Location = new System.Drawing.Point(547, 260);
            this.AdditionalDaysPaidSpinEdit.MenuManager = this.barManager1;
            this.AdditionalDaysPaidSpinEdit.Name = "AdditionalDaysPaidSpinEdit";
            this.AdditionalDaysPaidSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AdditionalDaysPaidSpinEdit.Properties.Mask.EditMask = "####0.00";
            this.AdditionalDaysPaidSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AdditionalDaysPaidSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999999,
            0,
            0,
            131072});
            this.AdditionalDaysPaidSpinEdit.Size = new System.Drawing.Size(275, 20);
            this.AdditionalDaysPaidSpinEdit.StyleController = this.dataLayoutControl1;
            this.AdditionalDaysPaidSpinEdit.TabIndex = 61;
            // 
            // HalfDayEndCheckEdit
            // 
            this.HalfDayEndCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "HalfDayEnd", true));
            this.HalfDayEndCheckEdit.Location = new System.Drawing.Point(547, 189);
            this.HalfDayEndCheckEdit.MenuManager = this.barManager1;
            this.HalfDayEndCheckEdit.Name = "HalfDayEndCheckEdit";
            this.HalfDayEndCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.HalfDayEndCheckEdit.Properties.ValueChecked = 1;
            this.HalfDayEndCheckEdit.Properties.ValueUnchecked = 0;
            this.HalfDayEndCheckEdit.Size = new System.Drawing.Size(275, 19);
            this.HalfDayEndCheckEdit.StyleController = this.dataLayoutControl1;
            this.HalfDayEndCheckEdit.TabIndex = 60;
            this.HalfDayEndCheckEdit.EditValueChanged += new System.EventHandler(this.HalfDayEndCheckEdit_EditValueChanged);
            this.HalfDayEndCheckEdit.Validated += new System.EventHandler(this.HalfDayEndCheckEdit_Validated);
            // 
            // HalfDayStartCheckEdit
            // 
            this.HalfDayStartCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "HalfDayStart", true));
            this.HalfDayStartCheckEdit.Location = new System.Drawing.Point(152, 189);
            this.HalfDayStartCheckEdit.MenuManager = this.barManager1;
            this.HalfDayStartCheckEdit.Name = "HalfDayStartCheckEdit";
            this.HalfDayStartCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.HalfDayStartCheckEdit.Properties.ValueChecked = 1;
            this.HalfDayStartCheckEdit.Properties.ValueUnchecked = 0;
            this.HalfDayStartCheckEdit.Size = new System.Drawing.Size(275, 19);
            this.HalfDayStartCheckEdit.StyleController = this.dataLayoutControl1;
            this.HalfDayStartCheckEdit.TabIndex = 59;
            this.HalfDayStartCheckEdit.EditValueChanged += new System.EventHandler(this.HalfDayStartCheckEdit_EditValueChanged);
            this.HalfDayStartCheckEdit.Validated += new System.EventHandler(this.HalfDayStartCheckEdit_Validated);
            // 
            // EWCDateDateEdit
            // 
            this.EWCDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "EWCDate", true));
            this.EWCDateDateEdit.EditValue = null;
            this.EWCDateDateEdit.Location = new System.Drawing.Point(152, 236);
            this.EWCDateDateEdit.MenuManager = this.barManager1;
            this.EWCDateDateEdit.Name = "EWCDateDateEdit";
            this.EWCDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EWCDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EWCDateDateEdit.Properties.Mask.EditMask = "g";
            this.EWCDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EWCDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EWCDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EWCDateDateEdit.Size = new System.Drawing.Size(275, 20);
            this.EWCDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EWCDateDateEdit.TabIndex = 29;
            this.EWCDateDateEdit.EditValueChanged += new System.EventHandler(this.EWCDateDateEdit_EditValueChanged);
            this.EWCDateDateEdit.Validated += new System.EventHandler(this.EWCDateDateEdit_Validated);
            // 
            // TotalHolidaysRemainingTextEdit
            // 
            this.TotalHolidaysRemainingTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "TotalHolidaysRemaining", true));
            this.TotalHolidaysRemainingTextEdit.Location = new System.Drawing.Point(687, 587);
            this.TotalHolidaysRemainingTextEdit.MenuManager = this.barManager1;
            this.TotalHolidaysRemainingTextEdit.Name = "TotalHolidaysRemainingTextEdit";
            this.TotalHolidaysRemainingTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalHolidaysRemainingTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalHolidaysRemainingTextEdit.Properties.Mask.EditMask = "###0.00";
            this.TotalHolidaysRemainingTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalHolidaysRemainingTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalHolidaysRemainingTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalHolidaysRemainingTextEdit, true);
            this.TotalHolidaysRemainingTextEdit.Size = new System.Drawing.Size(127, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalHolidaysRemainingTextEdit, optionsSpelling9);
            this.TotalHolidaysRemainingTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalHolidaysRemainingTextEdit.TabIndex = 58;
            // 
            // TotalHolidaysTakenTextEdit
            // 
            this.TotalHolidaysTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "TotalHolidaysTaken", true));
            this.TotalHolidaysTakenTextEdit.Location = new System.Drawing.Point(430, 587);
            this.TotalHolidaysTakenTextEdit.MenuManager = this.barManager1;
            this.TotalHolidaysTakenTextEdit.Name = "TotalHolidaysTakenTextEdit";
            this.TotalHolidaysTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalHolidaysTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalHolidaysTakenTextEdit.Properties.Mask.EditMask = "###0.00";
            this.TotalHolidaysTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalHolidaysTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalHolidaysTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalHolidaysTakenTextEdit, true);
            this.TotalHolidaysTakenTextEdit.Size = new System.Drawing.Size(115, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalHolidaysTakenTextEdit, optionsSpelling10);
            this.TotalHolidaysTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalHolidaysTakenTextEdit.TabIndex = 57;
            // 
            // BankHolidaysRemainingTextEdit
            // 
            this.BankHolidaysRemainingTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "BankHolidaysRemaining", true));
            this.BankHolidaysRemainingTextEdit.Location = new System.Drawing.Point(687, 561);
            this.BankHolidaysRemainingTextEdit.MenuManager = this.barManager1;
            this.BankHolidaysRemainingTextEdit.Name = "BankHolidaysRemainingTextEdit";
            this.BankHolidaysRemainingTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.BankHolidaysRemainingTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BankHolidaysRemainingTextEdit.Properties.Mask.EditMask = "###0.00";
            this.BankHolidaysRemainingTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.BankHolidaysRemainingTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BankHolidaysRemainingTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BankHolidaysRemainingTextEdit, true);
            this.BankHolidaysRemainingTextEdit.Size = new System.Drawing.Size(127, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BankHolidaysRemainingTextEdit, optionsSpelling11);
            this.BankHolidaysRemainingTextEdit.StyleController = this.dataLayoutControl1;
            this.BankHolidaysRemainingTextEdit.TabIndex = 56;
            // 
            // HolidaysRemainingTextEdit
            // 
            this.HolidaysRemainingTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "HolidaysRemaining", true));
            this.HolidaysRemainingTextEdit.Location = new System.Drawing.Point(687, 537);
            this.HolidaysRemainingTextEdit.MenuManager = this.barManager1;
            this.HolidaysRemainingTextEdit.Name = "HolidaysRemainingTextEdit";
            this.HolidaysRemainingTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.HolidaysRemainingTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.HolidaysRemainingTextEdit.Properties.Mask.EditMask = "###0.00";
            this.HolidaysRemainingTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.HolidaysRemainingTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HolidaysRemainingTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HolidaysRemainingTextEdit, true);
            this.HolidaysRemainingTextEdit.Size = new System.Drawing.Size(127, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HolidaysRemainingTextEdit, optionsSpelling12);
            this.HolidaysRemainingTextEdit.StyleController = this.dataLayoutControl1;
            this.HolidaysRemainingTextEdit.TabIndex = 55;
            // 
            // TotalHolidayEntitlementTextEdit
            // 
            this.TotalHolidayEntitlementTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "TotalHolidayEntitlement", true));
            this.TotalHolidayEntitlementTextEdit.Location = new System.Drawing.Point(160, 587);
            this.TotalHolidayEntitlementTextEdit.MenuManager = this.barManager1;
            this.TotalHolidayEntitlementTextEdit.Name = "TotalHolidayEntitlementTextEdit";
            this.TotalHolidayEntitlementTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TotalHolidayEntitlementTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TotalHolidayEntitlementTextEdit.Properties.Mask.EditMask = "####0.00";
            this.TotalHolidayEntitlementTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TotalHolidayEntitlementTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TotalHolidayEntitlementTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.TotalHolidayEntitlementTextEdit, true);
            this.TotalHolidayEntitlementTextEdit.Size = new System.Drawing.Size(128, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.TotalHolidayEntitlementTextEdit, optionsSpelling13);
            this.TotalHolidayEntitlementTextEdit.StyleController = this.dataLayoutControl1;
            this.TotalHolidayEntitlementTextEdit.TabIndex = 54;
            // 
            // HoursPerHolidayDayTextEdit
            // 
            this.HoursPerHolidayDayTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "HoursPerHolidayDay", true));
            this.HoursPerHolidayDayTextEdit.Location = new System.Drawing.Point(160, 635);
            this.HoursPerHolidayDayTextEdit.MenuManager = this.barManager1;
            this.HoursPerHolidayDayTextEdit.Name = "HoursPerHolidayDayTextEdit";
            this.HoursPerHolidayDayTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.HoursPerHolidayDayTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.HoursPerHolidayDayTextEdit.Properties.Mask.EditMask = "###0.00";
            this.HoursPerHolidayDayTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.HoursPerHolidayDayTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HoursPerHolidayDayTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HoursPerHolidayDayTextEdit, true);
            this.HoursPerHolidayDayTextEdit.Size = new System.Drawing.Size(128, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HoursPerHolidayDayTextEdit, optionsSpelling14);
            this.HoursPerHolidayDayTextEdit.StyleController = this.dataLayoutControl1;
            this.HoursPerHolidayDayTextEdit.TabIndex = 53;
            // 
            // PurchasedHolidayEntitlementTextEdit
            // 
            this.PurchasedHolidayEntitlementTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "PurchasedHolidayEntitlement", true));
            this.PurchasedHolidayEntitlementTextEdit.Location = new System.Drawing.Point(160, 513);
            this.PurchasedHolidayEntitlementTextEdit.MenuManager = this.barManager1;
            this.PurchasedHolidayEntitlementTextEdit.Name = "PurchasedHolidayEntitlementTextEdit";
            this.PurchasedHolidayEntitlementTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PurchasedHolidayEntitlementTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PurchasedHolidayEntitlementTextEdit.Properties.Mask.EditMask = "####0.00";
            this.PurchasedHolidayEntitlementTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.PurchasedHolidayEntitlementTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PurchasedHolidayEntitlementTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.PurchasedHolidayEntitlementTextEdit, true);
            this.PurchasedHolidayEntitlementTextEdit.Size = new System.Drawing.Size(128, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.PurchasedHolidayEntitlementTextEdit, optionsSpelling15);
            this.PurchasedHolidayEntitlementTextEdit.StyleController = this.dataLayoutControl1;
            this.PurchasedHolidayEntitlementTextEdit.TabIndex = 52;
            // 
            // BankHolidayEntitlementTextEdit
            // 
            this.BankHolidayEntitlementTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "BankHolidayEntitlement", true));
            this.BankHolidayEntitlementTextEdit.Location = new System.Drawing.Point(160, 561);
            this.BankHolidayEntitlementTextEdit.MenuManager = this.barManager1;
            this.BankHolidayEntitlementTextEdit.Name = "BankHolidayEntitlementTextEdit";
            this.BankHolidayEntitlementTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.BankHolidayEntitlementTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BankHolidayEntitlementTextEdit.Properties.Mask.EditMask = "####0.00";
            this.BankHolidayEntitlementTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.BankHolidayEntitlementTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BankHolidayEntitlementTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BankHolidayEntitlementTextEdit, true);
            this.BankHolidayEntitlementTextEdit.Size = new System.Drawing.Size(128, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BankHolidayEntitlementTextEdit, optionsSpelling16);
            this.BankHolidayEntitlementTextEdit.StyleController = this.dataLayoutControl1;
            this.BankHolidayEntitlementTextEdit.TabIndex = 51;
            // 
            // CancelledByIdTextEdit
            // 
            this.CancelledByIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "CancelledById", true));
            this.CancelledByIdTextEdit.Location = new System.Drawing.Point(142, 338);
            this.CancelledByIdTextEdit.MenuManager = this.barManager1;
            this.CancelledByIdTextEdit.Name = "CancelledByIdTextEdit";
            this.CancelledByIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CancelledByIdTextEdit, true);
            this.CancelledByIdTextEdit.Size = new System.Drawing.Size(657, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CancelledByIdTextEdit, optionsSpelling17);
            this.CancelledByIdTextEdit.StyleController = this.dataLayoutControl1;
            this.CancelledByIdTextEdit.TabIndex = 48;
            // 
            // ApproverIdTextEdit
            // 
            this.ApproverIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "ApproverId", true));
            this.ApproverIdTextEdit.Location = new System.Drawing.Point(130, 68);
            this.ApproverIdTextEdit.MenuManager = this.barManager1;
            this.ApproverIdTextEdit.Name = "ApproverIdTextEdit";
            this.ApproverIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ApproverIdTextEdit, true);
            this.ApproverIdTextEdit.Size = new System.Drawing.Size(681, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ApproverIdTextEdit, optionsSpelling18);
            this.ApproverIdTextEdit.StyleController = this.dataLayoutControl1;
            this.ApproverIdTextEdit.TabIndex = 47;
            // 
            // AbsenceSubTypeIdTextEdit
            // 
            this.AbsenceSubTypeIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "AbsenceSubTypeId", true));
            this.AbsenceSubTypeIdTextEdit.Location = new System.Drawing.Point(130, 68);
            this.AbsenceSubTypeIdTextEdit.MenuManager = this.barManager1;
            this.AbsenceSubTypeIdTextEdit.Name = "AbsenceSubTypeIdTextEdit";
            this.AbsenceSubTypeIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceSubTypeIdTextEdit, true);
            this.AbsenceSubTypeIdTextEdit.Size = new System.Drawing.Size(681, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceSubTypeIdTextEdit, optionsSpelling19);
            this.AbsenceSubTypeIdTextEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceSubTypeIdTextEdit.TabIndex = 45;
            // 
            // AbsenceTypeIdTextEdit
            // 
            this.AbsenceTypeIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "AbsenceTypeId", true));
            this.AbsenceTypeIdTextEdit.Location = new System.Drawing.Point(130, 83);
            this.AbsenceTypeIdTextEdit.MenuManager = this.barManager1;
            this.AbsenceTypeIdTextEdit.Name = "AbsenceTypeIdTextEdit";
            this.AbsenceTypeIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceTypeIdTextEdit, true);
            this.AbsenceTypeIdTextEdit.Size = new System.Drawing.Size(681, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceTypeIdTextEdit, optionsSpelling20);
            this.AbsenceTypeIdTextEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceTypeIdTextEdit.TabIndex = 44;
            // 
            // StatusIdGridLookUpEdit
            // 
            this.StatusIdGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "StatusId", true));
            this.StatusIdGridLookUpEdit.Location = new System.Drawing.Point(152, 165);
            this.StatusIdGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIdGridLookUpEdit.Name = "StatusIdGridLookUpEdit";
            this.StatusIdGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StatusIdGridLookUpEdit.Properties.DataSource = this.spHR00080AbsenceStatusListWithBlankBindingSource;
            this.StatusIdGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIdGridLookUpEdit.Properties.NullText = "";
            this.StatusIdGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.StatusIdGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIdGridLookUpEdit.Size = new System.Drawing.Size(670, 20);
            this.StatusIdGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIdGridLookUpEdit.TabIndex = 50;
            this.StatusIdGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StatusIdGridLookUpEdit_Validating);
            // 
            // spHR00080AbsenceStatusListWithBlankBindingSource
            // 
            this.spHR00080AbsenceStatusListWithBlankBindingSource.DataMember = "sp_HR_00080_Absence_Status_List_With_Blank";
            this.spHR00080AbsenceStatusListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn4;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Absence Status";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // PayCategoryIdGridLookUpEdit
            // 
            this.PayCategoryIdGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "PayCategoryId", true));
            this.PayCategoryIdGridLookUpEdit.Location = new System.Drawing.Point(152, 260);
            this.PayCategoryIdGridLookUpEdit.MenuManager = this.barManager1;
            this.PayCategoryIdGridLookUpEdit.Name = "PayCategoryIdGridLookUpEdit";
            this.PayCategoryIdGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PayCategoryIdGridLookUpEdit.Properties.DataSource = this.spHR00079AbsencePaymentTypesListWithBlankBindingSource;
            this.PayCategoryIdGridLookUpEdit.Properties.DisplayMember = "Description";
            this.PayCategoryIdGridLookUpEdit.Properties.NullText = "";
            this.PayCategoryIdGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.PayCategoryIdGridLookUpEdit.Properties.ValueMember = "ID";
            this.PayCategoryIdGridLookUpEdit.Size = new System.Drawing.Size(275, 20);
            this.PayCategoryIdGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.PayCategoryIdGridLookUpEdit.TabIndex = 49;
            this.PayCategoryIdGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.PayCategoryIdGridLookUpEdit_Validating);
            // 
            // spHR00079AbsencePaymentTypesListWithBlankBindingSource
            // 
            this.spHR00079AbsencePaymentTypesListWithBlankBindingSource.DataMember = "sp_HR_00079_Absence_Payment_Types_List_With_Blank";
            this.spHR00079AbsencePaymentTypesListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.colDescription,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn2;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Absence Payment Category";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // HolidayUnitDescriptorTextEdit
            // 
            this.HolidayUnitDescriptorTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "HolidayUnitDescriptor", true));
            this.HolidayUnitDescriptorTextEdit.Location = new System.Drawing.Point(547, 284);
            this.HolidayUnitDescriptorTextEdit.MenuManager = this.barManager1;
            this.HolidayUnitDescriptorTextEdit.Name = "HolidayUnitDescriptorTextEdit";
            this.HolidayUnitDescriptorTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HolidayUnitDescriptorTextEdit, true);
            this.HolidayUnitDescriptorTextEdit.Size = new System.Drawing.Size(275, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HolidayUnitDescriptorTextEdit, optionsSpelling21);
            this.HolidayUnitDescriptorTextEdit.StyleController = this.dataLayoutControl1;
            this.HolidayUnitDescriptorTextEdit.TabIndex = 46;
            // 
            // RecordTypeIdGridLookUpEdit
            // 
            this.RecordTypeIdGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "RecordTypeId", true));
            this.RecordTypeIdGridLookUpEdit.Location = new System.Drawing.Point(128, 59);
            this.RecordTypeIdGridLookUpEdit.MenuManager = this.barManager1;
            this.RecordTypeIdGridLookUpEdit.Name = "RecordTypeIdGridLookUpEdit";
            this.RecordTypeIdGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RecordTypeIdGridLookUpEdit.Properties.DataSource = this.spHR00078AbsenceRecordTypesListWithBlankBindingSource;
            this.RecordTypeIdGridLookUpEdit.Properties.DisplayMember = "Description";
            this.RecordTypeIdGridLookUpEdit.Properties.NullText = "";
            this.RecordTypeIdGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.RecordTypeIdGridLookUpEdit.Properties.ValueMember = "ID";
            this.RecordTypeIdGridLookUpEdit.Size = new System.Drawing.Size(718, 20);
            this.RecordTypeIdGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RecordTypeIdGridLookUpEdit.TabIndex = 43;
            this.RecordTypeIdGridLookUpEdit.EditValueChanged += new System.EventHandler(this.RecordTypeIdGridLookUpEdit_EditValueChanged);
            this.RecordTypeIdGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RecordTypeIdGridLookUpEdit_Validating);
            this.RecordTypeIdGridLookUpEdit.Validated += new System.EventHandler(this.RecordTypeIdGridLookUpEdit_Validated);
            // 
            // spHR00078AbsenceRecordTypesListWithBlankBindingSource
            // 
            this.spHR00078AbsenceRecordTypesListWithBlankBindingSource.DataMember = "sp_HR_00078_Absence_Record_Types_List_With_Blank";
            this.spHR00078AbsenceRecordTypesListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.gridColumn1,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colID1;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Absence Record Type";
            this.gridColumn1.FieldName = "Description";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // IdTextEdit
            // 
            this.IdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "Id", true));
            this.IdTextEdit.Location = new System.Drawing.Point(130, 71);
            this.IdTextEdit.MenuManager = this.barManager1;
            this.IdTextEdit.Name = "IdTextEdit";
            this.IdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.IdTextEdit, true);
            this.IdTextEdit.Size = new System.Drawing.Size(681, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.IdTextEdit, optionsSpelling22);
            this.IdTextEdit.StyleController = this.dataLayoutControl1;
            this.IdTextEdit.TabIndex = 19;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00051GetEmployeeAbsenceItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(128, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 17;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // EmployeeHolidayYearIdTextEdit
            // 
            this.EmployeeHolidayYearIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "EmployeeHolidayYearId", true));
            this.EmployeeHolidayYearIdTextEdit.Location = new System.Drawing.Point(130, 71);
            this.EmployeeHolidayYearIdTextEdit.MenuManager = this.barManager1;
            this.EmployeeHolidayYearIdTextEdit.Name = "EmployeeHolidayYearIdTextEdit";
            this.EmployeeHolidayYearIdTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.EmployeeHolidayYearIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeHolidayYearIdTextEdit, true);
            this.EmployeeHolidayYearIdTextEdit.Size = new System.Drawing.Size(681, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeHolidayYearIdTextEdit, optionsSpelling23);
            this.EmployeeHolidayYearIdTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeHolidayYearIdTextEdit.TabIndex = 4;
            this.EmployeeHolidayYearIdTextEdit.TabStop = false;
            // 
            // HolidayYearDescriptionButtonEdit
            // 
            this.HolidayYearDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "HolidayYearDescription", true));
            this.HolidayYearDescriptionButtonEdit.Location = new System.Drawing.Point(128, 35);
            this.HolidayYearDescriptionButtonEdit.MenuManager = this.barManager1;
            this.HolidayYearDescriptionButtonEdit.Name = "HolidayYearDescriptionButtonEdit";
            this.HolidayYearDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to select employee contract", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.HolidayYearDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.HolidayYearDescriptionButtonEdit.Size = new System.Drawing.Size(718, 20);
            this.HolidayYearDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.HolidayYearDescriptionButtonEdit.TabIndex = 20;
            this.HolidayYearDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HolidayYearDescriptionButtonEdit_ButtonClick);
            this.HolidayYearDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.HolidayYearDescriptionButtonEdit_Validating);
            // 
            // DeclaredDisabilityCheckEdit
            // 
            this.DeclaredDisabilityCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "DeclaredDisability", true));
            this.DeclaredDisabilityCheckEdit.Location = new System.Drawing.Point(547, 404);
            this.DeclaredDisabilityCheckEdit.MenuManager = this.barManager1;
            this.DeclaredDisabilityCheckEdit.Name = "DeclaredDisabilityCheckEdit";
            this.DeclaredDisabilityCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.DeclaredDisabilityCheckEdit.Properties.ValueChecked = 1;
            this.DeclaredDisabilityCheckEdit.Properties.ValueUnchecked = 0;
            this.DeclaredDisabilityCheckEdit.Size = new System.Drawing.Size(275, 19);
            this.DeclaredDisabilityCheckEdit.StyleController = this.dataLayoutControl1;
            this.DeclaredDisabilityCheckEdit.TabIndex = 25;
            // 
            // StartDateDateEdit
            // 
            this.StartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "StartDate", true));
            this.StartDateDateEdit.EditValue = null;
            this.StartDateDateEdit.Location = new System.Drawing.Point(152, 212);
            this.StartDateDateEdit.MenuManager = this.barManager1;
            this.StartDateDateEdit.Name = "StartDateDateEdit";
            this.StartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.Mask.EditMask = "g";
            this.StartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Size = new System.Drawing.Size(275, 20);
            this.StartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateDateEdit.TabIndex = 26;
            this.StartDateDateEdit.EditValueChanged += new System.EventHandler(this.StartDateDateEdit_EditValueChanged);
            this.StartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StartDateDateEdit_Validating);
            this.StartDateDateEdit.Validated += new System.EventHandler(this.StartDateDateEdit_Validated);
            // 
            // EndDateDateEdit
            // 
            this.EndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "EndDate", true));
            this.EndDateDateEdit.EditValue = null;
            this.EndDateDateEdit.Location = new System.Drawing.Point(547, 212);
            this.EndDateDateEdit.MenuManager = this.barManager1;
            this.EndDateDateEdit.Name = "EndDateDateEdit";
            this.EndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.Mask.EditMask = "g";
            this.EndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EndDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Size = new System.Drawing.Size(275, 20);
            this.EndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateDateEdit.TabIndex = 27;
            this.EndDateDateEdit.EditValueChanged += new System.EventHandler(this.EndDateDateEdit_EditValueChanged);
            this.EndDateDateEdit.Validated += new System.EventHandler(this.EndDateDateEdit_Validated);
            // 
            // ExpectedReturnDateDateEdit
            // 
            this.ExpectedReturnDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "ExpectedReturnDate", true));
            this.ExpectedReturnDateDateEdit.EditValue = null;
            this.ExpectedReturnDateDateEdit.Location = new System.Drawing.Point(547, 236);
            this.ExpectedReturnDateDateEdit.MenuManager = this.barManager1;
            this.ExpectedReturnDateDateEdit.Name = "ExpectedReturnDateDateEdit";
            this.ExpectedReturnDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedReturnDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpectedReturnDateDateEdit.Properties.Mask.EditMask = "g";
            this.ExpectedReturnDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ExpectedReturnDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ExpectedReturnDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ExpectedReturnDateDateEdit.Size = new System.Drawing.Size(275, 20);
            this.ExpectedReturnDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ExpectedReturnDateDateEdit.TabIndex = 28;
            // 
            // UnitsUsedSpinEdit
            // 
            this.UnitsUsedSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "UnitsUsed", true));
            this.UnitsUsedSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitsUsedSpinEdit.Location = new System.Drawing.Point(152, 284);
            this.UnitsUsedSpinEdit.MenuManager = this.barManager1;
            this.UnitsUsedSpinEdit.Name = "UnitsUsedSpinEdit";
            this.UnitsUsedSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnitsUsedSpinEdit.Properties.Mask.EditMask = "n2";
            this.UnitsUsedSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitsUsedSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999,
            0,
            0,
            131072});
            this.UnitsUsedSpinEdit.Size = new System.Drawing.Size(275, 20);
            this.UnitsUsedSpinEdit.StyleController = this.dataLayoutControl1;
            this.UnitsUsedSpinEdit.TabIndex = 29;
            this.UnitsUsedSpinEdit.EditValueChanged += new System.EventHandler(this.UnitsUsedSpinEdit_EditValueChanged);
            this.UnitsUsedSpinEdit.Validated += new System.EventHandler(this.UnitsUsedSpinEdit_Validated);
            // 
            // IsWorkRelatedCheckEdit
            // 
            this.IsWorkRelatedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "IsWorkRelated", true));
            this.IsWorkRelatedCheckEdit.Location = new System.Drawing.Point(152, 404);
            this.IsWorkRelatedCheckEdit.MenuManager = this.barManager1;
            this.IsWorkRelatedCheckEdit.Name = "IsWorkRelatedCheckEdit";
            this.IsWorkRelatedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.IsWorkRelatedCheckEdit.Properties.ValueChecked = 1;
            this.IsWorkRelatedCheckEdit.Properties.ValueUnchecked = 0;
            this.IsWorkRelatedCheckEdit.Size = new System.Drawing.Size(275, 19);
            this.IsWorkRelatedCheckEdit.StyleController = this.dataLayoutControl1;
            this.IsWorkRelatedCheckEdit.TabIndex = 30;
            // 
            // ApprovedDateDateEdit
            // 
            this.ApprovedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "ApprovedDate", true));
            this.ApprovedDateDateEdit.EditValue = null;
            this.ApprovedDateDateEdit.Location = new System.Drawing.Point(547, 189);
            this.ApprovedDateDateEdit.MenuManager = this.barManager1;
            this.ApprovedDateDateEdit.Name = "ApprovedDateDateEdit";
            this.ApprovedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ApprovedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ApprovedDateDateEdit.Properties.Mask.EditMask = "G";
            this.ApprovedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ApprovedDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ApprovedDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ApprovedDateDateEdit.Size = new System.Drawing.Size(275, 20);
            this.ApprovedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ApprovedDateDateEdit.TabIndex = 31;
            // 
            // CancelledDateDateEdit
            // 
            this.CancelledDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "CancelledDate", true));
            this.CancelledDateDateEdit.EditValue = null;
            this.CancelledDateDateEdit.Location = new System.Drawing.Point(547, 213);
            this.CancelledDateDateEdit.MenuManager = this.barManager1;
            this.CancelledDateDateEdit.Name = "CancelledDateDateEdit";
            this.CancelledDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CancelledDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CancelledDateDateEdit.Properties.Mask.EditMask = "G";
            this.CancelledDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CancelledDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.CancelledDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.CancelledDateDateEdit.Size = new System.Drawing.Size(275, 20);
            this.CancelledDateDateEdit.StyleController = this.dataLayoutControl1;
            this.CancelledDateDateEdit.TabIndex = 33;
            // 
            // CommentsMemoEdit
            // 
            this.CommentsMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "Comments", true));
            this.CommentsMemoEdit.Location = new System.Drawing.Point(36, 165);
            this.CommentsMemoEdit.MenuManager = this.barManager1;
            this.CommentsMemoEdit.Name = "CommentsMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.CommentsMemoEdit, true);
            this.CommentsMemoEdit.Size = new System.Drawing.Size(786, 258);
            this.scSpellChecker.SetSpellCheckerOptions(this.CommentsMemoEdit, optionsSpelling24);
            this.CommentsMemoEdit.StyleController = this.dataLayoutControl1;
            this.CommentsMemoEdit.TabIndex = 42;
            // 
            // ApprovedByPersonButtonEdit
            // 
            this.ApprovedByPersonButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "ApprovedByPerson", true));
            this.ApprovedByPersonButtonEdit.Location = new System.Drawing.Point(152, 189);
            this.ApprovedByPersonButtonEdit.MenuManager = this.barManager1;
            this.ApprovedByPersonButtonEdit.Name = "ApprovedByPersonButtonEdit";
            this.ApprovedByPersonButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click to select employee", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Clear Selected Value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ApprovedByPersonButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ApprovedByPersonButtonEdit.Size = new System.Drawing.Size(275, 20);
            this.ApprovedByPersonButtonEdit.StyleController = this.dataLayoutControl1;
            this.ApprovedByPersonButtonEdit.TabIndex = 32;
            this.ApprovedByPersonButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ApprovedByPersonButtonEdit_ButtonClick);
            // 
            // CancelledByPersonButtonEdit
            // 
            this.CancelledByPersonButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "CancelledByPerson", true));
            this.CancelledByPersonButtonEdit.Location = new System.Drawing.Point(152, 213);
            this.CancelledByPersonButtonEdit.MenuManager = this.barManager1;
            this.CancelledByPersonButtonEdit.Name = "CancelledByPersonButtonEdit";
            this.CancelledByPersonButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to select employee", "choose", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Clear Selected Value", "clear", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.CancelledByPersonButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CancelledByPersonButtonEdit.Size = new System.Drawing.Size(275, 20);
            this.CancelledByPersonButtonEdit.StyleController = this.dataLayoutControl1;
            this.CancelledByPersonButtonEdit.TabIndex = 34;
            this.CancelledByPersonButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.CancelledByPersonButtonEdit_ButtonClick);
            // 
            // AbsenceTypeDescriptionButtonEdit
            // 
            this.AbsenceTypeDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "AbsenceTypeDescription", true));
            this.AbsenceTypeDescriptionButtonEdit.Location = new System.Drawing.Point(152, 165);
            this.AbsenceTypeDescriptionButtonEdit.MenuManager = this.barManager1;
            this.AbsenceTypeDescriptionButtonEdit.Name = "AbsenceTypeDescriptionButtonEdit";
            this.AbsenceTypeDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.AbsenceTypeDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AbsenceTypeDescriptionButtonEdit.Size = new System.Drawing.Size(275, 20);
            this.AbsenceTypeDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceTypeDescriptionButtonEdit.TabIndex = 23;
            this.AbsenceTypeDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AbsenceTypeDescriptionButtonEdit_ButtonClick);
            this.AbsenceTypeDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AbsenceTypeDescriptionButtonEdit_Validating);
            // 
            // AbsenceSubTypeDescriptionTextEdit
            // 
            this.AbsenceSubTypeDescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "AbsenceSubTypeDescription", true));
            this.AbsenceSubTypeDescriptionTextEdit.Location = new System.Drawing.Point(547, 165);
            this.AbsenceSubTypeDescriptionTextEdit.MenuManager = this.barManager1;
            this.AbsenceSubTypeDescriptionTextEdit.Name = "AbsenceSubTypeDescriptionTextEdit";
            this.AbsenceSubTypeDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceSubTypeDescriptionTextEdit, true);
            this.AbsenceSubTypeDescriptionTextEdit.Size = new System.Drawing.Size(275, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceSubTypeDescriptionTextEdit, optionsSpelling25);
            this.AbsenceSubTypeDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem5.Text = "Absence Sub-Type - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Absence Sub-Type is read only. Value set from the Absence Type box (click Choose " +
    "button).";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.AbsenceSubTypeDescriptionTextEdit.SuperTip = superToolTip5;
            this.AbsenceSubTypeDescriptionTextEdit.TabIndex = 24;
            this.AbsenceSubTypeDescriptionTextEdit.TabStop = false;
            // 
            // HolidaysTakenTextEdit
            // 
            this.HolidaysTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "HolidaysTaken", true));
            this.HolidaysTakenTextEdit.Location = new System.Drawing.Point(430, 537);
            this.HolidaysTakenTextEdit.MenuManager = this.barManager1;
            this.HolidaysTakenTextEdit.Name = "HolidaysTakenTextEdit";
            this.HolidaysTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.HolidaysTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.HolidaysTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.HolidaysTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.HolidaysTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HolidaysTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HolidaysTakenTextEdit, true);
            this.HolidaysTakenTextEdit.Size = new System.Drawing.Size(115, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HolidaysTakenTextEdit, optionsSpelling26);
            this.HolidaysTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.HolidaysTakenTextEdit.TabIndex = 38;
            // 
            // BankHolidaysTakenTextEdit
            // 
            this.BankHolidaysTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "BankHolidaysTaken", true));
            this.BankHolidaysTakenTextEdit.Location = new System.Drawing.Point(430, 561);
            this.BankHolidaysTakenTextEdit.MenuManager = this.barManager1;
            this.BankHolidaysTakenTextEdit.Name = "BankHolidaysTakenTextEdit";
            this.BankHolidaysTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.BankHolidaysTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BankHolidaysTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.BankHolidaysTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.BankHolidaysTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BankHolidaysTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BankHolidaysTakenTextEdit, true);
            this.BankHolidaysTakenTextEdit.Size = new System.Drawing.Size(115, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BankHolidaysTakenTextEdit, optionsSpelling27);
            this.BankHolidaysTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.BankHolidaysTakenTextEdit.TabIndex = 37;
            // 
            // AbsenceAuthorisedTakenTextEdit
            // 
            this.AbsenceAuthorisedTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "AbsenceAuthorisedTaken", true));
            this.AbsenceAuthorisedTakenTextEdit.Location = new System.Drawing.Point(430, 611);
            this.AbsenceAuthorisedTakenTextEdit.MenuManager = this.barManager1;
            this.AbsenceAuthorisedTakenTextEdit.Name = "AbsenceAuthorisedTakenTextEdit";
            this.AbsenceAuthorisedTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AbsenceAuthorisedTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AbsenceAuthorisedTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.AbsenceAuthorisedTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AbsenceAuthorisedTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AbsenceAuthorisedTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceAuthorisedTakenTextEdit, true);
            this.AbsenceAuthorisedTakenTextEdit.Size = new System.Drawing.Size(115, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceAuthorisedTakenTextEdit, optionsSpelling28);
            this.AbsenceAuthorisedTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceAuthorisedTakenTextEdit.TabIndex = 36;
            // 
            // AbsenceUnauthorisedTakenTextEdit
            // 
            this.AbsenceUnauthorisedTakenTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "AbsenceUnauthorisedTaken", true));
            this.AbsenceUnauthorisedTakenTextEdit.Location = new System.Drawing.Point(430, 635);
            this.AbsenceUnauthorisedTakenTextEdit.MenuManager = this.barManager1;
            this.AbsenceUnauthorisedTakenTextEdit.Name = "AbsenceUnauthorisedTakenTextEdit";
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Mask.EditMask = "####0.00";
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.AbsenceUnauthorisedTakenTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AbsenceUnauthorisedTakenTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceUnauthorisedTakenTextEdit, true);
            this.AbsenceUnauthorisedTakenTextEdit.Size = new System.Drawing.Size(115, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceUnauthorisedTakenTextEdit, optionsSpelling29);
            this.AbsenceUnauthorisedTakenTextEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceUnauthorisedTakenTextEdit.TabIndex = 39;
            // 
            // BasicHolidayEntitlementTextEdit
            // 
            this.BasicHolidayEntitlementTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "BasicHolidayEntitlement", true));
            this.BasicHolidayEntitlementTextEdit.Location = new System.Drawing.Point(160, 537);
            this.BasicHolidayEntitlementTextEdit.MenuManager = this.barManager1;
            this.BasicHolidayEntitlementTextEdit.Name = "BasicHolidayEntitlementTextEdit";
            this.BasicHolidayEntitlementTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.BasicHolidayEntitlementTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.BasicHolidayEntitlementTextEdit.Properties.Mask.EditMask = "####0.00";
            this.BasicHolidayEntitlementTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.BasicHolidayEntitlementTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BasicHolidayEntitlementTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BasicHolidayEntitlementTextEdit, true);
            this.BasicHolidayEntitlementTextEdit.Size = new System.Drawing.Size(128, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BasicHolidayEntitlementTextEdit, optionsSpelling30);
            this.BasicHolidayEntitlementTextEdit.StyleController = this.dataLayoutControl1;
            this.BasicHolidayEntitlementTextEdit.TabIndex = 40;
            // 
            // UnitsPaidUnpaidSpinEdit
            // 
            this.UnitsPaidUnpaidSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00051GetEmployeeAbsenceItemBindingSource, "UnitsPaidUnpaid", true));
            this.UnitsPaidUnpaidSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitsPaidUnpaidSpinEdit.Location = new System.Drawing.Point(152, 380);
            this.UnitsPaidUnpaidSpinEdit.MenuManager = this.barManager1;
            this.UnitsPaidUnpaidSpinEdit.Name = "UnitsPaidUnpaidSpinEdit";
            this.UnitsPaidUnpaidSpinEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.UnitsPaidUnpaidSpinEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.UnitsPaidUnpaidSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.UnitsPaidUnpaidSpinEdit.Properties.Mask.EditMask = "n2";
            this.UnitsPaidUnpaidSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.UnitsPaidUnpaidSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitsPaidUnpaidSpinEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.UnitsPaidUnpaidSpinEdit, true);
            this.UnitsPaidUnpaidSpinEdit.Size = new System.Drawing.Size(275, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.UnitsPaidUnpaidSpinEdit, optionsSpelling31);
            this.UnitsPaidUnpaidSpinEdit.StyleController = this.dataLayoutControl1;
            this.UnitsPaidUnpaidSpinEdit.TabIndex = 30;
            // 
            // ItemForId
            // 
            this.ItemForId.Control = this.IdTextEdit;
            this.ItemForId.CustomizationFormText = "Absence ID:";
            this.ItemForId.Location = new System.Drawing.Point(0, 59);
            this.ItemForId.Name = "ItemForId";
            this.ItemForId.Size = new System.Drawing.Size(803, 24);
            this.ItemForId.Text = "Absence ID:";
            this.ItemForId.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForEmployeeHolidayYearId
            // 
            this.ItemForEmployeeHolidayYearId.Control = this.EmployeeHolidayYearIdTextEdit;
            this.ItemForEmployeeHolidayYearId.CustomizationFormText = "Holiday Year ID:";
            this.ItemForEmployeeHolidayYearId.Location = new System.Drawing.Point(0, 59);
            this.ItemForEmployeeHolidayYearId.Name = "ItemForEmployeeHolidayYearId";
            this.ItemForEmployeeHolidayYearId.Size = new System.Drawing.Size(803, 24);
            this.ItemForEmployeeHolidayYearId.Text = "Holiday Year ID:";
            this.ItemForEmployeeHolidayYearId.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForAbsenceTypeId
            // 
            this.ItemForAbsenceTypeId.Control = this.AbsenceTypeIdTextEdit;
            this.ItemForAbsenceTypeId.CustomizationFormText = "Absence Type ID:";
            this.ItemForAbsenceTypeId.Location = new System.Drawing.Point(0, 71);
            this.ItemForAbsenceTypeId.Name = "ItemForAbsenceTypeId";
            this.ItemForAbsenceTypeId.Size = new System.Drawing.Size(803, 24);
            this.ItemForAbsenceTypeId.Text = "Absence Type ID:";
            this.ItemForAbsenceTypeId.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAbsenceSubTypeId
            // 
            this.ItemForAbsenceSubTypeId.Control = this.AbsenceSubTypeIdTextEdit;
            this.ItemForAbsenceSubTypeId.CustomizationFormText = "Absence Sub-Type ID:";
            this.ItemForAbsenceSubTypeId.Location = new System.Drawing.Point(0, 83);
            this.ItemForAbsenceSubTypeId.Name = "ItemForAbsenceSubTypeId";
            this.ItemForAbsenceSubTypeId.Size = new System.Drawing.Size(803, 24);
            this.ItemForAbsenceSubTypeId.Text = "Absence Sub-Type ID:";
            this.ItemForAbsenceSubTypeId.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForApproverId
            // 
            this.ItemForApproverId.Control = this.ApproverIdTextEdit;
            this.ItemForApproverId.CustomizationFormText = "Approved By ID:";
            this.ItemForApproverId.Location = new System.Drawing.Point(0, 83);
            this.ItemForApproverId.Name = "ItemForApproverId";
            this.ItemForApproverId.Size = new System.Drawing.Size(803, 24);
            this.ItemForApproverId.Text = "Approved By ID:";
            this.ItemForApproverId.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForCancelledById
            // 
            this.ItemForCancelledById.Control = this.CancelledByIdTextEdit;
            this.ItemForCancelledById.CustomizationFormText = "Cancelled By ID:";
            this.ItemForCancelledById.Location = new System.Drawing.Point(0, 237);
            this.ItemForCancelledById.Name = "ItemForCancelledById";
            this.ItemForCancelledById.Size = new System.Drawing.Size(779, 24);
            this.ItemForCancelledById.Text = "Cancelled By ID:";
            this.ItemForCancelledById.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForEmployeeID
            // 
            this.ItemForEmployeeID.Control = this.EmployeeIDTextEdit;
            this.ItemForEmployeeID.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeID.Location = new System.Drawing.Point(0, 71);
            this.ItemForEmployeeID.Name = "ItemForEmployeeID";
            this.ItemForEmployeeID.Size = new System.Drawing.Size(838, 24);
            this.ItemForEmployeeID.Text = "Employee ID:";
            this.ItemForEmployeeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(858, 893);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.ItemForHolidayYearDescription,
            this.ItemForRecordId,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(838, 873);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutControlGroup8,
            this.emptySpaceItem8,
            this.layoutControlGroup9,
            this.emptySpaceItem13});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 83);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(838, 780);
            this.layoutControlGroup5.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(814, 310);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layGrpStatus,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details:";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAbsenceTypeDescription,
            this.ItemForStartDate,
            this.ItemForUnitsUsed,
            this.ItemForIsWorkRelated,
            this.ItemForAbsenceSubTypeDescription,
            this.ItemForExpectedReturnDate,
            this.ItemForEndDate,
            this.ItemForPayCategoryId,
            this.ItemForDeclaredDisability,
            this.ItemForHolidayUnitDescriptor,
            this.ItemForEWCDate,
            this.ItemForHalfDayStart,
            this.ItemForHalfDayEnd,
            this.ItemForAdditionalDaysPaid,
            this.ItemForUnitsPaidCSP,
            this.ItemForUnitsPaidHalfPay,
            this.ItemForUnitsPaidOther,
            this.ItemForUnitsPaidUnpaid,
            this.ItemForAbsenceComment});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(790, 262);
            this.layoutControlGroup4.Text = "Details";
            // 
            // ItemForAbsenceTypeDescription
            // 
            this.ItemForAbsenceTypeDescription.Control = this.AbsenceTypeDescriptionButtonEdit;
            this.ItemForAbsenceTypeDescription.CustomizationFormText = "Absence Type:";
            this.ItemForAbsenceTypeDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForAbsenceTypeDescription.Name = "ItemForAbsenceTypeDescription";
            this.ItemForAbsenceTypeDescription.Size = new System.Drawing.Size(395, 24);
            this.ItemForAbsenceTypeDescription.Text = "Absence Type:";
            this.ItemForAbsenceTypeDescription.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.StartDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(395, 24);
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForUnitsUsed
            // 
            this.ItemForUnitsUsed.Control = this.UnitsUsedSpinEdit;
            this.ItemForUnitsUsed.CustomizationFormText = "Units Used:";
            this.ItemForUnitsUsed.Location = new System.Drawing.Point(0, 119);
            this.ItemForUnitsUsed.Name = "ItemForUnitsUsed";
            this.ItemForUnitsUsed.Size = new System.Drawing.Size(395, 24);
            this.ItemForUnitsUsed.Text = "Units Used:";
            this.ItemForUnitsUsed.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForIsWorkRelated
            // 
            this.ItemForIsWorkRelated.Control = this.IsWorkRelatedCheckEdit;
            this.ItemForIsWorkRelated.CustomizationFormText = "Is Work Related:";
            this.ItemForIsWorkRelated.Location = new System.Drawing.Point(0, 239);
            this.ItemForIsWorkRelated.Name = "ItemForIsWorkRelated";
            this.ItemForIsWorkRelated.Size = new System.Drawing.Size(395, 23);
            this.ItemForIsWorkRelated.Text = "Is Work Related:";
            this.ItemForIsWorkRelated.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForAbsenceSubTypeDescription
            // 
            this.ItemForAbsenceSubTypeDescription.Control = this.AbsenceSubTypeDescriptionTextEdit;
            this.ItemForAbsenceSubTypeDescription.CustomizationFormText = "Absence Sub-Type:";
            this.ItemForAbsenceSubTypeDescription.Location = new System.Drawing.Point(395, 0);
            this.ItemForAbsenceSubTypeDescription.Name = "ItemForAbsenceSubTypeDescription";
            this.ItemForAbsenceSubTypeDescription.Size = new System.Drawing.Size(395, 24);
            this.ItemForAbsenceSubTypeDescription.Text = "Absence Sub-Type:";
            this.ItemForAbsenceSubTypeDescription.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForExpectedReturnDate
            // 
            this.ItemForExpectedReturnDate.Control = this.ExpectedReturnDateDateEdit;
            this.ItemForExpectedReturnDate.CustomizationFormText = "Expected Return Date:";
            this.ItemForExpectedReturnDate.Location = new System.Drawing.Point(395, 71);
            this.ItemForExpectedReturnDate.Name = "ItemForExpectedReturnDate";
            this.ItemForExpectedReturnDate.Size = new System.Drawing.Size(395, 24);
            this.ItemForExpectedReturnDate.Text = "Expected Return Date:";
            this.ItemForExpectedReturnDate.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.EndDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(395, 47);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(395, 24);
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForPayCategoryId
            // 
            this.ItemForPayCategoryId.Control = this.PayCategoryIdGridLookUpEdit;
            this.ItemForPayCategoryId.CustomizationFormText = "Payment Category:";
            this.ItemForPayCategoryId.Location = new System.Drawing.Point(0, 95);
            this.ItemForPayCategoryId.Name = "ItemForPayCategoryId";
            this.ItemForPayCategoryId.Size = new System.Drawing.Size(395, 24);
            this.ItemForPayCategoryId.Text = "Payment Category:";
            this.ItemForPayCategoryId.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForDeclaredDisability
            // 
            this.ItemForDeclaredDisability.Control = this.DeclaredDisabilityCheckEdit;
            this.ItemForDeclaredDisability.CustomizationFormText = "Declared Disability:";
            this.ItemForDeclaredDisability.Location = new System.Drawing.Point(395, 239);
            this.ItemForDeclaredDisability.Name = "ItemForDeclaredDisability";
            this.ItemForDeclaredDisability.Size = new System.Drawing.Size(395, 23);
            this.ItemForDeclaredDisability.Text = "Declared Disability:";
            this.ItemForDeclaredDisability.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForHolidayUnitDescriptor
            // 
            this.ItemForHolidayUnitDescriptor.Control = this.HolidayUnitDescriptorTextEdit;
            this.ItemForHolidayUnitDescriptor.CustomizationFormText = "Unit Descriptor:";
            this.ItemForHolidayUnitDescriptor.Location = new System.Drawing.Point(395, 119);
            this.ItemForHolidayUnitDescriptor.Name = "ItemForHolidayUnitDescriptor";
            this.ItemForHolidayUnitDescriptor.Size = new System.Drawing.Size(395, 24);
            this.ItemForHolidayUnitDescriptor.Text = "Unit Descriptor:";
            this.ItemForHolidayUnitDescriptor.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForEWCDate
            // 
            this.ItemForEWCDate.Control = this.EWCDateDateEdit;
            this.ItemForEWCDate.CustomizationFormText = "EWC Date:";
            this.ItemForEWCDate.Location = new System.Drawing.Point(0, 71);
            this.ItemForEWCDate.Name = "ItemForEWCDate";
            this.ItemForEWCDate.Size = new System.Drawing.Size(395, 24);
            this.ItemForEWCDate.Text = "EWC Date:";
            this.ItemForEWCDate.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForHalfDayStart
            // 
            this.ItemForHalfDayStart.Control = this.HalfDayStartCheckEdit;
            this.ItemForHalfDayStart.CustomizationFormText = "Half Day Start:";
            this.ItemForHalfDayStart.Location = new System.Drawing.Point(0, 24);
            this.ItemForHalfDayStart.Name = "ItemForHalfDayStart";
            this.ItemForHalfDayStart.Size = new System.Drawing.Size(395, 23);
            this.ItemForHalfDayStart.Text = "Half Day Start:";
            this.ItemForHalfDayStart.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForHalfDayEnd
            // 
            this.ItemForHalfDayEnd.Control = this.HalfDayEndCheckEdit;
            this.ItemForHalfDayEnd.CustomizationFormText = "Half Day End:";
            this.ItemForHalfDayEnd.Location = new System.Drawing.Point(395, 24);
            this.ItemForHalfDayEnd.Name = "ItemForHalfDayEnd";
            this.ItemForHalfDayEnd.Size = new System.Drawing.Size(395, 23);
            this.ItemForHalfDayEnd.Text = "Half Day End:";
            this.ItemForHalfDayEnd.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForAdditionalDaysPaid
            // 
            this.ItemForAdditionalDaysPaid.Control = this.AdditionalDaysPaidSpinEdit;
            this.ItemForAdditionalDaysPaid.CustomizationFormText = "Days Paid:";
            this.ItemForAdditionalDaysPaid.Location = new System.Drawing.Point(395, 95);
            this.ItemForAdditionalDaysPaid.Name = "ItemForAdditionalDaysPaid";
            this.ItemForAdditionalDaysPaid.Size = new System.Drawing.Size(395, 24);
            this.ItemForAdditionalDaysPaid.Text = "Days Paid:";
            this.ItemForAdditionalDaysPaid.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForUnitsPaidCSP
            // 
            this.ItemForUnitsPaidCSP.Control = this.UnitsPaidCSPSpinEdit;
            this.ItemForUnitsPaidCSP.CustomizationFormText = "Units Paid CSP:";
            this.ItemForUnitsPaidCSP.Location = new System.Drawing.Point(0, 143);
            this.ItemForUnitsPaidCSP.Name = "ItemForUnitsPaidCSP";
            this.ItemForUnitsPaidCSP.Size = new System.Drawing.Size(395, 24);
            this.ItemForUnitsPaidCSP.Text = "Units Paid CSP:";
            this.ItemForUnitsPaidCSP.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForUnitsPaidHalfPay
            // 
            this.ItemForUnitsPaidHalfPay.Control = this.UnitsPaidHalfPaySpinEdit;
            this.ItemForUnitsPaidHalfPay.CustomizationFormText = "Units Paid Half Pay:";
            this.ItemForUnitsPaidHalfPay.Location = new System.Drawing.Point(0, 167);
            this.ItemForUnitsPaidHalfPay.Name = "ItemForUnitsPaidHalfPay";
            this.ItemForUnitsPaidHalfPay.Size = new System.Drawing.Size(395, 24);
            this.ItemForUnitsPaidHalfPay.Text = "Units Paid Half Pay:";
            this.ItemForUnitsPaidHalfPay.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForUnitsPaidOther
            // 
            this.ItemForUnitsPaidOther.Control = this.UnitsPaidOtherSpinEdit;
            this.ItemForUnitsPaidOther.CustomizationFormText = "Units Paid Other:";
            this.ItemForUnitsPaidOther.Location = new System.Drawing.Point(0, 191);
            this.ItemForUnitsPaidOther.Name = "ItemForUnitsPaidOther";
            this.ItemForUnitsPaidOther.Size = new System.Drawing.Size(395, 24);
            this.ItemForUnitsPaidOther.Text = "Units Paid Other:";
            this.ItemForUnitsPaidOther.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForUnitsPaidUnpaid
            // 
            this.ItemForUnitsPaidUnpaid.Control = this.UnitsPaidUnpaidSpinEdit;
            this.ItemForUnitsPaidUnpaid.CustomizationFormText = "Units Unpaid:";
            this.ItemForUnitsPaidUnpaid.Location = new System.Drawing.Point(0, 215);
            this.ItemForUnitsPaidUnpaid.Name = "ItemForUnitsPaidUnpaid";
            this.ItemForUnitsPaidUnpaid.Size = new System.Drawing.Size(395, 24);
            this.ItemForUnitsPaidUnpaid.Text = "Units Unpaid:";
            this.ItemForUnitsPaidUnpaid.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForAbsenceComment
            // 
            this.ItemForAbsenceComment.Control = this.AbsenceCommentMemoEdit;
            this.ItemForAbsenceComment.CustomizationFormText = "Absence Comment:";
            this.ItemForAbsenceComment.Location = new System.Drawing.Point(395, 143);
            this.ItemForAbsenceComment.Name = "ItemForAbsenceComment";
            this.ItemForAbsenceComment.Size = new System.Drawing.Size(395, 96);
            this.ItemForAbsenceComment.Text = "Absence Comment:";
            this.ItemForAbsenceComment.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layGrpStatus
            // 
            this.layGrpStatus.CustomizationFormText = "Status";
            this.layGrpStatus.ExpandButtonVisible = true;
            this.layGrpStatus.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpStatus.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCancelledByPerson,
            this.ItemForCancelledDate,
            this.ItemForApprovedDate,
            this.ItemForApprovedByPerson,
            this.emptySpaceItem7,
            this.ItemForStatusId});
            this.layGrpStatus.Location = new System.Drawing.Point(0, 0);
            this.layGrpStatus.Name = "layGrpStatus";
            this.layGrpStatus.Size = new System.Drawing.Size(790, 262);
            this.layGrpStatus.Text = "Status";
            // 
            // ItemForCancelledByPerson
            // 
            this.ItemForCancelledByPerson.Control = this.CancelledByPersonButtonEdit;
            this.ItemForCancelledByPerson.CustomizationFormText = "Cancelled By:";
            this.ItemForCancelledByPerson.Location = new System.Drawing.Point(0, 48);
            this.ItemForCancelledByPerson.Name = "ItemForCancelledByPerson";
            this.ItemForCancelledByPerson.Size = new System.Drawing.Size(395, 24);
            this.ItemForCancelledByPerson.Text = "Cancelled By:";
            this.ItemForCancelledByPerson.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForCancelledDate
            // 
            this.ItemForCancelledDate.Control = this.CancelledDateDateEdit;
            this.ItemForCancelledDate.CustomizationFormText = "Cancelled Date";
            this.ItemForCancelledDate.Location = new System.Drawing.Point(395, 48);
            this.ItemForCancelledDate.Name = "ItemForCancelledDate";
            this.ItemForCancelledDate.Size = new System.Drawing.Size(395, 24);
            this.ItemForCancelledDate.Text = "Cancelled Date";
            this.ItemForCancelledDate.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForApprovedDate
            // 
            this.ItemForApprovedDate.Control = this.ApprovedDateDateEdit;
            this.ItemForApprovedDate.CustomizationFormText = "Approved Date";
            this.ItemForApprovedDate.Location = new System.Drawing.Point(395, 24);
            this.ItemForApprovedDate.Name = "ItemForApprovedDate";
            this.ItemForApprovedDate.Size = new System.Drawing.Size(395, 24);
            this.ItemForApprovedDate.Text = "Approved Date";
            this.ItemForApprovedDate.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForApprovedByPerson
            // 
            this.ItemForApprovedByPerson.Control = this.ApprovedByPersonButtonEdit;
            this.ItemForApprovedByPerson.CustomizationFormText = "Approved By:";
            this.ItemForApprovedByPerson.Location = new System.Drawing.Point(0, 24);
            this.ItemForApprovedByPerson.Name = "ItemForApprovedByPerson";
            this.ItemForApprovedByPerson.Size = new System.Drawing.Size(395, 24);
            this.ItemForApprovedByPerson.Text = "Approved By:";
            this.ItemForApprovedByPerson.TextSize = new System.Drawing.Size(113, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 72);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(790, 190);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStatusId
            // 
            this.ItemForStatusId.Control = this.StatusIdGridLookUpEdit;
            this.ItemForStatusId.CustomizationFormText = "Status:";
            this.ItemForStatusId.Location = new System.Drawing.Point(0, 0);
            this.ItemForStatusId.Name = "ItemForStatusId";
            this.ItemForStatusId.Size = new System.Drawing.Size(790, 24);
            this.ItemForStatusId.Text = "Status:";
            this.ItemForStatusId.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks:";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForComments});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(790, 262);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForComments
            // 
            this.ItemForComments.Control = this.CommentsMemoEdit;
            this.ItemForComments.CustomizationFormText = "Remarks:";
            this.ItemForComments.Location = new System.Drawing.Point(0, 0);
            this.ItemForComments.Name = "ItemForComments";
            this.ItemForComments.Size = new System.Drawing.Size(790, 262);
            this.ItemForComments.Text = "Remarks:";
            this.ItemForComments.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForComments.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "Holiday Entitlement - [Read Only]";
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup7,
            this.splitterItem1,
            this.layGrpEntitlements,
            this.splitterItem2});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 320);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(814, 240);
            this.layoutControlGroup8.Text = "Holiday Entitlement - [Read Only]";
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Entitlement";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForBankHolidayEntitlement,
            this.ItemForTotalHolidayEntitlement,
            this.ItemForHoursPerHolidayDay,
            this.emptySpaceItem9,
            this.emptySpaceItem3,
            this.simpleSeparator1,
            this.ItemForBasicHolidayEntitlement,
            this.ItemForPurchasedHolidayEntitlement});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup6.Size = new System.Drawing.Size(264, 194);
            this.layoutControlGroup6.Text = "Entitlement";
            // 
            // ItemForBankHolidayEntitlement
            // 
            this.ItemForBankHolidayEntitlement.Control = this.BankHolidayEntitlementTextEdit;
            this.ItemForBankHolidayEntitlement.CustomizationFormText = "Bank Holiday:";
            this.ItemForBankHolidayEntitlement.Location = new System.Drawing.Point(0, 48);
            this.ItemForBankHolidayEntitlement.Name = "ItemForBankHolidayEntitlement";
            this.ItemForBankHolidayEntitlement.Size = new System.Drawing.Size(248, 24);
            this.ItemForBankHolidayEntitlement.Text = "Bank Holidays:";
            this.ItemForBankHolidayEntitlement.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForTotalHolidayEntitlement
            // 
            this.ItemForTotalHolidayEntitlement.Control = this.TotalHolidayEntitlementTextEdit;
            this.ItemForTotalHolidayEntitlement.CustomizationFormText = "Total Holidays:";
            this.ItemForTotalHolidayEntitlement.Location = new System.Drawing.Point(0, 74);
            this.ItemForTotalHolidayEntitlement.Name = "ItemForTotalHolidayEntitlement";
            this.ItemForTotalHolidayEntitlement.Size = new System.Drawing.Size(248, 24);
            this.ItemForTotalHolidayEntitlement.Text = "Total Holidays:";
            this.ItemForTotalHolidayEntitlement.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForHoursPerHolidayDay
            // 
            this.ItemForHoursPerHolidayDay.Control = this.HoursPerHolidayDayTextEdit;
            this.ItemForHoursPerHolidayDay.CustomizationFormText = "Hours Per Holiday Day:";
            this.ItemForHoursPerHolidayDay.Location = new System.Drawing.Point(0, 122);
            this.ItemForHoursPerHolidayDay.Name = "ItemForHoursPerHolidayDay";
            this.ItemForHoursPerHolidayDay.Size = new System.Drawing.Size(248, 24);
            this.ItemForHoursPerHolidayDay.Text = "Hours Per Holiday Day:";
            this.ItemForHoursPerHolidayDay.TextSize = new System.Drawing.Size(113, 13);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.CustomizationFormText = "emptySpaceItem9";
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 146);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(248, 10);
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 24);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(248, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.CustomizationFormText = "simpleSeparator1";
            this.simpleSeparator1.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(248, 2);
            // 
            // ItemForBasicHolidayEntitlement
            // 
            this.ItemForBasicHolidayEntitlement.Control = this.BasicHolidayEntitlementTextEdit;
            this.ItemForBasicHolidayEntitlement.CustomizationFormText = "Holiday Entitlement:";
            this.ItemForBasicHolidayEntitlement.Location = new System.Drawing.Point(0, 24);
            this.ItemForBasicHolidayEntitlement.Name = "ItemForBasicHolidayEntitlement";
            this.ItemForBasicHolidayEntitlement.Size = new System.Drawing.Size(248, 24);
            this.ItemForBasicHolidayEntitlement.Text = "Holidays:";
            this.ItemForBasicHolidayEntitlement.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForPurchasedHolidayEntitlement
            // 
            this.ItemForPurchasedHolidayEntitlement.Control = this.PurchasedHolidayEntitlementTextEdit;
            this.ItemForPurchasedHolidayEntitlement.CustomizationFormText = "Purchased Holiday:";
            this.ItemForPurchasedHolidayEntitlement.Location = new System.Drawing.Point(0, 0);
            this.ItemForPurchasedHolidayEntitlement.Name = "ItemForPurchasedHolidayEntitlement";
            this.ItemForPurchasedHolidayEntitlement.Size = new System.Drawing.Size(248, 24);
            this.ItemForPurchasedHolidayEntitlement.Text = "Purchased Holidays:";
            this.ItemForPurchasedHolidayEntitlement.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Used";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHolidaysTaken,
            this.ItemForBankHolidaysTaken,
            this.ItemForAbsenceAuthorisedTaken,
            this.ItemForAbsenceUnauthorisedTaken,
            this.ItemForTotalHolidaysTaken,
            this.emptySpaceItem10,
            this.simpleSeparator2,
            this.emptySpaceItem4});
            this.layoutControlGroup7.Location = new System.Drawing.Point(270, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup7.Size = new System.Drawing.Size(251, 194);
            this.layoutControlGroup7.Text = "Used";
            // 
            // ItemForHolidaysTaken
            // 
            this.ItemForHolidaysTaken.Control = this.HolidaysTakenTextEdit;
            this.ItemForHolidaysTaken.CustomizationFormText = "Holidays Used:";
            this.ItemForHolidaysTaken.Location = new System.Drawing.Point(0, 24);
            this.ItemForHolidaysTaken.Name = "ItemForHolidaysTaken";
            this.ItemForHolidaysTaken.Size = new System.Drawing.Size(235, 24);
            this.ItemForHolidaysTaken.Text = "Holidays:";
            this.ItemForHolidaysTaken.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForBankHolidaysTaken
            // 
            this.ItemForBankHolidaysTaken.Control = this.BankHolidaysTakenTextEdit;
            this.ItemForBankHolidaysTaken.CustomizationFormText = "Bank Holidays Used:";
            this.ItemForBankHolidaysTaken.Location = new System.Drawing.Point(0, 48);
            this.ItemForBankHolidaysTaken.Name = "ItemForBankHolidaysTaken";
            this.ItemForBankHolidaysTaken.Size = new System.Drawing.Size(235, 24);
            this.ItemForBankHolidaysTaken.Text = "Bank Holidays:";
            this.ItemForBankHolidaysTaken.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForAbsenceAuthorisedTaken
            // 
            this.ItemForAbsenceAuthorisedTaken.Control = this.AbsenceAuthorisedTakenTextEdit;
            this.ItemForAbsenceAuthorisedTaken.CustomizationFormText = "Authorised Absence Used:";
            this.ItemForAbsenceAuthorisedTaken.Location = new System.Drawing.Point(0, 98);
            this.ItemForAbsenceAuthorisedTaken.Name = "ItemForAbsenceAuthorisedTaken";
            this.ItemForAbsenceAuthorisedTaken.Size = new System.Drawing.Size(235, 24);
            this.ItemForAbsenceAuthorisedTaken.Text = "Authorised Absence:";
            this.ItemForAbsenceAuthorisedTaken.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForAbsenceUnauthorisedTaken
            // 
            this.ItemForAbsenceUnauthorisedTaken.Control = this.AbsenceUnauthorisedTakenTextEdit;
            this.ItemForAbsenceUnauthorisedTaken.CustomizationFormText = "Unauthorised Absence Used:";
            this.ItemForAbsenceUnauthorisedTaken.Location = new System.Drawing.Point(0, 122);
            this.ItemForAbsenceUnauthorisedTaken.Name = "ItemForAbsenceUnauthorisedTaken";
            this.ItemForAbsenceUnauthorisedTaken.Size = new System.Drawing.Size(235, 24);
            this.ItemForAbsenceUnauthorisedTaken.Text = "Unauthorised Absence:";
            this.ItemForAbsenceUnauthorisedTaken.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForTotalHolidaysTaken
            // 
            this.ItemForTotalHolidaysTaken.Control = this.TotalHolidaysTakenTextEdit;
            this.ItemForTotalHolidaysTaken.CustomizationFormText = "Total Holidays Used:";
            this.ItemForTotalHolidaysTaken.Location = new System.Drawing.Point(0, 74);
            this.ItemForTotalHolidaysTaken.Name = "ItemForTotalHolidaysTaken";
            this.ItemForTotalHolidaysTaken.Size = new System.Drawing.Size(235, 24);
            this.ItemForTotalHolidaysTaken.Text = "Total Holidays:";
            this.ItemForTotalHolidaysTaken.TextSize = new System.Drawing.Size(113, 13);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.CustomizationFormText = "emptySpaceItem10";
            this.emptySpaceItem10.Location = new System.Drawing.Point(0, 146);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(235, 10);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator2
            // 
            this.simpleSeparator2.AllowHotTrack = false;
            this.simpleSeparator2.CustomizationFormText = "simpleSeparator2";
            this.simpleSeparator2.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator2.Name = "simpleSeparator2";
            this.simpleSeparator2.Size = new System.Drawing.Size(235, 2);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 24);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(235, 24);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(264, 0);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(6, 194);
            // 
            // layGrpEntitlements
            // 
            this.layGrpEntitlements.CustomizationFormText = "Entitlements";
            this.layGrpEntitlements.ExpandButtonVisible = true;
            this.layGrpEntitlements.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpEntitlements.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForBankHolidaysRemaining,
            this.ItemForHolidaysRemaining,
            this.ItemForTotalHolidaysRemaining,
            this.emptySpaceItem11,
            this.simpleSeparator3,
            this.emptySpaceItem12});
            this.layGrpEntitlements.Location = new System.Drawing.Point(527, 0);
            this.layGrpEntitlements.Name = "layGrpEntitlements";
            this.layGrpEntitlements.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layGrpEntitlements.Size = new System.Drawing.Size(263, 194);
            this.layGrpEntitlements.Text = "Remaining";
            // 
            // ItemForBankHolidaysRemaining
            // 
            this.ItemForBankHolidaysRemaining.Control = this.BankHolidaysRemainingTextEdit;
            this.ItemForBankHolidaysRemaining.CustomizationFormText = "Bank Holidays:";
            this.ItemForBankHolidaysRemaining.Location = new System.Drawing.Point(0, 48);
            this.ItemForBankHolidaysRemaining.Name = "ItemForBankHolidaysRemaining";
            this.ItemForBankHolidaysRemaining.Size = new System.Drawing.Size(247, 24);
            this.ItemForBankHolidaysRemaining.Text = "Bank Holidays:";
            this.ItemForBankHolidaysRemaining.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForHolidaysRemaining
            // 
            this.ItemForHolidaysRemaining.Control = this.HolidaysRemainingTextEdit;
            this.ItemForHolidaysRemaining.CustomizationFormText = "Holidays:";
            this.ItemForHolidaysRemaining.Location = new System.Drawing.Point(0, 24);
            this.ItemForHolidaysRemaining.Name = "ItemForHolidaysRemaining";
            this.ItemForHolidaysRemaining.Size = new System.Drawing.Size(247, 24);
            this.ItemForHolidaysRemaining.Text = "Holidays:";
            this.ItemForHolidaysRemaining.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForTotalHolidaysRemaining
            // 
            this.ItemForTotalHolidaysRemaining.Control = this.TotalHolidaysRemainingTextEdit;
            this.ItemForTotalHolidaysRemaining.CustomizationFormText = "Total Holidays Remaining:";
            this.ItemForTotalHolidaysRemaining.Location = new System.Drawing.Point(0, 74);
            this.ItemForTotalHolidaysRemaining.Name = "ItemForTotalHolidaysRemaining";
            this.ItemForTotalHolidaysRemaining.Size = new System.Drawing.Size(247, 24);
            this.ItemForTotalHolidaysRemaining.Text = "Total Holidays:";
            this.ItemForTotalHolidaysRemaining.TextSize = new System.Drawing.Size(113, 13);
            // 
            // emptySpaceItem11
            // 
            this.emptySpaceItem11.AllowHotTrack = false;
            this.emptySpaceItem11.CustomizationFormText = "emptySpaceItem11";
            this.emptySpaceItem11.Location = new System.Drawing.Point(0, 98);
            this.emptySpaceItem11.Name = "emptySpaceItem11";
            this.emptySpaceItem11.Size = new System.Drawing.Size(247, 58);
            this.emptySpaceItem11.TextSize = new System.Drawing.Size(0, 0);
            // 
            // simpleSeparator3
            // 
            this.simpleSeparator3.AllowHotTrack = false;
            this.simpleSeparator3.CustomizationFormText = "simpleSeparator3";
            this.simpleSeparator3.Location = new System.Drawing.Point(0, 72);
            this.simpleSeparator3.Name = "simpleSeparator3";
            this.simpleSeparator3.Size = new System.Drawing.Size(247, 2);
            // 
            // emptySpaceItem12
            // 
            this.emptySpaceItem12.AllowHotTrack = false;
            this.emptySpaceItem12.CustomizationFormText = "emptySpaceItem12";
            this.emptySpaceItem12.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem12.MaxSize = new System.Drawing.Size(0, 24);
            this.emptySpaceItem12.MinSize = new System.Drawing.Size(10, 24);
            this.emptySpaceItem12.Name = "emptySpaceItem12";
            this.emptySpaceItem12.Size = new System.Drawing.Size(247, 24);
            this.emptySpaceItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem12.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(521, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(6, 194);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 310);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(814, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "Absence Entitlement - [Read Only]";
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup10,
            this.layoutControlGroup11,
            this.splitterItem3});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 570);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(814, 164);
            this.layoutControlGroup9.Text = "Absence Entitlement - [Read Only]";
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "Sick Pay 1";
            this.layoutControlGroup10.ExpandButtonVisible = true;
            this.layoutControlGroup10.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSickPayUnits1,
            this.ItemForSickPayUnitDescriptor1,
            this.ItemForSickPayPercentage1});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(394, 118);
            this.layoutControlGroup10.Text = "Sick Pay 1";
            // 
            // ItemForSickPayUnits1
            // 
            this.ItemForSickPayUnits1.Control = this.SickPayUnits1TextEdit;
            this.ItemForSickPayUnits1.CustomizationFormText = "Sick Pay Units 1:";
            this.ItemForSickPayUnits1.Location = new System.Drawing.Point(0, 0);
            this.ItemForSickPayUnits1.Name = "ItemForSickPayUnits1";
            this.ItemForSickPayUnits1.Size = new System.Drawing.Size(370, 24);
            this.ItemForSickPayUnits1.Text = "Units:";
            this.ItemForSickPayUnits1.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForSickPayUnitDescriptor1
            // 
            this.ItemForSickPayUnitDescriptor1.Control = this.SickPayUnitDescriptor1TextEdit;
            this.ItemForSickPayUnitDescriptor1.CustomizationFormText = "Sick Pay Unit Descriptor 1:";
            this.ItemForSickPayUnitDescriptor1.Location = new System.Drawing.Point(0, 24);
            this.ItemForSickPayUnitDescriptor1.Name = "ItemForSickPayUnitDescriptor1";
            this.ItemForSickPayUnitDescriptor1.Size = new System.Drawing.Size(370, 24);
            this.ItemForSickPayUnitDescriptor1.Text = "Unit Descriptor:";
            this.ItemForSickPayUnitDescriptor1.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForSickPayPercentage1
            // 
            this.ItemForSickPayPercentage1.Control = this.SickPayPercentage1TextEdit;
            this.ItemForSickPayPercentage1.CustomizationFormText = "Sick Pay Percentage 1:";
            this.ItemForSickPayPercentage1.Location = new System.Drawing.Point(0, 48);
            this.ItemForSickPayPercentage1.Name = "ItemForSickPayPercentage1";
            this.ItemForSickPayPercentage1.Size = new System.Drawing.Size(370, 24);
            this.ItemForSickPayPercentage1.Text = "Pay Percentage:";
            this.ItemForSickPayPercentage1.TextSize = new System.Drawing.Size(113, 13);
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Sick Pay 2";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSickPayUnits2,
            this.ItemForSickPayUnitDescriptor2,
            this.ItemForSickPayPercentage2});
            this.layoutControlGroup11.Location = new System.Drawing.Point(400, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(390, 118);
            this.layoutControlGroup11.Text = "Sick Pay 2";
            // 
            // ItemForSickPayUnits2
            // 
            this.ItemForSickPayUnits2.Control = this.SickPayUnits2TextEdit;
            this.ItemForSickPayUnits2.CustomizationFormText = "Sick Pay Units 2:";
            this.ItemForSickPayUnits2.Location = new System.Drawing.Point(0, 0);
            this.ItemForSickPayUnits2.Name = "ItemForSickPayUnits2";
            this.ItemForSickPayUnits2.Size = new System.Drawing.Size(366, 24);
            this.ItemForSickPayUnits2.Text = "Units:";
            this.ItemForSickPayUnits2.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForSickPayUnitDescriptor2
            // 
            this.ItemForSickPayUnitDescriptor2.Control = this.SickPayUnitDescriptor2TextEdit;
            this.ItemForSickPayUnitDescriptor2.CustomizationFormText = "Sick Pay Unit Descriptor 2:";
            this.ItemForSickPayUnitDescriptor2.Location = new System.Drawing.Point(0, 24);
            this.ItemForSickPayUnitDescriptor2.Name = "ItemForSickPayUnitDescriptor2";
            this.ItemForSickPayUnitDescriptor2.Size = new System.Drawing.Size(366, 24);
            this.ItemForSickPayUnitDescriptor2.Text = "Unit Descriptor:";
            this.ItemForSickPayUnitDescriptor2.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForSickPayPercentage2
            // 
            this.ItemForSickPayPercentage2.Control = this.SickPayPercentage2TextEdit;
            this.ItemForSickPayPercentage2.CustomizationFormText = "Sick Pay Percentage 2:";
            this.ItemForSickPayPercentage2.Location = new System.Drawing.Point(0, 48);
            this.ItemForSickPayPercentage2.Name = "ItemForSickPayPercentage2";
            this.ItemForSickPayPercentage2.Size = new System.Drawing.Size(366, 24);
            this.ItemForSickPayPercentage2.Text = "Pay Percentage:";
            this.ItemForSickPayPercentage2.TextSize = new System.Drawing.Size(113, 13);
            // 
            // splitterItem3
            // 
            this.splitterItem3.AllowHotTrack = true;
            this.splitterItem3.CustomizationFormText = "splitterItem3";
            this.splitterItem3.Location = new System.Drawing.Point(394, 0);
            this.splitterItem3.Name = "splitterItem3";
            this.splitterItem3.Size = new System.Drawing.Size(6, 118);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.AllowHotTrack = false;
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 560);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(814, 10);
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(838, 12);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(116, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(116, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(116, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(116, 23);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(316, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(522, 23);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForHolidayYearDescription
            // 
            this.ItemForHolidayYearDescription.Control = this.HolidayYearDescriptionButtonEdit;
            this.ItemForHolidayYearDescription.CustomizationFormText = "Employee Holiday Year:";
            this.ItemForHolidayYearDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForHolidayYearDescription.Name = "ItemForHolidayYearDescription";
            this.ItemForHolidayYearDescription.Size = new System.Drawing.Size(838, 24);
            this.ItemForHolidayYearDescription.Text = "Employee Holiday Year:";
            this.ItemForHolidayYearDescription.TextSize = new System.Drawing.Size(113, 13);
            // 
            // ItemForRecordId
            // 
            this.ItemForRecordId.Control = this.RecordTypeIdGridLookUpEdit;
            this.ItemForRecordId.CustomizationFormText = "Record Type:";
            this.ItemForRecordId.Location = new System.Drawing.Point(0, 47);
            this.ItemForRecordId.Name = "ItemForRecordId";
            this.ItemForRecordId.Size = new System.Drawing.Size(838, 24);
            this.ItemForRecordId.Text = "Record Type:";
            this.ItemForRecordId.TextSize = new System.Drawing.Size(113, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 863);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(838, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00051_Get_Employee_Absence_ItemTableAdapter
            // 
            this.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00078_Absence_Record_Types_List_With_BlankTableAdapter
            // 
            this.sp_HR_00078_Absence_Record_Types_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00079_Absence_Payment_Types_List_With_BlankTableAdapter
            // 
            this.sp_HR_00079_Absence_Payment_Types_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00080_Absence_Status_List_With_BlankTableAdapter
            // 
            this.sp_HR_00080_Absence_Status_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Employee_Absence_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(875, 496);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Absence_Edit";
            this.Text = "Edit Time & Attendance Record";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Absence_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Absence_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Absence_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SickPayPercentage2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00051GetEmployeeAbsenceItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayPercentage1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayUnitDescriptor2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayUnitDescriptor1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayUnits2TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SickPayUnits1TextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceCommentMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsPaidOtherSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsPaidHalfPaySpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsPaidCSPSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdditionalDaysPaidSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HalfDayEndCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HalfDayStartCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EWCDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EWCDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidaysRemainingTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidaysTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidaysRemainingTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidaysRemainingTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TotalHolidayEntitlementTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HoursPerHolidayDayTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PurchasedHolidayEntitlementTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidayEntitlementTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledByIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApproverIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceSubTypeIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceTypeIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIdGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00080AbsenceStatusListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayCategoryIdGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00079AbsencePaymentTypesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayUnitDescriptorTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecordTypeIdGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00078AbsenceRecordTypesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeHolidayYearIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidayYearDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DeclaredDisabilityCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedReturnDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpectedReturnDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsUsedSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IsWorkRelatedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CommentsMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ApprovedByPersonButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelledByPersonButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceTypeDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceSubTypeDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HolidaysTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankHolidaysTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceAuthorisedTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceUnauthorisedTakenTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BasicHolidayEntitlementTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitsPaidUnpaidSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeHolidayYearId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceTypeId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceSubTypeId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApproverId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledById)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIsWorkRelated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpectedReturnDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPayCategoryId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDeclaredDisability)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayUnitDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEWCDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHalfDayStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHalfDayEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdditionalDaysPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsPaidCSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsPaidHalfPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsPaidOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitsPaidUnpaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledByPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCancelledDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApprovedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForApprovedByPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidayEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidayEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHoursPerHolidayDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBasicHolidayEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPurchasedHolidayEntitlement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidaysTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidaysTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceAuthorisedTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceUnauthorisedTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidaysTaken)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpEntitlements)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBankHolidaysRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidaysRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTotalHolidaysRemaining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayUnits1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayUnitDescriptor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayPercentage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayUnits2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayUnitDescriptor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSickPayPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHolidayYearDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRecordId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeHolidayYearId;
        private DevExpress.XtraEditors.TextEdit EmployeeHolidayYearIdTextEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit IdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForId;
        private System.Windows.Forms.BindingSource spHR00051GetEmployeeAbsenceItemBindingSource;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.ButtonEdit HolidayYearDescriptionButtonEdit;
        private DevExpress.XtraEditors.CheckEdit DeclaredDisabilityCheckEdit;
        private DevExpress.XtraEditors.DateEdit StartDateDateEdit;
        private DevExpress.XtraEditors.DateEdit EndDateDateEdit;
        private DevExpress.XtraEditors.DateEdit ExpectedReturnDateDateEdit;
        private DevExpress.XtraEditors.SpinEdit UnitsUsedSpinEdit;
        private DevExpress.XtraEditors.CheckEdit IsWorkRelatedCheckEdit;
        private DevExpress.XtraEditors.DateEdit ApprovedDateDateEdit;
        private DevExpress.XtraEditors.DateEdit CancelledDateDateEdit;
        private DevExpress.XtraEditors.MemoEdit CommentsMemoEdit;
        private DevExpress.XtraEditors.ButtonEdit ApprovedByPersonButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit CancelledByPersonButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpEntitlements;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidaysTaken;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBasicHolidayEntitlement;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceAuthorisedTaken;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceUnauthorisedTaken;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBankHolidaysTaken;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceTypeDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceSubTypeDescription;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpectedReturnDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitsUsed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIsWorkRelated;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpStatus;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCancelledByPerson;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCancelledDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForApprovedDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDeclaredDisability;
        private DevExpress.XtraLayout.LayoutControlItem ItemForApprovedByPerson;
        private DevExpress.XtraLayout.LayoutControlItem ItemForComments;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidayYearDescription;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter sp_HR_00051_Get_Employee_Absence_ItemTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit AbsenceTypeDescriptionButtonEdit;
        private DevExpress.XtraEditors.TextEdit AbsenceSubTypeDescriptionTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit RecordTypeIdGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRecordId;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00078AbsenceRecordTypesListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00078_Absence_Record_Types_List_With_BlankTableAdapter sp_HR_00078_Absence_Record_Types_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.TextEdit AbsenceTypeIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceTypeId;
        private DevExpress.XtraEditors.TextEdit AbsenceSubTypeIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceSubTypeId;
        private DevExpress.XtraEditors.TextEdit HolidayUnitDescriptorTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidayUnitDescriptor;
        private DevExpress.XtraEditors.TextEdit ApproverIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForApproverId;
        private DevExpress.XtraEditors.TextEdit CancelledByIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCancelledById;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.GridLookUpEdit PayCategoryIdGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPayCategoryId;
        private System.Windows.Forms.BindingSource spHR00079AbsencePaymentTypesListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00079_Absence_Payment_Types_List_With_BlankTableAdapter sp_HR_00079_Absence_Payment_Types_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIdGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusId;
        private System.Windows.Forms.BindingSource spHR00080AbsenceStatusListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00080_Absence_Status_List_With_BlankTableAdapter sp_HR_00080_Absence_Status_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.TextEdit HolidaysTakenTextEdit;
        private DevExpress.XtraEditors.TextEdit BankHolidaysTakenTextEdit;
        private DevExpress.XtraEditors.TextEdit AbsenceAuthorisedTakenTextEdit;
        private DevExpress.XtraEditors.TextEdit AbsenceUnauthorisedTakenTextEdit;
        private DevExpress.XtraEditors.TextEdit BasicHolidayEntitlementTextEdit;
        private DevExpress.XtraEditors.TextEdit BankHolidayEntitlementTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBankHolidayEntitlement;
        private DevExpress.XtraEditors.TextEdit PurchasedHolidayEntitlementTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPurchasedHolidayEntitlement;
        private DevExpress.XtraEditors.TextEdit HoursPerHolidayDayTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHoursPerHolidayDay;
        private DevExpress.XtraEditors.TextEdit TotalHolidayEntitlementTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalHolidayEntitlement;
        private DevExpress.XtraEditors.TextEdit HolidaysRemainingTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHolidaysRemaining;
        private DevExpress.XtraEditors.TextEdit BankHolidaysRemainingTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBankHolidaysRemaining;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.TextEdit TotalHolidaysTakenTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalHolidaysTaken;
        private DevExpress.XtraEditors.TextEdit TotalHolidaysRemainingTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTotalHolidaysRemaining;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem11;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator2;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem12;
        private DevExpress.XtraEditors.DateEdit EWCDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEWCDate;
        private DevExpress.XtraEditors.CheckEdit HalfDayEndCheckEdit;
        private DevExpress.XtraEditors.CheckEdit HalfDayStartCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHalfDayStart;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHalfDayEnd;
        private DevExpress.XtraEditors.SpinEdit AdditionalDaysPaidSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAdditionalDaysPaid;
        private DevExpress.XtraEditors.TextEdit EmployeeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeID;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private DevExpress.XtraEditors.SpinEdit UnitsPaidOtherSpinEdit;
        private DevExpress.XtraEditors.SpinEdit UnitsPaidHalfPaySpinEdit;
        private DevExpress.XtraEditors.SpinEdit UnitsPaidCSPSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitsPaidCSP;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitsPaidHalfPay;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitsPaidOther;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitsPaidUnpaid;
        private DevExpress.XtraEditors.TextEdit UnitsPaidUnpaidSpinEdit;
        private DevExpress.XtraEditors.MemoEdit AbsenceCommentMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceComment;
        private DevExpress.XtraEditors.TextEdit SickPayUnits2TextEdit;
        private DevExpress.XtraEditors.TextEdit SickPayUnits1TextEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSickPayUnits1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSickPayUnits2;
        private DevExpress.XtraLayout.SplitterItem splitterItem3;
        private DevExpress.XtraEditors.TextEdit SickPayUnitDescriptor2TextEdit;
        private DevExpress.XtraEditors.TextEdit SickPayUnitDescriptor1TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSickPayUnitDescriptor1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSickPayUnitDescriptor2;
        private DevExpress.XtraEditors.TextEdit SickPayPercentage2TextEdit;
        private DevExpress.XtraEditors.TextEdit SickPayPercentage1TextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSickPayPercentage1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSickPayPercentage2;
    }
}

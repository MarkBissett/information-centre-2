namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Shares
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00225EmployeeSharesSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSharesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShareTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateReceived1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colShareUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShareUnitsUnits = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colShareUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShareUnitsCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShareType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShareUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShareUnitsPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditShareUnitsUnknown = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00225_Employee_Shares_SelectTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00225_Employee_Shares_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00225EmployeeSharesSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsUnknown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colActive3
            // 
            this.colActive3.Caption = "Active";
            this.colActive3.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive3.FieldName = "Active";
            this.colActive3.Name = "colActive3";
            this.colActive3.OptionsColumn.AllowEdit = false;
            this.colActive3.OptionsColumn.AllowFocus = false;
            this.colActive3.OptionsColumn.ReadOnly = true;
            this.colActive3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive3.Visible = true;
            this.colActive3.VisibleIndex = 2;
            this.colActive3.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00225EmployeeSharesSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditShareUnitsCurrency,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditShareUnitsPercentage,
            this.repositoryItemTextEditShareUnitsUnits,
            this.repositoryItemTextEditShareUnitsUnknown});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00225EmployeeSharesSelectBindingSource
            // 
            this.spHR00225EmployeeSharesSelectBindingSource.DataMember = "sp_HR_00225_Employee_Shares_Select";
            this.spHR00225EmployeeSharesSelectBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSharesID,
            this.colEmployeeID13,
            this.colShareTypeID,
            this.colActive3,
            this.colDateReceived1,
            this.colShareUnits,
            this.colShareUnitDescriptorID,
            this.colUnitValue,
            this.colRemarks11,
            this.colEmployeeName9,
            this.colEmployeeSurname10,
            this.colEmployeeFirstname8,
            this.colEmployeeNumber13,
            this.colShareType,
            this.colShareUnitDescriptor});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive3;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.BestFitMaxRowCount = 5;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName9, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateReceived1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colSharesID
            // 
            this.colSharesID.Caption = "Shares ID";
            this.colSharesID.FieldName = "SharesID";
            this.colSharesID.Name = "colSharesID";
            this.colSharesID.OptionsColumn.AllowEdit = false;
            this.colSharesID.OptionsColumn.AllowFocus = false;
            this.colSharesID.OptionsColumn.ReadOnly = true;
            this.colSharesID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEmployeeID13
            // 
            this.colEmployeeID13.Caption = "Employee ID";
            this.colEmployeeID13.FieldName = "EmployeeID";
            this.colEmployeeID13.Name = "colEmployeeID13";
            this.colEmployeeID13.OptionsColumn.AllowEdit = false;
            this.colEmployeeID13.OptionsColumn.AllowFocus = false;
            this.colEmployeeID13.OptionsColumn.ReadOnly = true;
            this.colEmployeeID13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID13.Width = 81;
            // 
            // colShareTypeID
            // 
            this.colShareTypeID.Caption = "Share Type ID";
            this.colShareTypeID.FieldName = "ShareTypeID";
            this.colShareTypeID.Name = "colShareTypeID";
            this.colShareTypeID.OptionsColumn.AllowEdit = false;
            this.colShareTypeID.OptionsColumn.AllowFocus = false;
            this.colShareTypeID.OptionsColumn.ReadOnly = true;
            this.colShareTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShareTypeID.Width = 90;
            // 
            // colDateReceived1
            // 
            this.colDateReceived1.Caption = "Date Received";
            this.colDateReceived1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateReceived1.FieldName = "DateReceived";
            this.colDateReceived1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateReceived1.Name = "colDateReceived1";
            this.colDateReceived1.OptionsColumn.AllowEdit = false;
            this.colDateReceived1.OptionsColumn.AllowFocus = false;
            this.colDateReceived1.OptionsColumn.ReadOnly = true;
            this.colDateReceived1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateReceived1.Visible = true;
            this.colDateReceived1.VisibleIndex = 0;
            this.colDateReceived1.Width = 108;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colShareUnits
            // 
            this.colShareUnits.Caption = "Share Units";
            this.colShareUnits.ColumnEdit = this.repositoryItemTextEditShareUnitsUnits;
            this.colShareUnits.FieldName = "ShareUnits";
            this.colShareUnits.Name = "colShareUnits";
            this.colShareUnits.OptionsColumn.AllowEdit = false;
            this.colShareUnits.OptionsColumn.AllowFocus = false;
            this.colShareUnits.OptionsColumn.ReadOnly = true;
            this.colShareUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShareUnits.Visible = true;
            this.colShareUnits.VisibleIndex = 3;
            this.colShareUnits.Width = 103;
            // 
            // repositoryItemTextEditShareUnitsUnits
            // 
            this.repositoryItemTextEditShareUnitsUnits.AutoHeight = false;
            this.repositoryItemTextEditShareUnitsUnits.Mask.EditMask = "######0.00 Units";
            this.repositoryItemTextEditShareUnitsUnits.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditShareUnitsUnits.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShareUnitsUnits.Name = "repositoryItemTextEditShareUnitsUnits";
            // 
            // colShareUnitDescriptorID
            // 
            this.colShareUnitDescriptorID.Caption = "Unit Descriptor ID";
            this.colShareUnitDescriptorID.FieldName = "ShareUnitDescriptorID";
            this.colShareUnitDescriptorID.Name = "colShareUnitDescriptorID";
            this.colShareUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colShareUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colShareUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colShareUnitDescriptorID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShareUnitDescriptorID.Width = 106;
            // 
            // colUnitValue
            // 
            this.colUnitValue.AppearanceCell.Options.UseTextOptions = true;
            this.colUnitValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colUnitValue.Caption = "Unit Value";
            this.colUnitValue.ColumnEdit = this.repositoryItemTextEditShareUnitsCurrency;
            this.colUnitValue.FieldName = "UnitValue";
            this.colUnitValue.Name = "colUnitValue";
            this.colUnitValue.OptionsColumn.AllowEdit = false;
            this.colUnitValue.OptionsColumn.AllowFocus = false;
            this.colUnitValue.OptionsColumn.ReadOnly = true;
            this.colUnitValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUnitValue.Visible = true;
            this.colUnitValue.VisibleIndex = 4;
            this.colUnitValue.Width = 112;
            // 
            // repositoryItemTextEditShareUnitsCurrency
            // 
            this.repositoryItemTextEditShareUnitsCurrency.AutoHeight = false;
            this.repositoryItemTextEditShareUnitsCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditShareUnitsCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditShareUnitsCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShareUnitsCurrency.Name = "repositoryItemTextEditShareUnitsCurrency";
            // 
            // colRemarks11
            // 
            this.colRemarks11.Caption = "Remarks";
            this.colRemarks11.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks11.FieldName = "Remarks";
            this.colRemarks11.Name = "colRemarks11";
            this.colRemarks11.OptionsColumn.ReadOnly = true;
            this.colRemarks11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks11.Visible = true;
            this.colRemarks11.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colEmployeeName9
            // 
            this.colEmployeeName9.Caption = "Linked To Employee";
            this.colEmployeeName9.FieldName = "EmployeeName";
            this.colEmployeeName9.Name = "colEmployeeName9";
            this.colEmployeeName9.OptionsColumn.AllowEdit = false;
            this.colEmployeeName9.OptionsColumn.AllowFocus = false;
            this.colEmployeeName9.OptionsColumn.ReadOnly = true;
            this.colEmployeeName9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName9.Visible = true;
            this.colEmployeeName9.VisibleIndex = 7;
            this.colEmployeeName9.Width = 229;
            // 
            // colEmployeeSurname10
            // 
            this.colEmployeeSurname10.Caption = "Surname";
            this.colEmployeeSurname10.FieldName = "EmployeeSurname";
            this.colEmployeeSurname10.Name = "colEmployeeSurname10";
            this.colEmployeeSurname10.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname10.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname10.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname10.Width = 147;
            // 
            // colEmployeeFirstname8
            // 
            this.colEmployeeFirstname8.Caption = "Forename";
            this.colEmployeeFirstname8.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname8.Name = "colEmployeeFirstname8";
            this.colEmployeeFirstname8.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname8.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname8.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname8.Width = 157;
            // 
            // colEmployeeNumber13
            // 
            this.colEmployeeNumber13.Caption = "Employee #";
            this.colEmployeeNumber13.FieldName = "EmployeeNumber";
            this.colEmployeeNumber13.Name = "colEmployeeNumber13";
            this.colEmployeeNumber13.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber13.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber13.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber13.Width = 121;
            // 
            // colShareType
            // 
            this.colShareType.Caption = "Share Type";
            this.colShareType.FieldName = "ShareType";
            this.colShareType.Name = "colShareType";
            this.colShareType.OptionsColumn.AllowEdit = false;
            this.colShareType.OptionsColumn.AllowFocus = false;
            this.colShareType.OptionsColumn.ReadOnly = true;
            this.colShareType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShareType.Visible = true;
            this.colShareType.VisibleIndex = 1;
            this.colShareType.Width = 238;
            // 
            // colShareUnitDescriptor
            // 
            this.colShareUnitDescriptor.Caption = "Unit Descriptor";
            this.colShareUnitDescriptor.FieldName = "ShareUnitDescriptor";
            this.colShareUnitDescriptor.Name = "colShareUnitDescriptor";
            this.colShareUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colShareUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colShareUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colShareUnitDescriptor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colShareUnitDescriptor.Width = 92;
            // 
            // repositoryItemTextEditShareUnitsPercentage
            // 
            this.repositoryItemTextEditShareUnitsPercentage.AutoHeight = false;
            this.repositoryItemTextEditShareUnitsPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditShareUnitsPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditShareUnitsPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShareUnitsPercentage.Name = "repositoryItemTextEditShareUnitsPercentage";
            // 
            // repositoryItemTextEditShareUnitsUnknown
            // 
            this.repositoryItemTextEditShareUnitsUnknown.AutoHeight = false;
            this.repositoryItemTextEditShareUnitsUnknown.Mask.EditMask = "######0.00";
            this.repositoryItemTextEditShareUnitsUnknown.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditShareUnitsUnknown.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShareUnitsUnknown.Name = "repositoryItemTextEditShareUnitsUnknown";
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00225_Employee_Shares_SelectTableAdapter
            // 
            this.sp_HR_00225_Employee_Shares_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_Shares
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Shares";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Shares";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Shares_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00225EmployeeSharesSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsUnknown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShareUnitsCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource spHR00225EmployeeSharesSelectBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00225_Employee_Shares_SelectTableAdapter sp_HR_00225_Employee_Shares_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSharesID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID13;
        private DevExpress.XtraGrid.Columns.GridColumn colShareTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colActive3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateReceived1;
        private DevExpress.XtraGrid.Columns.GridColumn colShareUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShareUnitsPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colShareUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks11;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName9;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname10;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname8;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber13;
        private DevExpress.XtraGrid.Columns.GridColumn colShareType;
        private DevExpress.XtraGrid.Columns.GridColumn colShareUnitDescriptor;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShareUnitsUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShareUnitsUnknown;
    }
}

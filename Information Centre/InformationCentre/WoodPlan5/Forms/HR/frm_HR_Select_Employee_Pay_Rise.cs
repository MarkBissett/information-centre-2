using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_HR_Select_Employee_Pay_Rise : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public int intOriginalID = 0;
        public string strOriginalIDs = "";
        public int intSelectedID = 0;
        public string strSelectedIDs = "";
        public string strSelectedDescription = "";
        public int _SelectedCount = 0;
        BaseObjects.GridCheckMarksSelection selection1;
        
        private int ShowSalaryData = 1;  // Controls if the Contracts grid returns salary info //

        #endregion

        public frm_HR_Select_Employee_Pay_Rise()
        {
            InitializeComponent();
        }

        private void frm_HR_Select_Employee_Pay_Rise_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500114;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            ShowSalaryData = (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey) ? 0 : 1);
            
            sp_HR_00217_Employee_PayRises_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;

            LoadData();
            gridControl1.ForceInitialize();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;

                Array arrayRecords = strOriginalIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["PayRiseID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["PayRiseID"], intOriginalID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp_HR_00217_Employee_PayRises_SelectTableAdapter.Fill(dataSet_HR_Core.sp_HR_00217_Employee_PayRises_Select, ShowSalaryData);
            
            // Decrypt all Financial Data //
            SimplerAES Encryptor = new SimplerAES();
            string strAmount = "";
            try
            {
                foreach (DataRow dr in dataSet_HR_Core.sp_HR_00217_Employee_PayRises_Select.Rows)
                {
                    strAmount = dr["PreviousPayAmount"].ToString();
                    if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["PreviousPayAmount"] = Encryptor.Decrypt(strAmount);
                    strAmount = dr["PayRiseAmount"].ToString();
                    if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["PayRiseAmount"] = Encryptor.Decrypt(strAmount);
                    strAmount = dr["PayRisePercentage"].ToString();
                    if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["PayRisePercentage"] = Encryptor.Decrypt(strAmount);
                    strAmount = dr["NewPayAmount"].ToString();
                    if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["NewPayAmount"] = Encryptor.Decrypt(strAmount);
                }
            }
            catch (Exception ex) { }
            
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Employee Bonuses Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }


        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (_Mode != "single")
            {
                strSelectedIDs = "";    // Reset any prior values first //
                strSelectedDescription = "";  // Reset any prior values first //
                string strDescriptor = "";
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "PayRiseID")) + ",";
                        strDescriptor = Convert.ToString(view.GetRowCellValue(i, "EmployeeName")) + " - " + string.Format("dd/MM/yyyy HH:mm", Convert.ToString(view.GetRowCellValue(i, "PayRiseDate")));
                        
                        if (intCount == 0)
                        {
                            strSelectedDescription = strDescriptor;
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedDescription += ", " + strDescriptor;
                        }
                        intCount++;
                    }
                }
                _SelectedCount = intCount;
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    var currentRowView = (DataRowView)spHR00217EmployeePayRisesSelectBindingSource.Current;
                    var currentRow = (DataSet_HR_Core.sp_HR_00217_Employee_PayRises_SelectRow)currentRowView.Row;
                    if (currentRow == null) return;
                    intSelectedID = (string.IsNullOrEmpty(currentRow.PayRiseID.ToString()) ? 0 : currentRow.PayRiseID);
                    strSelectedDescription = (string.IsNullOrEmpty(currentRow.EmployeeName.ToString()) ? "" : currentRow.EmployeeName) + " - " + (string.IsNullOrEmpty(currentRow.PayRiseDate.ToString()) ? "" : currentRow.PayRiseDate.ToString("dd/MM/yyyy HH:mm"));
                }
            }
        }

    }
}


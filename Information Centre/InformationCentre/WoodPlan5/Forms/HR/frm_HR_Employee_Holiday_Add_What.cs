﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Holiday_Add_What : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        public string strSelectedType = "None";
        public string strMode = "Add";
        #endregion

        public frm_HR_Employee_Holiday_Add_What()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Holiday_Add_What_Load(object sender, EventArgs e)
        {
            this.FormID = 990105;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.Text = String.Format("Employee Holiday - {0} What?", strMode);
            labelControlInformation.Text = String.Format("<b>{0}</b> Employee Holidays - What would you like to <b>{0}</b>?", strMode);
        }

        private void btnHoliday_Click(object sender, EventArgs e)
        {
            strSelectedType = "Holiday";
            this.Close();        
        }

        private void btnHolidayYear_Click(object sender, EventArgs e)
        {
            strSelectedType = "HolidayYear";
            this.Close();
        }

    }
}

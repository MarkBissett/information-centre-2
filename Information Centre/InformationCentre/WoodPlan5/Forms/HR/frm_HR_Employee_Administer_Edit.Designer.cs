namespace WoodPlan5
{
    partial class frm_HR_Employee_Administer_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Administer_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.spHR00250EmployeeAdministerEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.DateToDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.DateFromDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.AdministratorEmployeeNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AdministratorEmployeeFirstnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AdministratorEmployeeSurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AdministratorIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AdministratorEmployeeNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.EmployeeNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeFirstnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeAdministratorIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.EmployeeNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.EmployeeSurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TypeIdGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00252AdministerTypeListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForEmployeeAdministratorId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeFirstname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemforAdministratorId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAdministratorEmployeeSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAdministratorEmployeeFirstname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAdministratorEmployeeNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAdministratorEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDateTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTypeId = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00250_Employee_Administer_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00250_Employee_Administer_EditTableAdapter();
            this.sp_HR_00252_Administer_Type_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00252_Administer_Type_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00250EmployeeAdministerEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateToDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateToDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFromDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFromDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorEmployeeNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorEmployeeFirstnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorEmployeeSurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorEmployeeNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeAdministratorIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeIdGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00252AdministerTypeListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeAdministratorId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforAdministratorId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdministratorEmployeeSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdministratorEmployeeFirstname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdministratorEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdministratorEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 478);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 478);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 452);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DateToDateEdit);
            this.dataLayoutControl1.Controls.Add(this.DateFromDateEdit);
            this.dataLayoutControl1.Controls.Add(this.AdministratorEmployeeNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AdministratorEmployeeFirstnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AdministratorEmployeeSurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AdministratorIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AdministratorEmployeeNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeFirstnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeAdministratorIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeSurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TypeIdGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.spHR00250EmployeeAdministerEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmployeeAdministratorId,
            this.ItemForEmployeeId,
            this.ItemForEmployeeFirstname,
            this.ItemForEmployeeNumber,
            this.ItemForEmployeeSurname,
            this.ItemforAdministratorId,
            this.ItemForAdministratorEmployeeSurname,
            this.ItemForAdministratorEmployeeFirstname,
            this.ItemForAdministratorEmployeeNumber,
            this.layoutControlGroup3});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1255, 163, 365, 551);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 452);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "Active", true));
            this.ActiveCheckEdit.Location = new System.Drawing.Point(130, 175);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "[Tick if Yes]";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(136, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 55;
            // 
            // spHR00250EmployeeAdministerEditBindingSource
            // 
            this.spHR00250EmployeeAdministerEditBindingSource.DataMember = "sp_HR_00250_Employee_Administer_Edit";
            this.spHR00250EmployeeAdministerEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DateToDateEdit
            // 
            this.DateToDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "DateTo", true));
            this.DateToDateEdit.EditValue = null;
            this.DateToDateEdit.Location = new System.Drawing.Point(130, 151);
            this.DateToDateEdit.MenuManager = this.barManager1;
            this.DateToDateEdit.Name = "DateToDateEdit";
            this.DateToDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateToDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateToDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateToDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateToDateEdit.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.DateToDateEdit.Size = new System.Drawing.Size(136, 20);
            this.DateToDateEdit.StyleController = this.dataLayoutControl1;
            this.DateToDateEdit.TabIndex = 54;
            // 
            // DateFromDateEdit
            // 
            this.DateFromDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "DateFrom", true));
            this.DateFromDateEdit.EditValue = null;
            this.DateFromDateEdit.Location = new System.Drawing.Point(130, 127);
            this.DateFromDateEdit.MenuManager = this.barManager1;
            this.DateFromDateEdit.Name = "DateFromDateEdit";
            this.DateFromDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DateFromDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateFromDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateFromDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateFromDateEdit.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.DateFromDateEdit.Size = new System.Drawing.Size(136, 20);
            this.DateFromDateEdit.StyleController = this.dataLayoutControl1;
            this.DateFromDateEdit.TabIndex = 53;
            this.DateFromDateEdit.EditValueChanged += new System.EventHandler(this.DateFromDateEdit_EditValueChanged);
            this.DateFromDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DateFromDateEdit_Validating);
            // 
            // AdministratorEmployeeNumberTextEdit
            // 
            this.AdministratorEmployeeNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "AdministratorEmployeeNumber", true));
            this.AdministratorEmployeeNumberTextEdit.Location = new System.Drawing.Point(130, 225);
            this.AdministratorEmployeeNumberTextEdit.MenuManager = this.barManager1;
            this.AdministratorEmployeeNumberTextEdit.Name = "AdministratorEmployeeNumberTextEdit";
            this.AdministratorEmployeeNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AdministratorEmployeeNumberTextEdit, true);
            this.AdministratorEmployeeNumberTextEdit.Size = new System.Drawing.Size(469, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AdministratorEmployeeNumberTextEdit, optionsSpelling1);
            this.AdministratorEmployeeNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.AdministratorEmployeeNumberTextEdit.TabIndex = 52;
            // 
            // AdministratorEmployeeFirstnameTextEdit
            // 
            this.AdministratorEmployeeFirstnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "EmployeeFirstname", true));
            this.AdministratorEmployeeFirstnameTextEdit.Location = new System.Drawing.Point(134, 225);
            this.AdministratorEmployeeFirstnameTextEdit.MenuManager = this.barManager1;
            this.AdministratorEmployeeFirstnameTextEdit.Name = "AdministratorEmployeeFirstnameTextEdit";
            this.AdministratorEmployeeFirstnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AdministratorEmployeeFirstnameTextEdit, true);
            this.AdministratorEmployeeFirstnameTextEdit.Size = new System.Drawing.Size(465, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AdministratorEmployeeFirstnameTextEdit, optionsSpelling2);
            this.AdministratorEmployeeFirstnameTextEdit.StyleController = this.dataLayoutControl1;
            this.AdministratorEmployeeFirstnameTextEdit.TabIndex = 51;
            // 
            // AdministratorEmployeeSurnameTextEdit
            // 
            this.AdministratorEmployeeSurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "AdministratorEmployeeSurname", true));
            this.AdministratorEmployeeSurnameTextEdit.Location = new System.Drawing.Point(130, 225);
            this.AdministratorEmployeeSurnameTextEdit.MenuManager = this.barManager1;
            this.AdministratorEmployeeSurnameTextEdit.Name = "AdministratorEmployeeSurnameTextEdit";
            this.AdministratorEmployeeSurnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AdministratorEmployeeSurnameTextEdit, true);
            this.AdministratorEmployeeSurnameTextEdit.Size = new System.Drawing.Size(469, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AdministratorEmployeeSurnameTextEdit, optionsSpelling3);
            this.AdministratorEmployeeSurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.AdministratorEmployeeSurnameTextEdit.TabIndex = 50;
            // 
            // AdministratorIdTextEdit
            // 
            this.AdministratorIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "AdministratorId", true));
            this.AdministratorIdTextEdit.Location = new System.Drawing.Point(130, 93);
            this.AdministratorIdTextEdit.MenuManager = this.barManager1;
            this.AdministratorIdTextEdit.Name = "AdministratorIdTextEdit";
            this.AdministratorIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AdministratorIdTextEdit, true);
            this.AdministratorIdTextEdit.Size = new System.Drawing.Size(469, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AdministratorIdTextEdit, optionsSpelling4);
            this.AdministratorIdTextEdit.StyleController = this.dataLayoutControl1;
            this.AdministratorIdTextEdit.TabIndex = 49;
            // 
            // AdministratorEmployeeNameButtonEdit
            // 
            this.AdministratorEmployeeNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "AdministratorEmployeeName", true));
            this.AdministratorEmployeeNameButtonEdit.EditValue = "";
            this.AdministratorEmployeeNameButtonEdit.Location = new System.Drawing.Point(130, 35);
            this.AdministratorEmployeeNameButtonEdit.MenuManager = this.barManager1;
            this.AdministratorEmployeeNameButtonEdit.Name = "AdministratorEmployeeNameButtonEdit";
            this.AdministratorEmployeeNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to open Select Employee screen", "choose", null, true)});
            this.AdministratorEmployeeNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AdministratorEmployeeNameButtonEdit.Size = new System.Drawing.Size(486, 20);
            this.AdministratorEmployeeNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.AdministratorEmployeeNameButtonEdit.TabIndex = 13;
            this.AdministratorEmployeeNameButtonEdit.TabStop = false;
            this.AdministratorEmployeeNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AdministratorEmployeeNameButtonEdit_ButtonClick);
            this.AdministratorEmployeeNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AdministratorEmployeeNameButtonEdite_Validating);
            // 
            // EmployeeNumberTextEdit
            // 
            this.EmployeeNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "EmployeeNumber", true));
            this.EmployeeNumberTextEdit.Location = new System.Drawing.Point(111, 201);
            this.EmployeeNumberTextEdit.MenuManager = this.barManager1;
            this.EmployeeNumberTextEdit.Name = "EmployeeNumberTextEdit";
            this.EmployeeNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNumberTextEdit, true);
            this.EmployeeNumberTextEdit.Size = new System.Drawing.Size(488, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNumberTextEdit, optionsSpelling5);
            this.EmployeeNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNumberTextEdit.TabIndex = 48;
            // 
            // EmployeeIdTextEdit
            // 
            this.EmployeeIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "EmployeeId", true));
            this.EmployeeIdTextEdit.Location = new System.Drawing.Point(116, 225);
            this.EmployeeIdTextEdit.MenuManager = this.barManager1;
            this.EmployeeIdTextEdit.Name = "EmployeeIdTextEdit";
            this.EmployeeIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIdTextEdit, true);
            this.EmployeeIdTextEdit.Size = new System.Drawing.Size(483, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIdTextEdit, optionsSpelling6);
            this.EmployeeIdTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIdTextEdit.TabIndex = 28;
            // 
            // EmployeeFirstnameTextEdit
            // 
            this.EmployeeFirstnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "EmployeeFirstname", true));
            this.EmployeeFirstnameTextEdit.Location = new System.Drawing.Point(116, 201);
            this.EmployeeFirstnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeFirstnameTextEdit.Name = "EmployeeFirstnameTextEdit";
            this.EmployeeFirstnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeFirstnameTextEdit, true);
            this.EmployeeFirstnameTextEdit.Size = new System.Drawing.Size(483, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeFirstnameTextEdit, optionsSpelling7);
            this.EmployeeFirstnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeFirstnameTextEdit.TabIndex = 27;
            // 
            // EmployeeAdministratorIdTextEdit
            // 
            this.EmployeeAdministratorIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "EmployeeAdministratorId", true));
            this.EmployeeAdministratorIdTextEdit.Location = new System.Drawing.Point(146, 297);
            this.EmployeeAdministratorIdTextEdit.MenuManager = this.barManager1;
            this.EmployeeAdministratorIdTextEdit.Name = "EmployeeAdministratorIdTextEdit";
            this.EmployeeAdministratorIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeAdministratorIdTextEdit, true);
            this.EmployeeAdministratorIdTextEdit.Size = new System.Drawing.Size(453, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeAdministratorIdTextEdit, optionsSpelling8);
            this.EmployeeAdministratorIdTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeAdministratorIdTextEdit.TabIndex = 25;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00250EmployeeAdministerEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(130, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // EmployeeNameButtonEdit
            // 
            this.EmployeeNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "EmployeeName", true));
            this.EmployeeNameButtonEdit.EditValue = "";
            this.EmployeeNameButtonEdit.Location = new System.Drawing.Point(130, 59);
            this.EmployeeNameButtonEdit.MenuManager = this.barManager1;
            this.EmployeeNameButtonEdit.Name = "EmployeeNameButtonEdit";
            this.EmployeeNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click to open Select Employee screen", "choose", null, true)});
            this.EmployeeNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EmployeeNameButtonEdit.Size = new System.Drawing.Size(486, 20);
            this.EmployeeNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameButtonEdit.TabIndex = 5;
            this.EmployeeNameButtonEdit.TabStop = false;
            this.EmployeeNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EmployeeNameButtonEdit_ButtonClick);
            this.EmployeeNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EmployeeNameButtonEdit_Validating);
            // 
            // EmployeeSurnameTextEdit
            // 
            this.EmployeeSurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "EmployeeSurname", true));
            this.EmployeeSurnameTextEdit.Location = new System.Drawing.Point(111, 201);
            this.EmployeeSurnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeSurnameTextEdit.Name = "EmployeeSurnameTextEdit";
            this.EmployeeSurnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeSurnameTextEdit, true);
            this.EmployeeSurnameTextEdit.Size = new System.Drawing.Size(488, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeSurnameTextEdit, optionsSpelling9);
            this.EmployeeSurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeSurnameTextEdit.TabIndex = 30;
            // 
            // TypeIdGridLookUpEdit
            // 
            this.TypeIdGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00250EmployeeAdministerEditBindingSource, "TypeId", true));
            this.TypeIdGridLookUpEdit.Location = new System.Drawing.Point(130, 93);
            this.TypeIdGridLookUpEdit.MenuManager = this.barManager1;
            this.TypeIdGridLookUpEdit.Name = "TypeIdGridLookUpEdit";
            this.TypeIdGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.DropDown)});
            this.TypeIdGridLookUpEdit.Properties.DataSource = this.spHR00252AdministerTypeListWithBlankBindingSource;
            this.TypeIdGridLookUpEdit.Properties.DisplayMember = "Description";
            this.TypeIdGridLookUpEdit.Properties.NullText = "";
            this.TypeIdGridLookUpEdit.Properties.ValueMember = "ID";
            this.TypeIdGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.TypeIdGridLookUpEdit.Size = new System.Drawing.Size(486, 20);
            this.TypeIdGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.TypeIdGridLookUpEdit.TabIndex = 46;
            this.TypeIdGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TypeIdGridLookUpEdit_Validating);
            // 
            // spHR00252AdministerTypeListWithBlankBindingSource
            // 
            this.spHR00252AdministerTypeListWithBlankBindingSource.DataMember = "sp_HR_00252_Administer_Type_List_With_Blank";
            this.spHR00252AdministerTypeListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colID1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridLookUpEdit1View.FormatRules.Add(gridFormatRule1);
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Administering Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 223;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ItemForEmployeeAdministratorId
            // 
            this.ItemForEmployeeAdministratorId.Control = this.EmployeeAdministratorIdTextEdit;
            this.ItemForEmployeeAdministratorId.CustomizationFormText = "Employee Administrator ID:";
            this.ItemForEmployeeAdministratorId.Location = new System.Drawing.Point(0, 285);
            this.ItemForEmployeeAdministratorId.Name = "ItemForEmployeeAdministratorId";
            this.ItemForEmployeeAdministratorId.Size = new System.Drawing.Size(591, 24);
            this.ItemForEmployeeAdministratorId.Text = "Employee Administrator ID:";
            this.ItemForEmployeeAdministratorId.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeId
            // 
            this.ItemForEmployeeId.Control = this.EmployeeIdTextEdit;
            this.ItemForEmployeeId.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeId.Location = new System.Drawing.Point(0, 213);
            this.ItemForEmployeeId.Name = "ItemForEmployeeId";
            this.ItemForEmployeeId.Size = new System.Drawing.Size(591, 24);
            this.ItemForEmployeeId.Text = "Employee ID:";
            this.ItemForEmployeeId.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeFirstname
            // 
            this.ItemForEmployeeFirstname.Control = this.EmployeeFirstnameTextEdit;
            this.ItemForEmployeeFirstname.CustomizationFormText = "Employee Forename:";
            this.ItemForEmployeeFirstname.Location = new System.Drawing.Point(0, 189);
            this.ItemForEmployeeFirstname.Name = "ItemForEmployeeFirstname";
            this.ItemForEmployeeFirstname.Size = new System.Drawing.Size(591, 24);
            this.ItemForEmployeeFirstname.Text = "Employee Forename:";
            this.ItemForEmployeeFirstname.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemForEmployeeNumber
            // 
            this.ItemForEmployeeNumber.Control = this.EmployeeNumberTextEdit;
            this.ItemForEmployeeNumber.CustomizationFormText = "Employee Number:";
            this.ItemForEmployeeNumber.Location = new System.Drawing.Point(0, 189);
            this.ItemForEmployeeNumber.Name = "ItemForEmployeeNumber";
            this.ItemForEmployeeNumber.Size = new System.Drawing.Size(591, 24);
            this.ItemForEmployeeNumber.Text = "Employee Number:";
            this.ItemForEmployeeNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeSurname
            // 
            this.ItemForEmployeeSurname.Control = this.EmployeeSurnameTextEdit;
            this.ItemForEmployeeSurname.CustomizationFormText = "Employee Surname:";
            this.ItemForEmployeeSurname.Location = new System.Drawing.Point(0, 189);
            this.ItemForEmployeeSurname.Name = "ItemForEmployeeSurname";
            this.ItemForEmployeeSurname.Size = new System.Drawing.Size(591, 24);
            this.ItemForEmployeeSurname.Text = "Employee Surname:";
            this.ItemForEmployeeSurname.TextSize = new System.Drawing.Size(96, 13);
            // 
            // ItemforAdministratorId
            // 
            this.ItemforAdministratorId.Control = this.AdministratorIdTextEdit;
            this.ItemforAdministratorId.Location = new System.Drawing.Point(0, 81);
            this.ItemforAdministratorId.Name = "ItemforAdministratorId";
            this.ItemforAdministratorId.Size = new System.Drawing.Size(591, 24);
            this.ItemforAdministratorId.Text = "Administrator ID:";
            this.ItemforAdministratorId.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAdministratorEmployeeSurname
            // 
            this.ItemForAdministratorEmployeeSurname.Control = this.AdministratorEmployeeSurnameTextEdit;
            this.ItemForAdministratorEmployeeSurname.Location = new System.Drawing.Point(0, 213);
            this.ItemForAdministratorEmployeeSurname.Name = "ItemForAdministratorEmployeeSurname";
            this.ItemForAdministratorEmployeeSurname.Size = new System.Drawing.Size(591, 24);
            this.ItemForAdministratorEmployeeSurname.Text = "Administrator Surname:";
            this.ItemForAdministratorEmployeeSurname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAdministratorEmployeeFirstname
            // 
            this.ItemForAdministratorEmployeeFirstname.Control = this.AdministratorEmployeeFirstnameTextEdit;
            this.ItemForAdministratorEmployeeFirstname.Location = new System.Drawing.Point(0, 213);
            this.ItemForAdministratorEmployeeFirstname.Name = "ItemForAdministratorEmployeeFirstname";
            this.ItemForAdministratorEmployeeFirstname.Size = new System.Drawing.Size(591, 24);
            this.ItemForAdministratorEmployeeFirstname.Text = "Administrator Forename:";
            this.ItemForAdministratorEmployeeFirstname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAdministratorEmployeeNumber
            // 
            this.ItemForAdministratorEmployeeNumber.Control = this.AdministratorEmployeeNumberTextEdit;
            this.ItemForAdministratorEmployeeNumber.Location = new System.Drawing.Point(0, 213);
            this.ItemForAdministratorEmployeeNumber.Name = "ItemForAdministratorEmployeeNumber";
            this.ItemForAdministratorEmployeeNumber.Size = new System.Drawing.Size(591, 24);
            this.ItemForAdministratorEmployeeNumber.Text = "Administrator Number:";
            this.ItemForAdministratorEmployeeNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 384);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(608, 48);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 452);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmployeeName,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem6,
            this.emptySpaceItem3,
            this.ItemForAdministratorEmployeeName,
            this.ItemForDateFrom,
            this.emptySpaceItem5,
            this.ItemForDateTo,
            this.emptySpaceItem7,
            this.ItemForActive,
            this.ItemForTypeId,
            this.emptySpaceItem4,
            this.emptySpaceItem8});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 432);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.AllowHide = false;
            this.ItemForEmployeeName.Control = this.EmployeeNameButtonEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Employee Administered:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 47);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(608, 24);
            this.ItemForEmployeeName.Text = "Employee Administered:";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(118, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(118, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(118, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(295, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(313, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(118, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 71);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 186);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(608, 246);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAdministratorEmployeeName
            // 
            this.ItemForAdministratorEmployeeName.Control = this.AdministratorEmployeeNameButtonEdit;
            this.ItemForAdministratorEmployeeName.Location = new System.Drawing.Point(0, 23);
            this.ItemForAdministratorEmployeeName.Name = "ItemForAdministratorEmployeeName";
            this.ItemForAdministratorEmployeeName.Size = new System.Drawing.Size(608, 24);
            this.ItemForAdministratorEmployeeName.Text = "Administrator:";
            this.ItemForAdministratorEmployeeName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForDateFrom
            // 
            this.ItemForDateFrom.Control = this.DateFromDateEdit;
            this.ItemForDateFrom.Location = new System.Drawing.Point(0, 115);
            this.ItemForDateFrom.Name = "ItemForDateFrom";
            this.ItemForDateFrom.Size = new System.Drawing.Size(258, 24);
            this.ItemForDateFrom.Text = "Date From:";
            this.ItemForDateFrom.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(258, 115);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(350, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDateTo
            // 
            this.ItemForDateTo.Control = this.DateToDateEdit;
            this.ItemForDateTo.Location = new System.Drawing.Point(0, 139);
            this.ItemForDateTo.Name = "ItemForDateTo";
            this.ItemForDateTo.Size = new System.Drawing.Size(258, 24);
            this.ItemForDateTo.Text = "Date To:";
            this.ItemForDateTo.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(258, 139);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(350, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForActive
            // 
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.Location = new System.Drawing.Point(0, 163);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(258, 23);
            this.ItemForActive.Text = "Active:";
            this.ItemForActive.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForTypeId
            // 
            this.ItemForTypeId.Control = this.TypeIdGridLookUpEdit;
            this.ItemForTypeId.CustomizationFormText = "Administer Type:";
            this.ItemForTypeId.Location = new System.Drawing.Point(0, 81);
            this.ItemForTypeId.Name = "ItemForTypeId";
            this.ItemForTypeId.Size = new System.Drawing.Size(608, 24);
            this.ItemForTypeId.Text = "Administer Type:";
            this.ItemForTypeId.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 105);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(258, 163);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(350, 23);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00250_Employee_Administer_EditTableAdapter
            // 
            this.sp_HR_00250_Employee_Administer_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00252_Administer_Type_List_With_BlankTableAdapter
            // 
            this.sp_HR_00252_Administer_Type_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Employee_Administer_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 508);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Administer_Edit";
            this.Text = "Edit Employee Administer";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Administer_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Administer_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Administer_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00250EmployeeAdministerEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateToDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateToDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFromDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateFromDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorEmployeeNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorEmployeeFirstnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorEmployeeSurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AdministratorEmployeeNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeAdministratorIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TypeIdGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00252AdministerTypeListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeAdministratorId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemforAdministratorId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdministratorEmployeeSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdministratorEmployeeFirstname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdministratorEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAdministratorEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.ButtonEdit EmployeeNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeAdministratorIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeAdministratorId;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.TextEdit EmployeeFirstnameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeFirstname;
        private DevExpress.XtraEditors.TextEdit EmployeeIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeId;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.TextEdit EmployeeSurnameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeSurname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTypeId;
        private DevExpress.XtraEditors.TextEdit EmployeeNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.GridLookUpEdit TypeIdGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private System.Windows.Forms.BindingSource spHR00250EmployeeAdministerEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00250_Employee_Administer_EditTableAdapter sp_HR_00250_Employee_Administer_EditTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit AdministratorEmployeeNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAdministratorEmployeeName;
        private DevExpress.XtraEditors.TextEdit AdministratorIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemforAdministratorId;
        private DevExpress.XtraEditors.TextEdit AdministratorEmployeeFirstnameTextEdit;
        private DevExpress.XtraEditors.TextEdit AdministratorEmployeeSurnameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAdministratorEmployeeSurname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAdministratorEmployeeFirstname;
        private DevExpress.XtraEditors.TextEdit AdministratorEmployeeNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAdministratorEmployeeNumber;
        private DevExpress.XtraEditors.DateEdit DateFromDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateFrom;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.DateEdit DateToDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateTo;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private System.Windows.Forms.BindingSource spHR00252AdministerTypeListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00252_Administer_Type_List_With_BlankTableAdapter sp_HR_00252_Administer_Type_List_With_BlankTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
    }
}

namespace WoodPlan5
{
    partial class frm_HR_Select_Person_Training
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp09114HRQualificationSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQualificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationFromID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostToCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSummerMaintenance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWinterMaintenance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityRail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysUntilExpiry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colArchived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssessmentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp09114_HR_Qualification_SelectTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp09114_HR_Qualification_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09114HRQualificationSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp09114HRQualificationSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditNumericDays,
            this.repositoryItemMemoExEdit2});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp09114HRQualificationSelectBindingSource
            // 
            this.sp09114HRQualificationSelectBindingSource.DataMember = "sp09114_HR_Qualification_Select";
            this.sp09114HRQualificationSelectBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQualificationID,
            this.colLinkedToPersonID,
            this.colLinkedToPersonTypeID,
            this.colQualificationTypeID,
            this.colQualificationFromID,
            this.colCourseNumber,
            this.colCourseName,
            this.colCostToCompany,
            this.colStartDate,
            this.colEndDate,
            this.colExpiryDate,
            this.colRemarks1,
            this.colSummerMaintenance,
            this.colWinterMaintenance,
            this.colUtilityArb,
            this.colUtilityRail,
            this.colWoodPlan,
            this.colGUID,
            this.colLinkedToPersonType,
            this.colLinkedToPersonName,
            this.colQualificationType,
            this.colQualificationFrom,
            this.colDaysUntilExpiry,
            this.colArchived,
            this.colAssessmentDate,
            this.colCourseEndDate,
            this.colCourseStartDate,
            this.colQualificationSubType,
            this.colQualificationSubTypeID});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDaysUntilExpiry, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colQualificationID
            // 
            this.colQualificationID.Caption = "Qualification ID";
            this.colQualificationID.FieldName = "QualificationID";
            this.colQualificationID.Name = "colQualificationID";
            this.colQualificationID.OptionsColumn.AllowEdit = false;
            this.colQualificationID.OptionsColumn.AllowFocus = false;
            this.colQualificationID.OptionsColumn.ReadOnly = true;
            this.colQualificationID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQualificationID.Width = 94;
            // 
            // colLinkedToPersonID
            // 
            this.colLinkedToPersonID.Caption = "Linked To Person ID";
            this.colLinkedToPersonID.FieldName = "LinkedToPersonID";
            this.colLinkedToPersonID.Name = "colLinkedToPersonID";
            this.colLinkedToPersonID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToPersonID.Width = 116;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Linked To Person Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToPersonTypeID.Width = 143;
            // 
            // colQualificationTypeID
            // 
            this.colQualificationTypeID.Caption = "Qualification Type ID";
            this.colQualificationTypeID.FieldName = "QualificationTypeID";
            this.colQualificationTypeID.Name = "colQualificationTypeID";
            this.colQualificationTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQualificationTypeID.Width = 121;
            // 
            // colQualificationFromID
            // 
            this.colQualificationFromID.Caption = "Qualification From ID";
            this.colQualificationFromID.FieldName = "QualificationFromID";
            this.colQualificationFromID.Name = "colQualificationFromID";
            this.colQualificationFromID.OptionsColumn.AllowEdit = false;
            this.colQualificationFromID.OptionsColumn.AllowFocus = false;
            this.colQualificationFromID.OptionsColumn.ReadOnly = true;
            this.colQualificationFromID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQualificationFromID.Width = 121;
            // 
            // colCourseNumber
            // 
            this.colCourseNumber.Caption = "Course #";
            this.colCourseNumber.FieldName = "CourseNumber";
            this.colCourseNumber.Name = "colCourseNumber";
            this.colCourseNumber.OptionsColumn.AllowEdit = false;
            this.colCourseNumber.OptionsColumn.AllowFocus = false;
            this.colCourseNumber.OptionsColumn.ReadOnly = true;
            this.colCourseNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCourseNumber.Visible = true;
            this.colCourseNumber.VisibleIndex = 12;
            // 
            // colCourseName
            // 
            this.colCourseName.Caption = "Course Name";
            this.colCourseName.FieldName = "CourseName";
            this.colCourseName.Name = "colCourseName";
            this.colCourseName.OptionsColumn.AllowEdit = false;
            this.colCourseName.OptionsColumn.AllowFocus = false;
            this.colCourseName.OptionsColumn.ReadOnly = true;
            this.colCourseName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCourseName.Visible = true;
            this.colCourseName.VisibleIndex = 11;
            this.colCourseName.Width = 120;
            // 
            // colCostToCompany
            // 
            this.colCostToCompany.Caption = "Cost to Company";
            this.colCostToCompany.FieldName = "CostToCompany";
            this.colCostToCompany.Name = "colCostToCompany";
            this.colCostToCompany.OptionsColumn.AllowEdit = false;
            this.colCostToCompany.OptionsColumn.AllowFocus = false;
            this.colCostToCompany.OptionsColumn.ReadOnly = true;
            this.colCostToCompany.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCostToCompany.Visible = true;
            this.colCostToCompany.VisibleIndex = 14;
            this.colCostToCompany.Width = 104;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Qualification Start";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 6;
            this.colStartDate.Width = 107;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "Qualification End";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 7;
            this.colEndDate.Width = 101;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.Caption = "Qualification Expiry";
            this.colExpiryDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 5;
            this.colExpiryDate.Width = 113;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colSummerMaintenance
            // 
            this.colSummerMaintenance.Caption = "Summer Maintenance";
            this.colSummerMaintenance.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSummerMaintenance.FieldName = "SummerMaintenance";
            this.colSummerMaintenance.Name = "colSummerMaintenance";
            this.colSummerMaintenance.OptionsColumn.AllowEdit = false;
            this.colSummerMaintenance.OptionsColumn.AllowFocus = false;
            this.colSummerMaintenance.OptionsColumn.ReadOnly = true;
            this.colSummerMaintenance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSummerMaintenance.Visible = true;
            this.colSummerMaintenance.VisibleIndex = 16;
            this.colSummerMaintenance.Width = 123;
            // 
            // colWinterMaintenance
            // 
            this.colWinterMaintenance.Caption = "Winter Maintenance";
            this.colWinterMaintenance.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colWinterMaintenance.FieldName = "WinterMaintenance";
            this.colWinterMaintenance.Name = "colWinterMaintenance";
            this.colWinterMaintenance.OptionsColumn.AllowEdit = false;
            this.colWinterMaintenance.OptionsColumn.AllowFocus = false;
            this.colWinterMaintenance.OptionsColumn.ReadOnly = true;
            this.colWinterMaintenance.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWinterMaintenance.Visible = true;
            this.colWinterMaintenance.VisibleIndex = 17;
            this.colWinterMaintenance.Width = 117;
            // 
            // colUtilityArb
            // 
            this.colUtilityArb.Caption = "Utility ARB";
            this.colUtilityArb.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colUtilityArb.FieldName = "UtilityArb";
            this.colUtilityArb.Name = "colUtilityArb";
            this.colUtilityArb.OptionsColumn.AllowEdit = false;
            this.colUtilityArb.OptionsColumn.AllowFocus = false;
            this.colUtilityArb.OptionsColumn.ReadOnly = true;
            this.colUtilityArb.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUtilityArb.Visible = true;
            this.colUtilityArb.VisibleIndex = 18;
            // 
            // colUtilityRail
            // 
            this.colUtilityRail.Caption = "Utility Rail";
            this.colUtilityRail.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colUtilityRail.FieldName = "UtilityRail";
            this.colUtilityRail.Name = "colUtilityRail";
            this.colUtilityRail.OptionsColumn.AllowEdit = false;
            this.colUtilityRail.OptionsColumn.AllowFocus = false;
            this.colUtilityRail.OptionsColumn.ReadOnly = true;
            this.colUtilityRail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colUtilityRail.Visible = true;
            this.colUtilityRail.VisibleIndex = 19;
            // 
            // colWoodPlan
            // 
            this.colWoodPlan.Caption = "WoodPlan";
            this.colWoodPlan.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colWoodPlan.FieldName = "WoodPlan";
            this.colWoodPlan.Name = "colWoodPlan";
            this.colWoodPlan.OptionsColumn.AllowEdit = false;
            this.colWoodPlan.OptionsColumn.AllowFocus = false;
            this.colWoodPlan.OptionsColumn.ReadOnly = true;
            this.colWoodPlan.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWoodPlan.Visible = true;
            this.colWoodPlan.VisibleIndex = 20;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            this.colGUID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Owner Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 0;
            this.colLinkedToPersonType.Width = 102;
            // 
            // colLinkedToPersonName
            // 
            this.colLinkedToPersonName.Caption = "Owner";
            this.colLinkedToPersonName.FieldName = "LinkedToPersonName";
            this.colLinkedToPersonName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonName.Name = "colLinkedToPersonName";
            this.colLinkedToPersonName.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonName.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonName.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToPersonName.Visible = true;
            this.colLinkedToPersonName.VisibleIndex = 1;
            this.colLinkedToPersonName.Width = 151;
            // 
            // colQualificationType
            // 
            this.colQualificationType.Caption = "Qualification Type";
            this.colQualificationType.FieldName = "QualificationType";
            this.colQualificationType.Name = "colQualificationType";
            this.colQualificationType.OptionsColumn.AllowEdit = false;
            this.colQualificationType.OptionsColumn.AllowFocus = false;
            this.colQualificationType.OptionsColumn.ReadOnly = true;
            this.colQualificationType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQualificationType.Visible = true;
            this.colQualificationType.VisibleIndex = 2;
            this.colQualificationType.Width = 133;
            // 
            // colQualificationFrom
            // 
            this.colQualificationFrom.Caption = "Qualification Source";
            this.colQualificationFrom.FieldName = "QualificationFrom";
            this.colQualificationFrom.Name = "colQualificationFrom";
            this.colQualificationFrom.OptionsColumn.AllowEdit = false;
            this.colQualificationFrom.OptionsColumn.AllowFocus = false;
            this.colQualificationFrom.OptionsColumn.ReadOnly = true;
            this.colQualificationFrom.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQualificationFrom.Visible = true;
            this.colQualificationFrom.VisibleIndex = 13;
            this.colQualificationFrom.Width = 147;
            // 
            // colDaysUntilExpiry
            // 
            this.colDaysUntilExpiry.Caption = "Expires In";
            this.colDaysUntilExpiry.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colDaysUntilExpiry.FieldName = "DaysUntilExpiry";
            this.colDaysUntilExpiry.Name = "colDaysUntilExpiry";
            this.colDaysUntilExpiry.OptionsColumn.AllowEdit = false;
            this.colDaysUntilExpiry.OptionsColumn.AllowFocus = false;
            this.colDaysUntilExpiry.OptionsColumn.ReadOnly = true;
            this.colDaysUntilExpiry.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDaysUntilExpiry.Visible = true;
            this.colDaysUntilExpiry.VisibleIndex = 4;
            this.colDaysUntilExpiry.Width = 82;
            // 
            // repositoryItemTextEditNumericDays
            // 
            this.repositoryItemTextEditNumericDays.AutoHeight = false;
            this.repositoryItemTextEditNumericDays.Mask.EditMask = "######0 Days";
            this.repositoryItemTextEditNumericDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericDays.Name = "repositoryItemTextEditNumericDays";
            // 
            // colArchived
            // 
            this.colArchived.Caption = "Archived";
            this.colArchived.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colArchived.FieldName = "Archived";
            this.colArchived.Name = "colArchived";
            this.colArchived.OptionsColumn.AllowEdit = false;
            this.colArchived.OptionsColumn.AllowFocus = false;
            this.colArchived.OptionsColumn.ReadOnly = true;
            this.colArchived.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colArchived.Visible = true;
            this.colArchived.VisibleIndex = 21;
            // 
            // colAssessmentDate
            // 
            this.colAssessmentDate.Caption = "Assessment Date";
            this.colAssessmentDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colAssessmentDate.FieldName = "AssessmentDate";
            this.colAssessmentDate.Name = "colAssessmentDate";
            this.colAssessmentDate.OptionsColumn.AllowEdit = false;
            this.colAssessmentDate.OptionsColumn.AllowFocus = false;
            this.colAssessmentDate.OptionsColumn.ReadOnly = true;
            this.colAssessmentDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAssessmentDate.Visible = true;
            this.colAssessmentDate.VisibleIndex = 10;
            this.colAssessmentDate.Width = 104;
            // 
            // colCourseEndDate
            // 
            this.colCourseEndDate.Caption = "Course End";
            this.colCourseEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCourseEndDate.FieldName = "CourseEndDate";
            this.colCourseEndDate.Name = "colCourseEndDate";
            this.colCourseEndDate.OptionsColumn.AllowEdit = false;
            this.colCourseEndDate.OptionsColumn.AllowFocus = false;
            this.colCourseEndDate.OptionsColumn.ReadOnly = true;
            this.colCourseEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCourseEndDate.Visible = true;
            this.colCourseEndDate.VisibleIndex = 9;
            this.colCourseEndDate.Width = 100;
            // 
            // colCourseStartDate
            // 
            this.colCourseStartDate.Caption = "Course Start";
            this.colCourseStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colCourseStartDate.FieldName = "CourseStartDate";
            this.colCourseStartDate.Name = "colCourseStartDate";
            this.colCourseStartDate.OptionsColumn.AllowEdit = false;
            this.colCourseStartDate.OptionsColumn.AllowFocus = false;
            this.colCourseStartDate.OptionsColumn.ReadOnly = true;
            this.colCourseStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCourseStartDate.Visible = true;
            this.colCourseStartDate.VisibleIndex = 8;
            this.colCourseStartDate.Width = 100;
            // 
            // colQualificationSubType
            // 
            this.colQualificationSubType.Caption = "Qualification Sub Type";
            this.colQualificationSubType.FieldName = "QualificationSubType";
            this.colQualificationSubType.Name = "colQualificationSubType";
            this.colQualificationSubType.OptionsColumn.AllowEdit = false;
            this.colQualificationSubType.OptionsColumn.AllowFocus = false;
            this.colQualificationSubType.OptionsColumn.ReadOnly = true;
            this.colQualificationSubType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQualificationSubType.Visible = true;
            this.colQualificationSubType.VisibleIndex = 3;
            this.colQualificationSubType.Width = 128;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.Caption = "Qualification Sub Type ID";
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQualificationSubTypeID.Width = 142;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp09114_HR_Qualification_SelectTableAdapter
            // 
            this.sp09114_HR_Qualification_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Person_Training
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Person_Training";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Training";
            this.Load += new System.EventHandler(this.frm_HR_Select_Person_Training_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09114HRQualificationSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource sp09114HRQualificationSelectBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationFromID;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostToCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colSummerMaintenance;
        private DevExpress.XtraGrid.Columns.GridColumn colWinterMaintenance;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArb;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityRail;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationType;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysUntilExpiry;
        private DataSet_HR_CoreTableAdapters.sp09114_HR_Qualification_SelectTableAdapter sp09114_HR_Qualification_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colArchived;
        private DevExpress.XtraGrid.Columns.GridColumn colAssessmentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
    }
}

namespace WoodPlan5
{
    partial class frm_HR_Master_Qualification_Types_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Master_Qualification_Types_Manager));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlVettingTypes = new DevExpress.XtraGrid.GridControl();
            this.spHR00230MasterQualifiactionTypesManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewVettingTypes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQualificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridControlVettingSubTypes = new DevExpress.XtraGrid.GridControl();
            this.spHR00231MasterQualificationTypesManagerLinkedSubTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewVettingSubTypes = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationStatusTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidityTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshIntervalDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryPeriodDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.spHR00142MasterVettingTypesManagerLinkedRestrictionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.beiShowActiveHolidayYearOnly = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.sp_HR_00142_Master_Vetting_Types_Manager_Linked_RestrictionsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00142_Master_Vetting_Types_Manager_Linked_RestrictionsTableAdapter();
            this.sp_HR_00230_Master_Qualifiaction_Types_ManagerTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00230_Master_Qualifiaction_Types_ManagerTableAdapter();
            this.sp_HR_00231_Master_Qualification_Types_Manager_Linked_SubTypesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00231_Master_Qualification_Types_Manager_Linked_SubTypesTableAdapter();
            this.colHideFromTotalviewDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVettingTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00230MasterQualifiactionTypesManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVettingTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVettingSubTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00231MasterQualificationTypesManagerLinkedSubTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVettingSubTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00142MasterVettingTypesManagerLinkedRestrictionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(769, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 438);
            this.barDockControlBottom.Size = new System.Drawing.Size(769, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 438);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(769, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 438);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemHyperLinkEdit6
            // 
            this.repositoryItemHyperLinkEdit6.AutoHeight = false;
            this.repositoryItemHyperLinkEdit6.Name = "repositoryItemHyperLinkEdit6";
            this.repositoryItemHyperLinkEdit6.SingleClick = true;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // repositoryItemDateEdit6
            // 
            this.repositoryItemDateEdit6.AutoHeight = false;
            this.repositoryItemDateEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit6.Name = "repositoryItemDateEdit6";
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ReadOnly = true;
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlVettingTypes);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Qualification Types ";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControlVettingSubTypes);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Qualification Sub-Types ";
            this.splitContainerControl1.Size = new System.Drawing.Size(769, 438);
            this.splitContainerControl1.SplitterPosition = 193;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControlVettingTypes
            // 
            this.gridControlVettingTypes.DataSource = this.spHR00230MasterQualifiactionTypesManagerBindingSource;
            this.gridControlVettingTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlVettingTypes.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlVettingTypes.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Refresh Data", "reload")});
            this.gridControlVettingTypes.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVettingTypes_EmbeddedNavigator_ButtonClick);
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.RelationName = "Level2";
            gridLevelNode3.RelationName = "Level3";
            this.gridControlVettingTypes.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3});
            this.gridControlVettingTypes.Location = new System.Drawing.Point(0, 0);
            this.gridControlVettingTypes.MainView = this.gridViewVettingTypes;
            this.gridControlVettingTypes.MenuManager = this.barManager1;
            this.gridControlVettingTypes.Name = "gridControlVettingTypes";
            this.gridControlVettingTypes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControlVettingTypes.Size = new System.Drawing.Size(744, 236);
            this.gridControlVettingTypes.TabIndex = 0;
            this.gridControlVettingTypes.UseEmbeddedNavigator = true;
            this.gridControlVettingTypes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVettingTypes});
            // 
            // spHR00230MasterQualifiactionTypesManagerBindingSource
            // 
            this.spHR00230MasterQualifiactionTypesManagerBindingSource.DataMember = "sp_HR_00230_Master_Qualifiaction_Types_Manager";
            this.spHR00230MasterQualifiactionTypesManagerBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridViewVettingTypes
            // 
            this.gridViewVettingTypes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQualificationTypeID,
            this.colDescription,
            this.colRecordOrder,
            this.colRemarks});
            this.gridViewVettingTypes.GridControl = this.gridControlVettingTypes;
            this.gridViewVettingTypes.Name = "gridViewVettingTypes";
            this.gridViewVettingTypes.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewVettingTypes.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewVettingTypes.OptionsLayout.StoreAppearance = true;
            this.gridViewVettingTypes.OptionsLayout.StoreFormatRules = true;
            this.gridViewVettingTypes.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewVettingTypes.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewVettingTypes.OptionsSelection.MultiSelect = true;
            this.gridViewVettingTypes.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewVettingTypes.OptionsView.ColumnAutoWidth = false;
            this.gridViewVettingTypes.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewVettingTypes.OptionsView.ShowGroupPanel = false;
            this.gridViewVettingTypes.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewVettingTypes.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewVettingTypes.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewVettingTypes.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewVettingTypes.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewVettingTypes.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewVettingTypes.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewVettingTypes_MouseUp);
            this.gridViewVettingTypes.DoubleClick += new System.EventHandler(this.gridViewVettingTypes_DoubleClick);
            this.gridViewVettingTypes.GotFocus += new System.EventHandler(this.gridViewVettingTypes_GotFocus);
            // 
            // colQualificationTypeID
            // 
            this.colQualificationTypeID.Caption = "Qualification Type ID";
            this.colQualificationTypeID.FieldName = "QualificationTypeID";
            this.colQualificationTypeID.Name = "colQualificationTypeID";
            this.colQualificationTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeID.Width = 96;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 236;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Record Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Visible = true;
            this.colRecordOrder.VisibleIndex = 1;
            this.colRecordOrder.Width = 99;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 2;
            this.colRemarks.Width = 222;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridControlVettingSubTypes
            // 
            this.gridControlVettingSubTypes.DataSource = this.spHR00231MasterQualificationTypesManagerLinkedSubTypesBindingSource;
            this.gridControlVettingSubTypes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlVettingSubTypes.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlVettingSubTypes.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVettingSubTypes_EmbeddedNavigator_ButtonClick);
            this.gridControlVettingSubTypes.Location = new System.Drawing.Point(0, 0);
            this.gridControlVettingSubTypes.MainView = this.gridViewVettingSubTypes;
            this.gridControlVettingSubTypes.MenuManager = this.barManager1;
            this.gridControlVettingSubTypes.Name = "gridControlVettingSubTypes";
            this.gridControlVettingSubTypes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlVettingSubTypes.Size = new System.Drawing.Size(744, 190);
            this.gridControlVettingSubTypes.TabIndex = 0;
            this.gridControlVettingSubTypes.UseEmbeddedNavigator = true;
            this.gridControlVettingSubTypes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVettingSubTypes});
            // 
            // spHR00231MasterQualificationTypesManagerLinkedSubTypesBindingSource
            // 
            this.spHR00231MasterQualificationTypesManagerLinkedSubTypesBindingSource.DataMember = "sp_HR_00231_Master_Qualification_Types_Manager_Linked_SubTypes";
            this.spHR00231MasterQualificationTypesManagerLinkedSubTypesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewVettingSubTypes
            // 
            this.gridViewVettingSubTypes.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.colTypeDescription,
            this.colQualificationSubTypeID,
            this.colQualificationTypeID1,
            this.colQualificationStatusTypeDescription,
            this.colValidityTypeDescription,
            this.colRefreshIntervalDescription,
            this.colExpiryPeriodDescription,
            this.colHideFromTotalviewDescription});
            this.gridViewVettingSubTypes.GridControl = this.gridControlVettingSubTypes;
            this.gridViewVettingSubTypes.GroupCount = 1;
            this.gridViewVettingSubTypes.Name = "gridViewVettingSubTypes";
            this.gridViewVettingSubTypes.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewVettingSubTypes.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewVettingSubTypes.OptionsLayout.StoreAppearance = true;
            this.gridViewVettingSubTypes.OptionsLayout.StoreFormatRules = true;
            this.gridViewVettingSubTypes.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewVettingSubTypes.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewVettingSubTypes.OptionsSelection.MultiSelect = true;
            this.gridViewVettingSubTypes.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewVettingSubTypes.OptionsView.ColumnAutoWidth = false;
            this.gridViewVettingSubTypes.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewVettingSubTypes.OptionsView.ShowGroupPanel = false;
            this.gridViewVettingSubTypes.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHideFromTotalviewDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewVettingSubTypes.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewVettingSubTypes.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewVettingSubTypes.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewVettingSubTypes.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewVettingSubTypes.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewVettingSubTypes.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewVettingSubTypes_MouseUp);
            this.gridViewVettingSubTypes.DoubleClick += new System.EventHandler(this.gridViewVettingSubTypes_DoubleClick);
            this.gridViewVettingSubTypes.GotFocus += new System.EventHandler(this.gridViewVettingSubTypes_GotFocus);
            // 
            // gridColumn2
            // 
            this.gridColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn2.Caption = "Sub-Type Description";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 236;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Record Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            this.gridColumn3.Width = 99;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Remarks";
            this.gridColumn4.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn4.FieldName = "Remarks";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 7;
            this.gridColumn4.Width = 168;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Linked To Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 3;
            this.colTypeDescription.Width = 225;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.Caption = "Qualification Sub-Type ID";
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeID.Width = 143;
            // 
            // colQualificationTypeID1
            // 
            this.colQualificationTypeID1.Caption = "Qualification Type ID";
            this.colQualificationTypeID1.FieldName = "QualificationTypeID";
            this.colQualificationTypeID1.Name = "colQualificationTypeID1";
            this.colQualificationTypeID1.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID1.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID1.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeID1.Width = 121;
            // 
            // colQualificationStatusTypeDescription
            // 
            this.colQualificationStatusTypeDescription.Caption = "Status ";
            this.colQualificationStatusTypeDescription.FieldName = "QualificationStatusTypeDescription";
            this.colQualificationStatusTypeDescription.Name = "colQualificationStatusTypeDescription";
            this.colQualificationStatusTypeDescription.OptionsColumn.AllowEdit = false;
            this.colQualificationStatusTypeDescription.OptionsColumn.AllowFocus = false;
            this.colQualificationStatusTypeDescription.OptionsColumn.ReadOnly = true;
            this.colQualificationStatusTypeDescription.Visible = true;
            this.colQualificationStatusTypeDescription.VisibleIndex = 2;
            // 
            // colValidityTypeDescription
            // 
            this.colValidityTypeDescription.Caption = "Validity";
            this.colValidityTypeDescription.FieldName = "ValidityTypeDescription";
            this.colValidityTypeDescription.Name = "colValidityTypeDescription";
            this.colValidityTypeDescription.OptionsColumn.AllowEdit = false;
            this.colValidityTypeDescription.OptionsColumn.AllowFocus = false;
            this.colValidityTypeDescription.OptionsColumn.ReadOnly = true;
            this.colValidityTypeDescription.Visible = true;
            this.colValidityTypeDescription.VisibleIndex = 3;
            // 
            // colRefreshIntervalDescription
            // 
            this.colRefreshIntervalDescription.Caption = "Refresh Interval";
            this.colRefreshIntervalDescription.FieldName = "RefreshIntervalDescription";
            this.colRefreshIntervalDescription.Name = "colRefreshIntervalDescription";
            this.colRefreshIntervalDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshIntervalDescription.OptionsColumn.AllowFocus = false;
            this.colRefreshIntervalDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshIntervalDescription.Visible = true;
            this.colRefreshIntervalDescription.VisibleIndex = 4;
            this.colRefreshIntervalDescription.Width = 110;
            // 
            // colExpiryPeriodDescription
            // 
            this.colExpiryPeriodDescription.Caption = "Expiry Period";
            this.colExpiryPeriodDescription.FieldName = "ExpiryPeriodDescription";
            this.colExpiryPeriodDescription.Name = "colExpiryPeriodDescription";
            this.colExpiryPeriodDescription.OptionsColumn.AllowEdit = false;
            this.colExpiryPeriodDescription.OptionsColumn.AllowFocus = false;
            this.colExpiryPeriodDescription.OptionsColumn.ReadOnly = true;
            this.colExpiryPeriodDescription.Visible = true;
            this.colExpiryPeriodDescription.VisibleIndex = 5;
            this.colExpiryPeriodDescription.Width = 96;
            // 
            // spHR00142MasterVettingTypesManagerLinkedRestrictionsBindingSource
            // 
            this.spHR00142MasterVettingTypesManagerLinkedRestrictionsBindingSource.DataMember = "sp_HR_00142_Master_Vetting_Types_Manager_Linked_Restrictions";
            this.spHR00142MasterVettingTypesManagerLinkedRestrictionsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlVettingTypes;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControlVettingSubTypes;
            // 
            // beiShowActiveHolidayYearOnly
            // 
            this.beiShowActiveHolidayYearOnly.Caption = "Active Year Only:";
            this.beiShowActiveHolidayYearOnly.Edit = this.repositoryItemCheckEditShowActiveHoilidayYearOnly;
            this.beiShowActiveHolidayYearOnly.EditValue = 1;
            this.beiShowActiveHolidayYearOnly.EditWidth = 20;
            this.beiShowActiveHolidayYearOnly.Id = 27;
            this.beiShowActiveHolidayYearOnly.Name = "beiShowActiveHolidayYearOnly";
            this.beiShowActiveHolidayYearOnly.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Active Year Only - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Tick me to load only the active holiday year data. \r\n\r\nUntick me to load all data" +
    ".";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiShowActiveHolidayYearOnly.SuperTip = superToolTip1;
            // 
            // repositoryItemCheckEditShowActiveHoilidayYearOnly
            // 
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Caption = "Check";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Name = "repositoryItemCheckEditShowActiveHoilidayYearOnly";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueUnchecked = 0;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 28;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // sp_HR_00142_Master_Vetting_Types_Manager_Linked_RestrictionsTableAdapter
            // 
            this.sp_HR_00142_Master_Vetting_Types_Manager_Linked_RestrictionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00230_Master_Qualifiaction_Types_ManagerTableAdapter
            // 
            this.sp_HR_00230_Master_Qualifiaction_Types_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00231_Master_Qualification_Types_Manager_Linked_SubTypesTableAdapter
            // 
            this.sp_HR_00231_Master_Qualification_Types_Manager_Linked_SubTypesTableAdapter.ClearBeforeFill = true;
            // 
            // colHideFromTotalviewDescription
            // 
            this.colHideFromTotalviewDescription.AppearanceHeader.Options.UseTextOptions = true;
            this.colHideFromTotalviewDescription.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHideFromTotalviewDescription.Caption = "Totalview Visibility";
            this.colHideFromTotalviewDescription.FieldName = "HideFromTotalviewDescription";
            this.colHideFromTotalviewDescription.Name = "colHideFromTotalviewDescription";
            this.colHideFromTotalviewDescription.OptionsColumn.AllowEdit = false;
            this.colHideFromTotalviewDescription.OptionsColumn.AllowFocus = false;
            this.colHideFromTotalviewDescription.OptionsColumn.ReadOnly = true;
            this.colHideFromTotalviewDescription.Visible = true;
            this.colHideFromTotalviewDescription.VisibleIndex = 6;
            this.colHideFromTotalviewDescription.Width = 115;
            // 
            // frm_HR_Master_Qualification_Types_Manager
            // 
            this.ClientSize = new System.Drawing.Size(769, 438);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Master_Qualification_Types_Manager";
            this.Text = "Master Qualification Type \\ Sub-Type Manager";
            this.Activated += new System.EventHandler(this.frm_HR_Master_Qualification_Types_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Master_Qualification_Types_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Master_Qualification_Types_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVettingTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00230MasterQualifiactionTypesManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVettingTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVettingSubTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00231MasterQualificationTypesManagerLinkedSubTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVettingSubTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00142MasterVettingTypesManagerLinkedRestrictionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControlVettingTypes;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVettingTypes;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlVettingSubTypes;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVettingSubTypes;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraBars.BarEditItem beiShowActiveHolidayYearOnly;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveHoilidayYearOnly;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private System.Windows.Forms.BindingSource spHR00142MasterVettingTypesManagerLinkedRestrictionsBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00142_Master_Vetting_Types_Manager_Linked_RestrictionsTableAdapter sp_HR_00142_Master_Vetting_Types_Manager_Linked_RestrictionsTableAdapter;
        private System.Windows.Forms.BindingSource spHR00230MasterQualifiactionTypesManagerBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00230_Master_Qualifiaction_Types_ManagerTableAdapter sp_HR_00230_Master_Qualifiaction_Types_ManagerTableAdapter;
        private System.Windows.Forms.BindingSource spHR00231MasterQualificationTypesManagerLinkedSubTypesBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00231_Master_Qualification_Types_Manager_Linked_SubTypesTableAdapter sp_HR_00231_Master_Qualification_Types_Manager_Linked_SubTypesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationStatusTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colValidityTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshIntervalDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryPeriodDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colHideFromTotalviewDescription;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using WoodPlan5.Classes.HR;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Bonus_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_FormStillLoading = true;
        private bool ibool_ignoreValidation = true;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public string strLinkedToRecordDesc2 = "";
        public string strLinkedToRecordDesc3 = "";
        public string strLinkedToRecordDesc4 = "";

        private int ShowFinancialData = 1;  // Controls if the Contracts grid returns salary info //

        #endregion

        public frm_HR_Employee_Bonus_Edit()
        {
            InitializeComponent();
        }
        
        private void frm_HR_Employee_Bonus_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500098;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            ShowFinancialData = (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey) ? 0 : 1);

            // Populate Main Dataset //
            sp_HR_00040_Employee_Bonus_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            //LoadCombos();

            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        var drNewRow = this.dataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_Edit.Newsp_HR_00040_Employee_Bonus_EditRow();       //.NewRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordIds = strRecordIDs;
                        drNewRow.EmployeeID = intLinkedToRecordID;
                        drNewRow.EmployeeName = strLinkedToRecordDesc;
                        drNewRow.EmployeeSurname = strLinkedToRecordDesc2;
                        drNewRow.EmployeeFirstname = strLinkedToRecordDesc3;
                        drNewRow.EmployeeNumber = strLinkedToRecordDesc4;
                        drNewRow.GuaranteeBonus = 0;

                        SimplerAES Encryptor = new SimplerAES();
                        string strAmount = Encryptor.Encrypt("0.00");
                        drNewRow["GuaranteeBonusAmount"] = strAmount;
                        drNewRow["BonusAmountPaid"] = strAmount;
                        GuaranteeBonusAmountSpinEdit.EditValue = "0.00";
                        BonusAmountPaidSpinEdit.EditValue = "0.00";

                        this.dataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_Edit.Addsp_HR_00040_Employee_Bonus_EditRow(drNewRow);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        //DataRow drNewRow;
                        var drNewRow = this.dataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_Edit.Newsp_HR_00040_Employee_Bonus_EditRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordIds = strRecordIDs;
                        drNewRow.EmployeeName = strLinkedToRecordDesc;
                        drNewRow.EmployeeSurname = strLinkedToRecordDesc2;
                        drNewRow.EmployeeFirstname = strLinkedToRecordDesc3;
                        drNewRow.EmployeeNumber = strLinkedToRecordDesc4;
                        drNewRow.ShowFinancialData = ShowFinancialData;
                        this.dataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_Edit.Addsp_HR_00040_Employee_Bonus_EditRow(drNewRow);
                        drNewRow.AcceptChanges();
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_HR_00040_Employee_Bonus_EditTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_Edit, strFormMode, strRecordIDs, ShowFinancialData);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);

                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }
       
        private void frm_HR_Employee_Bonus_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Employee_Bonus_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

 
        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Employee Bonus", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        BonusDueDateDateEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = false;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        BonusDueDateDateEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = true;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = false;

                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        BonusDueDateDateEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = false;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        BonusDueDateDateEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = true;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["BonusID"] == null ? 0 : Convert.ToInt32(currentRow["BonusID"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of Linked Documents button //

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.spHR00040EmployeeBonusEditBindingSource.EndEdit();
            try
            {
                this.sp_HR_00040_Employee_Bonus_EditTableAdapter.Update(dataSet_HR_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
                var currentRow = (DataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_EditRow)currentRowView.Row;
                if (currentRow != null) strNewIDs = currentRow.BonusID + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["Mode"] = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
                var currentRow = (DataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_EditRow)currentRowView.Row;
                if (currentRow != null)
                {
                    currentRow["Mode"] = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["RecordIds"]);  // Values returned from Update SP //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        var fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Utils.enmMainGrids.Bonus, strNewIDs);
                    }
                    if (frmChild.Name == "frm_HR_Employee_Edit")
                    {
                        var fParentForm = (frm_HR_Employee_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(11, Utils.enmMainGrids.Bonus, strNewIDs);
                    }
                    if (frmChild.Name == "frm_HR_Bonus_Manager")
                    {
                        var fParentForm = (frm_HR_Bonus_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Decrypt_Financial_Fields();
                    Set_Guaranteed_Enabled_Status(null);
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void EmployeeNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
                var currentRow = (DataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow.EmployeeID.ToString()) ? 0 : Convert.ToInt32(currentRow.EmployeeID));
                var fChildForm = new frm_HR_Select_Employee();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = intEmployeeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.EmployeeID = fChildForm.intSelectedEmployeeID;
                    currentRow.EmployeeName = fChildForm.strSelectedEmployeeName;
                    currentRow.EmployeeSurname = fChildForm.strSelectedSurname;
                    currentRow.EmployeeFirstname = fChildForm.strSelectedForename;
                    currentRow.EmployeeNumber = fChildForm.strSelectedEmployeeNumber;

                    spHR00040EmployeeBonusEditBindingSource.EndEdit();
                }
            }
            else if ("edit".Equals(e.Button.Tag))
            {
                //Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 3001, "Clients");
            }
        }
        private void EmployeeNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "edit" || this.strFormMode == "add") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(EmployeeNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(EmployeeNameButtonEdit, "");
            }
        }

        private void BonusDueDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit dt = (DateEdit)sender;
            if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;
            if (string.IsNullOrEmpty(dt.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(BonusDueDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(BonusDueDateDateEdit, "");
            }
        }

        private void GuaranteeBonusAmountSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
            if (currentRow == null) return;
            string strValue = GuaranteeBonusAmountSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["GuaranteeBonusAmount"].ToString() != strValue) currentRow["GuaranteeBonusAmount"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Guaranteed Bonus Amount data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void BonusAmountPaidSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
            if (currentRow == null) return;
            string strValue = BonusAmountPaidSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["BonusAmountPaid"].ToString() != strValue) currentRow["BonusAmountPaid"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Bonus Amount data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void GuaranteeBonusCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            Set_Guaranteed_Enabled_Status((CheckEdit)sender);
        }
        private void GuaranteeBonusCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //
        }
        private void Set_Guaranteed_Enabled_Status(CheckEdit ce)
        {
            if (strFormMode == "view") return;
            if (ce == null)
            {
                DataRowView currentRow = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
                if (currentRow == null) return;
                if (Convert.ToInt32(currentRow["GuaranteeBonus"]) != 0)
                {
                    GuaranteeBonusAmountSpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    GuaranteeBonusAmountSpinEdit.Properties.ReadOnly = true;
                }
            }
            else
            {
                if (ce.Checked)
                {
                    GuaranteeBonusAmountSpinEdit.Properties.ReadOnly = false;
                }
                else
                {
                    GuaranteeBonusAmountSpinEdit.Properties.ReadOnly = true;
                    if (!ibool_FormStillLoading)
                    {
                        DataRowView currentRow = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
                        if (currentRow != null)
                        {
                            SimplerAES Encryptor = new SimplerAES();
                            string strAmount = Encryptor.Encrypt("0.00");

                            if (currentRow["GuaranteeBonusAmount"].ToString() != strAmount)
                            {
                                currentRow["GuaranteeBonusAmount"] = strAmount;
                                GuaranteeBonusAmountSpinEdit.EditValue = "0.00";
                            }
                        }
                    }
                }
            }
        }

        #endregion


        private void Decrypt_Financial_Fields()
        {
            if (!string.IsNullOrEmpty(GlobalSettings.HRSecurityKey))  // Decrypt financial Info //
            {
                DataRowView currentRow = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
                if (currentRow == null) return;

                SimplerAES Encryptor = new SimplerAES();
                try
                {
                    string strValue = currentRow["GuaranteeBonusAmount"].ToString();
                    GuaranteeBonusAmountSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");

                    strValue = currentRow["BonusAmountPaid"].ToString();
                    BonusAmountPaidSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");
                }
                catch (Exception ex)
                {
                    GuaranteeBonusAmountSpinEdit.EditValue = "0.00";
                    BonusAmountPaidSpinEdit.EditValue = "0.00";
                }
            }
            else  // Don't display any financial info //
            {
                GuaranteeBonusAmountSpinEdit.EditValue = "0.00";
                BonusAmountPaidSpinEdit.EditValue = "0.00";
            }
        }


        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)spHR00040EmployeeBonusEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["BonusID"].ToString()) ? 0 : Convert.ToInt32(currentRow["BonusID"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 10;  // Bonus //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = currentRow["EmployeeName"].ToString() + " - " + Convert.ToDateTime(currentRow["BonusDueDate"]).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }







    }
}


namespace WoodPlan5
{
    partial class frm_HR_Work_Schedule_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Work_Schedule_Manager));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colDateStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00170EmployeeDaysWorkedManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDayWorkedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkPatternID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBreakLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMinutes = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPaidForBreaks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalculatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEmployeeDepartmentWorkedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeAbsenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentEmployee1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiDepartment = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDepartment = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlDepartments = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.spHR00114DepartmentFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDepartmentFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiEmployee = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlEmployees = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.spHR00115EmployeesByDeptFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnEmployeeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiActive = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditActiveOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.beiDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlDateRangeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterSelected = new DevExpress.XtraBars.BarCheckItem();
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp_HR_00114_Department_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter();
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00170_Employee_Days_Worked_ManagerTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00170_Employee_Days_Worked_ManagerTableAdapter();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageGrid = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraTabPageCalendar = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.schedulerControl1 = new DevExpress.XtraScheduler.SchedulerControl();
            this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage(this.components);
            this.spHR00208EmployeeDaysWorkedCalendarBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spHR00171EmployeeDaysWorkedManagerResourcesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.resourcesCheckedListBoxControl1 = new DevExpress.XtraScheduler.UI.ResourcesCheckedListBoxControl();
            this.popupContainerControlStyling = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.spHR00209AbsenceTypesGraphicalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGraphicalID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGraphicalColour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorPickEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit();
            this.colGraphicalDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnStylingOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.schedulerBarController1 = new DevExpress.XtraScheduler.UI.SchedulerBarController();
            this.openScheduleItem1 = new DevExpress.XtraScheduler.UI.OpenScheduleItem();
            this.saveScheduleItem1 = new DevExpress.XtraScheduler.UI.SaveScheduleItem();
            this.printPreviewItem1 = new DevExpress.XtraScheduler.UI.PrintPreviewItem();
            this.printItem1 = new DevExpress.XtraScheduler.UI.PrintItem();
            this.printPageSetupItem1 = new DevExpress.XtraScheduler.UI.PrintPageSetupItem();
            this.newAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewAppointmentItem();
            this.newRecurringAppointmentItem1 = new DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem();
            this.navigateViewBackwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewBackwardItem();
            this.navigateViewForwardItem1 = new DevExpress.XtraScheduler.UI.NavigateViewForwardItem();
            this.gotoTodayItem1 = new DevExpress.XtraScheduler.UI.GotoTodayItem();
            this.viewZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewZoomInItem();
            this.viewZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewZoomOutItem();
            this.switchToDayViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToDayViewItem();
            this.switchToWorkWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem();
            this.switchToWeekViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToWeekViewItem();
            this.switchToMonthViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToMonthViewItem();
            this.switchToTimelineViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem();
            this.switchToGanttViewItem1 = new DevExpress.XtraScheduler.UI.SwitchToGanttViewItem();
            this.groupByNoneItem1 = new DevExpress.XtraScheduler.UI.GroupByNoneItem();
            this.groupByDateItem1 = new DevExpress.XtraScheduler.UI.GroupByDateItem();
            this.groupByResourceItem1 = new DevExpress.XtraScheduler.UI.GroupByResourceItem();
            this.switchTimeScalesItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesItem();
            this.changeScaleWidthItem1 = new DevExpress.XtraScheduler.UI.ChangeScaleWidthItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.switchTimeScalesCaptionItem1 = new DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem();
            this.switchCompressWeekendItem1 = new DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem();
            this.switchShowWorkTimeOnlyItem1 = new DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem();
            this.switchCellsAutoHeightItem1 = new DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem();
            this.changeSnapToCellsUIItem1 = new DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem();
            this.editAppointmentQueryItem1 = new DevExpress.XtraScheduler.UI.EditAppointmentQueryItem();
            this.editOccurrenceUICommandItem1 = new DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem();
            this.editSeriesUICommandItem1 = new DevExpress.XtraScheduler.UI.EditSeriesUICommandItem();
            this.deleteAppointmentsItem1 = new DevExpress.XtraScheduler.UI.DeleteAppointmentsItem();
            this.deleteOccurrenceItem1 = new DevExpress.XtraScheduler.UI.DeleteOccurrenceItem();
            this.deleteSeriesItem1 = new DevExpress.XtraScheduler.UI.DeleteSeriesItem();
            this.splitAppointmentItem1 = new DevExpress.XtraScheduler.UI.SplitAppointmentItem();
            this.changeAppointmentStatusItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentStatusItem();
            this.changeAppointmentLabelItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentLabelItem();
            this.toggleRecurrenceItem1 = new DevExpress.XtraScheduler.UI.ToggleRecurrenceItem();
            this.changeAppointmentReminderItem1 = new DevExpress.XtraScheduler.UI.ChangeAppointmentReminderItem();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.printBar1 = new DevExpress.XtraScheduler.UI.PrintBar();
            this.appointmentBar1 = new DevExpress.XtraScheduler.UI.AppointmentBar();
            this.navigatorBar1 = new DevExpress.XtraScheduler.UI.NavigatorBar();
            this.arrangeBar1 = new DevExpress.XtraScheduler.UI.ArrangeBar();
            this.groupByBar1 = new DevExpress.XtraScheduler.UI.GroupByBar();
            this.timeScaleBar1 = new DevExpress.XtraScheduler.UI.TimeScaleBar();
            this.layoutBar1 = new DevExpress.XtraScheduler.UI.LayoutBar();
            this.sp_HR_00171_Employee_Days_Worked_Manager_ResourcesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00171_Employee_Days_Worked_Manager_ResourcesTableAdapter();
            this.sp_HR_00208_Employee_Days_Worked_CalendarTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00208_Employee_Days_Worked_CalendarTableAdapter();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.beiStyling = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditStyling = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp_HR_00209_Absence_Types_GraphicalTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00209_Absence_Types_GraphicalTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00170EmployeeDaysWorkedManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).BeginInit();
            this.popupContainerControlDepartments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).BeginInit();
            this.popupContainerControlEmployees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFilter)).BeginInit();
            this.popupContainerControlDateRangeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.xtraTabPageCalendar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00208EmployeeDaysWorkedCalendarBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00171EmployeeDaysWorkedManagerResourcesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesCheckedListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlStyling)).BeginInit();
            this.popupContainerControlStyling.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00209AbsenceTypesGraphicalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStyling)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1129, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 557);
            this.barDockControlBottom.Size = new System.Drawing.Size(1129, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 515);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1129, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 515);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.printBar1,
            this.appointmentBar1,
            this.navigatorBar1,
            this.arrangeBar1,
            this.groupByBar1,
            this.timeScaleBar1,
            this.layoutBar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.beiDateRange,
            this.beiDepartment,
            this.beiEmployee,
            this.beiActive,
            this.openScheduleItem1,
            this.saveScheduleItem1,
            this.printPreviewItem1,
            this.printItem1,
            this.printPageSetupItem1,
            this.newAppointmentItem1,
            this.newRecurringAppointmentItem1,
            this.navigateViewBackwardItem1,
            this.navigateViewForwardItem1,
            this.gotoTodayItem1,
            this.viewZoomInItem1,
            this.viewZoomOutItem1,
            this.switchToDayViewItem1,
            this.switchToWorkWeekViewItem1,
            this.switchToWeekViewItem1,
            this.switchToMonthViewItem1,
            this.switchToTimelineViewItem1,
            this.switchToGanttViewItem1,
            this.groupByNoneItem1,
            this.groupByDateItem1,
            this.groupByResourceItem1,
            this.switchTimeScalesItem1,
            this.changeScaleWidthItem1,
            this.switchTimeScalesCaptionItem1,
            this.switchCompressWeekendItem1,
            this.switchShowWorkTimeOnlyItem1,
            this.switchCellsAutoHeightItem1,
            this.changeSnapToCellsUIItem1,
            this.editAppointmentQueryItem1,
            this.editOccurrenceUICommandItem1,
            this.editSeriesUICommandItem1,
            this.deleteAppointmentsItem1,
            this.deleteOccurrenceItem1,
            this.deleteSeriesItem1,
            this.splitAppointmentItem1,
            this.changeAppointmentStatusItem1,
            this.changeAppointmentLabelItem1,
            this.toggleRecurrenceItem1,
            this.changeAppointmentReminderItem1,
            this.beiStyling,
            this.bciFilterSelected});
            this.barManager1.MaxItemId = 75;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly,
            this.repositoryItemPopupContainerEditDepartment,
            this.repositoryItemPopupContainerEditEmployee,
            this.repositoryItemCheckEditActiveOnly,
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1,
            this.repositoryItemPopupContainerEditStyling});
            // 
            // colDateStatus
            // 
            this.colDateStatus.Caption = "Status";
            this.colDateStatus.FieldName = "DateStatus";
            this.colDateStatus.Name = "colDateStatus";
            this.colDateStatus.OptionsColumn.AllowEdit = false;
            this.colDateStatus.OptionsColumn.AllowFocus = false;
            this.colDateStatus.OptionsColumn.ReadOnly = true;
            this.colDateStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCurrentEmployee
            // 
            this.colCurrentEmployee.Caption = "Current Employee";
            this.colCurrentEmployee.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCurrentEmployee.FieldName = "CurrentEmployee";
            this.colCurrentEmployee.Name = "colCurrentEmployee";
            this.colCurrentEmployee.OptionsColumn.AllowEdit = false;
            this.colCurrentEmployee.OptionsColumn.AllowFocus = false;
            this.colCurrentEmployee.OptionsColumn.ReadOnly = true;
            this.colCurrentEmployee.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentEmployee.Visible = true;
            this.colCurrentEmployee.VisibleIndex = 4;
            this.colCurrentEmployee.Width = 107;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00170EmployeeDaysWorkedManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditHours,
            this.repositoryItemTextEditTime,
            this.repositoryItemTextEditMinutes});
            this.gridControl1.Size = new System.Drawing.Size(1124, 489);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00170EmployeeDaysWorkedManagerBindingSource
            // 
            this.spHR00170EmployeeDaysWorkedManagerBindingSource.DataMember = "sp_HR_00170_Employee_Days_Worked_Manager";
            this.spHR00170EmployeeDaysWorkedManagerBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "linked_documents_16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDayWorkedID,
            this.colWorkPatternID,
            this.colEmployeeID1,
            this.colDateWorked,
            this.colStartTime,
            this.colEndTime,
            this.colBreakLength,
            this.colPaidForBreaks,
            this.colCalculatedHours,
            this.colEmployeeDepartmentWorkedID,
            this.colEmployeeAbsenceID,
            this.colRemarks,
            this.colEmployeeName,
            this.colEmployeeSurname,
            this.colEmployeeFirstname,
            this.colEmployeeNumber,
            this.colCurrentEmployee1,
            this.colHeaderDescription,
            this.colDepartmentName1,
            this.colLocationName,
            this.colBusinessAreaName,
            this.colRegionName,
            this.colAbsenceRecordType,
            this.colAbsenceType,
            this.colAbsenceStatus,
            this.colAbsenceSubType,
            this.colDateStatus,
            this.colSelected});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colDateStatus;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = -1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colDateStatus;
            gridFormatRule2.Name = "Format1";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 1;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.FormatRules.Add(gridFormatRule2);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.BestFitMaxRowCount = 20;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateWorked, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartTime, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEndTime, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colDayWorkedID
            // 
            this.colDayWorkedID.Caption = "Day Worked ID";
            this.colDayWorkedID.FieldName = "DayWorkedID";
            this.colDayWorkedID.Name = "colDayWorkedID";
            this.colDayWorkedID.OptionsColumn.AllowEdit = false;
            this.colDayWorkedID.OptionsColumn.AllowFocus = false;
            this.colDayWorkedID.OptionsColumn.ReadOnly = true;
            this.colDayWorkedID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDayWorkedID.Width = 94;
            // 
            // colWorkPatternID
            // 
            this.colWorkPatternID.Caption = "Work Pattern ID";
            this.colWorkPatternID.FieldName = "WorkPatternID";
            this.colWorkPatternID.Name = "colWorkPatternID";
            this.colWorkPatternID.OptionsColumn.AllowEdit = false;
            this.colWorkPatternID.OptionsColumn.AllowFocus = false;
            this.colWorkPatternID.OptionsColumn.ReadOnly = true;
            this.colWorkPatternID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colWorkPatternID.Width = 99;
            // 
            // colEmployeeID1
            // 
            this.colEmployeeID1.Caption = "Employee ID";
            this.colEmployeeID1.FieldName = "EmployeeID";
            this.colEmployeeID1.Name = "colEmployeeID1";
            this.colEmployeeID1.OptionsColumn.AllowEdit = false;
            this.colEmployeeID1.OptionsColumn.AllowFocus = false;
            this.colEmployeeID1.OptionsColumn.ReadOnly = true;
            this.colEmployeeID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID1.Width = 81;
            // 
            // colDateWorked
            // 
            this.colDateWorked.Caption = "Date Worked";
            this.colDateWorked.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateWorked.FieldName = "DateWorked";
            this.colDateWorked.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateWorked.Name = "colDateWorked";
            this.colDateWorked.OptionsColumn.AllowEdit = false;
            this.colDateWorked.OptionsColumn.AllowFocus = false;
            this.colDateWorked.OptionsColumn.ReadOnly = true;
            this.colDateWorked.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDateWorked.Visible = true;
            this.colDateWorked.VisibleIndex = 1;
            this.colDateWorked.Width = 97;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 2;
            this.colStartTime.Width = 83;
            // 
            // repositoryItemTextEditTime
            // 
            this.repositoryItemTextEditTime.AutoHeight = false;
            this.repositoryItemTextEditTime.Mask.EditMask = "t";
            this.repositoryItemTextEditTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditTime.Name = "repositoryItemTextEditTime";
            // 
            // colEndTime
            // 
            this.colEndTime.Caption = "End Time";
            this.colEndTime.ColumnEdit = this.repositoryItemTextEditTime;
            this.colEndTime.FieldName = "EndTime";
            this.colEndTime.Name = "colEndTime";
            this.colEndTime.OptionsColumn.AllowEdit = false;
            this.colEndTime.OptionsColumn.AllowFocus = false;
            this.colEndTime.OptionsColumn.ReadOnly = true;
            this.colEndTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEndTime.Visible = true;
            this.colEndTime.VisibleIndex = 3;
            this.colEndTime.Width = 77;
            // 
            // colBreakLength
            // 
            this.colBreakLength.Caption = "Break Length";
            this.colBreakLength.ColumnEdit = this.repositoryItemTextEditMinutes;
            this.colBreakLength.FieldName = "BreakLength";
            this.colBreakLength.Name = "colBreakLength";
            this.colBreakLength.OptionsColumn.AllowEdit = false;
            this.colBreakLength.OptionsColumn.AllowFocus = false;
            this.colBreakLength.OptionsColumn.ReadOnly = true;
            this.colBreakLength.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBreakLength.Visible = true;
            this.colBreakLength.VisibleIndex = 4;
            this.colBreakLength.Width = 84;
            // 
            // repositoryItemTextEditMinutes
            // 
            this.repositoryItemTextEditMinutes.AutoHeight = false;
            this.repositoryItemTextEditMinutes.Mask.EditMask = "####0 Minutes";
            this.repositoryItemTextEditMinutes.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMinutes.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMinutes.Name = "repositoryItemTextEditMinutes";
            // 
            // colPaidForBreaks
            // 
            this.colPaidForBreaks.Caption = "Paid For Breaks";
            this.colPaidForBreaks.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colPaidForBreaks.FieldName = "PaidForBreaks";
            this.colPaidForBreaks.Name = "colPaidForBreaks";
            this.colPaidForBreaks.OptionsColumn.AllowEdit = false;
            this.colPaidForBreaks.OptionsColumn.AllowFocus = false;
            this.colPaidForBreaks.OptionsColumn.ReadOnly = true;
            this.colPaidForBreaks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPaidForBreaks.Visible = true;
            this.colPaidForBreaks.VisibleIndex = 5;
            this.colPaidForBreaks.Width = 95;
            // 
            // colCalculatedHours
            // 
            this.colCalculatedHours.Caption = "Calculated Hours";
            this.colCalculatedHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colCalculatedHours.FieldName = "CalculatedHours";
            this.colCalculatedHours.Name = "colCalculatedHours";
            this.colCalculatedHours.OptionsColumn.AllowEdit = false;
            this.colCalculatedHours.OptionsColumn.AllowFocus = false;
            this.colCalculatedHours.OptionsColumn.ReadOnly = true;
            this.colCalculatedHours.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCalculatedHours.Visible = true;
            this.colCalculatedHours.VisibleIndex = 6;
            this.colCalculatedHours.Width = 102;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "####0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colEmployeeDepartmentWorkedID
            // 
            this.colEmployeeDepartmentWorkedID.Caption = "Department Worked ID";
            this.colEmployeeDepartmentWorkedID.FieldName = "EmployeeDepartmentWorkedID";
            this.colEmployeeDepartmentWorkedID.Name = "colEmployeeDepartmentWorkedID";
            this.colEmployeeDepartmentWorkedID.OptionsColumn.AllowEdit = false;
            this.colEmployeeDepartmentWorkedID.OptionsColumn.AllowFocus = false;
            this.colEmployeeDepartmentWorkedID.OptionsColumn.ReadOnly = true;
            this.colEmployeeDepartmentWorkedID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeDepartmentWorkedID.Width = 132;
            // 
            // colEmployeeAbsenceID
            // 
            this.colEmployeeAbsenceID.Caption = "Absence ID";
            this.colEmployeeAbsenceID.FieldName = "EmployeeAbsenceID";
            this.colEmployeeAbsenceID.Name = "colEmployeeAbsenceID";
            this.colEmployeeAbsenceID.OptionsColumn.AllowEdit = false;
            this.colEmployeeAbsenceID.OptionsColumn.AllowFocus = false;
            this.colEmployeeAbsenceID.OptionsColumn.ReadOnly = true;
            this.colEmployeeAbsenceID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeAbsenceID.Width = 76;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.Caption = "Employee Name";
            this.colEmployeeName.FieldName = "EmployeeName";
            this.colEmployeeName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeName.Name = "colEmployeeName";
            this.colEmployeeName.OptionsColumn.AllowEdit = false;
            this.colEmployeeName.OptionsColumn.AllowFocus = false;
            this.colEmployeeName.OptionsColumn.ReadOnly = true;
            this.colEmployeeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName.Visible = true;
            this.colEmployeeName.VisibleIndex = 0;
            this.colEmployeeName.Width = 189;
            // 
            // colEmployeeSurname
            // 
            this.colEmployeeSurname.Caption = "Surname";
            this.colEmployeeSurname.FieldName = "EmployeeSurname";
            this.colEmployeeSurname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeSurname.Name = "colEmployeeSurname";
            this.colEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname.Width = 106;
            // 
            // colEmployeeFirstname
            // 
            this.colEmployeeFirstname.Caption = "Firstname";
            this.colEmployeeFirstname.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeFirstname.Name = "colEmployeeFirstname";
            this.colEmployeeFirstname.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname.Width = 109;
            // 
            // colEmployeeNumber
            // 
            this.colEmployeeNumber.Caption = "Employee #";
            this.colEmployeeNumber.FieldName = "EmployeeNumber";
            this.colEmployeeNumber.Name = "colEmployeeNumber";
            this.colEmployeeNumber.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber.Width = 78;
            // 
            // colCurrentEmployee1
            // 
            this.colCurrentEmployee1.Caption = "Current Employee";
            this.colCurrentEmployee1.FieldName = "CurrentEmployee";
            this.colCurrentEmployee1.Name = "colCurrentEmployee1";
            this.colCurrentEmployee1.OptionsColumn.AllowEdit = false;
            this.colCurrentEmployee1.OptionsColumn.AllowFocus = false;
            this.colCurrentEmployee1.OptionsColumn.ReadOnly = true;
            this.colCurrentEmployee1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentEmployee1.Width = 107;
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Work Pattern Header";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeaderDescription.Width = 123;
            // 
            // colDepartmentName1
            // 
            this.colDepartmentName1.Caption = "Department Worked";
            this.colDepartmentName1.FieldName = "DepartmentName";
            this.colDepartmentName1.Name = "colDepartmentName1";
            this.colDepartmentName1.OptionsColumn.AllowEdit = false;
            this.colDepartmentName1.OptionsColumn.AllowFocus = false;
            this.colDepartmentName1.OptionsColumn.ReadOnly = true;
            this.colDepartmentName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentName1.Visible = true;
            this.colDepartmentName1.VisibleIndex = 8;
            this.colDepartmentName1.Width = 118;
            // 
            // colLocationName
            // 
            this.colLocationName.Caption = "Location";
            this.colLocationName.FieldName = "LocationName";
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.OptionsColumn.AllowEdit = false;
            this.colLocationName.OptionsColumn.AllowFocus = false;
            this.colLocationName.OptionsColumn.ReadOnly = true;
            this.colLocationName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLocationName.Visible = true;
            this.colLocationName.VisibleIndex = 9;
            // 
            // colBusinessAreaName
            // 
            this.colBusinessAreaName.Caption = "Business Area";
            this.colBusinessAreaName.FieldName = "BusinessAreaName";
            this.colBusinessAreaName.Name = "colBusinessAreaName";
            this.colBusinessAreaName.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBusinessAreaName.Visible = true;
            this.colBusinessAreaName.VisibleIndex = 10;
            this.colBusinessAreaName.Width = 88;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 11;
            // 
            // colAbsenceRecordType
            // 
            this.colAbsenceRecordType.Caption = "Absence Record Type";
            this.colAbsenceRecordType.FieldName = "AbsenceRecordType";
            this.colAbsenceRecordType.Name = "colAbsenceRecordType";
            this.colAbsenceRecordType.OptionsColumn.AllowEdit = false;
            this.colAbsenceRecordType.OptionsColumn.AllowFocus = false;
            this.colAbsenceRecordType.OptionsColumn.ReadOnly = true;
            this.colAbsenceRecordType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAbsenceRecordType.Visible = true;
            this.colAbsenceRecordType.VisibleIndex = 12;
            this.colAbsenceRecordType.Width = 126;
            // 
            // colAbsenceType
            // 
            this.colAbsenceType.Caption = "Absence Type";
            this.colAbsenceType.FieldName = "AbsenceType";
            this.colAbsenceType.Name = "colAbsenceType";
            this.colAbsenceType.OptionsColumn.AllowEdit = false;
            this.colAbsenceType.OptionsColumn.AllowFocus = false;
            this.colAbsenceType.OptionsColumn.ReadOnly = true;
            this.colAbsenceType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAbsenceType.Visible = true;
            this.colAbsenceType.VisibleIndex = 13;
            this.colAbsenceType.Width = 89;
            // 
            // colAbsenceStatus
            // 
            this.colAbsenceStatus.Caption = "Absence Status";
            this.colAbsenceStatus.FieldName = "AbsenceStatus";
            this.colAbsenceStatus.Name = "colAbsenceStatus";
            this.colAbsenceStatus.OptionsColumn.AllowEdit = false;
            this.colAbsenceStatus.OptionsColumn.AllowFocus = false;
            this.colAbsenceStatus.OptionsColumn.ReadOnly = true;
            this.colAbsenceStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAbsenceStatus.Visible = true;
            this.colAbsenceStatus.VisibleIndex = 15;
            this.colAbsenceStatus.Width = 96;
            // 
            // colAbsenceSubType
            // 
            this.colAbsenceSubType.Caption = "Absence Sub-Type";
            this.colAbsenceSubType.FieldName = "AbsenceSubType";
            this.colAbsenceSubType.Name = "colAbsenceSubType";
            this.colAbsenceSubType.OptionsColumn.AllowEdit = false;
            this.colAbsenceSubType.OptionsColumn.AllowFocus = false;
            this.colAbsenceSubType.OptionsColumn.ReadOnly = true;
            this.colAbsenceSubType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAbsenceSubType.Visible = true;
            this.colAbsenceSubType.VisibleIndex = 14;
            this.colAbsenceSubType.Width = 111;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDepartment, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiEmployee),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiActive),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDateRange, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterSelected, true)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // beiDepartment
            // 
            this.beiDepartment.Caption = "Department(s):";
            this.beiDepartment.Edit = this.repositoryItemPopupContainerEditDepartment;
            this.beiDepartment.EditValue = "No Department Filter";
            this.beiDepartment.EditWidth = 160;
            this.beiDepartment.Id = 31;
            this.beiDepartment.Name = "beiDepartment";
            this.beiDepartment.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Department(s) - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Select one or more department from me to view just those departments. \r\n\r\nSelect " +
    "nothing from me to view all departments.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiDepartment.SuperTip = superToolTip1;
            // 
            // repositoryItemPopupContainerEditDepartment
            // 
            this.repositoryItemPopupContainerEditDepartment.AutoHeight = false;
            this.repositoryItemPopupContainerEditDepartment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDepartment.Name = "repositoryItemPopupContainerEditDepartment";
            this.repositoryItemPopupContainerEditDepartment.PopupControl = this.popupContainerControlDepartments;
            this.repositoryItemPopupContainerEditDepartment.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDepartment_QueryResultValue);
            // 
            // popupContainerControlDepartments
            // 
            this.popupContainerControlDepartments.Controls.Add(this.gridControl2);
            this.popupContainerControlDepartments.Controls.Add(this.btnDepartmentFilterOK);
            this.popupContainerControlDepartments.Location = new System.Drawing.Point(11, 148);
            this.popupContainerControlDepartments.Name = "popupContainerControlDepartments";
            this.popupContainerControlDepartments.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlDepartments.TabIndex = 3;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.spHR00114DepartmentFilterListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(230, 141);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00114DepartmentFilterListBindingSource
            // 
            this.spHR00114DepartmentFilterListBindingSource.DataMember = "sp_HR_00114_Department_Filter_List";
            this.spHR00114DepartmentFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepartmentID,
            this.colBusinessAreaID1,
            this.colDepartmentName,
            this.gridColumn2,
            this.colBusinessAreaName1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBusinessAreaName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartmentName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.Caption = "Department ID";
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentID.Width = 92;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBusinessAreaID1.Width = 102;
            // 
            // colDepartmentName
            // 
            this.colDepartmentName.Caption = "Department Name";
            this.colDepartmentName.FieldName = "DepartmentName";
            this.colDepartmentName.Name = "colDepartmentName";
            this.colDepartmentName.OptionsColumn.AllowEdit = false;
            this.colDepartmentName.OptionsColumn.AllowFocus = false;
            this.colDepartmentName.OptionsColumn.ReadOnly = true;
            this.colDepartmentName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentName.Visible = true;
            this.colDepartmentName.VisibleIndex = 0;
            this.colDepartmentName.Width = 224;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Remarks";
            this.gridColumn2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn2.FieldName = "Remarks";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 175;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area Name";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBusinessAreaName1.Width = 224;
            // 
            // btnDepartmentFilterOK
            // 
            this.btnDepartmentFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDepartmentFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnDepartmentFilterOK.Name = "btnDepartmentFilterOK";
            this.btnDepartmentFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDepartmentFilterOK.TabIndex = 2;
            this.btnDepartmentFilterOK.Text = "OK";
            this.btnDepartmentFilterOK.Click += new System.EventHandler(this.btnDepartmentFilterOK_Click);
            // 
            // beiEmployee
            // 
            this.beiEmployee.Caption = "Employee(s):";
            this.beiEmployee.Edit = this.repositoryItemPopupContainerEditEmployee;
            this.beiEmployee.EditValue = "No Employee Filter";
            this.beiEmployee.EditWidth = 158;
            this.beiEmployee.Id = 32;
            this.beiEmployee.Name = "beiEmployee";
            this.beiEmployee.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Employee(s) - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Select one or more employees from me to view just those employees.\r\n\r\nSelect noth" +
    "ing from me to view all employees.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.beiEmployee.SuperTip = superToolTip2;
            // 
            // repositoryItemPopupContainerEditEmployee
            // 
            this.repositoryItemPopupContainerEditEmployee.AutoHeight = false;
            this.repositoryItemPopupContainerEditEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditEmployee.Name = "repositoryItemPopupContainerEditEmployee";
            this.repositoryItemPopupContainerEditEmployee.PopupControl = this.popupContainerControlEmployees;
            this.repositoryItemPopupContainerEditEmployee.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditEmployee_QueryResultValue);
            // 
            // popupContainerControlEmployees
            // 
            this.popupContainerControlEmployees.Controls.Add(this.gridControl4);
            this.popupContainerControlEmployees.Controls.Add(this.btnEmployeeFilterOK);
            this.popupContainerControlEmployees.Location = new System.Drawing.Point(251, 148);
            this.popupContainerControlEmployees.Name = "popupContainerControlEmployees";
            this.popupContainerControlEmployees.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlEmployees.TabIndex = 6;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.spHR00115EmployeesByDeptFilterListBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(2, 2);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(230, 141);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // spHR00115EmployeesByDeptFilterListBindingSource
            // 
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataMember = "sp_HR_00115_Employees_By_Dept_Filter_List";
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeID,
            this.colEmployeeNumber1,
            this.colTitle,
            this.colFirstname,
            this.colSurname,
            this.colCurrentEmployee,
            this.colRemarks1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colCurrentEmployee;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFirstname, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colEmployeeID
            // 
            this.colEmployeeID.Caption = "Employee ID";
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.Name = "colEmployeeID";
            this.colEmployeeID.OptionsColumn.AllowEdit = false;
            this.colEmployeeID.OptionsColumn.AllowFocus = false;
            this.colEmployeeID.OptionsColumn.ReadOnly = true;
            this.colEmployeeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID.Width = 81;
            // 
            // colEmployeeNumber1
            // 
            this.colEmployeeNumber1.Caption = "Employee #";
            this.colEmployeeNumber1.FieldName = "EmployeeNumber";
            this.colEmployeeNumber1.Name = "colEmployeeNumber1";
            this.colEmployeeNumber1.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber1.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber1.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber1.Visible = true;
            this.colEmployeeNumber1.VisibleIndex = 2;
            this.colEmployeeNumber1.Width = 91;
            // 
            // colTitle
            // 
            this.colTitle.Caption = "Title";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 3;
            this.colTitle.Width = 59;
            // 
            // colFirstname
            // 
            this.colFirstname.Caption = "Forename";
            this.colFirstname.FieldName = "Firstname";
            this.colFirstname.Name = "colFirstname";
            this.colFirstname.OptionsColumn.AllowEdit = false;
            this.colFirstname.OptionsColumn.AllowFocus = false;
            this.colFirstname.OptionsColumn.ReadOnly = true;
            this.colFirstname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFirstname.Visible = true;
            this.colFirstname.VisibleIndex = 1;
            this.colFirstname.Width = 110;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 0;
            this.colSurname.Width = 126;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // btnEmployeeFilterOK
            // 
            this.btnEmployeeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEmployeeFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnEmployeeFilterOK.Name = "btnEmployeeFilterOK";
            this.btnEmployeeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnEmployeeFilterOK.TabIndex = 2;
            this.btnEmployeeFilterOK.Text = "OK";
            this.btnEmployeeFilterOK.Click += new System.EventHandler(this.btnEmployeeFilterOK_Click);
            // 
            // beiActive
            // 
            this.beiActive.Caption = "Active Only:";
            this.beiActive.Edit = this.repositoryItemCheckEditActiveOnly;
            this.beiActive.EditValue = 1;
            this.beiActive.EditWidth = 20;
            this.beiActive.Id = 33;
            this.beiActive.Name = "beiActive";
            this.beiActive.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEditActiveOnly
            // 
            this.repositoryItemCheckEditActiveOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AutoHeight = false;
            this.repositoryItemCheckEditActiveOnly.Caption = "Check";
            this.repositoryItemCheckEditActiveOnly.Name = "repositoryItemCheckEditActiveOnly";
            this.repositoryItemCheckEditActiveOnly.ValueChecked = 1;
            this.repositoryItemCheckEditActiveOnly.ValueUnchecked = 0;
            // 
            // beiDateRange
            // 
            this.beiDateRange.Caption = "Date Range:";
            this.beiDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.beiDateRange.EditValue = "No Date Range Filter";
            this.beiDateRange.EditWidth = 228;
            this.beiDateRange.Id = 28;
            this.beiDateRange.Name = "beiDateRange";
            this.beiDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Date Range - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Optionally select a Start and \\ or End Date from me to restrict the date range of" +
    " the data.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.beiDateRange.SuperTip = superToolTip3;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRangeFilter;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditType_QueryResultValue);
            // 
            // popupContainerControlDateRangeFilter
            // 
            this.popupContainerControlDateRangeFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.popupContainerControlDateRangeFilter.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRangeFilter.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRangeFilter.Location = new System.Drawing.Point(491, 148);
            this.popupContainerControlDateRangeFilter.Name = "popupContainerControlDateRangeFilter";
            this.popupContainerControlDateRangeFilter.Size = new System.Drawing.Size(278, 105);
            this.popupContainerControlDateRangeFilter.TabIndex = 7;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditEnd);
            this.groupControl1.Controls.Add(this.dateEditStart);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(272, 71);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Date Range Filter";
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(65, 51);
            this.dateEditEnd.MenuManager = this.barManager1;
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.Mask.EditMask = "g";
            this.dateEditEnd.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditEnd.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditEnd.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditEnd.Size = new System.Drawing.Size(202, 20);
            this.dateEditEnd.TabIndex = 3;
            // 
            // dateEditStart
            // 
            this.dateEditStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(65, 25);
            this.dateEditStart.MenuManager = this.barManager1;
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.Mask.EditMask = "g";
            this.dateEditStart.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditStart.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditStart.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditStart.Size = new System.Drawing.Size(202, 20);
            this.dateEditStart.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(7, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "End Date:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Start Date:";
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 79);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDateRangeFilterOK.TabIndex = 2;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Glyph = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bciFilterSelected
            // 
            this.bciFilterSelected.Caption = "Selected";
            this.bciFilterSelected.Glyph = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.Glyph")));
            this.bciFilterSelected.Id = 74;
            this.bciFilterSelected.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.LargeGlyph")));
            this.bciFilterSelected.Name = "bciFilterSelected";
            toolTipTitleItem4.Text = "Filter Selected - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bciFilterSelected.SuperTip = superToolTip4;
            this.bciFilterSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterSelected_CheckedChanged);
            // 
            // repositoryItemCheckEditShowActiveHoilidayYearOnly
            // 
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Caption = "(Tick if Yes)";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Name = "repositoryItemCheckEditShowActiveHoilidayYearOnly";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // sp_HR_00114_Department_Filter_ListTableAdapter
            // 
            this.sp_HR_00114_Department_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter
            // 
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00170_Employee_Days_Worked_ManagerTableAdapter
            // 
            this.sp_HR_00170_Employee_Days_Worked_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 42);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageGrid;
            this.xtraTabControl1.Size = new System.Drawing.Size(1129, 515);
            this.xtraTabControl1.TabIndex = 4;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageGrid,
            this.xtraTabPageCalendar});
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPageGrid
            // 
            this.xtraTabPageGrid.Controls.Add(this.gridSplitContainer1);
            this.xtraTabPageGrid.Name = "xtraTabPageGrid";
            this.xtraTabPageGrid.Size = new System.Drawing.Size(1124, 489);
            this.xtraTabPageGrid.Text = "Grid View";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDateRangeFilter);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlEmployees);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDepartments);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1124, 489);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // xtraTabPageCalendar
            // 
            this.xtraTabPageCalendar.Controls.Add(this.splitContainerControl1);
            this.xtraTabPageCalendar.Controls.Add(this.standaloneBarDockControl1);
            this.xtraTabPageCalendar.Name = "xtraTabPageCalendar";
            this.xtraTabPageCalendar.Size = new System.Drawing.Size(1124, 489);
            this.xtraTabPageCalendar.Text = "Calendar View";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 28);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.popupContainerControlStyling);
            this.splitContainerControl1.Panel2.Controls.Add(this.schedulerControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1123, 460);
            this.splitContainerControl1.SplitterPosition = 195;
            this.splitContainerControl1.TabIndex = 3;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.dateNavigator1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.resourcesCheckedListBoxControl1);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(195, 460);
            this.splitContainerControl2.SplitterPosition = 176;
            this.splitContainerControl2.TabIndex = 3;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // dateNavigator1
            // 
            this.dateNavigator1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNavigator1.CellPadding = new System.Windows.Forms.Padding(2);
            this.dateNavigator1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateNavigator1.FirstDayOfWeek = System.DayOfWeek.Monday;
            this.dateNavigator1.Location = new System.Drawing.Point(0, 0);
            this.dateNavigator1.Name = "dateNavigator1";
            this.dateNavigator1.SchedulerControl = this.schedulerControl1;
            this.dateNavigator1.Size = new System.Drawing.Size(195, 176);
            this.dateNavigator1.TabIndex = 0;
            // 
            // schedulerControl1
            // 
            this.schedulerControl1.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            this.schedulerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schedulerControl1.GroupType = DevExpress.XtraScheduler.SchedulerGroupType.Resource;
            this.schedulerControl1.Location = new System.Drawing.Point(0, 0);
            this.schedulerControl1.MenuManager = this.barManager1;
            this.schedulerControl1.Name = "schedulerControl1";
            this.schedulerControl1.OptionsCustomization.AllowAppointmentCreate = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.OptionsCustomization.AllowAppointmentDelete = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.OptionsCustomization.AllowAppointmentEdit = DevExpress.XtraScheduler.UsedAppointmentType.NonRecurring;
            this.schedulerControl1.OptionsCustomization.AllowAppointmentResize = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            this.schedulerControl1.Size = new System.Drawing.Size(922, 460);
            this.schedulerControl1.Start = new System.DateTime(2014, 4, 7, 0, 0, 0, 0);
            this.schedulerControl1.Storage = this.schedulerStorage1;
            this.schedulerControl1.TabIndex = 0;
            this.schedulerControl1.Text = "schedulerControl1";
            this.schedulerControl1.Views.DayView.TimeRulers.Add(timeRuler1);
            this.schedulerControl1.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            this.schedulerControl1.EditAppointmentFormShowing += new DevExpress.XtraScheduler.AppointmentFormEventHandler(this.schedulerControl1_EditAppointmentFormShowing);
            // 
            // schedulerStorage1
            // 
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("AbsenceSubType", "AbsenceSubType"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("BreakLength", "BreakLength"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("BusinessAreaName", "BusinessAreaName"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("CalculatedHours", "CalculatedHours"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("CurrentEmployee", "CurrentEmployee"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("EmployeeFirstname", "EmployeeFirstname"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("EmployeeName", "EmployeeName"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("EmployeeNumber", "EmployeeNumber"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("EmployeeSurname", "EmployeeSurname"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("EndTime", "EndTime"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("HeaderDescription", "HeaderDescription"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("LocationName", "LocationName"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("PaidForBreaks", "PaidForBreaks"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("RegionName", "RegionName"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("Remarks", "Remarks"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("StartTime", "StartTime"));
            this.schedulerStorage1.Appointments.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentCustomFieldMapping("DayWorkedID", "DayWorkedID"));
            this.schedulerStorage1.Appointments.DataSource = this.spHR00208EmployeeDaysWorkedCalendarBindingSource;
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))), "None", "&None");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(194)))), ((int)(((byte)(190))))), "Holiday", "Holiday");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))), "Bank Holiday", "Bank Holiday");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(244)))), ((int)(((byte)(156))))), "Sickness", "Sickness");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(228)))), ((int)(((byte)(199))))), "Maternity", "Maternity Leave");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(206)))), ((int)(((byte)(147))))), "Paternity", "Paternity Leave");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(199)))), ((int)(((byte)(244)))), ((int)(((byte)(255))))), "Parental", "Parental Leave");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(219)))), ((int)(((byte)(152))))), "Needs Preparation", "&Needs Preparation");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(207)))), ((int)(((byte)(233))))), "Adoption", "Adoption");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(233)))), ((int)(((byte)(223))))), "Anniversary", "&Anniversary");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(247)))), ((int)(((byte)(165))))), "Dependency", "Dependency");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))), "Bereavement", "Bereavement");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192))))), "Emergency", "Emergency");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192))))), "Compassionate", "Compassionate");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192))))), "Unauthorised", "Unauthorised");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255))))), "Other - Authorised Absence", "Other - Authorised Absence");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255))))), "Medical / Dental", "Medical / Dental");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255))))), "Jury Service", "Jury Service");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64))))), "Laid Off", "Laid Off");
            this.schedulerStorage1.Appointments.Labels.Add(System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255))))), "Training", "Training");
            this.schedulerStorage1.Appointments.Mappings.Description = "AbsenceRecordType";
            this.schedulerStorage1.Appointments.Mappings.End = "EndDateTime";
            this.schedulerStorage1.Appointments.Mappings.Label = "AbsenceTypeID";
            this.schedulerStorage1.Appointments.Mappings.Location = "DepartmentName";
            this.schedulerStorage1.Appointments.Mappings.ResourceId = "EmployeeID";
            this.schedulerStorage1.Appointments.Mappings.Start = "StartDateTime";
            this.schedulerStorage1.Appointments.Mappings.Status = "AbsenceStatus";
            this.schedulerStorage1.Appointments.Mappings.Subject = "EmployeeName";
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Free, "None", "None", System.Drawing.Color.White);
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Tentative, "Pending", "Pending", System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(135)))), ((int)(((byte)(226))))));
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Busy, "Approved", "Approved", System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(135)))), ((int)(((byte)(226))))));
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.OutOfOffice, "Rejected", "Rejected", System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(83))))));
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Custom, "Cancelled", "Cancelled", System.Drawing.Color.White);
            this.schedulerStorage1.Appointments.Statuses.Add(DevExpress.XtraScheduler.AppointmentStatusType.Custom, "Unauthorised", "Unauthorised", System.Drawing.Color.White);
            this.schedulerStorage1.EnableReminders = false;
            this.schedulerStorage1.Resources.DataSource = this.spHR00171EmployeeDaysWorkedManagerResourcesBindingSource;
            this.schedulerStorage1.Resources.Mappings.Caption = "EmployeeName";
            this.schedulerStorage1.Resources.Mappings.Id = "EmployeeID";
            // 
            // spHR00208EmployeeDaysWorkedCalendarBindingSource
            // 
            this.spHR00208EmployeeDaysWorkedCalendarBindingSource.DataMember = "sp_HR_00208_Employee_Days_Worked_Calendar";
            this.spHR00208EmployeeDaysWorkedCalendarBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // spHR00171EmployeeDaysWorkedManagerResourcesBindingSource
            // 
            this.spHR00171EmployeeDaysWorkedManagerResourcesBindingSource.DataMember = "sp_HR_00171_Employee_Days_Worked_Manager_Resources";
            this.spHR00171EmployeeDaysWorkedManagerResourcesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // resourcesCheckedListBoxControl1
            // 
            this.resourcesCheckedListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resourcesCheckedListBoxControl1.Location = new System.Drawing.Point(0, 0);
            this.resourcesCheckedListBoxControl1.Name = "resourcesCheckedListBoxControl1";
            this.resourcesCheckedListBoxControl1.SchedulerControl = this.schedulerControl1;
            this.resourcesCheckedListBoxControl1.Size = new System.Drawing.Size(195, 278);
            this.resourcesCheckedListBoxControl1.TabIndex = 2;
            // 
            // popupContainerControlStyling
            // 
            this.popupContainerControlStyling.Controls.Add(this.gridControl3);
            this.popupContainerControlStyling.Controls.Add(this.btnStylingOK);
            this.popupContainerControlStyling.Location = new System.Drawing.Point(300, 64);
            this.popupContainerControlStyling.Name = "popupContainerControlStyling";
            this.popupContainerControlStyling.Size = new System.Drawing.Size(254, 243);
            this.popupContainerControlStyling.TabIndex = 7;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.spHR00209AbsenceTypesGraphicalBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(2, 2);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemColorPickEdit1});
            this.gridControl3.Size = new System.Drawing.Size(250, 212);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // spHR00209AbsenceTypesGraphicalBindingSource
            // 
            this.spHR00209AbsenceTypesGraphicalBindingSource.DataMember = "sp_HR_00209_Absence_Types_Graphical";
            this.spHR00209AbsenceTypesGraphicalBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTypeID,
            this.colDescription,
            this.colTypeOrder,
            this.colComments,
            this.colGraphicalID,
            this.colGraphicalColour,
            this.colGraphicalDescription});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colTypeOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colTypeID
            // 
            this.colTypeID.Caption = "Type ID";
            this.colTypeID.FieldName = "TypeID";
            this.colTypeID.Name = "colTypeID";
            this.colTypeID.OptionsColumn.AllowEdit = false;
            this.colTypeID.OptionsColumn.AllowFocus = false;
            this.colTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Type Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Width = 101;
            // 
            // colTypeOrder
            // 
            this.colTypeOrder.Caption = "Type Order";
            this.colTypeOrder.FieldName = "TypeOrder";
            this.colTypeOrder.Name = "colTypeOrder";
            this.colTypeOrder.OptionsColumn.AllowEdit = false;
            this.colTypeOrder.OptionsColumn.AllowFocus = false;
            this.colTypeOrder.OptionsColumn.ReadOnly = true;
            this.colTypeOrder.Width = 89;
            // 
            // colComments
            // 
            this.colComments.Caption = "Remarks";
            this.colComments.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colComments.FieldName = "Comments";
            this.colComments.Name = "colComments";
            this.colComments.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colGraphicalID
            // 
            this.colGraphicalID.Caption = "Graphical ID";
            this.colGraphicalID.FieldName = "GraphicalID";
            this.colGraphicalID.Name = "colGraphicalID";
            this.colGraphicalID.OptionsColumn.AllowEdit = false;
            this.colGraphicalID.OptionsColumn.AllowFocus = false;
            this.colGraphicalID.OptionsColumn.ReadOnly = true;
            this.colGraphicalID.Width = 79;
            // 
            // colGraphicalColour
            // 
            this.colGraphicalColour.Caption = "Colour";
            this.colGraphicalColour.ColumnEdit = this.repositoryItemColorPickEdit1;
            this.colGraphicalColour.FieldName = "GraphicalColour";
            this.colGraphicalColour.Name = "colGraphicalColour";
            this.colGraphicalColour.Visible = true;
            this.colGraphicalColour.VisibleIndex = 0;
            this.colGraphicalColour.Width = 49;
            // 
            // repositoryItemColorPickEdit1
            // 
            this.repositoryItemColorPickEdit1.AutoHeight = false;
            this.repositoryItemColorPickEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorPickEdit1.ColorText = DevExpress.XtraEditors.Controls.ColorText.Integer;
            this.repositoryItemColorPickEdit1.Name = "repositoryItemColorPickEdit1";
            this.repositoryItemColorPickEdit1.StoreColorAsInteger = true;
            this.repositoryItemColorPickEdit1.CustomDisplayText += new DevExpress.XtraEditors.Controls.CustomDisplayTextEventHandler(this.repositoryItemColorPickEdit1_CustomDisplayText);
            // 
            // colGraphicalDescription
            // 
            this.colGraphicalDescription.Caption = "Description";
            this.colGraphicalDescription.FieldName = "GraphicalDescription";
            this.colGraphicalDescription.Name = "colGraphicalDescription";
            this.colGraphicalDescription.OptionsColumn.AllowEdit = false;
            this.colGraphicalDescription.OptionsColumn.AllowFocus = false;
            this.colGraphicalDescription.OptionsColumn.ReadOnly = true;
            this.colGraphicalDescription.Visible = true;
            this.colGraphicalDescription.VisibleIndex = 1;
            this.colGraphicalDescription.Width = 173;
            // 
            // btnStylingOK
            // 
            this.btnStylingOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnStylingOK.Location = new System.Drawing.Point(3, 217);
            this.btnStylingOK.Name = "btnStylingOK";
            this.btnStylingOK.Size = new System.Drawing.Size(38, 23);
            this.btnStylingOK.TabIndex = 2;
            this.btnStylingOK.Text = "OK";
            this.btnStylingOK.Click += new System.EventHandler(this.btnStylingOK_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1123, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // schedulerBarController1
            // 
            this.schedulerBarController1.BarItems.Add(this.openScheduleItem1);
            this.schedulerBarController1.BarItems.Add(this.saveScheduleItem1);
            this.schedulerBarController1.BarItems.Add(this.printPreviewItem1);
            this.schedulerBarController1.BarItems.Add(this.printItem1);
            this.schedulerBarController1.BarItems.Add(this.printPageSetupItem1);
            this.schedulerBarController1.BarItems.Add(this.newAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.newRecurringAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewBackwardItem1);
            this.schedulerBarController1.BarItems.Add(this.navigateViewForwardItem1);
            this.schedulerBarController1.BarItems.Add(this.gotoTodayItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomInItem1);
            this.schedulerBarController1.BarItems.Add(this.viewZoomOutItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToDayViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWorkWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToWeekViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToMonthViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToTimelineViewItem1);
            this.schedulerBarController1.BarItems.Add(this.switchToGanttViewItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByNoneItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByDateItem1);
            this.schedulerBarController1.BarItems.Add(this.groupByResourceItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesItem1);
            this.schedulerBarController1.BarItems.Add(this.changeScaleWidthItem1);
            this.schedulerBarController1.BarItems.Add(this.switchTimeScalesCaptionItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCompressWeekendItem1);
            this.schedulerBarController1.BarItems.Add(this.switchShowWorkTimeOnlyItem1);
            this.schedulerBarController1.BarItems.Add(this.switchCellsAutoHeightItem1);
            this.schedulerBarController1.BarItems.Add(this.changeSnapToCellsUIItem1);
            this.schedulerBarController1.BarItems.Add(this.editAppointmentQueryItem1);
            this.schedulerBarController1.BarItems.Add(this.editOccurrenceUICommandItem1);
            this.schedulerBarController1.BarItems.Add(this.editSeriesUICommandItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteAppointmentsItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteOccurrenceItem1);
            this.schedulerBarController1.BarItems.Add(this.deleteSeriesItem1);
            this.schedulerBarController1.BarItems.Add(this.splitAppointmentItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentStatusItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentLabelItem1);
            this.schedulerBarController1.BarItems.Add(this.toggleRecurrenceItem1);
            this.schedulerBarController1.BarItems.Add(this.changeAppointmentReminderItem1);
            this.schedulerBarController1.Control = this.schedulerControl1;
            // 
            // openScheduleItem1
            // 
            this.openScheduleItem1.Id = 60;
            this.openScheduleItem1.Name = "openScheduleItem1";
            // 
            // saveScheduleItem1
            // 
            this.saveScheduleItem1.Id = 61;
            this.saveScheduleItem1.Name = "saveScheduleItem1";
            // 
            // printPreviewItem1
            // 
            this.printPreviewItem1.Id = 34;
            this.printPreviewItem1.Name = "printPreviewItem1";
            // 
            // printItem1
            // 
            this.printItem1.Id = 35;
            this.printItem1.Name = "printItem1";
            // 
            // printPageSetupItem1
            // 
            this.printPageSetupItem1.Id = 36;
            this.printPageSetupItem1.Name = "printPageSetupItem1";
            // 
            // newAppointmentItem1
            // 
            this.newAppointmentItem1.Id = 37;
            this.newAppointmentItem1.Name = "newAppointmentItem1";
            // 
            // newRecurringAppointmentItem1
            // 
            this.newRecurringAppointmentItem1.Id = 38;
            this.newRecurringAppointmentItem1.Name = "newRecurringAppointmentItem1";
            // 
            // navigateViewBackwardItem1
            // 
            this.navigateViewBackwardItem1.Id = 39;
            this.navigateViewBackwardItem1.Name = "navigateViewBackwardItem1";
            // 
            // navigateViewForwardItem1
            // 
            this.navigateViewForwardItem1.Id = 40;
            this.navigateViewForwardItem1.Name = "navigateViewForwardItem1";
            // 
            // gotoTodayItem1
            // 
            this.gotoTodayItem1.Id = 41;
            this.gotoTodayItem1.Name = "gotoTodayItem1";
            // 
            // viewZoomInItem1
            // 
            this.viewZoomInItem1.Id = 42;
            this.viewZoomInItem1.Name = "viewZoomInItem1";
            // 
            // viewZoomOutItem1
            // 
            this.viewZoomOutItem1.Id = 43;
            this.viewZoomOutItem1.Name = "viewZoomOutItem1";
            // 
            // switchToDayViewItem1
            // 
            this.switchToDayViewItem1.Id = 44;
            this.switchToDayViewItem1.Name = "switchToDayViewItem1";
            // 
            // switchToWorkWeekViewItem1
            // 
            this.switchToWorkWeekViewItem1.Id = 45;
            this.switchToWorkWeekViewItem1.Name = "switchToWorkWeekViewItem1";
            // 
            // switchToWeekViewItem1
            // 
            this.switchToWeekViewItem1.Id = 46;
            this.switchToWeekViewItem1.Name = "switchToWeekViewItem1";
            // 
            // switchToMonthViewItem1
            // 
            this.switchToMonthViewItem1.Id = 47;
            this.switchToMonthViewItem1.Name = "switchToMonthViewItem1";
            // 
            // switchToTimelineViewItem1
            // 
            this.switchToTimelineViewItem1.Id = 48;
            this.switchToTimelineViewItem1.Name = "switchToTimelineViewItem1";
            // 
            // switchToGanttViewItem1
            // 
            this.switchToGanttViewItem1.Id = 49;
            this.switchToGanttViewItem1.Name = "switchToGanttViewItem1";
            // 
            // groupByNoneItem1
            // 
            this.groupByNoneItem1.Id = 50;
            this.groupByNoneItem1.Name = "groupByNoneItem1";
            // 
            // groupByDateItem1
            // 
            this.groupByDateItem1.Id = 51;
            this.groupByDateItem1.Name = "groupByDateItem1";
            // 
            // groupByResourceItem1
            // 
            this.groupByResourceItem1.Id = 52;
            this.groupByResourceItem1.Name = "groupByResourceItem1";
            // 
            // switchTimeScalesItem1
            // 
            this.switchTimeScalesItem1.Id = 53;
            this.switchTimeScalesItem1.Name = "switchTimeScalesItem1";
            // 
            // changeScaleWidthItem1
            // 
            this.changeScaleWidthItem1.Edit = this.repositoryItemSpinEdit1;
            this.changeScaleWidthItem1.Id = 54;
            this.changeScaleWidthItem1.Name = "changeScaleWidthItem1";
            this.changeScaleWidthItem1.UseCommandCaption = true;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // switchTimeScalesCaptionItem1
            // 
            this.switchTimeScalesCaptionItem1.Id = 55;
            this.switchTimeScalesCaptionItem1.Name = "switchTimeScalesCaptionItem1";
            // 
            // switchCompressWeekendItem1
            // 
            this.switchCompressWeekendItem1.Id = 56;
            this.switchCompressWeekendItem1.Name = "switchCompressWeekendItem1";
            // 
            // switchShowWorkTimeOnlyItem1
            // 
            this.switchShowWorkTimeOnlyItem1.Id = 57;
            this.switchShowWorkTimeOnlyItem1.Name = "switchShowWorkTimeOnlyItem1";
            // 
            // switchCellsAutoHeightItem1
            // 
            this.switchCellsAutoHeightItem1.Id = 58;
            this.switchCellsAutoHeightItem1.Name = "switchCellsAutoHeightItem1";
            // 
            // changeSnapToCellsUIItem1
            // 
            this.changeSnapToCellsUIItem1.Id = 59;
            this.changeSnapToCellsUIItem1.Name = "changeSnapToCellsUIItem1";
            // 
            // editAppointmentQueryItem1
            // 
            this.editAppointmentQueryItem1.Id = 62;
            this.editAppointmentQueryItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.editOccurrenceUICommandItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.editSeriesUICommandItem1)});
            this.editAppointmentQueryItem1.Name = "editAppointmentQueryItem1";
            this.editAppointmentQueryItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // editOccurrenceUICommandItem1
            // 
            this.editOccurrenceUICommandItem1.Id = 63;
            this.editOccurrenceUICommandItem1.Name = "editOccurrenceUICommandItem1";
            // 
            // editSeriesUICommandItem1
            // 
            this.editSeriesUICommandItem1.Id = 64;
            this.editSeriesUICommandItem1.Name = "editSeriesUICommandItem1";
            // 
            // deleteAppointmentsItem1
            // 
            this.deleteAppointmentsItem1.Id = 65;
            this.deleteAppointmentsItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.deleteOccurrenceItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.deleteSeriesItem1)});
            this.deleteAppointmentsItem1.Name = "deleteAppointmentsItem1";
            this.deleteAppointmentsItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // deleteOccurrenceItem1
            // 
            this.deleteOccurrenceItem1.Id = 66;
            this.deleteOccurrenceItem1.Name = "deleteOccurrenceItem1";
            // 
            // deleteSeriesItem1
            // 
            this.deleteSeriesItem1.Id = 67;
            this.deleteSeriesItem1.Name = "deleteSeriesItem1";
            // 
            // splitAppointmentItem1
            // 
            this.splitAppointmentItem1.Id = 68;
            this.splitAppointmentItem1.Name = "splitAppointmentItem1";
            // 
            // changeAppointmentStatusItem1
            // 
            this.changeAppointmentStatusItem1.Id = 69;
            this.changeAppointmentStatusItem1.Name = "changeAppointmentStatusItem1";
            // 
            // changeAppointmentLabelItem1
            // 
            this.changeAppointmentLabelItem1.Id = 70;
            this.changeAppointmentLabelItem1.Name = "changeAppointmentLabelItem1";
            // 
            // toggleRecurrenceItem1
            // 
            this.toggleRecurrenceItem1.Id = 71;
            this.toggleRecurrenceItem1.Name = "toggleRecurrenceItem1";
            // 
            // changeAppointmentReminderItem1
            // 
            this.changeAppointmentReminderItem1.Edit = this.repositoryItemDuration1;
            this.changeAppointmentReminderItem1.Id = 72;
            this.changeAppointmentReminderItem1.Name = "changeAppointmentReminderItem1";
            this.changeAppointmentReminderItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // printBar1
            // 
            this.printBar1.BarName = "";
            this.printBar1.Control = this.schedulerControl1;
            this.printBar1.DockCol = 2;
            this.printBar1.DockRow = 0;
            this.printBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.printBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPageSetupItem1)});
            this.printBar1.OptionsBar.DrawDragBorder = false;
            this.printBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.printBar1.Text = "";
            // 
            // appointmentBar1
            // 
            this.appointmentBar1.BarName = "";
            this.appointmentBar1.Control = this.schedulerControl1;
            this.appointmentBar1.DockCol = 3;
            this.appointmentBar1.DockRow = 0;
            this.appointmentBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.appointmentBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.newAppointmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.newRecurringAppointmentItem1)});
            this.appointmentBar1.OptionsBar.DrawDragBorder = false;
            this.appointmentBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.appointmentBar1.Text = "";
            // 
            // navigatorBar1
            // 
            this.navigatorBar1.BarName = "";
            this.navigatorBar1.Control = this.schedulerControl1;
            this.navigatorBar1.DockCol = 4;
            this.navigatorBar1.DockRow = 0;
            this.navigatorBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.navigatorBar1.FloatLocation = new System.Drawing.Point(1055, 251);
            this.navigatorBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewBackwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.navigateViewForwardItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.gotoTodayItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomInItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.viewZoomOutItem1)});
            this.navigatorBar1.OptionsBar.DrawDragBorder = false;
            this.navigatorBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.navigatorBar1.Text = "";
            // 
            // arrangeBar1
            // 
            this.arrangeBar1.BarName = "";
            this.arrangeBar1.Control = this.schedulerControl1;
            this.arrangeBar1.DockCol = 5;
            this.arrangeBar1.DockRow = 0;
            this.arrangeBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.arrangeBar1.FloatLocation = new System.Drawing.Point(1244, 246);
            this.arrangeBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToDayViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWorkWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToWeekViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToMonthViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToTimelineViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchToGanttViewItem1)});
            this.arrangeBar1.OptionsBar.DrawDragBorder = false;
            this.arrangeBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.arrangeBar1.Text = "";
            // 
            // groupByBar1
            // 
            this.groupByBar1.BarName = "";
            this.groupByBar1.Control = this.schedulerControl1;
            this.groupByBar1.DockCol = 6;
            this.groupByBar1.DockRow = 0;
            this.groupByBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.groupByBar1.FloatLocation = new System.Drawing.Point(1191, 229);
            this.groupByBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByNoneItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByDateItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.groupByResourceItem1)});
            this.groupByBar1.OptionsBar.DrawDragBorder = false;
            this.groupByBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.groupByBar1.Text = "";
            // 
            // timeScaleBar1
            // 
            this.timeScaleBar1.BarName = "";
            this.timeScaleBar1.Control = this.schedulerControl1;
            this.timeScaleBar1.DockCol = 0;
            this.timeScaleBar1.DockRow = 0;
            this.timeScaleBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.timeScaleBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchTimeScalesItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeScaleWidthItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchTimeScalesCaptionItem1)});
            this.timeScaleBar1.OptionsBar.DrawDragBorder = false;
            this.timeScaleBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.timeScaleBar1.Text = "";
            // 
            // layoutBar1
            // 
            this.layoutBar1.BarName = "";
            this.layoutBar1.Control = this.schedulerControl1;
            this.layoutBar1.DockCol = 1;
            this.layoutBar1.DockRow = 0;
            this.layoutBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.layoutBar1.FloatLocation = new System.Drawing.Point(658, 255);
            this.layoutBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.switchCompressWeekendItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchShowWorkTimeOnlyItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.switchCellsAutoHeightItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.changeSnapToCellsUIItem1)});
            this.layoutBar1.OptionsBar.DrawDragBorder = false;
            this.layoutBar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.layoutBar1.Text = "";
            // 
            // sp_HR_00171_Employee_Days_Worked_Manager_ResourcesTableAdapter
            // 
            this.sp_HR_00171_Employee_Days_Worked_Manager_ResourcesTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00208_Employee_Days_Worked_CalendarTableAdapter
            // 
            this.sp_HR_00208_Employee_Days_Worked_CalendarTableAdapter.ClearBeforeFill = true;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 10";
            this.bar2.DockCol = 7;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(1230, 226);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiStyling)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "Custom 10";
            // 
            // beiStyling
            // 
            this.beiStyling.Caption = "Styling";
            this.beiStyling.Edit = this.repositoryItemPopupContainerEditStyling;
            this.beiStyling.EditValue = "Styling";
            this.beiStyling.EditWidth = 56;
            this.beiStyling.Id = 73;
            this.beiStyling.Name = "beiStyling";
            // 
            // repositoryItemPopupContainerEditStyling
            // 
            this.repositoryItemPopupContainerEditStyling.AutoHeight = false;
            this.repositoryItemPopupContainerEditStyling.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditStyling.Name = "repositoryItemPopupContainerEditStyling";
            this.repositoryItemPopupContainerEditStyling.PopupControl = this.popupContainerControlStyling;
            this.repositoryItemPopupContainerEditStyling.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditStyling.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditStyling_QueryResultValue);
            // 
            // sp_HR_00209_Absence_Types_GraphicalTableAdapter
            // 
            this.sp_HR_00209_Absence_Types_GraphicalTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Work_Schedule_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1129, 557);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Work_Schedule_Manager";
            this.Text = "Work Schedule Manager  [READ ONLY]";
            this.Activated += new System.EventHandler(this.frm_HR_Work_Schedule_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Work_Schedule_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Work_Schedule_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00170EmployeeDaysWorkedManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).EndInit();
            this.popupContainerControlDepartments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).EndInit();
            this.popupContainerControlEmployees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFilter)).EndInit();
            this.popupContainerControlDateRangeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.xtraTabPageCalendar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00208EmployeeDaysWorkedCalendarBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00171EmployeeDaysWorkedManagerResourcesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesCheckedListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlStyling)).EndInit();
            this.popupContainerControlStyling.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00209AbsenceTypesGraphicalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorPickEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerBarController1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditStyling)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem beiDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveHoilidayYearOnly;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraBars.BarEditItem beiDepartment;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDepartment;
        private DevExpress.XtraBars.BarEditItem beiEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private System.Windows.Forms.BindingSource spHR00114DepartmentFilterListBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter sp_HR_00114_Department_Filter_ListTableAdapter;
        private System.Windows.Forms.BindingSource spHR00115EmployeesByDeptFilterListBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private System.Windows.Forms.BindingSource spHR00170EmployeeDaysWorkedManagerBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00170_Employee_Days_Worked_ManagerTableAdapter sp_HR_00170_Employee_Days_Worked_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDayWorkedID;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkPatternID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colBreakLength;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMinutes;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidForBreaks;
        private DevExpress.XtraGrid.Columns.GridColumn colCalculatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeDepartmentWorkedID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeAbsenceID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentEmployee1;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceType;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colDateStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname;
        private DevExpress.XtraBars.BarEditItem beiActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditActiveOnly;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageGrid;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCalendar;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControl1;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDepartments;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DevExpress.XtraEditors.SimpleButton btnDepartmentFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlEmployees;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.SimpleButton btnEmployeeFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRangeFilter;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraScheduler.UI.PrintBar printBar1;
        private DevExpress.XtraScheduler.UI.PrintPreviewItem printPreviewItem1;
        private DevExpress.XtraScheduler.UI.PrintItem printItem1;
        private DevExpress.XtraScheduler.UI.PrintPageSetupItem printPageSetupItem1;
        private DevExpress.XtraScheduler.UI.AppointmentBar appointmentBar1;
        private DevExpress.XtraScheduler.UI.NewAppointmentItem newAppointmentItem1;
        private DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem newRecurringAppointmentItem1;
        private DevExpress.XtraScheduler.UI.NavigatorBar navigatorBar1;
        private DevExpress.XtraScheduler.UI.NavigateViewBackwardItem navigateViewBackwardItem1;
        private DevExpress.XtraScheduler.UI.NavigateViewForwardItem navigateViewForwardItem1;
        private DevExpress.XtraScheduler.UI.GotoTodayItem gotoTodayItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomInItem viewZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewZoomOutItem viewZoomOutItem1;
        private DevExpress.XtraScheduler.UI.ArrangeBar arrangeBar1;
        private DevExpress.XtraScheduler.UI.SwitchToDayViewItem switchToDayViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem switchToWorkWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToWeekViewItem switchToWeekViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToMonthViewItem switchToMonthViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem switchToTimelineViewItem1;
        private DevExpress.XtraScheduler.UI.SwitchToGanttViewItem switchToGanttViewItem1;
        private DevExpress.XtraScheduler.UI.GroupByBar groupByBar1;
        private DevExpress.XtraScheduler.UI.GroupByNoneItem groupByNoneItem1;
        private DevExpress.XtraScheduler.UI.GroupByDateItem groupByDateItem1;
        private DevExpress.XtraScheduler.UI.GroupByResourceItem groupByResourceItem1;
        private DevExpress.XtraScheduler.UI.TimeScaleBar timeScaleBar1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesItem switchTimeScalesItem1;
        private DevExpress.XtraScheduler.UI.ChangeScaleWidthItem changeScaleWidthItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem switchTimeScalesCaptionItem1;
        private DevExpress.XtraScheduler.UI.LayoutBar layoutBar1;
        private DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem switchCompressWeekendItem1;
        private DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem switchShowWorkTimeOnlyItem1;
        private DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem switchCellsAutoHeightItem1;
        private DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem changeSnapToCellsUIItem1;
        private DevExpress.XtraScheduler.UI.OpenScheduleItem openScheduleItem1;
        private DevExpress.XtraScheduler.UI.SaveScheduleItem saveScheduleItem1;
        private DevExpress.XtraScheduler.UI.EditAppointmentQueryItem editAppointmentQueryItem1;
        private DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem editOccurrenceUICommandItem1;
        private DevExpress.XtraScheduler.UI.EditSeriesUICommandItem editSeriesUICommandItem1;
        private DevExpress.XtraScheduler.UI.DeleteAppointmentsItem deleteAppointmentsItem1;
        private DevExpress.XtraScheduler.UI.DeleteOccurrenceItem deleteOccurrenceItem1;
        private DevExpress.XtraScheduler.UI.DeleteSeriesItem deleteSeriesItem1;
        private DevExpress.XtraScheduler.UI.SplitAppointmentItem splitAppointmentItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentStatusItem changeAppointmentStatusItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentLabelItem changeAppointmentLabelItem1;
        private DevExpress.XtraScheduler.UI.ToggleRecurrenceItem toggleRecurrenceItem1;
        private DevExpress.XtraScheduler.UI.ChangeAppointmentReminderItem changeAppointmentReminderItem1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraScheduler.UI.SchedulerBarController schedulerBarController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraScheduler.UI.ResourcesCheckedListBoxControl resourcesCheckedListBoxControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private System.Windows.Forms.BindingSource spHR00171EmployeeDaysWorkedManagerResourcesBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00171_Employee_Days_Worked_Manager_ResourcesTableAdapter sp_HR_00171_Employee_Days_Worked_Manager_ResourcesTableAdapter;
        private System.Windows.Forms.BindingSource spHR00208EmployeeDaysWorkedCalendarBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00208_Employee_Days_Worked_CalendarTableAdapter sp_HR_00208_Employee_Days_Worked_CalendarTableAdapter;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarEditItem beiStyling;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditStyling;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlStyling;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.SimpleButton btnStylingOK;
        private System.Windows.Forms.BindingSource spHR00209AbsenceTypesGraphicalBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00209_Absence_Types_GraphicalTableAdapter sp_HR_00209_Absence_Types_GraphicalTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DevExpress.XtraGrid.Columns.GridColumn colGraphicalID;
        private DevExpress.XtraGrid.Columns.GridColumn colGraphicalColour;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorPickEdit repositoryItemColorPickEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colGraphicalDescription;
        private DevExpress.XtraBars.BarCheckItem bciFilterSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
    }
}

namespace WoodPlan5
{
    partial class frm_HR_Employee_Department_Worked_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Department_Worked_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.CostCentreIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00002EmployeeDeptsWorkedEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.spHR00015CostCentreListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.ToDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.FromDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ReportsToStaffButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ReportsToStaffIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PositionIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00005EmployeePositionsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BaseTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00004LocationBaseDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LocationNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DepartmentNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegionNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.RegionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DepartmentLocationIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BusinessAreaNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.EmployeeDeptWorkedIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.EmployeeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.EmployeeNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.TimePercentageSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForEmployeeDeptWorkedID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDepartmentLocationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportsToStaffID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForPositionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForReportsToStaff = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFromDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForToDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCostCentreID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTimePercentage = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBusinessAreaName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDepartmentName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLocationName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBaseTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.TableAdapterManager();
            this.sp_HR_00002_Employee_Depts_Worked_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00002_Employee_Depts_Worked_EditTableAdapter();
            this.sp_HR_00004_Location_Base_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00004_Location_Base_Descriptors_With_BlankTableAdapter();
            this.sp_HR_00005_Employee_Positions_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00005_Employee_Positions_With_BlankTableAdapter();
            this.sp_HR_00015_Cost_Centre_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00015_Cost_Centre_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00002EmployeeDeptsWorkedEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00015CostCentreListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsToStaffButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsToStaffIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PositionIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00005EmployeePositionsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00004LocationBaseDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentLocationIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeDeptWorkedIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimePercentageSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeDeptWorkedID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentLocationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportsToStaffID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPositionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportsToStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimePercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBaseTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(861, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 554);
            this.barDockControlBottom.Size = new System.Drawing.Size(861, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 528);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(861, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 528);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(861, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 554);
            this.barDockControl2.Size = new System.Drawing.Size(861, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 528);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(861, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 528);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.CostCentreIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.checkEdit3);
            this.dataLayoutControl1.Controls.Add(this.ToDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.FromDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportsToStaffButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ReportsToStaffIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PositionIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.BaseTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DepartmentNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DepartmentLocationIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BusinessAreaNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeDeptWorkedIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.TimePercentageSpinEdit);
            this.dataLayoutControl1.DataSource = this.spHR00002EmployeeDeptsWorkedEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmployeeDeptWorkedID,
            this.ItemForDepartmentLocationID,
            this.ItemForRegionID,
            this.ItemForReportsToStaffID,
            this.layoutControlItem4,
            this.ItemForEmployeeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(112, 83, 460, 571);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(861, 528);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // CostCentreIDGridLookUpEdit
            // 
            this.CostCentreIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "CostCentreID", true));
            this.CostCentreIDGridLookUpEdit.Location = new System.Drawing.Point(144, 357);
            this.CostCentreIDGridLookUpEdit.MenuManager = this.barManager1;
            this.CostCentreIDGridLookUpEdit.Name = "CostCentreIDGridLookUpEdit";
            this.CostCentreIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CostCentreIDGridLookUpEdit.Properties.DataSource = this.spHR00015CostCentreListWithBlankBindingSource;
            this.CostCentreIDGridLookUpEdit.Properties.DisplayMember = "Code";
            this.CostCentreIDGridLookUpEdit.Properties.NullText = "";
            this.CostCentreIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemMemoExEdit1});
            this.CostCentreIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.CostCentreIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.CostCentreIDGridLookUpEdit.Size = new System.Drawing.Size(681, 20);
            this.CostCentreIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.CostCentreIDGridLookUpEdit.TabIndex = 51;
            // 
            // spHR00002EmployeeDeptsWorkedEditBindingSource
            // 
            this.spHR00002EmployeeDeptsWorkedEditBindingSource.DataMember = "sp_HR_00002_Employee_Depts_Worked_Edit";
            this.spHR00002EmployeeDeptsWorkedEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spHR00015CostCentreListWithBlankBindingSource
            // 
            this.spHR00015CostCentreListWithBlankBindingSource.DataMember = "sp_HR_00015_Cost_Centre_List_With_Blank";
            this.spHR00015CostCentreListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.colActive,
            this.colCode,
            this.colRemarks1});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn4;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.Column = this.colActive;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1,
            styleFormatCondition2});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCode, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Description";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 233;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colCode
            // 
            this.colCode.Caption = "Code";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            this.colCode.OptionsColumn.AllowEdit = false;
            this.colCode.OptionsColumn.AllowFocus = false;
            this.colCode.OptionsColumn.ReadOnly = true;
            this.colCode.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 0;
            this.colCode.Width = 214;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 2;
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "Active", true));
            this.ActiveCheckEdit.Location = new System.Drawing.Point(144, 381);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "(Tick if Active)";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(681, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 48;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(477, 363);
            this.checkEdit3.MenuManager = this.barManager1;
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "checkEdit3";
            this.checkEdit3.Size = new System.Drawing.Size(216, 19);
            this.checkEdit3.StyleController = this.dataLayoutControl1;
            this.checkEdit3.TabIndex = 44;
            // 
            // ToDateDateEdit
            // 
            this.ToDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "ToDate", true));
            this.ToDateDateEdit.EditValue = null;
            this.ToDateDateEdit.Location = new System.Drawing.Point(144, 333);
            this.ToDateDateEdit.MenuManager = this.barManager1;
            this.ToDateDateEdit.Name = "ToDateDateEdit";
            this.ToDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ToDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ToDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ToDateDateEdit.Size = new System.Drawing.Size(681, 20);
            this.ToDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ToDateDateEdit.TabIndex = 41;
            // 
            // FromDateDateEdit
            // 
            this.FromDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "FromDate", true));
            this.FromDateDateEdit.EditValue = null;
            this.FromDateDateEdit.Location = new System.Drawing.Point(144, 309);
            this.FromDateDateEdit.MenuManager = this.barManager1;
            this.FromDateDateEdit.Name = "FromDateDateEdit";
            this.FromDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FromDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FromDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.FromDateDateEdit.Size = new System.Drawing.Size(681, 20);
            this.FromDateDateEdit.StyleController = this.dataLayoutControl1;
            this.FromDateDateEdit.TabIndex = 40;
            // 
            // ReportsToStaffButtonEdit
            // 
            this.ReportsToStaffButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "ReportsToStaff", true));
            this.ReportsToStaffButtonEdit.Location = new System.Drawing.Point(144, 285);
            this.ReportsToStaffButtonEdit.MenuManager = this.barManager1;
            this.ReportsToStaffButtonEdit.Name = "ReportsToStaffButtonEdit";
            this.ReportsToStaffButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to Select Staff", "choose", null, true),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to Clear Reports To value.", "clear", null, true)});
            this.ReportsToStaffButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ReportsToStaffButtonEdit.Size = new System.Drawing.Size(681, 20);
            this.ReportsToStaffButtonEdit.StyleController = this.dataLayoutControl1;
            this.ReportsToStaffButtonEdit.TabIndex = 39;
            this.ReportsToStaffButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ReportsToStaffButtonEdit_ButtonClick);
            // 
            // ReportsToStaffIDTextEdit
            // 
            this.ReportsToStaffIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "ReportsToStaffID", true));
            this.ReportsToStaffIDTextEdit.Location = new System.Drawing.Point(123, 179);
            this.ReportsToStaffIDTextEdit.MenuManager = this.barManager1;
            this.ReportsToStaffIDTextEdit.Name = "ReportsToStaffIDTextEdit";
            this.ReportsToStaffIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ReportsToStaffIDTextEdit, true);
            this.ReportsToStaffIDTextEdit.Size = new System.Drawing.Size(606, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ReportsToStaffIDTextEdit, optionsSpelling1);
            this.ReportsToStaffIDTextEdit.StyleController = this.dataLayoutControl1;
            this.ReportsToStaffIDTextEdit.TabIndex = 38;
            // 
            // PositionIDGridLookUpEdit
            // 
            this.PositionIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "PositionID", true));
            this.PositionIDGridLookUpEdit.Location = new System.Drawing.Point(144, 261);
            this.PositionIDGridLookUpEdit.MenuManager = this.barManager1;
            this.PositionIDGridLookUpEdit.Name = "PositionIDGridLookUpEdit";
            this.PositionIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PositionIDGridLookUpEdit.Properties.DataSource = this.spHR00005EmployeePositionsWithBlankBindingSource;
            this.PositionIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.PositionIDGridLookUpEdit.Properties.NullText = "";
            this.PositionIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.PositionIDGridLookUpEdit.Properties.View = this.gridView2;
            this.PositionIDGridLookUpEdit.Size = new System.Drawing.Size(681, 20);
            this.PositionIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.PositionIDGridLookUpEdit.TabIndex = 37;
            // 
            // spHR00005EmployeePositionsWithBlankBindingSource
            // 
            this.spHR00005EmployeePositionsWithBlankBindingSource.DataMember = "sp_HR_00005_Employee_Positions_With_Blank";
            this.spHR00005EmployeePositionsWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn1;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridView2.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Description";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 206;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // BaseTypeIDGridLookUpEdit
            // 
            this.BaseTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "BaseTypeID", true));
            this.BaseTypeIDGridLookUpEdit.Location = new System.Drawing.Point(120, 155);
            this.BaseTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.BaseTypeIDGridLookUpEdit.Name = "BaseTypeIDGridLookUpEdit";
            this.BaseTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BaseTypeIDGridLookUpEdit.Properties.DataSource = this.spHR00004LocationBaseDescriptorsWithBlankBindingSource;
            this.BaseTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.BaseTypeIDGridLookUpEdit.Properties.NullText = "";
            this.BaseTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.BaseTypeIDGridLookUpEdit.Properties.View = this.gridView1;
            this.BaseTypeIDGridLookUpEdit.Size = new System.Drawing.Size(729, 20);
            this.BaseTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.BaseTypeIDGridLookUpEdit.TabIndex = 36;
            // 
            // spHR00004LocationBaseDescriptorsWithBlankBindingSource
            // 
            this.spHR00004LocationBaseDescriptorsWithBlankBindingSource.DataMember = "sp_HR_00004_Location_Base_Descriptors_With_Blank";
            this.spHR00004LocationBaseDescriptorsWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.colID1;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Base Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // LocationNameTextEdit
            // 
            this.LocationNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "LocationName", true));
            this.LocationNameTextEdit.Location = new System.Drawing.Point(120, 107);
            this.LocationNameTextEdit.MenuManager = this.barManager1;
            this.LocationNameTextEdit.Name = "LocationNameTextEdit";
            this.LocationNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationNameTextEdit, true);
            this.LocationNameTextEdit.Size = new System.Drawing.Size(729, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationNameTextEdit, optionsSpelling2);
            this.LocationNameTextEdit.StyleController = this.dataLayoutControl1;
            this.LocationNameTextEdit.TabIndex = 35;
            // 
            // DepartmentNameTextEdit
            // 
            this.DepartmentNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "DepartmentName", true));
            this.DepartmentNameTextEdit.Location = new System.Drawing.Point(120, 83);
            this.DepartmentNameTextEdit.MenuManager = this.barManager1;
            this.DepartmentNameTextEdit.Name = "DepartmentNameTextEdit";
            this.DepartmentNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DepartmentNameTextEdit, true);
            this.DepartmentNameTextEdit.Size = new System.Drawing.Size(729, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DepartmentNameTextEdit, optionsSpelling3);
            this.DepartmentNameTextEdit.StyleController = this.dataLayoutControl1;
            this.DepartmentNameTextEdit.TabIndex = 34;
            // 
            // RegionNameButtonEdit
            // 
            this.RegionNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "RegionName", true));
            this.RegionNameButtonEdit.Location = new System.Drawing.Point(120, 131);
            this.RegionNameButtonEdit.MenuManager = this.barManager1;
            this.RegionNameButtonEdit.Name = "RegionNameButtonEdit";
            this.RegionNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to Select Region", "choose", null, true)});
            this.RegionNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.RegionNameButtonEdit.Size = new System.Drawing.Size(729, 20);
            this.RegionNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.RegionNameButtonEdit.TabIndex = 33;
            this.RegionNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.RegionNameButtonEdit_ButtonClick);
            // 
            // RegionIDTextEdit
            // 
            this.RegionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "RegionID", true));
            this.RegionIDTextEdit.Location = new System.Drawing.Point(123, 83);
            this.RegionIDTextEdit.MenuManager = this.barManager1;
            this.RegionIDTextEdit.Name = "RegionIDTextEdit";
            this.RegionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionIDTextEdit, true);
            this.RegionIDTextEdit.Size = new System.Drawing.Size(606, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionIDTextEdit, optionsSpelling4);
            this.RegionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionIDTextEdit.TabIndex = 32;
            // 
            // DepartmentLocationIDTextEdit
            // 
            this.DepartmentLocationIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "DepartmentLocationID", true));
            this.DepartmentLocationIDTextEdit.Location = new System.Drawing.Point(133, 83);
            this.DepartmentLocationIDTextEdit.MenuManager = this.barManager1;
            this.DepartmentLocationIDTextEdit.Name = "DepartmentLocationIDTextEdit";
            this.DepartmentLocationIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DepartmentLocationIDTextEdit, true);
            this.DepartmentLocationIDTextEdit.Size = new System.Drawing.Size(596, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DepartmentLocationIDTextEdit, optionsSpelling5);
            this.DepartmentLocationIDTextEdit.StyleController = this.dataLayoutControl1;
            this.DepartmentLocationIDTextEdit.TabIndex = 31;
            // 
            // BusinessAreaNameButtonEdit
            // 
            this.BusinessAreaNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "BusinessAreaName", true));
            this.BusinessAreaNameButtonEdit.Location = new System.Drawing.Point(120, 59);
            this.BusinessAreaNameButtonEdit.MenuManager = this.barManager1;
            this.BusinessAreaNameButtonEdit.Name = "BusinessAreaNameButtonEdit";
            this.BusinessAreaNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to Select Business Area, Department and Office", "choose", null, true)});
            this.BusinessAreaNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.BusinessAreaNameButtonEdit.Size = new System.Drawing.Size(729, 20);
            this.BusinessAreaNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.BusinessAreaNameButtonEdit.TabIndex = 30;
            this.BusinessAreaNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.BusinessAreaNameButtonEdit_ButtonClick);
            this.BusinessAreaNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.BusinessAreaNameButtonEdit_Validating);
            // 
            // EmployeeDeptWorkedIDTextEdit
            // 
            this.EmployeeDeptWorkedIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "EmployeeDeptWorkedID", true));
            this.EmployeeDeptWorkedIDTextEdit.Location = new System.Drawing.Point(145, 59);
            this.EmployeeDeptWorkedIDTextEdit.MenuManager = this.barManager1;
            this.EmployeeDeptWorkedIDTextEdit.Name = "EmployeeDeptWorkedIDTextEdit";
            this.EmployeeDeptWorkedIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeDeptWorkedIDTextEdit, true);
            this.EmployeeDeptWorkedIDTextEdit.Size = new System.Drawing.Size(584, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeDeptWorkedIDTextEdit, optionsSpelling6);
            this.EmployeeDeptWorkedIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeDeptWorkedIDTextEdit.TabIndex = 19;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00002EmployeeDeptsWorkedEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(120, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(177, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 17;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // EmployeeIDTextEdit
            // 
            this.EmployeeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "EmployeeID", true));
            this.EmployeeIDTextEdit.Location = new System.Drawing.Point(149, 179);
            this.EmployeeIDTextEdit.MenuManager = this.barManager1;
            this.EmployeeIDTextEdit.Name = "EmployeeIDTextEdit";
            this.EmployeeIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.EmployeeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIDTextEdit, true);
            this.EmployeeIDTextEdit.Size = new System.Drawing.Size(563, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIDTextEdit, optionsSpelling7);
            this.EmployeeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIDTextEdit.TabIndex = 4;
            this.EmployeeIDTextEdit.TabStop = false;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 261);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(789, 173);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling8);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 27;
            // 
            // EmployeeNameButtonEdit
            // 
            this.EmployeeNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "EmployeeName", true));
            this.EmployeeNameButtonEdit.EditValue = "";
            this.EmployeeNameButtonEdit.Location = new System.Drawing.Point(120, 35);
            this.EmployeeNameButtonEdit.MenuManager = this.barManager1;
            this.EmployeeNameButtonEdit.Name = "EmployeeNameButtonEdit";
            this.EmployeeNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click to open Select Employee screen", "choose", null, true)});
            this.EmployeeNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EmployeeNameButtonEdit.Size = new System.Drawing.Size(729, 20);
            this.EmployeeNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameButtonEdit.TabIndex = 28;
            this.EmployeeNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EmployeeContractButtonEdit_ButtonClick);
            this.EmployeeNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EmployeeContractButtonEdit_Validating);
            // 
            // TimePercentageSpinEdit
            // 
            this.TimePercentageSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00002EmployeeDeptsWorkedEditBindingSource, "TimePercentage", true));
            this.TimePercentageSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.TimePercentageSpinEdit.Location = new System.Drawing.Point(144, 404);
            this.TimePercentageSpinEdit.MenuManager = this.barManager1;
            this.TimePercentageSpinEdit.Name = "TimePercentageSpinEdit";
            this.TimePercentageSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TimePercentageSpinEdit.Properties.IsFloatValue = false;
            this.TimePercentageSpinEdit.Properties.Mask.EditMask = "P";
            this.TimePercentageSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TimePercentageSpinEdit.Properties.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.TimePercentageSpinEdit.Size = new System.Drawing.Size(681, 20);
            this.TimePercentageSpinEdit.StyleController = this.dataLayoutControl1;
            this.TimePercentageSpinEdit.TabIndex = 24;
            this.TimePercentageSpinEdit.ToolTip = "The combined length of all breaks, in minutes";
            // 
            // ItemForEmployeeDeptWorkedID
            // 
            this.ItemForEmployeeDeptWorkedID.Control = this.EmployeeDeptWorkedIDTextEdit;
            this.ItemForEmployeeDeptWorkedID.CustomizationFormText = "Employee Dept Worked ID:";
            this.ItemForEmployeeDeptWorkedID.Location = new System.Drawing.Point(0, 47);
            this.ItemForEmployeeDeptWorkedID.Name = "ItemForEmployeeDeptWorkedID";
            this.ItemForEmployeeDeptWorkedID.Size = new System.Drawing.Size(721, 24);
            this.ItemForEmployeeDeptWorkedID.Text = "Employee Dept Worked ID:";
            this.ItemForEmployeeDeptWorkedID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForDepartmentLocationID
            // 
            this.ItemForDepartmentLocationID.Control = this.DepartmentLocationIDTextEdit;
            this.ItemForDepartmentLocationID.CustomizationFormText = "Department Location ID:";
            this.ItemForDepartmentLocationID.Location = new System.Drawing.Point(0, 71);
            this.ItemForDepartmentLocationID.Name = "ItemForDepartmentLocationID";
            this.ItemForDepartmentLocationID.Size = new System.Drawing.Size(721, 24);
            this.ItemForDepartmentLocationID.Text = "Department Location ID:";
            this.ItemForDepartmentLocationID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRegionID
            // 
            this.ItemForRegionID.Control = this.RegionIDTextEdit;
            this.ItemForRegionID.CustomizationFormText = "Region ID:";
            this.ItemForRegionID.Location = new System.Drawing.Point(0, 71);
            this.ItemForRegionID.Name = "ItemForRegionID";
            this.ItemForRegionID.Size = new System.Drawing.Size(721, 24);
            this.ItemForRegionID.Text = "Region ID:";
            this.ItemForRegionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForReportsToStaffID
            // 
            this.ItemForReportsToStaffID.Control = this.ReportsToStaffIDTextEdit;
            this.ItemForReportsToStaffID.CustomizationFormText = "Reports To Staff ID:";
            this.ItemForReportsToStaffID.Location = new System.Drawing.Point(0, 167);
            this.ItemForReportsToStaffID.Name = "ItemForReportsToStaffID";
            this.ItemForReportsToStaffID.Size = new System.Drawing.Size(721, 24);
            this.ItemForReportsToStaffID.Text = "Reports To Staff ID:";
            this.ItemForReportsToStaffID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.checkEdit3;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 70);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(220, 23);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // ItemForEmployeeID
            // 
            this.ItemForEmployeeID.Control = this.EmployeeIDTextEdit;
            this.ItemForEmployeeID.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeID.Location = new System.Drawing.Point(0, 167);
            this.ItemForEmployeeID.Name = "ItemForEmployeeID";
            this.ItemForEmployeeID.Size = new System.Drawing.Size(704, 24);
            this.ItemForEmployeeID.Text = "Employee ID:";
            this.ItemForEmployeeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(861, 528);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.ItemForEmployeeName,
            this.ItemForBusinessAreaName,
            this.ItemForRegionName,
            this.ItemForDepartmentName,
            this.ItemForLocationName,
            this.ItemForBaseTypeID,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(841, 508);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 179);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(841, 271);
            this.layoutControlGroup5.Text = "Details";
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(817, 225);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details:";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForPositionID,
            this.ItemForReportsToStaff,
            this.ItemForFromDate,
            this.ItemForToDate,
            this.ItemForActive,
            this.emptySpaceItem4,
            this.ItemForCostCentreID,
            this.ItemForTimePercentage});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(793, 177);
            this.layoutControlGroup4.Text = "Details";
            // 
            // ItemForPositionID
            // 
            this.ItemForPositionID.Control = this.PositionIDGridLookUpEdit;
            this.ItemForPositionID.CustomizationFormText = "Position:";
            this.ItemForPositionID.Location = new System.Drawing.Point(0, 0);
            this.ItemForPositionID.Name = "ItemForPositionID";
            this.ItemForPositionID.Size = new System.Drawing.Size(793, 24);
            this.ItemForPositionID.Text = "Position:";
            this.ItemForPositionID.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForReportsToStaff
            // 
            this.ItemForReportsToStaff.Control = this.ReportsToStaffButtonEdit;
            this.ItemForReportsToStaff.CustomizationFormText = "Reports To:";
            this.ItemForReportsToStaff.Location = new System.Drawing.Point(0, 24);
            this.ItemForReportsToStaff.Name = "ItemForReportsToStaff";
            this.ItemForReportsToStaff.Size = new System.Drawing.Size(793, 24);
            this.ItemForReportsToStaff.Text = "Reports To:";
            this.ItemForReportsToStaff.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForFromDate
            // 
            this.ItemForFromDate.Control = this.FromDateDateEdit;
            this.ItemForFromDate.CustomizationFormText = "From Date:";
            this.ItemForFromDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForFromDate.Name = "ItemForFromDate";
            this.ItemForFromDate.Size = new System.Drawing.Size(793, 24);
            this.ItemForFromDate.Text = "From Date (Optional):";
            this.ItemForFromDate.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForToDate
            // 
            this.ItemForToDate.Control = this.ToDateDateEdit;
            this.ItemForToDate.CustomizationFormText = "To Date:";
            this.ItemForToDate.Location = new System.Drawing.Point(0, 72);
            this.ItemForToDate.Name = "ItemForToDate";
            this.ItemForToDate.Size = new System.Drawing.Size(793, 24);
            this.ItemForToDate.Text = "To Date (Optional):";
            this.ItemForToDate.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForActive
            // 
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.CustomizationFormText = "layoutControlItem8";
            this.ItemForActive.Location = new System.Drawing.Point(0, 120);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(793, 23);
            this.ItemForActive.Text = "Active:";
            this.ItemForActive.TextSize = new System.Drawing.Size(105, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(793, 10);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCostCentreID
            // 
            this.ItemForCostCentreID.Control = this.CostCentreIDGridLookUpEdit;
            this.ItemForCostCentreID.CustomizationFormText = "Cost Centre:";
            this.ItemForCostCentreID.Location = new System.Drawing.Point(0, 96);
            this.ItemForCostCentreID.Name = "ItemForCostCentreID";
            this.ItemForCostCentreID.Size = new System.Drawing.Size(793, 24);
            this.ItemForCostCentreID.Text = "Cost Centre:";
            this.ItemForCostCentreID.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForTimePercentage
            // 
            this.ItemForTimePercentage.Control = this.TimePercentageSpinEdit;
            this.ItemForTimePercentage.CustomizationFormText = "Percentage of Time in Dept:";
            this.ItemForTimePercentage.Location = new System.Drawing.Point(0, 143);
            this.ItemForTimePercentage.Name = "ItemForTimePercentage";
            this.ItemForTimePercentage.Size = new System.Drawing.Size(793, 24);
            this.ItemForTimePercentage.Text = "% of Time in Dept:";
            this.ItemForTimePercentage.TextSize = new System.Drawing.Size(105, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks:";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(793, 177);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(793, 177);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(841, 12);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(108, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(181, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(108, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(108, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(108, 23);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(289, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(552, 23);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.AllowHide = false;
            this.ItemForEmployeeName.Control = this.EmployeeNameButtonEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Employee Name:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 23);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(841, 24);
            this.ItemForEmployeeName.Text = "Employee Name:";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForBusinessAreaName
            // 
            this.ItemForBusinessAreaName.AllowHide = false;
            this.ItemForBusinessAreaName.Control = this.BusinessAreaNameButtonEdit;
            this.ItemForBusinessAreaName.CustomizationFormText = "Business Area:";
            this.ItemForBusinessAreaName.Location = new System.Drawing.Point(0, 47);
            this.ItemForBusinessAreaName.Name = "ItemForBusinessAreaName";
            this.ItemForBusinessAreaName.Size = new System.Drawing.Size(841, 24);
            this.ItemForBusinessAreaName.Text = "Business Area:";
            this.ItemForBusinessAreaName.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForRegionName
            // 
            this.ItemForRegionName.Control = this.RegionNameButtonEdit;
            this.ItemForRegionName.CustomizationFormText = "Region:";
            this.ItemForRegionName.Location = new System.Drawing.Point(0, 119);
            this.ItemForRegionName.Name = "ItemForRegionName";
            this.ItemForRegionName.Size = new System.Drawing.Size(841, 24);
            this.ItemForRegionName.Text = "Region:";
            this.ItemForRegionName.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForDepartmentName
            // 
            this.ItemForDepartmentName.Control = this.DepartmentNameTextEdit;
            this.ItemForDepartmentName.CustomizationFormText = "Department:";
            this.ItemForDepartmentName.Location = new System.Drawing.Point(0, 71);
            this.ItemForDepartmentName.Name = "ItemForDepartmentName";
            this.ItemForDepartmentName.Size = new System.Drawing.Size(841, 24);
            this.ItemForDepartmentName.Text = "Department:";
            this.ItemForDepartmentName.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForLocationName
            // 
            this.ItemForLocationName.Control = this.LocationNameTextEdit;
            this.ItemForLocationName.CustomizationFormText = "Location:";
            this.ItemForLocationName.Location = new System.Drawing.Point(0, 95);
            this.ItemForLocationName.Name = "ItemForLocationName";
            this.ItemForLocationName.Size = new System.Drawing.Size(841, 24);
            this.ItemForLocationName.Text = "Location:";
            this.ItemForLocationName.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForBaseTypeID
            // 
            this.ItemForBaseTypeID.Control = this.BaseTypeIDGridLookUpEdit;
            this.ItemForBaseTypeID.CustomizationFormText = "Base Type:";
            this.ItemForBaseTypeID.Location = new System.Drawing.Point(0, 143);
            this.ItemForBaseTypeID.Name = "ItemForBaseTypeID";
            this.ItemForBaseTypeID.Size = new System.Drawing.Size(841, 24);
            this.ItemForBaseTypeID.Text = "Base Type:";
            this.ItemForBaseTypeID.TextSize = new System.Drawing.Size(105, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 450);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(841, 58);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_HR_00002_Employee_Depts_Worked_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00013_Get_Employee_Address_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00017_Pension_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00019_Get_Employee_Next_Of_Kin_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00023_Master_Salary_Bandings_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00040_Employee_Bonus_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00085_Master_Holiday_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00093_Business_Area_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00095_Location_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00097_Department_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00100_Region_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00102_Department_Location_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00132_Employee_Vetting_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00137_Employee_Vetting_Restriction_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00143_Master_Vetting_Types_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00145_Master_Vetting_SubType_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00148_Master_Vetting_Restriction_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00155_Employee_Working_Pattern_Headers_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00164_Master_Working_Pattern_Headers_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00166_Master_Working_Pattern_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00175_Employee_Pension_Contribution_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00182_Linked_Document_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00194_Employee_CRM_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00201_Employee_Reference_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00214_Employee_PayRise_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00226_Employee_Shares_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00232_Master_Qualification_Types_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00250_Employee_Administer_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00255_Business_Area_Insurance_Requirement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00258_Employee_Subsistence_EditTableAdapter = null;
            this.tableAdapterManager.sp09002_HR_Employee_ItemTableAdapter = null;
            this.tableAdapterManager.sp09105_HR_Qualification_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_HR_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_HR_00002_Employee_Depts_Worked_EditTableAdapter
            // 
            this.sp_HR_00002_Employee_Depts_Worked_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00004_Location_Base_Descriptors_With_BlankTableAdapter
            // 
            this.sp_HR_00004_Location_Base_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00005_Employee_Positions_With_BlankTableAdapter
            // 
            this.sp_HR_00005_Employee_Positions_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00015_Cost_Centre_List_With_BlankTableAdapter
            // 
            this.sp_HR_00015_Cost_Centre_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Employee_Department_Worked_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(861, 584);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Department_Worked_Edit";
            this.Text = "Edit Employee Department Worked";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Department_Worked_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Department_Worked_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Department_Worked_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CostCentreIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00002EmployeeDeptsWorkedEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00015CostCentreListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ToDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FromDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsToStaffButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReportsToStaffIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PositionIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00005EmployeePositionsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BaseTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00004LocationBaseDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentLocationIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeDeptWorkedIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimePercentageSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeDeptWorkedID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentLocationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportsToStaffID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPositionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForReportsToStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFromDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForToDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCostCentreID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimePercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBaseTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeID;
        private DevExpress.XtraEditors.TextEdit EmployeeIDTextEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private WoodPlanDataSet woodPlanDataSet;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit EmployeeDeptWorkedIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeDeptWorkedID;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DataSet_HR_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraEditors.ButtonEdit EmployeeNameButtonEdit;
        private DevExpress.XtraEditors.SpinEdit TimePercentageSpinEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTimePercentage;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00002EmployeeDeptsWorkedEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00002_Employee_Depts_Worked_EditTableAdapter sp_HR_00002_Employee_Depts_Worked_EditTableAdapter;
        private DevExpress.XtraEditors.ButtonEdit BusinessAreaNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBusinessAreaName;
        private DevExpress.XtraEditors.TextEdit DepartmentLocationIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDepartmentLocationID;
        private DevExpress.XtraEditors.TextEdit RegionIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionID;
        private DevExpress.XtraEditors.ButtonEdit RegionNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionName;
        private DevExpress.XtraEditors.TextEdit LocationNameTextEdit;
        private DevExpress.XtraEditors.TextEdit DepartmentNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDepartmentName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocationName;
        private DevExpress.XtraEditors.GridLookUpEdit BaseTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBaseTypeID;
        private DevExpress.XtraEditors.GridLookUpEdit PositionIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPositionID;
        private DevExpress.XtraEditors.TextEdit ReportsToStaffIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportsToStaffID;
        private DevExpress.XtraEditors.ButtonEdit ReportsToStaffButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForReportsToStaff;
        private DevExpress.XtraEditors.DateEdit FromDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFromDate;
        private DevExpress.XtraEditors.DateEdit ToDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForToDate;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private System.Windows.Forms.BindingSource spHR00004LocationBaseDescriptorsWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00004_Location_Base_Descriptors_With_BlankTableAdapter sp_HR_00004_Location_Base_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private System.Windows.Forms.BindingSource spHR00005EmployeePositionsWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00005_Employee_Positions_With_BlankTableAdapter sp_HR_00005_Employee_Positions_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.GridLookUpEdit CostCentreIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCostCentreID;
        private System.Windows.Forms.BindingSource spHR00015CostCentreListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00015_Cost_Centre_List_With_BlankTableAdapter sp_HR_00015_Cost_Centre_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
    }
}

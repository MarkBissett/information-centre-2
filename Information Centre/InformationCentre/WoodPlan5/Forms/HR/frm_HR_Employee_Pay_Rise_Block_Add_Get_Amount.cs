using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using WoodPlan5.Properties;
using BaseObjects;


namespace WoodPlan5
{
    public partial class frm_HR_Employee_Pay_Rise_Block_Add_Get_Amount : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public int intRecordCount = 0;
        public string strEmployeeIDs = "";
        public int intAppliedByStaffID = 0;
        public string strAppliedByStaffName = "";
        public int intPayRiseType = 0;
        public decimal decPayRiseAmount = (decimal)0.00;
        public DateTime dtDateApplied;

        #endregion

        public frm_HR_Employee_Pay_Rise_Block_Add_Get_Amount()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Pay_Rise_Block_Add_Get_Amount_Load(object sender, EventArgs e)
        {
            this.FormID = 500116;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            barStaticItemInformation.Caption = "Employees Selected For Blocking Adding To: " + intRecordCount.ToString();
            intAppliedByStaffID = GlobalSettings.UserID;
            strAppliedByStaffName = GlobalSettings.UserSurname + ": " + GlobalSettings.UserForename;
            dtDateApplied = DateTime.Now;

            AppliedByPersonNameButtonEdit.Text = strAppliedByStaffName;
            FromDate.DateTime = dtDateApplied;

            this.ValidateChildren();  // Force Validation Message on any controls containing them //     
        }

        private void checkEdit1_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditAmount.Properties.Mask.EditMask = "P";  // Percentage //
                intPayRiseType = 0;
            }
            else
            {
                spinEditAmount.Properties.Mask.EditMask = "c";  // Currency //
                intPayRiseType = 1;
            }
        }

        private void checkEdit2_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit ce = (CheckEdit)sender;
            if (ce.Checked)
            {
                spinEditAmount.Properties.Mask.EditMask = "c";  // Currency //
                intPayRiseType = 1;
            }
            else
            {
                spinEditAmount.Properties.Mask.EditMask = "P";  // Percentage //
                intPayRiseType = 0;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.ValidateChildren();  // Force Validation Message on any controls containing them //     

            if (dxErrorProvider1.HasErrors)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the screen!\n\nTip: Errors have a stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Block Add Employee Pay Rise", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (Convert.ToDecimal(spinEditAmount.EditValue) == (decimal)0.00)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter the Uplift Amount, Date and Person Applying the Pay Rise before proceeding.", "Block Add Employee Pay Rise", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            decPayRiseAmount = Convert.ToDecimal(spinEditAmount.EditValue);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void FromDate_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(FromDate, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(FromDate, "");
            }
        }

        private void spinEditAmount_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if (se.EditValue == null || Convert.ToInt32(se.EditValue) == 0)
            {
                dxErrorProvider1.SetError(spinEditAmount, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(spinEditAmount, "");
            }
        }

        private void AppliedByPersonNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (be.EditValue == null || string.IsNullOrWhiteSpace(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(AppliedByPersonNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AppliedByPersonNameButtonEdit, "");
            }
        }

        private void AppliedByPersonNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_HR_Select_Employee();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = intAppliedByStaffID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    intAppliedByStaffID = fChildForm.intSelectedEmployeeID;
                    strAppliedByStaffName = fChildForm.strSelectedEmployeeName;
                    AppliedByPersonNameButtonEdit.Text = strAppliedByStaffName;
                }
            }

        }




    
    }
}


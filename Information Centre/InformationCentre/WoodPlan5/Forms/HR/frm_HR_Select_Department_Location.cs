using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_HR_Select_Department_Location : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public int intPassedInBusinessAreaID = 0;
        public int intPassedInDepartmentID = 0;
        public int intPassedInDepartmentLocationID = 0;
        
        public int intSelectedBusinessAreaID = 0;
        public int intSelectedDepartmentID = 0;
        public int intSelectedDepartmentLocationID = 0;

        public string strSelectedValue = "";
        public string strSelectedBusinessAreaName = "";
        public string strSelectedDepartmentName = "";
        public string strSelectedLocationName = "";

        GridHitInfo downHitInfo = null;

        #endregion

        public frm_HR_Select_Department_Location()
        {
            InitializeComponent();
        }

        private void frm_HR_Select_Department_Location_Load(object sender, EventArgs e)
        {
            this.FormID = 990115;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp_HR_00007_Departments_For_Business_AreaTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00008_Department_Locations_For_DepartmentTableAdapter.Connection.ConnectionString = strConnectionString;

            try
            {
                sp_HR_00006_Business_AreasTableAdapter.Connection.ConnectionString = strConnectionString;
                sp_HR_00006_Business_AreasTableAdapter.Fill(dataSet_HR_Core.sp_HR_00006_Business_Areas);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Business Areas list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInBusinessAreaID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["BusinessAreaID"], intPassedInBusinessAreaID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            if (intPassedInDepartmentID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["DepartmentID"], intPassedInDepartmentID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl3.ForceInitialize();
            view = (GridView)gridControl3.MainView;
            if (intPassedInDepartmentLocationID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["DepartmentLocationID"], intPassedInDepartmentLocationID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        bool internalRowFocusing;
 

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Business Areas Available";
                    break;
                case "gridView2":
                    message = "No Departments Available For Selection - Select a Business Area to see Related Departments";
                    break;
                case "gridView3":
                    message = "No Department Locations Available For Selection - Select a Department to see Related Department Locations";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                case "gridView2":
                    LoadLinkedData2();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["BusinessAreaID"])) + ',';
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00007_Departments_For_Business_Area.Clear();
            }
            else
            {
                try
                {
                    sp_HR_00007_Departments_For_Business_AreaTableAdapter.Fill(dataSet_HR_Core.sp_HR_00007_Departments_For_Business_Area, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related departments.\n\nTry selecting a business area again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void LoadLinkedData2()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["DepartmentID"])) + ',';
            }

            gridControl3.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00008_Department_Locations_For_Department.Clear();
            }
            else
            {
                try
                {
                    sp_HR_00008_Department_Locations_For_DepartmentTableAdapter.Fill(dataSet_HR_Core.sp_HR_00008_Department_Locations_For_Department, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related department locations.\n\nTry selecting a department again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl3.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedValue))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl3.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedValue = "Business Area: " + (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "BusinessAreaName"))) ? "Unknown Business Area" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "BusinessAreaName"))) + " - " +
                        (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "DepartmentName"))) ? "Unknown Department" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "DepartmentName"))) + " - " +
                                (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "LocationName"))) ? "Unknown Location" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "LocationName")));
                strSelectedBusinessAreaName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "BusinessAreaName"))) ? "Unknown Business Area" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "BusinessAreaName")));
                strSelectedDepartmentName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "DepartmentName"))) ? "Unknown Department" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "DepartmentName")));
                strSelectedLocationName = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "LocationName"))) ? "Unknown Location" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "LocationName")));
                intSelectedBusinessAreaID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BusinessAreaID"));
                intSelectedDepartmentID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "DepartmentID"));
                intSelectedDepartmentLocationID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "DepartmentLocationID"));
            }
        }



    }
}


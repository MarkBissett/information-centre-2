namespace WoodPlan5
{
    partial class frm_HR_Sanction_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Sanction_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions4 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject14 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject15 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject16 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions5 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject17 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject18 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject19 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject20 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions6 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject21 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject22 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject23 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject24 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions7 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject25 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject26 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject27 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject28 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions8 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject29 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject30 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject31 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject32 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions9 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject33 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject34 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject35 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject36 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions10 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject37 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject38 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject39 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject40 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions11 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject41 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject42 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject43 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject44 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions12 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject45 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject46 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject47 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject48 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling15 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling16 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions15 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling17 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions16 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions17 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.AppealNoteTakerPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.spHR00055GetEmployeeSanctionItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.HearingNoteTakerPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HearingOutcomeIssuedByPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.InvestigatedByPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SanctionOutcomeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AppealEmployeeRepresentativeNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HearingOutcomeIssuedByPersonNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.HearingOutcomeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00130SanctionHearingOutcomeListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.HearingEmployeeRepresentativeNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.InvestigationOutcomeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00129DisciplineInvestigationOutcomesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InvestigatedByPersonNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.InvestigationEndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.InvestigationStartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.HRManagerNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.HRManagerIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AppealHeardByPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SanctionIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RaisedByPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HeardByPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AppealHeardByPersonNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AppealOutcomeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00039AppealOutcomesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AppealDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00075SanctionDurationUnitsDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.StatusIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00074SanctionStatusesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ERIssueTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00073SanctionTypesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EmployeeNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00057GetEmployeeSanctionProgressBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSanctionId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDetail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClosedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartedById = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedById = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.EmployeeFirstnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeSurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.DateRaisedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.HearingOutcomeDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ExpiryDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.LiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ExpiryDurationUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RaisedByPersonNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.HeardByPersonNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.HearingDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.HearingNoteTakerNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AppealNoteTakerNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForSanctionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeFirstname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHRManagerID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRaisedByPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvestigatedByPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHeardByPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHearingOutcomeIssuedByPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAppealConductedByID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHearingNoteTakerPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAppealNoteTakerPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForERIssueTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDateRaised = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLive = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRaisedByPersonName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStatusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSanctionOutcome = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForInvestigationStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvestigationEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvestigatedByPersonName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInvestigationOutcomeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layGrpHearing = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHeardByPersonName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHearingDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHearingNoteTakerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForHearingEmployeeRepresentativeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHearingOutcomeDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHearingOutcomeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHearingOutcomeIssuedByPersonName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForAppealDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAppealHeardByPersonName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAppealNoteTakerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAppealEmployeeRepresentativeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAppealOutcomeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layGrpExpiry = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForExpiryDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpiryDurationUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpiryDurationUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layGrpRemarks = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layGrpActions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHRManagerName = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter();
            this.sp_HR_00057_Get_Employee_Sanction_ProgressTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00057_Get_Employee_Sanction_ProgressTableAdapter();
            this.sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter();
            this.sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter();
            this.sp_HR_00075_Sanction_Duration_Units_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00075_Sanction_Duration_Units_Descriptors_With_BlankTableAdapter();
            this.sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter();
            this.sp_HR_00129_Discipline_Investigation_Outcomes_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00129_Discipline_Investigation_Outcomes_With_BlankTableAdapter();
            this.sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AppealNoteTakerPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00055GetEmployeeSanctionItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingNoteTakerPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeIssuedByPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigatedByPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SanctionOutcomeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealEmployeeRepresentativeNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeIssuedByPersonNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00130SanctionHearingOutcomeListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingEmployeeRepresentativeNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationOutcomeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00129DisciplineInvestigationOutcomesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigatedByPersonNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationEndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationEndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationStartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationStartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HRManagerNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HRManagerIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealHeardByPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SanctionIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaisedByPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeardByPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealHeardByPersonNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealOutcomeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00039AppealOutcomesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00075SanctionDurationUnitsDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00074SanctionStatusesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERIssueTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00073SanctionTypesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00057GetEmployeeSanctionProgressBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryDurationUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaisedByPersonNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeardByPersonNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingNoteTakerNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealNoteTakerNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSanctionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHRManagerID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRaisedByPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigatedByPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeardByPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingOutcomeIssuedByPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealConductedByID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingNoteTakerPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealNoteTakerPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForERIssueTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRaisedByPersonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSanctionOutcome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigationStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigationEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigatedByPersonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigationOutcomeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpHearing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeardByPersonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingNoteTakerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingEmployeeRepresentativeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingOutcomeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingOutcomeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingOutcomeIssuedByPersonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealHeardByPersonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealNoteTakerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealEmployeeRepresentativeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealOutcomeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpExpiry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryDurationUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryDurationUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpActions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHRManagerName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(630, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 635);
            this.barDockControlBottom.Size = new System.Drawing.Size(630, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 609);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(630, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 609);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "ID";
            this.gridColumn17.FieldName = "ID";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn17.Width = 53;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "ID";
            this.gridColumn14.FieldName = "ID";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn14.Width = 53;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "ID";
            this.gridColumn11.FieldName = "ID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn11.Width = 53;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "ID";
            this.gridColumn5.FieldName = "ID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Width = 53;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ID";
            this.gridColumn2.FieldName = "ID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.ImageOptions.Image")));
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(630, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 635);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(630, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 609);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(630, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 609);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "BlockAdd_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.AppealNoteTakerPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HearingNoteTakerPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HearingOutcomeIssuedByPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.InvestigatedByPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SanctionOutcomeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AppealEmployeeRepresentativeNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HearingOutcomeIssuedByPersonNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.HearingOutcomeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.HearingEmployeeRepresentativeNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.InvestigationOutcomeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.InvestigatedByPersonNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.InvestigationEndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.InvestigationStartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.HRManagerNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.HRManagerIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AppealHeardByPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SanctionIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RaisedByPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HeardByPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AppealHeardByPersonNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AppealOutcomeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AppealDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpiryDurationUnitDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.StatusIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ERIssueTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.btnSave);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.EmployeeFirstnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeSurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.DateRaisedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.HearingOutcomeDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpiryDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.LiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpiryDurationUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RaisedByPersonNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.HeardByPersonNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.HearingDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.HearingNoteTakerNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AppealNoteTakerNameButtonEdit);
            this.dataLayoutControl1.DataSource = this.spHR00055GetEmployeeSanctionItemsBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSanctionID,
            this.ItemForEmployeeID,
            this.ItemForEmployeeSurname,
            this.ItemForEmployeeFirstname,
            this.ItemForEmployeeNumber,
            this.ItemForHRManagerID,
            this.ItemForRaisedByPersonID,
            this.ItemForInvestigatedByPersonID,
            this.ItemForHeardByPersonID,
            this.ItemForHearingOutcomeIssuedByPersonID,
            this.ItemForAppealConductedByID,
            this.ItemForHearingNoteTakerPersonID,
            this.ItemForAppealNoteTakerPersonID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(736, 178, 282, 415);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(630, 609);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // AppealNoteTakerPersonIDTextEdit
            // 
            this.AppealNoteTakerPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "AppealNoteTakerPersonID", true));
            this.AppealNoteTakerPersonIDTextEdit.Location = new System.Drawing.Point(172, 109);
            this.AppealNoteTakerPersonIDTextEdit.MenuManager = this.barManager1;
            this.AppealNoteTakerPersonIDTextEdit.Name = "AppealNoteTakerPersonIDTextEdit";
            this.AppealNoteTakerPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AppealNoteTakerPersonIDTextEdit, true);
            this.AppealNoteTakerPersonIDTextEdit.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AppealNoteTakerPersonIDTextEdit, optionsSpelling1);
            this.AppealNoteTakerPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AppealNoteTakerPersonIDTextEdit.TabIndex = 99;
            // 
            // spHR00055GetEmployeeSanctionItemsBindingSource
            // 
            this.spHR00055GetEmployeeSanctionItemsBindingSource.DataMember = "sp_HR_00055_Get_Employee_Sanction_Items";
            this.spHR00055GetEmployeeSanctionItemsBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // HearingNoteTakerPersonIDTextEdit
            // 
            this.HearingNoteTakerPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HearingNoteTakerPersonID", true));
            this.HearingNoteTakerPersonIDTextEdit.Location = new System.Drawing.Point(172, 119);
            this.HearingNoteTakerPersonIDTextEdit.MenuManager = this.barManager1;
            this.HearingNoteTakerPersonIDTextEdit.Name = "HearingNoteTakerPersonIDTextEdit";
            this.HearingNoteTakerPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HearingNoteTakerPersonIDTextEdit, true);
            this.HearingNoteTakerPersonIDTextEdit.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HearingNoteTakerPersonIDTextEdit, optionsSpelling2);
            this.HearingNoteTakerPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.HearingNoteTakerPersonIDTextEdit.TabIndex = 98;
            // 
            // HearingOutcomeIssuedByPersonIDTextEdit
            // 
            this.HearingOutcomeIssuedByPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HearingOutcomeIssuedByPersonID", true));
            this.HearingOutcomeIssuedByPersonIDTextEdit.Location = new System.Drawing.Point(226, 382);
            this.HearingOutcomeIssuedByPersonIDTextEdit.MenuManager = this.barManager1;
            this.HearingOutcomeIssuedByPersonIDTextEdit.Name = "HearingOutcomeIssuedByPersonIDTextEdit";
            this.HearingOutcomeIssuedByPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HearingOutcomeIssuedByPersonIDTextEdit, true);
            this.HearingOutcomeIssuedByPersonIDTextEdit.Size = new System.Drawing.Size(368, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HearingOutcomeIssuedByPersonIDTextEdit, optionsSpelling3);
            this.HearingOutcomeIssuedByPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.HearingOutcomeIssuedByPersonIDTextEdit.TabIndex = 94;
            // 
            // InvestigatedByPersonIDTextEdit
            // 
            this.InvestigatedByPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "InvestigatedByPersonID", true));
            this.InvestigatedByPersonIDTextEdit.Location = new System.Drawing.Point(169, 308);
            this.InvestigatedByPersonIDTextEdit.MenuManager = this.barManager1;
            this.InvestigatedByPersonIDTextEdit.Name = "InvestigatedByPersonIDTextEdit";
            this.InvestigatedByPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.InvestigatedByPersonIDTextEdit, true);
            this.InvestigatedByPersonIDTextEdit.Size = new System.Drawing.Size(425, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.InvestigatedByPersonIDTextEdit, optionsSpelling4);
            this.InvestigatedByPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.InvestigatedByPersonIDTextEdit.TabIndex = 91;
            // 
            // SanctionOutcomeTextEdit
            // 
            this.SanctionOutcomeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "SanctionOutcome", true));
            this.SanctionOutcomeTextEdit.Location = new System.Drawing.Point(196, 286);
            this.SanctionOutcomeTextEdit.MenuManager = this.barManager1;
            this.SanctionOutcomeTextEdit.Name = "SanctionOutcomeTextEdit";
            this.SanctionOutcomeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SanctionOutcomeTextEdit, true);
            this.SanctionOutcomeTextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SanctionOutcomeTextEdit, optionsSpelling5);
            this.SanctionOutcomeTextEdit.StyleController = this.dataLayoutControl1;
            this.SanctionOutcomeTextEdit.TabIndex = 97;
            // 
            // AppealEmployeeRepresentativeNameTextEdit
            // 
            this.AppealEmployeeRepresentativeNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "AppealEmployeeRepresentativeName", true));
            this.AppealEmployeeRepresentativeNameTextEdit.Location = new System.Drawing.Point(196, 261);
            this.AppealEmployeeRepresentativeNameTextEdit.MenuManager = this.barManager1;
            this.AppealEmployeeRepresentativeNameTextEdit.Name = "AppealEmployeeRepresentativeNameTextEdit";
            this.AppealEmployeeRepresentativeNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AppealEmployeeRepresentativeNameTextEdit, true);
            this.AppealEmployeeRepresentativeNameTextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AppealEmployeeRepresentativeNameTextEdit, optionsSpelling6);
            this.AppealEmployeeRepresentativeNameTextEdit.StyleController = this.dataLayoutControl1;
            this.AppealEmployeeRepresentativeNameTextEdit.TabIndex = 96;
            // 
            // HearingOutcomeIssuedByPersonNameButtonEdit
            // 
            this.HearingOutcomeIssuedByPersonNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HearingOutcomeIssuedByPersonName", true));
            this.HearingOutcomeIssuedByPersonNameButtonEdit.Location = new System.Drawing.Point(196, 335);
            this.HearingOutcomeIssuedByPersonNameButtonEdit.MenuManager = this.barManager1;
            this.HearingOutcomeIssuedByPersonNameButtonEdit.Name = "HearingOutcomeIssuedByPersonNameButtonEdit";
            this.HearingOutcomeIssuedByPersonNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to Select HR Manager", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.HearingOutcomeIssuedByPersonNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.HearingOutcomeIssuedByPersonNameButtonEdit.Size = new System.Drawing.Size(398, 20);
            this.HearingOutcomeIssuedByPersonNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.HearingOutcomeIssuedByPersonNameButtonEdit.TabIndex = 89;
            this.HearingOutcomeIssuedByPersonNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HearingOutcomeIssuedByPersonNameButtonEdit_ButtonClick);
            // 
            // HearingOutcomeIDGridLookUpEdit
            // 
            this.HearingOutcomeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HearingOutcomeID", true));
            this.HearingOutcomeIDGridLookUpEdit.Location = new System.Drawing.Point(196, 285);
            this.HearingOutcomeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.HearingOutcomeIDGridLookUpEdit.Name = "HearingOutcomeIDGridLookUpEdit";
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            editorButtonImageOptions3.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions3.Image")));
            this.HearingOutcomeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.HearingOutcomeIDGridLookUpEdit.Properties.DataSource = this.spHR00130SanctionHearingOutcomeListWithBlankBindingSource;
            this.HearingOutcomeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.HearingOutcomeIDGridLookUpEdit.Properties.NullText = "";
            this.HearingOutcomeIDGridLookUpEdit.Properties.PopupView = this.gridView7;
            this.HearingOutcomeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.HearingOutcomeIDGridLookUpEdit.Size = new System.Drawing.Size(398, 22);
            this.HearingOutcomeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.HearingOutcomeIDGridLookUpEdit.TabIndex = 84;
            this.HearingOutcomeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HearingOutcomeIDGridLookUpEdit_ButtonClick);
            this.HearingOutcomeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.HearingOutcomeIDGridLookUpEdit_EditValueChanged);
            this.HearingOutcomeIDGridLookUpEdit.Validated += new System.EventHandler(this.HearingOutcomeIDGridLookUpEdit_Validated);
            // 
            // spHR00130SanctionHearingOutcomeListWithBlankBindingSource
            // 
            this.spHR00130SanctionHearingOutcomeListWithBlankBindingSource.DataMember = "sp_HR_00130_Sanction_Hearing_Outcome_List_With_Blank";
            this.spHR00130SanctionHearingOutcomeListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19});
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn17;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView7.OptionsView.ShowIndicator = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn19, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Hearing Outcome";
            this.gridColumn18.FieldName = "Description";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 0;
            this.gridColumn18.Width = 220;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Order";
            this.gridColumn19.FieldName = "Order";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // HearingEmployeeRepresentativeNameTextEdit
            // 
            this.HearingEmployeeRepresentativeNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HearingEmployeeRepresentativeName", true));
            this.HearingEmployeeRepresentativeNameTextEdit.Location = new System.Drawing.Point(196, 261);
            this.HearingEmployeeRepresentativeNameTextEdit.MenuManager = this.barManager1;
            this.HearingEmployeeRepresentativeNameTextEdit.Name = "HearingEmployeeRepresentativeNameTextEdit";
            this.HearingEmployeeRepresentativeNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HearingEmployeeRepresentativeNameTextEdit, true);
            this.HearingEmployeeRepresentativeNameTextEdit.Size = new System.Drawing.Size(398, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HearingEmployeeRepresentativeNameTextEdit, optionsSpelling7);
            this.HearingEmployeeRepresentativeNameTextEdit.StyleController = this.dataLayoutControl1;
            this.HearingEmployeeRepresentativeNameTextEdit.TabIndex = 93;
            // 
            // InvestigationOutcomeIDGridLookUpEdit
            // 
            this.InvestigationOutcomeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "InvestigationOutcomeID", true));
            this.InvestigationOutcomeIDGridLookUpEdit.EditValue = "";
            this.InvestigationOutcomeIDGridLookUpEdit.Location = new System.Drawing.Point(196, 261);
            this.InvestigationOutcomeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.InvestigationOutcomeIDGridLookUpEdit.Name = "InvestigationOutcomeIDGridLookUpEdit";
            this.InvestigationOutcomeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvestigationOutcomeIDGridLookUpEdit.Properties.DataSource = this.spHR00129DisciplineInvestigationOutcomesWithBlankBindingSource;
            this.InvestigationOutcomeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.InvestigationOutcomeIDGridLookUpEdit.Properties.NullText = "";
            this.InvestigationOutcomeIDGridLookUpEdit.Properties.PopupView = this.gridView6;
            this.InvestigationOutcomeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.InvestigationOutcomeIDGridLookUpEdit.Size = new System.Drawing.Size(398, 20);
            this.InvestigationOutcomeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.InvestigationOutcomeIDGridLookUpEdit.TabIndex = 92;
            this.InvestigationOutcomeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.InvestigationOutcomeIDGridLookUpEdit_EditValueChanged);
            this.InvestigationOutcomeIDGridLookUpEdit.Validated += new System.EventHandler(this.InvestigationOutcomeIDGridLookUpEdit_Validated);
            // 
            // spHR00129DisciplineInvestigationOutcomesWithBlankBindingSource
            // 
            this.spHR00129DisciplineInvestigationOutcomesWithBlankBindingSource.DataMember = "sp_HR_00129_Discipline_Investigation_Outcomes_With_Blank";
            this.spHR00129DisciplineInvestigationOutcomesWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16});
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn14;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView6.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView6.OptionsView.ShowIndicator = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn16, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Investigation Outcome";
            this.gridColumn15.FieldName = "Description";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 0;
            this.gridColumn15.Width = 220;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Order";
            this.gridColumn16.FieldName = "Order";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // InvestigatedByPersonNameButtonEdit
            // 
            this.InvestigatedByPersonNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "InvestigatedByPersonName", true));
            this.InvestigatedByPersonNameButtonEdit.Location = new System.Drawing.Point(196, 237);
            this.InvestigatedByPersonNameButtonEdit.MenuManager = this.barManager1;
            this.InvestigatedByPersonNameButtonEdit.Name = "InvestigatedByPersonNameButtonEdit";
            this.InvestigatedByPersonNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions4, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject13, serializableAppearanceObject14, serializableAppearanceObject15, serializableAppearanceObject16, "Click to Select HR Manager", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.InvestigatedByPersonNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.InvestigatedByPersonNameButtonEdit.Size = new System.Drawing.Size(398, 20);
            this.InvestigatedByPersonNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.InvestigatedByPersonNameButtonEdit.TabIndex = 89;
            this.InvestigatedByPersonNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.InvestigatedByPersonNameButtonEdit_ButtonClick);
            // 
            // InvestigationEndDateDateEdit
            // 
            this.InvestigationEndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "InvestigationEndDate", true));
            this.InvestigationEndDateDateEdit.EditValue = null;
            this.InvestigationEndDateDateEdit.Location = new System.Drawing.Point(196, 213);
            this.InvestigationEndDateDateEdit.MenuManager = this.barManager1;
            this.InvestigationEndDateDateEdit.Name = "InvestigationEndDateDateEdit";
            this.InvestigationEndDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.InvestigationEndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvestigationEndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvestigationEndDateDateEdit.Properties.Mask.EditMask = "g";
            this.InvestigationEndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.InvestigationEndDateDateEdit.Size = new System.Drawing.Size(398, 20);
            this.InvestigationEndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.InvestigationEndDateDateEdit.TabIndex = 90;
            // 
            // InvestigationStartDateDateEdit
            // 
            this.InvestigationStartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "InvestigationStartDate", true));
            this.InvestigationStartDateDateEdit.EditValue = null;
            this.InvestigationStartDateDateEdit.Location = new System.Drawing.Point(196, 189);
            this.InvestigationStartDateDateEdit.MenuManager = this.barManager1;
            this.InvestigationStartDateDateEdit.Name = "InvestigationStartDateDateEdit";
            this.InvestigationStartDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.InvestigationStartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvestigationStartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.InvestigationStartDateDateEdit.Properties.Mask.EditMask = "g";
            this.InvestigationStartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.InvestigationStartDateDateEdit.Size = new System.Drawing.Size(398, 20);
            this.InvestigationStartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.InvestigationStartDateDateEdit.TabIndex = 89;
            // 
            // HRManagerNameButtonEdit
            // 
            this.HRManagerNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HRManagerName", true));
            this.HRManagerNameButtonEdit.Location = new System.Drawing.Point(172, 85);
            this.HRManagerNameButtonEdit.MenuManager = this.barManager1;
            this.HRManagerNameButtonEdit.Name = "HRManagerNameButtonEdit";
            this.HRManagerNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions5, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject17, serializableAppearanceObject18, serializableAppearanceObject19, serializableAppearanceObject20, "Click to Select HR Manager", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.HRManagerNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.HRManagerNameButtonEdit.Size = new System.Drawing.Size(446, 20);
            this.HRManagerNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.HRManagerNameButtonEdit.TabIndex = 88;
            this.HRManagerNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HRManagerNameButtonEdit_ButtonClick);
            // 
            // HRManagerIDTextEdit
            // 
            this.HRManagerIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HRManagerID", true));
            this.HRManagerIDTextEdit.Location = new System.Drawing.Point(136, 157);
            this.HRManagerIDTextEdit.MenuManager = this.barManager1;
            this.HRManagerIDTextEdit.Name = "HRManagerIDTextEdit";
            this.HRManagerIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HRManagerIDTextEdit, true);
            this.HRManagerIDTextEdit.Size = new System.Drawing.Size(482, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HRManagerIDTextEdit, optionsSpelling8);
            this.HRManagerIDTextEdit.StyleController = this.dataLayoutControl1;
            this.HRManagerIDTextEdit.TabIndex = 87;
            // 
            // AppealHeardByPersonIDTextEdit
            // 
            this.AppealHeardByPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "AppealHeardByPersonID", true));
            this.AppealHeardByPersonIDTextEdit.Location = new System.Drawing.Point(172, 109);
            this.AppealHeardByPersonIDTextEdit.MenuManager = this.barManager1;
            this.AppealHeardByPersonIDTextEdit.Name = "AppealHeardByPersonIDTextEdit";
            this.AppealHeardByPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AppealHeardByPersonIDTextEdit, true);
            this.AppealHeardByPersonIDTextEdit.Size = new System.Drawing.Size(446, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AppealHeardByPersonIDTextEdit, optionsSpelling9);
            this.AppealHeardByPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AppealHeardByPersonIDTextEdit.TabIndex = 84;
            // 
            // SanctionIDTextEdit
            // 
            this.SanctionIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "SanctionID", true));
            this.SanctionIDTextEdit.Location = new System.Drawing.Point(136, 325);
            this.SanctionIDTextEdit.MenuManager = this.barManager1;
            this.SanctionIDTextEdit.Name = "SanctionIDTextEdit";
            this.SanctionIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SanctionIDTextEdit, true);
            this.SanctionIDTextEdit.Size = new System.Drawing.Size(465, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SanctionIDTextEdit, optionsSpelling10);
            this.SanctionIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SanctionIDTextEdit.TabIndex = 78;
            // 
            // RaisedByPersonIDTextEdit
            // 
            this.RaisedByPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "RaisedByPersonID", true));
            this.RaisedByPersonIDTextEdit.Location = new System.Drawing.Point(136, 181);
            this.RaisedByPersonIDTextEdit.MenuManager = this.barManager1;
            this.RaisedByPersonIDTextEdit.Name = "RaisedByPersonIDTextEdit";
            this.RaisedByPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RaisedByPersonIDTextEdit, true);
            this.RaisedByPersonIDTextEdit.Size = new System.Drawing.Size(482, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RaisedByPersonIDTextEdit, optionsSpelling11);
            this.RaisedByPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.RaisedByPersonIDTextEdit.TabIndex = 75;
            // 
            // HeardByPersonIDTextEdit
            // 
            this.HeardByPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HeardByPersonID", true));
            this.HeardByPersonIDTextEdit.Location = new System.Drawing.Point(136, 133);
            this.HeardByPersonIDTextEdit.MenuManager = this.barManager1;
            this.HeardByPersonIDTextEdit.Name = "HeardByPersonIDTextEdit";
            this.HeardByPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HeardByPersonIDTextEdit, true);
            this.HeardByPersonIDTextEdit.Size = new System.Drawing.Size(482, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HeardByPersonIDTextEdit, optionsSpelling12);
            this.HeardByPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.HeardByPersonIDTextEdit.TabIndex = 74;
            // 
            // AppealHeardByPersonNameButtonEdit
            // 
            this.AppealHeardByPersonNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "AppealHeardByPersonName", true));
            this.AppealHeardByPersonNameButtonEdit.Location = new System.Drawing.Point(196, 213);
            this.AppealHeardByPersonNameButtonEdit.MenuManager = this.barManager1;
            this.AppealHeardByPersonNameButtonEdit.Name = "AppealHeardByPersonNameButtonEdit";
            this.AppealHeardByPersonNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions6, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject21, serializableAppearanceObject22, serializableAppearanceObject23, serializableAppearanceObject24, "CLick to Select Employee", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.AppealHeardByPersonNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AppealHeardByPersonNameButtonEdit.Size = new System.Drawing.Size(398, 20);
            this.AppealHeardByPersonNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.AppealHeardByPersonNameButtonEdit.TabIndex = 85;
            this.AppealHeardByPersonNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AppealHeardByPersonNameButtonEdit_ButtonClick);
            // 
            // AppealOutcomeIDGridLookUpEdit
            // 
            this.AppealOutcomeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "AppealOutcomeID", true));
            this.AppealOutcomeIDGridLookUpEdit.Location = new System.Drawing.Point(196, 285);
            this.AppealOutcomeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.AppealOutcomeIDGridLookUpEdit.Name = "AppealOutcomeIDGridLookUpEdit";
            editorButtonImageOptions7.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions7.Image")));
            editorButtonImageOptions8.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions8.Image")));
            this.AppealOutcomeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions7, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject25, serializableAppearanceObject26, serializableAppearanceObject27, serializableAppearanceObject28, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions8, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject29, serializableAppearanceObject30, serializableAppearanceObject31, serializableAppearanceObject32, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.AppealOutcomeIDGridLookUpEdit.Properties.DataSource = this.spHR00039AppealOutcomesListWithBlankBindingSource;
            this.AppealOutcomeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.AppealOutcomeIDGridLookUpEdit.Properties.NullText = "";
            this.AppealOutcomeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.AppealOutcomeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.AppealOutcomeIDGridLookUpEdit.Size = new System.Drawing.Size(398, 22);
            this.AppealOutcomeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AppealOutcomeIDGridLookUpEdit.TabIndex = 83;
            this.AppealOutcomeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AppealOutcomeIDGridLookUpEdit_ButtonClick);
            this.AppealOutcomeIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.AppealOutcomeIDGridLookUpEdit_EditValueChanged);
            this.AppealOutcomeIDGridLookUpEdit.Validated += new System.EventHandler(this.AppealOutcomeIDGridLookUpEdit_Validated);
            // 
            // spHR00039AppealOutcomesListWithBlankBindingSource
            // 
            this.spHR00039AppealOutcomesListWithBlankBindingSource.DataMember = "sp_HR_00039_Appeal_Outcomes_List_With_Blank";
            this.spHR00039AppealOutcomesListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.gridColumn11;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn13, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Appeal Outcome";
            this.gridColumn12.FieldName = "Description";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn12.Visible = true;
            this.gridColumn12.VisibleIndex = 0;
            this.gridColumn12.Width = 220;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Order";
            this.gridColumn13.FieldName = "Order";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // AppealDateDateEdit
            // 
            this.AppealDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "AppealDate", true));
            this.AppealDateDateEdit.EditValue = null;
            this.AppealDateDateEdit.Location = new System.Drawing.Point(196, 189);
            this.AppealDateDateEdit.MenuManager = this.barManager1;
            this.AppealDateDateEdit.Name = "AppealDateDateEdit";
            this.AppealDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AppealDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AppealDateDateEdit.Properties.Mask.EditMask = "g";
            this.AppealDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.AppealDateDateEdit.Size = new System.Drawing.Size(398, 20);
            this.AppealDateDateEdit.StyleController = this.dataLayoutControl1;
            this.AppealDateDateEdit.TabIndex = 82;
            // 
            // ExpiryDurationUnitDescriptorIDGridLookUpEdit
            // 
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "ExpiryDurationUnitDescriptorID", true));
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(410, 189);
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Name = "ExpiryDurationUnitDescriptorIDGridLookUpEdit";
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.spHR00075SanctionDurationUnitsDescriptorsWithBlankBindingSource;
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(184, 20);
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.TabIndex = 76;
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.EditValueChanged += new System.EventHandler(this.ExpiryDurationUnitDescriptorIDGridLookUpEdit_EditValueChanged);
            this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Validated += new System.EventHandler(this.ExpiryDurationUnitDescriptorIDGridLookUpEdit_Validated);
            // 
            // spHR00075SanctionDurationUnitsDescriptorsWithBlankBindingSource
            // 
            this.spHR00075SanctionDurationUnitsDescriptorsWithBlankBindingSource.DataMember = "sp_HR_00075_Sanction_Duration_Units_Descriptors_With_Blank";
            this.spHR00075SanctionDurationUnitsDescriptorsWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn5;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn7, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Duration Unit Descriptor";
            this.gridColumn6.FieldName = "Description";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 220;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Order";
            this.gridColumn7.FieldName = "Order";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // StatusIDGridLookUpEdit
            // 
            this.StatusIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "StatusID", true));
            this.StatusIDGridLookUpEdit.Location = new System.Drawing.Point(196, 260);
            this.StatusIDGridLookUpEdit.MenuManager = this.barManager1;
            this.StatusIDGridLookUpEdit.Name = "StatusIDGridLookUpEdit";
            editorButtonImageOptions9.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions9.Image")));
            editorButtonImageOptions10.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions10.Image")));
            this.StatusIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions9, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject33, serializableAppearanceObject34, serializableAppearanceObject35, serializableAppearanceObject36, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions10, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject37, serializableAppearanceObject38, serializableAppearanceObject39, serializableAppearanceObject40, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.StatusIDGridLookUpEdit.Properties.DataSource = this.spHR00074SanctionStatusesListWithBlankBindingSource;
            this.StatusIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.StatusIDGridLookUpEdit.Properties.NullText = "";
            this.StatusIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.StatusIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.StatusIDGridLookUpEdit.Size = new System.Drawing.Size(398, 22);
            this.StatusIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.StatusIDGridLookUpEdit.TabIndex = 73;
            this.StatusIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.StatusIdGridLookUpEdit_ButtonClick);
            // 
            // spHR00074SanctionStatusesListWithBlankBindingSource
            // 
            this.spHR00074SanctionStatusesListWithBlankBindingSource.DataMember = "sp_HR_00074_Sanction_Statuses_List_With_Blank";
            this.spHR00074SanctionStatusesListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.gridColumn2;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Status";
            this.gridColumn3.FieldName = "Description";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 220;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Order";
            this.gridColumn4.FieldName = "Order";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // ERIssueTypeIDGridLookUpEdit
            // 
            this.ERIssueTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "ERIssueTypeID", true));
            this.ERIssueTypeIDGridLookUpEdit.EditValue = "";
            this.ERIssueTypeIDGridLookUpEdit.Location = new System.Drawing.Point(172, 59);
            this.ERIssueTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ERIssueTypeIDGridLookUpEdit.Name = "ERIssueTypeIDGridLookUpEdit";
            editorButtonImageOptions11.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions11.Image")));
            editorButtonImageOptions12.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions12.Image")));
            this.ERIssueTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions11, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject41, serializableAppearanceObject42, serializableAppearanceObject43, serializableAppearanceObject44, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions12, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject45, serializableAppearanceObject46, serializableAppearanceObject47, serializableAppearanceObject48, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ERIssueTypeIDGridLookUpEdit.Properties.DataSource = this.spHR00073SanctionTypesListWithBlankBindingSource;
            this.ERIssueTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ERIssueTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ERIssueTypeIDGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.ERIssueTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ERIssueTypeIDGridLookUpEdit.Size = new System.Drawing.Size(446, 22);
            this.ERIssueTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ERIssueTypeIDGridLookUpEdit.TabIndex = 72;
            this.ERIssueTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.SanctionTypeIdGridLookUpEdit_ButtonClick);
            // 
            // spHR00073SanctionTypesListWithBlankBindingSource
            // 
            this.spHR00073SanctionTypesListWithBlankBindingSource.DataMember = "sp_HR_00073_Sanction_Types_List_With_Blank";
            this.spHR00073SanctionTypesListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colDescription,
            this.colOrder});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.gridColumn1;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Sanction Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // EmployeeNumberTextEdit
            // 
            this.EmployeeNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "EmployeeNumber", true));
            this.EmployeeNumberTextEdit.Location = new System.Drawing.Point(136, 133);
            this.EmployeeNumberTextEdit.MenuManager = this.barManager1;
            this.EmployeeNumberTextEdit.Name = "EmployeeNumberTextEdit";
            this.EmployeeNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNumberTextEdit, true);
            this.EmployeeNumberTextEdit.Size = new System.Drawing.Size(465, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNumberTextEdit, optionsSpelling13);
            this.EmployeeNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNumberTextEdit.TabIndex = 71;
            // 
            // EmployeeIDTextEdit
            // 
            this.EmployeeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "EmployeeID", true));
            this.EmployeeIDTextEdit.Location = new System.Drawing.Point(136, 157);
            this.EmployeeIDTextEdit.MenuManager = this.barManager1;
            this.EmployeeIDTextEdit.Name = "EmployeeIDTextEdit";
            this.EmployeeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIDTextEdit, true);
            this.EmployeeIDTextEdit.Size = new System.Drawing.Size(465, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIDTextEdit, optionsSpelling14);
            this.EmployeeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIDTextEdit.TabIndex = 70;
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(12, 393);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.StyleController = this.dataLayoutControl1;
            this.btnSave.TabIndex = 69;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(24, 453);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(582, 132);
            this.gridSplitContainer1.TabIndex = 79;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00057GetEmployeeSanctionProgressBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(582, 132);
            this.gridControl1.TabIndex = 46;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00057GetEmployeeSanctionProgressBindingSource
            // 
            this.spHR00057GetEmployeeSanctionProgressBindingSource.DataMember = "sp_HR_00057_Get_Employee_Sanction_Progress";
            this.spHR00057GetEmployeeSanctionProgressBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId1,
            this.colSanctionId,
            this.colActionDate,
            this.colDetail,
            this.colClosedByName,
            this.colClosedDate,
            this.colStartedByName,
            this.colStartedById,
            this.colClosedById});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView2.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView2.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActionDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView2_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView2_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView2_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView2_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colId1
            // 
            this.colId1.Caption = "Progress ID";
            this.colId1.FieldName = "Id";
            this.colId1.Name = "colId1";
            this.colId1.OptionsColumn.AllowEdit = false;
            this.colId1.OptionsColumn.AllowFocus = false;
            this.colId1.OptionsColumn.ReadOnly = true;
            this.colId1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colSanctionId
            // 
            this.colSanctionId.Caption = "Sanction ID";
            this.colSanctionId.FieldName = "SanctionId";
            this.colSanctionId.Name = "colSanctionId";
            this.colSanctionId.OptionsColumn.AllowEdit = false;
            this.colSanctionId.OptionsColumn.AllowFocus = false;
            this.colSanctionId.OptionsColumn.ReadOnly = true;
            this.colSanctionId.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colActionDate
            // 
            this.colActionDate.Caption = "Action Date";
            this.colActionDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActionDate.FieldName = "ActionDate";
            this.colActionDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colActionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActionDate.Name = "colActionDate";
            this.colActionDate.OptionsColumn.AllowEdit = false;
            this.colActionDate.OptionsColumn.AllowFocus = false;
            this.colActionDate.OptionsColumn.ReadOnly = true;
            this.colActionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActionDate.Visible = true;
            this.colActionDate.VisibleIndex = 0;
            this.colActionDate.Width = 110;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colDetail
            // 
            this.colDetail.Caption = "Action Detail";
            this.colDetail.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colDetail.FieldName = "Detail";
            this.colDetail.Name = "colDetail";
            this.colDetail.OptionsColumn.ReadOnly = true;
            this.colDetail.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDetail.Visible = true;
            this.colDetail.VisibleIndex = 2;
            this.colDetail.Width = 236;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colClosedByName
            // 
            this.colClosedByName.Caption = "Closed By";
            this.colClosedByName.FieldName = "ClosedByName";
            this.colClosedByName.Name = "colClosedByName";
            this.colClosedByName.OptionsColumn.AllowEdit = false;
            this.colClosedByName.OptionsColumn.AllowFocus = false;
            this.colClosedByName.OptionsColumn.ReadOnly = true;
            this.colClosedByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colClosedByName.Visible = true;
            this.colClosedByName.VisibleIndex = 3;
            this.colClosedByName.Width = 140;
            // 
            // colClosedDate
            // 
            this.colClosedDate.Caption = "Closed Date";
            this.colClosedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClosedDate.FieldName = "ClosedDate";
            this.colClosedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colClosedDate.Name = "colClosedDate";
            this.colClosedDate.OptionsColumn.AllowEdit = false;
            this.colClosedDate.OptionsColumn.AllowFocus = false;
            this.colClosedDate.OptionsColumn.ReadOnly = true;
            this.colClosedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colClosedDate.Visible = true;
            this.colClosedDate.VisibleIndex = 4;
            this.colClosedDate.Width = 100;
            // 
            // colStartedByName
            // 
            this.colStartedByName.Caption = "Started By";
            this.colStartedByName.FieldName = "StartedByName";
            this.colStartedByName.Name = "colStartedByName";
            this.colStartedByName.OptionsColumn.AllowEdit = false;
            this.colStartedByName.OptionsColumn.AllowFocus = false;
            this.colStartedByName.OptionsColumn.ReadOnly = true;
            this.colStartedByName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStartedByName.Visible = true;
            this.colStartedByName.VisibleIndex = 1;
            this.colStartedByName.Width = 140;
            // 
            // colStartedById
            // 
            this.colStartedById.Caption = "Started By ID";
            this.colStartedById.FieldName = "StartedById";
            this.colStartedById.Name = "colStartedById";
            this.colStartedById.OptionsColumn.AllowEdit = false;
            this.colStartedById.OptionsColumn.AllowFocus = false;
            this.colStartedById.OptionsColumn.ReadOnly = true;
            this.colStartedById.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colClosedById
            // 
            this.colClosedById.Caption = "Closed By ID";
            this.colClosedById.FieldName = "ClosedById";
            this.colClosedById.Name = "colClosedById";
            this.colClosedById.OptionsColumn.AllowEdit = false;
            this.colClosedById.OptionsColumn.AllowFocus = false;
            this.colClosedById.OptionsColumn.ReadOnly = true;
            this.colClosedById.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00055GetEmployeeSanctionItemsBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(172, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // EmployeeFirstnameTextEdit
            // 
            this.EmployeeFirstnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "EmployeeFirstname", true));
            this.EmployeeFirstnameTextEdit.Location = new System.Drawing.Point(136, 133);
            this.EmployeeFirstnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeFirstnameTextEdit.Name = "EmployeeFirstnameTextEdit";
            this.EmployeeFirstnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeFirstnameTextEdit, true);
            this.EmployeeFirstnameTextEdit.Size = new System.Drawing.Size(465, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeFirstnameTextEdit, optionsSpelling15);
            this.EmployeeFirstnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeFirstnameTextEdit.TabIndex = 4;
            // 
            // EmployeeSurnameTextEdit
            // 
            this.EmployeeSurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "EmployeeSurname", true));
            this.EmployeeSurnameTextEdit.Location = new System.Drawing.Point(136, 181);
            this.EmployeeSurnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeSurnameTextEdit.Name = "EmployeeSurnameTextEdit";
            this.EmployeeSurnameTextEdit.Properties.MaxLength = 50;
            this.EmployeeSurnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeSurnameTextEdit, true);
            this.EmployeeSurnameTextEdit.Size = new System.Drawing.Size(465, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeSurnameTextEdit, optionsSpelling16);
            this.EmployeeSurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeSurnameTextEdit.TabIndex = 6;
            // 
            // EmployeeNameButtonEdit
            // 
            this.EmployeeNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "EmployeeName", true));
            this.EmployeeNameButtonEdit.Location = new System.Drawing.Point(172, 35);
            this.EmployeeNameButtonEdit.MenuManager = this.barManager1;
            this.EmployeeNameButtonEdit.Name = "EmployeeNameButtonEdit";
            this.EmployeeNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "Click to select an employee", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.EmployeeNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EmployeeNameButtonEdit.Size = new System.Drawing.Size(446, 20);
            this.EmployeeNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameButtonEdit.TabIndex = 49;
            this.EmployeeNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EmployeeNameButtonEdit_ButtonClick);
            this.EmployeeNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EmployeeNameButtonEdit_Validating);
            // 
            // DateRaisedDateEdit
            // 
            this.DateRaisedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "DateRaised", true));
            this.DateRaisedDateEdit.EditValue = null;
            this.DateRaisedDateEdit.Location = new System.Drawing.Point(196, 212);
            this.DateRaisedDateEdit.MenuManager = this.barManager1;
            this.DateRaisedDateEdit.Name = "DateRaisedDateEdit";
            this.DateRaisedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateRaisedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateRaisedDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateRaisedDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateRaisedDateEdit.Size = new System.Drawing.Size(398, 20);
            this.DateRaisedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateRaisedDateEdit.TabIndex = 51;
            this.DateRaisedDateEdit.EditValueChanged += new System.EventHandler(this.DateRaisedDateEdit_EditValueChanged);
            this.DateRaisedDateEdit.Validated += new System.EventHandler(this.DateRaisedDateEdit_Validated);
            // 
            // HearingOutcomeDateDateEdit
            // 
            this.HearingOutcomeDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HearingOutcomeDate", true));
            this.HearingOutcomeDateDateEdit.EditValue = null;
            this.HearingOutcomeDateDateEdit.Location = new System.Drawing.Point(196, 311);
            this.HearingOutcomeDateDateEdit.MenuManager = this.barManager1;
            this.HearingOutcomeDateDateEdit.Name = "HearingOutcomeDateDateEdit";
            this.HearingOutcomeDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HearingOutcomeDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HearingOutcomeDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.HearingOutcomeDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.HearingOutcomeDateDateEdit.Size = new System.Drawing.Size(398, 20);
            this.HearingOutcomeDateDateEdit.StyleController = this.dataLayoutControl1;
            this.HearingOutcomeDateDateEdit.TabIndex = 52;
            // 
            // ExpiryDateDateEdit
            // 
            this.ExpiryDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "ExpiryDate", true));
            this.ExpiryDateDateEdit.EditValue = null;
            this.ExpiryDateDateEdit.Location = new System.Drawing.Point(196, 213);
            this.ExpiryDateDateEdit.MenuManager = this.barManager1;
            this.ExpiryDateDateEdit.Name = "ExpiryDateDateEdit";
            this.ExpiryDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpiryDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpiryDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.ExpiryDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.ExpiryDateDateEdit.Size = new System.Drawing.Size(398, 20);
            this.ExpiryDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ExpiryDateDateEdit.TabIndex = 54;
            this.ExpiryDateDateEdit.EditValueChanged += new System.EventHandler(this.ExpiryDateDateEdit_EditValueChanged);
            this.ExpiryDateDateEdit.Validated += new System.EventHandler(this.ExpiryDateDateEdit_Validated);
            // 
            // LiveCheckEdit
            // 
            this.LiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "Live", true));
            this.LiveCheckEdit.Location = new System.Drawing.Point(196, 189);
            this.LiveCheckEdit.MenuManager = this.barManager1;
            this.LiveCheckEdit.Name = "LiveCheckEdit";
            this.LiveCheckEdit.Properties.Caption = "(Calculated - Read Only)";
            this.LiveCheckEdit.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.LiveCheckEdit.Properties.ReadOnly = true;
            this.LiveCheckEdit.Properties.ValueChecked = 1;
            this.LiveCheckEdit.Properties.ValueUnchecked = 0;
            this.LiveCheckEdit.Size = new System.Drawing.Size(398, 19);
            this.LiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.LiveCheckEdit.TabIndex = 56;
            // 
            // ExpiryDurationUnitsSpinEdit
            // 
            this.ExpiryDurationUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "ExpiryDurationUnits", true));
            this.ExpiryDurationUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ExpiryDurationUnitsSpinEdit.Location = new System.Drawing.Point(196, 189);
            this.ExpiryDurationUnitsSpinEdit.MenuManager = this.barManager1;
            this.ExpiryDurationUnitsSpinEdit.Name = "ExpiryDurationUnitsSpinEdit";
            this.ExpiryDurationUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpiryDurationUnitsSpinEdit.Size = new System.Drawing.Size(50, 20);
            this.ExpiryDurationUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.ExpiryDurationUnitsSpinEdit.TabIndex = 57;
            this.ExpiryDurationUnitsSpinEdit.EditValueChanged += new System.EventHandler(this.ExpiryDurationUnitsSpinEdit_EditValueChanged);
            this.ExpiryDurationUnitsSpinEdit.Validated += new System.EventHandler(this.ExpiryDurationUnitsSpinEdit_Validated);
            // 
            // RaisedByPersonNameButtonEdit
            // 
            this.RaisedByPersonNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "RaisedByPersonName", true));
            this.RaisedByPersonNameButtonEdit.Location = new System.Drawing.Point(196, 236);
            this.RaisedByPersonNameButtonEdit.MenuManager = this.barManager1;
            this.RaisedByPersonNameButtonEdit.Name = "RaisedByPersonNameButtonEdit";
            this.RaisedByPersonNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions14, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.RaisedByPersonNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.RaisedByPersonNameButtonEdit.Size = new System.Drawing.Size(398, 20);
            this.RaisedByPersonNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.RaisedByPersonNameButtonEdit.TabIndex = 59;
            this.RaisedByPersonNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.RaisedByPersonNameButtonEdit_ButtonClick);
            // 
            // HeardByPersonNameButtonEdit
            // 
            this.HeardByPersonNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HeardByPersonName", true));
            this.HeardByPersonNameButtonEdit.Location = new System.Drawing.Point(196, 213);
            this.HeardByPersonNameButtonEdit.MenuManager = this.barManager1;
            this.HeardByPersonNameButtonEdit.Name = "HeardByPersonNameButtonEdit";
            this.HeardByPersonNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions15, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.HeardByPersonNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.HeardByPersonNameButtonEdit.Size = new System.Drawing.Size(398, 20);
            this.HeardByPersonNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.HeardByPersonNameButtonEdit.TabIndex = 60;
            this.HeardByPersonNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HeardByPersonNameButtonEdit_ButtonClick);
            // 
            // HearingDateDateEdit
            // 
            this.HearingDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HearingDate", true));
            this.HearingDateDateEdit.EditValue = null;
            this.HearingDateDateEdit.Location = new System.Drawing.Point(196, 189);
            this.HearingDateDateEdit.MenuManager = this.barManager1;
            this.HearingDateDateEdit.Name = "HearingDateDateEdit";
            this.HearingDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HearingDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.HearingDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.HearingDateDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.HearingDateDateEdit.Size = new System.Drawing.Size(398, 20);
            this.HearingDateDateEdit.StyleController = this.dataLayoutControl1;
            this.HearingDateDateEdit.TabIndex = 61;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 189);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(558, 176);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling17);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 68;
            // 
            // HearingNoteTakerNameButtonEdit
            // 
            this.HearingNoteTakerNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "HearingNoteTakerName", true));
            this.HearingNoteTakerNameButtonEdit.Location = new System.Drawing.Point(196, 237);
            this.HearingNoteTakerNameButtonEdit.MenuManager = this.barManager1;
            this.HearingNoteTakerNameButtonEdit.Name = "HearingNoteTakerNameButtonEdit";
            this.HearingNoteTakerNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.HearingNoteTakerNameButtonEdit.Properties.MaxLength = 50;
            this.HearingNoteTakerNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.HearingNoteTakerNameButtonEdit.Size = new System.Drawing.Size(398, 20);
            this.HearingNoteTakerNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.HearingNoteTakerNameButtonEdit.TabIndex = 86;
            this.HearingNoteTakerNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HearingNoteTakerNameButtonEdit_ButtonClick);
            // 
            // AppealNoteTakerNameButtonEdit
            // 
            this.AppealNoteTakerNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00055GetEmployeeSanctionItemsBindingSource, "AppealNoteTakerName", true));
            this.AppealNoteTakerNameButtonEdit.Location = new System.Drawing.Point(196, 237);
            this.AppealNoteTakerNameButtonEdit.MenuManager = this.barManager1;
            this.AppealNoteTakerNameButtonEdit.Name = "AppealNoteTakerNameButtonEdit";
            this.AppealNoteTakerNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions17, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.AppealNoteTakerNameButtonEdit.Properties.MaxLength = 50;
            this.AppealNoteTakerNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AppealNoteTakerNameButtonEdit.Size = new System.Drawing.Size(398, 20);
            this.AppealNoteTakerNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.AppealNoteTakerNameButtonEdit.TabIndex = 95;
            this.AppealNoteTakerNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.AppealNoteTakerNameButtonEdit_ButtonClick);
            // 
            // ItemForSanctionID
            // 
            this.ItemForSanctionID.Control = this.SanctionIDTextEdit;
            this.ItemForSanctionID.CustomizationFormText = "Sanction ID:";
            this.ItemForSanctionID.Location = new System.Drawing.Point(0, 313);
            this.ItemForSanctionID.Name = "ItemForSanctionID";
            this.ItemForSanctionID.Size = new System.Drawing.Size(593, 24);
            this.ItemForSanctionID.Text = "Sanction ID:";
            this.ItemForSanctionID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeID
            // 
            this.ItemForEmployeeID.Control = this.EmployeeIDTextEdit;
            this.ItemForEmployeeID.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeID.Location = new System.Drawing.Point(0, 145);
            this.ItemForEmployeeID.Name = "ItemForEmployeeID";
            this.ItemForEmployeeID.Size = new System.Drawing.Size(593, 24);
            this.ItemForEmployeeID.Text = "Employee ID:";
            this.ItemForEmployeeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeSurname
            // 
            this.ItemForEmployeeSurname.Control = this.EmployeeSurnameTextEdit;
            this.ItemForEmployeeSurname.CustomizationFormText = "Employee Surname:";
            this.ItemForEmployeeSurname.Location = new System.Drawing.Point(0, 169);
            this.ItemForEmployeeSurname.Name = "ItemForEmployeeSurname";
            this.ItemForEmployeeSurname.Size = new System.Drawing.Size(593, 24);
            this.ItemForEmployeeSurname.Text = "Employee Surname:";
            this.ItemForEmployeeSurname.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForEmployeeFirstname
            // 
            this.ItemForEmployeeFirstname.Control = this.EmployeeFirstnameTextEdit;
            this.ItemForEmployeeFirstname.CustomizationFormText = "Employee Forename:";
            this.ItemForEmployeeFirstname.Location = new System.Drawing.Point(0, 121);
            this.ItemForEmployeeFirstname.Name = "ItemForEmployeeFirstname";
            this.ItemForEmployeeFirstname.Size = new System.Drawing.Size(593, 24);
            this.ItemForEmployeeFirstname.Text = "Employee Forename:";
            this.ItemForEmployeeFirstname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeNumber
            // 
            this.ItemForEmployeeNumber.Control = this.EmployeeNumberTextEdit;
            this.ItemForEmployeeNumber.CustomizationFormText = "Employee Number:";
            this.ItemForEmployeeNumber.Location = new System.Drawing.Point(0, 121);
            this.ItemForEmployeeNumber.Name = "ItemForEmployeeNumber";
            this.ItemForEmployeeNumber.Size = new System.Drawing.Size(593, 24);
            this.ItemForEmployeeNumber.Text = "Employee Number:";
            this.ItemForEmployeeNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForHRManagerID
            // 
            this.ItemForHRManagerID.Control = this.HRManagerIDTextEdit;
            this.ItemForHRManagerID.CustomizationFormText = "HR Manager ID:";
            this.ItemForHRManagerID.Location = new System.Drawing.Point(0, 145);
            this.ItemForHRManagerID.Name = "ItemForHRManagerID";
            this.ItemForHRManagerID.Size = new System.Drawing.Size(610, 24);
            this.ItemForHRManagerID.Text = "HR Manager ID:";
            this.ItemForHRManagerID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForRaisedByPersonID
            // 
            this.ItemForRaisedByPersonID.Control = this.RaisedByPersonIDTextEdit;
            this.ItemForRaisedByPersonID.CustomizationFormText = "Raised By ID:";
            this.ItemForRaisedByPersonID.Location = new System.Drawing.Point(0, 169);
            this.ItemForRaisedByPersonID.Name = "ItemForRaisedByPersonID";
            this.ItemForRaisedByPersonID.Size = new System.Drawing.Size(610, 24);
            this.ItemForRaisedByPersonID.Text = "Raised By ID:";
            this.ItemForRaisedByPersonID.TextSize = new System.Drawing.Size(103, 13);
            // 
            // ItemForInvestigatedByPersonID
            // 
            this.ItemForInvestigatedByPersonID.Control = this.InvestigatedByPersonIDTextEdit;
            this.ItemForInvestigatedByPersonID.CustomizationFormText = "Investigated By Person ID:";
            this.ItemForInvestigatedByPersonID.Location = new System.Drawing.Point(0, 48);
            this.ItemForInvestigatedByPersonID.Name = "ItemForInvestigatedByPersonID";
            this.ItemForInvestigatedByPersonID.Size = new System.Drawing.Size(562, 121);
            this.ItemForInvestigatedByPersonID.Text = "Investigated By Person ID:";
            this.ItemForInvestigatedByPersonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForHeardByPersonID
            // 
            this.ItemForHeardByPersonID.Control = this.HeardByPersonIDTextEdit;
            this.ItemForHeardByPersonID.CustomizationFormText = "Hearing By ID:";
            this.ItemForHeardByPersonID.Location = new System.Drawing.Point(0, 121);
            this.ItemForHeardByPersonID.Name = "ItemForHeardByPersonID";
            this.ItemForHeardByPersonID.Size = new System.Drawing.Size(610, 24);
            this.ItemForHeardByPersonID.Text = "Hearing By ID:";
            this.ItemForHeardByPersonID.TextSize = new System.Drawing.Size(121, 13);
            // 
            // ItemForHearingOutcomeIssuedByPersonID
            // 
            this.ItemForHearingOutcomeIssuedByPersonID.Control = this.HearingOutcomeIssuedByPersonIDTextEdit;
            this.ItemForHearingOutcomeIssuedByPersonID.CustomizationFormText = "Hearing Outcome Issued By Person ID:";
            this.ItemForHearingOutcomeIssuedByPersonID.Location = new System.Drawing.Point(0, 146);
            this.ItemForHearingOutcomeIssuedByPersonID.Name = "ItemForHearingOutcomeIssuedByPersonID";
            this.ItemForHearingOutcomeIssuedByPersonID.Size = new System.Drawing.Size(562, 24);
            this.ItemForHearingOutcomeIssuedByPersonID.Text = "Hearing Outcome Issued By Person ID:";
            this.ItemForHearingOutcomeIssuedByPersonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAppealConductedByID
            // 
            this.ItemForAppealConductedByID.Control = this.AppealHeardByPersonIDTextEdit;
            this.ItemForAppealConductedByID.CustomizationFormText = "Appeal Conducted By ID:";
            this.ItemForAppealConductedByID.Location = new System.Drawing.Point(0, 97);
            this.ItemForAppealConductedByID.Name = "ItemForAppealConductedByID";
            this.ItemForAppealConductedByID.Size = new System.Drawing.Size(610, 24);
            this.ItemForAppealConductedByID.Text = "Appeal Heard By Person ID:";
            this.ItemForAppealConductedByID.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForHearingNoteTakerPersonID
            // 
            this.ItemForHearingNoteTakerPersonID.Control = this.HearingNoteTakerPersonIDTextEdit;
            this.ItemForHearingNoteTakerPersonID.CustomizationFormText = "Hearing Note Taker Person ID:";
            this.ItemForHearingNoteTakerPersonID.Location = new System.Drawing.Point(0, 107);
            this.ItemForHearingNoteTakerPersonID.Name = "ItemForHearingNoteTakerPersonID";
            this.ItemForHearingNoteTakerPersonID.Size = new System.Drawing.Size(610, 24);
            this.ItemForHearingNoteTakerPersonID.Text = "Hearing Note Taker Person ID:";
            this.ItemForHearingNoteTakerPersonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAppealNoteTakerPersonID
            // 
            this.ItemForAppealNoteTakerPersonID.Control = this.AppealNoteTakerPersonIDTextEdit;
            this.ItemForAppealNoteTakerPersonID.CustomizationFormText = "Appeal Note Taker Person ID:";
            this.ItemForAppealNoteTakerPersonID.Location = new System.Drawing.Point(0, 97);
            this.ItemForAppealNoteTakerPersonID.Name = "ItemForAppealNoteTakerPersonID";
            this.ItemForAppealNoteTakerPersonID.Size = new System.Drawing.Size(610, 24);
            this.ItemForAppealNoteTakerPersonID.Text = "Appeal Note Taker Person ID:";
            this.ItemForAppealNoteTakerPersonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(630, 609);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.ItemForEmployeeName,
            this.ItemForERIssueTypeID,
            this.layoutControlGroup6,
            this.layoutControlItem3,
            this.layGrpActions,
            this.ItemForHRManagerName});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(610, 589);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(160, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(160, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(160, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(337, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(273, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(160, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 97);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(610, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.AllowHide = false;
            this.ItemForEmployeeName.Control = this.EmployeeNameButtonEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Linked to Employee:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 23);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(610, 24);
            this.ItemForEmployeeName.Text = "Linked to Employee:";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForERIssueTypeID
            // 
            this.ItemForERIssueTypeID.Control = this.ERIssueTypeIDGridLookUpEdit;
            this.ItemForERIssueTypeID.CustomizationFormText = "ER Issue Type:";
            this.ItemForERIssueTypeID.Location = new System.Drawing.Point(0, 47);
            this.ItemForERIssueTypeID.Name = "ItemForERIssueTypeID";
            this.ItemForERIssueTypeID.Size = new System.Drawing.Size(610, 26);
            this.ItemForERIssueTypeID.Text = "ER Issue Type:";
            this.ItemForERIssueTypeID.TextSize = new System.Drawing.Size(157, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 107);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(610, 274);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpDetails;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(586, 228);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpDetails,
            this.layoutControlGroup4,
            this.layGrpHearing,
            this.layoutControlGroup3,
            this.layGrpExpiry,
            this.layGrpRemarks});
            // 
            // layGrpDetails
            // 
            this.layGrpDetails.CustomizationFormText = "Details";
            this.layGrpDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDateRaised,
            this.ItemForLive,
            this.ItemForRaisedByPersonName,
            this.ItemForStatusID,
            this.ItemForSanctionOutcome});
            this.layGrpDetails.Location = new System.Drawing.Point(0, 0);
            this.layGrpDetails.Name = "layGrpDetails";
            this.layGrpDetails.Size = new System.Drawing.Size(562, 180);
            this.layGrpDetails.Text = "Details";
            // 
            // ItemForDateRaised
            // 
            this.ItemForDateRaised.Control = this.DateRaisedDateEdit;
            this.ItemForDateRaised.CustomizationFormText = "Date Raised:";
            this.ItemForDateRaised.Location = new System.Drawing.Point(0, 23);
            this.ItemForDateRaised.Name = "ItemForDateRaised";
            this.ItemForDateRaised.Size = new System.Drawing.Size(562, 24);
            this.ItemForDateRaised.Text = "Date Raised:";
            this.ItemForDateRaised.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForLive
            // 
            this.ItemForLive.Control = this.LiveCheckEdit;
            this.ItemForLive.CustomizationFormText = "Live:";
            this.ItemForLive.Location = new System.Drawing.Point(0, 0);
            this.ItemForLive.Name = "ItemForLive";
            this.ItemForLive.Size = new System.Drawing.Size(562, 23);
            this.ItemForLive.Text = "Live:";
            this.ItemForLive.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForRaisedByPersonName
            // 
            this.ItemForRaisedByPersonName.AllowHide = false;
            this.ItemForRaisedByPersonName.Control = this.RaisedByPersonNameButtonEdit;
            this.ItemForRaisedByPersonName.CustomizationFormText = "Raised By:";
            this.ItemForRaisedByPersonName.Location = new System.Drawing.Point(0, 47);
            this.ItemForRaisedByPersonName.Name = "ItemForRaisedByPersonName";
            this.ItemForRaisedByPersonName.Size = new System.Drawing.Size(562, 24);
            this.ItemForRaisedByPersonName.Text = "Raised By:";
            this.ItemForRaisedByPersonName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForStatusID
            // 
            this.ItemForStatusID.Control = this.StatusIDGridLookUpEdit;
            this.ItemForStatusID.CustomizationFormText = "Status:";
            this.ItemForStatusID.Location = new System.Drawing.Point(0, 71);
            this.ItemForStatusID.Name = "ItemForStatusID";
            this.ItemForStatusID.Size = new System.Drawing.Size(562, 26);
            this.ItemForStatusID.Text = "Status:";
            this.ItemForStatusID.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForSanctionOutcome
            // 
            this.ItemForSanctionOutcome.Control = this.SanctionOutcomeTextEdit;
            this.ItemForSanctionOutcome.CustomizationFormText = "Outcome [Calculated]:";
            this.ItemForSanctionOutcome.Location = new System.Drawing.Point(0, 97);
            this.ItemForSanctionOutcome.Name = "ItemForSanctionOutcome";
            this.ItemForSanctionOutcome.Size = new System.Drawing.Size(562, 83);
            this.ItemForSanctionOutcome.Text = "Outcome [Calculated]:";
            this.ItemForSanctionOutcome.TextSize = new System.Drawing.Size(157, 13);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Investigation";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForInvestigationStartDate,
            this.ItemForInvestigationEndDate,
            this.ItemForInvestigatedByPersonName,
            this.ItemForInvestigationOutcomeID});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(562, 180);
            this.layoutControlGroup4.Text = "Investigation";
            // 
            // ItemForInvestigationStartDate
            // 
            this.ItemForInvestigationStartDate.Control = this.InvestigationStartDateDateEdit;
            this.ItemForInvestigationStartDate.CustomizationFormText = "Investigation Start Date:";
            this.ItemForInvestigationStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForInvestigationStartDate.Name = "ItemForInvestigationStartDate";
            this.ItemForInvestigationStartDate.Size = new System.Drawing.Size(562, 24);
            this.ItemForInvestigationStartDate.Text = "Investigation Start Date:";
            this.ItemForInvestigationStartDate.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForInvestigationEndDate
            // 
            this.ItemForInvestigationEndDate.Control = this.InvestigationEndDateDateEdit;
            this.ItemForInvestigationEndDate.CustomizationFormText = "Investigation End Date:";
            this.ItemForInvestigationEndDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForInvestigationEndDate.Name = "ItemForInvestigationEndDate";
            this.ItemForInvestigationEndDate.Size = new System.Drawing.Size(562, 24);
            this.ItemForInvestigationEndDate.Text = "Investigation End Date:";
            this.ItemForInvestigationEndDate.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForInvestigatedByPersonName
            // 
            this.ItemForInvestigatedByPersonName.Control = this.InvestigatedByPersonNameButtonEdit;
            this.ItemForInvestigatedByPersonName.CustomizationFormText = "Investigating Officer:";
            this.ItemForInvestigatedByPersonName.Location = new System.Drawing.Point(0, 48);
            this.ItemForInvestigatedByPersonName.Name = "ItemForInvestigatedByPersonName";
            this.ItemForInvestigatedByPersonName.Size = new System.Drawing.Size(562, 24);
            this.ItemForInvestigatedByPersonName.Text = "Investigating Officer:";
            this.ItemForInvestigatedByPersonName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForInvestigationOutcomeID
            // 
            this.ItemForInvestigationOutcomeID.Control = this.InvestigationOutcomeIDGridLookUpEdit;
            this.ItemForInvestigationOutcomeID.CustomizationFormText = "Investigation Outcome:";
            this.ItemForInvestigationOutcomeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForInvestigationOutcomeID.Name = "ItemForInvestigationOutcomeID";
            this.ItemForInvestigationOutcomeID.Size = new System.Drawing.Size(562, 108);
            this.ItemForInvestigationOutcomeID.Text = "Investigation Outcome:";
            this.ItemForInvestigationOutcomeID.TextSize = new System.Drawing.Size(157, 13);
            // 
            // layGrpHearing
            // 
            this.layGrpHearing.CustomizationFormText = "Hearing";
            this.layGrpHearing.ExpandButtonVisible = true;
            this.layGrpHearing.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpHearing.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHeardByPersonName,
            this.ItemForHearingDate,
            this.ItemForHearingNoteTakerName,
            this.emptySpaceItem3,
            this.ItemForHearingEmployeeRepresentativeName,
            this.ItemForHearingOutcomeDate,
            this.ItemForHearingOutcomeID,
            this.ItemForHearingOutcomeIssuedByPersonName});
            this.layGrpHearing.Location = new System.Drawing.Point(0, 0);
            this.layGrpHearing.Name = "layGrpHearing";
            this.layGrpHearing.Size = new System.Drawing.Size(562, 180);
            this.layGrpHearing.Text = "Hearing";
            // 
            // ItemForHeardByPersonName
            // 
            this.ItemForHeardByPersonName.Control = this.HeardByPersonNameButtonEdit;
            this.ItemForHeardByPersonName.CustomizationFormText = "Heard By:";
            this.ItemForHeardByPersonName.Location = new System.Drawing.Point(0, 24);
            this.ItemForHeardByPersonName.Name = "ItemForHeardByPersonName";
            this.ItemForHeardByPersonName.Size = new System.Drawing.Size(562, 24);
            this.ItemForHeardByPersonName.Text = "Heard By:";
            this.ItemForHeardByPersonName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForHearingDate
            // 
            this.ItemForHearingDate.Control = this.HearingDateDateEdit;
            this.ItemForHearingDate.CustomizationFormText = "Hearing Date:";
            this.ItemForHearingDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForHearingDate.Name = "ItemForHearingDate";
            this.ItemForHearingDate.Size = new System.Drawing.Size(562, 24);
            this.ItemForHearingDate.Text = "Hearing Date:";
            this.ItemForHearingDate.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForHearingNoteTakerName
            // 
            this.ItemForHearingNoteTakerName.Control = this.HearingNoteTakerNameButtonEdit;
            this.ItemForHearingNoteTakerName.CustomizationFormText = "Note Taker Name:";
            this.ItemForHearingNoteTakerName.Location = new System.Drawing.Point(0, 48);
            this.ItemForHearingNoteTakerName.Name = "ItemForHearingNoteTakerName";
            this.ItemForHearingNoteTakerName.Size = new System.Drawing.Size(562, 24);
            this.ItemForHearingNoteTakerName.Text = "Note Taker Name:";
            this.ItemForHearingNoteTakerName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 170);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(562, 10);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForHearingEmployeeRepresentativeName
            // 
            this.ItemForHearingEmployeeRepresentativeName.Control = this.HearingEmployeeRepresentativeNameTextEdit;
            this.ItemForHearingEmployeeRepresentativeName.CustomizationFormText = "Hearing Employee Representative Name:";
            this.ItemForHearingEmployeeRepresentativeName.Location = new System.Drawing.Point(0, 72);
            this.ItemForHearingEmployeeRepresentativeName.Name = "ItemForHearingEmployeeRepresentativeName";
            this.ItemForHearingEmployeeRepresentativeName.Size = new System.Drawing.Size(562, 24);
            this.ItemForHearingEmployeeRepresentativeName.Text = "Employee Representative Name:";
            this.ItemForHearingEmployeeRepresentativeName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForHearingOutcomeDate
            // 
            this.ItemForHearingOutcomeDate.Control = this.HearingOutcomeDateDateEdit;
            this.ItemForHearingOutcomeDate.CustomizationFormText = "From Date:";
            this.ItemForHearingOutcomeDate.Location = new System.Drawing.Point(0, 122);
            this.ItemForHearingOutcomeDate.Name = "ItemForHearingOutcomeDate";
            this.ItemForHearingOutcomeDate.Size = new System.Drawing.Size(562, 24);
            this.ItemForHearingOutcomeDate.Text = "Hearing Outcome Date:";
            this.ItemForHearingOutcomeDate.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForHearingOutcomeID
            // 
            this.ItemForHearingOutcomeID.Control = this.HearingOutcomeIDGridLookUpEdit;
            this.ItemForHearingOutcomeID.CustomizationFormText = "Hearing Outcome:";
            this.ItemForHearingOutcomeID.Location = new System.Drawing.Point(0, 96);
            this.ItemForHearingOutcomeID.Name = "ItemForHearingOutcomeID";
            this.ItemForHearingOutcomeID.Size = new System.Drawing.Size(562, 26);
            this.ItemForHearingOutcomeID.Text = "Hearing Outcome:";
            this.ItemForHearingOutcomeID.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForHearingOutcomeIssuedByPersonName
            // 
            this.ItemForHearingOutcomeIssuedByPersonName.Control = this.HearingOutcomeIssuedByPersonNameButtonEdit;
            this.ItemForHearingOutcomeIssuedByPersonName.CustomizationFormText = "Hearing Outcome Issued By:";
            this.ItemForHearingOutcomeIssuedByPersonName.Location = new System.Drawing.Point(0, 146);
            this.ItemForHearingOutcomeIssuedByPersonName.Name = "ItemForHearingOutcomeIssuedByPersonName";
            this.ItemForHearingOutcomeIssuedByPersonName.Size = new System.Drawing.Size(562, 24);
            this.ItemForHearingOutcomeIssuedByPersonName.Text = "Hearing Outcome Issued By:";
            this.ItemForHearingOutcomeIssuedByPersonName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "Appeal";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAppealDate,
            this.ItemForAppealHeardByPersonName,
            this.emptySpaceItem4,
            this.ItemForAppealNoteTakerName,
            this.ItemForAppealEmployeeRepresentativeName,
            this.ItemForAppealOutcomeID});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(562, 180);
            this.layoutControlGroup3.Text = "Appeal";
            // 
            // ItemForAppealDate
            // 
            this.ItemForAppealDate.Control = this.AppealDateDateEdit;
            this.ItemForAppealDate.CustomizationFormText = "Appeal Date:";
            this.ItemForAppealDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForAppealDate.Name = "ItemForAppealDate";
            this.ItemForAppealDate.Size = new System.Drawing.Size(562, 24);
            this.ItemForAppealDate.Text = "Appeal Date:";
            this.ItemForAppealDate.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForAppealHeardByPersonName
            // 
            this.ItemForAppealHeardByPersonName.Control = this.AppealHeardByPersonNameButtonEdit;
            this.ItemForAppealHeardByPersonName.CustomizationFormText = "Appeal Conducted By:";
            this.ItemForAppealHeardByPersonName.Location = new System.Drawing.Point(0, 24);
            this.ItemForAppealHeardByPersonName.Name = "ItemForAppealHeardByPersonName";
            this.ItemForAppealHeardByPersonName.Size = new System.Drawing.Size(562, 24);
            this.ItemForAppealHeardByPersonName.Text = "Appeal Heard By:";
            this.ItemForAppealHeardByPersonName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 122);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(562, 58);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAppealNoteTakerName
            // 
            this.ItemForAppealNoteTakerName.Control = this.AppealNoteTakerNameButtonEdit;
            this.ItemForAppealNoteTakerName.CustomizationFormText = "Appeal Note Taker Name:";
            this.ItemForAppealNoteTakerName.Location = new System.Drawing.Point(0, 48);
            this.ItemForAppealNoteTakerName.Name = "ItemForAppealNoteTakerName";
            this.ItemForAppealNoteTakerName.Size = new System.Drawing.Size(562, 24);
            this.ItemForAppealNoteTakerName.Text = "Appeal Note Taker Name:";
            this.ItemForAppealNoteTakerName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForAppealEmployeeRepresentativeName
            // 
            this.ItemForAppealEmployeeRepresentativeName.Control = this.AppealEmployeeRepresentativeNameTextEdit;
            this.ItemForAppealEmployeeRepresentativeName.CustomizationFormText = "Appeal Employee Representative Name:";
            this.ItemForAppealEmployeeRepresentativeName.Location = new System.Drawing.Point(0, 72);
            this.ItemForAppealEmployeeRepresentativeName.Name = "ItemForAppealEmployeeRepresentativeName";
            this.ItemForAppealEmployeeRepresentativeName.Size = new System.Drawing.Size(562, 24);
            this.ItemForAppealEmployeeRepresentativeName.Text = "Employee Representative Name:";
            this.ItemForAppealEmployeeRepresentativeName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForAppealOutcomeID
            // 
            this.ItemForAppealOutcomeID.Control = this.AppealOutcomeIDGridLookUpEdit;
            this.ItemForAppealOutcomeID.CustomizationFormText = "Appeal Outcome:";
            this.ItemForAppealOutcomeID.Location = new System.Drawing.Point(0, 96);
            this.ItemForAppealOutcomeID.Name = "ItemForAppealOutcomeID";
            this.ItemForAppealOutcomeID.Size = new System.Drawing.Size(562, 26);
            this.ItemForAppealOutcomeID.Text = "Appeal Outcome:";
            this.ItemForAppealOutcomeID.TextSize = new System.Drawing.Size(157, 13);
            // 
            // layGrpExpiry
            // 
            this.layGrpExpiry.CustomizationFormText = "Expiry";
            this.layGrpExpiry.ExpandButtonVisible = true;
            this.layGrpExpiry.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpExpiry.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForExpiryDate,
            this.ItemForExpiryDurationUnits,
            this.ItemForExpiryDurationUnitDescriptorID,
            this.emptySpaceItem6});
            this.layGrpExpiry.Location = new System.Drawing.Point(0, 0);
            this.layGrpExpiry.Name = "layGrpExpiry";
            this.layGrpExpiry.Size = new System.Drawing.Size(562, 180);
            this.layGrpExpiry.Text = "Expiry";
            // 
            // ItemForExpiryDate
            // 
            this.ItemForExpiryDate.Control = this.ExpiryDateDateEdit;
            this.ItemForExpiryDate.CustomizationFormText = "Expiry Date:";
            this.ItemForExpiryDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForExpiryDate.Name = "ItemForExpiryDate";
            this.ItemForExpiryDate.Size = new System.Drawing.Size(562, 24);
            this.ItemForExpiryDate.Text = "Expiry Date:";
            this.ItemForExpiryDate.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForExpiryDurationUnits
            // 
            this.ItemForExpiryDurationUnits.Control = this.ExpiryDurationUnitsSpinEdit;
            this.ItemForExpiryDurationUnits.CustomizationFormText = "Duration Units:";
            this.ItemForExpiryDurationUnits.Location = new System.Drawing.Point(0, 0);
            this.ItemForExpiryDurationUnits.Name = "ItemForExpiryDurationUnits";
            this.ItemForExpiryDurationUnits.Size = new System.Drawing.Size(214, 24);
            this.ItemForExpiryDurationUnits.Text = "Duration Units:";
            this.ItemForExpiryDurationUnits.TextSize = new System.Drawing.Size(157, 13);
            // 
            // ItemForExpiryDurationUnitDescriptorID
            // 
            this.ItemForExpiryDurationUnitDescriptorID.Control = this.ExpiryDurationUnitDescriptorIDGridLookUpEdit;
            this.ItemForExpiryDurationUnitDescriptorID.CustomizationFormText = "Duration Unit Descriptor:";
            this.ItemForExpiryDurationUnitDescriptorID.Location = new System.Drawing.Point(214, 0);
            this.ItemForExpiryDurationUnitDescriptorID.Name = "ItemForExpiryDurationUnitDescriptorID";
            this.ItemForExpiryDurationUnitDescriptorID.Size = new System.Drawing.Size(348, 24);
            this.ItemForExpiryDurationUnitDescriptorID.Text = "Unit Descriptor:";
            this.ItemForExpiryDurationUnitDescriptorID.TextSize = new System.Drawing.Size(157, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(562, 132);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layGrpRemarks
            // 
            this.layGrpRemarks.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layGrpRemarks.CustomizationFormText = "Remarks";
            this.layGrpRemarks.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layGrpRemarks.Location = new System.Drawing.Point(0, 0);
            this.layGrpRemarks.Name = "layGrpRemarks";
            this.layGrpRemarks.Size = new System.Drawing.Size(562, 180);
            this.layGrpRemarks.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(562, 180);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnSave;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 381);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(610, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layGrpActions
            // 
            this.layGrpActions.CustomizationFormText = "Action / Progress History";
            this.layGrpActions.ExpandButtonVisible = true;
            this.layGrpActions.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpActions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layGrpActions.Location = new System.Drawing.Point(0, 407);
            this.layGrpActions.Name = "layGrpActions";
            this.layGrpActions.Size = new System.Drawing.Size(610, 182);
            this.layGrpActions.Text = "Action / Progress History";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridSplitContainer1;
            this.layoutControlItem2.CustomizationFormText = "Linked Progress Grid:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(586, 136);
            this.layoutControlItem2.Text = "Linked Progress Grid:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForHRManagerName
            // 
            this.ItemForHRManagerName.Control = this.HRManagerNameButtonEdit;
            this.ItemForHRManagerName.CustomizationFormText = "HR Manager:";
            this.ItemForHRManagerName.Location = new System.Drawing.Point(0, 73);
            this.ItemForHRManagerName.Name = "ItemForHRManagerName";
            this.ItemForHRManagerName.Size = new System.Drawing.Size(610, 24);
            this.ItemForHRManagerName.Text = "HR Manager:";
            this.ItemForHRManagerName.TextSize = new System.Drawing.Size(157, 13);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter
            // 
            this.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00057_Get_Employee_Sanction_ProgressTableAdapter
            // 
            this.sp_HR_00057_Get_Employee_Sanction_ProgressTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter
            // 
            this.sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter
            // 
            this.sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00075_Sanction_Duration_Units_Descriptors_With_BlankTableAdapter
            // 
            this.sp_HR_00075_Sanction_Duration_Units_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter
            // 
            this.sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00129_Discipline_Investigation_Outcomes_With_BlankTableAdapter
            // 
            this.sp_HR_00129_Discipline_Investigation_Outcomes_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter
            // 
            this.sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Sanction_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(630, 665);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Sanction_Edit";
            this.Text = "Edit Discipline & Grievance Record";
            this.Activated += new System.EventHandler(this.frm_HR_Sanction_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Sanction_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Sanction_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AppealNoteTakerPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00055GetEmployeeSanctionItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingNoteTakerPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeIssuedByPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigatedByPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SanctionOutcomeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealEmployeeRepresentativeNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeIssuedByPersonNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00130SanctionHearingOutcomeListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingEmployeeRepresentativeNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationOutcomeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00129DisciplineInvestigationOutcomesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigatedByPersonNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationEndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationEndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationStartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InvestigationStartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HRManagerNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HRManagerIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealHeardByPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SanctionIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaisedByPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeardByPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealHeardByPersonNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealOutcomeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00039AppealOutcomesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryDurationUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00075SanctionDurationUnitsDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatusIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00074SanctionStatusesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERIssueTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00073SanctionTypesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00057GetEmployeeSanctionProgressBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateRaisedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingOutcomeDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryDurationUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RaisedByPersonNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeardByPersonNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HearingNoteTakerNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppealNoteTakerNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSanctionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHRManagerID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRaisedByPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigatedByPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeardByPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingOutcomeIssuedByPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealConductedByID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingNoteTakerPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealNoteTakerPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForERIssueTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateRaised)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRaisedByPersonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStatusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSanctionOutcome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigationStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigationEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigatedByPersonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInvestigationOutcomeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpHearing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeardByPersonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingNoteTakerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingEmployeeRepresentativeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingOutcomeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingOutcomeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHearingOutcomeIssuedByPersonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealHeardByPersonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealNoteTakerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealEmployeeRepresentativeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAppealOutcomeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpExpiry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryDurationUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryDurationUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpActions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHRManagerName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit EmployeeFirstnameTextEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.TextEdit EmployeeSurnameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeFirstname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeSurname;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpDetails;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpRemarks;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpHearing;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.ButtonEdit EmployeeNameButtonEdit;
        private System.Windows.Forms.BindingSource spHR00055GetEmployeeSanctionItemsBindingSource;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.DateEdit DateRaisedDateEdit;
        private DevExpress.XtraEditors.DateEdit HearingOutcomeDateDateEdit;
        private DevExpress.XtraEditors.DateEdit ExpiryDateDateEdit;
        private DevExpress.XtraEditors.CheckEdit LiveCheckEdit;
        private DevExpress.XtraEditors.SpinEdit ExpiryDurationUnitsSpinEdit;
        private DevExpress.XtraEditors.ButtonEdit RaisedByPersonNameButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit HeardByPersonNameButtonEdit;
        private DevExpress.XtraEditors.DateEdit HearingDateDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateRaised;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHearingOutcomeDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpiryDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLive;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpiryDurationUnits;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRaisedByPersonName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHeardByPersonName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHearingDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpExpiry;
        private System.Windows.Forms.BindingSource spHR00057GetEmployeeSanctionProgressBindingSource;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colId1;
        private DevExpress.XtraGrid.Columns.GridColumn colSanctionId;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDetail;
        private DataSet_HR_CoreTableAdapters.sp_HR_00057_Get_Employee_Sanction_ProgressTableAdapter sp_HR_00057_Get_Employee_Sanction_ProgressTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colStartedById;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedById;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit EmployeeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeID;
        private DevExpress.XtraEditors.TextEdit EmployeeNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeNumber;
        private DevExpress.XtraEditors.GridLookUpEdit ERIssueTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForERIssueTypeID;
        private System.Windows.Forms.BindingSource spHR00073SanctionTypesListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter sp_HR_00073_Sanction_Types_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.GridLookUpEdit StatusIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStatusID;
        private System.Windows.Forms.BindingSource spHR00074SanctionStatusesListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter sp_HR_00074_Sanction_Statuses_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.TextEdit HeardByPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHeardByPersonID;
        private DevExpress.XtraEditors.TextEdit RaisedByPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRaisedByPersonID;
        private DevExpress.XtraEditors.GridLookUpEdit ExpiryDurationUnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpiryDurationUnitDescriptorID;
        private System.Windows.Forms.BindingSource spHR00075SanctionDurationUnitsDescriptorsWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00075_Sanction_Duration_Units_Descriptors_With_BlankTableAdapter sp_HR_00075_Sanction_Duration_Units_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpActions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.TextEdit SanctionIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSanctionID;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.DateEdit AppealDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAppealDate;
        private DevExpress.XtraEditors.GridLookUpEdit AppealOutcomeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAppealOutcomeID;
        private DevExpress.XtraEditors.TextEdit AppealHeardByPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAppealConductedByID;
        private DevExpress.XtraEditors.ButtonEdit AppealHeardByPersonNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAppealHeardByPersonName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHearingNoteTakerName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private System.Windows.Forms.BindingSource spHR00039AppealOutcomesListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter sp_HR_00039_Appeal_Outcomes_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.TextEdit HRManagerIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHRManagerID;
        private DevExpress.XtraEditors.ButtonEdit HRManagerNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHRManagerName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraEditors.DateEdit InvestigationEndDateDateEdit;
        private DevExpress.XtraEditors.DateEdit InvestigationStartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvestigationStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvestigationEndDate;
        private DevExpress.XtraEditors.TextEdit InvestigatedByPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvestigatedByPersonID;
        private DevExpress.XtraEditors.ButtonEdit InvestigatedByPersonNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvestigatedByPersonName;
        private DevExpress.XtraEditors.GridLookUpEdit InvestigationOutcomeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInvestigationOutcomeID;
        private System.Windows.Forms.BindingSource spHR00129DisciplineInvestigationOutcomesWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00129_Discipline_Investigation_Outcomes_With_BlankTableAdapter sp_HR_00129_Discipline_Investigation_Outcomes_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraEditors.TextEdit HearingEmployeeRepresentativeNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHearingEmployeeRepresentativeName;
        private DevExpress.XtraEditors.GridLookUpEdit HearingOutcomeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHearingOutcomeID;
        private System.Windows.Forms.BindingSource spHR00130SanctionHearingOutcomeListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter sp_HR_00130_Sanction_Hearing_Outcome_List_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit HearingOutcomeIssuedByPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHearingOutcomeIssuedByPersonID;
        private DevExpress.XtraEditors.ButtonEdit HearingOutcomeIssuedByPersonNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHearingOutcomeIssuedByPersonName;
        private DevExpress.XtraEditors.TextEdit AppealEmployeeRepresentativeNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAppealNoteTakerName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAppealEmployeeRepresentativeName;
        private DevExpress.XtraEditors.TextEdit SanctionOutcomeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSanctionOutcome;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private DevExpress.XtraEditors.TextEdit AppealNoteTakerPersonIDTextEdit;
        private DevExpress.XtraEditors.TextEdit HearingNoteTakerPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHearingNoteTakerPersonID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAppealNoteTakerPersonID;
        private DevExpress.XtraEditors.ButtonEdit HearingNoteTakerNameButtonEdit;
        private DevExpress.XtraEditors.ButtonEdit AppealNoteTakerNameButtonEdit;
    }
}

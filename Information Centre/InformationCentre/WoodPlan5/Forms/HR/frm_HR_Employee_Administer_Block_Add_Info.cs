﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Administer_Block_Add_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public DateTime? dtDateFrom = null;
        public DateTime? dtDateTo = null;
        public int? intActive = null;
        public int? intTypeID = null;

        #endregion

        public frm_HR_Employee_Administer_Block_Add_Info()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Administer_Block_Add_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500240;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                sp_HR_00252_Administer_Type_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp_HR_00252_Administer_Type_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00252_Administer_Type_List_With_Blank);
            }
            catch (Exception) { }

            dateEditDateFrom.EditValue = DateTime.Today;
            dateEditDateTo.EditValue = null;
            gridLookUpEditTypeId.EditValue = 1;
            checkEditActive.Checked = true;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dateEditDateFrom.EditValue != null) dtDateFrom = Convert.ToDateTime(dateEditDateFrom.EditValue);
            if (dateEditDateTo.EditValue != null) dtDateTo = Convert.ToDateTime(dateEditDateTo.EditValue);
            if (gridLookUpEditTypeId.EditValue != null) intTypeID = Convert.ToInt32(gridLookUpEditTypeId.EditValue);
            if (checkEditActive.EditValue != null) intActive = Convert.ToInt32(checkEditActive.EditValue);

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }





    }
}

namespace WoodPlan5.Forms.HR
{
    partial class CustomAppointmentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbBreakLength = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cePaidForBreaks = new DevExpress.XtraEditors.CheckEdit();
            this.tbCalculatedHours = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.tbLocationName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.tbBusinessAreaName = new DevExpress.XtraEditors.TextEdit();
            this.tbRegionName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.tbEmployeeName = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAllDay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStartDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStartDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEndDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEndDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStartTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEndTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtLabel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtShowTimeAs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSubject.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtResource.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtResources.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtResources.ResourcesCheckedListBoxControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReminder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbReminder.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbLocation.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            this.progressPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbProgress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProgress.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBreakLength.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cePaidForBreaks.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalculatedHours.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbLocationName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBusinessAreaName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRegionName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEmployeeName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblSubject
            // 
            this.lblSubject.Location = new System.Drawing.Point(443, 51);
            this.lblSubject.Visible = false;
            // 
            // lblLocation
            // 
            this.lblLocation.Location = new System.Drawing.Point(16, 50);
            this.lblLocation.Size = new System.Drawing.Size(61, 13);
            this.lblLocation.Text = "Department:";
            // 
            // lblLabel
            // 
            this.lblLabel.Appearance.BackColor = System.Drawing.Color.Transparent;
            // 
            // lblStartTime
            // 
            this.lblStartTime.Location = new System.Drawing.Point(16, 159);
            this.lblStartTime.Text = "Start time:";
            // 
            // lblEndTime
            // 
            this.lblEndTime.Location = new System.Drawing.Point(16, 184);
            this.lblEndTime.Text = "End time:";
            // 
            // lblShowTimeAs
            // 
            this.lblShowTimeAs.Location = new System.Drawing.Point(440, 212);
            this.lblShowTimeAs.Visible = false;
            // 
            // chkAllDay
            // 
            this.chkAllDay.Enabled = false;
            this.chkAllDay.Visible = false;
            // 
            // btnOk
            // 
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(16, 354);
            this.btnOk.Visible = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(405, 354);
            // 
            // btnDelete
            // 
            this.btnDelete.Enabled = false;
            this.btnDelete.Location = new System.Drawing.Point(97, 354);
            this.btnDelete.Visible = false;
            // 
            // btnRecurrence
            // 
            this.btnRecurrence.Enabled = false;
            this.btnRecurrence.Location = new System.Drawing.Point(178, 354);
            this.btnRecurrence.Visible = false;
            // 
            // edtStartDate
            // 
            this.edtStartDate.EditValue = new System.DateTime(2005, 3, 31, 0, 0, 0, 0);
            this.edtStartDate.Location = new System.Drawing.Point(106, 156);
            this.edtStartDate.Size = new System.Drawing.Size(280, 20);
            // 
            // edtEndDate
            // 
            this.edtEndDate.EditValue = new System.DateTime(2005, 3, 31, 0, 0, 0, 0);
            this.edtEndDate.Location = new System.Drawing.Point(106, 181);
            this.edtEndDate.Size = new System.Drawing.Size(280, 20);
            // 
            // edtStartTime
            // 
            this.edtStartTime.EditValue = new System.DateTime(2005, 3, 31, 0, 0, 0, 0);
            this.edtStartTime.Location = new System.Drawing.Point(392, 157);
            // 
            // edtEndTime
            // 
            this.edtEndTime.EditValue = new System.DateTime(2005, 3, 31, 0, 0, 0, 0);
            this.edtEndTime.Location = new System.Drawing.Point(392, 181);
            // 
            // edtLabel
            // 
            this.edtLabel.Visible = false;
            // 
            // edtShowTimeAs
            // 
            this.edtShowTimeAs.Enabled = false;
            this.edtShowTimeAs.Location = new System.Drawing.Point(569, 186);
            this.edtShowTimeAs.Size = new System.Drawing.Size(184, 20);
            this.edtShowTimeAs.Visible = false;
            // 
            // tbSubject
            // 
            this.tbSubject.Enabled = false;
            this.tbSubject.Location = new System.Drawing.Point(489, 48);
            this.tbSubject.Size = new System.Drawing.Size(384, 20);
            this.tbSubject.Visible = false;
            // 
            // edtResource
            // 
            this.edtResource.Enabled = false;
            this.edtResource.Visible = false;
            // 
            // edtResources
            // 
            // 
            // 
            // 
            this.edtResources.ResourcesCheckedListBoxControl.CheckOnClick = true;
            this.edtResources.ResourcesCheckedListBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.edtResources.ResourcesCheckedListBoxControl.Location = new System.Drawing.Point(0, 0);
            this.edtResources.ResourcesCheckedListBoxControl.Name = "";
            this.edtResources.ResourcesCheckedListBoxControl.Size = new System.Drawing.Size(200, 100);
            this.edtResources.ResourcesCheckedListBoxControl.TabIndex = 0;
            // 
            // chkReminder
            // 
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(16, 279);
            this.tbDescription.Size = new System.Drawing.Size(464, 66);
            // 
            // cbReminder
            // 
            this.cbReminder.Enabled = false;
            this.cbReminder.Visible = false;
            // 
            // tbLocation
            // 
            this.tbLocation.Location = new System.Drawing.Point(106, 47);
            this.tbLocation.Size = new System.Drawing.Size(374, 20);
            // 
            // panel1
            // 
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(399, 86);
            this.panel1.Visible = false;
            // 
            // progressPanel
            // 
            this.progressPanel.Location = new System.Drawing.Point(441, 231);
            this.progressPanel.Size = new System.Drawing.Size(464, 30);
            // 
            // tbProgress
            // 
            this.tbProgress.Enabled = false;
            this.tbProgress.Properties.LabelAppearance.Options.UseTextOptions = true;
            this.tbProgress.Properties.LabelAppearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tbProgress.Size = new System.Drawing.Size(355, 27);
            this.tbProgress.Visible = false;
            // 
            // lblPercentComplete
            // 
            this.lblPercentComplete.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPercentComplete.Location = new System.Drawing.Point(4, 10);
            // 
            // lblPercentCompleteValue
            // 
            this.lblPercentCompleteValue.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.lblPercentCompleteValue.Location = new System.Drawing.Point(446, 10);
            // 
            // tbBreakLength
            // 
            this.tbBreakLength.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBreakLength.Location = new System.Drawing.Point(106, 206);
            this.tbBreakLength.Name = "tbBreakLength";
            this.tbBreakLength.Properties.Mask.EditMask = "####0 Minutes";
            this.tbBreakLength.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.tbBreakLength.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.tbBreakLength.Size = new System.Drawing.Size(280, 20);
            this.tbBreakLength.TabIndex = 29;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(16, 209);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(67, 13);
            this.labelControl1.TabIndex = 30;
            this.labelControl1.Text = "Break Length:";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(436, 209);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(24, 13);
            this.labelControl2.TabIndex = 31;
            this.labelControl2.Text = "Paid:";
            // 
            // cePaidForBreaks
            // 
            this.cePaidForBreaks.Location = new System.Drawing.Point(462, 206);
            this.cePaidForBreaks.Name = "cePaidForBreaks";
            this.cePaidForBreaks.Properties.Caption = "";
            this.cePaidForBreaks.Properties.ValueChecked = 1;
            this.cePaidForBreaks.Properties.ValueUnchecked = 0;
            this.cePaidForBreaks.Size = new System.Drawing.Size(21, 19);
            this.cePaidForBreaks.TabIndex = 32;
            // 
            // tbCalculatedHours
            // 
            this.tbCalculatedHours.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCalculatedHours.Location = new System.Drawing.Point(106, 231);
            this.tbCalculatedHours.Name = "tbCalculatedHours";
            this.tbCalculatedHours.Properties.Mask.EditMask = "####0.00 Hours";
            this.tbCalculatedHours.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.tbCalculatedHours.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.tbCalculatedHours.Size = new System.Drawing.Size(280, 20);
            this.tbCalculatedHours.TabIndex = 33;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(16, 234);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(85, 13);
            this.labelControl4.TabIndex = 35;
            this.labelControl4.Text = "Calculated Hours:";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(16, 75);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(44, 13);
            this.labelControl3.TabIndex = 36;
            this.labelControl3.Text = "Location:";
            // 
            // tbLocationName
            // 
            this.tbLocationName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLocationName.Location = new System.Drawing.Point(106, 72);
            this.tbLocationName.Name = "tbLocationName";
            this.tbLocationName.Size = new System.Drawing.Size(374, 20);
            this.tbLocationName.TabIndex = 37;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(16, 100);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(71, 13);
            this.labelControl5.TabIndex = 38;
            this.labelControl5.Text = "Business Area:";
            // 
            // tbBusinessAreaName
            // 
            this.tbBusinessAreaName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBusinessAreaName.Location = new System.Drawing.Point(106, 97);
            this.tbBusinessAreaName.Name = "tbBusinessAreaName";
            this.tbBusinessAreaName.Size = new System.Drawing.Size(374, 20);
            this.tbBusinessAreaName.TabIndex = 39;
            // 
            // tbRegionName
            // 
            this.tbRegionName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbRegionName.Location = new System.Drawing.Point(106, 122);
            this.tbRegionName.Name = "tbRegionName";
            this.tbRegionName.Size = new System.Drawing.Size(374, 20);
            this.tbRegionName.TabIndex = 40;
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(16, 125);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(37, 13);
            this.labelControl6.TabIndex = 41;
            this.labelControl6.Text = "Region:";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(16, 14);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(50, 13);
            this.labelControl7.TabIndex = 43;
            this.labelControl7.Text = "Employee:";
            // 
            // tbEmployeeName
            // 
            this.tbEmployeeName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbEmployeeName.Location = new System.Drawing.Point(106, 11);
            this.tbEmployeeName.Name = "tbEmployeeName";
            this.tbEmployeeName.Size = new System.Drawing.Size(374, 20);
            this.tbEmployeeName.TabIndex = 42;
            // 
            // CustomAppointmentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(496, 388);
            this.Controls.Add(this.cePaidForBreaks);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.tbEmployeeName);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.tbRegionName);
            this.Controls.Add(this.tbBusinessAreaName);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.tbLocationName);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.tbCalculatedHours);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.tbBreakLength);
            this.MinimumSize = new System.Drawing.Size(502, 294);
            this.Name = "CustomAppointmentForm";
            this.Text = "View Work Shift";
            this.Controls.SetChildIndex(this.edtShowTimeAs, 0);
            this.Controls.SetChildIndex(this.edtEndDate, 0);
            this.Controls.SetChildIndex(this.btnRecurrence, 0);
            this.Controls.SetChildIndex(this.btnDelete, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.lblShowTimeAs, 0);
            this.Controls.SetChildIndex(this.lblEndTime, 0);
            this.Controls.SetChildIndex(this.tbLocation, 0);
            this.Controls.SetChildIndex(this.lblSubject, 0);
            this.Controls.SetChildIndex(this.lblLocation, 0);
            this.Controls.SetChildIndex(this.tbSubject, 0);
            this.Controls.SetChildIndex(this.lblStartTime, 0);
            this.Controls.SetChildIndex(this.btnOk, 0);
            this.Controls.SetChildIndex(this.edtStartDate, 0);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.progressPanel, 0);
            this.Controls.SetChildIndex(this.tbDescription, 0);
            this.Controls.SetChildIndex(this.tbBreakLength, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.tbCalculatedHours, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.tbLocationName, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.tbBusinessAreaName, 0);
            this.Controls.SetChildIndex(this.tbRegionName, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.tbEmployeeName, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.edtEndTime, 0);
            this.Controls.SetChildIndex(this.edtStartTime, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.cePaidForBreaks, 0);
            ((System.ComponentModel.ISupportInitialize)(this.chkAllDay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStartDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStartDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEndDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEndDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtStartTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtEndTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtLabel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtShowTimeAs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbSubject.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtResource.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtResources.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtResources.ResourcesCheckedListBoxControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReminder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbReminder.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbLocation.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.progressPanel.ResumeLayout(false);
            this.progressPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbProgress.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbProgress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBreakLength.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cePaidForBreaks.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalculatedHours.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbLocationName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbBusinessAreaName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbRegionName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbEmployeeName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit tbBreakLength;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.CheckEdit cePaidForBreaks;
        private DevExpress.XtraEditors.TextEdit tbCalculatedHours;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit tbLocationName;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit tbBusinessAreaName;
        private DevExpress.XtraEditors.TextEdit tbRegionName;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit tbEmployeeName;
    }
}

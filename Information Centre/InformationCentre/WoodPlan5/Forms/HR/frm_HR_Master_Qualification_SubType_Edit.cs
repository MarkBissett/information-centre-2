using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class frm_HR_Master_Qualification_SubType_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        public int UpdateRefreshStatus = 0;
        public int ParentRecordId = 0;
        public int _LastOrder = 0;
        #endregion

        private string EquivalentMembersText;
        private string EquivalentOwnersText;

        public frm_HR_Master_Qualification_SubType_Edit()
        {
            InitializeComponent();

            EquivalentMembersText = layoutControlGroup9.Text;
            EquivalentOwnersText = layoutControlGroup11.Text;

        }

        private void frm_HR_Master_Qualification_SubType_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 700006;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00236_Master_Qualification_Types_With_Blank);

            sp_TR_00001_Qualification_SubType_StatusTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00001_Qualification_SubType_StatusTableAdapter.Fill(dataSet_TR_Core.sp_TR_00001_Qualification_SubType_Status);

            sp_TR_00002_Qualification_SubType_ValidityTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00002_Qualification_SubType_ValidityTableAdapter.Fill(dataSet_TR_Core.sp_TR_00002_Qualification_SubType_Validity);

            sp_TR_00003_Qualification_SubType_Expiry_PeriodTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00003_Qualification_SubType_Expiry_PeriodTableAdapter.Fill(dataSet_TR_Core.sp_TR_00003_Qualification_SubType_Expiry_Period);

            sp_TR_00004_Qualification_SubType_RefreshTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00004_Qualification_SubType_RefreshTableAdapter.Fill(dataSet_TR_Core.sp_TR_00004_Qualification_SubType_Refresh);
            
            sp_TR_00005_Qualification_SubType_Refresh_IntervalTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00005_Qualification_SubType_Refresh_IntervalTableAdapter.Fill(dataSet_TR_Core.sp_TR_00005_Qualification_SubType_Refresh_Interval);

            // Populate Main Dataset //   
            sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            BlockModePreset();
            awardingBodyLayoutControlGroup.Enabled = false;
            refreshesLayoutControlGroup.Enabled = false;
            refreshedByLayoutControlGroup.Enabled = false;
            allocationLayoutControlGroup.Enabled = false;

            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.NewRow();
                        drNewRow["Mode"] = strFormMode;
                        drNewRow["RecordIds"] = strRecordIDs;
                        drNewRow["QualificationTypeID"] = ParentRecordId;
                        drNewRow["OrderID"] = _LastOrder + 1;
                        drNewRow["CertificateRequired"] = 1;   //Default to IsRequired
                        this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.NewRow();
                        drNewRow["Mode"] = strFormMode;
                        drNewRow["RecordIds"] = strRecordIDs;
                        this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows.Add(drNewRow);
                        this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit, strFormMode, strRecordIDs);

                        awardingBodyLayoutControlGroup.Enabled = true;
                        refreshesLayoutControlGroup.Enabled = true;
                        refreshedByLayoutControlGroup.Enabled = true;
                        allocationLayoutControlGroup.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
            }

            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }

        private void BlockModePreset()
        {
            ExpiryPeriodTypeGridLookUpEdit.ReadOnly = true;
            expiryPeriodSpinEdit.ReadOnly = true;
            RefreshIntervalTypeGridLookUpEdit.ReadOnly = true;
            refreshIntervalSpinEdit.ReadOnly = true;
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Qualification Sub-Type", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        private void frm_HR_Master_Qualification_SubType_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                //if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedAwardingBodyIDs))
                if (UpdateRefreshStatus == 1)
                {
                    maintainQualificationAwardingBody.Reload();
                    awardingBodyLayoutControlGroup.Text = "Awarding Body : " + maintainQualificationAwardingBody.AwardingBodyCount.ToString();
                }
                if (UpdateRefreshStatus == 2)
                {
                    maintainQualificationRefreshes.Reload();
                    refreshesLayoutControlGroup.Text = "Refreshes : " + maintainQualificationRefreshes.RefreshesCount.ToString();                       
                }
                if (UpdateRefreshStatus == 3)
                {
                    maintainQualificationRefreshedBy.Reload();
                    refreshedByLayoutControlGroup.Text = "Refreshed By : " + maintainQualificationRefreshedBy.RefreshedByCount.ToString();                  
                }
                if (UpdateRefreshStatus == 4)
                {
                    maintainQualificationAllocation.Reload();
                    allocationLayoutControlGroup.Text = "Training Allocation : " + maintainQualificationAllocation.AllocationCount.ToString();
                }
                if (UpdateRefreshStatus == 5)
                {
                    maintainJointMemberQualifications.Reload();
                    layoutControlGroup9.Text = EquivalentMembersText + " : " + maintainJointMemberQualifications.MemberCount.ToString();
                }
                if (UpdateRefreshStatus == 6)
                {
                    maintainQualificationReplaces.Reload();
                    replacesLayoutControlGroup.Text = "Replaces : " + maintainQualificationReplaces.ReplacesCount.ToString();
                }
                if (UpdateRefreshStatus == 7)
                {
                    maintainJointOwnerQualifications.Reload();
                    layoutControlGroup11.Text = EquivalentOwnersText + " : " + maintainJointOwnerQualifications.OwnerCount.ToString();
                }
            }

            UpdateRefreshStatus = 0;
            SetMenuStatus();
        }

        //public void UpdateFormRefreshStatus(int status, string newIds)
        public void UpdateFormRefreshStatus(int status)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;

        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        DescriptionTextEdit.Focus();

                        DescriptionTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        DescriptionTextEdit.Focus();

                        DescriptionTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        DescriptionTextEdit.Focus();

                        DescriptionTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        RemarksMemoEdit.Focus();

                        DescriptionTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        /**
        private void frm_HR_Master_Qualification_SubType_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }
        **/

        private void frm_HR_Master_Qualification_SubType_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();
            
            this.spHR00234MasterQualificationSubTypeEditBindingSource.EndEdit();
            try
            {
                this.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.Update(dataSet_HR_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spHR00234MasterQualificationSubTypeEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["QualificationSubTypeID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["Mode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                frm_HR_Master_Qualification_Types_Manager fParentForm;
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Master_Qualification_Types_Manager")
                    {
                        fParentForm = (frm_HR_Master_Qualification_Types_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Utils.enmMasterVettingGrids.VettingSubType, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            StringBuilder sbMessage = new StringBuilder();

            if (intRecordNew > 0) sbMessage.Append( Convert.ToString(intRecordNew) + " New record(s)\n");
            if (intRecordModified > 0) sbMessage.Append(Convert.ToString(intRecordModified) + " Updated record(s)\n");
            if (intRecordDeleted > 0) sbMessage.Append(Convert.ToString(intRecordDeleted) + " Deleted record(s)\n");
            
            //to do this probably need a seperate pending changes check for the tabs, so
            //can go back and complete rather than auto-save.
            //if (maintainQualificationAwardingBody.ChangesPending()) { sbMessage.Append("Awarding Body amendments\n\n"); }              

            if (string.IsNullOrEmpty(sbMessage.ToString()))
                { return ""; }
            else
            {
                return "You have unsaved changes on the current screen...\n\n" +
                    sbMessage.ToString();
            }
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            //Set up tabbed detail screens
            if (this.strFormMode.ToLower() == "edit" || this.strFormMode.ToLower() == "view")
            {
                DataRowView currentRow = (DataRowView)spHR00234MasterQualificationSubTypeEditBindingSource.Current;
                if (currentRow != null)
                {
                    int qualificationSubTypeID = Convert.ToInt32(currentRow["QualificationSubTypeID"]);

                    string recordID = qualificationSubTypeID + ",";

                    maintainQualificationAwardingBody.SetUp(strConnectionString, recordID,strFormMode );
                    awardingBodyLayoutControlGroup.Text = "Awarding Body : " + maintainQualificationAwardingBody.AwardingBodyCount.ToString();

                    maintainQualificationRefreshes.SetUp(strConnectionString, qualificationSubTypeID, strFormMode);
                    refreshesLayoutControlGroup.Text = "Refreshes : " + maintainQualificationRefreshes.RefreshesCount.ToString();

                    maintainQualificationRefreshedBy.SetUp(strConnectionString, qualificationSubTypeID, strFormMode);
                    refreshedByLayoutControlGroup.Text = "Refreshed By : " + maintainQualificationRefreshedBy.RefreshedByCount.ToString();

                    maintainQualificationAllocation.SetUp(strConnectionString, qualificationSubTypeID, strFormMode);
                    allocationLayoutControlGroup.Text = "Training Allocation : " + maintainQualificationAllocation.AllocationCount.ToString();

                    maintainJointMemberQualifications.SetUp(strConnectionString, qualificationSubTypeID, strFormMode);
                    layoutControlGroup9.Text = EquivalentMembersText + " : " + maintainJointMemberQualifications.MemberCount.ToString();

                    maintainJointOwnerQualifications.SetUp(strConnectionString, qualificationSubTypeID, strFormMode);
                    layoutControlGroup11.Text = EquivalentOwnersText + " : " + maintainJointOwnerQualifications.OwnerCount.ToString();

                    maintainQualificationReplaces.SetUp(strConnectionString, qualificationSubTypeID, strFormMode);
                    replacesLayoutControlGroup.Text = "Replaces : " + maintainQualificationReplaces.ReplacesCount.ToString();

                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void DescriptionTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(te.EditValue.ToString()) || te.EditValue.ToString() == ""))
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "");
            }
        }

        private void QualificationTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || Convert.ToInt32(glue.EditValue) == 0))
            {
                dxErrorProvider1.SetError(QualificationTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(QualificationTypeIDGridLookUpEdit, "");
            }
        }

        private void QualificationSubTypeStatusTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || Convert.ToInt32(glue.EditValue) == 0))
            {
                dxErrorProvider1.SetError(QualificationSubTypeStatusTypeGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(QualificationSubTypeStatusTypeGridLookUpEdit, "");
            }
        }

        private void startDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;

            dxErrorProvider1.SetError(de,"");
            if (this.strFormMode != "blockedit")
            {
                if (de.EditValue == null)
                { return; }

                if (string.IsNullOrEmpty(de.EditValue.ToString()))
                { return; }

                if (string.IsNullOrEmpty(endDateDateEdit.EditValue.ToString()))
                { return; }

                if (de.DateTime > endDateDateEdit.DateTime)
                {
                    dxErrorProvider1.SetError(de, "Start Date cannot be greater than End Date.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }

        }

        private void endDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;

            dxErrorProvider1.SetError(de, "");
            if (this.strFormMode != "blockedit")
            {
                if (de.EditValue == null)
                { return; }

                if (string.IsNullOrEmpty(de.EditValue.ToString()))
                    { return;}

                if (string.IsNullOrEmpty(startDateDateEdit.EditValue.ToString()))
                    { return;}

                if (de.DateTime < startDateDateEdit.DateTime)
                {
                    dxErrorProvider1.SetError(de, "End Date cannot be less than Start Date.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }

        }

        private void ValidityTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || Convert.ToInt32(glue.EditValue) == 0))
            {
                //dxErrorProvider1.SetError(QualificationSubTypeStatusTypeGridLookUpEdit, "Select a value.");
                dxErrorProvider1.SetError(glue, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }

            dxErrorProvider1.SetError(glue, "");

            //Check if Null or Lifetime, in which case Expiry values are not relevant
            if (glue.EditValue == null)
            {
                if (this.strFormMode == "blockedit")
                {
                    ExpiryPeriodTypeGridLookUpEdit.EditValue = null;
                    expiryPeriodSpinEdit.EditValue = null;
                }
                else
                {
                    ExpiryPeriodTypeGridLookUpEdit.EditValue = 0;
                    expiryPeriodSpinEdit.EditValue = 0;
                }

                ExpiryPeriodTypeGridLookUpEdit.ReadOnly = true;
                expiryPeriodSpinEdit.ReadOnly = true;

                return;
            }

            if (!string.IsNullOrEmpty(glue.EditValue.ToString()) && Convert.ToInt32(glue.EditValue) == 1)
            {
                ExpiryPeriodTypeGridLookUpEdit.EditValue = 0;
                expiryPeriodSpinEdit.EditValue = 0;

                ExpiryPeriodTypeGridLookUpEdit.ReadOnly = true;
                expiryPeriodSpinEdit.ReadOnly = true;
            }
            else
            {

                ExpiryPeriodTypeGridLookUpEdit.ReadOnly = false;
                expiryPeriodSpinEdit.ReadOnly = false;
            }
        }
       
        private void ExpiryPeriodTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ( ( ( this.strFormMode == "blockedit" 
                        && ValidityTypeGridLookUpEdit.EditValue != null
                        && !string.IsNullOrEmpty(ValidityTypeGridLookUpEdit.EditValue.ToString())
                        && Convert.ToInt32(ValidityTypeGridLookUpEdit.EditValue) == 2 )
                || ( this.strFormMode != "blockedit" ) )
                && (string.IsNullOrEmpty(glue.EditValue.ToString()) || Convert.ToInt32(glue.EditValue) == 0))
            {
                //Validity Type is set to Expires
                if (ValidityTypeGridLookUpEdit.EditValue != null
                    && !string.IsNullOrEmpty(ValidityTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(ValidityTypeGridLookUpEdit.EditValue) == 2)
                {
                    dxErrorProvider1.SetError(glue, "Select a value.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }

            dxErrorProvider1.SetError(glue, "");
        }

        private void expiryPeriodSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if ( ( (this.strFormMode == "blockedit"
                    && ExpiryPeriodTypeGridLookUpEdit.EditValue != null
                    && !string.IsNullOrEmpty(ExpiryPeriodTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(ExpiryPeriodTypeGridLookUpEdit.EditValue) > 0 )
                || (this.strFormMode != "blockedit" ) ) 
                && (se.EditValue == null || string.IsNullOrEmpty(se.EditValue.ToString()) || Convert.ToInt32(se.EditValue) == 0))
            {
                if (ExpiryPeriodTypeGridLookUpEdit.EditValue != null 
                    && !string.IsNullOrEmpty(ExpiryPeriodTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(ExpiryPeriodTypeGridLookUpEdit.EditValue) > 0)
                {
                    dxErrorProvider1.SetError(se, "Select a value.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }
           
            dxErrorProvider1.SetError(se, "");
           
        }

        private void refreshTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || Convert.ToInt32(glue.EditValue) == 0))
            {
                dxErrorProvider1.SetError(glue, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }

            dxErrorProvider1.SetError(glue, "");

            //Check if Lifetime, in which case Expiry values are not relevant
            if (glue.EditValue == null)
            {
                if (this.strFormMode == "blockedit")
                {
                    RefreshIntervalTypeGridLookUpEdit.EditValue = null;
                    refreshIntervalSpinEdit.EditValue = null;
                }
                else
                {
                    RefreshIntervalTypeGridLookUpEdit.EditValue = 0;
                    refreshIntervalSpinEdit.EditValue = 0;
                }

                RefreshIntervalTypeGridLookUpEdit.ReadOnly = true;
                refreshIntervalSpinEdit.ReadOnly = true;

                return;
            }
            
            if (!string.IsNullOrEmpty(glue.EditValue.ToString()) && Convert.ToInt32(glue.EditValue) > 1)
            {
                RefreshIntervalTypeGridLookUpEdit.EditValue = 0;
                refreshIntervalSpinEdit.EditValue = 0;

                RefreshIntervalTypeGridLookUpEdit.ReadOnly = true;
                refreshIntervalSpinEdit.ReadOnly = true;
            }
            else
            {   
                RefreshIntervalTypeGridLookUpEdit.ReadOnly = false;
                refreshIntervalSpinEdit.ReadOnly = false;
            }
        }

        
        private void RefreshIntervalTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ( ( ( this.strFormMode == "blockedit" 
                        && refreshTypeGridLookUpEdit.EditValue != null
                        && !string.IsNullOrEmpty(refreshTypeGridLookUpEdit.EditValue.ToString())
                        && Convert.ToInt32(refreshTypeGridLookUpEdit.EditValue) == 1 )
                || (this.strFormMode != "blockedit" ) )
                && (string.IsNullOrEmpty(glue.EditValue.ToString()) || Convert.ToInt32(glue.EditValue) == 0))
            {
                //Refresh Required is set to Required
                if (refreshTypeGridLookUpEdit.EditValue != null
                    && !string.IsNullOrEmpty(refreshTypeGridLookUpEdit.EditValue.ToString()) 
                    && Convert.ToInt32( refreshTypeGridLookUpEdit.EditValue) == 1)
                {
                    dxErrorProvider1.SetError(glue, "Select a value.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }

            dxErrorProvider1.SetError(glue, "");
        }

        private void refreshIntervalSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if ( ( (this.strFormMode == "blockedit"
                    && RefreshIntervalTypeGridLookUpEdit.EditValue != null
                    && !string.IsNullOrEmpty(RefreshIntervalTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(RefreshIntervalTypeGridLookUpEdit.EditValue) > 0 )
                || ( this.strFormMode != "blockedit" ) )
                && (se.EditValue == null || string.IsNullOrEmpty(se.EditValue.ToString()) || Convert.ToInt32(se.EditValue) == 0))
            {
                if (RefreshIntervalTypeGridLookUpEdit.EditValue != null
                    && !string.IsNullOrEmpty(RefreshIntervalTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(RefreshIntervalTypeGridLookUpEdit.EditValue) > 0)
                {
                    dxErrorProvider1.SetError(se, "Select a value.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }

            dxErrorProvider1.SetError(se, "");
        }

        #endregion

    }
}


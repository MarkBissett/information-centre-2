using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Shares_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        public string strLinkedToRecordDesc = "";
        public string strLinkedToRecordDesc2 = "";
        public string strLinkedToRecordDesc3 = "";
        public string strLinkedToRecordDesc4 = "";
        public int intLinkedToRecordID = 0;

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        bool iBool_AllowAdd = true;
        bool iBool_AllowEdit = true;
        bool iBool_AllowDelete = true;

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        //private enmFocusedGrids _enmFocusedGrid = enmFocusedGrids.enmSanctionProgress;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private int ShowFinancialData = 1;  // Controls if the Contracts grid returns salary info //

        #endregion

        public frm_HR_Employee_Shares_Edit()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Shares_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500118;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;
            
            ShowFinancialData = (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey) ? 0 : 1);
            
            sp_HR_00228_Share_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00228_Share_Types_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00228_Share_Types_With_Blank);

            sp_HR_00229_Share_Unit_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00229_Share_Unit_Descriptors_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00229_Share_Unit_Descriptors_With_Blank);

            //sp_HR_00174_Employee_Pension_Contribution_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView2, "PensionContributionID");

            LoadLastSavedUserScreenSettings();  // Get any used sequence number prefix //

            // Populate Main Dataset //     
            
            sp_HR_00226_Employee_Shares_EditTableAdapter.Connection.ConnectionString = strConnectionString;

            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {                      
                        var drNewRow =(DataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_EditRow)dataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_Edit.NewRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordIds = strRecordIDs;
                        drNewRow.SharesID = 0;
                        drNewRow.EmployeeID = intLinkedToRecordID;
                        drNewRow.EmployeeName = strLinkedToRecordDesc;
                        drNewRow.EmployeeSurname = strLinkedToRecordDesc2;
                        drNewRow.EmployeeFirstname = strLinkedToRecordDesc3;
                        drNewRow.EmployeeNumber = strLinkedToRecordDesc4;
                        drNewRow.ShareTypeID = 0;
                        drNewRow.Active = 1;
                        drNewRow.ShareUnits = (decimal)10.00;
                        drNewRow.ShareUnitDescriptorID = 2;

                        SimplerAES Encryptor = new SimplerAES();
                        string strAmount = Encryptor.Encrypt("0.00");
                        drNewRow["UnitValue"] = strAmount;
                        UnitValueSpinEdit.EditValue = "0.00";

                        dataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_Edit.Rows.Add(drNewRow);
                        gridControl1.Enabled = false;

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        var drNewRow = (DataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_EditRow)dataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_Edit.Newsp_HR_00226_Employee_Shares_EditRow();
                        drNewRow.Mode = "blockedit";
                        drNewRow.RecordIds = strRecordIDs;
                        dataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_Edit.Addsp_HR_00226_Employee_Shares_EditRow(drNewRow);
                        drNewRow.AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_HR_00226_Employee_Shares_EditTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_Edit, strFormMode, strRecordIDs, ShowFinancialData);
                    }
                    catch (Exception)
                    {
                    }                 
                    if (strFormMode == "view")
                    {
                        iBool_AllowAdd = false;
                        iBool_AllowEdit = false;
                        iBool_AllowDelete = false;
                    }
                    break;
            }

            if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit")) LoadLinkedRecords();

            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Employee Shares", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "")
            {
                i_str_AddedRecordIDs1 = strNewIDs1;
                //iBool_AllowAdd = false;     //Only one holiday record ever allowed for the current year.
            }
            if (strFormMode == "add" || strFormMode == "edit")
            {
                LoadLinkedRecords();
                FilterGrids();
            }
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ShareTypeIDGridLookUpEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = false;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ShareTypeIDGridLookUpEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = true;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        ShareTypeIDGridLookUpEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = false;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        ShareTypeIDGridLookUpEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = true;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DefaultBoolean.True;
            }
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
            ShareTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            ShareTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            ShareTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            ShareTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9203", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9203:  // Share Types //    
                        {
                            ShareTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            ShareTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            ShareTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            ShareTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                }
            }
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                btnSave.Enabled = bbiSave.Enabled;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                btnSave.Enabled = bbiSave.Enabled;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            if (!iBool_AllowAdd || strFormMode == "blockedit")
            {
                gridControl1.Enabled = false;
            }

            GridView view = null;
            int[] intRowHandles;
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["SharesID"] == null ? 0 : Convert.ToInt32(currentRow["SharesID"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of Linked Documents button //
/*
            if (i_int_FocusedGrid == 1)  // Contributions //
            {
                view = (GridView)gridControl1.MainView;
                intRowHandles = view.GetSelectedRows();
                if (iBool_AllowAdd && strFormMode != "blockedit" && intID > 0)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && strFormMode != "blockedit" && intRowHandles.Length > 0 && intID > 0)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 1) bbiBlockEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }*/

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

/*            // Set enabled status of GridView1 navigator custom buttons //
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd && intID > 0 && strFormMode != "blockedit");
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 && strFormMode != "blockedit");
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
 */
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = false;

        }


        private void frm_HR_Employee_Shares_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Employee_Shares_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;

            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
            }

            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (this.strFormMode == "blockadd" || this.strFormMode == "blockedit") return;

            // Actions //
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
            gridControl1.BeginUpdate();
   //         sp_HR_00174_Employee_Pension_Contribution_ListTableAdapter.Fill(dataSet_HR_Core.sp_HR_00174_Employee_Pension_Contribution_List, strRecordIDs, ShowFinancialData);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            
            // Decrypt all Financial Data //
            SimplerAES Encryptor = new SimplerAES();
            string strAmount = "";
            try
            {
                foreach (DataRow dr in dataSet_HR_Core.sp_HR_00174_Employee_Pension_Contribution_List.Rows)
                {
                    strAmount = dr["EmployeeContributionAmount"].ToString();
                    if (!string.IsNullOrEmpty(strAmount)) dr["EmployeeContributionAmount"] = Encryptor.Decrypt(strAmount);
                    strAmount = dr["EmployerContributionAmount"].ToString();
                    if (!string.IsNullOrEmpty(strAmount)) dr["EmployerContributionAmount"] = Encryptor.Decrypt(strAmount);
                }
            }
            catch (Exception ex) { }
            
            gridControl1.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PensionContributionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.spHR00226EmployeeSharesEditBindingSource.EndEdit();
            try
            {
                this.sp_HR_00226_Employee_Shares_EditTableAdapter.Update(dataSet_HR_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }
            if (strFormMode == "add" || strFormMode == "edit")
            {
                gridControl1.Enabled = true;  // Child Grid now enabled //
            }

            GridView view = (GridView)gridControl1.MainView;
            view.PostEditor();

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["SharesID"]) + ";";
                    strRecordIDs = Convert.ToInt32(currentRow["SharesID"]) + ",";  // This is required so the linked child records can be loaded into the grid //
                }
                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["Mode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
                var currentRow = (DataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_EditRow)currentRowView.Row;
                if (currentRow != null)
                {
                    currentRow["Mode"] = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["RecordIds"]);  // Values returned from Update SP //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        var fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(2, Utils.enmMainGrids.Shares, strNewIDs);
                    }
                    else if (frmChild.Name == "frm_HR_Employee_Edit")
                    {
                        var fParentForm = (frm_HR_Employee_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(16, Utils.enmMainGrids.Shares, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0) //|| intAccessIssuesNew > 0 || intAccessIssuesModified > 0 || intAccessIssuesDeleted > 0 || intEnvironmentalIssuesNew > 0 || intEnvironmentalIssuesModified > 0 || intEnvironmentalIssuesDeleted > 0 || intSiteHazardsNew > 0 || intSiteHazardsModified > 0 || intSiteHazardsDeleted > 0 || intElecticalHazardsNew > 0 || intElecticalHazardsModified > 0 || intElecticalHazardsDeleted > 0
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                if (!(this.strFormMode == "blockadd" || this.strFormMode == "blockedit"))
                {
                    Decrypt_Financial_Fields();
                    FilterGrids();
                    GridView view = (GridView)gridControl1.MainView;
                    view.ExpandAllGroups();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region GridControl1

        private void gridView2_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            string message = (strFormMode == "blockedit" || strFormMode == "blockadd" ? "Linked Shares Taken NOT Shown When Block Adding and Block Editing" : "No Linked Shares Taken Available - Click the Add button to Create Shares Taken");
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void gridView2_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView2_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;

            SetMenuStatus();
        }

        private void gridView2_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView2_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView2_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            gridControl1.Focus();
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinkedRecords(); 
                        FilterGrids();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Editors

        private void EmployeeNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
                var currentRow = (DataSet_HR_DataEntry.sp_HR_00040_Employee_Bonus_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow.EmployeeID.ToString()) ? 0 : Convert.ToInt32(currentRow.EmployeeID));
                var fChildForm = new frm_HR_Select_Employee();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = intEmployeeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.EmployeeID = fChildForm.intSelectedEmployeeID;
                    currentRow.EmployeeName = fChildForm.strSelectedEmployeeName;
                    currentRow.EmployeeSurname = fChildForm.strSelectedSurname;
                    currentRow.EmployeeFirstname = fChildForm.strSelectedForename;
                    currentRow.EmployeeNumber = fChildForm.strSelectedEmployeeNumber;

                    spHR00226EmployeeSharesEditBindingSource.EndEdit();
                }
            }
        }
        private void EmployeeNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if ((this.strFormMode == "edit" || this.strFormMode == "add") && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(EmployeeNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(EmployeeNameButtonEdit, "");
            }
        }

        private void ShareTypeIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 9203, "HR Share Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00228_Share_Types_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00228_Share_Types_With_Blank);
                }
            }
        }
        private void ShareTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(ShareTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ShareTypeIDGridLookUpEdit, "");
            }
        }

        private void DateReceivedDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit dt = (DateEdit)sender;
            if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;
            if (string.IsNullOrEmpty(dt.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DateReceivedDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DateReceivedDateEdit, "");
            }
        }

        private void UnitValueSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
            if (currentRow == null) return;
            string strValue = UnitValueSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["UnitValue"].ToString() != strValue) currentRow["UnitValue"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Unit Value data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

         #endregion
        

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit_Record();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }


        private void Add_Record()
        {
           /* if (!(strFormMode == "add" || strFormMode == "edit")) return;
            DataRowView currentRowView = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_EditRow)currentRowView.Row;
            if (currentRow == null) return;
            int intSharesID = currentRow.SharesID;
            string strShareDescription = (string.IsNullOrWhiteSpace(currentRow.EmployeeName) ? "" : currentRow.EmployeeName) + " - " + Convert.ToDateTime(currentRow["DateReceived"]).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            if (intSharesID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to Add Vetting Restriction - Save changes to this Vetting record first then try again.", "Add Vetting Restriction", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            switch (i_int_FocusedGrid)
            {
                case 1:  // Contributions //
                    {
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pension_Contribution_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intLinkedToRecordID = intSharesID;
                        fChildForm.strLinkedToRecordDesc = strShareDescription;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        // Invoke Post Open event on Base form //
                         System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }*/
        }

        private void Edit_Record()
        {
            /*if (!(strFormMode == "add" || strFormMode == "edit")) return;
            var currentRowView = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
            if (currentRowView == null) return;

            var SharesID = 0;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_EditRow)currentRowView.Row;
            SharesID = currentRow.SharesID;

            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        var view = (GridView)gridControl1.MainView;
                        var intRowHandles = view.GetSelectedRows();
                        var intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Vetting Restriction records to edit by clicking on them then try again.", "Edit Linked Vetting Restriction Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var recordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            recordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionContributionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Pension_Contribution_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = recordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        // Invoke Post Open event on Base form //
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }*/
        }

        private void Block_Edit_Record()
        {
            /*if (!(strFormMode == "add" || strFormMode == "edit")) return;
            var currentRowView = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
            if (currentRowView == null) return;

            var SharesID = 0;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_EditRow)currentRowView.Row;
            SharesID = currentRow.SharesID;

            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        var view = (GridView)gridControl1.MainView;
                        var intRowHandles = view.GetSelectedRows();
                        var intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Pension Contribution records to block edit by clicking on them then try again.", "Block Edit Linked Pension Contribution Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var recordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            recordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionContributionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Pension_Contribution_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = recordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        // Invoke Post Open event on Base form //
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }*/
        }

        private void View_Record()
        {
            /*var currentRowView = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
            if (currentRowView == null) return;

            var SharesID = 0;
            var currentRow = (DataSet_HR_DataEntry.sp_HR_00226_Employee_Shares_EditRow)currentRowView.Row;
            SharesID = currentRow.SharesID;

            switch (i_int_FocusedGrid)
            {
                case 1:  // Restriction //
                    {
                        var view = (GridView)gridControl1.MainView;
                        var intRowHandles = view.GetSelectedRows();
                        var intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Pension Contribution records to view by clicking on them then try again.", "View Linked Pension Contribution Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var recordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            recordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionContributionID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Pension_Contribution_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = recordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        // Invoke Post Open event on Base form //
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }*/
        }

        private void Delete_Record()
        {
            /*int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:  // Restriction //
                    {
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Pension Contribution to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Pension Contribution" : Convert.ToString(intRowHandles.Length) + " Pension Contributions") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Pension Contribution" : "these Pension Contributions") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PensionContributionID")) + ",";
                            }

                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter();
                            RemoveRecords.ChangeConnectionString(strConnectionString);
                            RemoveRecords.sp09000_HR_Delete("employee_pension_contribution", strRecordIDs);  // Remove the records from the DB in one go //
                            LoadLinkedRecords();
                            FilterGrids();

                            RemoveRecords.Dispose();

                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }*/
        }

        private void FilterGrids()
        {
            string strID = "";
            DataRowView currentRow = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
            if (currentRow != null)
            {
                strID = (currentRow["SharesID"] == null ? "0" : currentRow["SharesID"].ToString());
            }

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = "[SharesID] = " + Convert.ToString(strID);
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
        }

        private void Decrypt_Financial_Fields()
        {
            if (!string.IsNullOrEmpty(GlobalSettings.HRSecurityKey))  // Decrypt financial Info //
            {
                DataRowView currentRow = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
                if (currentRow == null) return;

                SimplerAES Encryptor = new SimplerAES();
                try
                {
                    string strValue = currentRow["UnitValue"].ToString();
                    UnitValueSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");
                }
                catch (Exception ex)
                {
                    UnitValueSpinEdit.EditValue = "0.00";
                }
            }
            else  // Don't display any financial info //
            {
                UnitValueSpinEdit.EditValue = "0.00";
            }
        }

        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)spHR00226EmployeeSharesEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["SharesID"].ToString()) ? 0 : Convert.ToInt32(currentRow["SharesID"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 12;  // Shares //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = currentRow["EmployeeName"].ToString() + " - " + Convert.ToDateTime(currentRow["DateReceived"]).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }




 
 


    }


}


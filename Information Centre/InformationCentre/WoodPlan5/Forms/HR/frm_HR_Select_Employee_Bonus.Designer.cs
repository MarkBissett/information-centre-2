namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Bonus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00190SelectBonusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBonusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonusDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGuaranteeBonusAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBonusDatePaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBonusAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00190_Select_BonusTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00190_Select_BonusTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00190SelectBonusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00190SelectBonusBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditNumericCurrency,
            this.repositoryItemMemoExEdit2});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00190SelectBonusBindingSource
            // 
            this.spHR00190SelectBonusBindingSource.DataMember = "sp_HR_00190_Select_Bonus";
            this.spHR00190SelectBonusBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBonusID,
            this.colEmployeeID3,
            this.colGuaranteeBonus,
            this.colGuaranteeBonusDueDate,
            this.colGuaranteeBonusAmount,
            this.colBonusDatePaid,
            this.colBonusAmountPaid,
            this.colRemarks6,
            this.colEmployeeName4,
            this.colEmployeeSurname,
            this.colEmployeeFirstname3,
            this.colEmployeeNumber2,
            this.colRecordStatus});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.BestFitMaxRowCount = 5;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeFirstname3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGuaranteeBonusDueDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colBonusID
            // 
            this.colBonusID.Caption = "Bonus ID";
            this.colBonusID.FieldName = "BonusID";
            this.colBonusID.Name = "colBonusID";
            this.colBonusID.OptionsColumn.AllowEdit = false;
            this.colBonusID.OptionsColumn.AllowFocus = false;
            this.colBonusID.OptionsColumn.ReadOnly = true;
            this.colBonusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEmployeeID3
            // 
            this.colEmployeeID3.Caption = "Employee ID";
            this.colEmployeeID3.FieldName = "EmployeeID";
            this.colEmployeeID3.Name = "colEmployeeID3";
            this.colEmployeeID3.OptionsColumn.AllowEdit = false;
            this.colEmployeeID3.OptionsColumn.AllowFocus = false;
            this.colEmployeeID3.OptionsColumn.ReadOnly = true;
            this.colEmployeeID3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID3.Width = 81;
            // 
            // colGuaranteeBonus
            // 
            this.colGuaranteeBonus.Caption = "Guarantee Bonus";
            this.colGuaranteeBonus.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGuaranteeBonus.FieldName = "GuaranteeBonus";
            this.colGuaranteeBonus.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colGuaranteeBonus.Name = "colGuaranteeBonus";
            this.colGuaranteeBonus.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonus.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonus.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colGuaranteeBonus.Visible = true;
            this.colGuaranteeBonus.VisibleIndex = 3;
            this.colGuaranteeBonus.Width = 104;
            // 
            // colGuaranteeBonusDueDate
            // 
            this.colGuaranteeBonusDueDate.Caption = "Bonus Due Date";
            this.colGuaranteeBonusDueDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colGuaranteeBonusDueDate.FieldName = "BonusDueDate";
            this.colGuaranteeBonusDueDate.Name = "colGuaranteeBonusDueDate";
            this.colGuaranteeBonusDueDate.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonusDueDate.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonusDueDate.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonusDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGuaranteeBonusDueDate.Visible = true;
            this.colGuaranteeBonusDueDate.VisibleIndex = 2;
            this.colGuaranteeBonusDueDate.Width = 126;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colGuaranteeBonusAmount
            // 
            this.colGuaranteeBonusAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colGuaranteeBonusAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colGuaranteeBonusAmount.Caption = "Guarantee Bonus Amount";
            this.colGuaranteeBonusAmount.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGuaranteeBonusAmount.FieldName = "GuaranteeBonusAmount";
            this.colGuaranteeBonusAmount.Name = "colGuaranteeBonusAmount";
            this.colGuaranteeBonusAmount.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonusAmount.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonusAmount.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonusAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colGuaranteeBonusAmount.Visible = true;
            this.colGuaranteeBonusAmount.VisibleIndex = 4;
            this.colGuaranteeBonusAmount.Width = 144;
            // 
            // colBonusDatePaid
            // 
            this.colBonusDatePaid.Caption = "Date Paid";
            this.colBonusDatePaid.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colBonusDatePaid.FieldName = "BonusDatePaid";
            this.colBonusDatePaid.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colBonusDatePaid.Name = "colBonusDatePaid";
            this.colBonusDatePaid.OptionsColumn.AllowEdit = false;
            this.colBonusDatePaid.OptionsColumn.AllowFocus = false;
            this.colBonusDatePaid.OptionsColumn.ReadOnly = true;
            this.colBonusDatePaid.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colBonusDatePaid.Visible = true;
            this.colBonusDatePaid.VisibleIndex = 5;
            // 
            // colBonusAmountPaid
            // 
            this.colBonusAmountPaid.AppearanceCell.Options.UseTextOptions = true;
            this.colBonusAmountPaid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colBonusAmountPaid.Caption = "Amount Paid";
            this.colBonusAmountPaid.ColumnEdit = this.repositoryItemTextEditNumericCurrency;
            this.colBonusAmountPaid.FieldName = "BonusAmountPaid";
            this.colBonusAmountPaid.Name = "colBonusAmountPaid";
            this.colBonusAmountPaid.OptionsColumn.AllowEdit = false;
            this.colBonusAmountPaid.OptionsColumn.AllowFocus = false;
            this.colBonusAmountPaid.OptionsColumn.ReadOnly = true;
            this.colBonusAmountPaid.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBonusAmountPaid.Visible = true;
            this.colBonusAmountPaid.VisibleIndex = 6;
            this.colBonusAmountPaid.Width = 81;
            // 
            // repositoryItemTextEditNumericCurrency
            // 
            this.repositoryItemTextEditNumericCurrency.AutoHeight = false;
            this.repositoryItemTextEditNumericCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditNumericCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericCurrency.Name = "repositoryItemTextEditNumericCurrency";
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.OptionsColumn.ReadOnly = true;
            this.colRemarks6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colEmployeeName4
            // 
            this.colEmployeeName4.Caption = "Linked To Employee";
            this.colEmployeeName4.FieldName = "EmployeeName";
            this.colEmployeeName4.Name = "colEmployeeName4";
            this.colEmployeeName4.OptionsColumn.AllowEdit = false;
            this.colEmployeeName4.OptionsColumn.AllowFocus = false;
            this.colEmployeeName4.OptionsColumn.ReadOnly = true;
            this.colEmployeeName4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName4.Width = 227;
            // 
            // colEmployeeSurname
            // 
            this.colEmployeeSurname.Caption = "Employee Surname";
            this.colEmployeeSurname.FieldName = "EmployeeSurname";
            this.colEmployeeSurname.Name = "colEmployeeSurname";
            this.colEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname.Visible = true;
            this.colEmployeeSurname.VisibleIndex = 0;
            this.colEmployeeSurname.Width = 125;
            // 
            // colEmployeeFirstname3
            // 
            this.colEmployeeFirstname3.Caption = "Employee Forename";
            this.colEmployeeFirstname3.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname3.Name = "colEmployeeFirstname3";
            this.colEmployeeFirstname3.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname3.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname3.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname3.Visible = true;
            this.colEmployeeFirstname3.VisibleIndex = 1;
            this.colEmployeeFirstname3.Width = 131;
            // 
            // colEmployeeNumber2
            // 
            this.colEmployeeNumber2.Caption = "Employee #";
            this.colEmployeeNumber2.FieldName = "EmployeeNumber";
            this.colEmployeeNumber2.Name = "colEmployeeNumber2";
            this.colEmployeeNumber2.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber2.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber2.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber2.Width = 78;
            // 
            // colRecordStatus
            // 
            this.colRecordStatus.Caption = "Record Status";
            this.colRecordStatus.FieldName = "RecordStatus";
            this.colRecordStatus.Name = "colRecordStatus";
            this.colRecordStatus.OptionsColumn.AllowEdit = false;
            this.colRecordStatus.OptionsColumn.AllowFocus = false;
            this.colRecordStatus.OptionsColumn.ReadOnly = true;
            this.colRecordStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRecordStatus.Width = 89;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00190_Select_BonusTableAdapter
            // 
            this.sp_HR_00190_Select_BonusTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_Bonus
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Bonus";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Bonus";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Bonus_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00190SelectBonusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource spHR00190SelectBonusBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00190_Select_BonusTableAdapter sp_HR_00190_Select_BonusTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID3;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonus;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonusDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonusAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusDatePaid;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname3;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordStatus;
    }
}

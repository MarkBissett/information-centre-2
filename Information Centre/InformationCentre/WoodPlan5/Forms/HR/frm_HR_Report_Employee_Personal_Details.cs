﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //
using System.Reflection;  // Required by GridViewFiltering //

using DevExpress.XtraReports;
using DevExpress.XtraReports.UserDesigner;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Menu;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;
using System.ComponentModel.Design;  // Used by process to auto link event for custom sorting when a object is dropped onto the design panel of the report //
using DevExpress.XtraReports.Design;
using DevExpress.XtraReports.UI;

using WoodPlan5.Properties;
using WoodPlan5.Reports;
using BaseObjects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_HR_Report_Employee_Personal_Details : BaseObjects.frmBase
    {

        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_AllowDelete = false;

        WaitDialogForm loadingForm;
        private int i_int_SelectedLayoutID = 0;

        public string i_str_SavedDirectoryName = "";

        private string i_str_selected_department_ids = "";
        private string i_str_selected_departments = "";
        private string i_str_selected_employee_ids = "";
        private string i_str_selected_employees = "";

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        rpt_HR_Employee_Listing rptReport;
        
        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //
        bool iBool_EditLayoutEnabled = false;
       
        private DataSet_Selection DS_Selection1;

        #endregion

        public frm_HR_Report_Employee_Personal_Details()
        {
            InitializeComponent();
        }

        private void frm_HR_Report_Employee_Personal_Details_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 99002;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = this.GlobalSettings.ConnectionString;
           
            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridView1, strConnectionString);  // Used by DataSets //

            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp_HR_00114_Department_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00114_Department_Filter_ListTableAdapter.Fill(dataSet_HR_Core.sp_HR_00114_Department_Filter_List, "");
            gridControl2.ForceInitialize();

            sp_HR_00239_Reports_Employee_ListTableAdapter.Connection.ConnectionString = strConnectionString;
           
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.Fixed = FixedStyle.Left;
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
                     
            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;
            gridControl2.ForceInitialize();

            dateEditReturnDate.DateTime = DateTime.Today.AddMonths(1);

            RefreshGridViewState1 = new RefreshGridState(gridView1, "EmployeeID");
            emptyEditor = new RepositoryItem();

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                i_str_SavedDirectoryName = GetSetting.sp00043_RetrieveSingleSystemSetting(9, "HRReportLayouts").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for the Client Job Breakdown Layouts (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Report Layouts Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            if (!i_str_SavedDirectoryName.EndsWith("\\")) i_str_SavedDirectoryName += "\\";  // Add Backslash to end //
            
            popupContainerControlDepartments.Size = new System.Drawing.Size(312, 423);

        }

        private void frm_HR_Report_Employee_Personal_Details_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Report_Employee_Personal_Details_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "DepartmentFilter", i_str_selected_department_ids);
                default_screen_settings.SaveDefaultScreenSettings();
            }
         }

        public override void PostOpen(object objParameter)
        {
            dockPanel1.Width = 450;

            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            Application.DoEvents();  // Allow Form time to repaint itself //
            LoadLastSavedUserScreenSettings();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                            iBool_EditLayoutEnabled = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            // Code in Grid's MouseDown event to handle enabling/disabling Dataset Menu items //
            if (iBool_AllowAdd)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
            }
            else
            {
                bsiAdd.Enabled = false;
                bbiSingleAdd.Enabled = false;
            }
            if (iBool_AllowEdit)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                bbiEditReportLayout.Enabled = true;
            }
            else
            {
                bsiEdit.Enabled = false;
                bbiSingleEdit.Enabled = false;
                bbiEditReportLayout.Enabled = true;
            }
            if (iBool_AllowDelete)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
                iBool_AllowDelete = true;
            }
            else
            {
                bbiDelete.Enabled = false;
                iBool_AllowDelete = false;
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
            
            // Set Enabled Status of Main Toolbar Buttons //
            bbiEditReportLayout.Enabled = iBool_EditLayoutEnabled;
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Gritting Callout Filter //
                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("DepartmentFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array array = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl2.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in array)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["DepartmentID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    beiDepartment.EditValue = PopupContainerEdit2_Get_Selected();
                }
                bbiReloadData.PerformClick();
            }
        }

        private void Load_Data()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_department_ids == null) i_str_selected_department_ids = "";
            int intActive = Convert.ToInt32(beiActive.EditValue);
            GridView view = (GridView)gridControl1.MainView;
            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
            view.BeginUpdate();
            sp_HR_00239_Reports_Employee_ListTableAdapter.Fill(this.dataSet_HR_Reporting.sp_HR_00239_Reports_Employee_List, intActive, i_str_selected_department_ids);
            this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.EndUpdate();

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Employees Available - Click Reload Data");
        }

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    //Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            //i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetSelectionInverted.Enabled = true;
                bbiDatasetManager.Enabled = true;

                popupMenuGridControl1.ShowPopup(new Point(MousePosition.X, MousePosition.Y));         
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)gridControl2.MainView;
            view.ExpandAllGroups();
            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "SubContractorName")
            {
                switch (view.GetRowCellValue(e.RowHandle, "SubContractorName").ToString())
                {
                    case "":
                        //e.Appearance.BackColor = Color.LightCoral;
                        //e.Appearance.BackColor2 = Color.Red;
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }*/
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            /*
            if (e.RowHandle < 0) return; 
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "GrittingCalloutCount":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "GrittingCalloutCount").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                case "SnowClearanceCallOutCount":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "SnowClearanceCallOutCount").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }*/
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            /*// Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "GrittingCalloutCount":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("GrittingCalloutCount").ToString())) e.Cancel = true;
                    break;
                case "SnowClearanceCallOutCount":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("SnowClearanceCallOutCount").ToString())) e.Cancel = true;
                    break;
                default:
                    break;
            }*/
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strEmployeeIDs = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeIDs").ToString() + ",";
            DataSet_GC_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MinValue;
            string strRecordIDs = GetSetting.sp04077_GC_DrillDown_Get_Linked_Callouts(strEmployeeIDs, "site_report_completed", "", FromDate, ToDate).ToString();
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "site_gritting_callouts");
        }

        private void repositoryItemHyperLinkEdit2_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strEmployeeIDs = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeIDs").ToString() + ",";
            DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Snow_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            DateTime FromDate = DateTime.MinValue;
            DateTime ToDate = DateTime.MinValue;
            string strRecordIDs = GetSetting.sp04196_GC_Snow_DrillDown_Get_Linked_Callouts(strEmployeeIDs, "site_report_completed", "", FromDate, ToDate).ToString();
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "site_snow_clearance_callouts");
        }

        #endregion


        #region Department Filter Panel

        private void btnDepartmentFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditDepartment_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_department_ids = "";    // Reset any prior values first //
            i_str_selected_departments = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_department_ids = "";

                return "No Department Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_department_ids = "";

                return "No Department Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_department_ids += Convert.ToString(view.GetRowCellValue(i, "DepartmentID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_departments = Convert.ToString(view.GetRowCellValue(i, "DepartmentName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_departments += ", " + Convert.ToString(view.GetRowCellValue(i, "DepartmentName"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_departments;
        }

        #endregion


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = ",";
            string strColumnName = "";
            view = (GridView)gridControl1.MainView;
            strColumnName = "EmployeeId";
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strCurrentID = view.GetRowCellValue(intRowHandle, view.Columns[strColumnName]).ToString() + ',';
                if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                {
                    strSelectedIDs += strCurrentID;
                    intCount++;
                }
            }
            strSelectedIDs = strSelectedIDs.Remove(0, 1);  // Remove leading ',' //
            CreateDataset("Employee", intCount, strSelectedIDs);
        }

        private void CreateDataset(string strType, int intRecordCount, string strSelectedRecordIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_employee_count = intRecordCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_types = "Employee,"; // Comma seperated list needs to be passed //

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                return;
            }
            intSelectedDataset = fChildForm.i_int_selected_dataset;
            DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "EmployeeId");
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            frmMain.AddOwnedForm(fChildForm);
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_types = "Employee,"; // Comma seperated list needs to be passed //

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                return;
            }
            intSelectedDataset = fChildForm.i_int_selected_dataset;
            DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "EmployeeId");
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
            var fChildForm = new frm_DatasetManager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._i_PassedInFilterTypes = "Employee,";
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        #endregion

        private void bciFilterSelected_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (bciFilterSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more records to filter by before proceeding.", "Filter Selected Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControl1.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
            }
            else  // Clear Filter //
            {
                try
                {
                    gridControl1.BeginUpdate();
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00173_EmployeeBonus_Manager.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControl1.EndUpdate();
        }

        private void bbiReloadData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        private void bbiTick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);
                }
                view.EndUpdate();
            }
        }

        private void bbiUntick_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length > 0)
            {
                view.BeginUpdate();
                foreach (int intRowHandle in intRowHandles)
                {
                    view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 0);
                }
                view.EndUpdate();
            }
        }

        private void bbiPreviewReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (selection1.SelectedCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Employees to Preview before proceeding.", "Preview Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strEmployeeIDs = "";
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    strEmployeeIDs += Convert.ToString(view.GetRowCellValue(i, "EmployeeId")) + ",";
                 }
            }
            DateTime? dtFromDate = dateEditReturnDate.DateTime;

            string strReportFileName = "EmployeePersonalDetailsVerification.repx";

            rpt_HR_Employee_Listing rptReport = new rpt_HR_Employee_Listing(this.GlobalSettings, strEmployeeIDs, dtFromDate);
            /*try
            {
                rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                rptReport.ShowRibbonPreview();
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }*/

            loadingForm = new WaitDialogForm("Loading Report...", "Reporting");
            loadingForm.Show();
            try
            {
                rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
            }
            catch (Exception Ex)
            {
                loadingForm.Close();
                Console.WriteLine(Ex.Message);
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to view report, the layout may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "View Report", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            printControl1.PrintingSystem = rptReport.PrintingSystem;
            printingSystem1.Begin();
            rptReport.CreateDocument();
            printingSystem1.End();
            loadingForm.Close();
        }

        private void bbiEditReportLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string strReportFileName = "EmployeePersonalDetailsVerification.repx";

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to edit the selected layout?", "Edit Report Layout", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                string strEmployeeIDs = "";
                GridView view = (GridView)gridControl1.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strEmployeeIDs += Convert.ToString(view.GetRowCellValue(i, "EmployeeId")) + ",";
                    }
                }
                DateTime? dtFromDate = dateEditReturnDate.DateTime;
                rpt_HR_Employee_Listing rptReport = new rpt_HR_Employee_Listing(this.GlobalSettings, strEmployeeIDs, dtFromDate);
                try
                {
                    rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);
                }
                catch (Exception Ex)
                {
                    Console.WriteLine(Ex.Message);
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to load layout, it may no longer exist or you may not have the necessary permissions to access it - contact Technical Support.", "Load Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                loadingForm = new WaitDialogForm("Loading Report Builder...", "Reporting");
                loadingForm.Show();

                // Open the report in the Report Builder - Create a design form and get its panel //
                XRDesignRibbonFormEx form = new XRDesignRibbonFormEx();  // Create an End-User Designer Form with a Ribbon //
                //XRDesignFormEx form = new XRDesignFormEx();
                XRDesignPanel panel = form.DesignPanel;
                panel.SetCommandVisibility(ReportCommand.NewReport, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.OpenFile, CommandVisibility.None);
                panel.SetCommandVisibility(ReportCommand.SaveFileAs, CommandVisibility.None);

                // Add a new command handler to the Report Designer which saves the report in a custom way.
                panel.AddCommandHandler(new SaveCommandHandler(panel, i_str_SavedDirectoryName, strReportFileName));

                // Add a new command handler to the report panel to fire code on adding a new object to the report (Not currently used but could be used to preset values on new object for user) //
                panel.ComponentAdded += new ComponentEventHandler(panel_ComponentAdded);

                panel.OpenReport(rptReport);
                form.Shown += new EventHandler(ReportBuilder_Shown);  // Fires event after report builder is shown //
                form.WindowState = FormWindowState.Maximized;
                loadingForm.Close();
                form.ShowDialog();
                panel.CloseReport();
            }
        }

        private void bbiSendReport_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            /*if (selection1.SelectedCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Clients \\ Sites to Send before proceeding.", "Send Report", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            bool boolNoInternet = BaseObjects.PingTest.Ping("www.google.com");
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.microsoft.com");  // try another site just in case that one is down //
            if (!boolNoInternet) boolNoInternet = BaseObjects.PingTest.Ping("www.yahoo.com");  // try another site just in case that one is down //
            if (!boolNoInternet) // alert user and halt process //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to connect to the internet - unable to send reports.\n\nPlease check your internet connection then try again.\n\nIf the problem persists, contact Technical Support.", "Check Internet Connection", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            #region GetEmailSettings
            // Get DB Settings for generated PDF Client Reports //
            string strEmailBodyFile = "";
            string strEmailFrom = "";
            string strEmailSubjectLine = "";
            string strCCToEmailAddress = "";
            string strPDFFolderPath = "";
            string strSMTPMailServerAddress = "";
            string strSMTPMailServerUsername = "";
            string strSMTPMailServerPassword = "";
            string strSMTPMailServerPort = "";
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            cmd = new SqlCommand("sp04244_GC_Report_Get_Email_Settings", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sdaSettings = new SqlDataAdapter(cmd);
            DataSet dsSettings = new DataSet("NewDataSet");
            try
            {
                sdaSettings.Fill(dsSettings, "Table");
            }
            catch (Exception ex)
            {
                 DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Client Report Folder and Email Settings' (from the System Configuration Screen) [" + ex.Message + "].\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dsSettings.Tables[0].Rows.Count != 1)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the 'Client Report Folder and Email Settings' (from the System Configuration Screen) - number of rows returned not equal to 1.\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DataRow dr1 = dsSettings.Tables[0].Rows[0];
            strEmailBodyFile = dr1["BodyFileName"].ToString();
            strEmailFrom = dr1["EmailFrom"].ToString();
            strEmailSubjectLine = dr1["SubjectLine"].ToString();
            strCCToEmailAddress = dr1["CCToName"].ToString();
            strPDFFolderPath = dr1["PDFFolder"].ToString();
            strSMTPMailServerAddress = dr1["SMTPMailServerAddress"].ToString();
            strSMTPMailServerUsername = dr1["SMTPMailServerUsername"].ToString();
            strSMTPMailServerPassword = dr1["SMTPMailServerPassword"].ToString();
            strSMTPMailServerPort = dr1["SMTPMailServerPort"].ToString();
            if (string.IsNullOrEmpty(strSMTPMailServerPort) || !CheckingFunctions.IsNumeric(strSMTPMailServerPort)) strSMTPMailServerPort = "0";
            int intSMTPMailServerPort = Convert.ToInt32(strSMTPMailServerPort);
            
            if (string.IsNullOrEmpty(strEmailBodyFile) || string.IsNullOrEmpty(strEmailFrom) || string.IsNullOrEmpty(strPDFFolderPath) || string.IsNullOrEmpty(strSMTPMailServerAddress))
            {
                 DevExpress.XtraEditors.XtraMessageBox.Show("One or more of the folder and email settings (Email Layout File, From Email Address, PDF File Folder and SMTP Mail Server Name) are missing from the System Configuration Screen.\n\nPlease update the System Settings then try again. If the problem persists, contact Technical Support.", "Get Folder and Email Settings", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (!strPDFFolderPath.EndsWith("\\")) strPDFFolderPath += "\\";  // Add Backslash to end //

            #endregion

            // If at this point, we are good to generate self-billing invoices so get user confirmation //
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to email Client Reports to Clients\n\nProceed?", "Send Client Reports", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

            DateTime? dtFromDate = DateTime.MinValue;
            DateTime? dtToDate = DateTime.MinValue;
            
 
            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Creating Reports...");
            fProgress.Show();
            System.Windows.Forms.Application.DoEvents();
            int intUpdateProgressThreshhold = 0;  // Value set later //
            int intUpdateProgressTempCount = 0;

            // First get unique list of clients to send reports to //
            string strClients = ",";
            GridView view = (GridView)gridControl1.MainView;
            for (int i = 0; i < view.DataRowCount; i++)
            {
                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                {
                    if (!strClients.Contains(',' + view.GetRowCellValue(i, "ClientID").ToString() + ',')) strClients += view.GetRowCellValue(i, "ClientID").ToString() + ',';
                }
            }
            strClients = strClients.Remove(0, 1);  // Remove dummy ',' at start //

            char[] delimiters = new char[] { ',' };
            string[] strArray = strClients.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            intUpdateProgressThreshhold = strArray.Length / 10;
            intUpdateProgressTempCount = 0;
            string strClientIDsWitMissingEmailAddresses = "";

            DataSet_GC_ReportsTableAdapters.QueriesTableAdapter StoreSentReport = new DataSet_GC_ReportsTableAdapters.QueriesTableAdapter();
            StoreSentReport.ChangeConnectionString(strConnectionString);
 
            if (strArray.Length > 0)
            {
                DataSet_GC_ReportsTableAdapters.QueriesTableAdapter GetEmailAddresses = new DataSet_GC_ReportsTableAdapters.QueriesTableAdapter();
                GetEmailAddresses.ChangeConnectionString(strConnectionString);
                string strEmailAddresses = "";
                string strEmailPassword = "";
                string strPDFName = "";
                string strBody = System.IO.File.ReadAllText(strEmailBodyFile);
                DateTime dtTodayWithTime = DateTime.Now;
                string strEmployeeIDs = "";
                foreach (string strClientID in strArray)
                {
                    strEmailAddresses = GetEmailAddresses.sp04243_GC_Client_Email_Addresses_For_Reports(Convert.ToInt32(strClientID)).ToString();
                    if (!String.IsNullOrEmpty(strEmailAddresses))
                    {
                        try
                        {
                            strEmployeeIDs = "";
                            for (int i = 0; i < view.DataRowCount; i++)
                            {
                                if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")) && view.GetRowCellValue(i, "ClientID").ToString() == strClientID)
                                {
                                    strEmployeeIDs += Convert.ToString(view.GetRowCellValue(i, "EmployeeIDs")) + ",";
                                    strEmailPassword = view.GetRowCellValue(i, "EmailPassword").ToString();
                                }
                            }                           
                            strPDFName = "ClientJobBreakdown_" + strClientID.PadLeft(8, '0') + "_" + dtTodayWithTime.ToString("yyyy-MM-dd_HH_mm_ss") + ".PDF";
                            
                            // Store Client Sent Report record //
                            StoreSentReport.sp04245_GC_Client_Sent_Report_Add(1, Convert.ToInt32(strClientID), DateTime.Now, this.GlobalSettings.UserID, strPDFName);

                            // Create report and save it to PDF //
                            string strReportFileName = "EmployeePersonalDetailsVerification.repx";
                            rpt_HR_Employee_Listing rptReport = new rpt_HR_Employee_Listing(this.GlobalSettings, strEmployeeIDs, dtFromDate, dtToDate);
                            rptReport.LoadLayout(i_str_SavedDirectoryName + strReportFileName);

                            // Set security options of report so when it is exported, it can't be edited and is password protected //
                            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.EnableCopying = false;
                            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.ChangingPermissions = DevExpress.XtraPrinting.ChangingPermissions.None;
                            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsOptions.PrintingPermissions = DevExpress.XtraPrinting.PrintingPermissions.HighResolution;
                            rptReport.ExportOptions.Pdf.PasswordSecurityOptions.PermissionsPassword = "GroundControlXX";
                            if (!string.IsNullOrEmpty(strEmailPassword)) rptReport.ExportOptions.Pdf.PasswordSecurityOptions.OpenPassword = strEmailPassword;

                            strPDFName = strPDFFolderPath + strPDFName;  // Put path onto start of filename //
                            rptReport.ExportToPdf(strPDFName);

                            // PDF now created so email to team... //
                            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                            msg.From = new System.Net.Mail.MailAddress(strEmailFrom);
                            string[] strEmailTo = strEmailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (strEmailTo.Length > 0)
                            {
                                foreach (string strEmailAddress in strEmailTo)
                                {
                                    msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddress));
                                }
                            }
                            else
                            {
                                msg.To.Add(new System.Net.Mail.MailAddress(strEmailAddresses));  // Original value wouldn't split as no commas so it's just one email address so use it //
                            }
                            msg.Subject = strEmailSubjectLine;
                            if (!string.IsNullOrEmpty(strCCToEmailAddress)) msg.CC.Add(strCCToEmailAddress);
                            msg.Priority = System.Net.Mail.MailPriority.High;
                            msg.IsBodyHtml = true;

                            System.Net.Mail.AlternateView plainView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(System.Text.RegularExpressions.Regex.Replace(strBody, @"<(.|\n)*?>", string.Empty), null, "text/plain");
                            System.Net.Mail.AlternateView htmlView = System.Net.Mail.AlternateView.CreateAlternateViewFromString(strBody, null, "text/html");

                            // Create a new attachment //
                            System.Net.Mail.Attachment mailAttachment = new System.Net.Mail.Attachment(strPDFName); //create the attachment
                            msg.Attachments.Add(mailAttachment);

                            //create the LinkedResource (embedded image)
                            System.Net.Mail.LinkedResource logo = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Logo.jpg");
                            logo.ContentId = "companylogo";
                            //add the LinkedResource to the appropriate view
                            htmlView.LinkedResources.Add(logo);

                            //create the LinkedResource (embedded image)
                            System.Net.Mail.LinkedResource logo2 = new System.Net.Mail.LinkedResource(Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath) + "\\" + "Company_Footer.gif");
                            logo2.ContentId = "companyfooter";
                            //add the LinkedResource to the appropriate view
                            htmlView.LinkedResources.Add(logo2);

                            msg.AlternateViews.Add(plainView);
                            msg.AlternateViews.Add(htmlView);

                            object userState = msg;
                            System.Net.Mail.SmtpClient emailClient = null;
                            if (intSMTPMailServerPort != 0)
                            {
                                emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress, intSMTPMailServerPort);
                            }
                            else
                            {
                                emailClient = new System.Net.Mail.SmtpClient(strSMTPMailServerAddress);
                            }
                            if (!string.IsNullOrEmpty(strSMTPMailServerUsername) && !string.IsNullOrEmpty(strSMTPMailServerPassword))
                            {
                                System.Net.NetworkCredential basicCredential = new System.Net.NetworkCredential(strSMTPMailServerUsername, strSMTPMailServerPassword);
                                emailClient.UseDefaultCredentials = false;
                                emailClient.Credentials = basicCredential;
                            }
                            emailClient.SendAsync(msg, userState);
                        }
                        catch (Exception ex)
                        {
                            if (fProgress != null)
                            {
                                fProgress.Close();
                                fProgress = null;
                            }
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while creating the Client Job Breakdown Reports.\n\nMessage = [" + ex.Message + "].\n\nPlease try again. If the problem persists, contact Technical Support.", "Send Client Reports", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        // Untick Send Sites (if any are unsent due to an error they will be left ticked //
                        for (int i = 0; i < view.DataRowCount; i++)
                        {
                            if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")) && view.GetRowCellValue(i, "ClientID").ToString() == strClientID) view.SetRowCellValue(i, "CheckMarkSelection", false);
                        }                           

                    }
                    else // Keep track of the clients with no email address so we can leave their sites ticked at the end of the process //
                    {
                        strClientIDsWitMissingEmailAddresses += strClientID + ",";
                    }
                    intUpdateProgressTempCount++;
                    if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                    {
                        intUpdateProgressTempCount = 0;
                        fProgress.UpdateProgress(10);
                    }
                }
            }
            if (fProgress != null)
            {
                fProgress.Close();
                fProgress = null;
            }
            if (string.IsNullOrEmpty(strClientIDsWitMissingEmailAddresses))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Gritting Report(s) Emailed Successfully.", "Send Client Reports", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more Gritting Reports Failed to Email Successfully!\n\nTip: Clients which didn't send remain ticked in the Available Clients\\Sites list.\n\nTry sending again - if problems persist, contact Technical Support.", "Send Client Reports", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }*/
        }



        void ReportBuilder_Shown(object sender, EventArgs e)
        {
            if (loadingForm != null) loadingForm.Close();
        }

        void panel_ComponentAdded(object sender, ComponentEventArgs e)
        {
            if (e.Component is XRTableCell)// && FormLoaded)
            {
                //((XRTableCell)e.Component).Font = new Font("Arial", 12, FontStyle.Bold);
            }
            if (e.Component is XRLabel)
            {
                //((XRLabel)e.Component).PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
            }
        }

        private void AdjustEventHandlers(XtraReport ActiveReport)
        {
            foreach (Band b in ActiveReport.Bands)
            {
                foreach (XRControl c in b.Controls)
                {
                    if (c is XRTable)
                    {
                        XRTable t = (XRTable)c;
                        foreach (XRControl row in t.Controls)
                        {
                            foreach (XRControl cell in row.Controls)
                            {
                                if (cell.Tag.ToString() != "")
                                {
                                    cell.PreviewClick += new PreviewMouseEventHandler(My_PreviewClick);
                                }
                            }
                        }
                    }
                }
            }
        }

        private XRTableCell currentSortCell;  // Used for sorting on-the-fly //
        private Boolean SortAscending = false;
        private void My_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            // ***** Used for on-the-fly sorting ***** //
            // Turn off sorting //
            DetailBand db = (DetailBand)rptReport.Bands.GetBandByType(typeof(DetailBand));
            if (db != null)
            {
                if (((XRControl)sender).Tag.ToString() == null) return;

                printingSystem1.Begin();  // Switch redraw off //
                loadingForm = new WaitDialogForm("Sorting Report...", "Reporting");
                loadingForm.Show();

                db.SortFields.Clear();
                if (currentSortCell != null)
                {
                    currentSortCell.Text = currentSortCell.Text.Remove(currentSortCell.Text.Length - 1, 1);
                }
                // Create a new field to sort //
                GroupField grField = new GroupField();
                grField.FieldName = ((XRControl)sender).Tag.ToString();
                if (currentSortCell != null)
                {
                    if (currentSortCell.Text == ((XRLabel)sender).Text)
                    {
                        if (SortAscending)
                        {
                            grField.SortOrder = XRColumnSortOrder.Descending;
                            SortAscending = !SortAscending;
                        }
                        else
                        {
                            grField.SortOrder = XRColumnSortOrder.Ascending;
                            SortAscending = !SortAscending;
                        }
                    }
                    else
                    {
                        grField.SortOrder = XRColumnSortOrder.Ascending;
                        SortAscending = true;
                    }
                }
                else
                {
                    grField.SortOrder = XRColumnSortOrder.Ascending;
                    SortAscending = true;
                }
                // Add sorting //
                db.SortFields.Add(grField);
                ((XRLabel)sender).Text = ((XRLabel)sender).Text + "*";
                currentSortCell = (XRTableCell)sender;
                // Recreate the report document.
                rptReport.CreateDocument();
                loadingForm.Close();
                printingSystem1.End();  // Switch redraw back on //
            }
        }

        public class SaveCommandHandler : DevExpress.XtraReports.UserDesigner.ICommandHandler
        {
            XRDesignPanel panel;
            public string strFullPath = "";
            public string strFileName = "";

            public SaveCommandHandler(XRDesignPanel panel, string strFullPath, string strFileName)
            {
                this.panel = panel;
                this.strFullPath = strFullPath;
                this.strFileName = strFileName;
            }

            public void HandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, object[] args)
            {
                //if (!CanHandleCommand(command)) return;
                Save();  // Save report //
                //handled = true;  // Set handled to true to avoid the standard saving procedure to be called.
            }

            public bool CanHandleCommand(DevExpress.XtraReports.UserDesigner.ReportCommand command, ref bool useNextHandler)
            {
                useNextHandler = !(command == ReportCommand.SaveFile ||
                    command == ReportCommand.SaveFileAs ||
                    command == ReportCommand.Closing);
                return !useNextHandler;
            }

            void Save()
            {
                Boolean blSaved = false;
                panel.ReportState = ReportState.Saved;  // Prevent the "Report has been changed" dialog from being shown //

                // Update existing file layout //
                panel.Report.DataSource = null;
                panel.Report.DataMember = null;
                panel.Report.DataAdapter = null;
                try
                {
                    panel.Report.SaveLayout(strFullPath + strFileName);
                    blSaved = true;
                }
                catch (Exception ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save Report Layout... there is a problem with the default path!\n\nContact Technical Support.", "Save Report Layout", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Console.WriteLine(ex.Message);
                    blSaved = false;
                }
                if (blSaved)
                {
                    panel.ReportState = ReportState.Saved;   // Prevent the "Report has been changed" dialog from being shown //
                }
            }
        }


 



    }
}

namespace WoodPlan5
{
    partial class frm_HR_Work_Schedule_View_Shift
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Work_Schedule_View_Shift));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling14 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.AbsenceRecordTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.spHR00207WorkPatternShiftBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.RegionNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BusinessAreaNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LocationNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DepartmentNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CalculatedHoursTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PaidForBreaksCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.BreakLengthTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EndTimeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.StartTimeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.DayWorkedIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DateWorkedTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForRegionID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDateWorked = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStartTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBreakLength = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCalculatedHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPaidForBreaks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForDepartmentName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLocationName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBusinessAreaName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRegionName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAbsenceRecordType = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00207_Work_Pattern_ShiftTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00207_Work_Pattern_ShiftTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceRecordTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00207WorkPatternShiftBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculatedHoursTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidForBreaksCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BreakLengthTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndTimeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DayWorkedIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateWorkedTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateWorked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBreakLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculatedHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidForBreaks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceRecordType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 478);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 478);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 452);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.AbsenceRecordTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BusinessAreaNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DepartmentNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CalculatedHoursTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PaidForBreaksCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.BreakLengthTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EndTimeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StartTimeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.DayWorkedIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DateWorkedTextEdit);
            this.dataLayoutControl1.DataSource = this.spHR00207WorkPatternShiftBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRegionID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(738, 204, 254, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 452);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // AbsenceRecordTypeTextEdit
            // 
            this.AbsenceRecordTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "AbsenceRecordType", true));
            this.AbsenceRecordTypeTextEdit.Location = new System.Drawing.Point(434, 271);
            this.AbsenceRecordTypeTextEdit.MenuManager = this.barManager1;
            this.AbsenceRecordTypeTextEdit.Name = "AbsenceRecordTypeTextEdit";
            this.AbsenceRecordTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AbsenceRecordTypeTextEdit, true);
            this.AbsenceRecordTypeTextEdit.Size = new System.Drawing.Size(182, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AbsenceRecordTypeTextEdit, optionsSpelling1);
            this.AbsenceRecordTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.AbsenceRecordTypeTextEdit.TabIndex = 49;
            // 
            // spHR00207WorkPatternShiftBindingSource
            // 
            this.spHR00207WorkPatternShiftBindingSource.DataMember = "sp_HR_00207_Work_Pattern_Shift";
            this.spHR00207WorkPatternShiftBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // RegionNameTextEdit
            // 
            this.RegionNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "RegionName", true));
            this.RegionNameTextEdit.Location = new System.Drawing.Point(130, 165);
            this.RegionNameTextEdit.MenuManager = this.barManager1;
            this.RegionNameTextEdit.Name = "RegionNameTextEdit";
            this.RegionNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionNameTextEdit, true);
            this.RegionNameTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionNameTextEdit, optionsSpelling2);
            this.RegionNameTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionNameTextEdit.TabIndex = 48;
            // 
            // BusinessAreaNameTextEdit
            // 
            this.BusinessAreaNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "BusinessAreaName", true));
            this.BusinessAreaNameTextEdit.Location = new System.Drawing.Point(130, 141);
            this.BusinessAreaNameTextEdit.MenuManager = this.barManager1;
            this.BusinessAreaNameTextEdit.Name = "BusinessAreaNameTextEdit";
            this.BusinessAreaNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BusinessAreaNameTextEdit, true);
            this.BusinessAreaNameTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BusinessAreaNameTextEdit, optionsSpelling3);
            this.BusinessAreaNameTextEdit.StyleController = this.dataLayoutControl1;
            this.BusinessAreaNameTextEdit.TabIndex = 47;
            // 
            // LocationNameTextEdit
            // 
            this.LocationNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "LocationName", true));
            this.LocationNameTextEdit.Location = new System.Drawing.Point(130, 117);
            this.LocationNameTextEdit.MenuManager = this.barManager1;
            this.LocationNameTextEdit.Name = "LocationNameTextEdit";
            this.LocationNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationNameTextEdit, true);
            this.LocationNameTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationNameTextEdit, optionsSpelling4);
            this.LocationNameTextEdit.StyleController = this.dataLayoutControl1;
            this.LocationNameTextEdit.TabIndex = 46;
            // 
            // DepartmentNameTextEdit
            // 
            this.DepartmentNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "DepartmentName", true));
            this.DepartmentNameTextEdit.Location = new System.Drawing.Point(130, 93);
            this.DepartmentNameTextEdit.MenuManager = this.barManager1;
            this.DepartmentNameTextEdit.Name = "DepartmentNameTextEdit";
            this.DepartmentNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DepartmentNameTextEdit, true);
            this.DepartmentNameTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DepartmentNameTextEdit, optionsSpelling5);
            this.DepartmentNameTextEdit.StyleController = this.dataLayoutControl1;
            this.DepartmentNameTextEdit.TabIndex = 45;
            // 
            // EmployeeNumberTextEdit
            // 
            this.EmployeeNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "EmployeeNumber", true));
            this.EmployeeNumberTextEdit.Location = new System.Drawing.Point(130, 59);
            this.EmployeeNumberTextEdit.MenuManager = this.barManager1;
            this.EmployeeNumberTextEdit.Name = "EmployeeNumberTextEdit";
            this.EmployeeNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNumberTextEdit, true);
            this.EmployeeNumberTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNumberTextEdit, optionsSpelling6);
            this.EmployeeNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNumberTextEdit.TabIndex = 44;
            // 
            // EmployeeNameTextEdit
            // 
            this.EmployeeNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "EmployeeName", true));
            this.EmployeeNameTextEdit.Location = new System.Drawing.Point(130, 35);
            this.EmployeeNameTextEdit.MenuManager = this.barManager1;
            this.EmployeeNameTextEdit.Name = "EmployeeNameTextEdit";
            this.EmployeeNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNameTextEdit, true);
            this.EmployeeNameTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNameTextEdit, optionsSpelling7);
            this.EmployeeNameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameTextEdit.TabIndex = 43;
            // 
            // CalculatedHoursTextEdit
            // 
            this.CalculatedHoursTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "CalculatedHours", true));
            this.CalculatedHoursTextEdit.Location = new System.Drawing.Point(130, 271);
            this.CalculatedHoursTextEdit.MenuManager = this.barManager1;
            this.CalculatedHoursTextEdit.Name = "CalculatedHoursTextEdit";
            this.CalculatedHoursTextEdit.Properties.Mask.EditMask = "#####0.00 Hours";
            this.CalculatedHoursTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CalculatedHoursTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CalculatedHoursTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CalculatedHoursTextEdit, true);
            this.CalculatedHoursTextEdit.Size = new System.Drawing.Size(182, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CalculatedHoursTextEdit, optionsSpelling8);
            this.CalculatedHoursTextEdit.StyleController = this.dataLayoutControl1;
            this.CalculatedHoursTextEdit.TabIndex = 42;
            // 
            // PaidForBreaksCheckEdit
            // 
            this.PaidForBreaksCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "PaidForBreaks", true));
            this.PaidForBreaksCheckEdit.Location = new System.Drawing.Point(434, 247);
            this.PaidForBreaksCheckEdit.MenuManager = this.barManager1;
            this.PaidForBreaksCheckEdit.Name = "PaidForBreaksCheckEdit";
            this.PaidForBreaksCheckEdit.Properties.Caption = "(Ticked if Yes)";
            this.PaidForBreaksCheckEdit.Properties.ReadOnly = true;
            this.PaidForBreaksCheckEdit.Properties.ValueChecked = 1;
            this.PaidForBreaksCheckEdit.Properties.ValueUnchecked = 0;
            this.PaidForBreaksCheckEdit.Size = new System.Drawing.Size(182, 19);
            this.PaidForBreaksCheckEdit.StyleController = this.dataLayoutControl1;
            this.PaidForBreaksCheckEdit.TabIndex = 41;
            // 
            // BreakLengthTextEdit
            // 
            this.BreakLengthTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "BreakLength", true));
            this.BreakLengthTextEdit.Location = new System.Drawing.Point(130, 247);
            this.BreakLengthTextEdit.MenuManager = this.barManager1;
            this.BreakLengthTextEdit.Name = "BreakLengthTextEdit";
            this.BreakLengthTextEdit.Properties.Mask.EditMask = "#####0 minutes";
            this.BreakLengthTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.BreakLengthTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BreakLengthTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BreakLengthTextEdit, true);
            this.BreakLengthTextEdit.Size = new System.Drawing.Size(182, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BreakLengthTextEdit, optionsSpelling9);
            this.BreakLengthTextEdit.StyleController = this.dataLayoutControl1;
            this.BreakLengthTextEdit.TabIndex = 40;
            // 
            // EndTimeTextEdit
            // 
            this.EndTimeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "EndTime", true));
            this.EndTimeTextEdit.Location = new System.Drawing.Point(434, 223);
            this.EndTimeTextEdit.MenuManager = this.barManager1;
            this.EndTimeTextEdit.Name = "EndTimeTextEdit";
            this.EndTimeTextEdit.Properties.Mask.EditMask = "t";
            this.EndTimeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.EndTimeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EndTimeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EndTimeTextEdit, true);
            this.EndTimeTextEdit.Size = new System.Drawing.Size(182, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EndTimeTextEdit, optionsSpelling10);
            this.EndTimeTextEdit.StyleController = this.dataLayoutControl1;
            this.EndTimeTextEdit.TabIndex = 39;
            // 
            // StartTimeTextEdit
            // 
            this.StartTimeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "StartTime", true));
            this.StartTimeTextEdit.Location = new System.Drawing.Point(130, 223);
            this.StartTimeTextEdit.MenuManager = this.barManager1;
            this.StartTimeTextEdit.Name = "StartTimeTextEdit";
            this.StartTimeTextEdit.Properties.Mask.EditMask = "t";
            this.StartTimeTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.StartTimeTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartTimeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartTimeTextEdit, true);
            this.StartTimeTextEdit.Size = new System.Drawing.Size(182, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartTimeTextEdit, optionsSpelling11);
            this.StartTimeTextEdit.StyleController = this.dataLayoutControl1;
            this.StartTimeTextEdit.TabIndex = 38;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(24, 341);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(580, 70);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling12);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 37;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00207WorkPatternShiftBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(130, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // DayWorkedIDTextEdit
            // 
            this.DayWorkedIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "DayWorkedID", true));
            this.DayWorkedIDTextEdit.Location = new System.Drawing.Point(92, 35);
            this.DayWorkedIDTextEdit.MenuManager = this.barManager1;
            this.DayWorkedIDTextEdit.Name = "DayWorkedIDTextEdit";
            this.DayWorkedIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DayWorkedIDTextEdit, true);
            this.DayWorkedIDTextEdit.Size = new System.Drawing.Size(524, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DayWorkedIDTextEdit, optionsSpelling13);
            this.DayWorkedIDTextEdit.StyleController = this.dataLayoutControl1;
            this.DayWorkedIDTextEdit.TabIndex = 4;
            // 
            // DateWorkedTextEdit
            // 
            this.DateWorkedTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00207WorkPatternShiftBindingSource, "DateWorked", true));
            this.DateWorkedTextEdit.Location = new System.Drawing.Point(130, 199);
            this.DateWorkedTextEdit.MenuManager = this.barManager1;
            this.DateWorkedTextEdit.Name = "DateWorkedTextEdit";
            this.DateWorkedTextEdit.Properties.Mask.EditMask = "d";
            this.DateWorkedTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.DateWorkedTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DateWorkedTextEdit.Properties.MaxLength = 100;
            this.DateWorkedTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DateWorkedTextEdit, true);
            this.DateWorkedTextEdit.Size = new System.Drawing.Size(486, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DateWorkedTextEdit, optionsSpelling14);
            this.DateWorkedTextEdit.StyleController = this.dataLayoutControl1;
            this.DateWorkedTextEdit.TabIndex = 35;
            // 
            // ItemForRegionID
            // 
            this.ItemForRegionID.Control = this.DayWorkedIDTextEdit;
            this.ItemForRegionID.CustomizationFormText = "Day Worked ID:";
            this.ItemForRegionID.Location = new System.Drawing.Point(0, 0);
            this.ItemForRegionID.Name = "ItemForRegionID";
            this.ItemForRegionID.Size = new System.Drawing.Size(608, 24);
            this.ItemForRegionID.Text = "Day Worked ID:";
            this.ItemForRegionID.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 452);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 23);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(118, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(118, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(118, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(295, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(313, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(118, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDateWorked,
            this.tabbedControlGroup1,
            this.emptySpaceItem3,
            this.emptySpaceItem6,
            this.ItemForStartTime,
            this.ItemForBreakLength,
            this.ItemForCalculatedHours,
            this.ItemForEmployeeName,
            this.ItemForEmployeeNumber,
            this.ItemForEndTime,
            this.ItemForPaidForBreaks,
            this.emptySpaceItem4,
            this.ItemForDepartmentName,
            this.ItemForLocationName,
            this.ItemForBusinessAreaName,
            this.emptySpaceItem7,
            this.ItemForRegionName,
            this.ItemForAbsenceRecordType});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 23);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(608, 409);
            // 
            // ItemForDateWorked
            // 
            this.ItemForDateWorked.AllowHide = false;
            this.ItemForDateWorked.Control = this.DateWorkedTextEdit;
            this.ItemForDateWorked.CustomizationFormText = "Date Worked:";
            this.ItemForDateWorked.Location = new System.Drawing.Point(0, 164);
            this.ItemForDateWorked.Name = "ItemForDateWorked";
            this.ItemForDateWorked.Size = new System.Drawing.Size(608, 24);
            this.ItemForDateWorked.Text = "Date Worked:";
            this.ItemForDateWorked.TextSize = new System.Drawing.Size(115, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 270);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(608, 122);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(584, 74);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(584, 74);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 392);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(608, 17);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 260);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStartTime
            // 
            this.ItemForStartTime.Control = this.StartTimeTextEdit;
            this.ItemForStartTime.CustomizationFormText = "Start Time:";
            this.ItemForStartTime.Location = new System.Drawing.Point(0, 188);
            this.ItemForStartTime.Name = "ItemForStartTime";
            this.ItemForStartTime.Size = new System.Drawing.Size(304, 24);
            this.ItemForStartTime.Text = "Start Time:";
            this.ItemForStartTime.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForBreakLength
            // 
            this.ItemForBreakLength.Control = this.BreakLengthTextEdit;
            this.ItemForBreakLength.CustomizationFormText = "Break Length:";
            this.ItemForBreakLength.Location = new System.Drawing.Point(0, 212);
            this.ItemForBreakLength.Name = "ItemForBreakLength";
            this.ItemForBreakLength.Size = new System.Drawing.Size(304, 24);
            this.ItemForBreakLength.Text = "Break Length:";
            this.ItemForBreakLength.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForCalculatedHours
            // 
            this.ItemForCalculatedHours.Control = this.CalculatedHoursTextEdit;
            this.ItemForCalculatedHours.CustomizationFormText = "Calculated Hours:";
            this.ItemForCalculatedHours.Location = new System.Drawing.Point(0, 236);
            this.ItemForCalculatedHours.Name = "ItemForCalculatedHours";
            this.ItemForCalculatedHours.Size = new System.Drawing.Size(304, 24);
            this.ItemForCalculatedHours.Text = "Calculated Hours:";
            this.ItemForCalculatedHours.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.Control = this.EmployeeNameTextEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Employee Name:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 0);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(608, 24);
            this.ItemForEmployeeName.Text = "Employee Name:";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForEmployeeNumber
            // 
            this.ItemForEmployeeNumber.Control = this.EmployeeNumberTextEdit;
            this.ItemForEmployeeNumber.CustomizationFormText = "Employee #:";
            this.ItemForEmployeeNumber.Location = new System.Drawing.Point(0, 24);
            this.ItemForEmployeeNumber.Name = "ItemForEmployeeNumber";
            this.ItemForEmployeeNumber.Size = new System.Drawing.Size(608, 24);
            this.ItemForEmployeeNumber.Text = "Employee #:";
            this.ItemForEmployeeNumber.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForEndTime
            // 
            this.ItemForEndTime.Control = this.EndTimeTextEdit;
            this.ItemForEndTime.CustomizationFormText = "End Time:";
            this.ItemForEndTime.Location = new System.Drawing.Point(304, 188);
            this.ItemForEndTime.Name = "ItemForEndTime";
            this.ItemForEndTime.Size = new System.Drawing.Size(304, 24);
            this.ItemForEndTime.Text = "End Time:";
            this.ItemForEndTime.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForPaidForBreaks
            // 
            this.ItemForPaidForBreaks.Control = this.PaidForBreaksCheckEdit;
            this.ItemForPaidForBreaks.CustomizationFormText = "Paid For Breaks:";
            this.ItemForPaidForBreaks.Location = new System.Drawing.Point(304, 212);
            this.ItemForPaidForBreaks.Name = "ItemForPaidForBreaks";
            this.ItemForPaidForBreaks.Size = new System.Drawing.Size(304, 24);
            this.ItemForPaidForBreaks.Text = "Paid For Breaks:";
            this.ItemForPaidForBreaks.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 154);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForDepartmentName
            // 
            this.ItemForDepartmentName.Control = this.DepartmentNameTextEdit;
            this.ItemForDepartmentName.CustomizationFormText = "Department:";
            this.ItemForDepartmentName.Location = new System.Drawing.Point(0, 58);
            this.ItemForDepartmentName.Name = "ItemForDepartmentName";
            this.ItemForDepartmentName.Size = new System.Drawing.Size(608, 24);
            this.ItemForDepartmentName.Text = "Department:";
            this.ItemForDepartmentName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForLocationName
            // 
            this.ItemForLocationName.Control = this.LocationNameTextEdit;
            this.ItemForLocationName.CustomizationFormText = "Location:";
            this.ItemForLocationName.Location = new System.Drawing.Point(0, 82);
            this.ItemForLocationName.Name = "ItemForLocationName";
            this.ItemForLocationName.Size = new System.Drawing.Size(608, 24);
            this.ItemForLocationName.Text = "Location:";
            this.ItemForLocationName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForBusinessAreaName
            // 
            this.ItemForBusinessAreaName.Control = this.BusinessAreaNameTextEdit;
            this.ItemForBusinessAreaName.CustomizationFormText = "Business Area:";
            this.ItemForBusinessAreaName.Location = new System.Drawing.Point(0, 106);
            this.ItemForBusinessAreaName.Name = "ItemForBusinessAreaName";
            this.ItemForBusinessAreaName.Size = new System.Drawing.Size(608, 24);
            this.ItemForBusinessAreaName.Text = "Business Area:";
            this.ItemForBusinessAreaName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRegionName
            // 
            this.ItemForRegionName.Control = this.RegionNameTextEdit;
            this.ItemForRegionName.CustomizationFormText = "Region:";
            this.ItemForRegionName.Location = new System.Drawing.Point(0, 130);
            this.ItemForRegionName.Name = "ItemForRegionName";
            this.ItemForRegionName.Size = new System.Drawing.Size(608, 24);
            this.ItemForRegionName.Text = "Region:";
            this.ItemForRegionName.TextSize = new System.Drawing.Size(115, 13);
            // 
            // ItemForAbsenceRecordType
            // 
            this.ItemForAbsenceRecordType.Control = this.AbsenceRecordTypeTextEdit;
            this.ItemForAbsenceRecordType.CustomizationFormText = "Linked Absence Record:";
            this.ItemForAbsenceRecordType.Location = new System.Drawing.Point(304, 236);
            this.ItemForAbsenceRecordType.Name = "ItemForAbsenceRecordType";
            this.ItemForAbsenceRecordType.Size = new System.Drawing.Size(304, 24);
            this.ItemForAbsenceRecordType.Text = "Linked Absence Record:";
            this.ItemForAbsenceRecordType.TextSize = new System.Drawing.Size(115, 13);
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00207_Work_Pattern_ShiftTableAdapter
            // 
            this.sp_HR_00207_Work_Pattern_ShiftTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Work_Schedule_View_Shift
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 508);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Work_Schedule_View_Shift";
            this.Text = "Edit Shift Pattern";
            this.Activated += new System.EventHandler(this.frm_HR_Work_Schedule_View_Shift_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Work_Schedule_View_Shift_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Work_Schedule_View_Shift_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AbsenceRecordTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00207WorkPatternShiftBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculatedHoursTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidForBreaksCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BreakLengthTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndTimeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DayWorkedIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateWorkedTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateWorked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBreakLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculatedHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidForBreaks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAbsenceRecordType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit DayWorkedIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateWorked;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit DateWorkedTextEdit;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00207WorkPatternShiftBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00207_Work_Pattern_ShiftTableAdapter sp_HR_00207_Work_Pattern_ShiftTableAdapter;
        private DevExpress.XtraEditors.TextEdit StartTimeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartTime;
        private DevExpress.XtraEditors.TextEdit EndTimeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndTime;
        private DevExpress.XtraEditors.TextEdit BreakLengthTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBreakLength;
        private DevExpress.XtraEditors.CheckEdit PaidForBreaksCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaidForBreaks;
        private DevExpress.XtraEditors.TextEdit CalculatedHoursTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCalculatedHours;
        private DevExpress.XtraEditors.TextEdit EmployeeNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DevExpress.XtraEditors.TextEdit EmployeeNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit DepartmentNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDepartmentName;
        private DevExpress.XtraEditors.TextEdit LocationNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocationName;
        private DevExpress.XtraEditors.TextEdit BusinessAreaNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBusinessAreaName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.TextEdit RegionNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionName;
        private DevExpress.XtraEditors.TextEdit AbsenceRecordTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAbsenceRecordType;
    }
}

namespace WoodPlan5
{
    partial class frm_HR_Sanction_Action_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Sanction_Action_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.StartedByIdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.spHR00058GetEmployeeSanctionProgressItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnReOpen = new DevExpress.XtraEditors.SimpleButton();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.ActionDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.ProgressDetailMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.StartedByNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClosedByNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClosedDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.IdTextEditx = new DevExpress.XtraEditors.TextEdit();
            this.IdTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForSanctionId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForId = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartedById = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStartedByName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActionDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForProgressDetail = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layGrpClosed = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForClosedByName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClosedDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.layItemBtnClose = new DevExpress.XtraLayout.LayoutControlItem();
            this.layItemBtnReOpen = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.TableAdapterManager();
            this.sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartedByIdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00058GetEmployeeSanctionProgressItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressDetailMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartedByNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedByNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEditx.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSanctionId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartedById)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartedByName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProgressDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpClosed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedByName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layItemBtnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layItemBtnReOpen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(520, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 373);
            this.barDockControlBottom.Size = new System.Drawing.Size(520, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 347);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(520, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 347);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(520, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 373);
            this.barDockControl2.Size = new System.Drawing.Size(520, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 347);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(520, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 347);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.StartedByIdTextEdit);
            this.dataLayoutControl1.Controls.Add(this.btnReOpen);
            this.dataLayoutControl1.Controls.Add(this.btnClose);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.ActionDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.ProgressDetailMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.StartedByNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClosedByNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClosedDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.IdTextEditx);
            this.dataLayoutControl1.Controls.Add(this.IdTextEdit);
            this.dataLayoutControl1.DataSource = this.spHR00058GetEmployeeSanctionProgressItemsBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSanctionId,
            this.ItemForId,
            this.ItemForStartedById});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(846, 102, 267, 499);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(520, 347);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // StartedByIdTextEdit
            // 
            this.StartedByIdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00058GetEmployeeSanctionProgressItemsBindingSource, "StartedById", true));
            this.StartedByIdTextEdit.Location = new System.Drawing.Point(123, 254);
            this.StartedByIdTextEdit.MenuManager = this.barManager1;
            this.StartedByIdTextEdit.Name = "StartedByIdTextEdit";
            this.StartedByIdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartedByIdTextEdit, true);
            this.StartedByIdTextEdit.Size = new System.Drawing.Size(373, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartedByIdTextEdit, optionsSpelling1);
            this.StartedByIdTextEdit.StyleController = this.dataLayoutControl1;
            this.StartedByIdTextEdit.TabIndex = 32;
            // 
            // spHR00058GetEmployeeSanctionProgressItemsBindingSource
            // 
            this.spHR00058GetEmployeeSanctionProgressItemsBindingSource.DataMember = "sp_HR_00058_Get_Employee_Sanction_Progress_Items";
            this.spHR00058GetEmployeeSanctionProgressItemsBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnReOpen
            // 
            this.btnReOpen.Image = ((System.Drawing.Image)(resources.GetObject("btnReOpen.Image")));
            this.btnReOpen.Location = new System.Drawing.Point(383, 229);
            this.btnReOpen.Name = "btnReOpen";
            this.btnReOpen.Size = new System.Drawing.Size(113, 22);
            this.btnReOpen.StyleController = this.dataLayoutControl1;
            this.btnReOpen.TabIndex = 31;
            this.btnReOpen.Text = "Re-Open Action";
            this.btnReOpen.Click += new System.EventHandler(this.btnReOpen_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(283, 229);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(96, 22);
            this.btnClose.StyleController = this.dataLayoutControl1;
            this.btnClose.TabIndex = 30;
            this.btnClose.Text = "Close Action";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00058GetEmployeeSanctionProgressItemsBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(100, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(174, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 17;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // ActionDateDateEdit
            // 
            this.ActionDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00058GetEmployeeSanctionProgressItemsBindingSource, "ActionDate", true));
            this.ActionDateDateEdit.EditValue = null;
            this.ActionDateDateEdit.Location = new System.Drawing.Point(100, 59);
            this.ActionDateDateEdit.MenuManager = this.barManager1;
            this.ActionDateDateEdit.Name = "ActionDateDateEdit";
            this.ActionDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActionDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActionDateDateEdit.Properties.Mask.EditMask = "g";
            this.ActionDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ActionDateDateEdit.Size = new System.Drawing.Size(408, 20);
            this.ActionDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ActionDateDateEdit.TabIndex = 25;
            this.ActionDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ActionDateDateEdit_Validating);
            // 
            // ProgressDetailMemoEdit
            // 
            this.ProgressDetailMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00058GetEmployeeSanctionProgressItemsBindingSource, "Detail", true));
            this.ProgressDetailMemoEdit.Location = new System.Drawing.Point(100, 83);
            this.ProgressDetailMemoEdit.MenuManager = this.barManager1;
            this.ProgressDetailMemoEdit.Name = "ProgressDetailMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.ProgressDetailMemoEdit, true);
            this.ProgressDetailMemoEdit.Size = new System.Drawing.Size(408, 40);
            this.scSpellChecker.SetSpellCheckerOptions(this.ProgressDetailMemoEdit, optionsSpelling2);
            this.ProgressDetailMemoEdit.StyleController = this.dataLayoutControl1;
            this.ProgressDetailMemoEdit.TabIndex = 26;
            // 
            // StartedByNameTextEdit
            // 
            this.StartedByNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00058GetEmployeeSanctionProgressItemsBindingSource, "StartedByName", true));
            this.StartedByNameTextEdit.Location = new System.Drawing.Point(100, 35);
            this.StartedByNameTextEdit.MenuManager = this.barManager1;
            this.StartedByNameTextEdit.Name = "StartedByNameTextEdit";
            this.StartedByNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.StartedByNameTextEdit, true);
            this.StartedByNameTextEdit.Size = new System.Drawing.Size(408, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.StartedByNameTextEdit, optionsSpelling3);
            this.StartedByNameTextEdit.StyleController = this.dataLayoutControl1;
            this.StartedByNameTextEdit.TabIndex = 27;
            // 
            // ClosedByNameTextEdit
            // 
            this.ClosedByNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00058GetEmployeeSanctionProgressItemsBindingSource, "ClosedByName", true));
            this.ClosedByNameTextEdit.Location = new System.Drawing.Point(112, 171);
            this.ClosedByNameTextEdit.MenuManager = this.barManager1;
            this.ClosedByNameTextEdit.Name = "ClosedByNameTextEdit";
            this.ClosedByNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClosedByNameTextEdit, true);
            this.ClosedByNameTextEdit.Size = new System.Drawing.Size(384, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClosedByNameTextEdit, optionsSpelling4);
            this.ClosedByNameTextEdit.StyleController = this.dataLayoutControl1;
            this.ClosedByNameTextEdit.TabIndex = 28;
            // 
            // ClosedDateDateEdit
            // 
            this.ClosedDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00058GetEmployeeSanctionProgressItemsBindingSource, "ClosedDate", true));
            this.ClosedDateDateEdit.EditValue = null;
            this.ClosedDateDateEdit.Location = new System.Drawing.Point(112, 195);
            this.ClosedDateDateEdit.MenuManager = this.barManager1;
            this.ClosedDateDateEdit.Name = "ClosedDateDateEdit";
            this.ClosedDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClosedDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClosedDateDateEdit.Properties.Mask.EditMask = "g";
            this.ClosedDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ClosedDateDateEdit.Properties.ReadOnly = true;
            this.ClosedDateDateEdit.Size = new System.Drawing.Size(384, 20);
            this.ClosedDateDateEdit.StyleController = this.dataLayoutControl1;
            this.ClosedDateDateEdit.TabIndex = 29;
            // 
            // IdTextEditx
            // 
            this.IdTextEditx.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00058GetEmployeeSanctionProgressItemsBindingSource, "Id", true));
            this.IdTextEditx.Location = new System.Drawing.Point(107, 137);
            this.IdTextEditx.MenuManager = this.barManager1;
            this.IdTextEditx.Name = "IdTextEditx";
            this.IdTextEditx.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.IdTextEditx.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.IdTextEditx, true);
            this.IdTextEditx.Size = new System.Drawing.Size(401, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.IdTextEditx, optionsSpelling5);
            this.IdTextEditx.StyleController = this.dataLayoutControl1;
            this.IdTextEditx.TabIndex = 24;
            // 
            // IdTextEdit
            // 
            this.IdTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00058GetEmployeeSanctionProgressItemsBindingSource, "SanctionId", true));
            this.IdTextEdit.Location = new System.Drawing.Point(100, 137);
            this.IdTextEdit.MenuManager = this.barManager1;
            this.IdTextEdit.Name = "IdTextEdit";
            this.IdTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.IdTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.IdTextEdit, true);
            this.IdTextEdit.Size = new System.Drawing.Size(408, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.IdTextEdit, optionsSpelling6);
            this.IdTextEdit.StyleController = this.dataLayoutControl1;
            this.IdTextEdit.TabIndex = 23;
            // 
            // ItemForSanctionId
            // 
            this.ItemForSanctionId.Control = this.IdTextEditx;
            this.ItemForSanctionId.CustomizationFormText = "Sanction Action ID:";
            this.ItemForSanctionId.Location = new System.Drawing.Point(0, 125);
            this.ItemForSanctionId.Name = "ItemForSanctionId";
            this.ItemForSanctionId.Size = new System.Drawing.Size(500, 24);
            this.ItemForSanctionId.Text = "Sanction Action ID:";
            this.ItemForSanctionId.TextSize = new System.Drawing.Size(92, 13);
            // 
            // ItemForId
            // 
            this.ItemForId.Control = this.IdTextEdit;
            this.ItemForId.CustomizationFormText = "Sanction ID:";
            this.ItemForId.Location = new System.Drawing.Point(0, 125);
            this.ItemForId.Name = "ItemForId";
            this.ItemForId.Size = new System.Drawing.Size(500, 24);
            this.ItemForId.Text = "Sanction ID:";
            this.ItemForId.TextSize = new System.Drawing.Size(92, 13);
            // 
            // ItemForStartedById
            // 
            this.ItemForStartedById.Control = this.StartedByIdTextEdit;
            this.ItemForStartedById.CustomizationFormText = "Started By Staff ID:";
            this.ItemForStartedById.Location = new System.Drawing.Point(0, 84);
            this.ItemForStartedById.Name = "ItemForStartedById";
            this.ItemForStartedById.Size = new System.Drawing.Size(476, 24);
            this.ItemForStartedById.Text = "Started By Staff ID:";
            this.ItemForStartedById.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.ItemForActionDate,
            this.ItemForProgressDetail,
            this.emptySpaceItem1,
            this.layGrpClosed,
            this.emptySpaceItem7});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(520, 347);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.emptySpaceItem6,
            this.ItemForStartedByName});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(500, 47);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(88, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(178, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(88, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(88, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(88, 23);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(266, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(234, 23);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStartedByName
            // 
            this.ItemForStartedByName.Control = this.StartedByNameTextEdit;
            this.ItemForStartedByName.CustomizationFormText = "Started By Name:";
            this.ItemForStartedByName.Location = new System.Drawing.Point(0, 23);
            this.ItemForStartedByName.Name = "ItemForStartedByName";
            this.ItemForStartedByName.Size = new System.Drawing.Size(500, 24);
            this.ItemForStartedByName.Text = "Started By Name:";
            this.ItemForStartedByName.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForActionDate
            // 
            this.ItemForActionDate.Control = this.ActionDateDateEdit;
            this.ItemForActionDate.CustomizationFormText = "Action Date:";
            this.ItemForActionDate.Location = new System.Drawing.Point(0, 47);
            this.ItemForActionDate.Name = "ItemForActionDate";
            this.ItemForActionDate.Size = new System.Drawing.Size(500, 24);
            this.ItemForActionDate.Text = "Action Date:";
            this.ItemForActionDate.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForProgressDetail
            // 
            this.ItemForProgressDetail.Control = this.ProgressDetailMemoEdit;
            this.ItemForProgressDetail.CustomizationFormText = "Action Detail:";
            this.ItemForProgressDetail.Location = new System.Drawing.Point(0, 71);
            this.ItemForProgressDetail.Name = "ItemForProgressDetail";
            this.ItemForProgressDetail.Size = new System.Drawing.Size(500, 44);
            this.ItemForProgressDetail.Text = "Action Detail:";
            this.ItemForProgressDetail.TextSize = new System.Drawing.Size(85, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 255);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(500, 72);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layGrpClosed
            // 
            this.layGrpClosed.CustomizationFormText = "Closed Details";
            this.layGrpClosed.ExpandButtonVisible = true;
            this.layGrpClosed.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpClosed.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForClosedByName,
            this.ItemForClosedDate,
            this.layItemBtnClose,
            this.layItemBtnReOpen,
            this.emptySpaceItem2,
            this.emptySpaceItem3});
            this.layGrpClosed.Location = new System.Drawing.Point(0, 125);
            this.layGrpClosed.Name = "layGrpClosed";
            this.layGrpClosed.Size = new System.Drawing.Size(500, 130);
            this.layGrpClosed.Text = "Closed Details [Read Only - Click button to set values]";
            // 
            // ItemForClosedByName
            // 
            this.ItemForClosedByName.Control = this.ClosedByNameTextEdit;
            this.ItemForClosedByName.CustomizationFormText = "Closed By:";
            this.ItemForClosedByName.Location = new System.Drawing.Point(0, 0);
            this.ItemForClosedByName.Name = "ItemForClosedByName";
            this.ItemForClosedByName.Size = new System.Drawing.Size(476, 24);
            this.ItemForClosedByName.Text = "Closed By:";
            this.ItemForClosedByName.TextSize = new System.Drawing.Size(85, 13);
            // 
            // ItemForClosedDate
            // 
            this.ItemForClosedDate.Control = this.ClosedDateDateEdit;
            this.ItemForClosedDate.CustomizationFormText = "Closed Date:";
            this.ItemForClosedDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForClosedDate.Name = "ItemForClosedDate";
            this.ItemForClosedDate.Size = new System.Drawing.Size(476, 24);
            this.ItemForClosedDate.Text = "Closed Date:";
            this.ItemForClosedDate.TextSize = new System.Drawing.Size(85, 13);
            // 
            // layItemBtnClose
            // 
            this.layItemBtnClose.Control = this.btnClose;
            this.layItemBtnClose.CustomizationFormText = "layItemBtnClose";
            this.layItemBtnClose.Location = new System.Drawing.Point(259, 58);
            this.layItemBtnClose.MaxSize = new System.Drawing.Size(100, 0);
            this.layItemBtnClose.MinSize = new System.Drawing.Size(100, 26);
            this.layItemBtnClose.Name = "layItemBtnClose";
            this.layItemBtnClose.Size = new System.Drawing.Size(100, 26);
            this.layItemBtnClose.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layItemBtnClose.TextSize = new System.Drawing.Size(0, 0);
            this.layItemBtnClose.TextVisible = false;
            // 
            // layItemBtnReOpen
            // 
            this.layItemBtnReOpen.Control = this.btnReOpen;
            this.layItemBtnReOpen.CustomizationFormText = "layItemBtnReOpen";
            this.layItemBtnReOpen.Location = new System.Drawing.Point(359, 58);
            this.layItemBtnReOpen.MaxSize = new System.Drawing.Size(117, 26);
            this.layItemBtnReOpen.MinSize = new System.Drawing.Size(117, 26);
            this.layItemBtnReOpen.Name = "layItemBtnReOpen";
            this.layItemBtnReOpen.Size = new System.Drawing.Size(117, 26);
            this.layItemBtnReOpen.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layItemBtnReOpen.TextSize = new System.Drawing.Size(0, 0);
            this.layItemBtnReOpen.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(476, 10);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 58);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(259, 26);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem7";
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 115);
            this.emptySpaceItem7.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem7.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(500, 10);
            this.emptySpaceItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.sp_HR_00002_Employee_Depts_Worked_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00013_Get_Employee_Address_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00017_Pension_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00019_Get_Employee_Next_Of_Kin_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00023_Master_Salary_Bandings_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00040_Employee_Bonus_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00085_Master_Holiday_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00093_Business_Area_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00095_Location_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00097_Department_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00100_Region_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00102_Department_Location_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00132_Employee_Vetting_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00137_Employee_Vetting_Restriction_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00143_Master_Vetting_Types_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00145_Master_Vetting_SubType_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00148_Master_Vetting_Restriction_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00155_Employee_Working_Pattern_Headers_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00164_Master_Working_Pattern_Headers_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00166_Master_Working_Pattern_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00175_Employee_Pension_Contribution_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00182_Linked_Document_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00194_Employee_CRM_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00201_Employee_Reference_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00214_Employee_PayRise_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00226_Employee_Shares_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00232_Master_Qualification_Types_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00250_Employee_Administer_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00255_Business_Area_Insurance_Requirement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00258_Employee_Subsistence_EditTableAdapter = null;
            this.tableAdapterManager.sp09002_HR_Employee_ItemTableAdapter = null;
            this.tableAdapterManager.sp09105_HR_Qualification_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_HR_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter
            // 
            this.sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Sanction_Action_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(520, 403);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimumSize = new System.Drawing.Size(387, 264);
            this.Name = "frm_HR_Sanction_Action_Edit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Sanction Action";
            this.Activated += new System.EventHandler(this.frm_HR_Sanction_Action_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Sanction_Action_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Sanction_Action_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StartedByIdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00058GetEmployeeSanctionProgressItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActionDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressDetailMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartedByNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedByNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClosedDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEditx.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSanctionId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartedById)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartedByName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForProgressDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpClosed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedByName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClosedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layItemBtnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layItemBtnReOpen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private WoodPlanDataSet woodPlanDataSet;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSanctionId;
        private DevExpress.XtraLayout.LayoutControlItem ItemForId;
        private DataSet_HR_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource spHR00058GetEmployeeSanctionProgressItemsBindingSource;
        private DevExpress.XtraEditors.DateEdit ActionDateDateEdit;
        private DevExpress.XtraEditors.MemoEdit ProgressDetailMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActionDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForProgressDetail;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnReOpen;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.TextEdit StartedByNameTextEdit;
        private DevExpress.XtraEditors.TextEdit ClosedByNameTextEdit;
        private DevExpress.XtraEditors.DateEdit ClosedDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartedByName;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpClosed;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClosedByName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClosedDate;
        private DevExpress.XtraLayout.LayoutControlItem layItemBtnClose;
        private DevExpress.XtraLayout.LayoutControlItem layItemBtnReOpen;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.TextEdit IdTextEditx;
        private DevExpress.XtraEditors.TextEdit IdTextEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraEditors.TextEdit StartedByIdTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartedById;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class frm_HR_Business_Area_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public string strPassedInRecordDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private Utils.enmBusinessAreaGrids _enmFocusedGrid = Utils.enmBusinessAreaGrids.BusinessArea;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateBusinessArea;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateDepartment;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateRegion;
        public RefreshGridState RefreshGridViewStateDepartmentLocation;
        public RefreshGridState RefreshGridViewStateLocation;
        public RefreshGridState RefreshGridViewStateInsurance;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedBusinessAreaIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedDepartmentIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRegionIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedDepartmentLocationIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedLocationIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedInsuranceIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        #endregion

        public frm_HR_Business_Area_Manager()
        {
            InitializeComponent();
        }

        private void frm_HR_Business_Area_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 9903;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            sp_HR_00088_Business_Areas_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateBusinessArea = new RefreshGridState(gvwBusinessArea, "BusinessAreaID");

            sp_HR_00089_Business_Area_Manager_Linked_DepartmentsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateDepartment = new RefreshGridState(gvwDepartment, "DepartmentID");

            sp_HR_00090_Business_Area_Manager_Linked_Department_LocationsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateDepartmentLocation = new RefreshGridState(gvwDepartmentLocation, "DepartmentLocationID");

            sp_HR_00091_Business_Area_Manager_Linked_RegionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateRegion = new RefreshGridState(gvwRegion, "RegionID");

            sp_HR_00092_Business_Areas_Manager_LocationsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateLocation = new RefreshGridState(gvwLocation, "LocationID");

            sp_HR_00254_Business_Areas_Insurance_RequirementsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00254_Business_Areas_Insurance_Requirements);
            RefreshGridViewStateInsurance = new RefreshGridState(gridViewInsurance, "BusinessAreaInsuranceRequirementID");

            Load_Business_Areas();
            Load_Locations();
            Load_Insurance();

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

             _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_HR_Business_Area_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedBusinessAreaIDs))
                {
                    Load_Business_Areas();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedDepartmentIDs) || !string.IsNullOrEmpty(i_str_AddedRegionIDs))
                {
                    Load_Linked_Departments();
                }
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedDepartmentLocationIDs))
                {
                    Load_Linked_Department_Locations();
                }
                if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedLocationIDs))
                {
                    Load_Locations();
                }
                if (UpdateRefreshStatus == 5 || !string.IsNullOrEmpty(i_str_AddedInsuranceIDs))
                {
                    Load_Insurance();
                }
            }
            SetMenuStatus();
        }

        private void frm_HR_Business_Area_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        public void LoadLastSavedUserScreenSettings()
        {
        }


        private void Load_Business_Areas()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            RefreshGridViewStateBusinessArea.SaveViewInfo();
            grdBusinessArea.BeginUpdate();
            sp_HR_00088_Business_Areas_ManagerTableAdapter.Fill(dataSet_HR_Core.sp_HR_00088_Business_Areas_Manager);
            this.RefreshGridViewStateBusinessArea.LoadViewInfo();  // Reload any expanded groups and selected rows //
            grdBusinessArea.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedBusinessAreaIDs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedBusinessAreaIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)grdBusinessArea.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["BusinessAreaID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedBusinessAreaIDs = "";
            }
        }

        private void Load_Locations()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            RefreshGridViewStateLocation.SaveViewInfo();  // Store expanded groups and selected rows //
            grdLocation.BeginUpdate();
            sp_HR_00092_Business_Areas_Manager_LocationsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00092_Business_Areas_Manager_Locations);
            RefreshGridViewStateLocation.LoadViewInfo();  // Reload any expanded groups and selected rows //
            grdLocation.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedLocationIDs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedLocationIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)grdLocation.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LocationID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedLocationIDs = "";
            }
        }

        private void Load_Linked_Departments()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)grdBusinessArea.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["BusinessAreaID"])) + ',';
            }
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Departments //
            grdDepartment.MainView.BeginUpdate();
            this.RefreshGridViewStateDepartment.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00089_Business_Area_Manager_Linked_Departments.Clear();
            }
            else
            {
                sp_HR_00089_Business_Area_Manager_Linked_DepartmentsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00089_Business_Area_Manager_Linked_Departments, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewStateDepartment.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            grdDepartment.MainView.EndUpdate();
            // Highlight any recently added new rows //
            if (i_str_AddedDepartmentIDs != "")
            {
                strArray = i_str_AddedDepartmentIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)grdDepartment.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["DepartmentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedDepartmentIDs = "";
            }

            // Populate Linked Region Records //
            grdRegion.MainView.BeginUpdate();
            RefreshGridViewStateRegion.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00091_Business_Area_Manager_Linked_Regions.Clear();
            }
            else
            {
                sp_HR_00091_Business_Area_Manager_Linked_RegionsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00091_Business_Area_Manager_Linked_Regions, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateRegion.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            grdRegion.MainView.EndUpdate();

            if (i_str_AddedRegionIDs != "")
            {
                strArray = i_str_AddedRegionIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)grdRegion.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["RegionID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRegionIDs = "";
            }
        }

        private void Load_Linked_Department_Locations()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            var view = (GridView)grdDepartment.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellDisplayText(intRowHandle, view.Columns["DepartmentID"]).ToString() + ",");
            }
            strSelectedIDs = sb.ToString();
            grdDepartmentLocation.MainView.BeginUpdate();
            this.RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_HR_Core.sp_HR_00090_Business_Area_Manager_Linked_Department_Locations.Clear();
            }
            else
            {
                sp_HR_00090_Business_Area_Manager_Linked_Department_LocationsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00090_Business_Area_Manager_Linked_Department_Locations, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateDepartmentLocation.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            grdDepartmentLocation.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedDepartmentLocationIDs != "")
            {
                strArray = i_str_AddedDepartmentLocationIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)grdDepartmentLocation.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["DepartmentLocationID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedDepartmentLocationIDs = "";
            }
        }

        private void Load_Insurance()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            this.RefreshGridViewStateInsurance.SaveViewInfo();  // Reload any expanded groups and selected rows //
            gridControlInsurance.BeginUpdate();
            sp_HR_00254_Business_Areas_Insurance_RequirementsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00254_Business_Areas_Insurance_Requirements);
            this.RefreshGridViewStateInsurance.LoadViewInfo();  // Reload any expanded groups and selected rows //
            gridControlInsurance.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedInsuranceIDs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedInsuranceIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControlInsurance.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["BusinessAreaInsuranceRequirementID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedInsuranceIDs = "";
            }
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmBusinessAreaGrids grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmBusinessAreaGrids.BusinessArea:
                    i_str_AddedBusinessAreaIDs = newIds;
                    break;
                case Utils.enmBusinessAreaGrids.Department:
                    i_str_AddedDepartmentIDs = newIds;
                    break;
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    i_str_AddedDepartmentLocationIDs = newIds;
                    break;
                case Utils.enmBusinessAreaGrids.Region:
                    i_str_AddedRegionIDs = newIds;
                    break;
                case Utils.enmBusinessAreaGrids.Location:
                    i_str_AddedLocationIDs = newIds;
                    break;
                case Utils.enmBusinessAreaGrids.Insurance:
                    i_str_AddedInsuranceIDs = newIds;
                    break;
                default:
                    break;
            }
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    //case 1:  // Linked GBM Mobile Map Links //
                    //    if (sfpPermissions.blCreate)
                    //    {
                    //        iBool_AllowAddGBMLink = true;
                    //    }
                    //    if (sfpPermissions.blUpdate)
                    //    {
                    //        iBool_AllowEditGBMLink = true;
                    //    }
                    //    if (sfpPermissions.blDelete)
                    //    {
                    //        iBool_AllowDeleteGBMLink = true;
                    //    }
                    //    break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)grdBusinessArea.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmBusinessAreaGrids.BusinessArea:
                    view = (GridView)grdBusinessArea.MainView;
                    break;
                case Utils.enmBusinessAreaGrids.Department:
                    view = (GridView)grdDepartment.MainView;
                    break;
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    view = (GridView)grdDepartmentLocation.MainView;
                    break;
                case Utils.enmBusinessAreaGrids.Region:
                    view = (GridView)grdRegion.MainView;
                    break;
                case Utils.enmBusinessAreaGrids.Location:
                    view = (GridView)grdLocation.MainView;
                    break;
                case Utils.enmBusinessAreaGrids.Insurance:
                    view = (GridView)gridControlInsurance.MainView;
                    break;
                default:
                    break;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmBusinessAreaGrids.BusinessArea:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Department:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView ParentView = (GridView)grdDepartment.MainView;
                        int[] intParentRowHandles;
                        intParentRowHandles = ParentView.GetSelectedRows();
                        bbiBlockAdd.Enabled = intParentRowHandles.Length > 0;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Region:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Location:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Insurance:
                    if (iBool_AllowAdd)
                    {
                        //alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        //bsiAdd.Enabled = true;
                        //bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        //alItems.Add("iDelete");
                        //bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            view = (GridView)grdBusinessArea.MainView;
            intRowHandles = view.GetSelectedRows();
            grdBusinessArea.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            grdBusinessArea.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            grdBusinessArea.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            grdBusinessArea.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            
            view = (GridView)grdDepartment.MainView;
            intRowHandles = view.GetSelectedRows();
            grdDepartment.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            grdDepartment.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));      //? true : false
            grdDepartment.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));       //(intRowHandles.Length > 0 ? true : false);
            grdDepartment.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)grdDepartmentLocation.MainView;
            intRowHandles = view.GetSelectedRows();
            grdDepartmentLocation.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            grdDepartmentLocation.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            grdDepartmentLocation.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            grdDepartmentLocation.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)grdRegion.MainView;
            intRowHandles = view.GetSelectedRows();
            grdRegion.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            grdRegion.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            grdRegion.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            grdRegion.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)grdLocation.MainView;
            intRowHandles = view.GetSelectedRows();
            grdLocation.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            grdLocation.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            grdLocation.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            grdLocation.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)gridControlInsurance.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlInsurance.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlInsurance.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = intRowHandles.Length > 0;
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmBusinessAreaGrids.BusinessArea:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)grdBusinessArea.MainView;
                        view.PostEditor();
                        RefreshGridViewStateBusinessArea.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateRegion.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Business_Area_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Department:
                    {
                        //if (!iBool_AllowAddGBMLink) return;
                        if (!iBool_AllowAdd) return;

                        view = (GridView)grdDepartment.MainView;
                        view.PostEditor();
                        this.RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Department_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)grdBusinessArea.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.ParentRecordId = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "BusinessAreaID"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)grdDepartmentLocation.MainView;
                        view.PostEditor();
                        this.RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateRegion.SaveViewInfo();
                        var fChildForm = new frm_HR_Department_Location_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)grdDepartment.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.ParentRecordId = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "DepartmentID"));
                        }

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Region:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)grdRegion.MainView;
                        view.PostEditor();
                        this.RefreshGridViewStateRegion.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Region_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)grdBusinessArea.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.ParentRecordId = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "BusinessAreaID"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Location:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)grdLocation.MainView;
                        view.PostEditor();
                        this.RefreshGridViewStateLocation.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Location_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)grdDepartment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Get the Locations to be Block Added //
                        string strSelectIDs = "";
                        var fChildForm2 = new frm_HR_Select_Location();
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2._Mode = "multiple";
                        if (fChildForm2.ShowDialog() != DialogResult.OK) return;
                        strSelectIDs = fChildForm2.strSelectedLocationIDs;
                        if (string.IsNullOrEmpty(strSelectIDs)) return;

                        // Add the Locations to the database //
                        string strNewIDs = "";
                        int intDepartmentID = 0;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            intDepartmentID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "DepartmentID"));

                            using (var AddRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                AddRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    strNewIDs += AddRecords.sp_HR_00107_Department_Location_Block_Add(intDepartmentID, strSelectIDs, null).ToString();
                                }
                                catch (Exception) { }
                            }
                            if (!string.IsNullOrEmpty(strNewIDs))
                            {
                                i_str_AddedDepartmentLocationIDs = strNewIDs;
                                Load_Linked_Department_Locations();
                            }
                        }
                    }
                    break;
               default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            StringBuilder sb = new StringBuilder();
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmBusinessAreaGrids.BusinessArea:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdBusinessArea.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BusinessAreaID")) + ',';
                        }
                        RefreshGridViewStateBusinessArea.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateRegion.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Business_Area_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Department:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdDepartment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "DepartmentID")) + ',';
                        }
                        this.RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Department_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdDepartmentLocation.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "DepartmentLocationID")) + ',';
                        }
                        this.RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Department_Location_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Region:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdRegion.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RegionID")) + ',';
                        }
                        this.RefreshGridViewStateRegion.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Region_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Location:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdLocation.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LocationID")) + ',';
                        }
                        this.RefreshGridViewStateLocation.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Location_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Insurance:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlInsurance.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "BusinessAreaInsuranceRequirementID").ToString() + ",");
                        }
                        strRecordIDs = sb.ToString();
                        var fChildForm = new frm_HR_Business_Area_Insurance_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            StringBuilder sb = new StringBuilder();
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmBusinessAreaGrids.BusinessArea:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdBusinessArea.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BusinessAreaID")) + ',';
                        }
                        RefreshGridViewStateBusinessArea.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateRegion.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Business_Area_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Department:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdDepartment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "DepartmentID")) + ',';
                        }
                        this.RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State //
                        this.RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Department_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdDepartmentLocation.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "DepartmentLocationID")) + ',';
                        }
                        this.RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Department_Location_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Region:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdRegion.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RegionID")) + ',';
                        }
                        this.RefreshGridViewStateRegion.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Region_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Location:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdLocation.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LocationID")) + ',';
                        }
                        this.RefreshGridViewStateLocation.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Location_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Insurance:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlInsurance.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "BusinessAreaInsuranceRequirementID").ToString() + ",");
                        }
                        strRecordIDs = sb.ToString();
                        var fChildForm = new frm_HR_Business_Area_Insurance_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            switch (_enmFocusedGrid)
            {
                case Utils.enmBusinessAreaGrids.BusinessArea:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)grdBusinessArea.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Business Areas to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Business Area" : Convert.ToString(intRowHandles.Length) + " Business Areas") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Business Area" : "these Business Areas") + " will no longer be available for selection and any related records will also be deleted!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "BusinessAreaID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewStateBusinessArea.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("business_area", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Business_Areas();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Department:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)grdDepartment.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Departments to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Departments" : Convert.ToString(intRowHandles.Length) + " Linked Departments") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Department" : "these Departments") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "DepartmentID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            this.RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewStateRegion.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("department", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Linked_Departments();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)grdDepartmentLocation.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Department Locations to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Department Location" : Convert.ToString(intRowHandles.Length) + " Linked Department Locations") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Department Location" : "these Linked Department Locations") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "DepartmentLocationID")) + ",";
                            }

                            RefreshGridViewStateDepartmentLocation.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("department_location", strRecordIDs);
                                }
                                catch (Exception) { }
                            }
                            Load_Linked_Departments();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Region:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)grdRegion.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Regions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Region" : Convert.ToString(intRowHandles.Length) + " Linked Regions") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Region" : "these Linked Regions") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "RegionID")) + ",";
                            }

                            this.RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            this.RefreshGridViewStateRegion.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("region", strRecordIDs);
                                }
                                catch (Exception) { }
                            }
                            Load_Linked_Departments();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Location:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)grdLocation.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Locations to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Location" : Convert.ToString(intRowHandles.Length) + " Locations") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Location" : "these Locations") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "LocationID")) + ",";
                            }

                            this.RefreshGridViewStateLocation.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("location", strRecordIDs);
                                }
                                catch (Exception) { }
                            }
                            Load_Locations();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Insurance:
                    {
                        /*if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlInsurance.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Insurance Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Insurance Record" : Convert.ToString(intRowHandles.Length) + " Insurance Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Insurance Record" : "these Insurance Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            StringBuilder sb = new StringBuilder();
                            foreach (int intRowHandle in intRowHandles)
                            {
                                sb.Append(view.GetRowCellDisplayText(intRowHandle, "BusinessAreaInsuranceRequirementID").ToString() + ",");
                            }

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("business_area_insurance", sb.ToString());
                                }
                                catch (Exception) { }
                            }
                            Load_Insurance();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }*/
                    }
                    break;
                default:
                    break;
            }            
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            StringBuilder sb = new StringBuilder();
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmBusinessAreaGrids.BusinessArea:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdBusinessArea.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BusinessAreaID")) + ',';
                        }
                        var fChildForm = new frm_HR_Business_Area_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Department:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdDepartment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "DepartmentID")) + ',';
                        }
                        var fChildForm = new frm_HR_Department_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.DepartmentLocation:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdDepartmentLocation.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "DepartmentLocationID")) + ',';
                        }
                        var fChildForm = new frm_HR_Department_Location_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Region:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdRegion.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "RegionID")) + ',';
                        }
                        var fChildForm = new frm_HR_Region_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Location:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)grdLocation.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LocationID")) + ',';
                        }
                        var fChildForm = new frm_HR_Location_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmBusinessAreaGrids.Insurance:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlInsurance.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            sb.Append(view.GetRowCellValue(intRowHandle, "BusinessAreaInsuranceRequirementID").ToString() + ",");
                        }
                        strRecordIDs = sb.ToString();
                        var fChildForm = new frm_HR_Business_Area_Insurance_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "grdBusinessArea":
                    message = "No Business Areas - Adjust any filters";
                    break;
                case "grdDepartment":
                    message = "No Departments - Select one or more Business Areas to see linked Departments";
                    break;
                case "grdRegion":
                    message = "No Regions - Select one or more Business Areas to see linked Regions";
                    break;
                case "grdDepartmentLocation":
                    message = "No Department Locations - Select one or more Departments to see linked Department Locations";
                    break;
                case "grdLocation":
                    message = "No Locations - Adjust any filters";
                    break;
                case "gridViewInsurance":
                    message = "No Insurance - Adjust any filters";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "grdBusinessArea":
                    Load_Linked_Departments();
                    view = (GridView)grdDepartment.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)grdRegion.MainView;
                    view.ExpandAllGroups();
                    break;
                case "grdDepartment":
                    Load_Linked_Department_Locations();
                    view = (GridView)grdDepartmentLocation.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView Business Area

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private void grdBusinessArea_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void grdBusinessArea_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.BusinessArea;
            SetMenuStatus();
        }

        private void grdBusinessArea_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.BusinessArea;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void grdBusinessArea_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    GridView view = gvwBusinessArea;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        RefreshGridViewStateBusinessArea.SaveViewInfo();  // Store Grid View State //
                        Load_Business_Areas();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView Department

        private void grdDepartment_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    GridView view = (GridView)grdDepartment.MainView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void grdDepartment_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void grdDepartment_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.Department;
            SetMenuStatus();
        }

        private void grdDepartment_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.Department;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView Region

        private void grdRegion_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    GridView view = (GridView)grdRegion.MainView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void grdRegion_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void grdRegion_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.Region;
            SetMenuStatus();
        }

        private void grdRegion_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.Region;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion

 
        #region GridView Department Location

        private void grdDepartmentLocation_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void grdDepartmentLocation_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void grdDepartmentLocation_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.DepartmentLocation;
            SetMenuStatus();
        }

        private void grdDepartmentLocation_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.DepartmentLocation;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView Location

        private void grdLocation_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Locations();
                    }
                    break;
                default:
                    break;
            }
        }

        private void grdLocation_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void grdLocation_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.Location;
            SetMenuStatus();
        }

        private void grdLocation_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.Location;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView Insurance

        private void gridControlInsurance_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Insurance();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewInsurance_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewInsurance_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.Insurance;
            SetMenuStatus();
        }

        private void gridViewInsurance_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmBusinessAreaGrids.Insurance;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion




 




    }

}


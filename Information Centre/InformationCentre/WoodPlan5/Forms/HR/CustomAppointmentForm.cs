#region Note
/*
{**************************************************************************************************************}
{  This file is automatically created when you open the Scheduler Control smart tag                            }
{  *and click Create Customizable Appointment Dialog.                                                          }
{  It contains a a descendant of the default appointment editing form created by visual inheritance.           }
{  In Visual Studio Designer add an editor that is required to edit your appointment custom field.             }
{  Modify the LoadFormData method to get data from a custom field and fill your editor with data.              }
{  Modify the SaveFormData method to retrieve data from the editor and set the appointment custom field value. }
{  The code that displays this form is automatically inserted                                                  }
{  *in the EditAppointmentFormShowing event handler of the SchedulerControl.                                   }
{                                                                                                              }
{**************************************************************************************************************}
*/
#endregion Note

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraScheduler;

namespace WoodPlan5.Forms.HR
{
    public partial class CustomAppointmentForm : DevExpress.XtraScheduler.UI.AppointmentForm
    {
        //private int _BreakLength;
        //private int _PaidForBreaks;
        //private decimal _CalculatedHours;

        public CustomAppointmentForm()
        {
            InitializeComponent();
        }
        public CustomAppointmentForm(DevExpress.XtraScheduler.SchedulerControl control, DevExpress.XtraScheduler.Appointment apt)
            : base(control, apt)
        {
            InitializeComponent();
        }
        public CustomAppointmentForm(DevExpress.XtraScheduler.SchedulerControl control, DevExpress.XtraScheduler.Appointment apt, bool openRecurrenceForm)
            : base(control, apt, openRecurrenceForm)
        {
            InitializeComponent();
        }
        /// <summary>
        /// Add your code to obtain a custom field value and fill the editor with data.
        /// </summary>
        public override void LoadFormData(DevExpress.XtraScheduler.Appointment appointment)
        {
            if (appointment.CustomFields["BreakLength"] == null)
                tbBreakLength.Text = "0";
            else
            {
                //_BreakLength = Convert.ToInt32(appointment.CustomFields["BreakLength"]);
                tbBreakLength.Text = Convert.ToInt32(appointment.CustomFields["BreakLength"]).ToString();
            }

            if (appointment.CustomFields["PaidForBreaks"] == null) cePaidForBreaks.Checked = false;
            else cePaidForBreaks.Checked = Convert.ToInt32(appointment.CustomFields["PaidForBreaks"].ToString()) == 1;

            if (appointment.CustomFields["CalculatedHours"] == null) tbCalculatedHours.Text = "0.00";
            else tbCalculatedHours.Text = Convert.ToDecimal(appointment.CustomFields["CalculatedHours"]).ToString();

            if (appointment.CustomFields["EmployeeName"] == null) tbEmployeeName.Text = "";
            else tbEmployeeName.Text = appointment.CustomFields["EmployeeName"].ToString();

            if (appointment.CustomFields["LocationName"] == null) tbLocationName.Text = "";
            else tbLocationName.Text = appointment.CustomFields["LocationName"].ToString();

            if (appointment.CustomFields["BusinessAreaName"] == null) tbBusinessAreaName.Text = "";
            else tbBusinessAreaName.Text = appointment.CustomFields["BusinessAreaName"].ToString();

            if (appointment.CustomFields["RegionName"] == null) tbRegionName.Text = "";
            else tbRegionName.Text = appointment.CustomFields["RegionName"].ToString();
            
            base.LoadFormData(appointment);
        }
        /// <summary>
        /// Add your code to retrieve a value from the editor and set the custom appointment field.
        /// </summary>
        public override bool SaveFormData(DevExpress.XtraScheduler.Appointment appointment)
        {
            //appointment.CustomFields["BreakLength"] = Convert.ToInt32(tbBreakLength.Text);
            //appointment.CustomFields["PaidForBreaks"] = (cePaidForBreaks.Checked ? 1 : 0);
            //appointment.CustomFields["CalculatedHours"] = Convert.ToDecimal(tbCalculatedHours.Text);
            return base.SaveFormData(appointment);
        }
        /// <summary>
        /// Add your code to notify that any custom field is changed. Return true if a custom field is changed, otherwise false.
        /// </summary>
        public override bool IsAppointmentChanged(DevExpress.XtraScheduler.Appointment appointment)
        {
           // if (_BreakLength != Convert.ToInt32(appointment.CustomFields["BreakLength"])
           //     || _PaidForBreaks != (cePaidForBreaks.Checked ? 1 : 0)
           //     || _CalculatedHours != Convert.ToDecimal(appointment.CustomFields["CalculatedHours"]))
           //     return true;
           // else
           //     return false;
            return false;
        }

     }
}

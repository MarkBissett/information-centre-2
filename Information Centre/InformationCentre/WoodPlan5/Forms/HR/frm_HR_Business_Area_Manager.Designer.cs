namespace WoodPlan5
{
    partial class frm_HR_Business_Area_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Business_Area_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.grdBusinessArea = new DevExpress.XtraGrid.GridControl();
            this.spHR00088BusinessAreasManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gvwBusinessArea = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBusinessAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageContracts = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.grdDepartment = new DevExpress.XtraGrid.GridControl();
            this.spHR00089BusinessAreaManagerLinkedDepartmentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvwDepartment = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.grdDepartmentLocation = new DevExpress.XtraGrid.GridControl();
            this.spHR00090BusinessAreaManagerLinkedDepartmentLocationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvwDepartmentLocation = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageDiscipline = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer9 = new DevExpress.XtraGrid.GridSplitContainer();
            this.grdRegion = new DevExpress.XtraGrid.GridControl();
            this.spHR00091BusinessAreaManagerLinkedRegionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvwRegion = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.grdLocation = new DevExpress.XtraGrid.GridControl();
            this.spHR00092BusinessAreasManagerLocationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gvwLocation = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddress5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending4 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.beiShowActiveHolidayYearOnly = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.xtraTabControl3 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlInsurance = new DevExpress.XtraGrid.GridControl();
            this.spHR00254BusinessAreasInsuranceRequirementsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewInsurance = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBusinessAreaInsuranceRequirementID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinPublicInsuranceValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMinEmployersInsuranceValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp_HR_00088_Business_Areas_ManagerTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00088_Business_Areas_ManagerTableAdapter();
            this.sp_HR_00089_Business_Area_Manager_Linked_DepartmentsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00089_Business_Area_Manager_Linked_DepartmentsTableAdapter();
            this.sp_HR_00090_Business_Area_Manager_Linked_Department_LocationsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00090_Business_Area_Manager_Linked_Department_LocationsTableAdapter();
            this.sp_HR_00091_Business_Area_Manager_Linked_RegionsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00091_Business_Area_Manager_Linked_RegionsTableAdapter();
            this.sp_HR_00092_Business_Areas_Manager_LocationsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00092_Business_Areas_Manager_LocationsTableAdapter();
            this.xtraGridBlending6 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00254_Business_Areas_Insurance_RequirementsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00254_Business_Areas_Insurance_RequirementsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdBusinessArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00088BusinessAreasManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwBusinessArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageContracts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00089BusinessAreaManagerLinkedDepartmentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdDepartmentLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00090BusinessAreaManagerLinkedDepartmentLocationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwDepartmentLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).BeginInit();
            this.xtraTabPageDiscipline.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer9)).BeginInit();
            this.gridSplitContainer9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00091BusinessAreaManagerLinkedRegionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwRegion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00092BusinessAreasManagerLocationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).BeginInit();
            this.xtraTabControl3.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInsurance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00254BusinessAreasInsuranceRequirementsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInsurance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(859, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 438);
            this.barDockControlBottom.Size = new System.Drawing.Size(859, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 438);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(859, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 438);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beiShowActiveHolidayYearOnly,
            this.barButtonItem1});
            this.barManager1.MaxItemId = 31;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 8;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemHyperLinkEdit6
            // 
            this.repositoryItemHyperLinkEdit6.AutoHeight = false;
            this.repositoryItemHyperLinkEdit6.Name = "repositoryItemHyperLinkEdit6";
            this.repositoryItemHyperLinkEdit6.SingleClick = true;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // repositoryItemDateEdit6
            // 
            this.repositoryItemDateEdit6.AutoHeight = false;
            this.repositoryItemDateEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit6.Name = "repositoryItemDateEdit6";
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ReadOnly = true;
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(854, 412);
            this.splitContainerControl1.SplitterPosition = 252;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.grdBusinessArea;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.grdBusinessArea);
            this.gridSplitContainer1.Size = new System.Drawing.Size(854, 154);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // grdBusinessArea
            // 
            this.grdBusinessArea.DataSource = this.spHR00088BusinessAreasManagerBindingSource;
            this.grdBusinessArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.grdBusinessArea.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdBusinessArea.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Refresh Data", "reload")});
            this.grdBusinessArea.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.grdBusinessArea_EmbeddedNavigator_ButtonClick);
            this.grdBusinessArea.Location = new System.Drawing.Point(0, 0);
            this.grdBusinessArea.MainView = this.gvwBusinessArea;
            this.grdBusinessArea.MenuManager = this.barManager1;
            this.grdBusinessArea.Name = "grdBusinessArea";
            this.grdBusinessArea.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.grdBusinessArea.Size = new System.Drawing.Size(854, 154);
            this.grdBusinessArea.TabIndex = 0;
            this.grdBusinessArea.UseEmbeddedNavigator = true;
            this.grdBusinessArea.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvwBusinessArea});
            // 
            // spHR00088BusinessAreasManagerBindingSource
            // 
            this.spHR00088BusinessAreasManagerBindingSource.DataMember = "sp_HR_00088_Business_Areas_Manager";
            this.spHR00088BusinessAreasManagerBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gvwBusinessArea
            // 
            this.gvwBusinessArea.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBusinessAreaID,
            this.colBusinessAreaName,
            this.colRemarks});
            this.gvwBusinessArea.GridControl = this.grdBusinessArea;
            this.gvwBusinessArea.Name = "gvwBusinessArea";
            this.gvwBusinessArea.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvwBusinessArea.OptionsLayout.Columns.StoreAllOptions = true;
            this.gvwBusinessArea.OptionsLayout.StoreAppearance = true;
            this.gvwBusinessArea.OptionsLayout.StoreFormatRules = true;
            this.gvwBusinessArea.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gvwBusinessArea.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gvwBusinessArea.OptionsSelection.MultiSelect = true;
            this.gvwBusinessArea.OptionsView.AllowHtmlDrawHeaders = true;
            this.gvwBusinessArea.OptionsView.ColumnAutoWidth = false;
            this.gvwBusinessArea.OptionsView.EnableAppearanceEvenRow = true;
            this.gvwBusinessArea.OptionsView.ShowGroupPanel = false;
            this.gvwBusinessArea.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gvwBusinessArea.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gvwBusinessArea.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gvwBusinessArea.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gvwBusinessArea.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gvwBusinessArea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdBusinessArea_MouseUp);
            this.gvwBusinessArea.DoubleClick += new System.EventHandler(this.grdBusinessArea_DoubleClick);
            this.gvwBusinessArea.GotFocus += new System.EventHandler(this.grdBusinessArea_GotFocus);
            // 
            // colBusinessAreaID
            // 
            this.colBusinessAreaID.Caption = "Business Area ID";
            this.colBusinessAreaID.FieldName = "BusinessAreaID";
            this.colBusinessAreaID.Name = "colBusinessAreaID";
            this.colBusinessAreaID.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID.Width = 102;
            // 
            // colBusinessAreaName
            // 
            this.colBusinessAreaName.Caption = "Business Area";
            this.colBusinessAreaName.FieldName = "BusinessAreaName";
            this.colBusinessAreaName.Name = "colBusinessAreaName";
            this.colBusinessAreaName.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName.Visible = true;
            this.colBusinessAreaName.VisibleIndex = 0;
            this.colBusinessAreaName.Width = 224;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 1;
            this.colRemarks.Width = 175;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageContracts;
            this.xtraTabControl1.Size = new System.Drawing.Size(854, 252);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageContracts,
            this.xtraTabPageDiscipline});
            // 
            // xtraTabPageContracts
            // 
            this.xtraTabPageContracts.Controls.Add(this.splitContainerControl2);
            this.xtraTabPageContracts.Name = "xtraTabPageContracts";
            this.xtraTabPageContracts.Size = new System.Drawing.Size(849, 226);
            this.xtraTabPageContracts.Text = "Departments";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer4);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridSplitContainer3);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Locations Linked To Selected Departments";
            this.splitContainerControl2.Size = new System.Drawing.Size(849, 226);
            this.splitContainerControl2.SplitterPosition = 443;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.grdDepartment;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.grdDepartment);
            this.gridSplitContainer4.Size = new System.Drawing.Size(400, 226);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // grdDepartment
            // 
            this.grdDepartment.DataSource = this.spHR00089BusinessAreaManagerLinkedDepartmentsBindingSource;
            this.grdDepartment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDepartment.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.grdDepartment.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.grdDepartment.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdDepartment.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.grdDepartment.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.grdDepartment_EmbeddedNavigator_ButtonClick);
            this.grdDepartment.Location = new System.Drawing.Point(0, 0);
            this.grdDepartment.MainView = this.gvwDepartment;
            this.grdDepartment.MenuManager = this.barManager1;
            this.grdDepartment.Name = "grdDepartment";
            this.grdDepartment.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.grdDepartment.Size = new System.Drawing.Size(400, 226);
            this.grdDepartment.TabIndex = 0;
            this.grdDepartment.UseEmbeddedNavigator = true;
            this.grdDepartment.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvwDepartment});
            // 
            // spHR00089BusinessAreaManagerLinkedDepartmentsBindingSource
            // 
            this.spHR00089BusinessAreaManagerLinkedDepartmentsBindingSource.DataMember = "sp_HR_00089_Business_Area_Manager_Linked_Departments";
            this.spHR00089BusinessAreaManagerLinkedDepartmentsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gvwDepartment
            // 
            this.gvwDepartment.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepartmentID,
            this.colBusinessAreaID1,
            this.colDepartmentName,
            this.gridColumn1,
            this.colBusinessAreaName1});
            this.gvwDepartment.GridControl = this.grdDepartment;
            this.gvwDepartment.GroupCount = 1;
            this.gvwDepartment.Name = "gvwDepartment";
            this.gvwDepartment.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvwDepartment.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvwDepartment.OptionsLayout.Columns.StoreAllOptions = true;
            this.gvwDepartment.OptionsLayout.StoreAppearance = true;
            this.gvwDepartment.OptionsLayout.StoreFormatRules = true;
            this.gvwDepartment.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gvwDepartment.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gvwDepartment.OptionsSelection.MultiSelect = true;
            this.gvwDepartment.OptionsView.AllowHtmlDrawHeaders = true;
            this.gvwDepartment.OptionsView.ColumnAutoWidth = false;
            this.gvwDepartment.OptionsView.EnableAppearanceEvenRow = true;
            this.gvwDepartment.OptionsView.ShowGroupPanel = false;
            this.gvwDepartment.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBusinessAreaName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartmentName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gvwDepartment.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gvwDepartment.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gvwDepartment.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gvwDepartment.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gvwDepartment.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gvwDepartment.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdDepartment_MouseUp);
            this.gvwDepartment.DoubleClick += new System.EventHandler(this.grdDepartment_DoubleClick);
            this.gvwDepartment.GotFocus += new System.EventHandler(this.grdDepartment_GotFocus);
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.Caption = "Department ID";
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.Width = 92;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.Width = 102;
            // 
            // colDepartmentName
            // 
            this.colDepartmentName.Caption = "Department Name";
            this.colDepartmentName.FieldName = "DepartmentName";
            this.colDepartmentName.Name = "colDepartmentName";
            this.colDepartmentName.OptionsColumn.AllowEdit = false;
            this.colDepartmentName.OptionsColumn.AllowFocus = false;
            this.colDepartmentName.OptionsColumn.ReadOnly = true;
            this.colDepartmentName.Visible = true;
            this.colDepartmentName.VisibleIndex = 0;
            this.colDepartmentName.Width = 224;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Remarks";
            this.gridColumn1.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn1.FieldName = "Remarks";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            this.gridColumn1.Width = 175;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area Name";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.Width = 224;
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.grdDepartmentLocation;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.grdDepartmentLocation);
            this.gridSplitContainer3.Size = new System.Drawing.Size(439, 202);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // grdDepartmentLocation
            // 
            this.grdDepartmentLocation.DataSource = this.spHR00090BusinessAreaManagerLinkedDepartmentLocationsBindingSource;
            this.grdDepartmentLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.grdDepartmentLocation.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdDepartmentLocation.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record(s)", "view")});
            this.grdDepartmentLocation.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.grdDepartmentLocation_EmbeddedNavigator_ButtonClick);
            this.grdDepartmentLocation.Location = new System.Drawing.Point(0, 0);
            this.grdDepartmentLocation.MainView = this.gvwDepartmentLocation;
            this.grdDepartmentLocation.MenuManager = this.barManager1;
            this.grdDepartmentLocation.Name = "grdDepartmentLocation";
            this.grdDepartmentLocation.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit3});
            this.grdDepartmentLocation.Size = new System.Drawing.Size(439, 202);
            this.grdDepartmentLocation.TabIndex = 1;
            this.grdDepartmentLocation.UseEmbeddedNavigator = true;
            this.grdDepartmentLocation.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvwDepartmentLocation});
            // 
            // spHR00090BusinessAreaManagerLinkedDepartmentLocationsBindingSource
            // 
            this.spHR00090BusinessAreaManagerLinkedDepartmentLocationsBindingSource.DataMember = "sp_HR_00090_Business_Area_Manager_Linked_Department_Locations";
            this.spHR00090BusinessAreaManagerLinkedDepartmentLocationsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gvwDepartmentLocation
            // 
            this.gvwDepartmentLocation.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.colDepartmentLocationID,
            this.gridColumn7,
            this.gridColumn8});
            this.gvwDepartmentLocation.GridControl = this.grdDepartmentLocation;
            this.gvwDepartmentLocation.GroupCount = 2;
            this.gvwDepartmentLocation.Name = "gvwDepartmentLocation";
            this.gvwDepartmentLocation.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvwDepartmentLocation.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvwDepartmentLocation.OptionsLayout.Columns.StoreAllOptions = true;
            this.gvwDepartmentLocation.OptionsLayout.StoreAppearance = true;
            this.gvwDepartmentLocation.OptionsLayout.StoreFormatRules = true;
            this.gvwDepartmentLocation.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gvwDepartmentLocation.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gvwDepartmentLocation.OptionsSelection.MultiSelect = true;
            this.gvwDepartmentLocation.OptionsView.AllowHtmlDrawHeaders = true;
            this.gvwDepartmentLocation.OptionsView.ColumnAutoWidth = false;
            this.gvwDepartmentLocation.OptionsView.EnableAppearanceEvenRow = true;
            this.gvwDepartmentLocation.OptionsView.ShowGroupPanel = false;
            this.gvwDepartmentLocation.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gvwDepartmentLocation.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gvwDepartmentLocation.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gvwDepartmentLocation.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gvwDepartmentLocation.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gvwDepartmentLocation.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gvwDepartmentLocation.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdDepartmentLocation_MouseUp);
            this.gvwDepartmentLocation.DoubleClick += new System.EventHandler(this.grdDepartmentLocation_DoubleClick);
            this.gvwDepartmentLocation.GotFocus += new System.EventHandler(this.grdDepartmentLocation_GotFocus);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Department ID";
            this.gridColumn2.FieldName = "DepartmentID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 92;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Business Area ID";
            this.gridColumn3.FieldName = "BusinessAreaID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 102;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Department Name";
            this.gridColumn4.FieldName = "DepartmentName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 224;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Remarks";
            this.gridColumn5.ColumnEdit = this.repositoryItemMemoEdit3;
            this.gridColumn5.FieldName = "Remarks";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 175;
            // 
            // repositoryItemMemoEdit3
            // 
            this.repositoryItemMemoEdit3.Name = "repositoryItemMemoEdit3";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Business Area Name";
            this.gridColumn6.FieldName = "BusinessAreaName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 224;
            // 
            // colDepartmentLocationID
            // 
            this.colDepartmentLocationID.Caption = "Department Location ID";
            this.colDepartmentLocationID.FieldName = "DepartmentLocationID";
            this.colDepartmentLocationID.Name = "colDepartmentLocationID";
            this.colDepartmentLocationID.OptionsColumn.AllowEdit = false;
            this.colDepartmentLocationID.OptionsColumn.AllowFocus = false;
            this.colDepartmentLocationID.OptionsColumn.ReadOnly = true;
            this.colDepartmentLocationID.Width = 135;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Location ID";
            this.gridColumn7.FieldName = "LocationID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Location Name";
            this.gridColumn8.FieldName = "LocationName";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 224;
            // 
            // xtraTabPageDiscipline
            // 
            this.xtraTabPageDiscipline.AutoScroll = true;
            this.xtraTabPageDiscipline.Controls.Add(this.gridSplitContainer9);
            this.xtraTabPageDiscipline.Name = "xtraTabPageDiscipline";
            this.xtraTabPageDiscipline.Size = new System.Drawing.Size(849, 226);
            this.xtraTabPageDiscipline.Text = "Regions";
            // 
            // gridSplitContainer9
            // 
            this.gridSplitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer9.Grid = this.grdRegion;
            this.gridSplitContainer9.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer9.Name = "gridSplitContainer9";
            this.gridSplitContainer9.Panel1.Controls.Add(this.grdRegion);
            this.gridSplitContainer9.Size = new System.Drawing.Size(849, 226);
            this.gridSplitContainer9.TabIndex = 0;
            // 
            // grdRegion
            // 
            this.grdRegion.DataSource = this.spHR00091BusinessAreaManagerLinkedRegionsBindingSource;
            this.grdRegion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdRegion.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.grdRegion.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdRegion.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.grdRegion.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdRegion.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.grdRegion.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdRegion.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.grdRegion.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdRegion.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.grdRegion.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.grdRegion.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdRegion.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view")});
            this.grdRegion.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.grdRegion_EmbeddedNavigator_ButtonClick);
            this.grdRegion.Location = new System.Drawing.Point(0, 0);
            this.grdRegion.MainView = this.gvwRegion;
            this.grdRegion.MenuManager = this.barManager1;
            this.grdRegion.Name = "grdRegion";
            this.grdRegion.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5});
            this.grdRegion.Size = new System.Drawing.Size(849, 226);
            this.grdRegion.TabIndex = 0;
            this.grdRegion.UseEmbeddedNavigator = true;
            this.grdRegion.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvwRegion});
            // 
            // spHR00091BusinessAreaManagerLinkedRegionsBindingSource
            // 
            this.spHR00091BusinessAreaManagerLinkedRegionsBindingSource.DataMember = "sp_HR_00091_Business_Area_Manager_Linked_Regions";
            this.spHR00091BusinessAreaManagerLinkedRegionsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gvwRegion
            // 
            this.gvwRegion.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn13,
            this.colRegionName,
            this.colRemarks2});
            this.gvwRegion.GridControl = this.grdRegion;
            this.gvwRegion.GroupCount = 1;
            this.gvwRegion.Name = "gvwRegion";
            this.gvwRegion.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvwRegion.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvwRegion.OptionsLayout.Columns.StoreAllOptions = true;
            this.gvwRegion.OptionsLayout.StoreAppearance = true;
            this.gvwRegion.OptionsLayout.StoreFormatRules = true;
            this.gvwRegion.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gvwRegion.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gvwRegion.OptionsSelection.MultiSelect = true;
            this.gvwRegion.OptionsView.AllowHtmlDrawHeaders = true;
            this.gvwRegion.OptionsView.ColumnAutoWidth = false;
            this.gvwRegion.OptionsView.EnableAppearanceEvenRow = true;
            this.gvwRegion.OptionsView.ShowGroupPanel = false;
            this.gvwRegion.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn13, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRegionName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gvwRegion.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gvwRegion.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gvwRegion.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gvwRegion.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gvwRegion.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gvwRegion.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdRegion_MouseUp);
            this.gvwRegion.DoubleClick += new System.EventHandler(this.grdRegion_DoubleClick);
            this.gvwRegion.GotFocus += new System.EventHandler(this.grdRegion_GotFocus);
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Region ID";
            this.gridColumn9.FieldName = "RegionID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Width = 92;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Business Area ID";
            this.gridColumn10.FieldName = "BusinessAreaID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 102;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Business Area Name";
            this.gridColumn13.FieldName = "BusinessAreaName";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 224;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region Name";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 0;
            this.colRegionName.Width = 224;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 1;
            this.colRemarks2.Width = 125;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // grdLocation
            // 
            this.grdLocation.DataSource = this.spHR00092BusinessAreasManagerLocationsBindingSource;
            this.grdLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdLocation.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.grdLocation.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.grdLocation.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.grdLocation.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.grdLocation.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.grdLocation.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.grdLocation.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.grdLocation.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.grdLocation.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.grdLocation.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.grdLocation.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.grdLocation.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Refresh Data", "reload")});
            this.grdLocation.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.grdLocation_EmbeddedNavigator_ButtonClick);
            this.grdLocation.Location = new System.Drawing.Point(0, 0);
            this.grdLocation.MainView = this.gvwLocation;
            this.grdLocation.MenuManager = this.barManager1;
            this.grdLocation.Name = "grdLocation";
            this.grdLocation.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemCheckEdit1});
            this.grdLocation.Size = new System.Drawing.Size(854, 412);
            this.grdLocation.TabIndex = 0;
            this.grdLocation.UseEmbeddedNavigator = true;
            this.grdLocation.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvwLocation});
            // 
            // spHR00092BusinessAreasManagerLocationsBindingSource
            // 
            this.spHR00092BusinessAreasManagerLocationsBindingSource.DataMember = "sp_HR_00092_Business_Areas_Manager_Locations";
            this.spHR00092BusinessAreasManagerLocationsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gvwLocation
            // 
            this.gvwLocation.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLocationID,
            this.colLocationName,
            this.colAddress1,
            this.colAddress2,
            this.colAddress3,
            this.colAddress4,
            this.colAddress5,
            this.colPostcode,
            this.colTelephone,
            this.colActive,
            this.colRemarks1});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colActive;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gvwLocation.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gvwLocation.GridControl = this.grdLocation;
            this.gvwLocation.Name = "gvwLocation";
            this.gvwLocation.OptionsBehavior.AutoExpandAllGroups = true;
            this.gvwLocation.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gvwLocation.OptionsLayout.Columns.StoreAllOptions = true;
            this.gvwLocation.OptionsLayout.StoreAppearance = true;
            this.gvwLocation.OptionsLayout.StoreFormatRules = true;
            this.gvwLocation.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gvwLocation.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gvwLocation.OptionsSelection.MultiSelect = true;
            this.gvwLocation.OptionsView.AllowHtmlDrawHeaders = true;
            this.gvwLocation.OptionsView.ColumnAutoWidth = false;
            this.gvwLocation.OptionsView.EnableAppearanceEvenRow = true;
            this.gvwLocation.OptionsView.ShowGroupPanel = false;
            this.gvwLocation.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLocationName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gvwLocation.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gvwLocation.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gvwLocation.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gvwLocation.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gvwLocation.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gvwLocation.MouseUp += new System.Windows.Forms.MouseEventHandler(this.grdLocation_MouseUp);
            this.gvwLocation.DoubleClick += new System.EventHandler(this.grdLocation_DoubleClick);
            this.gvwLocation.GotFocus += new System.EventHandler(this.grdLocation_GotFocus);
            // 
            // colLocationID
            // 
            this.colLocationID.Caption = "Location ID";
            this.colLocationID.FieldName = "LocationID";
            this.colLocationID.Name = "colLocationID";
            this.colLocationID.OptionsColumn.AllowEdit = false;
            this.colLocationID.OptionsColumn.AllowFocus = false;
            this.colLocationID.OptionsColumn.ReadOnly = true;
            // 
            // colLocationName
            // 
            this.colLocationName.Caption = "Name";
            this.colLocationName.FieldName = "LocationName";
            this.colLocationName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.OptionsColumn.AllowEdit = false;
            this.colLocationName.OptionsColumn.AllowFocus = false;
            this.colLocationName.OptionsColumn.ReadOnly = true;
            this.colLocationName.Visible = true;
            this.colLocationName.VisibleIndex = 0;
            this.colLocationName.Width = 156;
            // 
            // colAddress1
            // 
            this.colAddress1.Caption = "Address Line 1";
            this.colAddress1.FieldName = "Address1";
            this.colAddress1.Name = "colAddress1";
            this.colAddress1.OptionsColumn.AllowEdit = false;
            this.colAddress1.OptionsColumn.AllowFocus = false;
            this.colAddress1.OptionsColumn.ReadOnly = true;
            this.colAddress1.Visible = true;
            this.colAddress1.VisibleIndex = 1;
            this.colAddress1.Width = 110;
            // 
            // colAddress2
            // 
            this.colAddress2.Caption = "Address Line 2";
            this.colAddress2.FieldName = "Address2";
            this.colAddress2.Name = "colAddress2";
            this.colAddress2.OptionsColumn.AllowEdit = false;
            this.colAddress2.OptionsColumn.AllowFocus = false;
            this.colAddress2.OptionsColumn.ReadOnly = true;
            this.colAddress2.Visible = true;
            this.colAddress2.VisibleIndex = 2;
            this.colAddress2.Width = 110;
            // 
            // colAddress3
            // 
            this.colAddress3.Caption = "Address Line 3";
            this.colAddress3.FieldName = "Address3";
            this.colAddress3.Name = "colAddress3";
            this.colAddress3.OptionsColumn.AllowEdit = false;
            this.colAddress3.OptionsColumn.AllowFocus = false;
            this.colAddress3.OptionsColumn.ReadOnly = true;
            this.colAddress3.Visible = true;
            this.colAddress3.VisibleIndex = 3;
            this.colAddress3.Width = 110;
            // 
            // colAddress4
            // 
            this.colAddress4.Caption = "Address Line 4";
            this.colAddress4.FieldName = "Address4";
            this.colAddress4.Name = "colAddress4";
            this.colAddress4.OptionsColumn.AllowEdit = false;
            this.colAddress4.OptionsColumn.AllowFocus = false;
            this.colAddress4.OptionsColumn.ReadOnly = true;
            this.colAddress4.Visible = true;
            this.colAddress4.VisibleIndex = 4;
            this.colAddress4.Width = 110;
            // 
            // colAddress5
            // 
            this.colAddress5.Caption = "Address Line 5";
            this.colAddress5.FieldName = "Address5";
            this.colAddress5.Name = "colAddress5";
            this.colAddress5.OptionsColumn.AllowEdit = false;
            this.colAddress5.OptionsColumn.AllowFocus = false;
            this.colAddress5.OptionsColumn.ReadOnly = true;
            this.colAddress5.Visible = true;
            this.colAddress5.VisibleIndex = 5;
            this.colAddress5.Width = 110;
            // 
            // colPostcode
            // 
            this.colPostcode.Caption = "Postcode";
            this.colPostcode.FieldName = "Postcode";
            this.colPostcode.Name = "colPostcode";
            this.colPostcode.OptionsColumn.AllowEdit = false;
            this.colPostcode.OptionsColumn.AllowFocus = false;
            this.colPostcode.OptionsColumn.ReadOnly = true;
            this.colPostcode.Visible = true;
            this.colPostcode.VisibleIndex = 6;
            // 
            // colTelephone
            // 
            this.colTelephone.Caption = "Telephone";
            this.colTelephone.FieldName = "Telephone";
            this.colTelephone.Name = "colTelephone";
            this.colTelephone.OptionsColumn.AllowEdit = false;
            this.colTelephone.OptionsColumn.AllowFocus = false;
            this.colTelephone.OptionsColumn.ReadOnly = true;
            this.colTelephone.Visible = true;
            this.colTelephone.VisibleIndex = 7;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 9;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.grdBusinessArea;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.grdDepartment;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.grdDepartmentLocation;
            // 
            // xtraGridBlending4
            // 
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending4.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending4.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending4.GridControl = this.grdRegion;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.grdLocation;
            // 
            // beiShowActiveHolidayYearOnly
            // 
            this.beiShowActiveHolidayYearOnly.Caption = "Active Year Only:";
            this.beiShowActiveHolidayYearOnly.Edit = this.repositoryItemCheckEditShowActiveHoilidayYearOnly;
            this.beiShowActiveHolidayYearOnly.EditValue = 1;
            this.beiShowActiveHolidayYearOnly.EditWidth = 20;
            this.beiShowActiveHolidayYearOnly.Id = 27;
            this.beiShowActiveHolidayYearOnly.Name = "beiShowActiveHolidayYearOnly";
            this.beiShowActiveHolidayYearOnly.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Active Year Only - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Tick me to load only the active holiday year data. \r\n\r\nUntick me to load all data" +
    ".";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiShowActiveHolidayYearOnly.SuperTip = superToolTip1;
            // 
            // repositoryItemCheckEditShowActiveHoilidayYearOnly
            // 
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Caption = "Check";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Name = "repositoryItemCheckEditShowActiveHoilidayYearOnly";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueUnchecked = 0;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 28;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // xtraTabControl3
            // 
            this.xtraTabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl3.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl3.Name = "xtraTabControl3";
            this.xtraTabControl3.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl3.Size = new System.Drawing.Size(859, 438);
            this.xtraTabControl3.TabIndex = 5;
            this.xtraTabControl3.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainerControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(854, 412);
            this.xtraTabPage1.Text = "Business Areas";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(854, 412);
            this.xtraTabPage2.Text = "Master Locations";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.grdLocation;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.grdLocation);
            this.gridSplitContainer2.Size = new System.Drawing.Size(854, 412);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControlInsurance);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(854, 412);
            this.xtraTabPage3.Text = "Team Insurance Requirements";
            // 
            // gridControlInsurance
            // 
            this.gridControlInsurance.DataSource = this.spHR00254BusinessAreasInsuranceRequirementsBindingSource;
            this.gridControlInsurance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlInsurance.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlInsurance.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Refresh Data", "reload")});
            this.gridControlInsurance.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInsurance_EmbeddedNavigator_ButtonClick);
            this.gridControlInsurance.Location = new System.Drawing.Point(0, 0);
            this.gridControlInsurance.MainView = this.gridViewInsurance;
            this.gridControlInsurance.MenuManager = this.barManager1;
            this.gridControlInsurance.Name = "gridControlInsurance";
            this.gridControlInsurance.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEditCurrency});
            this.gridControlInsurance.Size = new System.Drawing.Size(854, 412);
            this.gridControlInsurance.TabIndex = 1;
            this.gridControlInsurance.UseEmbeddedNavigator = true;
            this.gridControlInsurance.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInsurance});
            // 
            // spHR00254BusinessAreasInsuranceRequirementsBindingSource
            // 
            this.spHR00254BusinessAreasInsuranceRequirementsBindingSource.DataMember = "sp_HR_00254_Business_Areas_Insurance_Requirements";
            this.spHR00254BusinessAreasInsuranceRequirementsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewInsurance
            // 
            this.gridViewInsurance.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBusinessAreaInsuranceRequirementID,
            this.colBusinessAreaName2,
            this.colRecordTypeID,
            this.colMinPublicInsuranceValue,
            this.colMinEmployersInsuranceValue,
            this.colRemarks3,
            this.colRecordType});
            this.gridViewInsurance.GridControl = this.gridControlInsurance;
            this.gridViewInsurance.Name = "gridViewInsurance";
            this.gridViewInsurance.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewInsurance.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewInsurance.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewInsurance.OptionsLayout.StoreAppearance = true;
            this.gridViewInsurance.OptionsLayout.StoreFormatRules = true;
            this.gridViewInsurance.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewInsurance.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewInsurance.OptionsSelection.MultiSelect = true;
            this.gridViewInsurance.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewInsurance.OptionsView.ColumnAutoWidth = false;
            this.gridViewInsurance.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewInsurance.OptionsView.ShowGroupPanel = false;
            this.gridViewInsurance.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBusinessAreaName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordType, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewInsurance.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewInsurance.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewInsurance.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewInsurance.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewInsurance.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewInsurance.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewInsurance_MouseUp);
            this.gridViewInsurance.DoubleClick += new System.EventHandler(this.gridViewInsurance_DoubleClick);
            this.gridViewInsurance.GotFocus += new System.EventHandler(this.gridViewInsurance_GotFocus);
            // 
            // colBusinessAreaInsuranceRequirementID
            // 
            this.colBusinessAreaInsuranceRequirementID.Caption = "Insurance Requirement ID";
            this.colBusinessAreaInsuranceRequirementID.FieldName = "BusinessAreaInsuranceRequirementID";
            this.colBusinessAreaInsuranceRequirementID.Name = "colBusinessAreaInsuranceRequirementID";
            this.colBusinessAreaInsuranceRequirementID.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaInsuranceRequirementID.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaInsuranceRequirementID.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaInsuranceRequirementID.Width = 145;
            // 
            // colBusinessAreaName2
            // 
            this.colBusinessAreaName2.Caption = "Business Area";
            this.colBusinessAreaName2.FieldName = "BusinessAreaName";
            this.colBusinessAreaName2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBusinessAreaName2.Name = "colBusinessAreaName2";
            this.colBusinessAreaName2.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName2.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName2.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName2.Visible = true;
            this.colBusinessAreaName2.VisibleIndex = 0;
            this.colBusinessAreaName2.Width = 206;
            // 
            // colRecordTypeID
            // 
            this.colRecordTypeID.Caption = "Record Type ID";
            this.colRecordTypeID.FieldName = "RecordTypeID";
            this.colRecordTypeID.Name = "colRecordTypeID";
            this.colRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colRecordTypeID.Width = 94;
            // 
            // colMinPublicInsuranceValue
            // 
            this.colMinPublicInsuranceValue.Caption = "Min Public Value";
            this.colMinPublicInsuranceValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colMinPublicInsuranceValue.FieldName = "MinPublicInsuranceValue";
            this.colMinPublicInsuranceValue.Name = "colMinPublicInsuranceValue";
            this.colMinPublicInsuranceValue.OptionsColumn.AllowEdit = false;
            this.colMinPublicInsuranceValue.OptionsColumn.AllowFocus = false;
            this.colMinPublicInsuranceValue.OptionsColumn.ReadOnly = true;
            this.colMinPublicInsuranceValue.Visible = true;
            this.colMinPublicInsuranceValue.VisibleIndex = 2;
            this.colMinPublicInsuranceValue.Width = 116;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colMinEmployersInsuranceValue
            // 
            this.colMinEmployersInsuranceValue.Caption = "Min Employers Value";
            this.colMinEmployersInsuranceValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colMinEmployersInsuranceValue.FieldName = "MinEmployersInsuranceValue";
            this.colMinEmployersInsuranceValue.Name = "colMinEmployersInsuranceValue";
            this.colMinEmployersInsuranceValue.OptionsColumn.AllowEdit = false;
            this.colMinEmployersInsuranceValue.OptionsColumn.AllowFocus = false;
            this.colMinEmployersInsuranceValue.OptionsColumn.ReadOnly = true;
            this.colMinEmployersInsuranceValue.Visible = true;
            this.colMinEmployersInsuranceValue.VisibleIndex = 3;
            this.colMinEmployersInsuranceValue.Width = 116;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 4;
            this.colRemarks3.Width = 192;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Visible = true;
            this.colRecordType.VisibleIndex = 1;
            this.colRecordType.Width = 126;
            // 
            // sp_HR_00088_Business_Areas_ManagerTableAdapter
            // 
            this.sp_HR_00088_Business_Areas_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00089_Business_Area_Manager_Linked_DepartmentsTableAdapter
            // 
            this.sp_HR_00089_Business_Area_Manager_Linked_DepartmentsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00090_Business_Area_Manager_Linked_Department_LocationsTableAdapter
            // 
            this.sp_HR_00090_Business_Area_Manager_Linked_Department_LocationsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00091_Business_Area_Manager_Linked_RegionsTableAdapter
            // 
            this.sp_HR_00091_Business_Area_Manager_Linked_RegionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00092_Business_Areas_Manager_LocationsTableAdapter
            // 
            this.sp_HR_00092_Business_Areas_Manager_LocationsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending6
            // 
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending6.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending6.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending6.GridControl = this.gridControlInsurance;
            // 
            // sp_HR_00254_Business_Areas_Insurance_RequirementsTableAdapter
            // 
            this.sp_HR_00254_Business_Areas_Insurance_RequirementsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Business_Area_Manager
            // 
            this.ClientSize = new System.Drawing.Size(859, 438);
            this.Controls.Add(this.xtraTabControl3);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Business_Area_Manager";
            this.Text = "Business Area Manager";
            this.Activated += new System.EventHandler(this.frm_HR_Business_Area_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Business_Area_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Business_Area_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl3, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdBusinessArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00088BusinessAreasManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwBusinessArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageContracts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00089BusinessAreaManagerLinkedDepartmentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdDepartmentLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00090BusinessAreaManagerLinkedDepartmentLocationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwDepartmentLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit3)).EndInit();
            this.xtraTabPageDiscipline.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer9)).EndInit();
            this.gridSplitContainer9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00091BusinessAreaManagerLinkedRegionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwRegion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00092BusinessAreasManagerLocationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvwLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).EndInit();
            this.xtraTabControl3.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInsurance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00254BusinessAreasInsuranceRequirementsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInsurance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageContracts;
        private DevExpress.XtraGrid.GridControl grdBusinessArea;
        private DevExpress.XtraGrid.Views.Grid.GridView gvwBusinessArea;
        private DevExpress.XtraGrid.GridControl grdLocation;
        private DevExpress.XtraGrid.Views.Grid.GridView gvwLocation;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl grdDepartment;
        private DevExpress.XtraGrid.Views.Grid.GridView gvwDepartment;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageDiscipline;
        private DevExpress.XtraGrid.GridControl grdRegion;
        private DevExpress.XtraGrid.Views.Grid.GridView gvwRegion;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl grdDepartmentLocation;
        private DevExpress.XtraGrid.Views.Grid.GridView gvwDepartmentLocation;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending4;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer9;
        private DevExpress.XtraBars.BarEditItem beiShowActiveHolidayYearOnly;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveHoilidayYearOnly;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private System.Windows.Forms.BindingSource spHR00088BusinessAreasManagerBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00088_Business_Areas_ManagerTableAdapter sp_HR_00088_Business_Areas_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private System.Windows.Forms.BindingSource spHR00089BusinessAreaManagerLinkedDepartmentsBindingSource;
        private System.Windows.Forms.BindingSource spHR00090BusinessAreaManagerLinkedDepartmentLocationsBindingSource;
        private System.Windows.Forms.BindingSource spHR00091BusinessAreaManagerLinkedRegionsBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00089_Business_Area_Manager_Linked_DepartmentsTableAdapter sp_HR_00089_Business_Area_Manager_Linked_DepartmentsTableAdapter;
        private DataSet_HR_CoreTableAdapters.sp_HR_00090_Business_Area_Manager_Linked_Department_LocationsTableAdapter sp_HR_00090_Business_Area_Manager_Linked_Department_LocationsTableAdapter;
        private DataSet_HR_CoreTableAdapters.sp_HR_00091_Business_Area_Manager_Linked_RegionsTableAdapter sp_HR_00091_Business_Area_Manager_Linked_RegionsTableAdapter;
        private System.Windows.Forms.BindingSource spHR00092BusinessAreasManagerLocationsBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00092_Business_Areas_Manager_LocationsTableAdapter sp_HR_00092_Business_Areas_Manager_LocationsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddress5;
        private DevExpress.XtraGrid.Columns.GridColumn colPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControlInsurance;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInsurance;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending6;
        private System.Windows.Forms.BindingSource spHR00254BusinessAreasInsuranceRequirementsBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00254_Business_Areas_Insurance_RequirementsTableAdapter sp_HR_00254_Business_Areas_Insurance_RequirementsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaInsuranceRequirementID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMinPublicInsuranceValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colMinEmployersInsuranceValue;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
    }
}

namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Pension
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00177PensionsForEmployeeSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPensionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSchemeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSchemeTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContributionTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMatchedContribution = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumContributionAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMaximumContributionAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentContributionAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProviderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFixedInitialTermMonths = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMonths = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalEmployeeContributionToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalEmployerContributionToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSchemeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContributionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProvider = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContributionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00177_Pensions_For_Employee_SelectTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00177_Pensions_For_Employee_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00177PensionsForEmployeeSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMonths)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive1.FieldName = "Active";
            this.colActive1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 1;
            this.colActive1.Width = 64;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00177PensionsForEmployeeSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditMonths,
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00177PensionsForEmployeeSelectBindingSource
            // 
            this.spHR00177PensionsForEmployeeSelectBindingSource.DataMember = "sp_HR_00177_Pensions_For_Employee_Select";
            this.spHR00177PensionsForEmployeeSelectBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPensionID,
            this.colEmployeeID9,
            this.colSchemeNumber,
            this.colSchemeTypeID,
            this.colPayrollTypeID,
            this.colContributionTypeID,
            this.colMatchedContribution,
            this.colMinimumContributionAmount,
            this.colMaximumContributionAmount,
            this.colCurrentContributionAmount,
            this.colProviderID,
            this.colStartDate2,
            this.colEndDate2,
            this.colFixedInitialTermMonths,
            this.colTotalEmployeeContributionToDate,
            this.colTotalEmployerContributionToDate,
            this.colRemarks7,
            this.colEmployeeName5,
            this.colEmployeeSurname6,
            this.colEmployeeFirstname4,
            this.colEmployeeNumber9,
            this.colSchemeType,
            this.colPayrollType1,
            this.colContributionType,
            this.colProvider,
            this.colActive1,
            this.colContributionCount});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActive1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.BestFitMaxRowCount = 5;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActive1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate2, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colPensionID
            // 
            this.colPensionID.Caption = "Pension ID";
            this.colPensionID.FieldName = "PensionID";
            this.colPensionID.Name = "colPensionID";
            this.colPensionID.OptionsColumn.AllowEdit = false;
            this.colPensionID.OptionsColumn.AllowFocus = false;
            this.colPensionID.OptionsColumn.ReadOnly = true;
            this.colPensionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEmployeeID9
            // 
            this.colEmployeeID9.Caption = "Employee ID";
            this.colEmployeeID9.FieldName = "EmployeeID";
            this.colEmployeeID9.Name = "colEmployeeID9";
            this.colEmployeeID9.OptionsColumn.AllowEdit = false;
            this.colEmployeeID9.OptionsColumn.AllowFocus = false;
            this.colEmployeeID9.OptionsColumn.ReadOnly = true;
            this.colEmployeeID9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID9.Width = 81;
            // 
            // colSchemeNumber
            // 
            this.colSchemeNumber.Caption = "Scheme #";
            this.colSchemeNumber.FieldName = "SchemeNumber";
            this.colSchemeNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSchemeNumber.Name = "colSchemeNumber";
            this.colSchemeNumber.OptionsColumn.AllowEdit = false;
            this.colSchemeNumber.OptionsColumn.AllowFocus = false;
            this.colSchemeNumber.OptionsColumn.ReadOnly = true;
            this.colSchemeNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSchemeNumber.Visible = true;
            this.colSchemeNumber.VisibleIndex = 0;
            this.colSchemeNumber.Width = 110;
            // 
            // colSchemeTypeID
            // 
            this.colSchemeTypeID.Caption = "Scheme Type ID";
            this.colSchemeTypeID.FieldName = "SchemeTypeID";
            this.colSchemeTypeID.Name = "colSchemeTypeID";
            this.colSchemeTypeID.OptionsColumn.AllowEdit = false;
            this.colSchemeTypeID.OptionsColumn.AllowFocus = false;
            this.colSchemeTypeID.OptionsColumn.ReadOnly = true;
            this.colSchemeTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSchemeTypeID.Width = 99;
            // 
            // colPayrollTypeID
            // 
            this.colPayrollTypeID.Caption = "Payroll Type ID";
            this.colPayrollTypeID.FieldName = "PayrollTypeID";
            this.colPayrollTypeID.Name = "colPayrollTypeID";
            this.colPayrollTypeID.OptionsColumn.AllowEdit = false;
            this.colPayrollTypeID.OptionsColumn.AllowFocus = false;
            this.colPayrollTypeID.OptionsColumn.ReadOnly = true;
            this.colPayrollTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPayrollTypeID.Width = 94;
            // 
            // colContributionTypeID
            // 
            this.colContributionTypeID.Caption = "Contribution Type ID";
            this.colContributionTypeID.FieldName = "ContributionTypeID";
            this.colContributionTypeID.Name = "colContributionTypeID";
            this.colContributionTypeID.OptionsColumn.AllowEdit = false;
            this.colContributionTypeID.OptionsColumn.AllowFocus = false;
            this.colContributionTypeID.OptionsColumn.ReadOnly = true;
            this.colContributionTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContributionTypeID.Width = 121;
            // 
            // colMatchedContribution
            // 
            this.colMatchedContribution.Caption = "Matched Contribution";
            this.colMatchedContribution.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colMatchedContribution.FieldName = "MatchedContribution";
            this.colMatchedContribution.Name = "colMatchedContribution";
            this.colMatchedContribution.OptionsColumn.AllowEdit = false;
            this.colMatchedContribution.OptionsColumn.AllowFocus = false;
            this.colMatchedContribution.OptionsColumn.ReadOnly = true;
            this.colMatchedContribution.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMatchedContribution.Visible = true;
            this.colMatchedContribution.VisibleIndex = 9;
            this.colMatchedContribution.Width = 124;
            // 
            // colMinimumContributionAmount
            // 
            this.colMinimumContributionAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colMinimumContributionAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colMinimumContributionAmount.Caption = "Min Contribution";
            this.colMinimumContributionAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colMinimumContributionAmount.FieldName = "MinimumContributionAmount";
            this.colMinimumContributionAmount.Name = "colMinimumContributionAmount";
            this.colMinimumContributionAmount.OptionsColumn.AllowEdit = false;
            this.colMinimumContributionAmount.OptionsColumn.AllowFocus = false;
            this.colMinimumContributionAmount.OptionsColumn.ReadOnly = true;
            this.colMinimumContributionAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMinimumContributionAmount.Visible = true;
            this.colMinimumContributionAmount.VisibleIndex = 10;
            this.colMinimumContributionAmount.Width = 99;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colMaximumContributionAmount
            // 
            this.colMaximumContributionAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colMaximumContributionAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colMaximumContributionAmount.Caption = "Max Contribution";
            this.colMaximumContributionAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colMaximumContributionAmount.FieldName = "MaximumContributionAmount";
            this.colMaximumContributionAmount.Name = "colMaximumContributionAmount";
            this.colMaximumContributionAmount.OptionsColumn.AllowEdit = false;
            this.colMaximumContributionAmount.OptionsColumn.AllowFocus = false;
            this.colMaximumContributionAmount.OptionsColumn.ReadOnly = true;
            this.colMaximumContributionAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colMaximumContributionAmount.Visible = true;
            this.colMaximumContributionAmount.VisibleIndex = 11;
            this.colMaximumContributionAmount.Width = 103;
            // 
            // colCurrentContributionAmount
            // 
            this.colCurrentContributionAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colCurrentContributionAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colCurrentContributionAmount.Caption = "Current Contribution";
            this.colCurrentContributionAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCurrentContributionAmount.FieldName = "CurrentContributionAmount";
            this.colCurrentContributionAmount.Name = "colCurrentContributionAmount";
            this.colCurrentContributionAmount.OptionsColumn.AllowEdit = false;
            this.colCurrentContributionAmount.OptionsColumn.AllowFocus = false;
            this.colCurrentContributionAmount.OptionsColumn.ReadOnly = true;
            this.colCurrentContributionAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentContributionAmount.Visible = true;
            this.colCurrentContributionAmount.VisibleIndex = 12;
            this.colCurrentContributionAmount.Width = 120;
            // 
            // colProviderID
            // 
            this.colProviderID.Caption = "Provider ID";
            this.colProviderID.FieldName = "ProviderID";
            this.colProviderID.Name = "colProviderID";
            this.colProviderID.OptionsColumn.AllowEdit = false;
            this.colProviderID.OptionsColumn.AllowFocus = false;
            this.colProviderID.OptionsColumn.ReadOnly = true;
            this.colProviderID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStartDate2
            // 
            this.colStartDate2.Caption = "Start Date";
            this.colStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate2.FieldName = "StartDate";
            this.colStartDate2.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate2.Name = "colStartDate2";
            this.colStartDate2.OptionsColumn.AllowEdit = false;
            this.colStartDate2.OptionsColumn.AllowFocus = false;
            this.colStartDate2.OptionsColumn.ReadOnly = true;
            this.colStartDate2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate2.Visible = true;
            this.colStartDate2.VisibleIndex = 5;
            this.colStartDate2.Width = 84;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEndDate2
            // 
            this.colEndDate2.Caption = "End Date";
            this.colEndDate2.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate2.FieldName = "EndDate";
            this.colEndDate2.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate2.Name = "colEndDate2";
            this.colEndDate2.OptionsColumn.AllowEdit = false;
            this.colEndDate2.OptionsColumn.AllowFocus = false;
            this.colEndDate2.OptionsColumn.ReadOnly = true;
            this.colEndDate2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate2.Visible = true;
            this.colEndDate2.VisibleIndex = 6;
            // 
            // colFixedInitialTermMonths
            // 
            this.colFixedInitialTermMonths.Caption = "Fixed Initial Term";
            this.colFixedInitialTermMonths.ColumnEdit = this.repositoryItemTextEditMonths;
            this.colFixedInitialTermMonths.FieldName = "FixedInitialTermMonths";
            this.colFixedInitialTermMonths.Name = "colFixedInitialTermMonths";
            this.colFixedInitialTermMonths.OptionsColumn.AllowEdit = false;
            this.colFixedInitialTermMonths.OptionsColumn.AllowFocus = false;
            this.colFixedInitialTermMonths.OptionsColumn.ReadOnly = true;
            this.colFixedInitialTermMonths.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFixedInitialTermMonths.Visible = true;
            this.colFixedInitialTermMonths.VisibleIndex = 7;
            this.colFixedInitialTermMonths.Width = 103;
            // 
            // repositoryItemTextEditMonths
            // 
            this.repositoryItemTextEditMonths.AutoHeight = false;
            this.repositoryItemTextEditMonths.Mask.EditMask = "#####0 Months";
            this.repositoryItemTextEditMonths.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMonths.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMonths.Name = "repositoryItemTextEditMonths";
            // 
            // colTotalEmployeeContributionToDate
            // 
            this.colTotalEmployeeContributionToDate.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalEmployeeContributionToDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTotalEmployeeContributionToDate.Caption = "Total Employee Contributions";
            this.colTotalEmployeeContributionToDate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalEmployeeContributionToDate.FieldName = "TotalEmployeeContributionToDate";
            this.colTotalEmployeeContributionToDate.Name = "colTotalEmployeeContributionToDate";
            this.colTotalEmployeeContributionToDate.OptionsColumn.AllowEdit = false;
            this.colTotalEmployeeContributionToDate.OptionsColumn.AllowFocus = false;
            this.colTotalEmployeeContributionToDate.OptionsColumn.ReadOnly = true;
            this.colTotalEmployeeContributionToDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTotalEmployeeContributionToDate.Visible = true;
            this.colTotalEmployeeContributionToDate.VisibleIndex = 13;
            this.colTotalEmployeeContributionToDate.Width = 161;
            // 
            // colTotalEmployerContributionToDate
            // 
            this.colTotalEmployerContributionToDate.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalEmployerContributionToDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTotalEmployerContributionToDate.Caption = "Total Employer Contributions";
            this.colTotalEmployerContributionToDate.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colTotalEmployerContributionToDate.FieldName = "TotalEmployerContributionToDate";
            this.colTotalEmployerContributionToDate.Name = "colTotalEmployerContributionToDate";
            this.colTotalEmployerContributionToDate.OptionsColumn.AllowEdit = false;
            this.colTotalEmployerContributionToDate.OptionsColumn.AllowFocus = false;
            this.colTotalEmployerContributionToDate.OptionsColumn.ReadOnly = true;
            this.colTotalEmployerContributionToDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTotalEmployerContributionToDate.Visible = true;
            this.colTotalEmployerContributionToDate.VisibleIndex = 14;
            this.colTotalEmployerContributionToDate.Width = 159;
            // 
            // colRemarks7
            // 
            this.colRemarks7.Caption = "Remarks";
            this.colRemarks7.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks7.FieldName = "Remarks";
            this.colRemarks7.Name = "colRemarks7";
            this.colRemarks7.OptionsColumn.ReadOnly = true;
            this.colRemarks7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks7.Visible = true;
            this.colRemarks7.VisibleIndex = 16;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colEmployeeName5
            // 
            this.colEmployeeName5.Caption = "Linked To Employee";
            this.colEmployeeName5.FieldName = "EmployeeName";
            this.colEmployeeName5.Name = "colEmployeeName5";
            this.colEmployeeName5.OptionsColumn.AllowEdit = false;
            this.colEmployeeName5.OptionsColumn.AllowFocus = false;
            this.colEmployeeName5.OptionsColumn.ReadOnly = true;
            this.colEmployeeName5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName5.Visible = true;
            this.colEmployeeName5.VisibleIndex = 11;
            this.colEmployeeName5.Width = 215;
            // 
            // colEmployeeSurname6
            // 
            this.colEmployeeSurname6.Caption = "Surname";
            this.colEmployeeSurname6.FieldName = "EmployeeSurname";
            this.colEmployeeSurname6.Name = "colEmployeeSurname6";
            this.colEmployeeSurname6.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname6.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname6.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEmployeeFirstname4
            // 
            this.colEmployeeFirstname4.Caption = "Forename";
            this.colEmployeeFirstname4.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname4.Name = "colEmployeeFirstname4";
            this.colEmployeeFirstname4.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname4.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname4.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEmployeeNumber9
            // 
            this.colEmployeeNumber9.Caption = "Employee #";
            this.colEmployeeNumber9.FieldName = "EmployeeNumber";
            this.colEmployeeNumber9.Name = "colEmployeeNumber9";
            this.colEmployeeNumber9.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber9.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber9.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber9.Width = 78;
            // 
            // colSchemeType
            // 
            this.colSchemeType.Caption = "Scheme Type";
            this.colSchemeType.FieldName = "SchemeType";
            this.colSchemeType.Name = "colSchemeType";
            this.colSchemeType.OptionsColumn.AllowEdit = false;
            this.colSchemeType.OptionsColumn.AllowFocus = false;
            this.colSchemeType.OptionsColumn.ReadOnly = true;
            this.colSchemeType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSchemeType.Visible = true;
            this.colSchemeType.VisibleIndex = 2;
            this.colSchemeType.Width = 109;
            // 
            // colPayrollType1
            // 
            this.colPayrollType1.Caption = "Payroll Type";
            this.colPayrollType1.FieldName = "PayrollType";
            this.colPayrollType1.Name = "colPayrollType1";
            this.colPayrollType1.OptionsColumn.AllowEdit = false;
            this.colPayrollType1.OptionsColumn.AllowFocus = false;
            this.colPayrollType1.OptionsColumn.ReadOnly = true;
            this.colPayrollType1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPayrollType1.Visible = true;
            this.colPayrollType1.VisibleIndex = 4;
            this.colPayrollType1.Width = 116;
            // 
            // colContributionType
            // 
            this.colContributionType.Caption = "Contribution Type";
            this.colContributionType.FieldName = "ContributionType";
            this.colContributionType.Name = "colContributionType";
            this.colContributionType.OptionsColumn.AllowEdit = false;
            this.colContributionType.OptionsColumn.AllowFocus = false;
            this.colContributionType.OptionsColumn.ReadOnly = true;
            this.colContributionType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContributionType.Visible = true;
            this.colContributionType.VisibleIndex = 8;
            this.colContributionType.Width = 122;
            // 
            // colProvider
            // 
            this.colProvider.Caption = "Provider";
            this.colProvider.FieldName = "Provider";
            this.colProvider.Name = "colProvider";
            this.colProvider.OptionsColumn.AllowEdit = false;
            this.colProvider.OptionsColumn.AllowFocus = false;
            this.colProvider.OptionsColumn.ReadOnly = true;
            this.colProvider.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colProvider.Visible = true;
            this.colProvider.VisibleIndex = 3;
            this.colProvider.Width = 138;
            // 
            // colContributionCount
            // 
            this.colContributionCount.Caption = "Contribution Count";
            this.colContributionCount.FieldName = "ContributionCount";
            this.colContributionCount.Name = "colContributionCount";
            this.colContributionCount.OptionsColumn.AllowEdit = false;
            this.colContributionCount.OptionsColumn.AllowFocus = false;
            this.colContributionCount.OptionsColumn.ReadOnly = true;
            this.colContributionCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colContributionCount.Visible = true;
            this.colContributionCount.VisibleIndex = 15;
            this.colContributionCount.Width = 112;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00177_Pensions_For_Employee_SelectTableAdapter
            // 
            this.sp_HR_00177_Pensions_For_Employee_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_Pension
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Pension";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Pension";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Pension_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00177PensionsForEmployeeSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMonths)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private System.Windows.Forms.BindingSource spHR00177PensionsForEmployeeSelectBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPensionID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID9;
        private DevExpress.XtraGrid.Columns.GridColumn colSchemeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSchemeTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContributionTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMatchedContribution;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumContributionAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumContributionAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentContributionAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colProviderID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colFixedInitialTermMonths;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalEmployeeContributionToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalEmployerContributionToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks7;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName5;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname6;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber9;
        private DevExpress.XtraGrid.Columns.GridColumn colSchemeType;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollType1;
        private DevExpress.XtraGrid.Columns.GridColumn colContributionType;
        private DevExpress.XtraGrid.Columns.GridColumn colProvider;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraGrid.Columns.GridColumn colContributionCount;
        private DataSet_HR_CoreTableAdapters.sp_HR_00177_Pensions_For_Employee_SelectTableAdapter sp_HR_00177_Pensions_For_Employee_SelectTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMonths;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
    }
}

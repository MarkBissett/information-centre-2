namespace WoodPlan5
{
    partial class frm_HR_Employee_Bonus_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Bonus_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.BonusAmountPaidSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GuaranteeBonusAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.GuaranteeBonusCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.spHR00040EmployeeBonusEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.BonusDatePaidDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.BonusDueDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EmployeeNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeFirstnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeSurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.EmployeeNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.EmployeeIDSpinEdit = new DevExpress.XtraEditors.TextEdit();
            this.BonusIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForBonusID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeFirstname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGuaranteeBonus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGuaranteeBonusAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBonusDatePaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBonusDueDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForBonusAmountPaid = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00040_Employee_Bonus_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00040_Employee_Bonus_EditTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BonusAmountPaidSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GuaranteeBonusAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GuaranteeBonusCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00040EmployeeBonusEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusDatePaidDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusDatePaidDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusDueDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusDueDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBonusID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGuaranteeBonus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGuaranteeBonusAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBonusDatePaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBonusDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBonusAmountPaid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(553, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 421);
            this.barDockControlBottom.Size = new System.Drawing.Size(553, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 395);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(553, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 395);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(553, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 421);
            this.barDockControl2.Size = new System.Drawing.Size(553, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 395);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(553, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 395);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.BonusAmountPaidSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GuaranteeBonusAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.GuaranteeBonusCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.BonusDatePaidDateEdit);
            this.dataLayoutControl1.Controls.Add(this.BonusDueDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeFirstnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeSurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.BonusIDTextEdit);
            this.dataLayoutControl1.DataSource = this.spHR00040EmployeeBonusEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForBonusID,
            this.ItemForEmployeeID,
            this.ItemForEmployeeFirstname,
            this.ItemForEmployeeNumber,
            this.ItemForEmployeeSurname});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(813, 178, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(553, 395);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // BonusAmountPaidSpinEdit
            // 
            this.BonusAmountPaidSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.BonusAmountPaidSpinEdit.Location = new System.Drawing.Point(148, 174);
            this.BonusAmountPaidSpinEdit.MenuManager = this.barManager1;
            this.BonusAmountPaidSpinEdit.Name = "BonusAmountPaidSpinEdit";
            this.BonusAmountPaidSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BonusAmountPaidSpinEdit.Properties.Mask.EditMask = "c";
            this.BonusAmountPaidSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BonusAmountPaidSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.BonusAmountPaidSpinEdit.Properties.MinValue = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147352576});
            this.BonusAmountPaidSpinEdit.Size = new System.Drawing.Size(393, 20);
            this.BonusAmountPaidSpinEdit.StyleController = this.dataLayoutControl1;
            this.BonusAmountPaidSpinEdit.TabIndex = 45;
            this.BonusAmountPaidSpinEdit.EditValueChanged += new System.EventHandler(this.BonusAmountPaidSpinEdit_EditValueChanged);
            // 
            // GuaranteeBonusAmountSpinEdit
            // 
            this.GuaranteeBonusAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.GuaranteeBonusAmountSpinEdit.Location = new System.Drawing.Point(148, 92);
            this.GuaranteeBonusAmountSpinEdit.MenuManager = this.barManager1;
            this.GuaranteeBonusAmountSpinEdit.Name = "GuaranteeBonusAmountSpinEdit";
            this.GuaranteeBonusAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GuaranteeBonusAmountSpinEdit.Properties.Mask.EditMask = "c";
            this.GuaranteeBonusAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.GuaranteeBonusAmountSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.GuaranteeBonusAmountSpinEdit.Properties.MinValue = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147352576});
            this.GuaranteeBonusAmountSpinEdit.Size = new System.Drawing.Size(393, 20);
            this.GuaranteeBonusAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.GuaranteeBonusAmountSpinEdit.TabIndex = 44;
            this.GuaranteeBonusAmountSpinEdit.EditValueChanged += new System.EventHandler(this.GuaranteeBonusAmountSpinEdit_EditValueChanged);
            // 
            // GuaranteeBonusCheckEdit
            // 
            this.GuaranteeBonusCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "GuaranteeBonus", true));
            this.GuaranteeBonusCheckEdit.Location = new System.Drawing.Point(148, 69);
            this.GuaranteeBonusCheckEdit.MenuManager = this.barManager1;
            this.GuaranteeBonusCheckEdit.Name = "GuaranteeBonusCheckEdit";
            this.GuaranteeBonusCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.GuaranteeBonusCheckEdit.Properties.ValueChecked = 1;
            this.GuaranteeBonusCheckEdit.Properties.ValueUnchecked = 0;
            this.GuaranteeBonusCheckEdit.Size = new System.Drawing.Size(393, 19);
            this.GuaranteeBonusCheckEdit.StyleController = this.dataLayoutControl1;
            this.GuaranteeBonusCheckEdit.TabIndex = 43;
            this.GuaranteeBonusCheckEdit.CheckedChanged += new System.EventHandler(this.GuaranteeBonusCheckEdit_CheckedChanged);
            this.GuaranteeBonusCheckEdit.EditValueChanged += new System.EventHandler(this.GuaranteeBonusCheckEdit_EditValueChanged);
            // 
            // spHR00040EmployeeBonusEditBindingSource
            // 
            this.spHR00040EmployeeBonusEditBindingSource.DataMember = "sp_HR_00040_Employee_Bonus_Edit";
            this.spHR00040EmployeeBonusEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // BonusDatePaidDateEdit
            // 
            this.BonusDatePaidDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "BonusDatePaid", true));
            this.BonusDatePaidDateEdit.EditValue = null;
            this.BonusDatePaidDateEdit.Location = new System.Drawing.Point(148, 150);
            this.BonusDatePaidDateEdit.MenuManager = this.barManager1;
            this.BonusDatePaidDateEdit.Name = "BonusDatePaidDateEdit";
            this.BonusDatePaidDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.BonusDatePaidDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BonusDatePaidDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BonusDatePaidDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BonusDatePaidDateEdit.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.BonusDatePaidDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.BonusDatePaidDateEdit.Size = new System.Drawing.Size(393, 20);
            this.BonusDatePaidDateEdit.StyleController = this.dataLayoutControl1;
            this.BonusDatePaidDateEdit.TabIndex = 42;
            // 
            // BonusDueDateDateEdit
            // 
            this.BonusDueDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "BonusDueDate", true));
            this.BonusDueDateDateEdit.EditValue = null;
            this.BonusDueDateDateEdit.Location = new System.Drawing.Point(148, 126);
            this.BonusDueDateDateEdit.MenuManager = this.barManager1;
            this.BonusDueDateDateEdit.Name = "BonusDueDateDateEdit";
            this.BonusDueDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.BonusDueDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BonusDueDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BonusDueDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BonusDueDateDateEdit.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.BonusDueDateDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.BonusDueDateDateEdit.Size = new System.Drawing.Size(393, 20);
            this.BonusDueDateDateEdit.StyleController = this.dataLayoutControl1;
            this.BonusDueDateDateEdit.TabIndex = 41;
            this.BonusDueDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.BonusDueDateDateEdit_Validating);
            // 
            // EmployeeNumberTextEdit
            // 
            this.EmployeeNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "EmployeeNumber", true));
            this.EmployeeNumberTextEdit.Location = new System.Drawing.Point(114, 69);
            this.EmployeeNumberTextEdit.MenuManager = this.barManager1;
            this.EmployeeNumberTextEdit.Name = "EmployeeNumberTextEdit";
            this.EmployeeNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNumberTextEdit, true);
            this.EmployeeNumberTextEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNumberTextEdit, optionsSpelling1);
            this.EmployeeNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNumberTextEdit.TabIndex = 40;
            // 
            // EmployeeFirstnameTextEdit
            // 
            this.EmployeeFirstnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "EmployeeFirstname", true));
            this.EmployeeFirstnameTextEdit.Location = new System.Drawing.Point(116, 69);
            this.EmployeeFirstnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeFirstnameTextEdit.Name = "EmployeeFirstnameTextEdit";
            this.EmployeeFirstnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeFirstnameTextEdit, true);
            this.EmployeeFirstnameTextEdit.Size = new System.Drawing.Size(425, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeFirstnameTextEdit, optionsSpelling2);
            this.EmployeeFirstnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeFirstnameTextEdit.TabIndex = 39;
            // 
            // EmployeeSurnameTextEdit
            // 
            this.EmployeeSurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "EmployeeSurname", true));
            this.EmployeeSurnameTextEdit.Location = new System.Drawing.Point(114, 69);
            this.EmployeeSurnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeSurnameTextEdit.Name = "EmployeeSurnameTextEdit";
            this.EmployeeSurnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeSurnameTextEdit, true);
            this.EmployeeSurnameTextEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeSurnameTextEdit, optionsSpelling3);
            this.EmployeeSurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeSurnameTextEdit.TabIndex = 38;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 278);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(481, 81);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling4);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00040EmployeeBonusEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(147, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // EmployeeNameButtonEdit
            // 
            this.EmployeeNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "EmployeeName", true));
            this.EmployeeNameButtonEdit.EditValue = "";
            this.EmployeeNameButtonEdit.Location = new System.Drawing.Point(148, 35);
            this.EmployeeNameButtonEdit.MenuManager = this.barManager1;
            this.EmployeeNameButtonEdit.Name = "EmployeeNameButtonEdit";
            this.EmployeeNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Open Select Employee screen", "choose", null, true)});
            this.EmployeeNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EmployeeNameButtonEdit.Size = new System.Drawing.Size(393, 20);
            this.EmployeeNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameButtonEdit.TabIndex = 6;
            this.EmployeeNameButtonEdit.TabStop = false;
            this.EmployeeNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EmployeeNameButtonEdit_ButtonClick);
            this.EmployeeNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EmployeeNameButtonEdit_Validating);
            // 
            // EmployeeIDSpinEdit
            // 
            this.EmployeeIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "EmployeeID", true));
            this.EmployeeIDSpinEdit.EditValue = "";
            this.EmployeeIDSpinEdit.Location = new System.Drawing.Point(114, 69);
            this.EmployeeIDSpinEdit.MenuManager = this.barManager1;
            this.EmployeeIDSpinEdit.Name = "EmployeeIDSpinEdit";
            this.EmployeeIDSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.EmployeeIDSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.EmployeeIDSpinEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIDSpinEdit, true);
            this.EmployeeIDSpinEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIDSpinEdit, optionsSpelling5);
            this.EmployeeIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIDSpinEdit.TabIndex = 25;
            // 
            // BonusIDTextEdit
            // 
            this.BonusIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00040EmployeeBonusEditBindingSource, "BonusID", true));
            this.BonusIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.BonusIDTextEdit.Location = new System.Drawing.Point(114, 69);
            this.BonusIDTextEdit.MenuManager = this.barManager1;
            this.BonusIDTextEdit.Name = "BonusIDTextEdit";
            this.BonusIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.BonusIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.BonusIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BonusIDTextEdit, true);
            this.BonusIDTextEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BonusIDTextEdit, optionsSpelling6);
            this.BonusIDTextEdit.StyleController = this.dataLayoutControl1;
            this.BonusIDTextEdit.TabIndex = 27;
            // 
            // ItemForBonusID
            // 
            this.ItemForBonusID.Control = this.BonusIDTextEdit;
            this.ItemForBonusID.CustomizationFormText = "Bonus ID:";
            this.ItemForBonusID.Location = new System.Drawing.Point(0, 57);
            this.ItemForBonusID.Name = "ItemForBonusID";
            this.ItemForBonusID.Size = new System.Drawing.Size(533, 24);
            this.ItemForBonusID.Text = "Bonus ID:";
            this.ItemForBonusID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForEmployeeID
            // 
            this.ItemForEmployeeID.Control = this.EmployeeIDSpinEdit;
            this.ItemForEmployeeID.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeID.Location = new System.Drawing.Point(0, 57);
            this.ItemForEmployeeID.Name = "ItemForEmployeeID";
            this.ItemForEmployeeID.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeID.Text = "Employee ID:";
            this.ItemForEmployeeID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForEmployeeFirstname
            // 
            this.ItemForEmployeeFirstname.Control = this.EmployeeFirstnameTextEdit;
            this.ItemForEmployeeFirstname.CustomizationFormText = "Employee Forename:";
            this.ItemForEmployeeFirstname.Location = new System.Drawing.Point(0, 57);
            this.ItemForEmployeeFirstname.Name = "ItemForEmployeeFirstname";
            this.ItemForEmployeeFirstname.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeFirstname.Text = "Employee Forename:";
            this.ItemForEmployeeFirstname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeNumber
            // 
            this.ItemForEmployeeNumber.Control = this.EmployeeNumberTextEdit;
            this.ItemForEmployeeNumber.CustomizationFormText = "Employee Number:";
            this.ItemForEmployeeNumber.Location = new System.Drawing.Point(0, 57);
            this.ItemForEmployeeNumber.Name = "ItemForEmployeeNumber";
            this.ItemForEmployeeNumber.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeNumber.Text = "Employee Number:";
            this.ItemForEmployeeNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeSurname
            // 
            this.ItemForEmployeeSurname.Control = this.EmployeeSurnameTextEdit;
            this.ItemForEmployeeSurname.CustomizationFormText = "Employee Surname:";
            this.ItemForEmployeeSurname.Location = new System.Drawing.Point(0, 57);
            this.ItemForEmployeeSurname.Name = "ItemForEmployeeSurname";
            this.ItemForEmployeeSurname.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeSurname.Text = "Employee Surname:";
            this.ItemForEmployeeSurname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(553, 395);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmployeeName,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.ItemForGuaranteeBonus,
            this.ItemForGuaranteeBonusAmount,
            this.ItemForBonusDatePaid,
            this.ItemForBonusDueDate,
            this.emptySpaceItem5,
            this.ItemForBonusAmountPaid,
            this.emptySpaceItem3});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(533, 375);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.AllowHide = false;
            this.ItemForEmployeeName.Control = this.EmployeeNameButtonEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Firstname:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 23);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeName.Text = "Linked to Employee: ";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(135, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(135, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(135, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(312, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(221, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(135, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 196);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(533, 179);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(509, 133);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(485, 85);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(485, 85);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForGuaranteeBonus
            // 
            this.ItemForGuaranteeBonus.Control = this.GuaranteeBonusCheckEdit;
            this.ItemForGuaranteeBonus.CustomizationFormText = "Guaranteed Bonus:";
            this.ItemForGuaranteeBonus.Location = new System.Drawing.Point(0, 57);
            this.ItemForGuaranteeBonus.Name = "ItemForGuaranteeBonus";
            this.ItemForGuaranteeBonus.Size = new System.Drawing.Size(533, 23);
            this.ItemForGuaranteeBonus.Text = "Guaranteed Bonus:";
            this.ItemForGuaranteeBonus.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForGuaranteeBonusAmount
            // 
            this.ItemForGuaranteeBonusAmount.Control = this.GuaranteeBonusAmountSpinEdit;
            this.ItemForGuaranteeBonusAmount.CustomizationFormText = "Guaranteed Bonus Amount:";
            this.ItemForGuaranteeBonusAmount.Location = new System.Drawing.Point(0, 80);
            this.ItemForGuaranteeBonusAmount.Name = "ItemForGuaranteeBonusAmount";
            this.ItemForGuaranteeBonusAmount.Size = new System.Drawing.Size(533, 24);
            this.ItemForGuaranteeBonusAmount.Text = "Guaranteed Bonus Amount:";
            this.ItemForGuaranteeBonusAmount.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForBonusDatePaid
            // 
            this.ItemForBonusDatePaid.Control = this.BonusDatePaidDateEdit;
            this.ItemForBonusDatePaid.CustomizationFormText = "Bonus Date Paid:";
            this.ItemForBonusDatePaid.Location = new System.Drawing.Point(0, 138);
            this.ItemForBonusDatePaid.Name = "ItemForBonusDatePaid";
            this.ItemForBonusDatePaid.Size = new System.Drawing.Size(533, 24);
            this.ItemForBonusDatePaid.Text = "Bonus Date Paid:";
            this.ItemForBonusDatePaid.TextSize = new System.Drawing.Size(133, 13);
            // 
            // ItemForBonusDueDate
            // 
            this.ItemForBonusDueDate.Control = this.BonusDueDateDateEdit;
            this.ItemForBonusDueDate.CustomizationFormText = "Bonus Due Date:";
            this.ItemForBonusDueDate.Location = new System.Drawing.Point(0, 114);
            this.ItemForBonusDueDate.Name = "ItemForBonusDueDate";
            this.ItemForBonusDueDate.Size = new System.Drawing.Size(533, 24);
            this.ItemForBonusDueDate.Text = "Bonus Due Date:";
            this.ItemForBonusDueDate.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 104);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForBonusAmountPaid
            // 
            this.ItemForBonusAmountPaid.Control = this.BonusAmountPaidSpinEdit;
            this.ItemForBonusAmountPaid.CustomizationFormText = "Bonus Amount Paid:";
            this.ItemForBonusAmountPaid.Location = new System.Drawing.Point(0, 162);
            this.ItemForBonusAmountPaid.Name = "ItemForBonusAmountPaid";
            this.ItemForBonusAmountPaid.Size = new System.Drawing.Size(533, 24);
            this.ItemForBonusAmountPaid.Text = "Bonus Amount Paid:";
            this.ItemForBonusAmountPaid.TextSize = new System.Drawing.Size(133, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 186);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(533, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00040_Employee_Bonus_EditTableAdapter
            // 
            this.sp_HR_00040_Employee_Bonus_EditTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Employee_Bonus_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(553, 451);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Bonus_Edit";
            this.Text = "Edit Employee Bonus";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Bonus_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Bonus_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Bonus_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BonusAmountPaidSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GuaranteeBonusAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GuaranteeBonusCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00040EmployeeBonusEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusDatePaidDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusDatePaidDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusDueDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusDueDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BonusIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBonusID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGuaranteeBonus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGuaranteeBonusAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBonusDatePaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBonusDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBonusAmountPaid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeID;
        private DevExpress.XtraEditors.ButtonEdit EmployeeNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeIDSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBonusID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit BonusIDTextEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeFirstnameTextEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeSurnameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeSurname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeFirstname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeNumber;
        private DevExpress.XtraEditors.DateEdit BonusDatePaidDateEdit;
        private DevExpress.XtraEditors.DateEdit BonusDueDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBonusDueDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBonusDatePaid;
        private System.Windows.Forms.BindingSource spHR00040EmployeeBonusEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00040_Employee_Bonus_EditTableAdapter sp_HR_00040_Employee_Bonus_EditTableAdapter;
        private DevExpress.XtraEditors.CheckEdit GuaranteeBonusCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGuaranteeBonus;
        private DevExpress.XtraEditors.SpinEdit GuaranteeBonusAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGuaranteeBonusAmount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.SpinEdit BonusAmountPaidSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBonusAmountPaid;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
    }
}

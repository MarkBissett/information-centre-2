﻿namespace WoodPlan5
{
    partial class frm_HR_Employee_Holiday_Add_What
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Holiday_Add_What));
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControlInformation = new DevExpress.XtraEditors.LabelControl();
            this.btnHolidayYear = new DevExpress.XtraEditors.SimpleButton();
            this.btnHoliday = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(351, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 112);
            this.barDockControlBottom.Size = new System.Drawing.Size(351, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 86);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(351, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 86);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(7, 32);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(32, 32);
            this.pictureEdit1.TabIndex = 4;
            // 
            // labelControlInformation
            // 
            this.labelControlInformation.AllowHtmlString = true;
            this.labelControlInformation.Location = new System.Drawing.Point(46, 42);
            this.labelControlInformation.Name = "labelControlInformation";
            this.labelControlInformation.Size = new System.Drawing.Size(262, 13);
            this.labelControlInformation.TabIndex = 5;
            this.labelControlInformation.Text = "<b>Add</b> Employee Holidays - What would you like to <b>add</b>?";
            // 
            // btnHolidayYear
            // 
            this.btnHolidayYear.Image = ((System.Drawing.Image)(resources.GetObject("btnHolidayYear.Image")));
            this.btnHolidayYear.Location = new System.Drawing.Point(173, 76);
            this.btnHolidayYear.Name = "btnHolidayYear";
            this.btnHolidayYear.Size = new System.Drawing.Size(140, 23);
            this.btnHolidayYear.TabIndex = 6;
            this.btnHolidayYear.Text = "Employee Holiday Year";
            this.btnHolidayYear.Click += new System.EventHandler(this.btnHolidayYear_Click);
            // 
            // btnHoliday
            // 
            this.btnHoliday.Image = ((System.Drawing.Image)(resources.GetObject("btnHoliday.Image")));
            this.btnHoliday.Location = new System.Drawing.Point(45, 76);
            this.btnHoliday.Name = "btnHoliday";
            this.btnHoliday.Size = new System.Drawing.Size(122, 23);
            this.btnHoliday.TabIndex = 7;
            this.btnHoliday.Text = "Employee Holiday";
            this.btnHoliday.Click += new System.EventHandler(this.btnHoliday_Click);
            // 
            // frm_HR_Employee_Holiday_Add_What
            // 
            this.AcceptButton = this.btnHoliday;
            this.ClientSize = new System.Drawing.Size(351, 112);
            this.Controls.Add(this.btnHoliday);
            this.Controls.Add(this.btnHolidayYear);
            this.Controls.Add(this.labelControlInformation);
            this.Controls.Add(this.pictureEdit1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_HR_Employee_Holiday_Add_What";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Holiday - Add What?";
            this.Load += new System.EventHandler(this.frm_HR_Employee_Holiday_Add_What_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.pictureEdit1, 0);
            this.Controls.SetChildIndex(this.labelControlInformation, 0);
            this.Controls.SetChildIndex(this.btnHolidayYear, 0);
            this.Controls.SetChildIndex(this.btnHoliday, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControlInformation;
        private DevExpress.XtraEditors.SimpleButton btnHolidayYear;
        private DevExpress.XtraEditors.SimpleButton btnHoliday;
    }
}

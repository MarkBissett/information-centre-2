using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_HR_Linked_Document_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public int intRecordTypeID = 0;
        public string strRecordTypeDescription = "";
        public int intRecordSubTypeID = 0;
        public string strRecordSubTypeDescription = "";

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public frm_HR_Qualification_Edit frmCallingQualificationForm = null;  // Only used when called by frm_HR_Qualification_Edit screen //

        #endregion

        public frm_HR_Linked_Document_Edit()
        {
            InitializeComponent();
        }

        private void frm_HR_Linked_Document_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500101;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(9, (intRecordTypeID != 4 ? "HR_LinkedDocumentPath" : "HR_QualificationCertificatePath")).ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Populate Main Dataset //
            this.sp_HR_00182_Linked_Document_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["LinkedDocumentID"] = 0;
                        drNewRow["LinkedToRecordID"] = intLinkedToRecordID;
                        drNewRow["LinkedRecordDescription"] = strLinkedToRecordDesc;
                        drNewRow["LinkedDocumentTypeID"] = intRecordTypeID;
                        drNewRow["LinkedDocumentType"] = strRecordTypeDescription;
                        drNewRow["LinkedDocumentSubTypeID"] = intRecordSubTypeID;
                        drNewRow["LinkedDocumentSubType"] = strRecordSubTypeDescription;
                        drNewRow["AddedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["AddedByStaffName"] = this.GlobalSettings.UserSurname + ": " + this.GlobalSettings.UserForename;
                        drNewRow["DateAdded"] = DateTime.Now;
                        dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception) { }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedDocumentTypeID"] = 0;
                        drNewRow["LinkedRecordDescription"] = "Not Applicable - Block Adding";
                        drNewRow["LinkedDocumentTypeID"] = intRecordTypeID;
                        drNewRow["LinkedDocumentType"] = strRecordTypeDescription;
                        drNewRow["LinkedDocumentSubTypeID"] = intRecordSubTypeID;
                        drNewRow["LinkedDocumentSubType"] = strRecordSubTypeDescription;
                        drNewRow["AddedByStaffID"] = this.GlobalSettings.UserID;
                        drNewRow["AddedByStaffName"] = this.GlobalSettings.UserSurname + ": " + this.GlobalSettings.UserForename;
                        drNewRow["DateAdded"] = DateTime.Now;
                        dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception) { }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["LinkedDocumentID"] = 0;
                        drNewRow["LinkedToRecordID"] = 0;
                        drNewRow["LinkedDocumentTypeID"] = 0;
                        dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.Rows.Add(drNewRow);
                        dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception) { }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_HR_00182_Linked_Document_EditTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception) { }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " HR Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        LinkedRecordDescriptionButtonEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        LinkedRecordDescriptionButtonEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = true;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        DescriptionTextEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = false;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        DescriptionTextEdit.Focus();

                        LinkedRecordDescriptionButtonEdit.Properties.ReadOnly = true;
                        LinkedRecordDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_HR_Linked_Document_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Linked_Document_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            spHR00182LinkedDocumentEditBindingSource.EndEdit();
            try
            {
                sp_HR_00182_Linked_Document_EditTableAdapter.Update(dataSet_HR_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spHR00182LinkedDocumentEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["LinkedDocumentID"]) + ";";
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)spHR00182LinkedDocumentEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Linked_Document_Manager")
                    {
                        var fParentForm = (frm_HR_Linked_Document_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                    }
                    else if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        var fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Classes.HR.Utils.enmMainGrids.Employees, "");
                    }
                    else if (frmChild.Name == "frm_HR_Pension_Manager")
                    {
                        var fParentForm = (frm_HR_Pension_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", "");
                    }
                    else if (frmChild.Name == "frm_HR_Sanction_Manager")
                    {
                        var fParentForm = (frm_HR_Sanction_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "");
                    }
                    else if (frmChild.Name == "frm_HR_Qualifications_Manager")
                    {
                        var fParentForm = (frm_HR_Qualifications_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "");
                    }
                    else if (frmChild.Name == "frm_HR_Holiday_Manager")
                    {
                        var fParentForm = (frm_HR_Holiday_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", "");
                    }
                    else if (frmChild.Name == "frm_HR_Vetting_Manager")
                    {
                        var fParentForm = (frm_HR_Vetting_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", "");
                    }
                    else if (frmChild.Name == "frm_HR_Bonus_Manager")
                    {
                        var fParentForm = (frm_HR_Bonus_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "");
                    }
                    else if (frmChild.Name == "frm_HR_CRM_Manager")
                    {
                        var fParentForm = (frm_HR_CRM_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "");
                    }
                    else if (frmChild.Name == "frm_HR_Qualification_Edit")
                    {
                        // Update only specific instance of the form //
                        try
                        {
                            if (frmCallingQualificationForm != null) frmCallingQualificationForm.Get_Linked_Document_Count();
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.Rows.Count; i++)
            {
                switch (dataSet_HR_DataEntry.sp_HR_00182_Linked_Document_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void LinkedDocumentTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00182LinkedDocumentEditBindingSource.Current;
                if (currentRow == null) return;
                int TypeID = (string.IsNullOrEmpty(currentRow["LinkedDocumentTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedDocumentTypeID"]));
                int SubTypeID = (string.IsNullOrEmpty(currentRow["LinkedDocumentSubTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedDocumentSubTypeID"]));

                var fChildForm = new frm_HR_Select_Linked_Document_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intPassedInTypeID = TypeID;
                fChildForm.intPassedInSubTypeID = SubTypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["LinkedDocumentTypeID"] = fChildForm.intSelectedTypeID;
                    currentRow["LinkedDocumentSubTypeID"] = fChildForm.intSelectedSubTypeID;
                    currentRow["LinkedDocumentType"] = fChildForm.strSelectedTypeName;
                    currentRow["LinkedDocumentSubType"] = fChildForm.strSelectedSubTypeName;

                    if (TypeID != fChildForm.intSelectedTypeID)
                    {
                        currentRow["LinkedToRecordID"] = 0;
                        currentRow["LinkedRecordDescription"] = "";
                        if (strFormMode == "add" || strFormMode == "edit") dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "Select a value.");
                    }
                    spHR00182LinkedDocumentEditBindingSource.EndEdit();
                }
            }
        }
        private void LinkedDocumentTypeButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(LinkedDocumentTypeButtonEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedDocumentTypeButtonEdit, "");
            }
        }

        private void LinkedRecordDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)spHR00182LinkedDocumentEditBindingSource.Current;
            if (currentRow == null) return;
            int RecordTypeID = (string.IsNullOrEmpty(currentRow["LinkedDocumentTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedDocumentTypeID"]));
            if (RecordTypeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Linked Document Type before proceeding.", "Link Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intOriginalID = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
            switch (RecordTypeID)
            {
                case 1:  // Employee // 
                    {
                        var fChildForm = new frm_HR_Select_Employee();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalEmployeeID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedEmployeeID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedEmployeeName;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 2:  // Pension // 
                    {
                        var fChildForm = new frm_HR_Select_Employee_Pension();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalPensionID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedPensionID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedPensionDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 3:  // Discipline \ Grievance //
                    {
                        var fChildForm = new frm_HR_Select_Employee_Sanction();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 4:  // Training //
                    {
                        var fChildForm = new frm_HR_Select_Person_Training();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 5:  // CRM //
                    {
                        var fChildForm = new frm_HR_Select_Employee_CRM();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 6:  // Holiday \ Absence //
                    {
                        var fChildForm = new frm_HR_Select_Employee_Absence();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        //fChildForm.intOriginalPensionID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedChildID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedChildName;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 7:  // Department Worked \\
                    {
                        var fChildForm = new frm_HR_Select_Department_Worked();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._OriginalEmployeeDeptWorkedID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm._SelectedDepartmentWorkedID;
                            currentRow["LinkedRecordDescription"] = fChildForm._SelectedDepartmentName;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 8:  // References //
                    {
                        var fChildForm = new frm_HR_Select_Employee_Reference();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 9:  // Vetting //
                    {
                        var fChildForm = new frm_HR_Select_Employee_Vetting();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 10:  // Bonus //
                    {
                        var fChildForm = new frm_HR_Select_Employee_Bonus();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 11:  // Pay Rise //
                    {
                        var fChildForm = new frm_HR_Select_Employee_Pay_Rise();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 12:  // Shares //
                    {
                        var fChildForm = new frm_HR_Select_Employee_Shares();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedID;
                            currentRow["LinkedRecordDescription"] = fChildForm.strSelectedDescription;
                            spHR00182LinkedDocumentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        private void LinkedRecordDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit glue = (ButtonEdit)sender;
            string strValue = glue.EditValue.ToString();
            if (this.strFormMode != "blockadd" && this.strFormMode != "blockedit" && String.IsNullOrEmpty(strValue))
            {
                dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(LinkedRecordDescriptionButtonEdit, "");
            }
        }

        private void DescriptionTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit te = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DescriptionTextEdit, "");
            }
        }
 
        private void DocumentPathButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DocumentPathButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DocumentPathButtonEdit, "");
            }

        }
        private void DocumentPathButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = null;
            switch (e.Button.Tag.ToString())
            {
                case "select file":
                    {
                        if (strFormMode == "view") return;
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "tab";
                            dlg.Filter = "";
                            if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                string strExtension = "";
                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strDefaultPath != "")
                                        if (!filename.ToLower().StartsWith(strDefaultPath.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Default Linked Documents Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    strTempFileName = filename.Substring(strDefaultPath.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                    // Update Extension Field with extension from filename //
                                    strExtension = System.IO.Path.GetExtension(filename);
                                    if (!string.IsNullOrEmpty(strExtension))
                                    {
                                        currentRow = (DataRowView)spHR00182LinkedDocumentEditBindingSource.Current;
                                        if (currentRow != null)
                                        {
                                            currentRow["DocumentExtension"] = strExtension.Substring(1).ToUpper();  // Ignore the "." at the start of the extension //
                                        }
                                    }
                                }
                                DocumentPathButtonEdit.Text = strTempResult;
                            }
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter so linked file can be viewed if required //
                        }
                    }
                    break;
                case "view file":
                    {
                        currentRow = (DataRowView)spHR00182LinkedDocumentEditBindingSource.Current;
                        if (currentRow == null) return;
                        string strFile = currentRow["DocumentPath"].ToString();
                        string strExtension = currentRow["DocumentExtension"].ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            if (strExtension.ToLower() != "pdf")
                            {
                                System.Diagnostics.Process.Start(Path.Combine(strDefaultPath + strFile));
                            }
                            else
                            {
                                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                                {
                                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                    fChildForm.strPDFFile = Path.Combine(strDefaultPath + strFile);
                                    fChildForm.MdiParent = this.MdiParent;
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.Show();
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(Path.Combine(strDefaultPath + strFile));
                                }
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view the Linked Document: " + strFile + ".\n\nThe File may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Document", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }

        #endregion













   
    }
}


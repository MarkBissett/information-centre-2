using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_HR_CRM_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInCRMIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;
        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;
        private string i_str_selected_department_ids = "";
        private string i_str_selected_departments = "";
        private string i_str_selected_employee_ids = "";
        private string i_str_selected_employees = "";

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        private DataSet_Selection DS_Selection1;

        #endregion

        public frm_HR_CRM_Manager()
        {
            InitializeComponent();
        }

        private void frm_HR_CRM_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 9914;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            dateEditStart.DateTime = DateTime.Today.AddMonths(-12);
            dateEditEnd.DateTime = DateTime.Today.AddMonths(12);
            beiDateRange.EditValue = PopupContainerEdit1_Get_Selected();

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridView1, strConnectionString);  // Used by DataSets //

            sp_HR_00114_Department_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00114_Department_Filter_ListTableAdapter.Fill(dataSet_HR_Core.sp_HR_00114_Department_Filter_List, "");
            gridControl2.ForceInitialize();

            sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            // Don't Fill Yet (in case we need to filter by department) //
            gridControl4.ForceInitialize();
            
            sp_HR_00199_Employee_CRM_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridView1, "CRMID");

            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            if (strPassedInCRMIDs != "")  // Opened in drill-down mode //
            {
                beiDateRange.EditValue = "Custom Filter";
                beiDepartment.EditValue = "Custom Filter";
                beiEmployee.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }

            popupContainerControlDepartments.Size = new System.Drawing.Size(312, 423);
            popupContainerControlEmployees.Size = new System.Drawing.Size(312, 423);

            emptyEditor = new RepositoryItem();
        }
        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInCRMIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_HR_CRM_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
            }
            SetMenuStatus();
        }

        private void frm_HR_CRM_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInCRMIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "DepartmentFilter", i_str_selected_department_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "EmployeeFilter", i_str_selected_employee_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("DepartmentFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array array = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl2.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in array)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["DepartmentID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    beiDepartment.EditValue = PopupContainerEdit2_Get_Selected();
                }

                Load_Employee_Filter();

                // Employee Filter //
                intFoundRow = 0;
                strFilter = default_screen_settings.RetrieveSetting("EmployeeFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array array = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl4.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in array)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["EmployeeID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    beiEmployee.EditValue = PopupContainerEdit3_Get_Selected();
                }

                bbiRefresh.PerformClick();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                // Get BarButtons Location //
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 0, 0);
                foreach (BarItemLink link in bar1.ItemLinks)
                {
                    if (link.Item.Name == "beiDateRange")  // Find the Correct button then call function to gets it position using Reflection (as this info is not publicly available //
                    {
                        rect = GetLinksScreenRect(link);
                        break;
                    }
                }
                if (rect.X > 0 && rect.Y > 0)
                {
                    // Draw Locus Effect on object so user can see it //
                    System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, rect.Y + rect.Height - 10);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                }

            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    
                    alItems.Add("iBlockAdd");
                    bbiBlockAdd.Enabled = true;
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_department_ids == null) i_str_selected_department_ids = "";
            if (i_str_selected_employee_ids == null) i_str_selected_employee_ids = "";

            int intActive = Convert.ToInt32(beiActive.EditValue);
            GridView view = (GridView)gridControl1.MainView;
            
            RefreshGridViewState1.SaveViewInfo();
            view.BeginUpdate();
            if (beiDateRange.EditValue.ToString() == "Custom Filter" && strPassedInCRMIDs != "")  // Load passed in Records //
            {

                sp_HR_00199_Employee_CRM_ManagerTableAdapter.Fill(dataSet_HR_Core.sp_HR_00199_Employee_CRM_Manager, 0, "", "", null, null, strPassedInCRMIDs);
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
            }
            else // Load users selection //
            {
                sp_HR_00199_Employee_CRM_ManagerTableAdapter.Fill(dataSet_HR_Core.sp_HR_00199_Employee_CRM_Manager, intActive, i_str_selected_department_ids, i_str_selected_employee_ids, i_dtStart, i_dtEnd, "");
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["CRMID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void Load_Employee_Filter()
        {
            // This filter list oc controlled by the departments set - any selected departments are passed in to this SP //
            sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.Fill(dataSet_HR_Core.sp_HR_00115_Employees_By_Dept_Filter_List, (i_str_selected_department_ids == null ? "" : i_str_selected_department_ids));
       }

        private void Clear_Employee_filter()
        {
            i_str_selected_employee_ids = "";
            i_str_selected_employees = "";
            beiEmployee.EditValue = "No Employee Filter";
            selection3.ClearSelection();
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd) return;
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControl1.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp_HR_00199_Employee_CRM_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.EmployeeID;
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.EmployeeSurname, row.EmployeeFirstname);
                            fChildForm.strLinkedToRecordDesc2 = row.EmployeeSurname;
                            fChildForm.strLinkedToRecordDesc3 = row.EmployeeFirstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;      
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowAdd) return;

                        var fEmployeeSelect = new frm_HR_Select_Employee();
                        fEmployeeSelect.GlobalSettings = this.GlobalSettings;
                        fEmployeeSelect._Mode = "multiple";
                        if (fEmployeeSelect.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                        {
                            strRecordIDs = fEmployeeSelect.strSelectedEmployeeIDs;
                            intCount = fEmployeeSelect._SelectedCount;
                        }
                        if (string.IsNullOrWhiteSpace(strRecordIDs))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControl1.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Employee CRM Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Employee CRM Record" : Convert.ToString(intRowHandles.Length) + " Employee CRM Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Employee CRM Record" : "these Employee CRM Records") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "CRMID")) + ",";
                            }
                            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_crm", strRecordIDs);
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // CRM //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        #region Department Filter Panel

        private void btnDepartmentFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditDepartment_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_department_ids = "";    // Reset any prior values first //
            i_str_selected_departments = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_department_ids = "";

                Clear_Employee_filter();
                Load_Employee_Filter();
                return "No Department Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_department_ids = "";

                Clear_Employee_filter();
                Load_Employee_Filter();
                return "No Department Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_department_ids += Convert.ToString(view.GetRowCellValue(i, "DepartmentID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_departments = Convert.ToString(view.GetRowCellValue(i, "DepartmentName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_departments += ", " + Convert.ToString(view.GetRowCellValue(i, "DepartmentName"));
                        }
                        intCount++;
                    }
                }
            }

            Clear_Employee_filter();
            Load_Employee_Filter();
            return i_str_selected_departments;
        }

        #endregion


        #region Employee Filter Panel

        private void btnEmployeeFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditEmployee_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit3_Get_Selected();
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_employee_ids = "";    // Reset any prior values first //
            i_str_selected_employees = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_employee_ids = "";
                return "No Employee Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_employee_ids = "";
                return "No Employee Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_employee_ids += Convert.ToString(view.GetRowCellValue(i, "EmployeeID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_employees = Convert.ToString(view.GetRowCellValue(i, "Firstname")) + " " + Convert.ToString(view.GetRowCellValue(i, "Surname"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_employees += ", " + Convert.ToString(view.GetRowCellValue(i, "Firstname")) + " " + Convert.ToString(view.GetRowCellValue(i, "Surname"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_employees;
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            string strValue = "";
            if (dateEditStart.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditStart.DateTime;
            }
            if (dateEditEnd.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditEnd.DateTime;
            }
            if (dateEditStart.DateTime == DateTime.MinValue && dateEditEnd.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }


        #endregion


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Employee CRM - Adjust any filters then click Refresh button");
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetSelectionInverted.Enabled = true;
                bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void repositoryItemHyperLinkEditLinkedDocuments_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordType = 5;  // CRM //
            int intRecordSubType = 0;  // Not Used //
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CRMID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "Description").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DaysRemaining")
            {
                int intDaysRemaining = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysRemaining"));
                if (intDaysRemaining > 7)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                    e.Appearance.BackColor2 = Color.PaleGreen;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysRemaining > 0 && intDaysRemaining <= 7)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysRemaining < 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DaysRemaining":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysRemaining")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "DaysRemaining":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("DaysRemaining")) == 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridView1;
                        int intRecordType = 5;  // CRM //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CRMID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "Description").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = ",";
            string strColumnName = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    view = (GridView)gridControl1.MainView;
                    strColumnName = "EmployeeID";
                    break;
                default:
                    return;
            }
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strCurrentID = view.GetRowCellValue(intRowHandle, view.Columns[strColumnName]).ToString() + ',';
                if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                {
                    strSelectedIDs += strCurrentID;
                    intCount++;
                }
            }
            strSelectedIDs = strSelectedIDs.Remove(0, 1);  // Remove leading ',' //
            CreateDataset("Employee", intCount, strSelectedIDs);
        }

        private void CreateDataset(string strType, int intRecordCount, string strSelectedRecordIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_employee_count = intRecordCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Employee,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "EmployeeID");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Employee,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "EmployeeID");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
        }

        #endregion


        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void bciFilterSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (bciFilterSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more records to filter by before proceeding.", "Filter Selected Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControl1.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
            }
            else  // Clear Filter //
            {
                try
                {
                    gridControl1.BeginUpdate();
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00199_Employee_CRM_Manager.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControl1.EndUpdate();
        }



 

 


 

    }
}


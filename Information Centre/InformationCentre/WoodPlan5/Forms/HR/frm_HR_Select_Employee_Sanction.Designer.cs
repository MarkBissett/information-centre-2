namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Sanction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00186SanctionsSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSanctionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colERIssueTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colERIssueType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHRManagerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHRManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRaisedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigatedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigatedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeardByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeardByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingNoteTakerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingEmployeeRepresentativeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeIssuedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeIssuedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealHeardByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealHeardByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealNoteTakerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealEmployeeRepresentativeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric0DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpiryDurationUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDurationUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSanctionOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00186_Sanctions_SelectTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00186_Sanctions_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00186SanctionsSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00186SanctionsSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditNumeric0DP,
            this.repositoryItemMemoExEdit2});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00186SanctionsSelectBindingSource
            // 
            this.spHR00186SanctionsSelectBindingSource.DataMember = "sp_HR_00186_Sanctions_Select";
            this.spHR00186SanctionsSelectBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSanctionID,
            this.colEmployeeID4,
            this.colEmployeeName,
            this.colEmployeeSurname2,
            this.colEmployeeFirstname,
            this.colEmployeeNumber5,
            this.colERIssueTypeID,
            this.colERIssueType,
            this.colHRManagerID,
            this.colHRManagerName,
            this.colDateRaised,
            this.colRaisedByPersonID,
            this.colRaisedByPersonName,
            this.colStatusID,
            this.colStatusValue,
            this.colInvestigationStartDate,
            this.colInvestigationEndDate,
            this.colInvestigatedByPersonID,
            this.colInvestigatedByPersonName,
            this.colInvestigationOutcomeID,
            this.colInvestigationOutcome,
            this.colHearingDate,
            this.colHeardByPersonID,
            this.colHeardByPersonName,
            this.colHearingNoteTakerName,
            this.colHearingEmployeeRepresentativeName,
            this.colHearingOutcomeID,
            this.colHearingOutcome,
            this.colHearingOutcomeDate,
            this.colHearingOutcomeIssuedByPersonID,
            this.colHearingOutcomeIssuedByPersonName,
            this.colAppealDate,
            this.colAppealHeardByPersonID,
            this.colAppealHeardByPersonName,
            this.colAppealOutcomeID,
            this.colAppealOutcome,
            this.colAppealNoteTakerName,
            this.colAppealEmployeeRepresentativeName,
            this.colExpiryDurationUnits,
            this.colExpiryDurationUnitDescriptorID,
            this.colExpiryDurationUnitDescriptor,
            this.colExpiryDate,
            this.colRemarks2,
            this.colLive,
            this.colSanctionOutcome});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.BestFitMaxRowCount = 20;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colSanctionID
            // 
            this.colSanctionID.Caption = "Sanction ID";
            this.colSanctionID.FieldName = "SanctionID";
            this.colSanctionID.Name = "colSanctionID";
            this.colSanctionID.OptionsColumn.AllowEdit = false;
            this.colSanctionID.OptionsColumn.AllowFocus = false;
            this.colSanctionID.OptionsColumn.ReadOnly = true;
            this.colSanctionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSanctionID.Width = 76;
            // 
            // colEmployeeID4
            // 
            this.colEmployeeID4.Caption = "Employee ID";
            this.colEmployeeID4.FieldName = "EmployeeID";
            this.colEmployeeID4.Name = "colEmployeeID4";
            this.colEmployeeID4.OptionsColumn.AllowEdit = false;
            this.colEmployeeID4.OptionsColumn.AllowFocus = false;
            this.colEmployeeID4.OptionsColumn.ReadOnly = true;
            this.colEmployeeID4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID4.Width = 81;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.Caption = "Employee Surname: Forename";
            this.colEmployeeName.FieldName = "EmployeeName";
            this.colEmployeeName.Name = "colEmployeeName";
            this.colEmployeeName.OptionsColumn.AllowEdit = false;
            this.colEmployeeName.OptionsColumn.AllowFocus = false;
            this.colEmployeeName.OptionsColumn.ReadOnly = true;
            this.colEmployeeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName.Visible = true;
            this.colEmployeeName.VisibleIndex = 2;
            this.colEmployeeName.Width = 167;
            // 
            // colEmployeeSurname2
            // 
            this.colEmployeeSurname2.Caption = "Employee Surname";
            this.colEmployeeSurname2.FieldName = "EmployeeSurname";
            this.colEmployeeSurname2.Name = "colEmployeeSurname2";
            this.colEmployeeSurname2.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname2.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname2.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname2.Width = 112;
            // 
            // colEmployeeFirstname
            // 
            this.colEmployeeFirstname.Caption = "Employee Forename";
            this.colEmployeeFirstname.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname.Name = "colEmployeeFirstname";
            this.colEmployeeFirstname.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname.Width = 118;
            // 
            // colEmployeeNumber5
            // 
            this.colEmployeeNumber5.Caption = "Employee #";
            this.colEmployeeNumber5.FieldName = "EmployeeNumber";
            this.colEmployeeNumber5.Name = "colEmployeeNumber5";
            this.colEmployeeNumber5.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber5.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber5.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber5.Width = 78;
            // 
            // colERIssueTypeID
            // 
            this.colERIssueTypeID.Caption = "ER Issue Type ID";
            this.colERIssueTypeID.FieldName = "ERIssueTypeID";
            this.colERIssueTypeID.Name = "colERIssueTypeID";
            this.colERIssueTypeID.OptionsColumn.AllowEdit = false;
            this.colERIssueTypeID.OptionsColumn.AllowFocus = false;
            this.colERIssueTypeID.OptionsColumn.ReadOnly = true;
            this.colERIssueTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colERIssueTypeID.Width = 104;
            // 
            // colERIssueType
            // 
            this.colERIssueType.Caption = "ER Issue Type";
            this.colERIssueType.FieldName = "ERIssueType";
            this.colERIssueType.Name = "colERIssueType";
            this.colERIssueType.OptionsColumn.AllowEdit = false;
            this.colERIssueType.OptionsColumn.AllowFocus = false;
            this.colERIssueType.OptionsColumn.ReadOnly = true;
            this.colERIssueType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colERIssueType.Visible = true;
            this.colERIssueType.VisibleIndex = 2;
            this.colERIssueType.Width = 90;
            // 
            // colHRManagerID
            // 
            this.colHRManagerID.Caption = "HR Manager ID";
            this.colHRManagerID.FieldName = "HRManagerID";
            this.colHRManagerID.Name = "colHRManagerID";
            this.colHRManagerID.OptionsColumn.AllowEdit = false;
            this.colHRManagerID.OptionsColumn.AllowFocus = false;
            this.colHRManagerID.OptionsColumn.ReadOnly = true;
            this.colHRManagerID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHRManagerID.Width = 94;
            // 
            // colHRManagerName
            // 
            this.colHRManagerName.Caption = "HR Manager";
            this.colHRManagerName.FieldName = "HRManagerName";
            this.colHRManagerName.Name = "colHRManagerName";
            this.colHRManagerName.OptionsColumn.AllowEdit = false;
            this.colHRManagerName.OptionsColumn.AllowFocus = false;
            this.colHRManagerName.OptionsColumn.ReadOnly = true;
            this.colHRManagerName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHRManagerName.Visible = true;
            this.colHRManagerName.VisibleIndex = 3;
            this.colHRManagerName.Width = 80;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 0;
            this.colDateRaised.Width = 92;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colRaisedByPersonID
            // 
            this.colRaisedByPersonID.Caption = "Raised By Person ID";
            this.colRaisedByPersonID.FieldName = "RaisedByPersonID";
            this.colRaisedByPersonID.Name = "colRaisedByPersonID";
            this.colRaisedByPersonID.OptionsColumn.AllowEdit = false;
            this.colRaisedByPersonID.OptionsColumn.AllowFocus = false;
            this.colRaisedByPersonID.OptionsColumn.ReadOnly = true;
            this.colRaisedByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRaisedByPersonID.Width = 118;
            // 
            // colRaisedByPersonName
            // 
            this.colRaisedByPersonName.Caption = "Raised By Person Name";
            this.colRaisedByPersonName.FieldName = "RaisedByPersonName";
            this.colRaisedByPersonName.Name = "colRaisedByPersonName";
            this.colRaisedByPersonName.OptionsColumn.AllowEdit = false;
            this.colRaisedByPersonName.OptionsColumn.AllowFocus = false;
            this.colRaisedByPersonName.OptionsColumn.ReadOnly = true;
            this.colRaisedByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRaisedByPersonName.Visible = true;
            this.colRaisedByPersonName.VisibleIndex = 4;
            this.colRaisedByPersonName.Width = 134;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatusValue
            // 
            this.colStatusValue.Caption = "Status";
            this.colStatusValue.FieldName = "StatusValue";
            this.colStatusValue.Name = "colStatusValue";
            this.colStatusValue.OptionsColumn.AllowEdit = false;
            this.colStatusValue.OptionsColumn.AllowFocus = false;
            this.colStatusValue.OptionsColumn.ReadOnly = true;
            this.colStatusValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatusValue.Visible = true;
            this.colStatusValue.VisibleIndex = 5;
            // 
            // colInvestigationStartDate
            // 
            this.colInvestigationStartDate.Caption = "Investigation Start Date";
            this.colInvestigationStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colInvestigationStartDate.FieldName = "InvestigationStartDate";
            this.colInvestigationStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colInvestigationStartDate.Name = "colInvestigationStartDate";
            this.colInvestigationStartDate.OptionsColumn.AllowEdit = false;
            this.colInvestigationStartDate.OptionsColumn.AllowFocus = false;
            this.colInvestigationStartDate.OptionsColumn.ReadOnly = true;
            this.colInvestigationStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colInvestigationStartDate.Visible = true;
            this.colInvestigationStartDate.VisibleIndex = 7;
            this.colInvestigationStartDate.Width = 137;
            // 
            // colInvestigationEndDate
            // 
            this.colInvestigationEndDate.Caption = "Investigation End Date";
            this.colInvestigationEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colInvestigationEndDate.FieldName = "InvestigationEndDate";
            this.colInvestigationEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colInvestigationEndDate.Name = "colInvestigationEndDate";
            this.colInvestigationEndDate.OptionsColumn.AllowEdit = false;
            this.colInvestigationEndDate.OptionsColumn.AllowFocus = false;
            this.colInvestigationEndDate.OptionsColumn.ReadOnly = true;
            this.colInvestigationEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colInvestigationEndDate.Visible = true;
            this.colInvestigationEndDate.VisibleIndex = 8;
            this.colInvestigationEndDate.Width = 131;
            // 
            // colInvestigatedByPersonID
            // 
            this.colInvestigatedByPersonID.Caption = "Investigating Officer ID";
            this.colInvestigatedByPersonID.FieldName = "InvestigatedByPersonID";
            this.colInvestigatedByPersonID.Name = "colInvestigatedByPersonID";
            this.colInvestigatedByPersonID.OptionsColumn.AllowEdit = false;
            this.colInvestigatedByPersonID.OptionsColumn.AllowFocus = false;
            this.colInvestigatedByPersonID.OptionsColumn.ReadOnly = true;
            this.colInvestigatedByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInvestigatedByPersonID.Width = 134;
            // 
            // colInvestigatedByPersonName
            // 
            this.colInvestigatedByPersonName.Caption = "Investigating Officer";
            this.colInvestigatedByPersonName.FieldName = "InvestigatedByPersonName";
            this.colInvestigatedByPersonName.Name = "colInvestigatedByPersonName";
            this.colInvestigatedByPersonName.OptionsColumn.AllowEdit = false;
            this.colInvestigatedByPersonName.OptionsColumn.AllowFocus = false;
            this.colInvestigatedByPersonName.OptionsColumn.ReadOnly = true;
            this.colInvestigatedByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInvestigatedByPersonName.Visible = true;
            this.colInvestigatedByPersonName.VisibleIndex = 9;
            this.colInvestigatedByPersonName.Width = 120;
            // 
            // colInvestigationOutcomeID
            // 
            this.colInvestigationOutcomeID.Caption = "Investigation Outcome ID";
            this.colInvestigationOutcomeID.FieldName = "InvestigationOutcomeID";
            this.colInvestigationOutcomeID.Name = "colInvestigationOutcomeID";
            this.colInvestigationOutcomeID.OptionsColumn.AllowEdit = false;
            this.colInvestigationOutcomeID.OptionsColumn.AllowFocus = false;
            this.colInvestigationOutcomeID.OptionsColumn.ReadOnly = true;
            this.colInvestigationOutcomeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInvestigationOutcomeID.Width = 144;
            // 
            // colInvestigationOutcome
            // 
            this.colInvestigationOutcome.Caption = "Investigation Outcome";
            this.colInvestigationOutcome.FieldName = "InvestigationOutcome";
            this.colInvestigationOutcome.Name = "colInvestigationOutcome";
            this.colInvestigationOutcome.OptionsColumn.AllowEdit = false;
            this.colInvestigationOutcome.OptionsColumn.AllowFocus = false;
            this.colInvestigationOutcome.OptionsColumn.ReadOnly = true;
            this.colInvestigationOutcome.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInvestigationOutcome.Visible = true;
            this.colInvestigationOutcome.VisibleIndex = 10;
            this.colInvestigationOutcome.Width = 130;
            // 
            // colHearingDate
            // 
            this.colHearingDate.Caption = "Hearing Date";
            this.colHearingDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colHearingDate.FieldName = "HearingDate";
            this.colHearingDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHearingDate.Name = "colHearingDate";
            this.colHearingDate.OptionsColumn.AllowEdit = false;
            this.colHearingDate.OptionsColumn.AllowFocus = false;
            this.colHearingDate.OptionsColumn.ReadOnly = true;
            this.colHearingDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHearingDate.Visible = true;
            this.colHearingDate.VisibleIndex = 11;
            this.colHearingDate.Width = 84;
            // 
            // colHeardByPersonID
            // 
            this.colHeardByPersonID.Caption = "Heard By Person ID";
            this.colHeardByPersonID.FieldName = "HeardByPersonID";
            this.colHeardByPersonID.Name = "colHeardByPersonID";
            this.colHeardByPersonID.OptionsColumn.AllowEdit = false;
            this.colHeardByPersonID.OptionsColumn.AllowFocus = false;
            this.colHeardByPersonID.OptionsColumn.ReadOnly = true;
            this.colHeardByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeardByPersonID.Width = 115;
            // 
            // colHeardByPersonName
            // 
            this.colHeardByPersonName.Caption = "Heard By Person";
            this.colHeardByPersonName.FieldName = "HeardByPersonName";
            this.colHeardByPersonName.Name = "colHeardByPersonName";
            this.colHeardByPersonName.OptionsColumn.AllowEdit = false;
            this.colHeardByPersonName.OptionsColumn.AllowFocus = false;
            this.colHeardByPersonName.OptionsColumn.ReadOnly = true;
            this.colHeardByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeardByPersonName.Visible = true;
            this.colHeardByPersonName.VisibleIndex = 12;
            this.colHeardByPersonName.Width = 101;
            // 
            // colHearingNoteTakerName
            // 
            this.colHearingNoteTakerName.Caption = "Hearing Note Taker";
            this.colHearingNoteTakerName.FieldName = "HearingNoteTakerName";
            this.colHearingNoteTakerName.Name = "colHearingNoteTakerName";
            this.colHearingNoteTakerName.OptionsColumn.AllowEdit = false;
            this.colHearingNoteTakerName.OptionsColumn.AllowFocus = false;
            this.colHearingNoteTakerName.OptionsColumn.ReadOnly = true;
            this.colHearingNoteTakerName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingNoteTakerName.Visible = true;
            this.colHearingNoteTakerName.VisibleIndex = 13;
            this.colHearingNoteTakerName.Width = 114;
            // 
            // colHearingEmployeeRepresentativeName
            // 
            this.colHearingEmployeeRepresentativeName.Caption = "Hearing Employee Representative";
            this.colHearingEmployeeRepresentativeName.FieldName = "HearingEmployeeRepresentativeName";
            this.colHearingEmployeeRepresentativeName.Name = "colHearingEmployeeRepresentativeName";
            this.colHearingEmployeeRepresentativeName.OptionsColumn.AllowEdit = false;
            this.colHearingEmployeeRepresentativeName.OptionsColumn.AllowFocus = false;
            this.colHearingEmployeeRepresentativeName.OptionsColumn.ReadOnly = true;
            this.colHearingEmployeeRepresentativeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingEmployeeRepresentativeName.Visible = true;
            this.colHearingEmployeeRepresentativeName.VisibleIndex = 14;
            this.colHearingEmployeeRepresentativeName.Width = 184;
            // 
            // colHearingOutcomeID
            // 
            this.colHearingOutcomeID.Caption = "Hearing Outcome ID";
            this.colHearingOutcomeID.FieldName = "HearingOutcomeID";
            this.colHearingOutcomeID.Name = "colHearingOutcomeID";
            this.colHearingOutcomeID.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeID.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeID.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingOutcomeID.Width = 118;
            // 
            // colHearingOutcome
            // 
            this.colHearingOutcome.Caption = "Hearing Outcome";
            this.colHearingOutcome.FieldName = "HearingOutcome";
            this.colHearingOutcome.Name = "colHearingOutcome";
            this.colHearingOutcome.OptionsColumn.AllowEdit = false;
            this.colHearingOutcome.OptionsColumn.AllowFocus = false;
            this.colHearingOutcome.OptionsColumn.ReadOnly = true;
            this.colHearingOutcome.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingOutcome.Visible = true;
            this.colHearingOutcome.VisibleIndex = 15;
            this.colHearingOutcome.Width = 104;
            // 
            // colHearingOutcomeDate
            // 
            this.colHearingOutcomeDate.Caption = "Hearing Outcome Date";
            this.colHearingOutcomeDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colHearingOutcomeDate.FieldName = "HearingOutcomeDate";
            this.colHearingOutcomeDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHearingOutcomeDate.Name = "colHearingOutcomeDate";
            this.colHearingOutcomeDate.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeDate.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeDate.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHearingOutcomeDate.Visible = true;
            this.colHearingOutcomeDate.VisibleIndex = 16;
            this.colHearingOutcomeDate.Width = 130;
            // 
            // colHearingOutcomeIssuedByPersonID
            // 
            this.colHearingOutcomeIssuedByPersonID.Caption = "Hearing Outcome Issued By Person ID";
            this.colHearingOutcomeIssuedByPersonID.FieldName = "HearingOutcomeIssuedByPersonID";
            this.colHearingOutcomeIssuedByPersonID.Name = "colHearingOutcomeIssuedByPersonID";
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeIssuedByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingOutcomeIssuedByPersonID.Width = 204;
            // 
            // colHearingOutcomeIssuedByPersonName
            // 
            this.colHearingOutcomeIssuedByPersonName.Caption = "Hearing Outcome Issued By Person";
            this.colHearingOutcomeIssuedByPersonName.FieldName = "HearingOutcomeIssuedByPersonName";
            this.colHearingOutcomeIssuedByPersonName.Name = "colHearingOutcomeIssuedByPersonName";
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeIssuedByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingOutcomeIssuedByPersonName.Visible = true;
            this.colHearingOutcomeIssuedByPersonName.VisibleIndex = 17;
            this.colHearingOutcomeIssuedByPersonName.Width = 190;
            // 
            // colAppealDate
            // 
            this.colAppealDate.Caption = "Appeal Date";
            this.colAppealDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colAppealDate.FieldName = "AppealDate";
            this.colAppealDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colAppealDate.Name = "colAppealDate";
            this.colAppealDate.OptionsColumn.AllowEdit = false;
            this.colAppealDate.OptionsColumn.AllowFocus = false;
            this.colAppealDate.OptionsColumn.ReadOnly = true;
            this.colAppealDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colAppealDate.Visible = true;
            this.colAppealDate.VisibleIndex = 18;
            this.colAppealDate.Width = 80;
            // 
            // colAppealHeardByPersonID
            // 
            this.colAppealHeardByPersonID.Caption = "Appeal Heard By Person ID";
            this.colAppealHeardByPersonID.FieldName = "AppealHeardByPersonID";
            this.colAppealHeardByPersonID.Name = "colAppealHeardByPersonID";
            this.colAppealHeardByPersonID.OptionsColumn.AllowEdit = false;
            this.colAppealHeardByPersonID.OptionsColumn.AllowFocus = false;
            this.colAppealHeardByPersonID.OptionsColumn.ReadOnly = true;
            this.colAppealHeardByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealHeardByPersonID.Width = 151;
            // 
            // colAppealHeardByPersonName
            // 
            this.colAppealHeardByPersonName.Caption = "Appeal Heard By Person";
            this.colAppealHeardByPersonName.FieldName = "AppealHeardByPersonName";
            this.colAppealHeardByPersonName.Name = "colAppealHeardByPersonName";
            this.colAppealHeardByPersonName.OptionsColumn.AllowEdit = false;
            this.colAppealHeardByPersonName.OptionsColumn.AllowFocus = false;
            this.colAppealHeardByPersonName.OptionsColumn.ReadOnly = true;
            this.colAppealHeardByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealHeardByPersonName.Visible = true;
            this.colAppealHeardByPersonName.VisibleIndex = 19;
            this.colAppealHeardByPersonName.Width = 137;
            // 
            // colAppealOutcomeID
            // 
            this.colAppealOutcomeID.Caption = "Appeal Outcome ID";
            this.colAppealOutcomeID.FieldName = "AppealOutcomeID";
            this.colAppealOutcomeID.Name = "colAppealOutcomeID";
            this.colAppealOutcomeID.OptionsColumn.AllowEdit = false;
            this.colAppealOutcomeID.OptionsColumn.AllowFocus = false;
            this.colAppealOutcomeID.OptionsColumn.ReadOnly = true;
            this.colAppealOutcomeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealOutcomeID.Width = 114;
            // 
            // colAppealOutcome
            // 
            this.colAppealOutcome.Caption = "Appeal Outcome";
            this.colAppealOutcome.FieldName = "AppealOutcome";
            this.colAppealOutcome.Name = "colAppealOutcome";
            this.colAppealOutcome.OptionsColumn.AllowEdit = false;
            this.colAppealOutcome.OptionsColumn.AllowFocus = false;
            this.colAppealOutcome.OptionsColumn.ReadOnly = true;
            this.colAppealOutcome.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealOutcome.Visible = true;
            this.colAppealOutcome.VisibleIndex = 20;
            this.colAppealOutcome.Width = 100;
            // 
            // colAppealNoteTakerName
            // 
            this.colAppealNoteTakerName.Caption = "Appeal Note Taker";
            this.colAppealNoteTakerName.FieldName = "AppealNoteTakerName";
            this.colAppealNoteTakerName.Name = "colAppealNoteTakerName";
            this.colAppealNoteTakerName.OptionsColumn.AllowEdit = false;
            this.colAppealNoteTakerName.OptionsColumn.AllowFocus = false;
            this.colAppealNoteTakerName.OptionsColumn.ReadOnly = true;
            this.colAppealNoteTakerName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealNoteTakerName.Visible = true;
            this.colAppealNoteTakerName.VisibleIndex = 21;
            this.colAppealNoteTakerName.Width = 110;
            // 
            // colAppealEmployeeRepresentativeName
            // 
            this.colAppealEmployeeRepresentativeName.Caption = "Appeal Employee Representative";
            this.colAppealEmployeeRepresentativeName.FieldName = "AppealEmployeeRepresentativeName";
            this.colAppealEmployeeRepresentativeName.Name = "colAppealEmployeeRepresentativeName";
            this.colAppealEmployeeRepresentativeName.OptionsColumn.AllowEdit = false;
            this.colAppealEmployeeRepresentativeName.OptionsColumn.AllowFocus = false;
            this.colAppealEmployeeRepresentativeName.OptionsColumn.ReadOnly = true;
            this.colAppealEmployeeRepresentativeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealEmployeeRepresentativeName.Visible = true;
            this.colAppealEmployeeRepresentativeName.VisibleIndex = 22;
            this.colAppealEmployeeRepresentativeName.Width = 180;
            // 
            // colExpiryDurationUnits
            // 
            this.colExpiryDurationUnits.Caption = "Expiry Duration Units";
            this.colExpiryDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric0DP;
            this.colExpiryDurationUnits.FieldName = "ExpiryDurationUnits";
            this.colExpiryDurationUnits.Name = "colExpiryDurationUnits";
            this.colExpiryDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDurationUnits.Visible = true;
            this.colExpiryDurationUnits.VisibleIndex = 23;
            this.colExpiryDurationUnits.Width = 122;
            // 
            // repositoryItemTextEditNumeric0DP
            // 
            this.repositoryItemTextEditNumeric0DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric0DP.Mask.EditMask = "#####0";
            this.repositoryItemTextEditNumeric0DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric0DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric0DP.Name = "repositoryItemTextEditNumeric0DP";
            // 
            // colExpiryDurationUnitDescriptorID
            // 
            this.colExpiryDurationUnitDescriptorID.Caption = "Expiry Unit Descriptor ID";
            this.colExpiryDurationUnitDescriptorID.FieldName = "ExpiryDurationUnitDescriptorID";
            this.colExpiryDurationUnitDescriptorID.Name = "colExpiryDurationUnitDescriptorID";
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnitDescriptorID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDurationUnitDescriptorID.Width = 139;
            // 
            // colExpiryDurationUnitDescriptor
            // 
            this.colExpiryDurationUnitDescriptor.Caption = "Expiry Unit Descriptor";
            this.colExpiryDurationUnitDescriptor.FieldName = "ExpiryDurationUnitDescriptor";
            this.colExpiryDurationUnitDescriptor.Name = "colExpiryDurationUnitDescriptor";
            this.colExpiryDurationUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnitDescriptor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDurationUnitDescriptor.Visible = true;
            this.colExpiryDurationUnitDescriptor.VisibleIndex = 24;
            this.colExpiryDurationUnitDescriptor.Width = 125;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.Caption = "Expiry Date";
            this.colExpiryDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 25;
            this.colExpiryDate.Width = 77;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 26;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colLive
            // 
            this.colLive.Caption = "Live";
            this.colLive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colLive.FieldName = "Live";
            this.colLive.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLive.Name = "colLive";
            this.colLive.OptionsColumn.AllowEdit = false;
            this.colLive.OptionsColumn.AllowFocus = false;
            this.colLive.OptionsColumn.ReadOnly = true;
            this.colLive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLive.Visible = true;
            this.colLive.VisibleIndex = 1;
            this.colLive.Width = 40;
            // 
            // colSanctionOutcome
            // 
            this.colSanctionOutcome.Caption = "Outcome";
            this.colSanctionOutcome.FieldName = "SanctionOutcome";
            this.colSanctionOutcome.Name = "colSanctionOutcome";
            this.colSanctionOutcome.OptionsColumn.AllowEdit = false;
            this.colSanctionOutcome.OptionsColumn.AllowFocus = false;
            this.colSanctionOutcome.OptionsColumn.ReadOnly = true;
            this.colSanctionOutcome.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSanctionOutcome.Visible = true;
            this.colSanctionOutcome.VisibleIndex = 6;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00186_Sanctions_SelectTableAdapter
            // 
            this.sp_HR_00186_Sanctions_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_Sanction
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Sanction";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Sanction";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Sanction_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00186SanctionsSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric0DP;
        private System.Windows.Forms.BindingSource spHR00186SanctionsSelectBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00186_Sanctions_SelectTableAdapter sp_HR_00186_Sanctions_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSanctionID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber5;
        private DevExpress.XtraGrid.Columns.GridColumn colERIssueTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colERIssueType;
        private DevExpress.XtraGrid.Columns.GridColumn colHRManagerID;
        private DevExpress.XtraGrid.Columns.GridColumn colHRManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusValue;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigatedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigatedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHeardByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeardByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingNoteTakerName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingEmployeeRepresentativeName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeIssuedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeIssuedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealHeardByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealHeardByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealNoteTakerName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealEmployeeRepresentativeName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colLive;
        private DevExpress.XtraGrid.Columns.GridColumn colSanctionOutcome;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_HR_Select_Staff : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";

        public string _Mode = "single";  // single or multiple //
        public string strPassedFilterType = "";
        public string strPassedInFilterRecordIDs = "";

        public int intPassedInPreFilterType = 0;  // 0 = All staff, 1 = KAMs, 2 = CMs, 3 Preferred Labour from OM Site Gritting Contract //
        public bool boolPassedInAllStaffEnabled = true;
        public bool boolPassedInJustKAMsEnabled = true;
        public bool boolPassedInJustCMsEnabled = true;

        public bool boolPassedInJustPreferredLabourEnabled = false;
        public int intPassedInSiteContractID = 0;

        public bool boolAllowSetAsDefault = false;  // Optional //

        public string strSetAsDefaultText = "";  // Optional //

        public int intOriginalStaffID = 0;
        public int intSelectedStaffID = 0;
        public string strOriginalStaffIDs = "";
        public string strSelectedStaffIDs = "";
        public string strSelectedStaffName = "";
        public bool boolSetAsDefault = false;
        
        BaseObjects.GridCheckMarksSelection selection1;
        private bool ibool_FormStillLoading = true;

        #endregion

        public frm_HR_Select_Staff()
        {
            InitializeComponent();
        }

        private void frm_HR_Select_Staff_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 50007701;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp09110_HR_Staff_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;

            checkEditAllStaff.Enabled = boolPassedInAllStaffEnabled;
            checkEditKAMs.Enabled = boolPassedInJustKAMsEnabled;
            checkEditCMs.Enabled = boolPassedInJustCMsEnabled;
            checkEditPreferredLabour.Enabled = boolPassedInJustPreferredLabourEnabled && intPassedInSiteContractID > 0;
            switch (intPassedInPreFilterType)
            {
                case 0:
                    {
                        checkEditAllStaff.Checked = true;
                    }
                    break;
                case 1:
                    {
                        checkEditKAMs.Checked = true;
                    }
                    break;
                case 2:
                    {
                        checkEditCMs.Checked = true;
                    }
                    break;
                case 3:
                    {
                        checkEditPreferredLabour.Checked = true;
                    }
                    break;
                default:
                    break;
            }

            LoadData();
            gridControl1.ForceInitialize();
            Set_SetAsDefault_Visibility();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;

                Array arrayRecords = strOriginalStaffIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["intStaffID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalStaffID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["intStaffID"], intOriginalStaffID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            ibool_FormStillLoading = false;
        }

        public override void PostLoadView(object objParameter)
        {
            Set_SetAsDefault_Visibility();
        }

        private void LoadData()
        {
            int intPreFilter = (checkEditAllStaff.Checked ? 0 : (checkEditKAMs.Checked ? 1 : (checkEditCMs.Checked ? 2 : 3)));

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            try
            {
                sp09110_HR_Staff_SelectTableAdapter.Fill(dataSet_HR_Core.sp09110_HR_Staff_Select, strPassedFilterType, strPassedInFilterRecordIDs, intPreFilter, intPassedInSiteContractID);
            }
            catch (Exception) { }
            view.EndUpdate();
        }

        private void Set_SetAsDefault_Visibility()
        {
            checkEditSetAsDefault.Visible = boolAllowSetAsDefault;
            checkEditSetAsDefault.Enabled = boolAllowSetAsDefault;
            if (boolAllowSetAsDefault && !string.IsNullOrWhiteSpace(strSetAsDefaultText)) checkEditSetAsDefault.Properties.Caption = strSetAsDefaultText;
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Staff Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedStaffIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedStaffID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Staff", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (_Mode != "single")
            {
                strSelectedStaffIDs = "";    // Reset any prior values first //
                strSelectedStaffName = "";  // Reset any prior values first //
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedStaffIDs += Convert.ToString(view.GetRowCellValue(i, "intStaffID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedStaffName = Convert.ToString(view.GetRowCellValue(i, "SurnameForename"));
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedStaffName += ", " + Convert.ToString(view.GetRowCellValue(i, "SurnameForename"));
                        }
                        intCount++;
                    }
                }
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    intSelectedStaffID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "intStaffID"));
                    strSelectedStaffName = view.GetRowCellValue(view.FocusedRowHandle, "Surname").ToString() + ": " + view.GetRowCellValue(view.FocusedRowHandle, "Forename").ToString();
                }
            }
            boolSetAsDefault = checkEditSetAsDefault.Checked;
        }

        private void checkEditAllStaff_CheckedChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;
            LoadData();
        }

        private void checkEditKAMs_CheckedChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;
            LoadData();
        }

        private void checkEditCMs_CheckedChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;
            LoadData();
        }

        private void checkEditPreferredLabour_CheckedChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;
            LoadData();
        }





    }
}


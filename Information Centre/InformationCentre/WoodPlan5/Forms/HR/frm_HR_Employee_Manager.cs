using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using DevExpress.Data.Filtering;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.HR;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public string strPassedInRecordIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //
        private int ShowSalaryData = 1;  // Controls if the Contracts grid returns salary info //
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private Utils.enmMainGrids _enmFocusedGrid = Utils.enmMainGrids.Employees;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateAddress;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateBonus;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateNextOfKin;
        public RefreshGridState RefreshGridViewStateAbsence;
        public RefreshGridState RefreshGridViewStateWorkingPattern;
        public RefreshGridState RefreshGridViewStateWorkingPatternHeader;
        public RefreshGridState RefreshGridViewStateDiscipline;
        public RefreshGridState RefreshGridViewStateTraining;
        public RefreshGridState RefreshGridViewStateDepartment;
        public RefreshGridState RefreshGridViewStateVetting;
        public RefreshGridState RefreshGridViewStateHolidayYear;
        public RefreshGridState RefreshGridViewStatePension;
        public RefreshGridState RefreshGridViewStateCRM;
        public RefreshGridState RefreshGridViewStateReference;
        public RefreshGridState RefreshGridViewStatePayRise;
        public RefreshGridState RefreshGridViewStateShares;
        public RefreshGridState RefreshGridViewStateAdminister;
        public RefreshGridState RefreshGridViewStateSubsistence;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedEmployeeRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedAddressRecordIDs = "";
        string i_str_AddedBonusRecordIDs = "";
        string i_str_AddedNextKinRecordIDs = "";
        string i_str_AddedAbsenceRecordIDs = "";
        string i_str_AddedWorkingPatternRecordIDs = "";
        string i_str_AddedWorkingPatternHeaderRecordIDs = "";
        string i_str_AddedDisciplineRecordIDs = "";
        string i_str_AddedTrainingIDs = "";
        string i_str_AddedDepartmentIDs = "";
        string i_str_AddedVettingIDs = ""; 
        string i_str_AddedHolidayYearRecordIDs = "";
        string i_str_AddedPensionRecordIDs = "";
        string i_str_AddedCRMRecordIDs = "";
        string i_str_AddedReferenceRecordIDs = "";
        string i_str_AddedPayRiseRecordIDs = "";
        string i_str_AddedSharesRecordIDs = "";
        string i_str_AddedAdministerRecordIDs = "";
        string i_str_AddedSubsistenceRecordIDs = "";

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        private const string _CONTRACT_STATUS_CURRENT_ONLY = "1";
        private const string _CONTRACT_STATUS_PREVIOUS_ONLY = "0";
        private const string _CONTRACT_STATUS_ALL = "0,1";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection5;
        private string i_str_select_absence_type_ids = "";
        private string i_str_select_absence_types = "";

        private string i_str_selected_department_ids = "";
        private string i_str_selected_departments = "";
        private string i_str_selected_employee_ids = "";
        private string i_str_selected_employees = "";
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;

        private DataSet_Selection DS_Selection1;

        private bool i_boolVettingData = false;
        private bool i_boolReferenceData = false;
        private bool i_boolAddressData = false;
        private bool i_boolNextOfKinData = false;
        private bool i_boolDeptsWorkedData = false;
        private bool i_boolWorkPatternData = false;
        private bool i_boolHolidayData = false;
        private bool i_boolShareData = false;
        private bool i_boolBonusData = false;
        private bool i_boolPayRiseData = false;
        private bool i_boolPensionData = false;
        private bool i_boolTrainingData = false;
        private bool i_boolDisciplineData = false;
        private bool i_boolCRMData = false;
        private bool i_boolAdministerData = false;
        private bool i_boolSubsistenceData = false;

        #endregion

        public frm_HR_Employee_Manager()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 9901;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridViewEmployee, strConnectionString);  // Used by DataSets //

            sp_HR_00114_Department_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00114_Department_Filter_ListTableAdapter.Fill(dataSet_HR_Core.sp_HR_00114_Department_Filter_List, "");
            gridControl2.ForceInitialize();

            sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            // Don't Fill Yet (in case we need to filter by department) //
            gridControl4.ForceInitialize();

            sp_HR_00077_Absence_Types_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00077_Absence_Types_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00077_Absence_Types_List_With_Blank);
            gridControl5.ForceInitialize();

            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            // Add record selection checkboxes to popup grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(9, "HR_QualificationCertificatePath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Qualification Certificate Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Qualification Certificate Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            ShowSalaryData = (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey) ? 0 : 1);

            sp09001_Employee_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewState1 = new RefreshGridState(gridViewEmployee, "EmployeeId");

            sp_HR_00009_GetEmployeeAddressesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateAddress = new RefreshGridState(gridViewAddresses, "AddressId");

            sp_HR_00172_Employee_BonusesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateBonus = new RefreshGridState(gridViewBonuses, "BonusID");

            sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateNextOfKin = new RefreshGridState(gridViewNextOfKin, "Id");

            sp_HR_00037_Get_Employee_AbsencesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateAbsence = new RefreshGridState(gridViewAbsences, "AbsenceID");

            sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateWorkingPatternHeader = new RefreshGridState(gridViewWorkingPatternHeader, "WorkingPatternHeaderID");

            sp_HR_00046_Get_Employee_Working_PatternsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateWorkingPattern = new RefreshGridState(gridViewWorkingPattern, "WorkingPatternID");

            sp_HR_00053_Get_Employee_SanctionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateDiscipline = new RefreshGridState(gridViewDiscipline, "SanctionID");

            sp09113_HR_Qualifications_For_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateTraining = new RefreshGridState(gridViewTraining, "QualificationID");

            sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateDepartment = new RefreshGridState(gridViewDepartmentWorked, "EmployeeDeptWorkedID");

            sp_HR_00131_Get_Employee_VettingTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateVetting = new RefreshGridState(gridViewVetting, "VettingID");

            sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateHolidayYear = new RefreshGridState(gridViewHolidayYear, "EmployeeHolidayYearID");

            sp_HR_00016_Pensions_For_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStatePension = new RefreshGridState(gridViewPension, "PensionID");

            sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateCRM = new RefreshGridState(gridViewCRM, "CRMID");

            sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateReference = new RefreshGridState(gridViewReference, "ReferenceID");

            sp_HR_00213_Employee_PayRisesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStatePayRise = new RefreshGridState(gridViewPayRise, "PayRiseID");

            sp_HR_00224_Employee_SharesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateShares = new RefreshGridState(gridViewShares, "SharesID");

            sp_HR_00248_Employee_AdministeringTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateAdminister = new RefreshGridState(gridViewAdminister, "EmployeeAdministratorId");

            sp_HR_00257_Employee_SubsistenceTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateSubsistence = new RefreshGridState(gridViewSubsistence, "SubsistenceID");

            if (strPassedInRecordIDs != "")  // Opened in drill-down mode //
            {
                beiDepartment.EditValue = "Custom Filter";
                beiEmployee.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }
            gridControlEmployee.ForceInitialize();
            sp01059_Core_Alphabet_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            sp01059_Core_Alphabet_ListTableAdapter.Fill(dataSet_Common_Functionality.sp01059_Core_Alphabet_List);

            popupContainerControlDepartments.Size = new System.Drawing.Size(312, 423);
            popupContainerControlEmployees.Size = new System.Drawing.Size(312, 423);

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

             _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
            Set_Linked_Page_Visibility();
        }

        private void frm_HR_Employee_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedEmployeeRecordIDs))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedDisciplineRecordIDs) || !string.IsNullOrEmpty(i_str_AddedNextKinRecordIDs) || !string.IsNullOrEmpty(i_str_AddedAddressRecordIDs) || !string.IsNullOrEmpty(i_str_AddedVettingIDs) || !string.IsNullOrEmpty(i_str_AddedDepartmentIDs) || !string.IsNullOrEmpty(i_str_AddedWorkingPatternHeaderRecordIDs) || !string.IsNullOrEmpty(i_str_AddedBonusRecordIDs) || !string.IsNullOrEmpty(i_str_AddedPensionRecordIDs) || !string.IsNullOrEmpty(i_str_AddedCRMRecordIDs) || !string.IsNullOrEmpty(i_str_AddedReferenceRecordIDs) || !string.IsNullOrEmpty(i_str_AddedPayRiseRecordIDs) || !string.IsNullOrEmpty(i_str_AddedSharesRecordIDs) || !string.IsNullOrEmpty(i_str_AddedAdministerRecordIDs))
                {
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedWorkingPatternRecordIDs))
                {
                    LoadLinkedWorkingPatterns();
                }
                if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedAbsenceRecordIDs))
                {
                    LoadLinkedAbsences();
                }
                if (UpdateRefreshStatus == 5 || !string.IsNullOrEmpty(i_str_AddedHolidayYearRecordIDs))
                {
                    LoadLinkedHolidayYears();
                }
            }
            SetMenuStatus();
        }

        private void frm_HR_Employee_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInRecordIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "AbsenceTypeFilter", i_str_select_absence_type_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ActiveHolidayYear", beiShowActiveHolidayYearOnly.EditValue.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "DepartmentFilter", i_str_selected_department_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "EmployeeFilter", i_str_selected_employee_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Locality Filter //
                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("AbsenceTypeFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayFilter = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayFilter)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    beiAbsenceType.EditValue = PopupContainerEdit1_Get_Selected();
                }

                strFilter = default_screen_settings.RetrieveSetting("ActiveHolidayYear");
                if (!(string.IsNullOrEmpty(strFilter)))
                {
                    beiShowActiveHolidayYearOnly.EditValue = (strFilter == "0" ? 0 : 1);
                }

                intFoundRow = 0;
                strFilter = default_screen_settings.RetrieveSetting("DepartmentFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array array = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl2.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in array)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["DepartmentID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    beiDepartment.EditValue = PopupContainerEdit2_Get_Selected();
                }

                Load_Employee_Filter();

                // Employee Filter //
                intFoundRow = 0;
                strFilter = default_screen_settings.RetrieveSetting("EmployeeFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array array = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl4.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in array)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["EmployeeID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    beiEmployee.EditValue = PopupContainerEdit3_Get_Selected();
                }

                bbiRefresh.PerformClick();

            }
        }

        private void Load_Employee_Filter()
        {
            // This filter list is controlled by the departments set - any selected departments are passed in to this SP //
            sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.Fill(dataSet_HR_Core.sp_HR_00115_Employees_By_Dept_Filter_List, (i_str_selected_department_ids == null ? "" : i_str_selected_department_ids));
        }

        private void Clear_Employee_filter()
        {
            i_str_selected_employee_ids = "";
            i_str_selected_employees = "";
            beiEmployee.EditValue = "No Employee Filter";
            selection3.ClearSelection();
        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            if (i_str_selected_department_ids == null) i_str_selected_department_ids = "";
            if (i_str_selected_employee_ids == null) i_str_selected_employee_ids = "";
            int intActive = Convert.ToInt32(beiActive.EditValue);
            GridView view = (GridView)gridControlEmployee.MainView;

            RefreshGridViewState1.SaveViewInfo();
            gridControlEmployee.BeginUpdate();
            try
            {
                if (beiDepartment.EditValue.ToString() == "Custom Filter" && strPassedInRecordIDs != "")  // Load passed in Records //
                {

                    sp09001_Employee_ManagerTableAdapter.Fill(dataSet_HR_Core.sp09001_Employee_Manager, 0, "", "", strPassedInRecordIDs, ShowSalaryData);
                    this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    view.ExpandAllGroups();
                }
                else // Load users selection //
                {
                    sp09001_Employee_ManagerTableAdapter.Fill(dataSet_HR_Core.sp09001_Employee_Manager, intActive, i_str_selected_department_ids, i_str_selected_employee_ids, "", ShowSalaryData);
                    this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }

            }
            catch (Exception ex) { }
            
            if (!string.IsNullOrEmpty(GlobalSettings.HRSecurityKey))  // Decrypt Employee Payament Info //
            {
                SimplerAES Encryptor = new SimplerAES();
                string strValue = "";
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp09001_Employee_Manager.Rows)
                    {
                        strValue = dr["LastIncreaseAmount"].ToString();
                        if (!(string.IsNullOrEmpty(strValue) || strValue == "0.00")) dr["LastIncreaseAmount"] = Encryptor.Decrypt(strValue);
                        strValue = dr["LastIncreasePercentage"].ToString();
                        if (!(string.IsNullOrEmpty(strValue) || strValue == "0.00")) dr["LastIncreasePercentage"] = Encryptor.Decrypt(strValue);
                        strValue = dr["Salary"].ToString();
                        if (!(string.IsNullOrEmpty(strValue) || strValue == "0.00")) dr["Salary"] = Encryptor.Decrypt(strValue);
                        strValue = dr["PaymentAmount"].ToString();
                        if (!(string.IsNullOrEmpty(strValue) || strValue == "0.00")) dr["PaymentAmount"] = Encryptor.Decrypt(strValue);

                        strValue = dr["MedicalCoverEmployerContribution"].ToString();
                        if (!(string.IsNullOrEmpty(strValue) || strValue == "0.00")) dr["MedicalCoverEmployerContribution"] = Encryptor.Decrypt(strValue);
                        strValue = dr["CTWSchemeValue"].ToString();
                        if (!(string.IsNullOrEmpty(strValue) || strValue == "0.00")) dr["CTWSchemeValue"] = Encryptor.Decrypt(strValue);

                        strValue = dr["PreviousEmploymentSalary"].ToString();
                        if (!(string.IsNullOrEmpty(strValue) || strValue == "0.00")) dr["PreviousEmploymentSalary"] = Encryptor.Decrypt(strValue);
                        strValue = dr["SalaryExpectation"].ToString();
                        if (!(string.IsNullOrEmpty(strValue) || strValue == "0.00")) dr["SalaryExpectation"] = Encryptor.Decrypt(strValue);

                    }
                }
                catch (Exception ex) { }
            }
            
            gridControlEmployee.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedEmployeeRecordIDs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedEmployeeRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlEmployee.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["EmployeeId"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedEmployeeRecordIDs = "";
            }
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlEmployee.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            SimplerAES Encryptor = new SimplerAES();
            string strAmount = "";

            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["EmployeeId"])) + ',';
            }

            // Populate Linked Address Records //
            if (i_boolAddressData)
            {
                gridControlAddresses.MainView.BeginUpdate();
                this.RefreshGridViewStateAddress.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_HR_Core.sp_HR_00009_GetEmployeeAddresses.Clear();
                }
                else
                {
                    sp_HR_00009_GetEmployeeAddressesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00009_GetEmployeeAddresses, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    this.RefreshGridViewStateAddress.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlAddresses.MainView.EndUpdate();
                xtraTabPageAddresses.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageAddresses.Text, "clear", 0), "add", gridControlAddresses.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedAddressRecordIDs != "")
                {
                    strArray = i_str_AddedAddressRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlAddresses.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["AddressId"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedAddressRecordIDs = "";
                }
            }


            // Populate Linked Sanction Records //
            if (i_boolDisciplineData)
            {
                gridControlDiscipline.MainView.BeginUpdate();
                RefreshGridViewStateDiscipline.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_HR_Core.sp_HR_00053_Get_Employee_Sanctions.Clear();
                }
                else
                {
                    sp_HR_00053_Get_Employee_SanctionsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00053_Get_Employee_Sanctions, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateDiscipline.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlDiscipline.MainView.EndUpdate();
                xtraTabPageDiscipline.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageDiscipline.Text, "clear", 0), "add", gridControlDiscipline.MainView.DataRowCount);


                if (i_str_AddedDisciplineRecordIDs != "")
                {
                    strArray = i_str_AddedDisciplineRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlDiscipline.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SanctionID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedDisciplineRecordIDs = "";
                }
            }


            // Populate Linked Bonuses //
            if (i_boolBonusData)
            {
                gridControlBonuses.MainView.BeginUpdate();
                RefreshGridViewStateBonus.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00172_Employee_Bonuses.Clear();
                }
                else
                {
                    sp_HR_00172_Employee_BonusesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00172_Employee_Bonuses, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), ShowSalaryData);
                    RefreshGridViewStateBonus.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00172_Employee_Bonuses.Rows)
                    {
                        strAmount = dr["GuaranteeBonusAmount"].ToString();
                        if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["GuaranteeBonusAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["BonusAmountPaid"].ToString();
                        if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["BonusAmountPaid"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }

                gridControlBonuses.MainView.EndUpdate();
                xtraTabPageBonuses.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageBonuses.Text, "clear", 0), "add", gridControlBonuses.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedBonusRecordIDs != "")
                {
                    strArray = i_str_AddedBonusRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlBonuses.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["BonusID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedBonusRecordIDs = "";
                }
            }


            // Populate Next of Kin records //
            if (i_boolNextOfKinData)
            {
                gridControlNextOfKin.MainView.BeginUpdate();
                RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00021_Get_Next_Of_Kin_For_Employees.Clear();
                }
                else
                {
                    sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00021_Get_Next_Of_Kin_For_Employees, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateNextOfKin.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlNextOfKin.MainView.EndUpdate();
                xtraTabPageNextOfKin.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageNextOfKin.Text, "clear", 0), "add", gridControlNextOfKin.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedNextKinRecordIDs != "")
                {
                    strArray = i_str_AddedNextKinRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlNextOfKin.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["Id"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedNextKinRecordIDs = "";
                }
            }


            // Populate Linked Training \ Qualifications //
            if (i_boolTrainingData)
            {
                gridControlTraining.MainView.BeginUpdate();
                RefreshGridViewStateTraining.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp09113_HR_Qualifications_For_Employee.Clear();
                }
                else
                {
                    sp09113_HR_Qualifications_For_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp09113_HR_Qualifications_For_Employee, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateTraining.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlTraining.MainView.EndUpdate();
                xtraTabPageTraining.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageTraining.Text, "clear", 0), "add", gridControlTraining.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedTrainingIDs != "")
                {
                    strArray = i_str_AddedTrainingIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlTraining.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["QualificationID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedTrainingIDs = "";
                }


                // Populate Linked Vetting //
                gridControlVetting.MainView.BeginUpdate();
                RefreshGridViewStateVetting.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_HR_Core.sp_HR_00131_Get_Employee_Vetting.Clear();
                }
                else
                {
                    sp_HR_00131_Get_Employee_VettingTableAdapter.Fill(dataSet_HR_Core.sp_HR_00131_Get_Employee_Vetting, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateVetting.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlVetting.MainView.EndUpdate();
                xtraTabPageVetting.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageVetting.Text, "clear", 0), "add", gridControlVetting.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedVettingIDs != "")
                {
                    strArray = i_str_AddedVettingIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlVetting.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["VettingID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedVettingIDs = "";
                }
            }


            // Populate Linked Departments Worked //
            if (i_boolDeptsWorkedData)
            {
                gridControlDepartmentWorked.MainView.BeginUpdate();
                RefreshGridViewStateDepartment.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00001_Employee_Depts_Worked_Linked_To_Contract.Clear();
                }
                else
                {
                    sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter.Fill(dataSet_HR_Core.sp_HR_00001_Employee_Depts_Worked_Linked_To_Contract, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateDepartment.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlDepartmentWorked.MainView.EndUpdate();
                xtraTabPageDeptsWorked.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageDeptsWorked.Text, "clear", 0), "add", gridControlDepartmentWorked.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedDepartmentIDs != "")
                {
                    strArray = i_str_AddedDepartmentIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlDepartmentWorked.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["EmployeeDeptWorkedID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedDepartmentIDs = "";
                }
            }


            // Populate Linked Working Pattern Headers //
            if (i_boolWorkPatternData)
            {
                gridControlWorkingPatternHeader.MainView.BeginUpdate();
                RefreshGridViewStateWorkingPatternHeader.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_HR_Core.sp_HR_00154_Employee_Working_Pattern_Headers.Clear();
                }
                else
                {
                    sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter.Fill(dataSet_HR_Core.sp_HR_00154_Employee_Working_Pattern_Headers, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateWorkingPatternHeader.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlWorkingPatternHeader.MainView.EndUpdate();
                xtraTabPageWorkPatterns.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageWorkPatterns.Text, "clear", 0), "add", gridControlWorkingPatternHeader.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedWorkingPatternHeaderRecordIDs != "")
                {
                    strArray = i_str_AddedWorkingPatternHeaderRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlWorkingPatternHeader.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["WorkingPatternHeaderID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedWorkingPatternHeaderRecordIDs = "";
                }
            }


            // Populate Linked Pensions //
            if (i_boolPensionData)
            {
                gridControlPension.MainView.BeginUpdate();
                RefreshGridViewStatePension.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00016_Pensions_For_Employee.Clear();
                }
                else
                {
                    sp_HR_00016_Pensions_For_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp_HR_00016_Pensions_For_Employee, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), ShowSalaryData);
                    RefreshGridViewStatePension.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                Encryptor = new SimplerAES();
                strAmount = "";
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00016_Pensions_For_Employee.Rows)
                    {
                        strAmount = dr["MinimumContributionAmount"].ToString();
                        if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["MinimumContributionAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["MaximumContributionAmount"].ToString();
                        if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["MaximumContributionAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["CurrentContributionAmount"].ToString();
                        if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["CurrentContributionAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["TotalEmployeeContributionToDate"].ToString();
                        if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["TotalEmployeeContributionToDate"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["TotalEmployerContributionToDate"].ToString();
                        if (!(string.IsNullOrEmpty(strAmount) || strAmount == "0.00")) dr["TotalEmployerContributionToDate"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }

                gridControlPension.MainView.EndUpdate();
                xtraTabPagePensions.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPagePensions.Text, "clear", 0), "add", gridControlPension.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedPensionRecordIDs != "")
                {
                    strArray = i_str_AddedPensionRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlPension.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["PensionID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedPensionRecordIDs = "";
                }
            }


            // Populate CRM records //
            if (i_boolCRMData)
            {
                gridControlCRM.MainView.BeginUpdate();
                RefreshGridViewStateCRM.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00193_Employee_CRM_Linked_To_Employee.Clear();
                }
                else
                {
                    sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp_HR_00193_Employee_CRM_Linked_To_Employee, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateCRM.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlCRM.MainView.EndUpdate();
                xtraTabPageCRM.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageCRM.Text, "clear", 0), "add", gridControlCRM.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedCRMRecordIDs != "")
                {
                    strArray = i_str_AddedCRMRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlCRM.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["CRMID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedCRMRecordIDs = "";
                }
            }


            // Populate Reference records //
            if (i_boolReferenceData)
            {
                gridControlReference.MainView.BeginUpdate();
                RefreshGridViewStateReference.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00200_Employee_References_Linked_To_Employee.Clear();
                }
                else
                {
                    sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp_HR_00200_Employee_References_Linked_To_Employee, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateReference.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlReference.MainView.EndUpdate();
                xtraTabPageReferences.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageReferences.Text, "clear", 0), "add", gridControlReference.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedReferenceRecordIDs != "")
                {
                    strArray = i_str_AddedReferenceRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlReference.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["ReferenceID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedReferenceRecordIDs = "";
                }
            }


            // Populate Linked Pay Rises //
            if (i_boolPayRiseData)
            {
                gridControlPayRise.MainView.BeginUpdate();
                RefreshGridViewStatePayRise.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00213_Employee_PayRises.Clear();
                }
                else
                {
                    sp_HR_00213_Employee_PayRisesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00213_Employee_PayRises, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), ShowSalaryData);
                    RefreshGridViewStatePayRise.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00213_Employee_PayRises.Rows)
                    {
                        strAmount = dr["PreviousPayAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["PreviousPayAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["PayRiseAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["PayRiseAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["PayRisePercentage"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["PayRisePercentage"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["NewPayAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["NewPayAmount"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }

                gridControlPayRise.MainView.EndUpdate();
                xtraTabPagePayRises.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPagePayRises.Text, "clear", 0), "add", gridControlPayRise.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedPayRiseRecordIDs != "")
                {
                    strArray = i_str_AddedPayRiseRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlPayRise.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["PayRiseID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedPayRiseRecordIDs = "";
                }
            }


            // Populate Linked Shares //
            if (i_boolShareData)
            {
                gridControlShares.MainView.BeginUpdate();
                RefreshGridViewStateShares.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00224_Employee_Shares.Clear();
                }
                else
                {
                    sp_HR_00224_Employee_SharesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00224_Employee_Shares, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), ShowSalaryData);
                    RefreshGridViewStateShares.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00224_Employee_Shares.Rows)
                    {
                        strAmount = dr["UnitValue"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["UnitValue"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }

                gridControlShares.MainView.EndUpdate();
                xtraTabPageShares.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageShares.Text, "clear", 0), "add", gridControlShares.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedSharesRecordIDs != "")
                {
                    strArray = i_str_AddedSharesRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlShares.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SharesID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedSharesRecordIDs = "";
                }
            }


            // Populate Linked Administer //
            if (i_boolAdministerData)
            {
                gridControlAdminister.MainView.BeginUpdate();
                RefreshGridViewStateAdminister.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00248_Employee_Administering.Clear();
                }
                else
                {
                    sp_HR_00248_Employee_AdministeringTableAdapter.Fill(dataSet_HR_Core.sp_HR_00248_Employee_Administering, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    RefreshGridViewStateAdminister.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlAdminister.MainView.EndUpdate();
                xtraTabPageAdminister.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageAdminister.Text, "clear", 0), "add", gridControlAdminister.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedAdministerRecordIDs != "")
                {
                    strArray = i_str_AddedAdministerRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlAdminister.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["EmployeeAdministratorId"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedAdministerRecordIDs = "";
                }
            }

            // Populate Linked Subsistence //
            if (i_boolSubsistenceData)
            {
                gridControlSubsistence.MainView.BeginUpdate();
                RefreshGridViewStateSubsistence.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_HR_Core.sp_HR_00257_Employee_Subsistence.Clear();
                }
                else
                {
                    sp_HR_00257_Employee_SubsistenceTableAdapter.Fill(dataSet_HR_Core.sp_HR_00257_Employee_Subsistence, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), ShowSalaryData);
                    RefreshGridViewStateSubsistence.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00213_Employee_PayRises.Rows)
                    {
                        strAmount = dr["Rate1"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["Rate1"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["Rate2"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["Rate2"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["SalarySacrificeAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["SalarySacrificeAmount"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }
                
                gridControlSubsistence.MainView.EndUpdate();
                xtraTabPageSubsistence.Text = ExtensionFunctions.PageFramePageTextRecordCount(ExtensionFunctions.PageFramePageTextRecordCount(xtraTabPageSubsistence.Text, "clear", 0), "add", gridControlSubsistence.MainView.DataRowCount);

                // Highlight any recently added new rows //
                if (i_str_AddedSubsistenceRecordIDs != "")
                {
                    strArray = i_str_AddedSubsistenceRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlSubsistence.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SubsistenceID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedSubsistenceRecordIDs = "";
                }
            }

        }

        private void LoadLinkedWorkingPatterns()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            var view = (GridView)gridControlWorkingPatternHeader.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["WorkingPatternHeaderID"])) + ',';
            }

            gridControlWorkingPattern.MainView.BeginUpdate();
            this.RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00046_Get_Employee_Working_Patterns.Clear();
            }
            else
            {
                sp_HR_00046_Get_Employee_Working_PatternsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00046_Get_Employee_Working_Patterns, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewStateWorkingPattern.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlWorkingPattern.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedWorkingPatternRecordIDs != "")
            {
                strArray = i_str_AddedWorkingPatternRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlWorkingPattern.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["WorkingPatternID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedWorkingPatternRecordIDs = "";
            }
        }

        private void LoadLinkedHolidayYears()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlEmployee.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["EmployeeId"])) + ',';
            }

            //Populate Holiday Year records //
            gridControlHolidayYear.MainView.BeginUpdate();
            RefreshGridViewStateHolidayYear.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_HR_Core.sp_HR_00158_Holiday_Years_For_Employee.Clear();
            }
            else
            {
                sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp_HR_00158_Holiday_Years_For_Employee, Convert.ToInt32(beiShowActiveHolidayYearOnly.EditValue), (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateHolidayYear.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlHolidayYear.MainView.EndUpdate();

            // Highlight any recently added new rows //
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            if (i_str_AddedHolidayYearRecordIDs != "")
            {
                strArray = i_str_AddedHolidayYearRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlHolidayYear.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["EmployeeHolidayYearID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedHolidayYearRecordIDs = "";
            }
        }

        private void LoadLinkedAbsences()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            var view = (GridView)gridControlHolidayYear.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["EmployeeHolidayYearID"])) + ',';
            }

            gridControlAbsences.MainView.BeginUpdate();
            this.RefreshGridViewStateAbsence.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00037_Get_Employee_Absences.Clear();
            }
            else
            {
                sp_HR_00037_Get_Employee_AbsencesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00037_Get_Employee_Absences, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), Convert.ToInt32(beiShowActiveHolidayYearOnly.EditValue), i_str_select_absence_type_ids);
                this.RefreshGridViewStateAbsence.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlAbsences.MainView.EndUpdate();

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;
            // Highlight any recently added new rows //
            if (i_str_AddedAbsenceRecordIDs != "")
            {
                strArray = i_str_AddedAbsenceRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlAbsences.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["Id"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedAbsenceRecordIDs = "";
            }
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmMainGrids grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmMainGrids.Employees:
                    i_str_AddedEmployeeRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedEmployeeRecordIDs : newIds);
                    break;
                case Utils.enmMainGrids.Addresses:
                    i_str_AddedAddressRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedAddressRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.Bonus:
                    i_str_AddedBonusRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedBonusRecordIDs : newIds);
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    i_str_AddedWorkingPatternHeaderRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedWorkingPatternHeaderRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.WorkPattern:
                     i_str_AddedWorkingPatternRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedWorkingPatternRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    i_str_AddedDepartmentIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedDepartmentIDs : newIds);
                   break;
                case Utils.enmMainGrids.NextOfKin:
                    i_str_AddedNextKinRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedNextKinRecordIDs : newIds);
                    break;
                case Utils.enmMainGrids.Training:
                    i_str_AddedTrainingIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedTrainingIDs : newIds);
                    break;
                case Utils.enmMainGrids.Vetting:
                    i_str_AddedVettingIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedVettingIDs : newIds);
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    i_str_AddedHolidayYearRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedHolidayYearRecordIDs : newIds);
                    break;
                case Utils.enmMainGrids.Absences:
                     i_str_AddedAbsenceRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedAbsenceRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.Pensions:
                    i_str_AddedPensionRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedPensionRecordIDs : newIds);
                    break;
                case Utils.enmMainGrids.CRM:
                     i_str_AddedCRMRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedCRMRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.Reference:
                   i_str_AddedReferenceRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedReferenceRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.PayRise:
                   i_str_AddedPayRiseRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedPayRiseRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.Shares:
                   i_str_AddedSharesRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedSharesRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.Administer:
                   i_str_AddedAdministerRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedAdministerRecordIDs : newIds);
                   break;
                case Utils.enmMainGrids.Subsistence:
                   i_str_AddedSubsistenceRecordIDs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedSubsistenceRecordIDs : newIds);
                   break;
                default:
                    break;
            }
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_AllowAdd = sfpPermissions.blCreate;
                        iBool_AllowEdit = sfpPermissions.blUpdate;
                        iBool_AllowDelete = sfpPermissions.blDelete;
                        break;
                    case 2:  // Pension Data //
                        i_boolPensionData = sfpPermissions.blRead;
                        break;
                    case 3:  // Discipline \ Reward Data //
                        i_boolDisciplineData = sfpPermissions.blRead;
                        break;
                    case 4:  // Training Data //
                        i_boolTrainingData = sfpPermissions.blRead;
                        break;
                    case 5:  // CRM Data //
                        i_boolCRMData = sfpPermissions.blRead;
                        break;
                    case 6:  // Holiday Data //
                        i_boolHolidayData = sfpPermissions.blRead;
                        break;
                    case 7:  // Departments Worked Data //
                        i_boolDeptsWorkedData = sfpPermissions.blRead;
                        break;
                    case 8:  // Reference Data //
                        i_boolReferenceData = sfpPermissions.blRead;
                        break;
                    case 9:  // Vetting Data //
                        i_boolVettingData = sfpPermissions.blRead;
                        break;
                    case 10:  // Bonus Data //
                        i_boolBonusData = sfpPermissions.blRead;
                        break;
                    case 11:  // Pay Rise Data //
                        i_boolPayRiseData = sfpPermissions.blRead;
                        break;
                    case 12:  // Share Data //
                        i_boolShareData = sfpPermissions.blRead;
                        break;
                    case 13:  // Administer Data //
                        i_boolAdministerData = sfpPermissions.blRead;
                        break;
                    case 14:  // Subsistence Data //
                        i_boolSubsistenceData = sfpPermissions.blRead;
                        break;
                    case 101:  // Address Data //
                        i_boolAddressData = sfpPermissions.blRead;
                        break;
                    case 102:  // /next Of Kin Data //
                        i_boolNextOfKinData = sfpPermissions.blRead;
                        break;
                    case 103:  // Work Pattern Data //
                        i_boolWorkPatternData = sfpPermissions.blRead;
                        break;
                    default:
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            bsiAuditTrail.Enabled = false;
            bbiViewAuditTrail.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControlEmployee.MainView;
            GridView ParentView = (GridView)gridControlEmployee.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    view = (GridView)gridControlEmployee.MainView;
                    break;
                case Utils.enmMainGrids.Addresses:
                    view = (GridView)gridControlAddresses.MainView;
                    break;
                case Utils.enmMainGrids.Bonus:
                    view = (GridView)gridControlBonuses.MainView;
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    view = (GridView)gridControlNextOfKin.MainView;
                    break;
                case Utils.enmMainGrids.Discipline:
                    view = (GridView)gridControlDiscipline.MainView;
                    break;
                case Utils.enmMainGrids.Training:
                     view = (GridView)gridControlTraining.MainView;
                   break;
                case Utils.enmMainGrids.WorkPattern:
                   view = (GridView)gridControlWorkingPattern.MainView;
                   break;
                case Utils.enmMainGrids.WorkPatternHeader:
                   view = (GridView)gridControlWorkingPatternHeader.MainView;
                   break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    view = (GridView)gridControlDepartmentWorked.MainView;
                    break;
                case Utils.enmMainGrids.Absences:
                    view = (GridView)gridControlAbsences.MainView;
                    break;
                case Utils.enmMainGrids.Vetting:
                    view = (GridView)gridControlVetting.MainView;
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    view = (GridView)gridControlHolidayYear.MainView;
                    break;
                case Utils.enmMainGrids.Pensions:
                    view = (GridView)gridControlPension.MainView;
                    break;
                case Utils.enmMainGrids.CRM:
                    view = (GridView)gridControlCRM.MainView;
                    break;
                case Utils.enmMainGrids.Reference:
                    view = (GridView)gridControlReference.MainView;
                    break;
                case Utils.enmMainGrids.PayRise:
                    view = (GridView)gridControlPayRise.MainView;
                    break;
                case Utils.enmMainGrids.Shares:
                    view = (GridView)gridControlShares.MainView;
                    break;
                case Utils.enmMainGrids.Administer:
                    view = (GridView)gridControlAdminister.MainView;
                    break;
                case Utils.enmMainGrids.Subsistence:
                    view = (GridView)gridControlSubsistence.MainView;
                    break;
                default:
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();
            int[] intParentRowHandles = ParentView.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    bsiAuditTrail.Enabled = (intRowHandles.Length > 0);
                    bbiViewAuditTrail.Enabled = (intRowHandles.Length > 0);
                    break;
                case Utils.enmMainGrids.Addresses:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                case Utils.enmMainGrids.Subsistence:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                        else
                        {
                            bbiBlockAdd.Enabled = false;
                        }
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        //GridView viewParent = (GridView)gridControlEmployee.MainView;
                        //int[] intRowHandlesParent;
                        //intRowHandlesParent = viewParent.GetSelectedRows();
                        //if (intRowHandlesParent.Length >= 2)
                        //{
                        //    alItems.Add("iBlockAdd");
                        //    bbiBlockAdd.Enabled = true;
                        //}
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                    }
                    //bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                   break;
                case Utils.enmMainGrids.DepartmentsWorked:
                   if (iBool_AllowAdd)
                   {
                       alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                       bsiAdd.Enabled = true;
                       bbiSingleAdd.Enabled = true;

                       GridView viewParent = (GridView)gridControlEmployee.MainView;
                       int[] intRowHandlesParent;
                       intRowHandlesParent = viewParent.GetSelectedRows();
                       if (intRowHandlesParent.Length >= 2)
                       {
                           alItems.Add("iBlockAdd");
                           bbiBlockAdd.Enabled = true;
                       }
                   }
                   //bbiBlockAdd.Enabled = false;
                   if (iBool_AllowEdit && intRowHandles.Length >= 1)
                   {
                       alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                       bsiEdit.Enabled = true;
                       bbiSingleEdit.Enabled = true;
                       if (intRowHandles.Length >= 2)
                       {
                           alItems.Add("iBlockEdit");
                           bbiBlockEdit.Enabled = true;
                       }
                   }
                   if (iBool_AllowDelete && intRowHandles.Length >= 1)
                   {
                       alItems.Add("iDelete");
                       bbiDelete.Enabled = true;
                   }
                   break;
                case Utils.enmMainGrids.Absences:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        //if (intRowHandlesParent.Length >= 2)
                        //{
                        //    alItems.Add("iBlockAdd");
                        //    bbiBlockAdd.Enabled = false;
                        //}
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    bbiBlockAdd.Enabled = false;
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                        else
                        {
                            bbiBlockAdd.Enabled = false;
                        }
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                        else
                        {
                            bbiBlockAdd.Enabled = false;
                        }
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                        else
                        {
                            bbiBlockAdd.Enabled = false;
                        }
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.PensionContributions:
                    break;
                case Utils.enmMainGrids.PayRise:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        //if (intRowHandles.Length >= 2)
                        //{
                        //    alItems.Add("iBlockEdit");
                        //    bbiBlockEdit.Enabled = true;
                        //}
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                        else
                        {
                            bbiBlockAdd.Enabled = false;
                        }
                    }
                    break;
                case Utils.enmMainGrids.Administer:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;

                        GridView viewParent = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandlesParent;
                        intRowHandlesParent = viewParent.GetSelectedRows();
                        if (intRowHandlesParent.Length >= 2)
                        {
                            alItems.Add("iBlockAdd");
                            bbiBlockAdd.Enabled = true;
                        }
                        else
                        {
                            bbiBlockAdd.Enabled = false;
                        }
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            view = (GridView)gridControlEmployee.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlEmployee.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlEmployee.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControlEmployee.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControlEmployee.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlEmployee.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);
           
            view = (GridView)gridControlTraining.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlBonuses.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlWorkingPatternHeader.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = intParentRowHandles.Length > 0;

            view = (GridView)gridControlWorkingPattern.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)gridControlDepartmentWorked.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlDepartmentWorked.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlDepartmentWorked.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlDepartmentWorked.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlDepartmentWorked.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlDepartmentWorked.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlAbsences.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0)); 
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlDiscipline.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlAddresses.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = intRowHandles.Length == 1;

            view = (GridView)gridControlNextOfKin.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)gridControlVetting.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);
           
            view = (GridView)gridControlHolidayYear.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

            view = (GridView)gridControlPension.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlCRM.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlReference.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlPayRise.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0)); 
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlShares.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1 ? true : false);

            view = (GridView)gridControlAdminister.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlAdminister.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlAdminister.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowAdd && (intParentRowHandles.Length > 0));
            gridControlAdminister.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlAdminister.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlAdminister.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = intRowHandles.Length > 0;
            gridControlAdminister.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length > 0 ? true : false);

            view = (GridView)gridControlSubsistence.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlSubsistence.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlSubsistence.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && (intRowHandles.Length > 0));
            gridControlSubsistence.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && (intRowHandles.Length > 0));
            gridControlSubsistence.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Addresses:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlAddresses.MainView;
                        view.PostEditor();
               
                        var fChildForm = new frm_HR_Employee_Address_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.EmployeeId;
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                      
                        var fChildForm = new frm_HR_Employee_Next_Kin_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.EmployeeId;
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                      
                        var fChildForm = new frm_HR_Sanction_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.EmployeeId;
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Bonus_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)ParentView.GetDataRow(intRowHandles[0]);
                            fChildForm.intLinkedToRecordID = row.EmployeeId;         // Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeId"));
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.EmployeeId;
                            fChildForm.strLinkedToRecordDesc = row.Surname + ": " + row.Firstname;
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlWorkingPattern.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlWorkingPatternHeader.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp_HR_00154_Employee_Working_Pattern_HeadersRow)rowView.Row;
                            fChildForm.ParentRecordId = row.WorkingPatternHeaderID;
                            fChildForm.ParentRecordDescription = row.Description;
                            fChildForm.ParentEmployeeID = row.EmployeeID;
                            fChildForm.ParentRecordEmployeeName = row.EmployeeName;
                            fChildForm.ParentRecordStartDate = row.StartDate;
                            fChildForm.ParentRecordEndDate = row.EndDate;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlDepartmentWorked.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Department_Worked_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)rowView.Row;
                            fChildForm.ParentRecordId = row.EmployeeId;
                            fChildForm.ParentRecordDescription = row.Surname + ": " + row.Firstname;
                            fChildForm.JobTitleID = row.JobTitleID;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.HolidayYear:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlHolidayYear.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Holiday_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.ParentRecordId = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeId"));
                            fChildForm.ParentRecordDescription = ParentView.GetRowCellValue(intRowHandles[0], "Surname").ToString() + ": " + ParentView.GetRowCellValue(intRowHandles[0], "Firstname").ToString();
                            fChildForm.ParentRecordHolidays = Convert.ToDecimal(ParentView.GetRowCellValue(intRowHandles[0], "BasicLeaveEntitlement"));
                            fChildForm.ParentRecordBankHolidays = Convert.ToDecimal(ParentView.GetRowCellValue(intRowHandles[0], "BankHolidayEntitlement"));
                            fChildForm.ParentHolidayUnitDescriptorId = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "HolidayUnitDescriptorID"));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.Absences:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlAbsences.MainView;
                        view.PostEditor();
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Absence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlHolidayYear.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.ParentRecordId = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeHolidayYearID"));
                            fChildForm.ParentRecordDescription = ParentView.GetRowCellValue(intRowHandles[0], "EmployeeSurnameForename").ToString() + "  -  Holiday Year: " + ParentView.GetRowCellValue(intRowHandles[0], "HolidayYearDescription").ToString();
                            fChildForm.ParentEmployeeID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeID"));
                        }
                        else
                        {
                            fChildForm.ParentRecordId = 0;
                            fChildForm.ParentRecordDescription = "";
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.Training:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        int intPersonType = 0;
                        frm_HR_Qualification_Add_Type frm_Child = new frm_HR_Qualification_Add_Type();
                        frm_Child.ShowDialog();
                        switch (frm_Child._SelectedType)
                        {
                            case -1:
                                return;
                            default:
                                intPersonType = frm_Child._SelectedType;
                                break;
                        }

                        frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = intPersonType;

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            //fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                            //fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" :ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString());
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlVetting.MainView;
                        view.PostEditor();
                      
                        var fChildForm = new frm_HR_Employee_Vetting_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.EmployeeId;
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Pension_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)ParentView.GetDataRow(intRowHandles[0]);
                            fChildForm.intLinkedToRecordID = row.EmployeeId;         // Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeId"));
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)ParentView.GetDataRow(intRowHandles[0]);
                            fChildForm.intLinkedToRecordID = row.EmployeeId;         // Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeId"));
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlReference.MainView;
                        view.PostEditor();
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Reference_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)ParentView.GetDataRow(intRowHandles[0]);
                            fChildForm.intLinkedToRecordID = row.EmployeeId;         // Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeId"));
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlPayRise.MainView;
                        view.PostEditor();
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Pay_Rise_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)ParentView.GetDataRow(intRowHandles[0]);
                            fChildForm.intLinkedToRecordID = row.EmployeeId;         // Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeId"));
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlShares.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Shares_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)ParentView.GetDataRow(intRowHandles[0]);
                            fChildForm.intLinkedToRecordID = row.EmployeeId;         // Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeId"));
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Administer:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlAdminister.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Administer_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.EmployeeId;
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Subsistence:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlSubsistence.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_HR_Employee_Subsistence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var row = (DataSet_HR_Core.sp09001_Employee_ManagerRow)ParentView.GetDataRow(intRowHandles[0]);
                            fChildForm.intLinkedToRecordID = row.EmployeeId;         // Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeId"));
                            fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", row.Surname, row.Firstname);
                            fChildForm.strLinkedToRecordDesc2 = row.Surname;
                            fChildForm.strLinkedToRecordDesc3 = row.Firstname;
                            fChildForm.strLinkedToRecordDesc4 = row.EmployeeNumber;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            frmProgress fProgress = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Addresses:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }

                        var fChildForm1 = new frm_HR_Employee_Address_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        GridView ParentView = null;
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        int intPersonType = 0;
                        frm_HR_Qualification_Add_Type frm_Child = new frm_HR_Qualification_Add_Type();
                        frm_Child.ShowDialog();
                        switch (frm_Child._SelectedType)
                        {
                            case -1:
                                return;
                            default:
                                intPersonType = frm_Child._SelectedType;
                                break;
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = intPersonType;

                        ParentView = (GridView)gridControlEmployee.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            //fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                            //fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" :ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString());
                        }

                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }

                        var fChildForm1 = new frm_HR_Employee_Bonus_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }

                        var fChildForm1 = new frm_HR_Employee_Pension_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //

                        var fChildForm1 = new frm_HR_Employee_CRM_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //

                        var fChildForm1 = new frm_HR_Employee_Reference_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one employee record to block add a Pay Rise to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        string strAmount = "";
                        string strSalary = "0.00";
                        int intEmployeeID = 0;
                        int intDeselectedCount = 0;
                        using (var GetValue = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                        {
                            GetValue.ChangeConnectionString(strConnectionString);

                            try
                            {
                                foreach (int intRowHandle in intRowHandles)
                                {
                                    intEmployeeID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "EmployeeId"));
                                    strRecordIDs += intEmployeeID.ToString() + ',';

                                    // Make sure all selected rows have a salary set against them. Un-select them if not. //
                                    strSalary = GetValue.sp_HR_00218_Employee_PayRises_Get_Current_Pay(ShowSalaryData, intEmployeeID).ToString();
                                    if (string.IsNullOrWhiteSpace(strSalary) || strSalary == "0.00")
                                    {
                                        strAmount = "0.00";
                                    }
                                    else
                                    {
                                        SimplerAES Encryptor = new SimplerAES();
                                        strAmount = Encryptor.Decrypt(strSalary);
                                    }
                                    if (string.IsNullOrWhiteSpace(strAmount) || strAmount == "0.00")
                                    {
                                        intDeselectedCount++;
                                        view.UnselectRow(intRowHandle);
                                    }
                                }
                            }
                            catch (Exception) { }
                        }

                        // Check there is still at least 1 row selected - warn user and abort if not. //
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one employee record to block add a Pay Rise to before proceeding.\n\nNote: This process will automatically de-select any employees without a current salary.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Warn user that one or more records have been de-selected and allow them to abort. //
                        if (intDeselectedCount > 0)
                        {
                            string strMessage = intDeselectedCount.ToString() + " Employee " + (intDeselectedCount == 1 ? "record" : "records") + " have been de-selected. No Pay Rise will be added to any de-selected record.\n\nRecords are de-selected if they have no salary value set against them.\n\nProceed with block adding a pay rise to the remaining selected record(s)?";
                            if (XtraMessageBox.Show(strMessage, "Block Add Pay Rises", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;
                        }

                        RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Pay_Rise_Block_Add_Get_Amount();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intRecordCount = intCount;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Adding...");

                            DateTime dtDateApplied = fChildForm.dtDateApplied;
                            decimal decPayRiseAmount = fChildForm.decPayRiseAmount;
                            int intAppliedByStaffID = fChildForm.intAppliedByStaffID;
                            int intPayRiseType = fChildForm.intPayRiseType;  // 0 = Percentage, 1 = Amount //

                            // Create Pay rise record //
                            decimal decNewSalary = (decimal)0.00;
                            decimal decRiseAmount = (decimal)0.00;
                            decimal decRisePercentage = (decimal)0.00;
                            string strNewSalary = "";
                            string strRiseAmount = "";
                            string strRisePercentage = "";

                            i_str_AddedPayRiseRecordIDs = "";
                            char[] delimiters = new char[] { ',' };
                            string[] strArray = strRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

                            var CreateRecord = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter();
                            CreateRecord.ChangeConnectionString(strConnectionString);

                            using (var GetValue = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                GetValue.ChangeConnectionString(strConnectionString);

                                try
                                {
                                    SimplerAES Encryptor = new SimplerAES();
                                    foreach (string strElement in strArray)
                                    {
                                        decNewSalary = (decimal)0.00;
                                        decRiseAmount = (decimal)0.00;
                                        decRisePercentage = (decimal)0.00;
                                        strNewSalary = "";
                                        strRiseAmount = "";
                                        strRisePercentage = "";

                                        intEmployeeID = Convert.ToInt32(strElement);

                                        strSalary = GetValue.sp_HR_00218_Employee_PayRises_Get_Current_Pay(ShowSalaryData, intEmployeeID).ToString();
                                        if (string.IsNullOrWhiteSpace(strSalary) || strSalary == "0.00")
                                        {
                                            continue;  // Abort this person //
                                        }
                                        else
                                        {
                                            strAmount = Encryptor.Decrypt(strSalary);
                                        }
                                        if (string.IsNullOrWhiteSpace(strAmount) || strAmount == "0.00")
                                        {
                                            continue;  // Abort this person //
                                        }

                                        if (intPayRiseType == 0)  // Percentage //
                                        {
                                            decRiseAmount = ((Convert.ToDecimal(strAmount) * decPayRiseAmount) / 100);
                                            decRiseAmount = Math.Round(decRiseAmount, 2, MidpointRounding.AwayFromZero);
                                            decRisePercentage = decPayRiseAmount;
                                            decNewSalary = Convert.ToDecimal(strAmount) + decRiseAmount;
                                        }
                                        else  // Amount //
                                        {
                                            decRiseAmount = decPayRiseAmount;
                                            decNewSalary = (Convert.ToDecimal(strAmount) + decPayRiseAmount);
                                            decRisePercentage = (decPayRiseAmount / Convert.ToDecimal(strAmount)) * 100;
                                            decRisePercentage = Math.Round(decRisePercentage, 2, MidpointRounding.AwayFromZero);
                                        }
                                        // Now encrypt values //
                                        strNewSalary = Encryptor.Encrypt(decNewSalary.ToString());
                                        strRiseAmount = Encryptor.Encrypt(decRiseAmount.ToString());
                                        strRisePercentage = Encryptor.Encrypt(decRisePercentage.ToString());
                                        i_str_AddedPayRiseRecordIDs += CreateRecord.sp_HR_00219_Employee_PayRise_Insert(intEmployeeID, strSalary, strRiseAmount, strRisePercentage, strNewSalary, dtDateApplied, intAppliedByStaffID, "").ToString() + ";";
                                    }
                                }
                                catch (Exception ex) 
                                {
                                    if (splashScreenManager1.IsSplashFormVisible)
                                    {
                                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                        splashScreenManager1.CloseWaitForm();
                                    }
                                    XtraMessageBox.Show("An error occurred while generating employee pay rises.\n\nEmployee ID: " + intEmployeeID.ToString() + "\n\nError: " + ex.Message + "\n\nYou will need to double check which pay rises were created and which were not and if neccessary, remove any newly created records then repeat the process.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    Load_Data();
                                    return;
                                }
                                if (splashScreenManager1.IsSplashFormVisible)
                                {
                                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                    splashScreenManager1.CloseWaitForm();
                                }
                                Load_Data();
                            }
                        }
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }

                        var fChildForm1 = new frm_HR_Employee_Shares_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Administer:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Employee record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }
                        var fChildForm1 = new frm_HR_Employee_Administer_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Subsistence:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }

                        var fChildForm1 = new frm_HR_Employee_Subsistence_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = "";
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add_Multiple_Record()
        {
            GridView view = (GridView)gridControlEmployee.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Employees to block add the Administer to before proceeding.", "Block Add Employee Administer", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intRowHandles.Length > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intRowHandles.Length.ToString() + " Employees selected. If you proceed one or more new Employee Administer records will be created for each of these employees.\n\nProceed?", "Block Add Employee Administer", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }

            string strRecordIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
            }

            int intEmployeeAdministratorId = 0;
            var fChildForm = new frm_HR_Select_Employee();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._Mode = "multiple";
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                var fChildForm2 = new frm_HR_Employee_Administer_Block_Add_Info();
                fChildForm2.GlobalSettings = this.GlobalSettings;
                if (fChildForm2.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    i_str_AddedAdministerRecordIDs = "";
                    string strEmployeeIDs = fChildForm.strSelectedEmployeeIDs;
                    DateTime? dtDateFrom = fChildForm2.dtDateFrom;
                    DateTime? dtDateTo = fChildForm2.dtDateTo;
                    int? intActive = fChildForm2.intActive;
                    int? intType = fChildForm2.intTypeID;
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intEmployeeAdministratorId = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "EmployeeId"));

                        using (var AddRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                        {
                            AddRecords.ChangeConnectionString(strConnectionString);
                            try
                            {
                                i_str_AddedAdministerRecordIDs += AddRecords.sp_HR_00253_Block_Add_Employee_Administer(strEmployeeIDs, intEmployeeAdministratorId, dtDateFrom, dtDateTo, intActive, intType).ToString() + ";";
                            }
                            catch (Exception Ex) { }
                        }

                    }
                }
                LoadLinkedRecords();
            }
        }


        private void Block_Edit()
        {
            GridView view = null;
            frmProgress fProgress = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }
                       
                        var fChildForm = new frm_HR_Employee_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Addresses:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlAddresses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AddressId")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Address_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }

                        frm_HR_Employee_Next_Kin_Edit fChildForm = new frm_HR_Employee_Next_Kin_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlDiscipline.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SanctionID")) + ',';
                        }

                        var fChildForm = new frm_HR_Sanction_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BonusID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Bonus_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternHeaderID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWorkingPattern.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlDepartmentWorked.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeDeptWorkedID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Department_Worked_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.HolidayYear:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlHolidayYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeHolidayYearID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Holiday_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.Absences:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlAbsences.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AbsenceID")) + ',';
                        }
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Absence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.Vetting:
                    {
                        //if (!iBool_AllowEditGBMLink) return;
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlVetting.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VettingID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Vetting_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Pension_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlReference.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ReferenceID")) + ',';
                        }
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Reference_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPayRise.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PayRiseID")) + ',';
                        }
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Pay_Rise_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlShares.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SharesID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Shares_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Administer:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlAdminister.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeAdministratorId")) + ',';
                        }
                        var fChildForm = new frm_HR_Employee_Administer_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Subsistence:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlSubsistence.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubsistenceID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Subsistence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Addresses:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlAddresses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AddressId")) + ',';
                        }
                       
                        var fChildForm = new frm_HR_Employee_Address_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlNextOfKin.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }
                        
                        var fChildForm = new frm_HR_Employee_Next_Kin_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlDiscipline.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SanctionID")) + ',';
                        }

                        var fChildForm = new frm_HR_Sanction_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BonusID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Bonus_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternHeaderID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWorkingPattern.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlDepartmentWorked.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeDeptWorkedID")) + ',';
                        }
                       
                        var fChildForm = new frm_HR_Employee_Department_Worked_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }

                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlHolidayYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeHolidayYearID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Holiday_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Absences:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlAbsences.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AbsenceID")) + ',';
                        }
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Absence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlVetting.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VettingID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Vetting_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Pension_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlReference.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ReferenceID")) + ',';
                        }
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Reference_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPayRise.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PayRiseID")) + ',';
                        }
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Pay_Rise_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlShares.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SharesID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Shares_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Administer:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlAdminister.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeAdministratorId")) + ',';
                        }
                        var fChildForm = new frm_HR_Employee_Administer_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Subsistence:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlSubsistence.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubsistenceID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Subsistence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            var singleItem="";
            var pluralItems="";
            var usePlural=false;
            var itemText="";

            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlEmployee.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Employees to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Employee" : Convert.ToString(intRowHandles.Length) + " Employees") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Employee" : "these Employees") + " will no longer be available for selection and any related records will also be deleted!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "EmployeeId")) + ",";
                            }

                            this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Addresses:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlAddresses.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Addresses to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Address" : Convert.ToString(intRowHandles.Length) + " Linked Employee Addresses") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Address" : "these Employee Addresses") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "AddressId")) + ",";
                            }

                            RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //
                            
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_address", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlDiscipline.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Discipline/Grievance Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Discipline/Grievance" : Convert.ToString(intRowHandles.Length) + " Linked Employee Discipline/Grievances") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Discipline/Grievance" : "these Employee Discipline/Grievances") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SanctionID")) + ",";
                            }

                            RefreshGridViewStateDiscipline.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_sanction", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlBonuses.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Bonuses to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Bonus" : Convert.ToString(intRowHandles.Length) + " Linked Employee Bonuses") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Bonus" : "these Employee Bonuses") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "BonusID")) + ",";
                            }

                            RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_bonus", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlNextOfKin.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Next of Kin Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Next of Kin Record" : Convert.ToString(intRowHandles.Length) + " Linked Next of Kin Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Next of Kin Record" : "these Linked Next of Kin Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "Id")) + ",";
                            }

                            RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("next_of_kin", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        singleItem = "Work Pattern Header";
                        pluralItems = "Work Pattern Headers";
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show(string.Format("Select one or more Linked {0} to delete by clicking on them then try again.", pluralItems), "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        usePlural = (intCount == 1);

                        if (usePlural)
                            itemText = singleItem;
                        else
                            itemText = pluralItems;

                        // Checks passed so delete selected record(s) //
                        //strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Address" : Convert.ToString(intRowHandles.Length) + " Linked Employee Addresses") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Employee Address" : "these Employee Addresses") + " will no longer be available for selection!";
                        strMessage = "You have " + Convert.ToString(intRowHandles.Length) + " Linked " + itemText + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (usePlural ? "this Linked " : "these ") + itemText + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "WorkingPatternHeaderID")) + ",";
                            }

                            RefreshGridViewStateWorkingPatternHeader.SaveViewInfo();  // Store Grid View State //
                            RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store Grid View State //
                            
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("work_pattern_header", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;

                case Utils.enmMainGrids.WorkPattern:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlWorkingPattern.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        singleItem = "Work Pattern";
                        pluralItems = "Work Patterns";
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show(string.Format("Select one or more Linked {0} to delete by clicking on them then try again.", pluralItems), "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        usePlural = (intCount == 1);

                        if (usePlural)
                            itemText = singleItem;
                        else
                            itemText = pluralItems;

                        // Checks passed so delete selected record(s) //
                        //strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Address" : Convert.ToString(intRowHandles.Length) + " Linked Employee Addresses") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Linked Employee Address" : "these Employee Addresses") + " will no longer be available for selection!";
                        strMessage = "You have " + Convert.ToString(intRowHandles.Length) + " Linked " + itemText + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (usePlural ? "this Linked " : "these ") + itemText + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "WorkingPatternID")) + ",";
                            }

                            RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store Grid View State //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("work_pattern", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinkedWorkingPatterns();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;

                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlDepartmentWorked.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Departments Worked Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Department Worked Record" : Convert.ToString(intRowHandles.Length) + " Linked Department Worked Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Department Worked Record" : "these Linked Department Worked Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "EmployeeDeptWorkedID")) + ",";
                            }

                            RefreshGridViewStateDepartment.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("department_worked", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;

                case Utils.enmMainGrids.Training:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlTraining.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Qualifications to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Qualification" : Convert.ToString(intRowHandles.Length) + " Qualifications") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Qualification" : "these Qualifications") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "QualificationID")) + ",";
                            }
                            RefreshGridViewStateTraining.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("qualification", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case Utils.enmMainGrids.HolidayYear:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlHolidayYear.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Holiday Years to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Holiday Year" : Convert.ToString(intRowHandles.Length) + " Holiday Years") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Holiday Year" : "these Holiday Years") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "EmployeeHolidayYearID")) + ",";
                            }

                            RefreshGridViewStateHolidayYear.SaveViewInfo();  // Store Grid View State //
                            RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_holiday_year", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedHolidayYears();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case Utils.enmMainGrids.Absences:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlAbsences.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Absences to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Absence" : Convert.ToString(intRowHandles.Length) + " Absences") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Absence" : "these Absences") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "AbsenceID")) + ",";
                            }

                            RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_absence", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedAbsences();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case Utils.enmMainGrids.Vetting:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlVetting.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Vetting Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Vetting Record" : Convert.ToString(intRowHandles.Length) + " Linked Employee Vetting Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Vetting Record" : "these Employee Vetting Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "VettingID")) + ",";
                            }

                            RefreshGridViewStateVetting.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_vetting", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlPension.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Pensions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Pension" : Convert.ToString(intRowHandles.Length) + " Linked Employee Pensions") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Pension" : "these Employee Pensions") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PensionID")) + ",";
                            }

                            RefreshGridViewStatePension.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_pension", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlCRM.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee CRM Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee CRM Record" : Convert.ToString(intRowHandles.Length) + " Linked Employee CRM Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee CRM Record" : "these Employee CRM Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "CRMID")) + ",";
                            }

                            RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_crm", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlReference.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Reference Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Reference Record" : Convert.ToString(intRowHandles.Length) + " Linked Employee Reference Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Reference Record" : "these Employee Reference Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ReferenceID")) + ",";
                            }

                            RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_Reference", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlPayRise.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Pay Rise Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Pay Rise" : Convert.ToString(intRowHandles.Length) + " Linked Employee Pay Rises") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Pay Rise" : "these Employee Pay Rises") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PayRiseID")) + ",";
                            }

                            RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_pay_rise", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlShares.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Shares to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Share" : Convert.ToString(intRowHandles.Length) + " Linked Employee Shares") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Share" : "these Employee Shares") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SharesID")) + ",";
                            }

                            RefreshGridViewStateShares.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_shares", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Administer:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlAdminister.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Administer Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Administer Record" : Convert.ToString(intRowHandles.Length) + " Linked Administer Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Administer Record" : "these Linked Administer Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "EmployeeAdministratorId")) + ",";
                            }

                            RefreshGridViewStateShares.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_administer", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Subsistence:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlSubsistence.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Subsistence to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Subsistence" : Convert.ToString(intRowHandles.Length) + " Linked Employee Subsistence") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Subsistence" : "these Employee Subsistence") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SubsistenceID")) + ",";
                            }

                            RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_Subsistence", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }            
        }

        private void View_Record()
        {
            GridView view = null;
            frmProgress fProgress = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    {
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Addresses:
                    {
                        view = (GridView)gridControlAddresses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AddressId")) + ',';
                        }
                        
                        var fChildForm = new frm_HR_Employee_Address_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        view = (GridView)gridControlNextOfKin.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }
                       
                        var fChildForm = new frm_HR_Employee_Next_Kin_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        //fChildForm2.intRecordTypeID = 1;  // Districts //
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        view = (GridView)gridControlDiscipline.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SanctionID")) + ',';
                        }
                       
                        var fChildForm = new frm_HR_Sanction_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BonusID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Bonus_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternHeaderID")) + ',';
                        }

                        var fChildFormWp = new frm_HR_Employee_Working_Pattern_Header_Edit();
                        fChildFormWp.MdiParent = this.MdiParent;
                        fChildFormWp.GlobalSettings = this.GlobalSettings;
                        fChildFormWp.strRecordIDs = strRecordIDs;
                        fChildFormWp.strFormMode = "view";
                        fChildFormWp.strCaller = this.Name;
                        fChildFormWp.intRecordCount = intCount;
                        fChildFormWp.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildFormWp.splashScreenManager = splashScreenManager1;
                        fChildFormWp.splashScreenManager.ShowWaitForm();
                        fChildFormWp.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildFormWp, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlWorkingPattern.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternID")) + ',';
                        }

                        var fChildFormWp = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildFormWp.MdiParent = this.MdiParent;
                        fChildFormWp.GlobalSettings = this.GlobalSettings;
                        fChildFormWp.strRecordIDs = strRecordIDs;
                        fChildFormWp.strFormMode = "view";
                        fChildFormWp.strCaller = this.Name;
                        fChildFormWp.intRecordCount = intCount;
                        fChildFormWp.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildFormWp.splashScreenManager = splashScreenManager1;
                        fChildFormWp.splashScreenManager.ShowWaitForm();
                        fChildFormWp.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildFormWp, new object[] { null });
                    }
                    break;

                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlDepartmentWorked.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeDeptWorkedID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }
                       
                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    {
                        view = (GridView)gridControlHolidayYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeHolidayYearID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Holiday_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Absences:
                    {
                        view = (GridView)gridControlAbsences.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AbsenceID")) + ',';
                        }
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Absence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    {
                        view = (GridView)gridControlVetting.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VettingID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Vetting_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Pension_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlReference.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ReferenceID")) + ',';
                        }
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Reference_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPayRise.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PayRiseID")) + ',';
                        }
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Pay_Rise_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlShares.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SharesID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Shares_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Administer:
                    {
                        view = (GridView)gridControlAdminister.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeAdministratorId")) + ',';
                        }
                        var fChildForm = new frm_HR_Employee_Administer_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Subsistence:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlSubsistence.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SubsistenceID")) + ',';
                        }

                        var fChildForm = new frm_HR_Employee_Subsistence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }


        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewEmployee":
                    message = "No Employees - Adjust any filters then click Load Data";
                    break;
                case "gridViewAddresses":
                    message = "No Addresses - Select one or more Employees to see linked Addresses";
                    break;
                case "gridViewNextOfKin":
                    message = "No Next of Kin - Select one or more Employees to see linked Next of Kin";
                    break;
                case "gridControlTraining":
                    message = "No Training - Select one or more Employees to see linked Training";
                    break;
                case "gridViewDiscipline":
                    message = "No Discipline and Grievances - Select one or more Employees to see linked Discipline and Grievances";
                    break;
                case "gridViewVetting":
                    message = "No Vetting - Select one or more Employees to see linked Vetting";
                    break;
                case "gridViewDeptWorked":
                    message = "No Departments Worked In - Select one or more Employee to see linked Departments Worked In";
                    break;
                case "gridViewWorkingPatternHeader":
                    message = "No Working Pattern Header - Select one or more Employees to see linked Working Pattern Headers";
                    break;
                case "gridViewWorkingPattern":
                    message = "No Working Patterns - Select one or more Work Pattern Headers to see linked Working Patterns";
                    break;
                case "gridViewHolidayYear":
                    message = "No Holiday Years - Select one or more Employees to see linked Holiday Years";
                    break;
                case "gridViewAbsences":
                    message = "No Absences - Select one or more Employee Holiday Years to see linked Absences";
                    break;
                case "gridViewBonuses":
                    message = "No Bonuses - Select one or more Employees to see linked Bonuses";
                    break;
                case "gridViewPension":
                    message = "No Pensions - Select one or more Employees to see linked Pensions";
                    break;
                case "gridViewCRM":
                    message = "No CRM - Select one or more Employees to see linked CRM";
                    break;
                case "gridViewReferences":
                    message = "No References - Select one or more Employees to see linked References";
                    break;
                case "gridViewPayRise":
                    message = "No Pay Rises - Select one or more Employees to see linked Pay Rises";
                    break;
                case "gridViewShares":
                    message = "No Shares - Select one or more Employees to see linked Shares";
                    break;
                case "gridViewAdminister":
                    message = "No Administering -Select one or more Employees to see linked Administering";
                    break;
                case "gridViewSubsistence":
                    message = "No Subsistence - Select one or more Employees to see linked Subsistence";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewEmployee":
                    LoadLinkedRecords();
                    view = (GridView)gridControlTraining.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlAddresses.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlBonuses.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlNextOfKin.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlTraining.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlDiscipline.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlVetting.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlDepartmentWorked.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlWorkingPatternHeader.MainView;
                    view.ExpandAllGroups();

                    LoadLinkedHolidayYears();
                    view = (GridView)gridControlHolidayYear.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlPension.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlCRM.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlReference.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlPayRise.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlShares.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlAdminister.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlSubsistence.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridViewWorkingPatternHeader":
                    LoadLinkedWorkingPatterns();
                    view = (GridView)gridControlWorkingPattern.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridViewHolidayYear":
                    LoadLinkedAbsences();
                    view = (GridView)gridControlAbsences.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView Employees

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Employees;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Employees;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetSelectionInverted.Enabled = true;
                bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewEmployee;
                        //int intRecordType = 1;  // Employee //
                        int intRecordType = -1;  // Use dummy value so we can run branch in SP to return back all linked documents to do with the employee (employee, bonuses, holidays, discipline etc) //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "EmployeeId"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "Surname").ToString() + ": " + view.GetRowCellValue(view.FocusedRowHandle, "Firstname").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "PurchasedHolidaysRemaining":
                case "TotalHolidaysRemaining":
                    if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, e.Column.FieldName)) <= (decimal)0.00)
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "StaffRecordCreated":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "StaffRecordCreated")) != 0) e.RepositoryItem = emptyEditor;
                    break;
                case "BasicLeaveEntitlementFTE":
                case "BasicLeaveEntitlement":
                case "BankHolidayEntitlementFTE":
                case "BankHolidayEntitlement":

                case "DefaultBankHolidayEntitlement":
                case "DefaultBasicLeaveEntitlement":
                case "DefaultYearlyLeaveIncrement":
                case "HolidaysTaken":
                case "BankHolidaysTaken":
                case "PurchasedHolidaysTaken":
                case "AbsenceAuthorisedTaken":
                case "AbsenceUnauthorisedTaken":
                case "PurchasedHolidayEntitlement":
                case "TotalHolidayEntitlement":
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "PurchasedHolidaysRemaining":
                case "TotalHolidaysRemaining":
                    if (view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptor").ToString() == "Days")
                    {
                        e.RepositoryItem = repositoryItemTextEditDays;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptor").ToString() == "Hours")
                    {
                        e.RepositoryItem = repositoryItemTextEditHours;
                    }
                    else
                    {
                        e.RepositoryItem = repositoryItemTextEditUnknown;
                    }
                    break;

                case "SickPayEntitlementUnits":
                    if (view.GetRowCellValue(e.RowHandle, "SickPayUnitDescriptor").ToString() == "Days")
                    {
                        e.RepositoryItem = repositoryItemTextEditDays;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "SickPayUnitDescriptor").ToString() == "Hours")
                    {
                        e.RepositoryItem = repositoryItemTextEditHours;
                    }
                    else
                    {
                        e.RepositoryItem = repositoryItemTextEditUnknown;
                    }
                    break;

                case "MedicalCoverEmployerContribution":
                    if (view.GetRowCellValue(e.RowHandle, "MedicalCoverEmployerContributionDescriptor").ToString() == "�")
                    {
                        e.RepositoryItem = repositoryItemTextEditMoney;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "MedicalCoverEmployerContributionDescriptor").ToString() == "%")
                    {
                        e.RepositoryItem = repositoryItemTextEditPercentage2;
                    }
                    else
                    {
                        e.RepositoryItem = emptyEditor;
                    }
                    break;


                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;

                default:
                    break;
            }
        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "StaffRecordCreated":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("StaffRecordCreated")) != 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemButtonEditCreateStaff_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString())
            {
                case "create":
                    {
                        GridView view = gridViewEmployee;
                        if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
                        if (XtraMessageBox.Show("Are you sure you wish to create a Staff Record for the selected employee?", "Create Staff Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

                        int intEmployeeID = Convert.ToInt32(view.GetFocusedRowCellValue("EmployeeId"));

                        RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateAddress.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateDiscipline.SaveViewInfo();  // Store Grid View State //

                        string strReturnedValues = "";
                        using (var CreateRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                        {
                            CreateRecords.ChangeConnectionString(strConnectionString);
                            try
                            {
                                strReturnedValues = CreateRecords.sp_HR_00123_Create_Staff_Record(intEmployeeID).ToString();
                            }
                            catch (Exception) { }
                        }
                        if (!string.IsNullOrWhiteSpace(strReturnedValues))
                        {
                            char[] delimiters = new char[] { '|' };
                            string[] ReturnedValues = strReturnedValues.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (ReturnedValues.Length == 2)
                            {
                                try
                                {
                                    view.SetFocusedRowCellValue("StaffName", ReturnedValues[0].ToString());
                                    view.SetFocusedRowCellValue("StaffID", Convert.ToInt32(ReturnedValues[1]));
                                    view.SetFocusedRowCellValue("StaffRecordCreated", 1);
                                    gridViewEmployee.CloseEditor();  // Force editor to update itself so the button no longer shows //
                                }
                                catch (Exception) { }
                            }
                        }
                        //Load_Data();
                    }
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocuments_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            //int intRecordType = 1;  // Employee //
            int intRecordType = -1;  // Use dummy value so we can run branch in SP to return back all linked documents to do with the employee (employee, bonuses, holidays, discipline etc) //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "EmployeeId"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "Surname").ToString() + ": " + view.GetRowCellValue(view.FocusedRowHandle, "Firstname").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Addresses

        private void gridControlAddresses_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("copy".Equals(e.Button.Tag))
                    {
                        string strCopiedText = "";
                        string strName = "";
                        GridView view = (GridView)gridControlAddresses.MainView;
                        GridView parentView = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandles = view.GetSelectedRows();
                        int[] intParentRowHandles = parentView.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            XtraMessageBox.Show("Select just one address record to copy to the Clipbopard before proceeding.", "Copy Address to Clipboard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strName = (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(intParentRowHandles[0], "Title").ToString()) ? "" : parentView.GetRowCellValue(intParentRowHandles[0], "Title").ToString() + " ") + (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(intParentRowHandles[0], "Firstname").ToString()) ? "" : parentView.GetRowCellValue(intParentRowHandles[0], "Firstname").ToString() + " ") + (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(intParentRowHandles[0], "Surname").ToString()) ? "" : parentView.GetRowCellValue(intParentRowHandles[0], "Surname").ToString());

                            strCopiedText += (string.IsNullOrWhiteSpace(strName) ? "" : strName + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine1").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine1").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine2").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine2").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine3").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine3").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine4").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine4").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine5").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine5").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "PostCode").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "PostCode").ToString() + "\n");
                        }
                        if (!string.IsNullOrWhiteSpace(strCopiedText)) strCopiedText.TrimEnd('\n');
                        if (string.IsNullOrWhiteSpace(strCopiedText))
                        {
                            XtraMessageBox.Show("The current address record has no text to copy to the Clipboard.", "Copy Address to Clipboard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        Clipboard.SetText(strCopiedText);
                        XtraMessageBox.Show("Address copied to the Clipboard.\n\nUse Ctrl + V or right click and select Paste to copy the address into applications such as Microsoft Word and Excel.", "Copy Address to Clipboard", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewAddresses_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewAddresses_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Addresses;
            SetMenuStatus();
        }

        private void gridViewAddresses_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Addresses;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView Next Of Kin

        private void gridControlNextOfKin_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    GridView view = (GridView)gridControlNextOfKin.MainView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewNextOfKin_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.NextOfKin;
            SetMenuStatus();
        }

        private void gridViewNextOfKin_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.NextOfKin;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewNextOfKin_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
      
        #endregion


        #region GridView Bonuses

        private void gridControlBonuses_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlBonuses.MainView;
                        int intRecordType = 10;  // Bonuses //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BonusID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "BonusDueDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewBonuses_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewBonuses_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Bonus;
            SetMenuStatus();
        }

        private void gridViewBonuses_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Bonus;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewBonuses_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewBonuses_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewBonuses_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditBonusLinkedDocuments_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 10;  // Bonus //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BonusID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "BonusDueDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }


        #endregion


        #region GridView Dicipline \ Grievances

        private void gridControlDiscipline_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlDiscipline.MainView;
                        int intRecordType = 3;  // Sanctions //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SanctionID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateRaised")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewDiscipline_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StatusValue")
            {
                string strStatus = view.GetRowCellValue(e.RowHandle, "StatusValue").ToString();
                if (strStatus.ToLower() == "open")
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }
        
        private void gridViewDiscipline_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewDiscipline_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Discipline;
            SetMenuStatus();
        }

        private void gridViewDiscipline_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Discipline;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewDiscipline_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewDiscipline_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentSanction_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 3;  // Sanctions //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SanctionID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateRaised")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion

 
        #region GridView Vetting

        private void gridControlVetting_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewVetting;
                        int intRecordType = 9;  // Vetting //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VettingID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "VettingType").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridViewVetting_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewVetting_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Vetting;
            SetMenuStatus();
        }

        private void gridViewVetting_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Vetting;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewVetting_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "ExpiryDate")
            {
                int intExpiredStatus = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ExpiredStatus"));
                if (intExpiredStatus >= 1)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewVetting_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "RestrictionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RestrictionCount")) <= 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewVetting_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "RestrictionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RestrictionCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditVettingLinkedDocs_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 9;  // Vetting //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VettingID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "VettingType").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Working Pattern Header

        private void gridControlWorkingPatternHeader_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("add master".Equals(e.Button.Tag))
                    {
                        if (!iBool_AllowAdd) return;
                        GridView ParentView = (GridView)gridControlEmployee.MainView;
                        int[] intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length <= 0)
                        {
                            XtraMessageBox.Show("Select one or more employee records to create a shift pattern for before proceeding.", "Create Employee Shift Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Add_From_Master();
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        string strIDs = "";
                        string strNames = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strIDs += Convert.ToString(ParentView.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                            strNames += Convert.ToString(ParentView.GetRowCellValue(intRowHandle, "Firstname")) + " " + Convert.ToString(ParentView.GetRowCellValue(intRowHandle, "Surname")) + ", ";
                        }
                        strNames = strNames.Substring(0, strNames.Length - 2);  // Remove last comma from end //
                        fChildForm._PassedInEmployeeIDs = strIDs;
                        fChildForm._PassedInEmployeeNames = strNames;

                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                        {
                            if (!string.IsNullOrWhiteSpace(fChildForm._NewWorkingPatternHeaderIDs))
                            {
                                i_str_AddedWorkingPatternHeaderRecordIDs = fChildForm._NewWorkingPatternHeaderIDs;
                                LoadLinkedRecords();
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewWorkingPatternHeader_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewWorkingPatternHeader_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.WorkPatternHeader;
            SetMenuStatus();
        }

        private void gridViewWorkingPatternHeader_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.WorkPatternHeader;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewWorkingPatternHeader_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        #endregion


        #region GridView Working Pattern

        private void gridControlWorkingPattern_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    GridView view = (GridView)gridControlWorkingPattern.MainView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewWorkingPattern_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewWorkingPattern_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.WorkPattern;
            SetMenuStatus();
        }

        private void gridViewWorkingPattern_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.WorkPattern;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView Department Worked

        private void gridControlDepartmentWorked_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlDepartmentWorked.MainView;
                        int intRecordType = 7;  // Dept Worked //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "EmployeeDeptWorkedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeSurnameForename").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "DepartmentName").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewDepartmentWorked_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewDepartmentWorked_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.DepartmentsWorked;
            SetMenuStatus();
        }
        
        private void gridViewDepartmentWorked_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.DepartmentsWorked;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewDepartmentWorked_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewDepartmentWorked_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentDeptWorked_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 7;  // Dept Worked //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "EmployeeDeptWorkedID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeSurnameForename").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "DepartmentName").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Training

        private void gridControlTraining_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlTraining.MainView;
                        int intRecordType = 4;  // Training //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewTraining_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewTraining_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Training;
            SetMenuStatus();
        }

        private void gridViewTraining_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Training;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewTraining_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DaysUntilExpiry" )
            //   || e.Column == colDaysUntilNonCompliant)
            {
                int intDaysToDo = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, e.Column));
                if (intDaysToDo < 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysToDo < 30)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewTraining_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DaysUntilExpiry":
 //               case "DaysUntilNonCompliant":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, e.Column)) == 99999) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewTraining_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("CertificatePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit5_OpenLink(object sender, OpenLinkEventArgs e)
        {
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsTraining_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 4;  // Training //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Holiday Year

        private void gridControlHolidayYear_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridViewHolidayYear_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewHolidayYear_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.HolidayYear;
            SetMenuStatus();
        }

        private void gridViewHolidayYear_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.HolidayYear;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewHolidayYear_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "TotalHolidaysRemaining":
                    if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, e.Column.FieldName)) <= (decimal)0.00)
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewHolidayYear_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DefaultBankHolidayEntitlement":
                case "DefaultBasicLeaveEntitlement":
                case "DefaultYearlyLeaveIncrement":
                case "HolidaysTaken":
                case "BankHolidaysTaken":
                case "AbsenceAuthorisedTaken":
                case "AbsenceUnauthorisedTaken":
                case "BasicHolidayEntitlement":
                case "BankHolidayEntitlement":
                case "PurchasedHolidayEntitlement":
                case "HoursPerHolidayDay":
                case "TotalHolidayEntitlement":
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "TotalHolidaysRemaining":
                    if (view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptor").ToString() == "Days")
                    {
                        e.RepositoryItem = repositoryItemTextEditNumericDays;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptor").ToString() == "Hours")
                    {
                        e.RepositoryItem = repositoryItemTextEditNumericHours;
                    }
                    else
                    {
                        e.RepositoryItem = repositoryItemTextEditNumericUnknown;
                    }
                    break;
                default:
                    break;
            }
        }


        #endregion


        #region GridView Absences

        private void gridControlAbsences_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlAbsences.MainView;
                        int intRecordType = 6;  // Absences //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "AbsenceID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeSurnameForename").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date" + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridViewAbsences_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewAbsences_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Absences;
            SetMenuStatus();
        }

        private void gridViewAbsences_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Absences;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewAbsences_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "Status")
            {
                switch (view.GetRowCellValue(e.RowHandle, "Status").ToString())
                {
                    case "Pending":
                        e.Appearance.BackColor = Color.Khaki;
                        e.Appearance.BackColor2 = Color.DarkOrange;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case "Approved":
                        e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                        e.Appearance.BackColor2 = Color.PaleGreen;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case "Rejected":
                        e.Appearance.BackColor = Color.LightSteelBlue;
                        e.Appearance.BackColor2 = Color.CornflowerBlue;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case "Cancelled":
                        e.Appearance.BackColor = Color.Gainsboro;
                        e.Appearance.BackColor2 = Color.DarkGray;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case "Unauthorised":
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }
        }

        private void gridViewAbsences_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DeclaredDisability":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RecordTypeId")) == 1) e.RepositoryItem = emptyEditor;
                    break;
                case "IsWorkRelated":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RecordTypeId")) == 1) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }
        
        private void gridViewAbsences_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "DeclaredDisability":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RecordTypeId")) == 1) e.Cancel = true;
                    break;
                case "IsWorkRelated":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RecordTypeId")) == 1) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsAbsences_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 6;  // Absence //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "AbsenceID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeSurnameForename").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date" + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }


        #endregion


        #region GridView Pensions

        private void gridControlPension_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlPension.MainView;
                        int intRecordType = 2;  // Pension //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PensionID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "SchemeNumber").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewPension_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewPension_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Pensions;
            SetMenuStatus();
        }

        private void gridViewPension_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Pensions;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewPension_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewPension_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "ContributionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ContributionCount")) <= 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewPension_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "ContributionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ContributionCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentPension_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 2;  // Pension //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PensionID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "SchemeNumber").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView CRM

        private void gridControlCRM_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewCRM;
                        int intRecordType = 5;  // CRM //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CRMID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "Description").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridViewCRM_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewCRM_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.CRM;
            SetMenuStatus();
        }

        private void gridViewCRM_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.CRM;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewCRM_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DaysRemaining")
            {
                int intDaysRemaining = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysRemaining"));
                if (intDaysRemaining > 7)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                    e.Appearance.BackColor2 = Color.PaleGreen;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysRemaining > 0 && intDaysRemaining <= 7)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysRemaining < 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewCRM_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DaysRemaining":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysRemaining")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewCRM_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "DaysRemaining":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("DaysRemaining")) == 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsCRM_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 5;  // CRM //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CRMID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "Description");
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView References

        private void gridControlReference_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewReference;
                        int intRecordType = 8;  // Reference //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ReferenceID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "SuppliedByName");
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridViewReference_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewReference_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Reference;
            SetMenuStatus();
        }

        private void gridViewReference_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Reference;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewReference_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        private void gridViewReference_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewReference_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentReferences_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 8;  // Reference //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ReferenceID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "SuppliedByName");
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }


        #endregion


        #region GridView Pay Rises

        private void gridControlPayRise_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlPayRise.MainView;
                        int intRecordType = 11;  // Pay Rise //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PayRiseID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "PayRiseDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewPayRise_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewPayRise_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.PayRise;
            SetMenuStatus();
        }

        private void gridViewPayRise_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.PayRise;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewPayRise_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewPayRise_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewPayRise_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditPayRiseLinkedDocuments_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 11;  // Pay Rise //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PayRiseID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "PayRiseDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Shares

        private void gridControlShares_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlShares.MainView;
                        int intRecordType = 12;  // Shares //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SharesID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateReceived")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewShares_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewShares_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Shares;
            SetMenuStatus();
        }

        private void gridViewShares_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Shares;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewShares_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewShares_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "ShareUnits":
                    if (view.GetRowCellValue(e.RowHandle, "ShareUnitDescriptor").ToString() == "�")
                    {
                        e.RepositoryItem = repositoryItemTextEditShareUnitsCurrency;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "ShareUnitDescriptor").ToString() == "%")
                    {
                        e.RepositoryItem = repositoryItemTextEditShareUnitsPercentage;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "ShareUnitDescriptor").ToString() == "Units")
                    {
                        e.RepositoryItem = repositoryItemTextEditShareUnitsUnits;
                    }
                    else
                    {
                        e.RepositoryItem = repositoryItemTextEditShareUnitsUnknown;
                    }
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewShares_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditSharesLinkedDocuments_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 12;  // Shares //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SharesID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateReceived")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }


        #endregion


        #region GridView Administer

        private void gridControlAdminister_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("block_add".Equals(e.Button.Tag))
                    {
                        Block_Add_Multiple_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("toggle_active".Equals(e.Button.Tag))
                    {
                        if (!iBool_AllowEdit) return;
                        GridView view = (GridView)gridControlAdminister.MainView;
                        view.PostEditor();
                        int[] intRowHandles = view.GetSelectedRows();
                        int intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Administer record to toggle the Active Status of before proceeding.", "Toggle Active Status", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeAdministratorId")) + ',';
                        }
                        RefreshGridViewStateAdminister.SaveViewInfo();  // Store Grid View State //

                        using (var UpdateRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                        {
                            UpdateRecords.ChangeConnectionString(strConnectionString);
                            try
                            {
                                UpdateRecords.sp_HR_00249_Employee_Administer_Toggle_Active(strRecordIDs); 
                            }
                            catch (Exception) { }
                        }
                        LoadLinkedRecords();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewAdminister_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewAdminister_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Administer;
            SetMenuStatus();
        }

        private void gridViewAdminister_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Administer;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewAdminister_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        private void gridViewAdminister_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridViewAdminister_ShowingEditor(object sender, CancelEventArgs e)
        {
        }

        #endregion


        #region GridView Subsistence

        private void gridControlSubsistence_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewSubsistence_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewSubsistence_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Subsistence;
            SetMenuStatus();
        }

        private void gridViewSubsistence_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Subsistence;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                if (view.SelectedRowsCount > 0)
                {
                    bsiDataset.Enabled = true;
                    bbiDatasetCreate.Enabled = true;
                }
                else
                {
                    bsiDataset.Enabled = false;
                    bbiDatasetCreate.Enabled = false;
                }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion



        
        #region Absence Type Filter Panel

        private void btnAbsenceTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditAbsenceType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_select_absence_type_ids = "";  // Reset any prior values first //
            i_str_select_absence_types = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_select_absence_type_ids = "";
                return "No Absence Type Filter";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_select_absence_type_ids = "";
                return "No Absence Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_select_absence_type_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_select_absence_types = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_select_absence_types += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_select_absence_types;
        }

        private void bbiRefreshHolidays_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            LoadLinkedHolidayYears();
        }

        #endregion


        #region Department Filter Panel

        private void btnDepartmentFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditDepartment_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_department_ids = "";    // Reset any prior values first //
            i_str_selected_departments = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_department_ids = "";

                Clear_Employee_filter();
                Load_Employee_Filter();
                return "No Department Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_department_ids = "";

                Clear_Employee_filter();
                Load_Employee_Filter();
                return "No Department Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_department_ids += Convert.ToString(view.GetRowCellValue(i, "DepartmentID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_departments = Convert.ToString(view.GetRowCellValue(i, "DepartmentName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_departments += ", " + Convert.ToString(view.GetRowCellValue(i, "DepartmentName"));
                        }
                        intCount++;
                    }
                }
            }

            Clear_Employee_filter();
            Load_Employee_Filter();
            return i_str_selected_departments;
        }

        #endregion


        #region Employee Filter Panel

        private void btnEmployeeFilterOK_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEditEmployee_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit3_Get_Selected();
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_employee_ids = "";    // Reset any prior values first //
            i_str_selected_employees = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_employee_ids = "";
                return "No Employee Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_employee_ids = "";
                return "No Employee Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_employee_ids += Convert.ToString(view.GetRowCellValue(i, "EmployeeID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_employees = Convert.ToString(view.GetRowCellValue(i, "Firstname")) + " " + Convert.ToString(view.GetRowCellValue(i, "Surname"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_employees += ", " + Convert.ToString(view.GetRowCellValue(i, "Firstname")) + " " + Convert.ToString(view.GetRowCellValue(i, "Surname"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_employees;
        }

        #endregion


        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = ",";
            string strColumnName = "";
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    view = (GridView)gridControlEmployee.MainView;
                    strColumnName = "EmployeeId";
                    break;
                case Utils.enmMainGrids.Vetting:
                    view = (GridView)gridControlVetting.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.Reference:
                    view = (GridView)gridControlReference.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.Addresses:
                    view = (GridView)gridControlAddresses.MainView;
                    strColumnName = "EmployeeId";
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    view = (GridView)gridControlNextOfKin.MainView;
                    strColumnName = "EmployeeId";
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    view = (GridView)gridControlDepartmentWorked.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    view = (GridView)gridControlWorkingPatternHeader.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    view = (GridView)gridControlHolidayYear.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.Shares:
                    view = (GridView)gridControlShares.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.Bonus:
                    view = (GridView)gridControlBonuses.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.PayRise:
                    view = (GridView)gridControlPayRise.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.Pensions:
                    view = (GridView)gridControlPension.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.Training:
                    view = (GridView)gridControlTraining.MainView;
                    strColumnName = "LinkedToPersonID";
                    return;
                case Utils.enmMainGrids.Discipline:
                    view = (GridView)gridControlDiscipline.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.CRM:
                    view = (GridView)gridControlCRM.MainView;
                    strColumnName = "EmployeeID";
                    break;
                case Utils.enmMainGrids.Administer:
                    view = (GridView)gridControlAdminister.MainView;
                    strColumnName = "EmployeeId";
                    break;
                case Utils.enmMainGrids.Subsistence:
                    view = (GridView)gridControlSubsistence.MainView;
                    strColumnName = "EmployeeID";
                    break;
                default:
                    return;
            }
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strCurrentID = view.GetRowCellValue(intRowHandle, view.Columns[strColumnName]).ToString() + ',';
                if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                {
                    strSelectedIDs += strCurrentID;
                    intCount++;
                }
            }
            strSelectedIDs = strSelectedIDs.Remove(0, 1);  // Remove leading ',' //
            CreateDataset("Employee", intCount, strSelectedIDs);
        }

        private void CreateDataset(string strType, int intRecordCount, string strSelectedRecordIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_employee_count = intRecordCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Employee,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "EmployeeId");
                    break;
                 default:
                    break;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Employee,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "EmployeeId");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
            var fChildForm = new frm_DatasetManager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._i_PassedInFilterTypes = "Employee,";
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        #endregion


        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void bciFilterSelected_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlEmployee.MainView;
            if (bciFilterSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more records to filter by before proceeding.", "Filter Selected Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlEmployee.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr!= null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
            }
            else  // Clear Filter //
            {
                try
                {
                    gridControlEmployee.BeginUpdate();
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_HR_Core.sp09001_Employee_Manager.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlEmployee.EndUpdate();
        }

        private void Set_Linked_Page_Visibility()
        {
            xtraTabPageVetting.PageVisible = i_boolVettingData;
            xtraTabPageReferences.PageVisible = i_boolReferenceData;
            xtraTabPageAddresses.PageVisible = i_boolAddressData;
            xtraTabPageNextOfKin.PageVisible = i_boolNextOfKinData;
            xtraTabPageDeptsWorked.PageVisible = i_boolDeptsWorkedData;
            xtraTabPageWorkPatterns.PageVisible = i_boolWorkPatternData;
            xtraTabPageAbsences.PageVisible = i_boolHolidayData;
            xtraTabPageShares.PageVisible = i_boolShareData;
            xtraTabPageBonuses.PageVisible = i_boolBonusData;
            xtraTabPagePayRises.PageVisible = i_boolPayRiseData;
            xtraTabPagePensions.PageVisible = i_boolPensionData;
            xtraTabPageTraining.PageVisible = i_boolTrainingData;
            xtraTabPageDiscipline.PageVisible = i_boolDisciplineData;
            xtraTabPageCRM.PageVisible = i_boolCRMData;
            xtraTabPageAdminister.PageVisible = i_boolAdministerData;
            xtraTabPageSubsistence.PageVisible = i_boolSubsistenceData;
        }

        public override void OnViewAuditTrail(object sender, EventArgs e)
        {
            View_Audit_Trail();
        }
        private void View_Audit_Trail()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Employees:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEmployee.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeId")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.HR_Employee";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }


        private void indexGridView_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView view = (GridView)indexGridControl.MainView;
            if (view.FocusedRowHandle == GridControl.InvalidRowHandle) return;
            string strValue = view.GetFocusedRowCellValue("alpha").ToString();
            
            view = (GridView)gridControlEmployee.MainView;
            if (string.IsNullOrWhiteSpace(strValue)) return;

            if (strValue == "ALL")
            {
                view.Columns["Surname"].ClearFilter();
            }
            else
            {
                string filterString = "StartsWith([Surname], '" + strValue + "')";
                view.Columns["Surname"].FilterInfo = new ColumnFilterInfo(filterString, "Surname Starts With " + strValue);
            }
        }




    }

}


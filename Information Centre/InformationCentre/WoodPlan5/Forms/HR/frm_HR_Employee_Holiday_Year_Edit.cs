using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.HR;
using DevExpress.XtraEditors.Controls;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Holiday_Year_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int ParentRecordId { get; set; }
        public string ParentRecordDescription { get; set; }
        public decimal ParentRecordHolidays { get; set; }
        public decimal ParentRecordBankHolidays { get; set; }
        public int ParentHolidayUnitDescriptorId { get; set; }

        SqlDataAdapter sdaHolidayYears = new SqlDataAdapter();
        DataSet dsHolidayYears = new DataSet("NewDataSet");

        #endregion


        public frm_HR_Employee_Holiday_Year_Edit()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Holiday_Year_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 990113;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00082_Holiday_Unit_Descriptors_With_Blank);

            // Populate Main Dataset //
            sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item.NewRow();
                        drNewRow["Mode"] = strFormMode;
                        drNewRow["RecordIds"] = strRecordIDs;
                        drNewRow["EmployeeID"] = ParentRecordId;
                        drNewRow["EmployeeName"] = ParentRecordDescription;
                        drNewRow["HolidayUnitDescriptorId"] = ParentHolidayUnitDescriptorId;
                        drNewRow["HoursPerHolidayDay"] = (decimal)7.50;
                        drNewRow["MustTakeCertainHolidays"] = 1;
                        drNewRow["BasicHolidayEntitlement"] = (ParentRecordHolidays > (decimal)0.00 ? ParentRecordHolidays : (decimal)20.00);
                        drNewRow["BankHolidayEntitlement"] = (ParentRecordBankHolidays > (decimal)0.00 ? ParentRecordBankHolidays : (decimal)8.00);
                        drNewRow["PurchasedHolidayEntitlement"] = (decimal)0.00;
                        drNewRow["TotalHolidayEntitlement"] = (decimal)0.00;
                        drNewRow["HolidaysTaken"] = (decimal)0.00;
                        drNewRow["BankHolidaysTaken"] = (decimal)0.00;
                        drNewRow["AbsenceAuthorisedTaken"] = (decimal)0.00;
                        drNewRow["AbsenceUnauthorisedTaken"] = (decimal)0.00;
                        drNewRow["TotalHolidaysTaken"] = (decimal)0.00;
                        drNewRow["HolidaysRemaining"] = (decimal)0.00;
                        drNewRow["BankHolidaysRemaining"] = (decimal)0.00;
                        drNewRow["TotalHolidaysRemaining"] = (decimal)0.00;
                        drNewRow["HolidayAndPurchadedTotalEntitlement"] = (decimal)0.00;
                        drNewRow["ActiveHolidayYear"] = 0;
                        this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item.Rows.Add(drNewRow);
                      
                        if (strFormMode == "add")
                        {
                            Load_Related_Holiday_Years(); 
                            List<DateTime> list = Get_Next_Holiday_Year();
                            if (list.Count == 2)
                            {
                                drNewRow["HolidayYearStartDate"] = list[0];
                                drNewRow["HolidayYearEndDate"] = list[1];
                            }

                            if (ParentRecordId > 0)
                            {
                                // Get Employees Group Commencement Date //
                                DateTime dtGroupStartDate = new DateTime(1900, 1, 1);
                                using (var GetValue = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                                {
                                    GetValue.ChangeConnectionString(strConnectionString);
                                    try
                                    {
                                        dtGroupStartDate = Convert.ToDateTime(GetValue.sp_HR_00245_Employee_Get_Group_Start_Date(ParentRecordId));
                                    }
                                    catch (Exception) { }
                                }
                                drNewRow["ActualStartDate"] = (dtGroupStartDate > Convert.ToDateTime(drNewRow["HolidayYearStartDate"]) ? dtGroupStartDate : Convert.ToDateTime(drNewRow["HolidayYearStartDate"]));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item.NewRow();
                        drNewRow["Mode"] = strFormMode;
                        drNewRow["RecordIds"] = strRecordIDs;
                        this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item.Rows.Add(drNewRow);
                        drNewRow.AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }
                    if (strFormMode == "edit") Load_Related_Holiday_Years();
                    break;

            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Employee Holiday Year", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strFormMode == "add") CalculateHours();
            Set_Editor_Back_Colours();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
            Set_Editor_Masks();
            Set_Editor_Back_Colours();
        }

        private void ConfigureFormAccordingToMode()
        {
            ibool_FormStillLoading = true;  // Prevent validated code from firing on certain editors //
            
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        EmployeeNameButtonEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = false;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        btnAdjustHolidays.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount;
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        EmployeeNameButtonEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = true;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        btnAdjustHolidays.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        EmployeeNameButtonEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = false;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = true;
                        btnAdjustHolidays.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        EmployeeNameButtonEdit.Focus();

                        EmployeeNameButtonEdit.Properties.ReadOnly = true;
                        EmployeeNameButtonEdit.Properties.Buttons[0].Enabled = false;
                        btnAdjustHolidays.Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = false;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_HR_Employee_Holiday_Year_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Employee_Holiday_Year_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.spHR00010GetEmployeeHolidayYearItemBindingSource.EndEdit();
            try
            {
                this.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter.Update(dataSet_HR_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spHR00010GetEmployeeHolidayYearItemBindingSource.Current;
                if (currentRow != null) strNewIDs = currentRow["Id"].ToString() + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["Mode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        frm_HR_Employee_Manager fParentForm;
                        fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(5, Utils.enmMainGrids.HolidayYear, strNewIDs);
                    }
                    else if (frmChild.Name == "frm_HR_Employee_Edit")
                    {
                        frm_HR_Employee_Edit fParentForm;
                        fParentForm = (frm_HR_Employee_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(9, Utils.enmMainGrids.HolidayYear, strNewIDs);
                    }
                    else if (frmChild.Name == "frm_HR_Holiday_Manager")
                    {
                        frm_HR_Holiday_Manager fParentForm;
                        fParentForm = (frm_HR_Holiday_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Set_Editor_Masks();
                Set_Editor_Back_Colours();
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private bool ContinueFromParameterChangeWarning()
        {
            var msg = "Warning! Changing this value will mean that any attached holidays will also me moved to any selected Employee.\n\nClick OK to continue or Cancel to abort.";

            if (XtraMessageBox.Show(msg, "WARNING!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel) return false;
            return true;
        }
        private void EmployeeNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                if (!string.IsNullOrEmpty(EmployeeNameButtonEdit.EditValue.ToString()))  // Not empty so warn user //
                {
                    if (!ContinueFromParameterChangeWarning()) return;
                }
                DataRowView currentRow = (DataRowView)spHR00010GetEmployeeHolidayYearItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeID"]));
                var fChildForm = new frm_HR_Select_Employee();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = intEmployeeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["EmployeeID"] = fChildForm.intSelectedEmployeeID;
                    currentRow["EmployeeName"] = fChildForm.strSelectedEmployeeName;
                    currentRow["BasicHolidayEntitlement"] = fChildForm._BasicLeaveEntitlement;
                    currentRow["BankHolidayEntitlement"] = fChildForm._BankHolidayEntitlement;
                    currentRow["HolidayUnitDescriptorId"] = fChildForm._HolidayUnitDescriptorId;
                    spHR00010GetEmployeeHolidayYearItemBindingSource.EndEdit();
                    Set_Editor_Masks();

                    if (strFormMode == "add" || strFormMode == "edit")
                    {
                        Load_Related_Holiday_Years();

                        List<DateTime> list = Get_Next_Holiday_Year();
                        if (list.Count == 2)
                        {
                            currentRow["HolidayYearStartDate"] = list[0];
                            currentRow["HolidayYearEndDate"] = list[1];
                        }
                        if (fChildForm.intSelectedEmployeeID > 0)
                        {
                            // Get Employees Group Commencement Date //
                            DateTime dtGroupStartDate = new DateTime(1900, 1, 1);
                            using (var GetValue = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                GetValue.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    dtGroupStartDate = Convert.ToDateTime(GetValue.sp_HR_00245_Employee_Get_Group_Start_Date(fChildForm.intSelectedEmployeeID));
                                }
                                catch (Exception) { }
                            }
                            currentRow["ActualStartDate"] = (dtGroupStartDate > Convert.ToDateTime(currentRow["HolidayYearStartDate"]) ? dtGroupStartDate : Convert.ToDateTime(currentRow["HolidayYearStartDate"]));
                        }

                        this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    }
                }

            }
            else if ("edit".Equals(e.Button.Tag))
            {
                //Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 3001, "Clients");
            }

        }

        private void EmployeeNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(EmployeeNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(EmployeeNameButtonEdit, "");
            }
        }

        private void HolidayYearStartDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void HolidayYearStartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && de.EditValue == null)
            {
                dxErrorProvider1.SetError(HolidayYearStartDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                //return;
            }
            else if (!Is_Date_Valid(de.DateTime))
            {
                dxErrorProvider1.SetError(HolidayYearStartDateDateEdit, "Value clashes with an existing Holiday Year.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                //return;
            }
            else
            {
                dxErrorProvider1.SetError(HolidayYearStartDateDateEdit, "");
            }

            if (dxErrorProvider1.GetError(ActualStartDateDateEdit) == "" || dxErrorProvider1.GetError(ActualStartDateDateEdit) == "Date entered must be between Holiday Start date and Holiday End date.")
            {
                if (!Is_Actual_Start_Date_Valid())
                {
                    dxErrorProvider1.SetError(ActualStartDateDateEdit, "Date entered must be between Holiday Start date and Holiday End date.");
                }
                else
                {
                    dxErrorProvider1.SetError(ActualStartDateDateEdit, "");
                }
            }

            if (dxErrorProvider1.GetError(HolidayYearEndDateDateEdit) == "" || dxErrorProvider1.GetError(HolidayYearEndDateDateEdit) == "Date entered must be greater than or equal to Holiday Start date.")
            {
                if (!Is_End_Date_Greater_Than_End_Date())
                {
                    dxErrorProvider1.SetError(HolidayYearEndDateDateEdit, "Date entered must be greater than or equal to Holiday Start date.");
                }
                else
                {
                    dxErrorProvider1.SetError(HolidayYearEndDateDateEdit, "");
                }
            }
        }

        private void HolidayYearEndDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void HolidayYearEndDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && de.EditValue == null)
            {
                dxErrorProvider1.SetError(HolidayYearEndDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                //return;
            }
            else if (!Is_Date_Valid(de.DateTime))
            {
                dxErrorProvider1.SetError(HolidayYearEndDateDateEdit, "Value clashes with an existing Holiday Year.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                //return;
            }
            else
            {
                dxErrorProvider1.SetError(HolidayYearEndDateDateEdit, "");
            }

            if (dxErrorProvider1.GetError(ActualStartDateDateEdit) == "" || dxErrorProvider1.GetError(ActualStartDateDateEdit) == "Date entered must be between Holiday Start date and Holiday End date.")
            {
                if (!Is_Actual_Start_Date_Valid())
                {
                    dxErrorProvider1.SetError(ActualStartDateDateEdit, "Date entered must be between Holiday Start date and Holiday End date.");
                }
                else
                {
                    dxErrorProvider1.SetError(ActualStartDateDateEdit, "");
                }
            }

            if (dxErrorProvider1.GetError(HolidayYearEndDateDateEdit) == "" || dxErrorProvider1.GetError(HolidayYearEndDateDateEdit) == "Date entered must be greater than or equal to Holiday Start date.")
            {
                if (!Is_End_Date_Greater_Than_End_Date())
                {
                    dxErrorProvider1.SetError(HolidayYearEndDateDateEdit, "Date entered must be greater than or equal to Holiday Start date.");
                }
                else
                {
                    dxErrorProvider1.SetError(HolidayYearEndDateDateEdit, "");
                }
            }
        }

        private void ActualStartDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualStartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && de.EditValue == null)
            {
                dxErrorProvider1.SetError(ActualStartDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else if (!Is_Actual_Start_Date_Valid())
            {
                dxErrorProvider1.SetError(ActualStartDateDateEdit, "Date entered must be between Holiday Start date and Holiday End date.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;

            }
            else
            {
                dxErrorProvider1.SetError(ActualStartDateDateEdit, "");
            }
        }

        private void HolidayUnitDescriptorIdGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

        }
        private void HolidayUnitDescriptorIdGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(HolidayUnitDescriptorIdGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(HolidayUnitDescriptorIdGridLookUpEdit, "");
            }
            Set_Editor_Masks();
        }

        private void BasicHolidayEntitlementSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            CalculateHours();
        }

        private void BankHolidayEntitlementSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            CalculateHours();
        }

        private void PurchasedHolidayEntitlementSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            CalculateHours();
        }

        private void CalculateHours()
        {
            decimal decBasicHolidayEntitlement = (BasicHolidayEntitlementSpinEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(BasicHolidayEntitlementSpinEdit.EditValue));
            decimal decBankHolidayEntitlement = (BankHolidayEntitlementSpinEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(BankHolidayEntitlementSpinEdit.EditValue));
            decimal decPurchasedHolidayEntitlement = (PurchasedHolidayEntitlementSpinEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(PurchasedHolidayEntitlementSpinEdit.EditValue));
            decimal decTotalHolidayEntitlement = decBasicHolidayEntitlement + decBankHolidayEntitlement + decPurchasedHolidayEntitlement;

            decimal decHolidaysTaken = (HolidaysTakenTextEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(HolidaysTakenTextEdit.EditValue));
            decimal decBankHolidaysTaken = (BankHolidaysTakenTextEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(BankHolidaysTakenTextEdit.EditValue));
            decimal decTotalHolidayTaken = decHolidaysTaken + decBankHolidaysTaken;

            TotalHolidayEntitlementTextEdit.EditValue = decTotalHolidayEntitlement;
            TotalHolidaysTakenTextEdit.EditValue = decTotalHolidayTaken;
            HolidaysRemainingTextEdit.EditValue = (decBasicHolidayEntitlement + decPurchasedHolidayEntitlement) - decHolidaysTaken;
            BankHolidaysRemainingTextEdit.EditValue = decBankHolidayEntitlement - decBankHolidaysTaken;
            TotalHolidaysRemainingTextEdit.EditValue = decTotalHolidayEntitlement - decTotalHolidayTaken;
            Set_Editor_Back_Colours();
        }

        private void btnAdjustHolidays_Click(object sender, EventArgs e)
        {
            DateTime dtStartDate = (HolidayYearStartDateDateEdit.EditValue == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(HolidayYearStartDateDateEdit.EditValue));
            DateTime dtEndDate = (HolidayYearEndDateDateEdit.EditValue == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(HolidayYearEndDateDateEdit.EditValue));
            DateTime dtActualStartDate = (ActualStartDateDateEdit.EditValue == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(ActualStartDateDateEdit.EditValue));

            if (dtStartDate == DateTime.MinValue || dtEndDate == DateTime.MinValue || dtActualStartDate == DateTime.MinValue)
            {
                XtraMessageBox.Show("Unable to Calculate Holidays - Start Date, End Date and Actual Start Date must all contain a value before the calculation can be performed.", "Adjust Holidays By Actual Start Date", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            TimeSpan elapsed = dtEndDate.Subtract(dtStartDate);
            decimal daysAgo = (decimal)elapsed.TotalDays;
            TimeSpan elapsed2 = dtEndDate.Subtract(dtActualStartDate);
            decimal daysAgo2 = (decimal)elapsed2.TotalDays;

            if (daysAgo <= (decimal)0 || daysAgo2 <= (decimal)0) return;
            decimal ratio = daysAgo2 / daysAgo;

            if (ratio < 0 || ratio > 100) return;

            decimal decBasicHolidayEntitlement = (BasicHolidayEntitlementSpinEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(BasicHolidayEntitlementSpinEdit.EditValue));
            decimal decBankHolidayEntitlement = (BankHolidayEntitlementSpinEdit.EditValue == DBNull.Value ? (decimal)0.00 : Convert.ToDecimal(BankHolidayEntitlementSpinEdit.EditValue));
            decimal decTotalHolidays = decBasicHolidayEntitlement + decBankHolidayEntitlement;
            //decTotalHolidays = Math.Round(((decTotalHolidays * ratio) * 2), MidpointRounding.AwayFromZero) / 2;  // Rounded to the nearest 0.5 //
            decTotalHolidays = Math.Ceiling((decTotalHolidays * ratio) * 2) / 2;  // Rounded UP to the nearest 0.5 //

            using (var GetValue = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
            {
                GetValue.ChangeConnectionString(strConnectionString);
                try
                {
                    decBankHolidayEntitlement = Convert.ToDecimal(GetValue.sp_HR_00246_Get_Bank_Holidays_Pro_Rata(dtActualStartDate, dtEndDate));
                }
                catch (Exception) { return; }
            }
            bool boolIsLoadingFlagSet = false;  // Following code to prevent the code in BankHolidayEntitlementSpinEdit_EditValueChanged from firing // 
            if (!ibool_FormStillLoading) 
            {
                ibool_FormStillLoading = true;
                boolIsLoadingFlagSet = true;  // Store that we have changed the flag so we can set it back //
            }
            BasicHolidayEntitlementSpinEdit.EditValue = (decTotalHolidays - decBankHolidayEntitlement);
            BankHolidayEntitlementSpinEdit.EditValue = decBankHolidayEntitlement;
            if (boolIsLoadingFlagSet) ibool_FormStillLoading = false;  // Put flag back to original value //

            CalculateHours();
            btnAdjustHolidays.Enabled = false;  // Disable the button //
        }

        #endregion


        private void Set_Editor_Masks()
        {
            string strMaskDescription = "####0.00 " + HolidayUnitDescriptorIdGridLookUpEdit.Text ?? "";

            BasicHolidayEntitlementSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            BankHolidayEntitlementSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            PurchasedHolidayEntitlementSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            TotalHolidayEntitlementTextEdit.Properties.Mask.EditMask = strMaskDescription;
            
            HolidaysTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            BankHolidaysTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            TotalHolidaysTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            
            AbsenceAuthorisedTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            AbsenceUnauthorisedTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            HolidaysRemainingTextEdit.Properties.Mask.EditMask = strMaskDescription;
            BankHolidaysRemainingTextEdit.Properties.Mask.EditMask = strMaskDescription;
            TotalHolidaysRemainingTextEdit.Properties.Mask.EditMask = strMaskDescription;
        }

        private void Set_Editor_Back_Colours()
        {
            if (this.strFormMode == "blockedit") return;
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            DataRowView currentRow = (DataRowView)spHR00010GetEmployeeHolidayYearItemBindingSource.Current;
            if (currentRow == null) return;

            /*DevExpress.Skins.Skin currentSkin;
            DevExpress.Skins.SkinElement element;
            currentSkin = DevExpress.Skins.CommonSkins.GetSkin(dataLayoutControl1.LookAndFeel);
            Color foreColor =  currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.WindowText);
            Color backColor = currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.Window);
            Color disabledForeColor = currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.DisabledText);
            Color disabledBackColor =  currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.DisabledControl);*/

            Color foreColorGood = Color.Black;
            Color backColorGood = Color.PaleGreen; ;
            Color foreColorBad = Color.Black;
            Color backColorBad = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);


            HolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(HolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            HolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(HolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
            BankHolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(BankHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            BankHolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(BankHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
            TotalHolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(TotalHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            TotalHolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(TotalHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
        }

        private void Load_Related_Holiday_Years()
        {
            // Load list of existing Holiday years excluding the loaded records for all loaded records so we can check we don't overlap these //
            string strEmployeeIDs = ",";
            string strHolidayYearsIDsToIgnore = "";

            foreach (DataRow dr in this.dataSet_HR_DataEntry.sp_HR_00010_Get_Employee_Holiday_Year_Item.Rows)
            {
                if (!strEmployeeIDs.Contains("'" + dr["EmployeeID"].ToString() + ","))
                {
                    strEmployeeIDs += dr["EmployeeID"].ToString() + ",";
                }
                strHolidayYearsIDsToIgnore += dr["Id"].ToString() + ",";
            }
            strEmployeeIDs = strEmployeeIDs.Remove(0, 1);

            try
            {
                SqlConnection SQlConn = new SqlConnection(strConnectionString);
                SqlCommand cmd = null;
                cmd = new SqlCommand("sp_HR_00083_Get_Holiday_Years_For_Contract_ID", SQlConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@EmployeeIDs", strEmployeeIDs));
                cmd.Parameters.Add(new SqlParameter("@HolidayYearsIDsToIgnore", strHolidayYearsIDsToIgnore));
                sdaHolidayYears = new SqlDataAdapter(cmd);
                sdaHolidayYears.Fill(dsHolidayYears, "Table");
            }
            catch (Exception ex) { }
        }

        private bool Is_Date_Valid(DateTime dt)
        {
            // Checks if the passed in date is not within an exitsing holiday year - if it's not within a year it is valid //
            string strEmployeeID = "";
            DataRowView currentRow = (DataRowView)spHR00010GetEmployeeHolidayYearItemBindingSource.Current;
            if (currentRow != null) strEmployeeID = (currentRow["EmployeeID"] == null ? "-99" : currentRow["EmployeeID"].ToString());

            if (!string.IsNullOrEmpty(strEmployeeID))
            {
                DataRow[] drRows = dsHolidayYears.Tables[0].Select("EmployeeID = " + strEmployeeID);  // Filter on current contract //
                if (drRows.Length > 0)
                {
                    DateTime StartDate;
                    DateTime EndDate ;
                    foreach (DataRow dr in drRows)
                    {
                        StartDate = Convert.ToDateTime((dr["HolidayYearStartDate"] == null ? "2900-12-31" : dr["HolidayYearStartDate"].ToString()));
                        EndDate = Convert.ToDateTime((dr["HolidayYearEndDate"] == null ? "1900-01-01" : dr["HolidayYearEndDate"].ToString()));
                        if (dt >= StartDate && dt <= EndDate) return false;
                    }
                }
            }
            return true;
        }

        private List<DateTime> Get_Next_Holiday_Year()
        {
            // Returns the next holiday year as two dates (start and end) - stored within the returned list //
            List<DateTime> list = new List<DateTime>();

            string strEmployeeID = "";
            DataRowView currentRow = (DataRowView)spHR00010GetEmployeeHolidayYearItemBindingSource.Current;
            if (currentRow != null) strEmployeeID = (currentRow["EmployeeID"] == null ? "-99" : currentRow["EmployeeID"].ToString());

            if (!string.IsNullOrEmpty(strEmployeeID))
            {
                DataRow[] drRows = dsHolidayYears.Tables[0].Select("EmployeeID = " + strEmployeeID);  // Filter on current contract //
                if (drRows.Length > 0)
                {
                    DataRow dr = drRows[drRows.Length - 1];  // Last row as it has the latest date //
                    DateTime StartDate = Convert.ToDateTime((dr["HolidayYearStartDate"] == null ? "2900-12-31" : dr["HolidayYearStartDate"].ToString())).AddYears(1);
                    DateTime EndDate = Convert.ToDateTime((dr["HolidayYearEndDate"] == null ? "1900-01-01" : dr["HolidayYearEndDate"].ToString())).AddYears(1);
                    list.Add(StartDate);
                    list.Add(EndDate);
                }
                else  // No Holidays years present for contract so default it to this year //
                {
                    list.Add(new DateTime(DateTime.Today.Year, 1, 1));
                    list.Add(new DateTime(DateTime.Today.Year, 12, 31));
                }
            }

            return list;
        }

        private bool Is_Actual_Start_Date_Valid()
        {
            // Checks to see if ActualStartDate is between HolidayStartDate and HolidayEndDate - return true if yes otherwise false //
            DateTime HolidayStartDate = (HolidayYearStartDateDateEdit.EditValue == null ? DateTime.MinValue : HolidayYearStartDateDateEdit.DateTime);
            DateTime HolidayEndDate = (HolidayYearEndDateDateEdit.EditValue == null ? DateTime.MinValue : HolidayYearEndDateDateEdit.DateTime);
            DateTime ActualStartDate = (ActualStartDateDateEdit.EditValue == null ? DateTime.MinValue : ActualStartDateDateEdit.DateTime);
            if (ActualStartDate == DateTime.MinValue || HolidayStartDate == DateTime.MinValue || HolidayEndDate == DateTime.MinValue) return false;
            return (ActualStartDate >= HolidayStartDate && ActualStartDate <= HolidayEndDate ? true : false);
        }

        private bool Is_End_Date_Greater_Than_End_Date()
        {
            // Checks to see if ActualStartDate is between HolidayStartDate and HolidayEndDate - return true if yes otherwise false //
            DateTime HolidayStartDate = (HolidayYearStartDateDateEdit.EditValue == null ? DateTime.MinValue : HolidayYearStartDateDateEdit.DateTime);
            DateTime HolidayEndDate = (HolidayYearEndDateDateEdit.EditValue == null ? DateTime.MinValue : HolidayYearEndDateDateEdit.DateTime);
            if (HolidayStartDate == DateTime.MinValue || HolidayEndDate == DateTime.MinValue) return false;
            return (HolidayStartDate <= HolidayEndDate);
        }



    }
}


namespace WoodPlan5
{
    partial class frm_HR_Employee_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip7 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem7 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition4 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition5 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition6 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition7 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition8 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition9 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition10 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip6 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem6 = new DevExpress.Utils.ToolTipItem();
            this.colCurrentEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn35 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreatedFromWeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDummyShift = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRecordStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colActive5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemDateEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.indexGridControl = new DevExpress.XtraGrid.GridControl();
            this.sp01059CoreAlphabetListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.indexGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colalpha = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlEmployee = new DevExpress.XtraGrid.GridControl();
            this.sp09001EmployeeManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewEmployee = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiddleNames = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreviousSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCommonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colJoinedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDoB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNatInsuranceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkEmailAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEthnicityId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonalMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonalEmailAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateJoinedPension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateLeftPension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearsToAutoReEnrolment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextDateOfEnrolment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGenderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGender = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEthnicity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaritalStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReligion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDisability = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContinuousServiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPicturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffRecordCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEditCreateStaff = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colContractNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGenericJobTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractBasis = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupCommencementDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppointmentReason1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecruitmentSourceType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecruitmentSourceDetail1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecruitmentCost1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMoney = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colActualHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFTEHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFTERatio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentageMode2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContractIssuedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractReceivedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedHandbook = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTUPE1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferredFromLocalGovernment1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferredFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNJCConditionsApply1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginatingCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInductionDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInductionChecklistReturned = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptor2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnnualLeaveAnniversaryDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBasicLeaveEntitlementFTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBasicLeaveEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankHolidayEntitlementFTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankHolidayEntitlement1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearlyLeaveIncrementStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colYearlyLeaveIncrementAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaximumLeaveEntitlement1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastPayReviewDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextPayReviewDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastIncreaseAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastIncreasePercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTerminationDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastDateWorked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReasonForLastDateWorkedDifference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeavingReason1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeavingRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConsiderReEmployement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConsiderReEmploymentReason1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExitInterviewDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExitInterviewerName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrainingCostsToRecover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSchemeCostsToRecover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentCostsToRecover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalaryBanding1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentFrequency1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollReference1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaymentAmount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMonthlyPayDayNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSickPayEntitlementUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSickPayUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElegibleForBonus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElegibleForOvertime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colElegibleForShares = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonalTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankSortCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankAccountNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankAccountName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayslipEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayslipPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAnyOtherOwing = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysAccrued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysBalance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysTaken1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocuments = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colJobTitleID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAgreedReferenceInPlace = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEligibleForMedicalCover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMedicalCoverProvider = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMedicalCoverEmployerContribution = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMedicalCoverEmployerContributionDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMedicalCoverEmployerContributionFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTWScheme = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTWSchemeStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTWSchemeEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTWSchemeValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCTWSchemeEndOption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChildCareVouchers = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExitInterviewRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoticePeriodUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNoticePeriodUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInviteSummerBBQ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInviteWinterParty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInviteManagementConference = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSickPayPercentage1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSickPayUnits2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSickPayUnitDescriptor2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSickPayPercentage2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeePassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBiography = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShiftFTE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtraDaysHolidayYear1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuppressBirthday = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreviousEmploymentSalary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalaryExpectation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentTermsAndConditions = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentTermsAndConditionsDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOptedIn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOptedOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOptOutFormIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOptInToSubsistence = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDISCoverEligible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDISLevelCover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDISScheme = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIPEligible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIPLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIPDeferralPeriod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIPScheme = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditUnknown = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditInt = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.popupContainerControlAbsenceTypes = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.spHR00077AbsenceTypesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnAbsenceTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlEmployees = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.spHR00115EmployeesByDeptFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnEmployeeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.popupContainerControlDepartments = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.spHR00114DepartmentFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDepartmentFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageAbsences = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer11 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlHolidayYear = new DevExpress.XtraGrid.GridControl();
            this.spHR00158HolidayYearsForEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewHolidayYear = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayYearStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHolidayYearEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DPNoDescriptor = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMustTakeCertainHolidays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditNumericUnknown = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridSplitContainer12 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlAbsences = new DevExpress.XtraGrid.GridControl();
            this.sp_HR_00037_Get_Employee_AbsencesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewAbsences = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAbsenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeHolidayYearId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceSubTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShortDate2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHolidayUnitDescriptor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApprovedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLongDate2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCancelledDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeclaredDisability = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayCategoryId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsWorkRelated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayYearDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurnameForename1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeForename1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApprovedByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelledByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActiveHolidayYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEWCDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedReturnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHalfDayEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHalfDayStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdditionalDaysPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedDocumentCount6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsAbsences = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colUnitsPaidCSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsPaidHalfPay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsPaidOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsPaidUnpaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabPageVetting = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlVetting = new DevExpress.XtraGrid.GridControl();
            this.spHR00131GetEmployeeVettingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewVetting = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVettingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVettingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVettingType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVettingSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVettingSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryIteDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpiryDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotificationPeriodDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DPDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRequestedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEssentialForRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRestrictionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiredStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLinkedDocumentCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditVettingLinkedDocs = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colVisaRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisaType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageReferences = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer15 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlReference = new DevExpress.XtraGrid.GridControl();
            this.spHR00200EmployeeReferencesLinkedToEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewReference = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colReferenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCommunicationMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuppliedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuppliedByContactTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSuppliedByContactEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelationshipTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRequested = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEditDateTime15 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colRequestedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceSatisfactory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRemarks9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCommunicationMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestedByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelationshipType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentReferences = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPageAddresses = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer7 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlAddresses = new DevExpress.XtraGrid.GridControl();
            this.sp_HR_00009_GetEmployeeAddressesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewAddresses = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsCurrentAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colPostCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandlineNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFromDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colToDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageNextOfKin = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer6 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlNextOfKin = new DevExpress.XtraGrid.GridControl();
            this.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewNextOfKin = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeId2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLandline = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriorityOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRelationshipId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRelationship = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageDeptsWorked = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlDepartmentWorked = new DevExpress.XtraGrid.GridControl();
            this.spHR00001EmployeeDeptsWorkedLinkedToContractBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewDepartmentWorked = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeDeptWorkedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentLocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBaseTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPositionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportsToStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFromDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBaseType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurnameForename2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeForename2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportsToStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentreCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colPositionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageWorkPatterns = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer10 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlWorkingPatternHeader = new DevExpress.XtraGrid.GridControl();
            this.spHR00154EmployeeWorkingPatternHeadersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewWorkingPatternHeader = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlWorkingPattern = new DevExpress.XtraGrid.GridControl();
            this.sp_HR_00046_Get_Employee_Working_PatternsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewWorkingPattern = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colWorkingPatternID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWeekNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditWeekNumber = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDayOfWeek = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDayOfTheWeek = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTime = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.colEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBreakLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMinutes = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPaidForBreaks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCalculatedHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeDepartmentWorkedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDepartmentName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocationName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegionName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDates = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHeaderStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeaderDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageShares = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer17 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlShares = new DevExpress.XtraGrid.GridControl();
            this.spHR00224EmployeeSharesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewShares = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSharesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShareTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateReceived1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colShareUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShareUnitsUnits = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colShareUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShareUnitsCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRemarks11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit19 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShareType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShareUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditSharesLinkedDocuments = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEditShareUnitsUnknown = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditShareUnitsPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageBonuses = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlBonuses = new DevExpress.XtraGrid.GridControl();
            this.spHR00172EmployeeBonusesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewBonuses = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBonusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonusDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGuaranteeBonusAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBonusDatePaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBonusAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditBonusLinkedDocuments = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPagePayRises = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer16 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlPayRise = new DevExpress.XtraGrid.GridControl();
            this.spHR00213EmployeePayRisesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewPayRise = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPayRiseID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreviousPayAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPayRiseAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayRisePercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNewPayAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayRiseDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime6 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAppliedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppliedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditPayRiseLinkedDocuments = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPagePensions = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer13 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlPension = new DevExpress.XtraGrid.GridControl();
            this.spHR00016PensionsForEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewPension = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPensionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSchemeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSchemeTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContributionTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMatchedContribution = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMinimumContributionAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency13 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMaximumContributionAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentContributionAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProviderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime13 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFixedInitialTermMonths = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditMonths = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTotalEmployeeContributionToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalEmployerContributionToDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSchemeType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContributionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colProvider = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContributionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentPension = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPageSubsistence = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSubsistence = new DevExpress.XtraGrid.GridControl();
            this.spHR00257EmployeeSubsistenceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewSubsistence = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSubsistenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubsistenceTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime14 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency14 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRate1Units = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP_14 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRate1UnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate2Units = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate2UnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalarySacrificeAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit20 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate1UnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRate2UnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubsistenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageTraining = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer8 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlTraining = new DevExpress.XtraGrid.GridControl();
            this.sp09113HRQualificationsForEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewTraining = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQualificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationFromID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostToCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateOnky = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSummerMaintenance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colWinterMaintenance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityRail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGUID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDaysUntilExpiry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedDocumentCount7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colQualificationSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArchived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssessmentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime12 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPageDiscipline = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer9 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlDiscipline = new DevExpress.XtraGrid.GridControl();
            this.spHR00053GetEmployeeSanctionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewDiscipline = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSanctionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colERIssueTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colERIssueType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHRManagerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHRManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRaisedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigatedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigatedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeardByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeardByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingNoteTakerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingEmployeeRepresentativeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeIssuedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeIssuedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealHeardByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealHeardByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealNoteTakerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealEmployeeRepresentativeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric0DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpiryDurationUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDurationUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSanctionOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentSanction = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.xtraTabPageCRM = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer14 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlCRM = new DevExpress.XtraGrid.GridControl();
            this.spHR00193EmployeeCRMLinkedToEmployeeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewCRM = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCRMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTimeCRM = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContactMadeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactWithEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEditCRM = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactWithEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirection = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentsCRM = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageAdminister = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlAdminister = new DevExpress.XtraGrid.GridControl();
            this.spHR00248EmployeeAdministeringBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewAdminister = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeAdministratorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeId14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdministratorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime18 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colDateTo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit18 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdministratorEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdministratorEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdministratorEmployeeFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdministratorEmployeeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdministrationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp09001_Employee_ManagerTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp09001_Employee_ManagerTableAdapter();
            this.xtraGridBlendingEmployee = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00009_GetEmployeeAddressesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00009_GetEmployeeAddressesTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_HR_CoreTableAdapters.TableAdapterManager();
            this.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter();
            this.sp_HR_00037_Get_Employee_AbsencesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00037_Get_Employee_AbsencesTableAdapter();
            this.sp_HR_00046_Get_Employee_Working_PatternsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00046_Get_Employee_Working_PatternsTableAdapter();
            this.sp_HR_00053_Get_Employee_SanctionsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00053_Get_Employee_SanctionsTableAdapter();
            this.sp09113_HR_Qualifications_For_EmployeeTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp09113_HR_Qualifications_For_EmployeeTableAdapter();
            this.xtraGridBlendingWorkingPatternHeader = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingDiscipline = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingTraining = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingAddresses = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingNextOfKin = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingVetting = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingWorkingPattern = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingAbsences = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiShowActiveHolidayYearOnly = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.beiAbsenceType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditAbsenceType = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefreshHolidays = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.sp_HR_00077_Absence_Types_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00077_Absence_Types_List_With_BlankTableAdapter();
            this.sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter();
            this.xtraGridBlendingDepartmentWorked = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00131_Get_Employee_VettingTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00131_Get_Employee_VettingTableAdapter();
            this.sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter();
            this.sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter();
            this.xtraGridBlendingHolidayYear = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00172_Employee_BonusesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00172_Employee_BonusesTableAdapter();
            this.xtraGridBlendingBonuses = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00016_Pensions_For_EmployeeTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00016_Pensions_For_EmployeeTableAdapter();
            this.xtraGridBlendingPension = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter();
            this.xtraGridBlendingCRM = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter();
            this.xtraGridBlendingReference = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.beiDepartment = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDepartment = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.beiEmployee = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.beiActive = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditActiveOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterSelected = new DevExpress.XtraBars.BarCheckItem();
            this.sp_HR_00114_Department_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter();
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter();
            this.sp_HR_00213_Employee_PayRisesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00213_Employee_PayRisesTableAdapter();
            this.xtraGridBlendingPayRise = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlendingShares = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00224_Employee_SharesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00224_Employee_SharesTableAdapter();
            this.xtraGridBlendingAdminister = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00248_Employee_AdministeringTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00248_Employee_AdministeringTableAdapter();
            this.xtraGridBlendingSubsistence = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00257_Employee_SubsistenceTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00257_Employee_SubsistenceTableAdapter();
            this.sp01059_Core_Alphabet_ListTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01059_Core_Alphabet_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.indexGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01059CoreAlphabetListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.indexGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09001EmployeeManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditCreateStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentageMode2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditUnknown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlAbsenceTypes)).BeginInit();
            this.popupContainerControlAbsenceTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00077AbsenceTypesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).BeginInit();
            this.popupContainerControlEmployees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).BeginInit();
            this.popupContainerControlDepartments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageAbsences.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer11)).BeginInit();
            this.gridSplitContainer11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHolidayYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00158HolidayYearsForEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHolidayYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DPNoDescriptor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericUnknown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer12)).BeginInit();
            this.gridSplitContainer12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAbsences)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_HR_00037_Get_Employee_AbsencesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAbsences)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLongDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsAbsences)).BeginInit();
            this.xtraTabPageVetting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00131GetEmployeeVettingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryIteDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DPDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditVettingLinkedDocs)).BeginInit();
            this.xtraTabPageReferences.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer15)).BeginInit();
            this.gridSplitContainer15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00200EmployeeReferencesLinkedToEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditDateTime15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditDateTime15.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentReferences)).BeginInit();
            this.xtraTabPageAddresses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).BeginInit();
            this.gridSplitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAddresses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_HR_00009_GetEmployeeAddressesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAddresses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).BeginInit();
            this.xtraTabPageNextOfKin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).BeginInit();
            this.gridSplitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlNextOfKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNextOfKin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            this.xtraTabPageDeptsWorked.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDepartmentWorked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00001EmployeeDeptsWorkedLinkedToContractBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDepartmentWorked)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked)).BeginInit();
            this.xtraTabPageWorkPatterns.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer10)).BeginInit();
            this.gridSplitContainer10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWorkingPatternHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00154EmployeeWorkingPatternHeadersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWorkingPatternHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWorkingPattern)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_HR_00046_Get_Employee_Working_PatternsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWorkingPattern)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditWeekNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTime.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMinutes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            this.xtraTabPageShares.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer17)).BeginInit();
            this.gridSplitContainer17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlShares)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00224EmployeeSharesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewShares)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSharesLinkedDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsUnknown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsPercentage)).BeginInit();
            this.xtraTabPageBonuses.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBonuses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00172EmployeeBonusesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBonuses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditBonusLinkedDocuments)).BeginInit();
            this.xtraTabPagePayRises.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer16)).BeginInit();
            this.gridSplitContainer16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPayRise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00213EmployeePayRisesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPayRise)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditPayRiseLinkedDocuments)).BeginInit();
            this.xtraTabPagePensions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer13)).BeginInit();
            this.gridSplitContainer13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00016PensionsForEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMonths)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentPension)).BeginInit();
            this.xtraTabPageSubsistence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSubsistence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00257EmployeeSubsistenceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSubsistence)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit20)).BeginInit();
            this.xtraTabPageTraining.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).BeginInit();
            this.gridSplitContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTraining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09113HRQualificationsForEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTraining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateOnky)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsTraining)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime12)).BeginInit();
            this.xtraTabPageDiscipline.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer9)).BeginInit();
            this.gridSplitContainer9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDiscipline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00053GetEmployeeSanctionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDiscipline)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentSanction)).BeginInit();
            this.xtraTabPageCRM.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer14)).BeginInit();
            this.gridSplitContainer14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00193EmployeeCRMLinkedToEmployeeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTimeCRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditCRM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsCRM)).BeginInit();
            this.xtraTabPageAdminister.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAdminister)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00248EmployeeAdministeringBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAdminister)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditAbsenceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1194, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 517);
            this.barDockControlBottom.Size = new System.Drawing.Size(1194, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 475);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1194, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 475);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beiShowActiveHolidayYearOnly,
            this.barButtonItem1,
            this.bbiRefreshHolidays,
            this.beiAbsenceType,
            this.beiDepartment,
            this.beiEmployee,
            this.beiActive,
            this.bbiRefresh,
            this.bciFilterSelected});
            this.barManager1.MaxItemId = 37;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly,
            this.repositoryItemPopupContainerEditAbsenceType,
            this.repositoryItemPopupContainerEditDepartment,
            this.repositoryItemPopupContainerEditEmployee,
            this.repositoryItemCheckEditActiveOnly});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colCurrentEmployee
            // 
            this.colCurrentEmployee.Caption = "Current Employee";
            this.colCurrentEmployee.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colCurrentEmployee.FieldName = "CurrentEmployee";
            this.colCurrentEmployee.Name = "colCurrentEmployee";
            this.colCurrentEmployee.OptionsColumn.AllowEdit = false;
            this.colCurrentEmployee.OptionsColumn.AllowFocus = false;
            this.colCurrentEmployee.OptionsColumn.ReadOnly = true;
            this.colCurrentEmployee.Visible = true;
            this.colCurrentEmployee.VisibleIndex = 2;
            this.colCurrentEmployee.Width = 107;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ID";
            this.gridColumn3.FieldName = "ID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 53;
            // 
            // gridColumn35
            // 
            this.gridColumn35.Caption = "Current Employee";
            this.gridColumn35.ColumnEdit = this.repositoryItemCheckEdit15;
            this.gridColumn35.FieldName = "CurrentEmployee";
            this.gridColumn35.Name = "gridColumn35";
            this.gridColumn35.OptionsColumn.AllowEdit = false;
            this.gridColumn35.OptionsColumn.AllowFocus = false;
            this.gridColumn35.OptionsColumn.ReadOnly = true;
            this.gridColumn35.Visible = true;
            this.gridColumn35.VisibleIndex = 4;
            this.gridColumn35.Width = 107;
            // 
            // repositoryItemCheckEdit15
            // 
            this.repositoryItemCheckEdit15.AutoHeight = false;
            this.repositoryItemCheckEdit15.Caption = "Check";
            this.repositoryItemCheckEdit15.Name = "repositoryItemCheckEdit15";
            this.repositoryItemCheckEdit15.ValueChecked = 1;
            this.repositoryItemCheckEdit15.ValueUnchecked = 0;
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Current Holiday Year";
            this.gridColumn20.ColumnEdit = this.repositoryItemCheckEdit11;
            this.gridColumn20.FieldName = "ActiveHolidayYear";
            this.gridColumn20.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 134;
            // 
            // repositoryItemCheckEdit11
            // 
            this.repositoryItemCheckEdit11.AutoHeight = false;
            this.repositoryItemCheckEdit11.Caption = "Check";
            this.repositoryItemCheckEdit11.Name = "repositoryItemCheckEdit11";
            this.repositoryItemCheckEdit11.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit11.ValueChecked = 1;
            this.repositoryItemCheckEdit11.ValueUnchecked = 0;
            // 
            // colCreatedFromWeb
            // 
            this.colCreatedFromWeb.Caption = "Created From Web";
            this.colCreatedFromWeb.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCreatedFromWeb.FieldName = "CreatedFromWeb";
            this.colCreatedFromWeb.Name = "colCreatedFromWeb";
            this.colCreatedFromWeb.OptionsColumn.AllowEdit = false;
            this.colCreatedFromWeb.OptionsColumn.AllowFocus = false;
            this.colCreatedFromWeb.OptionsColumn.ReadOnly = true;
            this.colCreatedFromWeb.Visible = true;
            this.colCreatedFromWeb.VisibleIndex = 2;
            this.colCreatedFromWeb.Width = 110;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit7;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 5;
            this.colActive.Width = 51;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.ValueChecked = 1;
            this.repositoryItemCheckEdit7.ValueUnchecked = 0;
            // 
            // colActive2
            // 
            this.colActive2.Caption = "Active";
            this.colActive2.ColumnEdit = this.repositoryItemCheckEdit10;
            this.colActive2.FieldName = "Active";
            this.colActive2.Name = "colActive2";
            this.colActive2.OptionsColumn.AllowEdit = false;
            this.colActive2.OptionsColumn.AllowFocus = false;
            this.colActive2.OptionsColumn.ReadOnly = true;
            this.colActive2.Visible = true;
            this.colActive2.VisibleIndex = 4;
            this.colActive2.Width = 51;
            // 
            // repositoryItemCheckEdit10
            // 
            this.repositoryItemCheckEdit10.AutoHeight = false;
            this.repositoryItemCheckEdit10.Caption = "Check";
            this.repositoryItemCheckEdit10.Name = "repositoryItemCheckEdit10";
            this.repositoryItemCheckEdit10.ValueChecked = 1;
            this.repositoryItemCheckEdit10.ValueUnchecked = 0;
            // 
            // colDummyShift
            // 
            this.colDummyShift.Caption = "Dummy Shift";
            this.colDummyShift.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colDummyShift.FieldName = "DummyShift";
            this.colDummyShift.Name = "colDummyShift";
            this.colDummyShift.OptionsColumn.AllowEdit = false;
            this.colDummyShift.OptionsColumn.AllowFocus = false;
            this.colDummyShift.OptionsColumn.ReadOnly = true;
            this.colDummyShift.Visible = true;
            this.colDummyShift.VisibleIndex = 14;
            this.colDummyShift.Width = 81;
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Caption = "Check";
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            this.repositoryItemCheckEdit9.ValueChecked = 1;
            this.repositoryItemCheckEdit9.ValueUnchecked = 0;
            // 
            // colActive3
            // 
            this.colActive3.Caption = "Active";
            this.colActive3.ColumnEdit = this.repositoryItemCheckEdit16;
            this.colActive3.FieldName = "Active";
            this.colActive3.Name = "colActive3";
            this.colActive3.OptionsColumn.AllowEdit = false;
            this.colActive3.OptionsColumn.AllowFocus = false;
            this.colActive3.OptionsColumn.ReadOnly = true;
            this.colActive3.Visible = true;
            this.colActive3.VisibleIndex = 2;
            this.colActive3.Width = 51;
            // 
            // repositoryItemCheckEdit16
            // 
            this.repositoryItemCheckEdit16.AutoHeight = false;
            this.repositoryItemCheckEdit16.Caption = "Check";
            this.repositoryItemCheckEdit16.Name = "repositoryItemCheckEdit16";
            this.repositoryItemCheckEdit16.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit16.ValueChecked = 1;
            this.repositoryItemCheckEdit16.ValueUnchecked = 0;
            // 
            // colRecordStatus
            // 
            this.colRecordStatus.Caption = "Record Status";
            this.colRecordStatus.FieldName = "RecordStatus";
            this.colRecordStatus.Name = "colRecordStatus";
            this.colRecordStatus.OptionsColumn.AllowEdit = false;
            this.colRecordStatus.OptionsColumn.AllowFocus = false;
            this.colRecordStatus.OptionsColumn.ReadOnly = true;
            this.colRecordStatus.Width = 89;
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit12;
            this.colActive1.FieldName = "Active";
            this.colActive1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 0;
            this.colActive1.Width = 64;
            // 
            // repositoryItemCheckEdit12
            // 
            this.repositoryItemCheckEdit12.AutoHeight = false;
            this.repositoryItemCheckEdit12.Caption = "Check";
            this.repositoryItemCheckEdit12.Name = "repositoryItemCheckEdit12";
            this.repositoryItemCheckEdit12.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit12.ValueChecked = 1;
            this.repositoryItemCheckEdit12.ValueUnchecked = 0;
            // 
            // colActive5
            // 
            this.colActive5.Caption = "Active";
            this.colActive5.ColumnEdit = this.repositoryItemCheckEdit17;
            this.colActive5.FieldName = "Active";
            this.colActive5.Name = "colActive5";
            this.colActive5.OptionsColumn.AllowEdit = false;
            this.colActive5.OptionsColumn.AllowFocus = false;
            this.colActive5.OptionsColumn.ReadOnly = true;
            this.colActive5.Visible = true;
            this.colActive5.VisibleIndex = 3;
            this.colActive5.Width = 62;
            // 
            // repositoryItemCheckEdit17
            // 
            this.repositoryItemCheckEdit17.AutoHeight = false;
            this.repositoryItemCheckEdit17.Name = "repositoryItemCheckEdit17";
            this.repositoryItemCheckEdit17.ValueChecked = 1;
            this.repositoryItemCheckEdit17.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.LookAndFeel.SkinName = "Blue";
            this.repositoryItemTextEdit2.Mask.EditMask = "c";
            this.repositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // repositoryItemHyperLinkEdit6
            // 
            this.repositoryItemHyperLinkEdit6.AutoHeight = false;
            this.repositoryItemHyperLinkEdit6.Name = "repositoryItemHyperLinkEdit6";
            this.repositoryItemHyperLinkEdit6.SingleClick = true;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // repositoryItemDateEdit6
            // 
            this.repositoryItemDateEdit6.AutoHeight = false;
            this.repositoryItemDateEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit6.Name = "repositoryItemDateEdit6";
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ReadOnly = true;
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 42);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.indexGridControl);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1194, 475);
            this.splitContainerControl1.SplitterPosition = 176;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // indexGridControl
            // 
            this.indexGridControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.indexGridControl.DataSource = this.sp01059CoreAlphabetListBindingSource;
            this.indexGridControl.Location = new System.Drawing.Point(1, 0);
            this.indexGridControl.MainView = this.indexGridView;
            this.indexGridControl.MenuManager = this.barManager1;
            this.indexGridControl.Name = "indexGridControl";
            this.indexGridControl.Size = new System.Drawing.Size(33, 293);
            this.indexGridControl.TabIndex = 1;
            this.indexGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.indexGridView});
            // 
            // sp01059CoreAlphabetListBindingSource
            // 
            this.sp01059CoreAlphabetListBindingSource.DataMember = "sp01059_Core_Alphabet_List";
            this.sp01059CoreAlphabetListBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // indexGridView
            // 
            this.indexGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colalpha,
            this.colOrder1});
            this.indexGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.indexGridView.GridControl = this.indexGridControl;
            this.indexGridView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.indexGridView.Name = "indexGridView";
            this.indexGridView.OptionsBehavior.AllowPixelScrolling = DevExpress.Utils.DefaultBoolean.True;
            this.indexGridView.OptionsBehavior.Editable = false;
            this.indexGridView.OptionsDetail.EnableMasterViewMode = false;
            this.indexGridView.OptionsFind.AllowFindPanel = false;
            this.indexGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.indexGridView.OptionsSelection.EnableAppearanceHideSelection = false;
            this.indexGridView.OptionsView.ShowColumnHeaders = false;
            this.indexGridView.OptionsView.ShowGroupPanel = false;
            this.indexGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.indexGridView.OptionsView.ShowIndicator = false;
            this.indexGridView.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
            this.indexGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.indexGridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.indexGridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.indexGridView_FocusedRowChanged);
            // 
            // colalpha
            // 
            this.colalpha.AppearanceCell.Options.UseTextOptions = true;
            this.colalpha.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colalpha.FieldName = "alpha";
            this.colalpha.Name = "colalpha";
            this.colalpha.OptionsColumn.AllowEdit = false;
            this.colalpha.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.colalpha.OptionsColumn.AllowMove = false;
            this.colalpha.OptionsColumn.AllowShowHide = false;
            this.colalpha.OptionsColumn.AllowSize = false;
            this.colalpha.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colalpha.OptionsColumn.ReadOnly = true;
            this.colalpha.Visible = true;
            this.colalpha.VisibleIndex = 0;
            // 
            // colOrder1
            // 
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.ShowInCustomizationForm = false;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControlEmployee;
            this.gridSplitContainer1.Location = new System.Drawing.Point(36, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlAbsenceTypes);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlEmployees);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDepartments);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControlEmployee);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1158, 293);
            this.gridSplitContainer1.TabIndex = 2;
            // 
            // gridControlEmployee
            // 
            this.gridControlEmployee.DataSource = this.sp09001EmployeeManagerBindingSource;
            this.gridControlEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlEmployee.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlEmployee.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlEmployee.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControlEmployee.Location = new System.Drawing.Point(0, 0);
            this.gridControlEmployee.MainView = this.gridViewEmployee;
            this.gridControlEmployee.MenuManager = this.barManager1;
            this.gridControlEmployee.Name = "gridControlEmployee";
            this.gridControlEmployee.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDate,
            this.repositoryItemCheckEdit5,
            this.repositoryItemButtonEditCreateStaff,
            this.repositoryItemTextEditMoney,
            this.repositoryItemTextEdit2DP,
            this.repositoryItemTextEditPercentage2,
            this.repositoryItemTextEditHours,
            this.repositoryItemTextEditDays,
            this.repositoryItemTextEditUnknown,
            this.repositoryItemTextEditPercentageMode2,
            this.repositoryItemHyperLinkEditLinkedDocuments,
            this.repositoryItemTextEditInt});
            this.gridControlEmployee.Size = new System.Drawing.Size(1158, 293);
            this.gridControlEmployee.TabIndex = 0;
            this.gridControlEmployee.UseEmbeddedNavigator = true;
            this.gridControlEmployee.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewEmployee});
            // 
            // sp09001EmployeeManagerBindingSource
            // 
            this.sp09001EmployeeManagerBindingSource.DataMember = "sp09001_Employee_Manager";
            this.sp09001EmployeeManagerBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "Client_Sent.png");
            this.imageCollection1.Images.SetKeyName(5, "defaults_16.png");
            this.imageCollection1.Images.SetKeyName(6, "linked_documents_16_16.png");
            this.imageCollection1.InsertGalleryImage("copy_16x16.png", "images/edit/copy_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/edit/copy_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "copy_16x16.png");
            this.imageCollection1.InsertGalleryImage("checkbox_16x16.png", "images/content/checkbox_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/content/checkbox_16x16.png"), 8);
            this.imageCollection1.Images.SetKeyName(8, "checkbox_16x16.png");
            this.imageCollection1.Images.SetKeyName(9, "BlockAdd_16x16.png");
            // 
            // gridViewEmployee
            // 
            this.gridViewEmployee.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeId,
            this.colEmployeeNumber,
            this.colTitle,
            this.colFirstname,
            this.colMiddleNames,
            this.colSurname,
            this.colPreviousSurname,
            this.colCommonName,
            this.colComments,
            this.colJoinedDate,
            this.colDoB,
            this.colNatInsuranceNumber,
            this.colWorkEmailAddress,
            this.colEthnicityId,
            this.colPersonalMobile,
            this.colPersonalEmailAddress,
            this.colDateJoinedPension,
            this.colDateLeftPension,
            this.colYearsToAutoReEnrolment,
            this.colNextDateOfEnrolment,
            this.colGenderID,
            this.colGender,
            this.colEthnicity,
            this.colAge,
            this.colCurrentEmployee,
            this.colMaritalStatus,
            this.colReligion,
            this.colDisability,
            this.colContinuousServiceDate,
            this.colPicturePath,
            this.colWorkTelephone,
            this.colStaffRecordCreated,
            this.colContractNumber3,
            this.colJobTitle,
            this.colContractType1,
            this.colGenericJobTitle,
            this.colJobType,
            this.colContractBasis,
            this.colContractStartDate,
            this.colContractEndDate,
            this.colGroupCommencementDate1,
            this.colAppointmentReason1,
            this.colRecruitmentSourceType1,
            this.colRecruitmentSourceDetail1,
            this.colRecruitmentCost1,
            this.colActualHours,
            this.colFTEHours,
            this.colFTERatio,
            this.colContractIssuedDate,
            this.colContractReceivedDate,
            this.colIssuedHandbook,
            this.colTUPE1,
            this.colTransferredFromLocalGovernment1,
            this.colTransferredFrom,
            this.colNJCConditionsApply1,
            this.colTransferDate1,
            this.colOriginatingCompany,
            this.colInductionDate,
            this.colInductionChecklistReturned,
            this.colHolidayUnitDescriptor2,
            this.colAnnualLeaveAnniversaryDate1,
            this.colBasicLeaveEntitlementFTE,
            this.colBasicLeaveEntitlement,
            this.colBankHolidayEntitlementFTE,
            this.colBankHolidayEntitlement1,
            this.colYearlyLeaveIncrementStartDate,
            this.colYearlyLeaveIncrementAmount,
            this.colMaximumLeaveEntitlement1,
            this.colLastPayReviewDate1,
            this.colNextPayReviewDate1,
            this.colLastIncreaseAmount,
            this.colLastIncreasePercentage,
            this.colTerminationDate1,
            this.colLastDateWorked,
            this.colReasonForLastDateWorkedDifference,
            this.colLeavingReason1,
            this.colLeavingRemarks,
            this.colConsiderReEmployement,
            this.colConsiderReEmploymentReason1,
            this.colExitInterviewDate1,
            this.colExitInterviewerName1,
            this.colTrainingCostsToRecover,
            this.colSchemeCostsToRecover,
            this.colEquipmentCostsToRecover,
            this.colSalary,
            this.colSalaryBanding1,
            this.colPayrollType,
            this.colPaymentType1,
            this.colPaymentFrequency1,
            this.colPayrollReference1,
            this.colPaymentAmount1,
            this.colMonthlyPayDayNumber1,
            this.colSickPayEntitlementUnits,
            this.colSickPayUnitDescriptor,
            this.colElegibleForBonus,
            this.colElegibleForOvertime,
            this.colElegibleForShares,
            this.colPayrollNumber,
            this.colPersonalTelephone,
            this.colWorkMobile,
            this.colBankName,
            this.colBankSortCode,
            this.colBankAccountNumber,
            this.colBankAccountName,
            this.colPayslipEmail,
            this.colPayslipPassword,
            this.colAnyOtherOwing,
            this.colHolidaysAccrued,
            this.colHolidaysBalance,
            this.colHolidaysTaken1,
            this.colHolidayUnitDescriptorID,
            this.colLinkedDocumentCount,
            this.colJobTitleID,
            this.colAgreedReferenceInPlace,
            this.colEligibleForMedicalCover,
            this.colMedicalCoverProvider,
            this.colMedicalCoverEmployerContribution,
            this.colMedicalCoverEmployerContributionDescriptor,
            this.colMedicalCoverEmployerContributionFrequency,
            this.colCTWScheme,
            this.colCTWSchemeStartDate,
            this.colCTWSchemeEndDate,
            this.colCTWSchemeValue,
            this.colCTWSchemeEndOption,
            this.colChildCareVouchers,
            this.colExitInterviewRequired,
            this.colSelected,
            this.colNoticePeriodUnits,
            this.colNoticePeriodUnitDescriptor,
            this.colInviteSummerBBQ,
            this.colInviteWinterParty,
            this.colInviteManagementConference,
            this.colSickPayPercentage1,
            this.colSickPayUnits2,
            this.colSickPayUnitDescriptor2,
            this.colSickPayPercentage2,
            this.colEmployeePassword,
            this.colBiography,
            this.colShiftFTE,
            this.colExtraDaysHolidayYear1,
            this.colStaffID,
            this.colStaffName,
            this.colSuppressBirthday,
            this.colPreviousEmploymentSalary,
            this.colSalaryExpectation,
            this.colCurrentTermsAndConditions,
            this.colCurrentTermsAndConditionsDate,
            this.colDateOptedIn,
            this.colDateOptedOut,
            this.colOptOutFormIssued,
            this.colOptInToSubsistence,
            this.colDISCoverEligible,
            this.colDISLevelCover,
            this.colDISScheme,
            this.colIPEligible,
            this.colIPLevel,
            this.colIPDeferralPeriod,
            this.colIPScheme});
            styleFormatCondition3.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition3.Appearance.Options.UseForeColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.colCurrentEmployee;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition3.Value1 = 0;
            this.gridViewEmployee.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition3});
            this.gridViewEmployee.GridControl = this.gridControlEmployee;
            this.gridViewEmployee.Images = this.imageCollection1;
            this.gridViewEmployee.Name = "gridViewEmployee";
            this.gridViewEmployee.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewEmployee.OptionsFind.AlwaysVisible = true;
            this.gridViewEmployee.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewEmployee.OptionsLayout.StoreAppearance = true;
            this.gridViewEmployee.OptionsLayout.StoreFormatRules = true;
            this.gridViewEmployee.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewEmployee.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewEmployee.OptionsSelection.MultiSelect = true;
            this.gridViewEmployee.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewEmployee.OptionsView.BestFitMaxRowCount = 10;
            this.gridViewEmployee.OptionsView.ColumnAutoWidth = false;
            this.gridViewEmployee.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewEmployee.OptionsView.ShowGroupPanel = false;
            this.gridViewEmployee.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFirstname, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewEmployee.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridViewEmployee.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridViewEmployee.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewEmployee.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewEmployee.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewEmployee.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridViewEmployee.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewEmployee.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewEmployee.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewEmployee.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridViewEmployee.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridViewEmployee.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colEmployeeId
            // 
            this.colEmployeeId.Caption = "Employee ID";
            this.colEmployeeId.FieldName = "EmployeeId";
            this.colEmployeeId.Name = "colEmployeeId";
            this.colEmployeeId.OptionsColumn.AllowEdit = false;
            this.colEmployeeId.OptionsColumn.AllowFocus = false;
            this.colEmployeeId.OptionsColumn.ReadOnly = true;
            this.colEmployeeId.Width = 81;
            // 
            // colEmployeeNumber
            // 
            this.colEmployeeNumber.Caption = "Employee #";
            this.colEmployeeNumber.FieldName = "EmployeeNumber";
            this.colEmployeeNumber.Name = "colEmployeeNumber";
            this.colEmployeeNumber.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber.Visible = true;
            this.colEmployeeNumber.VisibleIndex = 4;
            this.colEmployeeNumber.Width = 83;
            // 
            // colTitle
            // 
            this.colTitle.Caption = "Title";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 5;
            this.colTitle.Width = 66;
            // 
            // colFirstname
            // 
            this.colFirstname.Caption = "Firstname";
            this.colFirstname.FieldName = "Firstname";
            this.colFirstname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colFirstname.Name = "colFirstname";
            this.colFirstname.OptionsColumn.AllowEdit = false;
            this.colFirstname.OptionsColumn.AllowFocus = false;
            this.colFirstname.OptionsColumn.ReadOnly = true;
            this.colFirstname.Visible = true;
            this.colFirstname.VisibleIndex = 1;
            this.colFirstname.Width = 111;
            // 
            // colMiddleNames
            // 
            this.colMiddleNames.Caption = "Middle Names";
            this.colMiddleNames.FieldName = "MiddleNames";
            this.colMiddleNames.Name = "colMiddleNames";
            this.colMiddleNames.OptionsColumn.AllowEdit = false;
            this.colMiddleNames.OptionsColumn.AllowFocus = false;
            this.colMiddleNames.OptionsColumn.ReadOnly = true;
            this.colMiddleNames.Visible = true;
            this.colMiddleNames.VisibleIndex = 6;
            this.colMiddleNames.Width = 86;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 0;
            this.colSurname.Width = 114;
            // 
            // colPreviousSurname
            // 
            this.colPreviousSurname.Caption = "Previous Surname";
            this.colPreviousSurname.FieldName = "PreviousSurname";
            this.colPreviousSurname.Name = "colPreviousSurname";
            this.colPreviousSurname.OptionsColumn.AllowEdit = false;
            this.colPreviousSurname.OptionsColumn.AllowFocus = false;
            this.colPreviousSurname.OptionsColumn.ReadOnly = true;
            this.colPreviousSurname.Visible = true;
            this.colPreviousSurname.VisibleIndex = 7;
            this.colPreviousSurname.Width = 107;
            // 
            // colCommonName
            // 
            this.colCommonName.Caption = "Known As";
            this.colCommonName.FieldName = "CommonName";
            this.colCommonName.Name = "colCommonName";
            this.colCommonName.OptionsColumn.AllowEdit = false;
            this.colCommonName.OptionsColumn.AllowFocus = false;
            this.colCommonName.OptionsColumn.ReadOnly = true;
            this.colCommonName.Visible = true;
            this.colCommonName.VisibleIndex = 8;
            this.colCommonName.Width = 69;
            // 
            // colComments
            // 
            this.colComments.Caption = "Remarks";
            this.colComments.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colComments.FieldName = "Comments";
            this.colComments.Name = "colComments";
            this.colComments.OptionsColumn.ReadOnly = true;
            this.colComments.Visible = true;
            this.colComments.VisibleIndex = 29;
            this.colComments.Width = 98;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colJoinedDate
            // 
            this.colJoinedDate.Caption = "Group Start Date";
            this.colJoinedDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colJoinedDate.FieldName = "JoinedDate";
            this.colJoinedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colJoinedDate.Name = "colJoinedDate";
            this.colJoinedDate.OptionsColumn.AllowEdit = false;
            this.colJoinedDate.OptionsColumn.AllowFocus = false;
            this.colJoinedDate.OptionsColumn.ReadOnly = true;
            this.colJoinedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colJoinedDate.Visible = true;
            this.colJoinedDate.VisibleIndex = 17;
            this.colJoinedDate.Width = 103;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "d";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colDoB
            // 
            this.colDoB.Caption = "Date of Birth";
            this.colDoB.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDoB.FieldName = "DoB";
            this.colDoB.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDoB.Name = "colDoB";
            this.colDoB.OptionsColumn.AllowEdit = false;
            this.colDoB.OptionsColumn.AllowFocus = false;
            this.colDoB.OptionsColumn.ReadOnly = true;
            this.colDoB.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDoB.Visible = true;
            this.colDoB.VisibleIndex = 14;
            this.colDoB.Width = 82;
            // 
            // colNatInsuranceNumber
            // 
            this.colNatInsuranceNumber.Caption = "National Insurance #";
            this.colNatInsuranceNumber.FieldName = "NatInsuranceNumber";
            this.colNatInsuranceNumber.Name = "colNatInsuranceNumber";
            this.colNatInsuranceNumber.OptionsColumn.AllowEdit = false;
            this.colNatInsuranceNumber.OptionsColumn.AllowFocus = false;
            this.colNatInsuranceNumber.OptionsColumn.ReadOnly = true;
            this.colNatInsuranceNumber.Visible = true;
            this.colNatInsuranceNumber.VisibleIndex = 95;
            this.colNatInsuranceNumber.Width = 122;
            // 
            // colWorkEmailAddress
            // 
            this.colWorkEmailAddress.Caption = "Work Email";
            this.colWorkEmailAddress.FieldName = "WorkEmailAddress";
            this.colWorkEmailAddress.Name = "colWorkEmailAddress";
            this.colWorkEmailAddress.OptionsColumn.AllowEdit = false;
            this.colWorkEmailAddress.OptionsColumn.AllowFocus = false;
            this.colWorkEmailAddress.OptionsColumn.ReadOnly = true;
            this.colWorkEmailAddress.Visible = true;
            this.colWorkEmailAddress.VisibleIndex = 19;
            this.colWorkEmailAddress.Width = 111;
            // 
            // colEthnicityId
            // 
            this.colEthnicityId.Caption = "Ethnicity ID";
            this.colEthnicityId.FieldName = "EthnicityId";
            this.colEthnicityId.Name = "colEthnicityId";
            this.colEthnicityId.OptionsColumn.AllowEdit = false;
            this.colEthnicityId.OptionsColumn.AllowFocus = false;
            this.colEthnicityId.OptionsColumn.ReadOnly = true;
            // 
            // colPersonalMobile
            // 
            this.colPersonalMobile.Caption = "Personal Mobile";
            this.colPersonalMobile.FieldName = "PersonalMobile";
            this.colPersonalMobile.Name = "colPersonalMobile";
            this.colPersonalMobile.OptionsColumn.AllowEdit = false;
            this.colPersonalMobile.OptionsColumn.AllowFocus = false;
            this.colPersonalMobile.OptionsColumn.ReadOnly = true;
            this.colPersonalMobile.Visible = true;
            this.colPersonalMobile.VisibleIndex = 24;
            this.colPersonalMobile.Width = 95;
            // 
            // colPersonalEmailAddress
            // 
            this.colPersonalEmailAddress.Caption = "Personal Email";
            this.colPersonalEmailAddress.FieldName = "PersonalEmailAddress";
            this.colPersonalEmailAddress.Name = "colPersonalEmailAddress";
            this.colPersonalEmailAddress.OptionsColumn.AllowEdit = false;
            this.colPersonalEmailAddress.OptionsColumn.AllowFocus = false;
            this.colPersonalEmailAddress.OptionsColumn.ReadOnly = true;
            this.colPersonalEmailAddress.Visible = true;
            this.colPersonalEmailAddress.VisibleIndex = 22;
            this.colPersonalEmailAddress.Width = 128;
            // 
            // colDateJoinedPension
            // 
            this.colDateJoinedPension.Caption = "Pension Joined Date";
            this.colDateJoinedPension.FieldName = "DateJoinedPension";
            this.colDateJoinedPension.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateJoinedPension.Name = "colDateJoinedPension";
            this.colDateJoinedPension.OptionsColumn.AllowEdit = false;
            this.colDateJoinedPension.OptionsColumn.AllowFocus = false;
            this.colDateJoinedPension.OptionsColumn.ReadOnly = true;
            this.colDateJoinedPension.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateJoinedPension.Visible = true;
            this.colDateJoinedPension.VisibleIndex = 25;
            this.colDateJoinedPension.Width = 118;
            // 
            // colDateLeftPension
            // 
            this.colDateLeftPension.Caption = "Pension Left Date";
            this.colDateLeftPension.FieldName = "DateLeftPension";
            this.colDateLeftPension.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateLeftPension.Name = "colDateLeftPension";
            this.colDateLeftPension.OptionsColumn.AllowEdit = false;
            this.colDateLeftPension.OptionsColumn.AllowFocus = false;
            this.colDateLeftPension.OptionsColumn.ReadOnly = true;
            this.colDateLeftPension.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateLeftPension.Visible = true;
            this.colDateLeftPension.VisibleIndex = 26;
            this.colDateLeftPension.Width = 106;
            // 
            // colYearsToAutoReEnrolment
            // 
            this.colYearsToAutoReEnrolment.Caption = "Years Until Re-Enrollment";
            this.colYearsToAutoReEnrolment.FieldName = "YearsToAutoReEnrolment";
            this.colYearsToAutoReEnrolment.Name = "colYearsToAutoReEnrolment";
            this.colYearsToAutoReEnrolment.OptionsColumn.AllowEdit = false;
            this.colYearsToAutoReEnrolment.OptionsColumn.AllowFocus = false;
            this.colYearsToAutoReEnrolment.OptionsColumn.ReadOnly = true;
            this.colYearsToAutoReEnrolment.Visible = true;
            this.colYearsToAutoReEnrolment.VisibleIndex = 27;
            this.colYearsToAutoReEnrolment.Width = 142;
            // 
            // colNextDateOfEnrolment
            // 
            this.colNextDateOfEnrolment.Caption = "Next Re-Enrollment Date";
            this.colNextDateOfEnrolment.FieldName = "NextDateOfEnrolment";
            this.colNextDateOfEnrolment.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextDateOfEnrolment.Name = "colNextDateOfEnrolment";
            this.colNextDateOfEnrolment.OptionsColumn.AllowEdit = false;
            this.colNextDateOfEnrolment.OptionsColumn.AllowFocus = false;
            this.colNextDateOfEnrolment.OptionsColumn.ReadOnly = true;
            this.colNextDateOfEnrolment.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextDateOfEnrolment.Visible = true;
            this.colNextDateOfEnrolment.VisibleIndex = 28;
            this.colNextDateOfEnrolment.Width = 140;
            // 
            // colGenderID
            // 
            this.colGenderID.Caption = "Gender ID";
            this.colGenderID.FieldName = "GenderID";
            this.colGenderID.Name = "colGenderID";
            this.colGenderID.OptionsColumn.AllowEdit = false;
            this.colGenderID.OptionsColumn.AllowFocus = false;
            this.colGenderID.OptionsColumn.ReadOnly = true;
            // 
            // colGender
            // 
            this.colGender.Caption = "Gender";
            this.colGender.FieldName = "Gender";
            this.colGender.Name = "colGender";
            this.colGender.OptionsColumn.AllowEdit = false;
            this.colGender.OptionsColumn.AllowFocus = false;
            this.colGender.OptionsColumn.ReadOnly = true;
            this.colGender.Visible = true;
            this.colGender.VisibleIndex = 9;
            this.colGender.Width = 58;
            // 
            // colEthnicity
            // 
            this.colEthnicity.Caption = "Ethnicity";
            this.colEthnicity.FieldName = "Ethnicity";
            this.colEthnicity.Name = "colEthnicity";
            this.colEthnicity.OptionsColumn.AllowEdit = false;
            this.colEthnicity.OptionsColumn.AllowFocus = false;
            this.colEthnicity.OptionsColumn.ReadOnly = true;
            this.colEthnicity.Visible = true;
            this.colEthnicity.VisibleIndex = 10;
            this.colEthnicity.Width = 62;
            // 
            // colAge
            // 
            this.colAge.Caption = "Age";
            this.colAge.FieldName = "Age";
            this.colAge.Name = "colAge";
            this.colAge.OptionsColumn.AllowEdit = false;
            this.colAge.OptionsColumn.AllowFocus = false;
            this.colAge.OptionsColumn.ReadOnly = true;
            this.colAge.Visible = true;
            this.colAge.VisibleIndex = 15;
            this.colAge.Width = 40;
            // 
            // colMaritalStatus
            // 
            this.colMaritalStatus.Caption = "Marital Status";
            this.colMaritalStatus.FieldName = "MaritalStatus";
            this.colMaritalStatus.Name = "colMaritalStatus";
            this.colMaritalStatus.OptionsColumn.AllowEdit = false;
            this.colMaritalStatus.OptionsColumn.AllowFocus = false;
            this.colMaritalStatus.OptionsColumn.ReadOnly = true;
            this.colMaritalStatus.Visible = true;
            this.colMaritalStatus.VisibleIndex = 12;
            this.colMaritalStatus.Width = 87;
            // 
            // colReligion
            // 
            this.colReligion.Caption = "Religion";
            this.colReligion.FieldName = "Religion";
            this.colReligion.Name = "colReligion";
            this.colReligion.OptionsColumn.AllowEdit = false;
            this.colReligion.OptionsColumn.AllowFocus = false;
            this.colReligion.OptionsColumn.ReadOnly = true;
            this.colReligion.Visible = true;
            this.colReligion.VisibleIndex = 11;
            // 
            // colDisability
            // 
            this.colDisability.Caption = "Disability";
            this.colDisability.FieldName = "Disability";
            this.colDisability.Name = "colDisability";
            this.colDisability.OptionsColumn.AllowEdit = false;
            this.colDisability.OptionsColumn.AllowFocus = false;
            this.colDisability.OptionsColumn.ReadOnly = true;
            this.colDisability.Visible = true;
            this.colDisability.VisibleIndex = 13;
            // 
            // colContinuousServiceDate
            // 
            this.colContinuousServiceDate.Caption = "Continuous Service Date";
            this.colContinuousServiceDate.FieldName = "ContinuousServiceDate";
            this.colContinuousServiceDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContinuousServiceDate.Name = "colContinuousServiceDate";
            this.colContinuousServiceDate.OptionsColumn.AllowEdit = false;
            this.colContinuousServiceDate.OptionsColumn.AllowFocus = false;
            this.colContinuousServiceDate.OptionsColumn.ReadOnly = true;
            this.colContinuousServiceDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContinuousServiceDate.Visible = true;
            this.colContinuousServiceDate.VisibleIndex = 18;
            this.colContinuousServiceDate.Width = 139;
            // 
            // colPicturePath
            // 
            this.colPicturePath.Caption = "Picture Path";
            this.colPicturePath.FieldName = "PicturePath";
            this.colPicturePath.Name = "colPicturePath";
            this.colPicturePath.OptionsColumn.AllowEdit = false;
            this.colPicturePath.OptionsColumn.AllowFocus = false;
            this.colPicturePath.OptionsColumn.ReadOnly = true;
            this.colPicturePath.Width = 79;
            // 
            // colWorkTelephone
            // 
            this.colWorkTelephone.Caption = "Work Landline";
            this.colWorkTelephone.FieldName = "WorkTelephone";
            this.colWorkTelephone.Name = "colWorkTelephone";
            this.colWorkTelephone.OptionsColumn.AllowEdit = false;
            this.colWorkTelephone.OptionsColumn.AllowFocus = false;
            this.colWorkTelephone.OptionsColumn.ReadOnly = true;
            this.colWorkTelephone.Visible = true;
            this.colWorkTelephone.VisibleIndex = 20;
            this.colWorkTelephone.Width = 88;
            // 
            // colStaffRecordCreated
            // 
            this.colStaffRecordCreated.Caption = "Staff Record";
            this.colStaffRecordCreated.ColumnEdit = this.repositoryItemButtonEditCreateStaff;
            this.colStaffRecordCreated.CustomizationCaption = "Create Staff Record";
            this.colStaffRecordCreated.FieldName = "StaffRecordCreated";
            this.colStaffRecordCreated.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colStaffRecordCreated.ImageOptions.Image")));
            this.colStaffRecordCreated.ImageOptions.ImageIndex = 4;
            this.colStaffRecordCreated.Name = "colStaffRecordCreated";
            this.colStaffRecordCreated.OptionsColumn.ReadOnly = true;
            this.colStaffRecordCreated.Visible = true;
            this.colStaffRecordCreated.VisibleIndex = 3;
            this.colStaffRecordCreated.Width = 110;
            // 
            // repositoryItemButtonEditCreateStaff
            // 
            this.repositoryItemButtonEditCreateStaff.AutoHeight = false;
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image10")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image11")));
            toolTipTitleItem7.Text = "Create Staff Record Button - Information";
            toolTipItem7.LeftIndent = 6;
            toolTipItem7.Text = resources.GetString("toolTipItem7.Text");
            superToolTip7.Items.Add(toolTipTitleItem7);
            superToolTip7.Items.Add(toolTipItem7);
            this.repositoryItemButtonEditCreateStaff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Create Staff Record", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", "create", superToolTip7, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditCreateStaff.Name = "repositoryItemButtonEditCreateStaff";
            this.repositoryItemButtonEditCreateStaff.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.repositoryItemButtonEditCreateStaff.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.repositoryItemButtonEditCreateStaff_ButtonClick);
            // 
            // colContractNumber3
            // 
            this.colContractNumber3.Caption = "Contract #";
            this.colContractNumber3.FieldName = "ContractNumber";
            this.colContractNumber3.Name = "colContractNumber3";
            this.colContractNumber3.OptionsColumn.AllowEdit = false;
            this.colContractNumber3.OptionsColumn.AllowFocus = false;
            this.colContractNumber3.OptionsColumn.ReadOnly = true;
            this.colContractNumber3.Visible = true;
            this.colContractNumber3.VisibleIndex = 30;
            // 
            // colJobTitle
            // 
            this.colJobTitle.Caption = "Job Title";
            this.colJobTitle.FieldName = "JobTitle";
            this.colJobTitle.Name = "colJobTitle";
            this.colJobTitle.OptionsColumn.AllowEdit = false;
            this.colJobTitle.OptionsColumn.AllowFocus = false;
            this.colJobTitle.OptionsColumn.ReadOnly = true;
            this.colJobTitle.Visible = true;
            this.colJobTitle.VisibleIndex = 31;
            this.colJobTitle.Width = 103;
            // 
            // colContractType1
            // 
            this.colContractType1.Caption = "Contract Type";
            this.colContractType1.FieldName = "ContractType";
            this.colContractType1.Name = "colContractType1";
            this.colContractType1.OptionsColumn.AllowEdit = false;
            this.colContractType1.OptionsColumn.AllowFocus = false;
            this.colContractType1.OptionsColumn.ReadOnly = true;
            this.colContractType1.Visible = true;
            this.colContractType1.VisibleIndex = 47;
            this.colContractType1.Width = 90;
            // 
            // colGenericJobTitle
            // 
            this.colGenericJobTitle.Caption = "Generic Job Title";
            this.colGenericJobTitle.FieldName = "GenericJobTitle";
            this.colGenericJobTitle.Name = "colGenericJobTitle";
            this.colGenericJobTitle.OptionsColumn.AllowEdit = false;
            this.colGenericJobTitle.OptionsColumn.AllowFocus = false;
            this.colGenericJobTitle.OptionsColumn.ReadOnly = true;
            this.colGenericJobTitle.Visible = true;
            this.colGenericJobTitle.VisibleIndex = 32;
            this.colGenericJobTitle.Width = 118;
            // 
            // colJobType
            // 
            this.colJobType.Caption = "Job Type";
            this.colJobType.FieldName = "JobType";
            this.colJobType.Name = "colJobType";
            this.colJobType.OptionsColumn.AllowEdit = false;
            this.colJobType.OptionsColumn.AllowFocus = false;
            this.colJobType.OptionsColumn.ReadOnly = true;
            this.colJobType.Visible = true;
            this.colJobType.VisibleIndex = 33;
            this.colJobType.Width = 91;
            // 
            // colContractBasis
            // 
            this.colContractBasis.Caption = "Contract Basis";
            this.colContractBasis.FieldName = "ContractBasis";
            this.colContractBasis.Name = "colContractBasis";
            this.colContractBasis.OptionsColumn.AllowEdit = false;
            this.colContractBasis.OptionsColumn.AllowFocus = false;
            this.colContractBasis.OptionsColumn.ReadOnly = true;
            this.colContractBasis.Visible = true;
            this.colContractBasis.VisibleIndex = 34;
            this.colContractBasis.Width = 101;
            // 
            // colContractStartDate
            // 
            this.colContractStartDate.Caption = "Contract Start";
            this.colContractStartDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colContractStartDate.FieldName = "ContractStartDate";
            this.colContractStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractStartDate.Name = "colContractStartDate";
            this.colContractStartDate.OptionsColumn.AllowEdit = false;
            this.colContractStartDate.OptionsColumn.AllowFocus = false;
            this.colContractStartDate.OptionsColumn.ReadOnly = true;
            this.colContractStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractStartDate.Visible = true;
            this.colContractStartDate.VisibleIndex = 35;
            this.colContractStartDate.Width = 90;
            // 
            // colContractEndDate
            // 
            this.colContractEndDate.Caption = "Contract End";
            this.colContractEndDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colContractEndDate.FieldName = "ContractEndDate";
            this.colContractEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractEndDate.Name = "colContractEndDate";
            this.colContractEndDate.OptionsColumn.AllowEdit = false;
            this.colContractEndDate.OptionsColumn.AllowFocus = false;
            this.colContractEndDate.OptionsColumn.ReadOnly = true;
            this.colContractEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractEndDate.Visible = true;
            this.colContractEndDate.VisibleIndex = 36;
            this.colContractEndDate.Width = 84;
            // 
            // colGroupCommencementDate1
            // 
            this.colGroupCommencementDate1.Caption = "Group Commencement";
            this.colGroupCommencementDate1.ColumnEdit = this.repositoryItemTextEditDate;
            this.colGroupCommencementDate1.FieldName = "GroupCommencementDate";
            this.colGroupCommencementDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colGroupCommencementDate1.Name = "colGroupCommencementDate1";
            this.colGroupCommencementDate1.OptionsColumn.AllowEdit = false;
            this.colGroupCommencementDate1.OptionsColumn.AllowFocus = false;
            this.colGroupCommencementDate1.OptionsColumn.ReadOnly = true;
            this.colGroupCommencementDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colGroupCommencementDate1.Visible = true;
            this.colGroupCommencementDate1.VisibleIndex = 37;
            this.colGroupCommencementDate1.Width = 129;
            // 
            // colAppointmentReason1
            // 
            this.colAppointmentReason1.Caption = "Appointment Reason";
            this.colAppointmentReason1.FieldName = "AppointmentReason";
            this.colAppointmentReason1.Name = "colAppointmentReason1";
            this.colAppointmentReason1.OptionsColumn.AllowEdit = false;
            this.colAppointmentReason1.OptionsColumn.AllowFocus = false;
            this.colAppointmentReason1.OptionsColumn.ReadOnly = true;
            this.colAppointmentReason1.Visible = true;
            this.colAppointmentReason1.VisibleIndex = 38;
            this.colAppointmentReason1.Width = 121;
            // 
            // colRecruitmentSourceType1
            // 
            this.colRecruitmentSourceType1.Caption = "Recruitment Source Type";
            this.colRecruitmentSourceType1.FieldName = "RecruitmentSourceType";
            this.colRecruitmentSourceType1.Name = "colRecruitmentSourceType1";
            this.colRecruitmentSourceType1.OptionsColumn.AllowEdit = false;
            this.colRecruitmentSourceType1.OptionsColumn.AllowFocus = false;
            this.colRecruitmentSourceType1.OptionsColumn.ReadOnly = true;
            this.colRecruitmentSourceType1.Visible = true;
            this.colRecruitmentSourceType1.VisibleIndex = 39;
            this.colRecruitmentSourceType1.Width = 142;
            // 
            // colRecruitmentSourceDetail1
            // 
            this.colRecruitmentSourceDetail1.Caption = "Recruitment Source";
            this.colRecruitmentSourceDetail1.FieldName = "RecruitmentSourceDetail";
            this.colRecruitmentSourceDetail1.Name = "colRecruitmentSourceDetail1";
            this.colRecruitmentSourceDetail1.OptionsColumn.AllowEdit = false;
            this.colRecruitmentSourceDetail1.OptionsColumn.AllowFocus = false;
            this.colRecruitmentSourceDetail1.OptionsColumn.ReadOnly = true;
            this.colRecruitmentSourceDetail1.Visible = true;
            this.colRecruitmentSourceDetail1.VisibleIndex = 40;
            this.colRecruitmentSourceDetail1.Width = 115;
            // 
            // colRecruitmentCost1
            // 
            this.colRecruitmentCost1.Caption = "Recruitment Cost";
            this.colRecruitmentCost1.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colRecruitmentCost1.FieldName = "RecruitmentCost";
            this.colRecruitmentCost1.Name = "colRecruitmentCost1";
            this.colRecruitmentCost1.OptionsColumn.AllowEdit = false;
            this.colRecruitmentCost1.OptionsColumn.AllowFocus = false;
            this.colRecruitmentCost1.OptionsColumn.ReadOnly = true;
            this.colRecruitmentCost1.Visible = true;
            this.colRecruitmentCost1.VisibleIndex = 41;
            this.colRecruitmentCost1.Width = 104;
            // 
            // repositoryItemTextEditMoney
            // 
            this.repositoryItemTextEditMoney.AutoHeight = false;
            this.repositoryItemTextEditMoney.Mask.EditMask = "c";
            this.repositoryItemTextEditMoney.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMoney.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMoney.Name = "repositoryItemTextEditMoney";
            // 
            // colActualHours
            // 
            this.colActualHours.Caption = "Actual Hours";
            this.colActualHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colActualHours.FieldName = "ActualHours";
            this.colActualHours.Name = "colActualHours";
            this.colActualHours.OptionsColumn.AllowEdit = false;
            this.colActualHours.OptionsColumn.AllowFocus = false;
            this.colActualHours.OptionsColumn.ReadOnly = true;
            this.colActualHours.Visible = true;
            this.colActualHours.VisibleIndex = 42;
            this.colActualHours.Width = 82;
            // 
            // repositoryItemTextEdit2DP
            // 
            this.repositoryItemTextEdit2DP.AutoHeight = false;
            this.repositoryItemTextEdit2DP.Mask.EditMask = "f2";
            this.repositoryItemTextEdit2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP.Name = "repositoryItemTextEdit2DP";
            // 
            // colFTEHours
            // 
            this.colFTEHours.Caption = "FTE Hours";
            this.colFTEHours.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colFTEHours.FieldName = "FTEHours";
            this.colFTEHours.Name = "colFTEHours";
            this.colFTEHours.OptionsColumn.AllowEdit = false;
            this.colFTEHours.OptionsColumn.AllowFocus = false;
            this.colFTEHours.OptionsColumn.ReadOnly = true;
            this.colFTEHours.Visible = true;
            this.colFTEHours.VisibleIndex = 43;
            // 
            // colFTERatio
            // 
            this.colFTERatio.Caption = "FTE Ratio";
            this.colFTERatio.ColumnEdit = this.repositoryItemTextEditPercentageMode2;
            this.colFTERatio.FieldName = "FTERatio";
            this.colFTERatio.Name = "colFTERatio";
            this.colFTERatio.OptionsColumn.AllowEdit = false;
            this.colFTERatio.OptionsColumn.AllowFocus = false;
            this.colFTERatio.OptionsColumn.ReadOnly = true;
            this.colFTERatio.Visible = true;
            this.colFTERatio.VisibleIndex = 44;
            // 
            // repositoryItemTextEditPercentageMode2
            // 
            this.repositoryItemTextEditPercentageMode2.AutoHeight = false;
            this.repositoryItemTextEditPercentageMode2.Mask.EditMask = "p";
            this.repositoryItemTextEditPercentageMode2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentageMode2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentageMode2.Name = "repositoryItemTextEditPercentageMode2";
            // 
            // colContractIssuedDate
            // 
            this.colContractIssuedDate.Caption = "Contract Issued";
            this.colContractIssuedDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colContractIssuedDate.FieldName = "ContractIssuedDate";
            this.colContractIssuedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractIssuedDate.Name = "colContractIssuedDate";
            this.colContractIssuedDate.OptionsColumn.AllowEdit = false;
            this.colContractIssuedDate.OptionsColumn.AllowFocus = false;
            this.colContractIssuedDate.OptionsColumn.ReadOnly = true;
            this.colContractIssuedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractIssuedDate.Visible = true;
            this.colContractIssuedDate.VisibleIndex = 45;
            this.colContractIssuedDate.Width = 98;
            // 
            // colContractReceivedDate
            // 
            this.colContractReceivedDate.Caption = "Contract Received";
            this.colContractReceivedDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colContractReceivedDate.FieldName = "ContractReceivedDate";
            this.colContractReceivedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContractReceivedDate.Name = "colContractReceivedDate";
            this.colContractReceivedDate.OptionsColumn.AllowEdit = false;
            this.colContractReceivedDate.OptionsColumn.AllowFocus = false;
            this.colContractReceivedDate.OptionsColumn.ReadOnly = true;
            this.colContractReceivedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContractReceivedDate.Visible = true;
            this.colContractReceivedDate.VisibleIndex = 46;
            this.colContractReceivedDate.Width = 110;
            // 
            // colIssuedHandbook
            // 
            this.colIssuedHandbook.Caption = "Handbook Issued";
            this.colIssuedHandbook.FieldName = "IssuedHandbook";
            this.colIssuedHandbook.Name = "colIssuedHandbook";
            this.colIssuedHandbook.OptionsColumn.AllowEdit = false;
            this.colIssuedHandbook.OptionsColumn.AllowFocus = false;
            this.colIssuedHandbook.OptionsColumn.ReadOnly = true;
            this.colIssuedHandbook.Visible = true;
            this.colIssuedHandbook.VisibleIndex = 48;
            this.colIssuedHandbook.Width = 104;
            // 
            // colTUPE1
            // 
            this.colTUPE1.Caption = "TUPE";
            this.colTUPE1.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colTUPE1.FieldName = "TUPE";
            this.colTUPE1.Name = "colTUPE1";
            this.colTUPE1.OptionsColumn.AllowEdit = false;
            this.colTUPE1.OptionsColumn.AllowFocus = false;
            this.colTUPE1.OptionsColumn.ReadOnly = true;
            this.colTUPE1.Visible = true;
            this.colTUPE1.VisibleIndex = 49;
            this.colTUPE1.Width = 46;
            // 
            // colTransferredFromLocalGovernment1
            // 
            this.colTransferredFromLocalGovernment1.Caption = "Transferred From Local Gov.";
            this.colTransferredFromLocalGovernment1.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colTransferredFromLocalGovernment1.FieldName = "TransferredFromLocalGovernment";
            this.colTransferredFromLocalGovernment1.Name = "colTransferredFromLocalGovernment1";
            this.colTransferredFromLocalGovernment1.OptionsColumn.AllowEdit = false;
            this.colTransferredFromLocalGovernment1.OptionsColumn.AllowFocus = false;
            this.colTransferredFromLocalGovernment1.OptionsColumn.ReadOnly = true;
            this.colTransferredFromLocalGovernment1.Visible = true;
            this.colTransferredFromLocalGovernment1.VisibleIndex = 50;
            this.colTransferredFromLocalGovernment1.Width = 158;
            // 
            // colTransferredFrom
            // 
            this.colTransferredFrom.Caption = "Transferred From";
            this.colTransferredFrom.FieldName = "TransferredFrom";
            this.colTransferredFrom.Name = "colTransferredFrom";
            this.colTransferredFrom.OptionsColumn.AllowEdit = false;
            this.colTransferredFrom.OptionsColumn.AllowFocus = false;
            this.colTransferredFrom.OptionsColumn.ReadOnly = true;
            this.colTransferredFrom.Visible = true;
            this.colTransferredFrom.VisibleIndex = 51;
            this.colTransferredFrom.Width = 105;
            // 
            // colNJCConditionsApply1
            // 
            this.colNJCConditionsApply1.Caption = "NJC Conditions Apply";
            this.colNJCConditionsApply1.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colNJCConditionsApply1.FieldName = "NJCConditionsApply";
            this.colNJCConditionsApply1.Name = "colNJCConditionsApply1";
            this.colNJCConditionsApply1.OptionsColumn.AllowEdit = false;
            this.colNJCConditionsApply1.OptionsColumn.AllowFocus = false;
            this.colNJCConditionsApply1.OptionsColumn.ReadOnly = true;
            this.colNJCConditionsApply1.Visible = true;
            this.colNJCConditionsApply1.VisibleIndex = 52;
            this.colNJCConditionsApply1.Width = 123;
            // 
            // colTransferDate1
            // 
            this.colTransferDate1.Caption = "Transfer Date";
            this.colTransferDate1.ColumnEdit = this.repositoryItemTextEditDate;
            this.colTransferDate1.FieldName = "TransferDate";
            this.colTransferDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTransferDate1.Name = "colTransferDate1";
            this.colTransferDate1.OptionsColumn.AllowEdit = false;
            this.colTransferDate1.OptionsColumn.AllowFocus = false;
            this.colTransferDate1.OptionsColumn.ReadOnly = true;
            this.colTransferDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colTransferDate1.Visible = true;
            this.colTransferDate1.VisibleIndex = 93;
            this.colTransferDate1.Width = 88;
            // 
            // colOriginatingCompany
            // 
            this.colOriginatingCompany.Caption = "Originating Company";
            this.colOriginatingCompany.FieldName = "OriginatingCompany";
            this.colOriginatingCompany.Name = "colOriginatingCompany";
            this.colOriginatingCompany.OptionsColumn.AllowEdit = false;
            this.colOriginatingCompany.OptionsColumn.AllowFocus = false;
            this.colOriginatingCompany.OptionsColumn.ReadOnly = true;
            this.colOriginatingCompany.Visible = true;
            this.colOriginatingCompany.VisibleIndex = 53;
            this.colOriginatingCompany.Width = 121;
            // 
            // colInductionDate
            // 
            this.colInductionDate.Caption = "Induction Date";
            this.colInductionDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colInductionDate.FieldName = "InductionDate";
            this.colInductionDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colInductionDate.Name = "colInductionDate";
            this.colInductionDate.OptionsColumn.AllowEdit = false;
            this.colInductionDate.OptionsColumn.AllowFocus = false;
            this.colInductionDate.OptionsColumn.ReadOnly = true;
            this.colInductionDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colInductionDate.Visible = true;
            this.colInductionDate.VisibleIndex = 56;
            this.colInductionDate.Width = 92;
            // 
            // colInductionChecklistReturned
            // 
            this.colInductionChecklistReturned.Caption = "Induction Checklist Returned";
            this.colInductionChecklistReturned.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colInductionChecklistReturned.FieldName = "InductionChecklistReturned";
            this.colInductionChecklistReturned.Name = "colInductionChecklistReturned";
            this.colInductionChecklistReturned.OptionsColumn.AllowEdit = false;
            this.colInductionChecklistReturned.OptionsColumn.AllowFocus = false;
            this.colInductionChecklistReturned.OptionsColumn.ReadOnly = true;
            this.colInductionChecklistReturned.Visible = true;
            this.colInductionChecklistReturned.VisibleIndex = 57;
            this.colInductionChecklistReturned.Width = 159;
            // 
            // colHolidayUnitDescriptor2
            // 
            this.colHolidayUnitDescriptor2.Caption = "Holiday Unit Decriptor";
            this.colHolidayUnitDescriptor2.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor2.Name = "colHolidayUnitDescriptor2";
            this.colHolidayUnitDescriptor2.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor2.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor2.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor2.Visible = true;
            this.colHolidayUnitDescriptor2.VisibleIndex = 58;
            this.colHolidayUnitDescriptor2.Width = 125;
            // 
            // colAnnualLeaveAnniversaryDate1
            // 
            this.colAnnualLeaveAnniversaryDate1.Caption = "Annual Leave Anniversary";
            this.colAnnualLeaveAnniversaryDate1.ColumnEdit = this.repositoryItemTextEditDate;
            this.colAnnualLeaveAnniversaryDate1.FieldName = "AnnualLeaveAnniversaryDate";
            this.colAnnualLeaveAnniversaryDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colAnnualLeaveAnniversaryDate1.Name = "colAnnualLeaveAnniversaryDate1";
            this.colAnnualLeaveAnniversaryDate1.OptionsColumn.AllowEdit = false;
            this.colAnnualLeaveAnniversaryDate1.OptionsColumn.AllowFocus = false;
            this.colAnnualLeaveAnniversaryDate1.OptionsColumn.ReadOnly = true;
            this.colAnnualLeaveAnniversaryDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colAnnualLeaveAnniversaryDate1.Visible = true;
            this.colAnnualLeaveAnniversaryDate1.VisibleIndex = 94;
            this.colAnnualLeaveAnniversaryDate1.Width = 147;
            // 
            // colBasicLeaveEntitlementFTE
            // 
            this.colBasicLeaveEntitlementFTE.Caption = "Basic Leave Entitlement FTE";
            this.colBasicLeaveEntitlementFTE.ColumnEdit = this.repositoryItemTextEditDays;
            this.colBasicLeaveEntitlementFTE.FieldName = "BasicLeaveEntitlementFTE";
            this.colBasicLeaveEntitlementFTE.Name = "colBasicLeaveEntitlementFTE";
            this.colBasicLeaveEntitlementFTE.OptionsColumn.AllowEdit = false;
            this.colBasicLeaveEntitlementFTE.OptionsColumn.AllowFocus = false;
            this.colBasicLeaveEntitlementFTE.OptionsColumn.ReadOnly = true;
            this.colBasicLeaveEntitlementFTE.Visible = true;
            this.colBasicLeaveEntitlementFTE.VisibleIndex = 62;
            this.colBasicLeaveEntitlementFTE.Width = 155;
            // 
            // repositoryItemTextEditDays
            // 
            this.repositoryItemTextEditDays.AutoHeight = false;
            this.repositoryItemTextEditDays.Mask.EditMask = "#####0.00 Days";
            this.repositoryItemTextEditDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDays.Name = "repositoryItemTextEditDays";
            // 
            // colBasicLeaveEntitlement
            // 
            this.colBasicLeaveEntitlement.Caption = "Basic Leave Entitlement";
            this.colBasicLeaveEntitlement.ColumnEdit = this.repositoryItemTextEditDays;
            this.colBasicLeaveEntitlement.FieldName = "BasicLeaveEntitlement";
            this.colBasicLeaveEntitlement.Name = "colBasicLeaveEntitlement";
            this.colBasicLeaveEntitlement.OptionsColumn.AllowEdit = false;
            this.colBasicLeaveEntitlement.OptionsColumn.AllowFocus = false;
            this.colBasicLeaveEntitlement.OptionsColumn.ReadOnly = true;
            this.colBasicLeaveEntitlement.Visible = true;
            this.colBasicLeaveEntitlement.VisibleIndex = 63;
            this.colBasicLeaveEntitlement.Width = 134;
            // 
            // colBankHolidayEntitlementFTE
            // 
            this.colBankHolidayEntitlementFTE.Caption = "Bank Holiday Entitlement FTE";
            this.colBankHolidayEntitlementFTE.ColumnEdit = this.repositoryItemTextEditDays;
            this.colBankHolidayEntitlementFTE.FieldName = "BankHolidayEntitlementFTE";
            this.colBankHolidayEntitlementFTE.Name = "colBankHolidayEntitlementFTE";
            this.colBankHolidayEntitlementFTE.OptionsColumn.AllowEdit = false;
            this.colBankHolidayEntitlementFTE.OptionsColumn.AllowFocus = false;
            this.colBankHolidayEntitlementFTE.OptionsColumn.ReadOnly = true;
            this.colBankHolidayEntitlementFTE.Visible = true;
            this.colBankHolidayEntitlementFTE.VisibleIndex = 60;
            this.colBankHolidayEntitlementFTE.Width = 160;
            // 
            // colBankHolidayEntitlement1
            // 
            this.colBankHolidayEntitlement1.Caption = "Bank Holiday Entitlement";
            this.colBankHolidayEntitlement1.ColumnEdit = this.repositoryItemTextEditDays;
            this.colBankHolidayEntitlement1.FieldName = "BankHolidayEntitlement";
            this.colBankHolidayEntitlement1.Name = "colBankHolidayEntitlement1";
            this.colBankHolidayEntitlement1.OptionsColumn.AllowEdit = false;
            this.colBankHolidayEntitlement1.OptionsColumn.AllowFocus = false;
            this.colBankHolidayEntitlement1.OptionsColumn.ReadOnly = true;
            this.colBankHolidayEntitlement1.Visible = true;
            this.colBankHolidayEntitlement1.VisibleIndex = 61;
            this.colBankHolidayEntitlement1.Width = 139;
            // 
            // colYearlyLeaveIncrementStartDate
            // 
            this.colYearlyLeaveIncrementStartDate.Caption = "Yearly Leave Increment Start";
            this.colYearlyLeaveIncrementStartDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colYearlyLeaveIncrementStartDate.FieldName = "YearlyLeaveIncrementStartDate";
            this.colYearlyLeaveIncrementStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colYearlyLeaveIncrementStartDate.Name = "colYearlyLeaveIncrementStartDate";
            this.colYearlyLeaveIncrementStartDate.OptionsColumn.AllowEdit = false;
            this.colYearlyLeaveIncrementStartDate.OptionsColumn.AllowFocus = false;
            this.colYearlyLeaveIncrementStartDate.OptionsColumn.ReadOnly = true;
            this.colYearlyLeaveIncrementStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colYearlyLeaveIncrementStartDate.Visible = true;
            this.colYearlyLeaveIncrementStartDate.VisibleIndex = 64;
            this.colYearlyLeaveIncrementStartDate.Width = 162;
            // 
            // colYearlyLeaveIncrementAmount
            // 
            this.colYearlyLeaveIncrementAmount.Caption = "Yearly Leave Increment";
            this.colYearlyLeaveIncrementAmount.ColumnEdit = this.repositoryItemTextEditDays;
            this.colYearlyLeaveIncrementAmount.FieldName = "YearlyLeaveIncrementAmount";
            this.colYearlyLeaveIncrementAmount.Name = "colYearlyLeaveIncrementAmount";
            this.colYearlyLeaveIncrementAmount.OptionsColumn.AllowEdit = false;
            this.colYearlyLeaveIncrementAmount.OptionsColumn.AllowFocus = false;
            this.colYearlyLeaveIncrementAmount.OptionsColumn.ReadOnly = true;
            this.colYearlyLeaveIncrementAmount.Visible = true;
            this.colYearlyLeaveIncrementAmount.VisibleIndex = 65;
            this.colYearlyLeaveIncrementAmount.Width = 135;
            // 
            // colMaximumLeaveEntitlement1
            // 
            this.colMaximumLeaveEntitlement1.Caption = "Maximum Leave Entitlement";
            this.colMaximumLeaveEntitlement1.ColumnEdit = this.repositoryItemTextEditDays;
            this.colMaximumLeaveEntitlement1.FieldName = "MaximumLeaveEntitlement";
            this.colMaximumLeaveEntitlement1.Name = "colMaximumLeaveEntitlement1";
            this.colMaximumLeaveEntitlement1.OptionsColumn.AllowEdit = false;
            this.colMaximumLeaveEntitlement1.OptionsColumn.AllowFocus = false;
            this.colMaximumLeaveEntitlement1.OptionsColumn.ReadOnly = true;
            this.colMaximumLeaveEntitlement1.Visible = true;
            this.colMaximumLeaveEntitlement1.VisibleIndex = 66;
            this.colMaximumLeaveEntitlement1.Width = 154;
            // 
            // colLastPayReviewDate1
            // 
            this.colLastPayReviewDate1.Caption = "Last Pay Review";
            this.colLastPayReviewDate1.ColumnEdit = this.repositoryItemTextEditDate;
            this.colLastPayReviewDate1.FieldName = "LastPayReviewDate";
            this.colLastPayReviewDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastPayReviewDate1.Name = "colLastPayReviewDate1";
            this.colLastPayReviewDate1.OptionsColumn.AllowEdit = false;
            this.colLastPayReviewDate1.OptionsColumn.AllowFocus = false;
            this.colLastPayReviewDate1.OptionsColumn.ReadOnly = true;
            this.colLastPayReviewDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastPayReviewDate1.Visible = true;
            this.colLastPayReviewDate1.VisibleIndex = 76;
            this.colLastPayReviewDate1.Width = 100;
            // 
            // colNextPayReviewDate1
            // 
            this.colNextPayReviewDate1.Caption = "Next Pay Review";
            this.colNextPayReviewDate1.ColumnEdit = this.repositoryItemTextEditDate;
            this.colNextPayReviewDate1.FieldName = "NextPayReviewDate";
            this.colNextPayReviewDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colNextPayReviewDate1.Name = "colNextPayReviewDate1";
            this.colNextPayReviewDate1.OptionsColumn.AllowEdit = false;
            this.colNextPayReviewDate1.OptionsColumn.AllowFocus = false;
            this.colNextPayReviewDate1.OptionsColumn.ReadOnly = true;
            this.colNextPayReviewDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colNextPayReviewDate1.Visible = true;
            this.colNextPayReviewDate1.VisibleIndex = 77;
            this.colNextPayReviewDate1.Width = 103;
            // 
            // colLastIncreaseAmount
            // 
            this.colLastIncreaseAmount.Caption = "Last Increase Amount";
            this.colLastIncreaseAmount.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colLastIncreaseAmount.FieldName = "LastIncreaseAmount";
            this.colLastIncreaseAmount.Name = "colLastIncreaseAmount";
            this.colLastIncreaseAmount.OptionsColumn.AllowEdit = false;
            this.colLastIncreaseAmount.OptionsColumn.AllowFocus = false;
            this.colLastIncreaseAmount.OptionsColumn.ReadOnly = true;
            this.colLastIncreaseAmount.Visible = true;
            this.colLastIncreaseAmount.VisibleIndex = 78;
            this.colLastIncreaseAmount.Width = 126;
            // 
            // colLastIncreasePercentage
            // 
            this.colLastIncreasePercentage.Caption = "Last Increase %";
            this.colLastIncreasePercentage.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colLastIncreasePercentage.FieldName = "LastIncreasePercentage";
            this.colLastIncreasePercentage.Name = "colLastIncreasePercentage";
            this.colLastIncreasePercentage.OptionsColumn.AllowEdit = false;
            this.colLastIncreasePercentage.OptionsColumn.AllowFocus = false;
            this.colLastIncreasePercentage.OptionsColumn.ReadOnly = true;
            this.colLastIncreasePercentage.Visible = true;
            this.colLastIncreasePercentage.VisibleIndex = 79;
            this.colLastIncreasePercentage.Width = 100;
            // 
            // repositoryItemTextEditPercentage2
            // 
            this.repositoryItemTextEditPercentage2.AutoHeight = false;
            this.repositoryItemTextEditPercentage2.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage2.Name = "repositoryItemTextEditPercentage2";
            // 
            // colTerminationDate1
            // 
            this.colTerminationDate1.Caption = "Termination Date";
            this.colTerminationDate1.ColumnEdit = this.repositoryItemTextEditDate;
            this.colTerminationDate1.FieldName = "TerminationDate";
            this.colTerminationDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colTerminationDate1.Name = "colTerminationDate1";
            this.colTerminationDate1.OptionsColumn.AllowEdit = false;
            this.colTerminationDate1.OptionsColumn.AllowFocus = false;
            this.colTerminationDate1.OptionsColumn.ReadOnly = true;
            this.colTerminationDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colTerminationDate1.Visible = true;
            this.colTerminationDate1.VisibleIndex = 109;
            this.colTerminationDate1.Width = 103;
            // 
            // colLastDateWorked
            // 
            this.colLastDateWorked.Caption = "Last Date Worked";
            this.colLastDateWorked.ColumnEdit = this.repositoryItemTextEditDate;
            this.colLastDateWorked.FieldName = "LastDateWorked";
            this.colLastDateWorked.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colLastDateWorked.Name = "colLastDateWorked";
            this.colLastDateWorked.OptionsColumn.AllowEdit = false;
            this.colLastDateWorked.OptionsColumn.AllowFocus = false;
            this.colLastDateWorked.OptionsColumn.ReadOnly = true;
            this.colLastDateWorked.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colLastDateWorked.Visible = true;
            this.colLastDateWorked.VisibleIndex = 110;
            this.colLastDateWorked.Width = 107;
            // 
            // colReasonForLastDateWorkedDifference
            // 
            this.colReasonForLastDateWorkedDifference.Caption = "Reason for Date Difference";
            this.colReasonForLastDateWorkedDifference.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colReasonForLastDateWorkedDifference.CustomizationCaption = "Reason for Last Date Worked Difference";
            this.colReasonForLastDateWorkedDifference.FieldName = "ReasonForLastDateWorkedDifference";
            this.colReasonForLastDateWorkedDifference.Name = "colReasonForLastDateWorkedDifference";
            this.colReasonForLastDateWorkedDifference.OptionsColumn.ReadOnly = true;
            this.colReasonForLastDateWorkedDifference.Visible = true;
            this.colReasonForLastDateWorkedDifference.VisibleIndex = 111;
            this.colReasonForLastDateWorkedDifference.Width = 153;
            // 
            // colLeavingReason1
            // 
            this.colLeavingReason1.Caption = "Leaving Reason";
            this.colLeavingReason1.FieldName = "LeavingReason";
            this.colLeavingReason1.Name = "colLeavingReason1";
            this.colLeavingReason1.OptionsColumn.ReadOnly = true;
            this.colLeavingReason1.Visible = true;
            this.colLeavingReason1.VisibleIndex = 112;
            this.colLeavingReason1.Width = 97;
            // 
            // colLeavingRemarks
            // 
            this.colLeavingRemarks.Caption = "Leaving Remarks";
            this.colLeavingRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colLeavingRemarks.FieldName = "LeavingRemarks";
            this.colLeavingRemarks.Name = "colLeavingRemarks";
            this.colLeavingRemarks.OptionsColumn.ReadOnly = true;
            this.colLeavingRemarks.Visible = true;
            this.colLeavingRemarks.VisibleIndex = 113;
            this.colLeavingRemarks.Width = 102;
            // 
            // colConsiderReEmployement
            // 
            this.colConsiderReEmployement.Caption = "Consider Re-employement";
            this.colConsiderReEmployement.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colConsiderReEmployement.FieldName = "ConsiderReEmployement";
            this.colConsiderReEmployement.Name = "colConsiderReEmployement";
            this.colConsiderReEmployement.OptionsColumn.AllowEdit = false;
            this.colConsiderReEmployement.OptionsColumn.AllowFocus = false;
            this.colConsiderReEmployement.OptionsColumn.ReadOnly = true;
            this.colConsiderReEmployement.Visible = true;
            this.colConsiderReEmployement.VisibleIndex = 117;
            this.colConsiderReEmployement.Width = 147;
            // 
            // colConsiderReEmploymentReason1
            // 
            this.colConsiderReEmploymentReason1.Caption = "Consider Re-employement Reason";
            this.colConsiderReEmploymentReason1.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colConsiderReEmploymentReason1.FieldName = "ConsiderReEmploymentReason";
            this.colConsiderReEmploymentReason1.Name = "colConsiderReEmploymentReason1";
            this.colConsiderReEmploymentReason1.OptionsColumn.ReadOnly = true;
            this.colConsiderReEmploymentReason1.Visible = true;
            this.colConsiderReEmploymentReason1.VisibleIndex = 118;
            this.colConsiderReEmploymentReason1.Width = 186;
            // 
            // colExitInterviewDate1
            // 
            this.colExitInterviewDate1.Caption = "Exit Interview Date";
            this.colExitInterviewDate1.ColumnEdit = this.repositoryItemTextEditDate;
            this.colExitInterviewDate1.FieldName = "ExitInterviewDate";
            this.colExitInterviewDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExitInterviewDate1.Name = "colExitInterviewDate1";
            this.colExitInterviewDate1.OptionsColumn.AllowEdit = false;
            this.colExitInterviewDate1.OptionsColumn.AllowFocus = false;
            this.colExitInterviewDate1.OptionsColumn.ReadOnly = true;
            this.colExitInterviewDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExitInterviewDate1.Visible = true;
            this.colExitInterviewDate1.VisibleIndex = 115;
            this.colExitInterviewDate1.Width = 114;
            // 
            // colExitInterviewerName1
            // 
            this.colExitInterviewerName1.Caption = "Exit Interviewer Name";
            this.colExitInterviewerName1.FieldName = "ExitInterviewerName";
            this.colExitInterviewerName1.Name = "colExitInterviewerName1";
            this.colExitInterviewerName1.OptionsColumn.AllowEdit = false;
            this.colExitInterviewerName1.OptionsColumn.AllowFocus = false;
            this.colExitInterviewerName1.OptionsColumn.ReadOnly = true;
            this.colExitInterviewerName1.Visible = true;
            this.colExitInterviewerName1.VisibleIndex = 116;
            this.colExitInterviewerName1.Width = 128;
            // 
            // colTrainingCostsToRecover
            // 
            this.colTrainingCostsToRecover.Caption = "Training Costs To Recover";
            this.colTrainingCostsToRecover.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colTrainingCostsToRecover.FieldName = "TrainingCostsToRecover";
            this.colTrainingCostsToRecover.Name = "colTrainingCostsToRecover";
            this.colTrainingCostsToRecover.OptionsColumn.AllowEdit = false;
            this.colTrainingCostsToRecover.OptionsColumn.AllowFocus = false;
            this.colTrainingCostsToRecover.OptionsColumn.ReadOnly = true;
            this.colTrainingCostsToRecover.Visible = true;
            this.colTrainingCostsToRecover.VisibleIndex = 121;
            this.colTrainingCostsToRecover.Width = 147;
            // 
            // colSchemeCostsToRecover
            // 
            this.colSchemeCostsToRecover.Caption = "Scheme Costs To Recover";
            this.colSchemeCostsToRecover.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colSchemeCostsToRecover.FieldName = "SchemeCostsToRecover";
            this.colSchemeCostsToRecover.Name = "colSchemeCostsToRecover";
            this.colSchemeCostsToRecover.OptionsColumn.AllowEdit = false;
            this.colSchemeCostsToRecover.OptionsColumn.AllowFocus = false;
            this.colSchemeCostsToRecover.OptionsColumn.ReadOnly = true;
            this.colSchemeCostsToRecover.Visible = true;
            this.colSchemeCostsToRecover.VisibleIndex = 120;
            this.colSchemeCostsToRecover.Width = 146;
            // 
            // colEquipmentCostsToRecover
            // 
            this.colEquipmentCostsToRecover.Caption = "Equipment Costs To Recover";
            this.colEquipmentCostsToRecover.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colEquipmentCostsToRecover.FieldName = "EquipmentCostsToRecover";
            this.colEquipmentCostsToRecover.Name = "colEquipmentCostsToRecover";
            this.colEquipmentCostsToRecover.OptionsColumn.AllowEdit = false;
            this.colEquipmentCostsToRecover.OptionsColumn.AllowFocus = false;
            this.colEquipmentCostsToRecover.OptionsColumn.ReadOnly = true;
            this.colEquipmentCostsToRecover.Visible = true;
            this.colEquipmentCostsToRecover.VisibleIndex = 122;
            this.colEquipmentCostsToRecover.Width = 159;
            // 
            // colSalary
            // 
            this.colSalary.AppearanceCell.Options.UseTextOptions = true;
            this.colSalary.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colSalary.Caption = "Salary";
            this.colSalary.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colSalary.FieldName = "Salary";
            this.colSalary.Name = "colSalary";
            this.colSalary.OptionsColumn.AllowEdit = false;
            this.colSalary.OptionsColumn.AllowFocus = false;
            this.colSalary.OptionsColumn.ReadOnly = true;
            this.colSalary.Visible = true;
            this.colSalary.VisibleIndex = 74;
            // 
            // colSalaryBanding1
            // 
            this.colSalaryBanding1.Caption = "Salary Banding";
            this.colSalaryBanding1.FieldName = "SalaryBanding";
            this.colSalaryBanding1.Name = "colSalaryBanding1";
            this.colSalaryBanding1.OptionsColumn.AllowEdit = false;
            this.colSalaryBanding1.OptionsColumn.AllowFocus = false;
            this.colSalaryBanding1.OptionsColumn.ReadOnly = true;
            this.colSalaryBanding1.Visible = true;
            this.colSalaryBanding1.VisibleIndex = 75;
            this.colSalaryBanding1.Width = 92;
            // 
            // colPayrollType
            // 
            this.colPayrollType.Caption = "Payroll Type";
            this.colPayrollType.FieldName = "PayrollType";
            this.colPayrollType.Name = "colPayrollType";
            this.colPayrollType.OptionsColumn.AllowEdit = false;
            this.colPayrollType.OptionsColumn.AllowFocus = false;
            this.colPayrollType.OptionsColumn.ReadOnly = true;
            this.colPayrollType.Visible = true;
            this.colPayrollType.VisibleIndex = 98;
            this.colPayrollType.Width = 80;
            // 
            // colPaymentType1
            // 
            this.colPaymentType1.Caption = "Payment Type";
            this.colPaymentType1.FieldName = "PaymentType";
            this.colPaymentType1.Name = "colPaymentType1";
            this.colPaymentType1.OptionsColumn.AllowEdit = false;
            this.colPaymentType1.OptionsColumn.AllowFocus = false;
            this.colPaymentType1.OptionsColumn.ReadOnly = true;
            this.colPaymentType1.Visible = true;
            this.colPaymentType1.VisibleIndex = 97;
            this.colPaymentType1.Width = 90;
            // 
            // colPaymentFrequency1
            // 
            this.colPaymentFrequency1.Caption = "Payment Frequency";
            this.colPaymentFrequency1.FieldName = "PaymentFrequency";
            this.colPaymentFrequency1.Name = "colPaymentFrequency1";
            this.colPaymentFrequency1.OptionsColumn.AllowEdit = false;
            this.colPaymentFrequency1.OptionsColumn.AllowFocus = false;
            this.colPaymentFrequency1.OptionsColumn.ReadOnly = true;
            this.colPaymentFrequency1.Visible = true;
            this.colPaymentFrequency1.VisibleIndex = 100;
            this.colPaymentFrequency1.Width = 117;
            // 
            // colPayrollReference1
            // 
            this.colPayrollReference1.Caption = "Payroll Reference";
            this.colPayrollReference1.FieldName = "PayrollReference";
            this.colPayrollReference1.Name = "colPayrollReference1";
            this.colPayrollReference1.OptionsColumn.AllowEdit = false;
            this.colPayrollReference1.OptionsColumn.AllowFocus = false;
            this.colPayrollReference1.OptionsColumn.ReadOnly = true;
            this.colPayrollReference1.Visible = true;
            this.colPayrollReference1.VisibleIndex = 99;
            this.colPayrollReference1.Width = 106;
            // 
            // colPaymentAmount1
            // 
            this.colPaymentAmount1.Caption = "Payment Amount";
            this.colPaymentAmount1.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colPaymentAmount1.FieldName = "PaymentAmount";
            this.colPaymentAmount1.Name = "colPaymentAmount1";
            this.colPaymentAmount1.OptionsColumn.AllowEdit = false;
            this.colPaymentAmount1.OptionsColumn.AllowFocus = false;
            this.colPaymentAmount1.OptionsColumn.ReadOnly = true;
            this.colPaymentAmount1.Visible = true;
            this.colPaymentAmount1.VisibleIndex = 101;
            this.colPaymentAmount1.Width = 103;
            // 
            // colMonthlyPayDayNumber1
            // 
            this.colMonthlyPayDayNumber1.Caption = "Pay Day";
            this.colMonthlyPayDayNumber1.FieldName = "MonthlyPayDayNumber";
            this.colMonthlyPayDayNumber1.Name = "colMonthlyPayDayNumber1";
            this.colMonthlyPayDayNumber1.OptionsColumn.AllowEdit = false;
            this.colMonthlyPayDayNumber1.OptionsColumn.AllowFocus = false;
            this.colMonthlyPayDayNumber1.OptionsColumn.ReadOnly = true;
            this.colMonthlyPayDayNumber1.Visible = true;
            this.colMonthlyPayDayNumber1.VisibleIndex = 102;
            this.colMonthlyPayDayNumber1.Width = 102;
            // 
            // colSickPayEntitlementUnits
            // 
            this.colSickPayEntitlementUnits.Caption = "Sick Pay Entitlement 1";
            this.colSickPayEntitlementUnits.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colSickPayEntitlementUnits.FieldName = "SickPayEntitlementUnits";
            this.colSickPayEntitlementUnits.Name = "colSickPayEntitlementUnits";
            this.colSickPayEntitlementUnits.OptionsColumn.AllowEdit = false;
            this.colSickPayEntitlementUnits.OptionsColumn.AllowFocus = false;
            this.colSickPayEntitlementUnits.OptionsColumn.ReadOnly = true;
            this.colSickPayEntitlementUnits.Visible = true;
            this.colSickPayEntitlementUnits.VisibleIndex = 68;
            this.colSickPayEntitlementUnits.Width = 126;
            // 
            // colSickPayUnitDescriptor
            // 
            this.colSickPayUnitDescriptor.Caption = "Sick Pay Unit Descriptor 1";
            this.colSickPayUnitDescriptor.FieldName = "SickPayUnitDescriptor";
            this.colSickPayUnitDescriptor.Name = "colSickPayUnitDescriptor";
            this.colSickPayUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colSickPayUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colSickPayUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colSickPayUnitDescriptor.Visible = true;
            this.colSickPayUnitDescriptor.VisibleIndex = 69;
            this.colSickPayUnitDescriptor.Width = 143;
            // 
            // colElegibleForBonus
            // 
            this.colElegibleForBonus.Caption = "Eligible For Bonus";
            this.colElegibleForBonus.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colElegibleForBonus.FieldName = "ElegibleForBonus";
            this.colElegibleForBonus.Name = "colElegibleForBonus";
            this.colElegibleForBonus.OptionsColumn.AllowEdit = false;
            this.colElegibleForBonus.OptionsColumn.AllowFocus = false;
            this.colElegibleForBonus.OptionsColumn.ReadOnly = true;
            this.colElegibleForBonus.Visible = true;
            this.colElegibleForBonus.VisibleIndex = 80;
            this.colElegibleForBonus.Width = 108;
            // 
            // colElegibleForOvertime
            // 
            this.colElegibleForOvertime.Caption = "Eligible For Overtime";
            this.colElegibleForOvertime.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colElegibleForOvertime.FieldName = "ElegibleForOvertime";
            this.colElegibleForOvertime.Name = "colElegibleForOvertime";
            this.colElegibleForOvertime.OptionsColumn.AllowEdit = false;
            this.colElegibleForOvertime.OptionsColumn.AllowFocus = false;
            this.colElegibleForOvertime.OptionsColumn.ReadOnly = true;
            this.colElegibleForOvertime.Visible = true;
            this.colElegibleForOvertime.VisibleIndex = 82;
            this.colElegibleForOvertime.Width = 123;
            // 
            // colElegibleForShares
            // 
            this.colElegibleForShares.Caption = "Eligible For Shares";
            this.colElegibleForShares.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colElegibleForShares.FieldName = "ElegibleForShares";
            this.colElegibleForShares.Name = "colElegibleForShares";
            this.colElegibleForShares.OptionsColumn.AllowEdit = false;
            this.colElegibleForShares.OptionsColumn.AllowFocus = false;
            this.colElegibleForShares.OptionsColumn.ReadOnly = true;
            this.colElegibleForShares.Visible = true;
            this.colElegibleForShares.VisibleIndex = 81;
            this.colElegibleForShares.Width = 112;
            // 
            // colPayrollNumber
            // 
            this.colPayrollNumber.Caption = "Payroll #";
            this.colPayrollNumber.FieldName = "PayrollNumber";
            this.colPayrollNumber.Name = "colPayrollNumber";
            this.colPayrollNumber.OptionsColumn.AllowEdit = false;
            this.colPayrollNumber.OptionsColumn.AllowFocus = false;
            this.colPayrollNumber.OptionsColumn.ReadOnly = true;
            this.colPayrollNumber.Visible = true;
            this.colPayrollNumber.VisibleIndex = 96;
            // 
            // colPersonalTelephone
            // 
            this.colPersonalTelephone.Caption = "Personal Landline";
            this.colPersonalTelephone.FieldName = "PersonalTelephone";
            this.colPersonalTelephone.Name = "colPersonalTelephone";
            this.colPersonalTelephone.OptionsColumn.AllowEdit = false;
            this.colPersonalTelephone.OptionsColumn.AllowFocus = false;
            this.colPersonalTelephone.OptionsColumn.ReadOnly = true;
            this.colPersonalTelephone.Visible = true;
            this.colPersonalTelephone.VisibleIndex = 23;
            this.colPersonalTelephone.Width = 104;
            // 
            // colWorkMobile
            // 
            this.colWorkMobile.Caption = "Work Mobile";
            this.colWorkMobile.FieldName = "WorkMobile";
            this.colWorkMobile.Name = "colWorkMobile";
            this.colWorkMobile.OptionsColumn.AllowEdit = false;
            this.colWorkMobile.OptionsColumn.AllowFocus = false;
            this.colWorkMobile.OptionsColumn.ReadOnly = true;
            this.colWorkMobile.Visible = true;
            this.colWorkMobile.VisibleIndex = 21;
            this.colWorkMobile.Width = 79;
            // 
            // colBankName
            // 
            this.colBankName.Caption = "Bank Name";
            this.colBankName.FieldName = "BankName";
            this.colBankName.Name = "colBankName";
            this.colBankName.OptionsColumn.AllowEdit = false;
            this.colBankName.OptionsColumn.AllowFocus = false;
            this.colBankName.OptionsColumn.ReadOnly = true;
            this.colBankName.Visible = true;
            this.colBankName.VisibleIndex = 103;
            // 
            // colBankSortCode
            // 
            this.colBankSortCode.Caption = "Bank Sort Code";
            this.colBankSortCode.FieldName = "BankSortCode";
            this.colBankSortCode.Name = "colBankSortCode";
            this.colBankSortCode.OptionsColumn.AllowEdit = false;
            this.colBankSortCode.OptionsColumn.AllowFocus = false;
            this.colBankSortCode.OptionsColumn.ReadOnly = true;
            this.colBankSortCode.Visible = true;
            this.colBankSortCode.VisibleIndex = 104;
            this.colBankSortCode.Width = 95;
            // 
            // colBankAccountNumber
            // 
            this.colBankAccountNumber.Caption = "Bank Account #";
            this.colBankAccountNumber.FieldName = "BankAccountNumber";
            this.colBankAccountNumber.Name = "colBankAccountNumber";
            this.colBankAccountNumber.OptionsColumn.AllowEdit = false;
            this.colBankAccountNumber.OptionsColumn.AllowFocus = false;
            this.colBankAccountNumber.OptionsColumn.ReadOnly = true;
            this.colBankAccountNumber.Visible = true;
            this.colBankAccountNumber.VisibleIndex = 106;
            this.colBankAccountNumber.Width = 97;
            // 
            // colBankAccountName
            // 
            this.colBankAccountName.Caption = "Bank Account Name";
            this.colBankAccountName.FieldName = "BankAccountName";
            this.colBankAccountName.Name = "colBankAccountName";
            this.colBankAccountName.OptionsColumn.AllowEdit = false;
            this.colBankAccountName.OptionsColumn.AllowFocus = false;
            this.colBankAccountName.OptionsColumn.ReadOnly = true;
            this.colBankAccountName.Visible = true;
            this.colBankAccountName.VisibleIndex = 105;
            this.colBankAccountName.Width = 116;
            // 
            // colPayslipEmail
            // 
            this.colPayslipEmail.Caption = "Payslip Email";
            this.colPayslipEmail.FieldName = "PayslipEmail";
            this.colPayslipEmail.Name = "colPayslipEmail";
            this.colPayslipEmail.OptionsColumn.AllowEdit = false;
            this.colPayslipEmail.OptionsColumn.AllowFocus = false;
            this.colPayslipEmail.OptionsColumn.ReadOnly = true;
            this.colPayslipEmail.Visible = true;
            this.colPayslipEmail.VisibleIndex = 107;
            this.colPayslipEmail.Width = 81;
            // 
            // colPayslipPassword
            // 
            this.colPayslipPassword.Caption = "Payslip Password";
            this.colPayslipPassword.FieldName = "PayslipPassword";
            this.colPayslipPassword.Name = "colPayslipPassword";
            this.colPayslipPassword.OptionsColumn.AllowEdit = false;
            this.colPayslipPassword.OptionsColumn.AllowFocus = false;
            this.colPayslipPassword.OptionsColumn.ReadOnly = true;
            this.colPayslipPassword.Visible = true;
            this.colPayslipPassword.VisibleIndex = 108;
            this.colPayslipPassword.Width = 103;
            // 
            // colAnyOtherOwing
            // 
            this.colAnyOtherOwing.Caption = "Any Other Owing";
            this.colAnyOtherOwing.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colAnyOtherOwing.FieldName = "AnyOtherOwing";
            this.colAnyOtherOwing.Name = "colAnyOtherOwing";
            this.colAnyOtherOwing.OptionsColumn.AllowEdit = false;
            this.colAnyOtherOwing.OptionsColumn.AllowFocus = false;
            this.colAnyOtherOwing.OptionsColumn.ReadOnly = true;
            this.colAnyOtherOwing.Visible = true;
            this.colAnyOtherOwing.VisibleIndex = 123;
            this.colAnyOtherOwing.Width = 104;
            // 
            // colHolidaysAccrued
            // 
            this.colHolidaysAccrued.Caption = "Holidays - Accrued";
            this.colHolidaysAccrued.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colHolidaysAccrued.FieldName = "HolidaysAccrued";
            this.colHolidaysAccrued.Name = "colHolidaysAccrued";
            this.colHolidaysAccrued.OptionsColumn.AllowEdit = false;
            this.colHolidaysAccrued.OptionsColumn.AllowFocus = false;
            this.colHolidaysAccrued.OptionsColumn.ReadOnly = true;
            this.colHolidaysAccrued.Visible = true;
            this.colHolidaysAccrued.VisibleIndex = 124;
            this.colHolidaysAccrued.Width = 110;
            // 
            // colHolidaysBalance
            // 
            this.colHolidaysBalance.Caption = "Holidays - Balance";
            this.colHolidaysBalance.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colHolidaysBalance.FieldName = "HolidaysBalance";
            this.colHolidaysBalance.Name = "colHolidaysBalance";
            this.colHolidaysBalance.OptionsColumn.AllowEdit = false;
            this.colHolidaysBalance.OptionsColumn.AllowFocus = false;
            this.colHolidaysBalance.OptionsColumn.ReadOnly = true;
            this.colHolidaysBalance.Visible = true;
            this.colHolidaysBalance.VisibleIndex = 126;
            this.colHolidaysBalance.Width = 108;
            // 
            // colHolidaysTaken1
            // 
            this.colHolidaysTaken1.Caption = "Holidays - Taken";
            this.colHolidaysTaken1.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colHolidaysTaken1.FieldName = "HolidaysTaken";
            this.colHolidaysTaken1.Name = "colHolidaysTaken1";
            this.colHolidaysTaken1.OptionsColumn.AllowEdit = false;
            this.colHolidaysTaken1.OptionsColumn.AllowFocus = false;
            this.colHolidaysTaken1.OptionsColumn.ReadOnly = true;
            this.colHolidaysTaken1.Visible = true;
            this.colHolidaysTaken1.VisibleIndex = 125;
            this.colHolidaysTaken1.Width = 100;
            // 
            // colHolidayUnitDescriptorID
            // 
            this.colHolidayUnitDescriptorID.Caption = "Holiday Unit Descriptor ID";
            this.colHolidayUnitDescriptorID.FieldName = "HolidayUnitDescriptorID";
            this.colHolidayUnitDescriptorID.Name = "colHolidayUnitDescriptorID";
            this.colHolidayUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptorID.Width = 144;
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.Caption = "Linked Documents";
            this.colLinkedDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocuments;
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.Visible = true;
            this.colLinkedDocumentCount.VisibleIndex = 147;
            this.colLinkedDocumentCount.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocuments
            // 
            this.repositoryItemHyperLinkEditLinkedDocuments.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocuments.Name = "repositoryItemHyperLinkEditLinkedDocuments";
            this.repositoryItemHyperLinkEditLinkedDocuments.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocuments.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocuments_OpenLink);
            // 
            // colJobTitleID
            // 
            this.colJobTitleID.Caption = "Job Title ID";
            this.colJobTitleID.FieldName = "JobTitleID";
            this.colJobTitleID.Name = "colJobTitleID";
            this.colJobTitleID.OptionsColumn.AllowEdit = false;
            this.colJobTitleID.OptionsColumn.AllowFocus = false;
            this.colJobTitleID.OptionsColumn.ReadOnly = true;
            // 
            // colAgreedReferenceInPlace
            // 
            this.colAgreedReferenceInPlace.Caption = "Agreed Reference In Place";
            this.colAgreedReferenceInPlace.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colAgreedReferenceInPlace.FieldName = "AgreedReferenceInPlace";
            this.colAgreedReferenceInPlace.Name = "colAgreedReferenceInPlace";
            this.colAgreedReferenceInPlace.OptionsColumn.AllowEdit = false;
            this.colAgreedReferenceInPlace.OptionsColumn.AllowFocus = false;
            this.colAgreedReferenceInPlace.OptionsColumn.ReadOnly = true;
            this.colAgreedReferenceInPlace.Visible = true;
            this.colAgreedReferenceInPlace.VisibleIndex = 119;
            this.colAgreedReferenceInPlace.Width = 150;
            // 
            // colEligibleForMedicalCover
            // 
            this.colEligibleForMedicalCover.Caption = "Eligible For Medical Cover";
            this.colEligibleForMedicalCover.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colEligibleForMedicalCover.FieldName = "EligibleForMedicalCover";
            this.colEligibleForMedicalCover.Name = "colEligibleForMedicalCover";
            this.colEligibleForMedicalCover.OptionsColumn.AllowEdit = false;
            this.colEligibleForMedicalCover.OptionsColumn.AllowFocus = false;
            this.colEligibleForMedicalCover.OptionsColumn.ReadOnly = true;
            this.colEligibleForMedicalCover.Visible = true;
            this.colEligibleForMedicalCover.VisibleIndex = 84;
            this.colEligibleForMedicalCover.Width = 142;
            // 
            // colMedicalCoverProvider
            // 
            this.colMedicalCoverProvider.Caption = "Medical Cover Provider";
            this.colMedicalCoverProvider.FieldName = "MedicalCoverProvider";
            this.colMedicalCoverProvider.Name = "colMedicalCoverProvider";
            this.colMedicalCoverProvider.OptionsColumn.AllowEdit = false;
            this.colMedicalCoverProvider.OptionsColumn.AllowFocus = false;
            this.colMedicalCoverProvider.OptionsColumn.ReadOnly = true;
            this.colMedicalCoverProvider.Visible = true;
            this.colMedicalCoverProvider.VisibleIndex = 85;
            this.colMedicalCoverProvider.Width = 131;
            // 
            // colMedicalCoverEmployerContribution
            // 
            this.colMedicalCoverEmployerContribution.AppearanceCell.Options.UseTextOptions = true;
            this.colMedicalCoverEmployerContribution.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colMedicalCoverEmployerContribution.Caption = "Medical Cover Employer Contribution";
            this.colMedicalCoverEmployerContribution.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colMedicalCoverEmployerContribution.FieldName = "MedicalCoverEmployerContribution";
            this.colMedicalCoverEmployerContribution.Name = "colMedicalCoverEmployerContribution";
            this.colMedicalCoverEmployerContribution.OptionsColumn.AllowEdit = false;
            this.colMedicalCoverEmployerContribution.OptionsColumn.AllowFocus = false;
            this.colMedicalCoverEmployerContribution.OptionsColumn.ReadOnly = true;
            this.colMedicalCoverEmployerContribution.Visible = true;
            this.colMedicalCoverEmployerContribution.VisibleIndex = 86;
            this.colMedicalCoverEmployerContribution.Width = 197;
            // 
            // colMedicalCoverEmployerContributionDescriptor
            // 
            this.colMedicalCoverEmployerContributionDescriptor.Caption = "Medical Cover Employer Contribution Descriptor";
            this.colMedicalCoverEmployerContributionDescriptor.FieldName = "MedicalCoverEmployerContributionDescriptor";
            this.colMedicalCoverEmployerContributionDescriptor.Name = "colMedicalCoverEmployerContributionDescriptor";
            this.colMedicalCoverEmployerContributionDescriptor.OptionsColumn.AllowEdit = false;
            this.colMedicalCoverEmployerContributionDescriptor.OptionsColumn.AllowFocus = false;
            this.colMedicalCoverEmployerContributionDescriptor.OptionsColumn.ReadOnly = true;
            this.colMedicalCoverEmployerContributionDescriptor.Width = 249;
            // 
            // colMedicalCoverEmployerContributionFrequency
            // 
            this.colMedicalCoverEmployerContributionFrequency.Caption = "Medical Cover Employer Contribution Frequency";
            this.colMedicalCoverEmployerContributionFrequency.FieldName = "MedicalCoverEmployerContributionFrequency";
            this.colMedicalCoverEmployerContributionFrequency.Name = "colMedicalCoverEmployerContributionFrequency";
            this.colMedicalCoverEmployerContributionFrequency.OptionsColumn.AllowEdit = false;
            this.colMedicalCoverEmployerContributionFrequency.OptionsColumn.AllowFocus = false;
            this.colMedicalCoverEmployerContributionFrequency.OptionsColumn.ReadOnly = true;
            this.colMedicalCoverEmployerContributionFrequency.Visible = true;
            this.colMedicalCoverEmployerContributionFrequency.VisibleIndex = 87;
            this.colMedicalCoverEmployerContributionFrequency.Width = 251;
            // 
            // colCTWScheme
            // 
            this.colCTWScheme.Caption = "CTW Scheme";
            this.colCTWScheme.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colCTWScheme.FieldName = "CTWScheme";
            this.colCTWScheme.Name = "colCTWScheme";
            this.colCTWScheme.OptionsColumn.AllowEdit = false;
            this.colCTWScheme.OptionsColumn.AllowFocus = false;
            this.colCTWScheme.OptionsColumn.ReadOnly = true;
            this.colCTWScheme.Visible = true;
            this.colCTWScheme.VisibleIndex = 88;
            this.colCTWScheme.Width = 84;
            // 
            // colCTWSchemeStartDate
            // 
            this.colCTWSchemeStartDate.Caption = "CTW Scheme Start";
            this.colCTWSchemeStartDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colCTWSchemeStartDate.FieldName = "CTWSchemeStartDate";
            this.colCTWSchemeStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCTWSchemeStartDate.Name = "colCTWSchemeStartDate";
            this.colCTWSchemeStartDate.OptionsColumn.AllowEdit = false;
            this.colCTWSchemeStartDate.OptionsColumn.AllowFocus = false;
            this.colCTWSchemeStartDate.OptionsColumn.ReadOnly = true;
            this.colCTWSchemeStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCTWSchemeStartDate.Visible = true;
            this.colCTWSchemeStartDate.VisibleIndex = 89;
            this.colCTWSchemeStartDate.Width = 111;
            // 
            // colCTWSchemeEndDate
            // 
            this.colCTWSchemeEndDate.Caption = "CTW Scheme End";
            this.colCTWSchemeEndDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colCTWSchemeEndDate.FieldName = "CTWSchemeEndDate";
            this.colCTWSchemeEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCTWSchemeEndDate.Name = "colCTWSchemeEndDate";
            this.colCTWSchemeEndDate.OptionsColumn.AllowEdit = false;
            this.colCTWSchemeEndDate.OptionsColumn.AllowFocus = false;
            this.colCTWSchemeEndDate.OptionsColumn.ReadOnly = true;
            this.colCTWSchemeEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCTWSchemeEndDate.Visible = true;
            this.colCTWSchemeEndDate.VisibleIndex = 90;
            this.colCTWSchemeEndDate.Width = 105;
            // 
            // colCTWSchemeValue
            // 
            this.colCTWSchemeValue.AppearanceCell.Options.UseTextOptions = true;
            this.colCTWSchemeValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colCTWSchemeValue.Caption = "CTW Scheme Value";
            this.colCTWSchemeValue.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colCTWSchemeValue.FieldName = "CTWSchemeValue";
            this.colCTWSchemeValue.Name = "colCTWSchemeValue";
            this.colCTWSchemeValue.OptionsColumn.AllowEdit = false;
            this.colCTWSchemeValue.OptionsColumn.AllowFocus = false;
            this.colCTWSchemeValue.OptionsColumn.ReadOnly = true;
            this.colCTWSchemeValue.Visible = true;
            this.colCTWSchemeValue.VisibleIndex = 91;
            this.colCTWSchemeValue.Width = 113;
            // 
            // colCTWSchemeEndOption
            // 
            this.colCTWSchemeEndOption.Caption = "CTW Scheme End Option";
            this.colCTWSchemeEndOption.FieldName = "CTWSchemeEndOption";
            this.colCTWSchemeEndOption.Name = "colCTWSchemeEndOption";
            this.colCTWSchemeEndOption.OptionsColumn.AllowEdit = false;
            this.colCTWSchemeEndOption.OptionsColumn.AllowFocus = false;
            this.colCTWSchemeEndOption.OptionsColumn.ReadOnly = true;
            this.colCTWSchemeEndOption.Visible = true;
            this.colCTWSchemeEndOption.VisibleIndex = 92;
            this.colCTWSchemeEndOption.Width = 140;
            // 
            // colChildCareVouchers
            // 
            this.colChildCareVouchers.Caption = "Child Care Vouchers";
            this.colChildCareVouchers.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colChildCareVouchers.FieldName = "ChildCareVouchers";
            this.colChildCareVouchers.Name = "colChildCareVouchers";
            this.colChildCareVouchers.OptionsColumn.AllowEdit = false;
            this.colChildCareVouchers.OptionsColumn.AllowFocus = false;
            this.colChildCareVouchers.OptionsColumn.ReadOnly = true;
            this.colChildCareVouchers.Visible = true;
            this.colChildCareVouchers.VisibleIndex = 83;
            this.colChildCareVouchers.Width = 117;
            // 
            // colExitInterviewRequired
            // 
            this.colExitInterviewRequired.Caption = "Exit Interview Required";
            this.colExitInterviewRequired.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colExitInterviewRequired.FieldName = "ExitInterviewRequired";
            this.colExitInterviewRequired.Name = "colExitInterviewRequired";
            this.colExitInterviewRequired.OptionsColumn.AllowEdit = false;
            this.colExitInterviewRequired.OptionsColumn.AllowFocus = false;
            this.colExitInterviewRequired.OptionsColumn.ReadOnly = true;
            this.colExitInterviewRequired.Visible = true;
            this.colExitInterviewRequired.VisibleIndex = 114;
            this.colExitInterviewRequired.Width = 134;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.Width = 62;
            // 
            // colNoticePeriodUnits
            // 
            this.colNoticePeriodUnits.Caption = "Notice Period Units";
            this.colNoticePeriodUnits.FieldName = "NoticePeriodUnits";
            this.colNoticePeriodUnits.Name = "colNoticePeriodUnits";
            this.colNoticePeriodUnits.OptionsColumn.AllowEdit = false;
            this.colNoticePeriodUnits.OptionsColumn.AllowFocus = false;
            this.colNoticePeriodUnits.OptionsColumn.ReadOnly = true;
            this.colNoticePeriodUnits.Visible = true;
            this.colNoticePeriodUnits.VisibleIndex = 127;
            this.colNoticePeriodUnits.Width = 111;
            // 
            // colNoticePeriodUnitDescriptor
            // 
            this.colNoticePeriodUnitDescriptor.Caption = "Notice Period Descriptor";
            this.colNoticePeriodUnitDescriptor.FieldName = "NoticePeriodUnitDescriptor";
            this.colNoticePeriodUnitDescriptor.Name = "colNoticePeriodUnitDescriptor";
            this.colNoticePeriodUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colNoticePeriodUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colNoticePeriodUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colNoticePeriodUnitDescriptor.Visible = true;
            this.colNoticePeriodUnitDescriptor.VisibleIndex = 128;
            this.colNoticePeriodUnitDescriptor.Width = 136;
            // 
            // colInviteSummerBBQ
            // 
            this.colInviteSummerBBQ.Caption = "Invite Summer BBQ";
            this.colInviteSummerBBQ.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colInviteSummerBBQ.FieldName = "InviteSummerBBQ";
            this.colInviteSummerBBQ.Name = "colInviteSummerBBQ";
            this.colInviteSummerBBQ.OptionsColumn.AllowEdit = false;
            this.colInviteSummerBBQ.OptionsColumn.AllowFocus = false;
            this.colInviteSummerBBQ.OptionsColumn.ReadOnly = true;
            this.colInviteSummerBBQ.Visible = true;
            this.colInviteSummerBBQ.VisibleIndex = 130;
            this.colInviteSummerBBQ.Width = 113;
            // 
            // colInviteWinterParty
            // 
            this.colInviteWinterParty.Caption = "Invite Winter Party";
            this.colInviteWinterParty.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colInviteWinterParty.FieldName = "InviteWinterParty";
            this.colInviteWinterParty.Name = "colInviteWinterParty";
            this.colInviteWinterParty.OptionsColumn.AllowEdit = false;
            this.colInviteWinterParty.OptionsColumn.AllowFocus = false;
            this.colInviteWinterParty.OptionsColumn.ReadOnly = true;
            this.colInviteWinterParty.Visible = true;
            this.colInviteWinterParty.VisibleIndex = 131;
            this.colInviteWinterParty.Width = 113;
            // 
            // colInviteManagementConference
            // 
            this.colInviteManagementConference.Caption = "Invite Management Conference";
            this.colInviteManagementConference.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colInviteManagementConference.FieldName = "InviteManagementConference";
            this.colInviteManagementConference.Name = "colInviteManagementConference";
            this.colInviteManagementConference.OptionsColumn.AllowEdit = false;
            this.colInviteManagementConference.OptionsColumn.AllowFocus = false;
            this.colInviteManagementConference.OptionsColumn.ReadOnly = true;
            this.colInviteManagementConference.Visible = true;
            this.colInviteManagementConference.VisibleIndex = 132;
            this.colInviteManagementConference.Width = 173;
            // 
            // colSickPayPercentage1
            // 
            this.colSickPayPercentage1.Caption = "Sick Pay % 1";
            this.colSickPayPercentage1.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSickPayPercentage1.FieldName = "SickPayPercentage1";
            this.colSickPayPercentage1.Name = "colSickPayPercentage1";
            this.colSickPayPercentage1.OptionsColumn.AllowEdit = false;
            this.colSickPayPercentage1.OptionsColumn.AllowFocus = false;
            this.colSickPayPercentage1.OptionsColumn.ReadOnly = true;
            this.colSickPayPercentage1.Visible = true;
            this.colSickPayPercentage1.VisibleIndex = 70;
            this.colSickPayPercentage1.Width = 83;
            // 
            // colSickPayUnits2
            // 
            this.colSickPayUnits2.Caption = "Sick Pay Entitlement 2";
            this.colSickPayUnits2.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colSickPayUnits2.FieldName = "SickPayUnits2";
            this.colSickPayUnits2.Name = "colSickPayUnits2";
            this.colSickPayUnits2.OptionsColumn.AllowEdit = false;
            this.colSickPayUnits2.OptionsColumn.AllowFocus = false;
            this.colSickPayUnits2.OptionsColumn.ReadOnly = true;
            this.colSickPayUnits2.Visible = true;
            this.colSickPayUnits2.VisibleIndex = 71;
            this.colSickPayUnits2.Width = 126;
            // 
            // colSickPayUnitDescriptor2
            // 
            this.colSickPayUnitDescriptor2.Caption = "Sick Pay Unit Descriptor 2";
            this.colSickPayUnitDescriptor2.FieldName = "SickPayUnitDescriptor2";
            this.colSickPayUnitDescriptor2.Name = "colSickPayUnitDescriptor2";
            this.colSickPayUnitDescriptor2.OptionsColumn.AllowEdit = false;
            this.colSickPayUnitDescriptor2.OptionsColumn.AllowFocus = false;
            this.colSickPayUnitDescriptor2.OptionsColumn.ReadOnly = true;
            this.colSickPayUnitDescriptor2.Visible = true;
            this.colSickPayUnitDescriptor2.VisibleIndex = 72;
            this.colSickPayUnitDescriptor2.Width = 143;
            // 
            // colSickPayPercentage2
            // 
            this.colSickPayPercentage2.Caption = "Sick Pay % 2";
            this.colSickPayPercentage2.ColumnEdit = this.repositoryItemTextEditPercentage2;
            this.colSickPayPercentage2.FieldName = "SickPayPercentage2";
            this.colSickPayPercentage2.Name = "colSickPayPercentage2";
            this.colSickPayPercentage2.OptionsColumn.AllowEdit = false;
            this.colSickPayPercentage2.OptionsColumn.AllowFocus = false;
            this.colSickPayPercentage2.OptionsColumn.ReadOnly = true;
            this.colSickPayPercentage2.Visible = true;
            this.colSickPayPercentage2.VisibleIndex = 73;
            this.colSickPayPercentage2.Width = 83;
            // 
            // colEmployeePassword
            // 
            this.colEmployeePassword.Caption = "Employee Password";
            this.colEmployeePassword.FieldName = "EmployeePassword";
            this.colEmployeePassword.Name = "colEmployeePassword";
            this.colEmployeePassword.OptionsColumn.AllowEdit = false;
            this.colEmployeePassword.OptionsColumn.AllowFocus = false;
            this.colEmployeePassword.OptionsColumn.ReadOnly = true;
            this.colEmployeePassword.Visible = true;
            this.colEmployeePassword.VisibleIndex = 129;
            this.colEmployeePassword.Width = 116;
            // 
            // colBiography
            // 
            this.colBiography.Caption = "Biography";
            this.colBiography.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colBiography.FieldName = "Biography";
            this.colBiography.Name = "colBiography";
            this.colBiography.OptionsColumn.ReadOnly = true;
            this.colBiography.Visible = true;
            this.colBiography.VisibleIndex = 16;
            // 
            // colShiftFTE
            // 
            this.colShiftFTE.Caption = "Shift FTE";
            this.colShiftFTE.ColumnEdit = this.repositoryItemTextEdit2DP;
            this.colShiftFTE.FieldName = "ShiftFTE";
            this.colShiftFTE.Name = "colShiftFTE";
            this.colShiftFTE.OptionsColumn.AllowEdit = false;
            this.colShiftFTE.OptionsColumn.AllowFocus = false;
            this.colShiftFTE.OptionsColumn.ReadOnly = true;
            this.colShiftFTE.Visible = true;
            this.colShiftFTE.VisibleIndex = 59;
            // 
            // colExtraDaysHolidayYear1
            // 
            this.colExtraDaysHolidayYear1.Caption = "Extra Days Holiday Year 1";
            this.colExtraDaysHolidayYear1.FieldName = "ExtraDaysHolidayYear1";
            this.colExtraDaysHolidayYear1.Name = "colExtraDaysHolidayYear1";
            this.colExtraDaysHolidayYear1.OptionsColumn.AllowEdit = false;
            this.colExtraDaysHolidayYear1.OptionsColumn.AllowFocus = false;
            this.colExtraDaysHolidayYear1.OptionsColumn.ReadOnly = true;
            this.colExtraDaysHolidayYear1.Visible = true;
            this.colExtraDaysHolidayYear1.VisibleIndex = 67;
            this.colExtraDaysHolidayYear1.Width = 144;
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 57;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Width = 134;
            // 
            // colSuppressBirthday
            // 
            this.colSuppressBirthday.Caption = "Suppress Birthday";
            this.colSuppressBirthday.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colSuppressBirthday.FieldName = "SuppressBirthday";
            this.colSuppressBirthday.Name = "colSuppressBirthday";
            this.colSuppressBirthday.OptionsColumn.AllowEdit = false;
            this.colSuppressBirthday.OptionsColumn.AllowFocus = false;
            this.colSuppressBirthday.OptionsColumn.ReadOnly = true;
            this.colSuppressBirthday.Visible = true;
            this.colSuppressBirthday.VisibleIndex = 133;
            this.colSuppressBirthday.Width = 106;
            // 
            // colPreviousEmploymentSalary
            // 
            this.colPreviousEmploymentSalary.Caption = "Previous Employment Salary";
            this.colPreviousEmploymentSalary.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colPreviousEmploymentSalary.FieldName = "PreviousEmploymentSalary";
            this.colPreviousEmploymentSalary.Name = "colPreviousEmploymentSalary";
            this.colPreviousEmploymentSalary.OptionsColumn.AllowEdit = false;
            this.colPreviousEmploymentSalary.OptionsColumn.AllowFocus = false;
            this.colPreviousEmploymentSalary.OptionsColumn.ReadOnly = true;
            this.colPreviousEmploymentSalary.Visible = true;
            this.colPreviousEmploymentSalary.VisibleIndex = 54;
            this.colPreviousEmploymentSalary.Width = 154;
            // 
            // colSalaryExpectation
            // 
            this.colSalaryExpectation.Caption = "Salary Expectation";
            this.colSalaryExpectation.ColumnEdit = this.repositoryItemTextEditMoney;
            this.colSalaryExpectation.FieldName = "SalaryExpectation";
            this.colSalaryExpectation.Name = "colSalaryExpectation";
            this.colSalaryExpectation.OptionsColumn.AllowEdit = false;
            this.colSalaryExpectation.OptionsColumn.AllowFocus = false;
            this.colSalaryExpectation.OptionsColumn.ReadOnly = true;
            this.colSalaryExpectation.Visible = true;
            this.colSalaryExpectation.VisibleIndex = 55;
            this.colSalaryExpectation.Width = 109;
            // 
            // colCurrentTermsAndConditions
            // 
            this.colCurrentTermsAndConditions.Caption = "Current T & C";
            this.colCurrentTermsAndConditions.FieldName = "CurrentTermsAndConditions";
            this.colCurrentTermsAndConditions.Name = "colCurrentTermsAndConditions";
            this.colCurrentTermsAndConditions.OptionsColumn.AllowEdit = false;
            this.colCurrentTermsAndConditions.OptionsColumn.AllowFocus = false;
            this.colCurrentTermsAndConditions.OptionsColumn.ReadOnly = true;
            this.colCurrentTermsAndConditions.Visible = true;
            this.colCurrentTermsAndConditions.VisibleIndex = 134;
            this.colCurrentTermsAndConditions.Width = 130;
            // 
            // colCurrentTermsAndConditionsDate
            // 
            this.colCurrentTermsAndConditionsDate.Caption = "T & C Date";
            this.colCurrentTermsAndConditionsDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colCurrentTermsAndConditionsDate.FieldName = "CurrentTermsAndConditionsDate";
            this.colCurrentTermsAndConditionsDate.Name = "colCurrentTermsAndConditionsDate";
            this.colCurrentTermsAndConditionsDate.OptionsColumn.AllowEdit = false;
            this.colCurrentTermsAndConditionsDate.OptionsColumn.AllowFocus = false;
            this.colCurrentTermsAndConditionsDate.OptionsColumn.ReadOnly = true;
            this.colCurrentTermsAndConditionsDate.Visible = true;
            this.colCurrentTermsAndConditionsDate.VisibleIndex = 135;
            this.colCurrentTermsAndConditionsDate.Width = 100;
            // 
            // colDateOptedIn
            // 
            this.colDateOptedIn.Caption = "T & C Opted In";
            this.colDateOptedIn.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDateOptedIn.FieldName = "DateOptedIn";
            this.colDateOptedIn.Name = "colDateOptedIn";
            this.colDateOptedIn.OptionsColumn.AllowEdit = false;
            this.colDateOptedIn.OptionsColumn.AllowFocus = false;
            this.colDateOptedIn.OptionsColumn.ReadOnly = true;
            this.colDateOptedIn.Visible = true;
            this.colDateOptedIn.VisibleIndex = 136;
            this.colDateOptedIn.Width = 100;
            // 
            // colDateOptedOut
            // 
            this.colDateOptedOut.Caption = "T & C Opted Out";
            this.colDateOptedOut.ColumnEdit = this.repositoryItemTextEditDate;
            this.colDateOptedOut.FieldName = "DateOptedOut";
            this.colDateOptedOut.Name = "colDateOptedOut";
            this.colDateOptedOut.OptionsColumn.AllowEdit = false;
            this.colDateOptedOut.OptionsColumn.AllowFocus = false;
            this.colDateOptedOut.OptionsColumn.ReadOnly = true;
            this.colDateOptedOut.Visible = true;
            this.colDateOptedOut.VisibleIndex = 137;
            this.colDateOptedOut.Width = 100;
            // 
            // colOptOutFormIssued
            // 
            this.colOptOutFormIssued.Caption = "T & C Opt Out Form Issued";
            this.colOptOutFormIssued.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colOptOutFormIssued.FieldName = "OptOutFormIssued";
            this.colOptOutFormIssued.Name = "colOptOutFormIssued";
            this.colOptOutFormIssued.OptionsColumn.AllowEdit = false;
            this.colOptOutFormIssued.OptionsColumn.AllowFocus = false;
            this.colOptOutFormIssued.OptionsColumn.ReadOnly = true;
            this.colOptOutFormIssued.Visible = true;
            this.colOptOutFormIssued.VisibleIndex = 138;
            this.colOptOutFormIssued.Width = 149;
            // 
            // colOptInToSubsistence
            // 
            this.colOptInToSubsistence.Caption = "Opt In to Subsistence";
            this.colOptInToSubsistence.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colOptInToSubsistence.FieldName = "OptInToSubsistence";
            this.colOptInToSubsistence.Name = "colOptInToSubsistence";
            this.colOptInToSubsistence.OptionsColumn.AllowEdit = false;
            this.colOptInToSubsistence.OptionsColumn.AllowFocus = false;
            this.colOptInToSubsistence.OptionsColumn.ReadOnly = true;
            this.colOptInToSubsistence.Visible = true;
            this.colOptInToSubsistence.VisibleIndex = 139;
            this.colOptInToSubsistence.Width = 123;
            // 
            // colDISCoverEligible
            // 
            this.colDISCoverEligible.Caption = "DIS Cover Eligible";
            this.colDISCoverEligible.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colDISCoverEligible.FieldName = "DISCoverEligible";
            this.colDISCoverEligible.Name = "colDISCoverEligible";
            this.colDISCoverEligible.OptionsColumn.AllowEdit = false;
            this.colDISCoverEligible.OptionsColumn.AllowFocus = false;
            this.colDISCoverEligible.OptionsColumn.ReadOnly = true;
            this.colDISCoverEligible.Visible = true;
            this.colDISCoverEligible.VisibleIndex = 140;
            this.colDISCoverEligible.Width = 103;
            // 
            // colDISLevelCover
            // 
            this.colDISLevelCover.Caption = "DIS Level Cover";
            this.colDISLevelCover.FieldName = "DISLevelCover";
            this.colDISLevelCover.Name = "colDISLevelCover";
            this.colDISLevelCover.OptionsColumn.AllowEdit = false;
            this.colDISLevelCover.OptionsColumn.AllowFocus = false;
            this.colDISLevelCover.OptionsColumn.ReadOnly = true;
            this.colDISLevelCover.Visible = true;
            this.colDISLevelCover.VisibleIndex = 141;
            this.colDISLevelCover.Width = 110;
            // 
            // colDISScheme
            // 
            this.colDISScheme.Caption = "DIS Scheme";
            this.colDISScheme.FieldName = "DISScheme";
            this.colDISScheme.Name = "colDISScheme";
            this.colDISScheme.OptionsColumn.AllowEdit = false;
            this.colDISScheme.OptionsColumn.AllowFocus = false;
            this.colDISScheme.OptionsColumn.ReadOnly = true;
            this.colDISScheme.Visible = true;
            this.colDISScheme.VisibleIndex = 142;
            this.colDISScheme.Width = 110;
            // 
            // colIPEligible
            // 
            this.colIPEligible.Caption = "IP Eligible";
            this.colIPEligible.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colIPEligible.FieldName = "IPEligible";
            this.colIPEligible.Name = "colIPEligible";
            this.colIPEligible.OptionsColumn.AllowEdit = false;
            this.colIPEligible.OptionsColumn.AllowFocus = false;
            this.colIPEligible.OptionsColumn.ReadOnly = true;
            this.colIPEligible.Visible = true;
            this.colIPEligible.VisibleIndex = 143;
            // 
            // colIPLevel
            // 
            this.colIPLevel.Caption = "IP Level";
            this.colIPLevel.FieldName = "IPLevel";
            this.colIPLevel.Name = "colIPLevel";
            this.colIPLevel.OptionsColumn.AllowEdit = false;
            this.colIPLevel.OptionsColumn.AllowFocus = false;
            this.colIPLevel.OptionsColumn.ReadOnly = true;
            this.colIPLevel.Visible = true;
            this.colIPLevel.VisibleIndex = 144;
            this.colIPLevel.Width = 110;
            // 
            // colIPDeferralPeriod
            // 
            this.colIPDeferralPeriod.Caption = "IP Deferral Period";
            this.colIPDeferralPeriod.FieldName = "IPDeferralPeriod";
            this.colIPDeferralPeriod.Name = "colIPDeferralPeriod";
            this.colIPDeferralPeriod.OptionsColumn.AllowEdit = false;
            this.colIPDeferralPeriod.OptionsColumn.AllowFocus = false;
            this.colIPDeferralPeriod.OptionsColumn.ReadOnly = true;
            this.colIPDeferralPeriod.Visible = true;
            this.colIPDeferralPeriod.VisibleIndex = 145;
            this.colIPDeferralPeriod.Width = 110;
            // 
            // colIPScheme
            // 
            this.colIPScheme.Caption = "IP Scheme";
            this.colIPScheme.FieldName = "IPScheme";
            this.colIPScheme.Name = "colIPScheme";
            this.colIPScheme.OptionsColumn.AllowEdit = false;
            this.colIPScheme.OptionsColumn.AllowFocus = false;
            this.colIPScheme.OptionsColumn.ReadOnly = true;
            this.colIPScheme.Visible = true;
            this.colIPScheme.VisibleIndex = 146;
            this.colIPScheme.Width = 110;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // repositoryItemTextEditUnknown
            // 
            this.repositoryItemTextEditUnknown.AutoHeight = false;
            this.repositoryItemTextEditUnknown.Mask.EditMask = "#####0.00 ??";
            this.repositoryItemTextEditUnknown.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditUnknown.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditUnknown.Name = "repositoryItemTextEditUnknown";
            // 
            // repositoryItemTextEditInt
            // 
            this.repositoryItemTextEditInt.AutoHeight = false;
            this.repositoryItemTextEditInt.Mask.EditMask = "f0";
            this.repositoryItemTextEditInt.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInt.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInt.Name = "repositoryItemTextEditInt";
            // 
            // popupContainerControlAbsenceTypes
            // 
            this.popupContainerControlAbsenceTypes.Controls.Add(this.gridControl5);
            this.popupContainerControlAbsenceTypes.Controls.Add(this.btnAbsenceTypeFilterOK);
            this.popupContainerControlAbsenceTypes.Location = new System.Drawing.Point(531, 74);
            this.popupContainerControlAbsenceTypes.Name = "popupContainerControlAbsenceTypes";
            this.popupContainerControlAbsenceTypes.Size = new System.Drawing.Size(244, 179);
            this.popupContainerControlAbsenceTypes.TabIndex = 5;
            // 
            // gridControl5
            // 
            this.gridControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl5.DataSource = this.spHR00077AbsenceTypesListWithBlankBindingSource;
            this.gridControl5.Location = new System.Drawing.Point(3, 3);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.Size = new System.Drawing.Size(238, 148);
            this.gridControl5.TabIndex = 3;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // spHR00077AbsenceTypesListWithBlankBindingSource
            // 
            this.spHR00077AbsenceTypesListWithBlankBindingSource.DataMember = "sp_HR_00077_Absence_Types_List_With_Blank";
            this.spHR00077AbsenceTypesListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.colDescription,
            this.colOrder});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn3;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView5.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Absence Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 173;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // btnAbsenceTypeFilterOK
            // 
            this.btnAbsenceTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAbsenceTypeFilterOK.Location = new System.Drawing.Point(3, 153);
            this.btnAbsenceTypeFilterOK.Name = "btnAbsenceTypeFilterOK";
            this.btnAbsenceTypeFilterOK.Size = new System.Drawing.Size(39, 23);
            this.btnAbsenceTypeFilterOK.TabIndex = 2;
            this.btnAbsenceTypeFilterOK.Text = "OK";
            this.btnAbsenceTypeFilterOK.Click += new System.EventHandler(this.btnAbsenceTypeFilterOK_Click);
            // 
            // popupContainerControlEmployees
            // 
            this.popupContainerControlEmployees.Controls.Add(this.gridControl4);
            this.popupContainerControlEmployees.Controls.Add(this.btnEmployeeFilterOK);
            this.popupContainerControlEmployees.Location = new System.Drawing.Point(274, 81);
            this.popupContainerControlEmployees.Name = "popupContainerControlEmployees";
            this.popupContainerControlEmployees.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlEmployees.TabIndex = 7;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.spHR00115EmployeesByDeptFilterListBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(2, 2);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit15,
            this.repositoryItemMemoExEdit16});
            this.gridControl4.Size = new System.Drawing.Size(230, 141);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // spHR00115EmployeesByDeptFilterListBindingSource
            // 
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataMember = "sp_HR_00115_Employees_By_Dept_Filter_List";
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34,
            this.gridColumn35,
            this.gridColumn36});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn35;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn34, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn33, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // gridColumn30
            // 
            this.gridColumn30.Caption = "Employee ID";
            this.gridColumn30.FieldName = "EmployeeID";
            this.gridColumn30.Name = "gridColumn30";
            this.gridColumn30.OptionsColumn.AllowEdit = false;
            this.gridColumn30.OptionsColumn.AllowFocus = false;
            this.gridColumn30.OptionsColumn.ReadOnly = true;
            this.gridColumn30.Width = 81;
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Employee #";
            this.gridColumn31.FieldName = "EmployeeNumber";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.OptionsColumn.AllowEdit = false;
            this.gridColumn31.OptionsColumn.AllowFocus = false;
            this.gridColumn31.OptionsColumn.ReadOnly = true;
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 2;
            this.gridColumn31.Width = 91;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Title";
            this.gridColumn32.FieldName = "Title";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.OptionsColumn.AllowEdit = false;
            this.gridColumn32.OptionsColumn.AllowFocus = false;
            this.gridColumn32.OptionsColumn.ReadOnly = true;
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 3;
            this.gridColumn32.Width = 59;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Forename";
            this.gridColumn33.FieldName = "Firstname";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.OptionsColumn.AllowEdit = false;
            this.gridColumn33.OptionsColumn.AllowFocus = false;
            this.gridColumn33.OptionsColumn.ReadOnly = true;
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 1;
            this.gridColumn33.Width = 110;
            // 
            // gridColumn34
            // 
            this.gridColumn34.Caption = "Surname";
            this.gridColumn34.FieldName = "Surname";
            this.gridColumn34.Name = "gridColumn34";
            this.gridColumn34.OptionsColumn.AllowEdit = false;
            this.gridColumn34.OptionsColumn.AllowFocus = false;
            this.gridColumn34.OptionsColumn.ReadOnly = true;
            this.gridColumn34.Visible = true;
            this.gridColumn34.VisibleIndex = 0;
            this.gridColumn34.Width = 126;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "Remarks";
            this.gridColumn36.ColumnEdit = this.repositoryItemMemoExEdit16;
            this.gridColumn36.FieldName = "Remarks";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit16
            // 
            this.repositoryItemMemoExEdit16.AutoHeight = false;
            this.repositoryItemMemoExEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit16.Name = "repositoryItemMemoExEdit16";
            this.repositoryItemMemoExEdit16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit16.ShowIcon = false;
            // 
            // btnEmployeeFilterOK
            // 
            this.btnEmployeeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEmployeeFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnEmployeeFilterOK.Name = "btnEmployeeFilterOK";
            this.btnEmployeeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnEmployeeFilterOK.TabIndex = 2;
            this.btnEmployeeFilterOK.Text = "OK";
            this.btnEmployeeFilterOK.Click += new System.EventHandler(this.btnEmployeeFilterOK_Click);
            // 
            // popupContainerControlDepartments
            // 
            this.popupContainerControlDepartments.Controls.Add(this.gridControl2);
            this.popupContainerControlDepartments.Controls.Add(this.btnDepartmentFilterOK);
            this.popupContainerControlDepartments.Location = new System.Drawing.Point(23, 81);
            this.popupContainerControlDepartments.Name = "popupContainerControlDepartments";
            this.popupContainerControlDepartments.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlDepartments.TabIndex = 8;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.spHR00114DepartmentFilterListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit17});
            this.gridControl2.Size = new System.Drawing.Size(230, 141);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00114DepartmentFilterListBindingSource
            // 
            this.spHR00114DepartmentFilterListBindingSource.DataMember = "sp_HR_00114_Department_Filter_List";
            this.spHR00114DepartmentFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepartmentID,
            this.colBusinessAreaID1,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn39, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn37, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.Caption = "Department ID";
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.Width = 92;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.Width = 102;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Department Name";
            this.gridColumn37.FieldName = "DepartmentName";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.OptionsColumn.AllowEdit = false;
            this.gridColumn37.OptionsColumn.AllowFocus = false;
            this.gridColumn37.OptionsColumn.ReadOnly = true;
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 0;
            this.gridColumn37.Width = 224;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Remarks";
            this.gridColumn38.ColumnEdit = this.repositoryItemMemoExEdit17;
            this.gridColumn38.FieldName = "Remarks";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.OptionsColumn.ReadOnly = true;
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 1;
            this.gridColumn38.Width = 175;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Business Area Name";
            this.gridColumn39.FieldName = "BusinessAreaName";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.OptionsColumn.AllowEdit = false;
            this.gridColumn39.OptionsColumn.AllowFocus = false;
            this.gridColumn39.OptionsColumn.ReadOnly = true;
            this.gridColumn39.Width = 224;
            // 
            // btnDepartmentFilterOK
            // 
            this.btnDepartmentFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDepartmentFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnDepartmentFilterOK.Name = "btnDepartmentFilterOK";
            this.btnDepartmentFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDepartmentFilterOK.TabIndex = 2;
            this.btnDepartmentFilterOK.Text = "OK";
            this.btnDepartmentFilterOK.Click += new System.EventHandler(this.btnDepartmentFilterOK_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageAbsences;
            this.xtraTabControl1.Size = new System.Drawing.Size(1194, 176);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageVetting,
            this.xtraTabPageReferences,
            this.xtraTabPageAddresses,
            this.xtraTabPageNextOfKin,
            this.xtraTabPageDeptsWorked,
            this.xtraTabPageWorkPatterns,
            this.xtraTabPageAbsences,
            this.xtraTabPageShares,
            this.xtraTabPageBonuses,
            this.xtraTabPagePayRises,
            this.xtraTabPagePensions,
            this.xtraTabPageSubsistence,
            this.xtraTabPageTraining,
            this.xtraTabPageDiscipline,
            this.xtraTabPageCRM,
            this.xtraTabPageAdminister});
            // 
            // xtraTabPageAbsences
            // 
            this.xtraTabPageAbsences.Controls.Add(this.splitContainerControl4);
            this.xtraTabPageAbsences.Controls.Add(this.standaloneBarDockControl1);
            this.xtraTabPageAbsences.Name = "xtraTabPageAbsences";
            this.xtraTabPageAbsences.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageAbsences.Text = "Holidays && Absence";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl4.Location = new System.Drawing.Point(3, 25);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.gridSplitContainer11);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.gridSplitContainer12);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(1186, 122);
            this.splitContainerControl4.SplitterPosition = 473;
            this.splitContainerControl4.TabIndex = 2;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridSplitContainer11
            // 
            this.gridSplitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer11.Grid = this.gridControlHolidayYear;
            this.gridSplitContainer11.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer11.Name = "gridSplitContainer11";
            this.gridSplitContainer11.Panel1.Controls.Add(this.gridControlHolidayYear);
            this.gridSplitContainer11.Size = new System.Drawing.Size(473, 122);
            this.gridSplitContainer11.TabIndex = 0;
            // 
            // gridControlHolidayYear
            // 
            this.gridControlHolidayYear.DataSource = this.spHR00158HolidayYearsForEmployeeBindingSource;
            this.gridControlHolidayYear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlHolidayYear.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlHolidayYear.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view")});
            this.gridControlHolidayYear.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlHolidayYear_EmbeddedNavigator_ButtonClick);
            this.gridControlHolidayYear.Location = new System.Drawing.Point(0, 0);
            this.gridControlHolidayYear.MainView = this.gridViewHolidayYear;
            this.gridControlHolidayYear.MenuManager = this.barManager1;
            this.gridControlHolidayYear.Name = "gridControlHolidayYear";
            this.gridControlHolidayYear.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11,
            this.repositoryItemCheckEdit11,
            this.repositoryItemTextEditDateTime5,
            this.repositoryItemTextEditNumeric2DPNoDescriptor,
            this.repositoryItemTextEditNumericHours,
            this.repositoryItemTextEditNumericDays,
            this.repositoryItemTextEditNumericUnknown});
            this.gridControlHolidayYear.Size = new System.Drawing.Size(473, 122);
            this.gridControlHolidayYear.TabIndex = 2;
            this.gridControlHolidayYear.UseEmbeddedNavigator = true;
            this.gridControlHolidayYear.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHolidayYear});
            // 
            // spHR00158HolidayYearsForEmployeeBindingSource
            // 
            this.spHR00158HolidayYearsForEmployeeBindingSource.DataMember = "sp_HR_00158_Holiday_Years_For_Employee";
            this.spHR00158HolidayYearsForEmployeeBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewHolidayYear
            // 
            this.gridViewHolidayYear.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn5,
            this.colEmployeeContractID,
            this.colHolidayYearStartDate,
            this.colHolidayYearEndDate,
            this.colActualStartDate,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.colMustTakeCertainHolidays,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.colTotalHolidaysTaken,
            this.gridColumn29});
            styleFormatCondition4.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition4.Appearance.Options.UseForeColor = true;
            styleFormatCondition4.ApplyToRow = true;
            styleFormatCondition4.Column = this.gridColumn20;
            styleFormatCondition4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition4.Value1 = 0;
            this.gridViewHolidayYear.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition4});
            this.gridViewHolidayYear.GridControl = this.gridControlHolidayYear;
            this.gridViewHolidayYear.GroupCount = 1;
            this.gridViewHolidayYear.Name = "gridViewHolidayYear";
            this.gridViewHolidayYear.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewHolidayYear.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewHolidayYear.OptionsLayout.StoreAppearance = true;
            this.gridViewHolidayYear.OptionsLayout.StoreFormatRules = true;
            this.gridViewHolidayYear.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewHolidayYear.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewHolidayYear.OptionsSelection.MultiSelect = true;
            this.gridViewHolidayYear.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewHolidayYear.OptionsView.ColumnAutoWidth = false;
            this.gridViewHolidayYear.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewHolidayYear.OptionsView.ShowGroupPanel = false;
            this.gridViewHolidayYear.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn16, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn20, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHolidayYearStartDate, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHolidayYearEndDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewHolidayYear.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewHolidayYear_CustomDrawCell);
            this.gridViewHolidayYear.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewHolidayYear_CustomRowCellEdit);
            this.gridViewHolidayYear.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewHolidayYear.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewHolidayYear.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewHolidayYear.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewHolidayYear.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewHolidayYear.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewHolidayYear.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewHolidayYear_MouseUp);
            this.gridViewHolidayYear.DoubleClick += new System.EventHandler(this.gridViewHolidayYear_DoubleClick);
            this.gridViewHolidayYear.GotFocus += new System.EventHandler(this.gridViewHolidayYear_GotFocus);
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Holiday Year ID";
            this.gridColumn5.FieldName = "EmployeeHolidayYearID";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 95;
            // 
            // colEmployeeContractID
            // 
            this.colEmployeeContractID.Caption = "Employee ID";
            this.colEmployeeContractID.FieldName = "EmployeeID";
            this.colEmployeeContractID.Name = "colEmployeeContractID";
            this.colEmployeeContractID.OptionsColumn.AllowEdit = false;
            this.colEmployeeContractID.OptionsColumn.AllowFocus = false;
            this.colEmployeeContractID.OptionsColumn.ReadOnly = true;
            this.colEmployeeContractID.Width = 77;
            // 
            // colHolidayYearStartDate
            // 
            this.colHolidayYearStartDate.Caption = "Year Start Date";
            this.colHolidayYearStartDate.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colHolidayYearStartDate.FieldName = "HolidayYearStartDate";
            this.colHolidayYearStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHolidayYearStartDate.Name = "colHolidayYearStartDate";
            this.colHolidayYearStartDate.OptionsColumn.AllowEdit = false;
            this.colHolidayYearStartDate.OptionsColumn.AllowFocus = false;
            this.colHolidayYearStartDate.OptionsColumn.ReadOnly = true;
            this.colHolidayYearStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHolidayYearStartDate.Visible = true;
            this.colHolidayYearStartDate.VisibleIndex = 1;
            this.colHolidayYearStartDate.Width = 109;
            // 
            // repositoryItemTextEditDateTime5
            // 
            this.repositoryItemTextEditDateTime5.AutoHeight = false;
            this.repositoryItemTextEditDateTime5.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime5.Name = "repositoryItemTextEditDateTime5";
            // 
            // colHolidayYearEndDate
            // 
            this.colHolidayYearEndDate.Caption = "Year End Date";
            this.colHolidayYearEndDate.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colHolidayYearEndDate.FieldName = "HolidayYearEndDate";
            this.colHolidayYearEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHolidayYearEndDate.Name = "colHolidayYearEndDate";
            this.colHolidayYearEndDate.OptionsColumn.AllowEdit = false;
            this.colHolidayYearEndDate.OptionsColumn.AllowFocus = false;
            this.colHolidayYearEndDate.OptionsColumn.ReadOnly = true;
            this.colHolidayYearEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHolidayYearEndDate.Visible = true;
            this.colHolidayYearEndDate.VisibleIndex = 2;
            this.colHolidayYearEndDate.Width = 103;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start Date";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 3;
            this.colActualStartDate.Width = 104;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Holiday Entitlement";
            this.gridColumn6.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn6.FieldName = "BasicHolidayEntitlement";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 4;
            this.gridColumn6.Width = 113;
            // 
            // repositoryItemTextEditNumeric2DPNoDescriptor
            // 
            this.repositoryItemTextEditNumeric2DPNoDescriptor.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DPNoDescriptor.Mask.EditMask = "#####0.00";
            this.repositoryItemTextEditNumeric2DPNoDescriptor.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DPNoDescriptor.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DPNoDescriptor.Name = "repositoryItemTextEditNumeric2DPNoDescriptor";
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Bank Holiday Entitlement";
            this.gridColumn7.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn7.FieldName = "BankHolidayEntitlement";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 5;
            this.gridColumn7.Width = 139;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Purchased Holiday Entitlement";
            this.gridColumn11.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn11.FieldName = "PurchasedHolidayEntitlement";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 6;
            this.gridColumn11.Width = 166;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Holiday Unit Descriptor ID";
            this.gridColumn12.FieldName = "HolidayUnitDescriptorID";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            this.gridColumn12.Width = 144;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Hours Per Holiday Day";
            this.gridColumn13.ColumnEdit = this.repositoryItemTextEditNumericHours;
            this.gridColumn13.FieldName = "HoursPerHolidayDay";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 16;
            this.gridColumn13.Width = 128;
            // 
            // repositoryItemTextEditNumericHours
            // 
            this.repositoryItemTextEditNumericHours.AutoHeight = false;
            this.repositoryItemTextEditNumericHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditNumericHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericHours.Name = "repositoryItemTextEditNumericHours";
            // 
            // colMustTakeCertainHolidays
            // 
            this.colMustTakeCertainHolidays.Caption = "Must Take Default Holidays";
            this.colMustTakeCertainHolidays.FieldName = "MustTakeCertainHolidays";
            this.colMustTakeCertainHolidays.Name = "colMustTakeCertainHolidays";
            this.colMustTakeCertainHolidays.OptionsColumn.AllowEdit = false;
            this.colMustTakeCertainHolidays.OptionsColumn.AllowFocus = false;
            this.colMustTakeCertainHolidays.OptionsColumn.ReadOnly = true;
            this.colMustTakeCertainHolidays.Visible = true;
            this.colMustTakeCertainHolidays.VisibleIndex = 17;
            this.colMustTakeCertainHolidays.Width = 151;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Remarks";
            this.gridColumn14.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.gridColumn14.FieldName = "Remarks";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 18;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Holiday Year Description";
            this.gridColumn15.FieldName = "HolidayYearDescription";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 137;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Linked To Employee";
            this.gridColumn16.FieldName = "EmployeeSurnameForename";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.AllowEdit = false;
            this.gridColumn16.OptionsColumn.AllowFocus = false;
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 16;
            this.gridColumn16.Width = 115;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Employee Surname";
            this.gridColumn17.FieldName = "EmployeeSurname";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsColumn.AllowEdit = false;
            this.gridColumn17.OptionsColumn.AllowFocus = false;
            this.gridColumn17.OptionsColumn.ReadOnly = true;
            this.gridColumn17.Width = 125;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Employee Forename";
            this.gridColumn18.FieldName = "EmployeeForename";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 131;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Employee #";
            this.gridColumn19.FieldName = "EmployeeNumber";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 78;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Holiday Unit Descriptor";
            this.gridColumn21.FieldName = "HolidayUnitDescriptor";
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Width = 130;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Holidays Used";
            this.gridColumn22.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn22.FieldName = "HolidaysTaken";
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 8;
            this.gridColumn22.Width = 88;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Bank Holidays Used";
            this.gridColumn23.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn23.FieldName = "BankHolidaysTaken";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 9;
            this.gridColumn23.Width = 114;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Authorised Holidays Used";
            this.gridColumn24.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn24.FieldName = "AbsenceAuthorisedTaken";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.AllowEdit = false;
            this.gridColumn24.OptionsColumn.AllowFocus = false;
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 10;
            this.gridColumn24.Width = 143;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Unauthorised Holidays Used";
            this.gridColumn25.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn25.FieldName = "AbsenceUnauthorisedTaken";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 11;
            this.gridColumn25.Width = 155;
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Total Holiday Entitlement";
            this.gridColumn26.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn26.FieldName = "TotalHolidayEntitlement";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.OptionsColumn.AllowEdit = false;
            this.gridColumn26.OptionsColumn.AllowFocus = false;
            this.gridColumn26.OptionsColumn.ReadOnly = true;
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 7;
            this.gridColumn26.Width = 140;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Holidays Left";
            this.gridColumn27.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn27.FieldName = "HolidaysRemaining";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.OptionsColumn.AllowEdit = false;
            this.gridColumn27.OptionsColumn.AllowFocus = false;
            this.gridColumn27.OptionsColumn.ReadOnly = true;
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 13;
            this.gridColumn27.Width = 83;
            // 
            // gridColumn28
            // 
            this.gridColumn28.Caption = "Bank Holidays Left";
            this.gridColumn28.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn28.FieldName = "BankHolidaysRemaining";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.OptionsColumn.AllowEdit = false;
            this.gridColumn28.OptionsColumn.AllowFocus = false;
            this.gridColumn28.OptionsColumn.ReadOnly = true;
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 14;
            this.gridColumn28.Width = 109;
            // 
            // colTotalHolidaysTaken
            // 
            this.colTotalHolidaysTaken.Caption = "Total Holidays Used";
            this.colTotalHolidaysTaken.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.colTotalHolidaysTaken.FieldName = "TotalHolidaysTaken";
            this.colTotalHolidaysTaken.Name = "colTotalHolidaysTaken";
            this.colTotalHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colTotalHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colTotalHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colTotalHolidaysTaken.Visible = true;
            this.colTotalHolidaysTaken.VisibleIndex = 12;
            this.colTotalHolidaysTaken.Width = 115;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Total Holidays Left";
            this.gridColumn29.ColumnEdit = this.repositoryItemTextEditNumeric2DPNoDescriptor;
            this.gridColumn29.FieldName = "TotalHolidaysRemaining";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.OptionsColumn.AllowEdit = false;
            this.gridColumn29.OptionsColumn.AllowFocus = false;
            this.gridColumn29.OptionsColumn.ReadOnly = true;
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 15;
            this.gridColumn29.Width = 110;
            // 
            // repositoryItemTextEditNumericDays
            // 
            this.repositoryItemTextEditNumericDays.AutoHeight = false;
            this.repositoryItemTextEditNumericDays.Mask.EditMask = "#####0.00 Days";
            this.repositoryItemTextEditNumericDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericDays.Name = "repositoryItemTextEditNumericDays";
            // 
            // repositoryItemTextEditNumericUnknown
            // 
            this.repositoryItemTextEditNumericUnknown.AutoHeight = false;
            this.repositoryItemTextEditNumericUnknown.Mask.EditMask = "#####0.00 ???";
            this.repositoryItemTextEditNumericUnknown.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericUnknown.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericUnknown.Name = "repositoryItemTextEditNumericUnknown";
            // 
            // gridSplitContainer12
            // 
            this.gridSplitContainer12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer12.Grid = this.gridControlAbsences;
            this.gridSplitContainer12.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer12.Name = "gridSplitContainer12";
            this.gridSplitContainer12.Panel1.Controls.Add(this.gridControlAbsences);
            this.gridSplitContainer12.Size = new System.Drawing.Size(707, 122);
            this.gridSplitContainer12.TabIndex = 0;
            // 
            // gridControlAbsences
            // 
            this.gridControlAbsences.DataSource = this.sp_HR_00037_Get_Employee_AbsencesBindingSource;
            this.gridControlAbsences.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlAbsences.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlAbsences.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlAbsences.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAbsences_EmbeddedNavigator_ButtonClick);
            this.gridControlAbsences.Location = new System.Drawing.Point(0, 0);
            this.gridControlAbsences.MainView = this.gridViewAbsences;
            this.gridControlAbsences.MenuManager = this.barManager1;
            this.gridControlAbsences.Name = "gridControlAbsences";
            this.gridControlAbsences.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemCheckEdit3,
            this.repositoryItemTextEditShortDate2,
            this.repositoryItemTextEditLongDate2,
            this.repositoryItemTextEditNumeric2DP2,
            this.repositoryItemTextEdit1,
            this.repositoryItemHyperLinkEditLinkedDocumentsAbsences});
            this.gridControlAbsences.Size = new System.Drawing.Size(707, 122);
            this.gridControlAbsences.TabIndex = 1;
            this.gridControlAbsences.UseEmbeddedNavigator = true;
            this.gridControlAbsences.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAbsences});
            // 
            // sp_HR_00037_Get_Employee_AbsencesBindingSource
            // 
            this.sp_HR_00037_Get_Employee_AbsencesBindingSource.DataMember = "sp_HR_00037_Get_Employee_Absences";
            this.sp_HR_00037_Get_Employee_AbsencesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewAbsences
            // 
            this.gridViewAbsences.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAbsenceID,
            this.colEmployeeHolidayYearId,
            this.colRecordTypeId,
            this.colAbsenceTypeId,
            this.colAbsenceSubTypeId,
            this.colStartDate,
            this.colEndDate,
            this.colUnitsUsed,
            this.colHolidayUnitDescriptor1,
            this.colApprovedDate,
            this.colCancelledDate,
            this.colDeclaredDisability,
            this.colPayCategoryId,
            this.colIsWorkRelated,
            this.colComments4,
            this.colRecordType,
            this.colAbsenceType,
            this.colStatus,
            this.colHolidayYearDescription,
            this.colEmployeeSurnameForename1,
            this.colEmployeeSurname1,
            this.colEmployeeForename1,
            this.colEmployeeNumber3,
            this.colApprovedByPerson,
            this.colCancelledByPerson,
            this.colAbsenceSubType,
            this.colActiveHolidayYear,
            this.colEmployeeID8,
            this.colEWCDate,
            this.colExpectedReturnDate,
            this.colHalfDayEnd,
            this.colHalfDayStart,
            this.colAdditionalDaysPaid,
            this.colLinkedDocumentCount6,
            this.colUnitsPaidCSP,
            this.colUnitsPaidHalfPay,
            this.colUnitsPaidOther,
            this.colUnitsPaidUnpaid,
            this.colAbsenceComment,
            this.colCreatedFromWeb});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colCreatedFromWeb;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            formatConditionRuleValue1.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridViewAbsences.FormatRules.Add(gridFormatRule1);
            this.gridViewAbsences.GridControl = this.gridControlAbsences;
            this.gridViewAbsences.GroupCount = 2;
            this.gridViewAbsences.Name = "gridViewAbsences";
            this.gridViewAbsences.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewAbsences.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewAbsences.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewAbsences.OptionsLayout.StoreAppearance = true;
            this.gridViewAbsences.OptionsLayout.StoreFormatRules = true;
            this.gridViewAbsences.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewAbsences.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewAbsences.OptionsSelection.MultiSelect = true;
            this.gridViewAbsences.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewAbsences.OptionsView.BestFitMaxRowCount = 10;
            this.gridViewAbsences.OptionsView.ColumnAutoWidth = false;
            this.gridViewAbsences.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewAbsences.OptionsView.ShowGroupPanel = false;
            this.gridViewAbsences.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurnameForename1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHolidayYearDescription, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewAbsences.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewAbsences_CustomDrawCell);
            this.gridViewAbsences.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewAbsences_CustomRowCellEdit);
            this.gridViewAbsences.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewAbsences.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewAbsences.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewAbsences.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewAbsences_ShowingEditor);
            this.gridViewAbsences.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewAbsences.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewAbsences.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewAbsences.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewAbsences_MouseUp);
            this.gridViewAbsences.DoubleClick += new System.EventHandler(this.gridViewAbsences_DoubleClick);
            this.gridViewAbsences.GotFocus += new System.EventHandler(this.gridViewAbsences_GotFocus);
            // 
            // colAbsenceID
            // 
            this.colAbsenceID.Caption = "Absence ID";
            this.colAbsenceID.FieldName = "AbsenceID";
            this.colAbsenceID.Name = "colAbsenceID";
            this.colAbsenceID.OptionsColumn.AllowEdit = false;
            this.colAbsenceID.OptionsColumn.AllowFocus = false;
            this.colAbsenceID.OptionsColumn.ReadOnly = true;
            this.colAbsenceID.Width = 76;
            // 
            // colEmployeeHolidayYearId
            // 
            this.colEmployeeHolidayYearId.Caption = "Holiday Year ID";
            this.colEmployeeHolidayYearId.FieldName = "EmployeeHolidayYearId";
            this.colEmployeeHolidayYearId.Name = "colEmployeeHolidayYearId";
            this.colEmployeeHolidayYearId.OptionsColumn.AllowEdit = false;
            this.colEmployeeHolidayYearId.OptionsColumn.AllowFocus = false;
            this.colEmployeeHolidayYearId.OptionsColumn.ReadOnly = true;
            this.colEmployeeHolidayYearId.Width = 95;
            // 
            // colRecordTypeId
            // 
            this.colRecordTypeId.Caption = "Record Type ID";
            this.colRecordTypeId.FieldName = "RecordTypeId";
            this.colRecordTypeId.Name = "colRecordTypeId";
            this.colRecordTypeId.OptionsColumn.AllowEdit = false;
            this.colRecordTypeId.OptionsColumn.AllowFocus = false;
            this.colRecordTypeId.OptionsColumn.ReadOnly = true;
            this.colRecordTypeId.Width = 96;
            // 
            // colAbsenceTypeId
            // 
            this.colAbsenceTypeId.Caption = "Absence Type ID";
            this.colAbsenceTypeId.FieldName = "AbsenceTypeId";
            this.colAbsenceTypeId.Name = "colAbsenceTypeId";
            this.colAbsenceTypeId.OptionsColumn.AllowEdit = false;
            this.colAbsenceTypeId.OptionsColumn.AllowFocus = false;
            this.colAbsenceTypeId.OptionsColumn.ReadOnly = true;
            this.colAbsenceTypeId.Width = 103;
            // 
            // colAbsenceSubTypeId
            // 
            this.colAbsenceSubTypeId.Caption = "Absence Sub-Type ID";
            this.colAbsenceSubTypeId.FieldName = "AbsenceSubTypeId";
            this.colAbsenceSubTypeId.Name = "colAbsenceSubTypeId";
            this.colAbsenceSubTypeId.OptionsColumn.AllowEdit = false;
            this.colAbsenceSubTypeId.OptionsColumn.AllowFocus = false;
            this.colAbsenceSubTypeId.OptionsColumn.ReadOnly = true;
            this.colAbsenceSubTypeId.Width = 125;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditShortDate2;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 0;
            this.colStartDate.Width = 110;
            // 
            // repositoryItemTextEditShortDate2
            // 
            this.repositoryItemTextEditShortDate2.AutoHeight = false;
            this.repositoryItemTextEditShortDate2.Mask.EditMask = "d";
            this.repositoryItemTextEditShortDate2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditShortDate2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShortDate2.Name = "repositoryItemTextEditShortDate2";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditShortDate2;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 1;
            this.colEndDate.Width = 88;
            // 
            // colUnitsUsed
            // 
            this.colUnitsUsed.Caption = "Units Used";
            this.colUnitsUsed.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colUnitsUsed.FieldName = "UnitsUsed";
            this.colUnitsUsed.Name = "colUnitsUsed";
            this.colUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colUnitsUsed.Visible = true;
            this.colUnitsUsed.VisibleIndex = 6;
            // 
            // repositoryItemTextEditNumeric2DP2
            // 
            this.repositoryItemTextEditNumeric2DP2.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP2.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP2.Name = "repositoryItemTextEditNumeric2DP2";
            // 
            // colHolidayUnitDescriptor1
            // 
            this.colHolidayUnitDescriptor1.Caption = "Unit Descriptor";
            this.colHolidayUnitDescriptor1.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor1.Name = "colHolidayUnitDescriptor1";
            this.colHolidayUnitDescriptor1.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor1.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor1.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor1.Visible = true;
            this.colHolidayUnitDescriptor1.VisibleIndex = 7;
            this.colHolidayUnitDescriptor1.Width = 92;
            // 
            // colApprovedDate
            // 
            this.colApprovedDate.Caption = "Approved Date";
            this.colApprovedDate.ColumnEdit = this.repositoryItemTextEditLongDate2;
            this.colApprovedDate.FieldName = "ApprovedDate";
            this.colApprovedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colApprovedDate.Name = "colApprovedDate";
            this.colApprovedDate.OptionsColumn.AllowEdit = false;
            this.colApprovedDate.OptionsColumn.AllowFocus = false;
            this.colApprovedDate.OptionsColumn.ReadOnly = true;
            this.colApprovedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colApprovedDate.Visible = true;
            this.colApprovedDate.VisibleIndex = 18;
            this.colApprovedDate.Width = 94;
            // 
            // repositoryItemTextEditLongDate2
            // 
            this.repositoryItemTextEditLongDate2.AutoHeight = false;
            this.repositoryItemTextEditLongDate2.Mask.EditMask = "g";
            this.repositoryItemTextEditLongDate2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditLongDate2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLongDate2.Name = "repositoryItemTextEditLongDate2";
            // 
            // colCancelledDate
            // 
            this.colCancelledDate.Caption = "Cancelled Date";
            this.colCancelledDate.ColumnEdit = this.repositoryItemTextEditLongDate2;
            this.colCancelledDate.FieldName = "CancelledDate";
            this.colCancelledDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCancelledDate.Name = "colCancelledDate";
            this.colCancelledDate.OptionsColumn.AllowEdit = false;
            this.colCancelledDate.OptionsColumn.AllowFocus = false;
            this.colCancelledDate.OptionsColumn.ReadOnly = true;
            this.colCancelledDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCancelledDate.Visible = true;
            this.colCancelledDate.VisibleIndex = 20;
            this.colCancelledDate.Width = 93;
            // 
            // colDeclaredDisability
            // 
            this.colDeclaredDisability.Caption = "Declared Disability";
            this.colDeclaredDisability.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colDeclaredDisability.FieldName = "DeclaredDisability";
            this.colDeclaredDisability.Name = "colDeclaredDisability";
            this.colDeclaredDisability.OptionsColumn.AllowEdit = false;
            this.colDeclaredDisability.OptionsColumn.AllowFocus = false;
            this.colDeclaredDisability.OptionsColumn.ReadOnly = true;
            this.colDeclaredDisability.Visible = true;
            this.colDeclaredDisability.VisibleIndex = 23;
            this.colDeclaredDisability.Width = 108;
            // 
            // colPayCategoryId
            // 
            this.colPayCategoryId.Caption = "Pay Category ID";
            this.colPayCategoryId.FieldName = "PayCategoryId";
            this.colPayCategoryId.Name = "colPayCategoryId";
            this.colPayCategoryId.OptionsColumn.AllowEdit = false;
            this.colPayCategoryId.OptionsColumn.AllowFocus = false;
            this.colPayCategoryId.OptionsColumn.ReadOnly = true;
            this.colPayCategoryId.Width = 101;
            // 
            // colIsWorkRelated
            // 
            this.colIsWorkRelated.Caption = "Work Related";
            this.colIsWorkRelated.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colIsWorkRelated.FieldName = "IsWorkRelated";
            this.colIsWorkRelated.Name = "colIsWorkRelated";
            this.colIsWorkRelated.OptionsColumn.AllowEdit = false;
            this.colIsWorkRelated.OptionsColumn.AllowFocus = false;
            this.colIsWorkRelated.OptionsColumn.ReadOnly = true;
            this.colIsWorkRelated.Visible = true;
            this.colIsWorkRelated.VisibleIndex = 22;
            this.colIsWorkRelated.Width = 86;
            // 
            // colComments4
            // 
            this.colComments4.Caption = "Remarks";
            this.colComments4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colComments4.FieldName = "Comments";
            this.colComments4.Name = "colComments4";
            this.colComments4.OptionsColumn.ReadOnly = true;
            this.colComments4.Visible = true;
            this.colComments4.VisibleIndex = 24;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Visible = true;
            this.colRecordType.VisibleIndex = 3;
            this.colRecordType.Width = 94;
            // 
            // colAbsenceType
            // 
            this.colAbsenceType.Caption = "Absence Type";
            this.colAbsenceType.FieldName = "AbsenceType";
            this.colAbsenceType.Name = "colAbsenceType";
            this.colAbsenceType.OptionsColumn.AllowEdit = false;
            this.colAbsenceType.OptionsColumn.AllowFocus = false;
            this.colAbsenceType.OptionsColumn.ReadOnly = true;
            this.colAbsenceType.Visible = true;
            this.colAbsenceType.VisibleIndex = 4;
            this.colAbsenceType.Width = 122;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 8;
            // 
            // colHolidayYearDescription
            // 
            this.colHolidayYearDescription.Caption = "Holiday Year";
            this.colHolidayYearDescription.FieldName = "HolidayYearDescription";
            this.colHolidayYearDescription.Name = "colHolidayYearDescription";
            this.colHolidayYearDescription.OptionsColumn.AllowEdit = false;
            this.colHolidayYearDescription.OptionsColumn.AllowFocus = false;
            this.colHolidayYearDescription.OptionsColumn.ReadOnly = true;
            this.colHolidayYearDescription.Visible = true;
            this.colHolidayYearDescription.VisibleIndex = 11;
            this.colHolidayYearDescription.Width = 94;
            // 
            // colEmployeeSurnameForename1
            // 
            this.colEmployeeSurnameForename1.Caption = "Surname: Forename";
            this.colEmployeeSurnameForename1.FieldName = "EmployeeSurnameForename";
            this.colEmployeeSurnameForename1.Name = "colEmployeeSurnameForename1";
            this.colEmployeeSurnameForename1.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurnameForename1.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurnameForename1.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurnameForename1.Visible = true;
            this.colEmployeeSurnameForename1.VisibleIndex = 22;
            this.colEmployeeSurnameForename1.Width = 131;
            // 
            // colEmployeeSurname1
            // 
            this.colEmployeeSurname1.Caption = "Surname";
            this.colEmployeeSurname1.FieldName = "EmployeeSurname";
            this.colEmployeeSurname1.Name = "colEmployeeSurname1";
            this.colEmployeeSurname1.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname1.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname1.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeForename1
            // 
            this.colEmployeeForename1.Caption = "Forename";
            this.colEmployeeForename1.FieldName = "EmployeeForename";
            this.colEmployeeForename1.Name = "colEmployeeForename1";
            this.colEmployeeForename1.OptionsColumn.AllowEdit = false;
            this.colEmployeeForename1.OptionsColumn.AllowFocus = false;
            this.colEmployeeForename1.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeNumber3
            // 
            this.colEmployeeNumber3.Caption = "Employee #";
            this.colEmployeeNumber3.FieldName = "EmployeeNumber";
            this.colEmployeeNumber3.Name = "colEmployeeNumber3";
            this.colEmployeeNumber3.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber3.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber3.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber3.Width = 78;
            // 
            // colApprovedByPerson
            // 
            this.colApprovedByPerson.Caption = "Approved By";
            this.colApprovedByPerson.FieldName = "ApprovedByPerson";
            this.colApprovedByPerson.Name = "colApprovedByPerson";
            this.colApprovedByPerson.OptionsColumn.AllowEdit = false;
            this.colApprovedByPerson.OptionsColumn.AllowFocus = false;
            this.colApprovedByPerson.OptionsColumn.ReadOnly = true;
            this.colApprovedByPerson.Visible = true;
            this.colApprovedByPerson.VisibleIndex = 19;
            this.colApprovedByPerson.Width = 83;
            // 
            // colCancelledByPerson
            // 
            this.colCancelledByPerson.Caption = "Cancelled By";
            this.colCancelledByPerson.FieldName = "CancelledByPerson";
            this.colCancelledByPerson.Name = "colCancelledByPerson";
            this.colCancelledByPerson.OptionsColumn.AllowEdit = false;
            this.colCancelledByPerson.OptionsColumn.AllowFocus = false;
            this.colCancelledByPerson.OptionsColumn.ReadOnly = true;
            this.colCancelledByPerson.Visible = true;
            this.colCancelledByPerson.VisibleIndex = 21;
            this.colCancelledByPerson.Width = 82;
            // 
            // colAbsenceSubType
            // 
            this.colAbsenceSubType.Caption = "Sub-Type";
            this.colAbsenceSubType.FieldName = "AbsenceSubType";
            this.colAbsenceSubType.Name = "colAbsenceSubType";
            this.colAbsenceSubType.OptionsColumn.AllowEdit = false;
            this.colAbsenceSubType.OptionsColumn.AllowFocus = false;
            this.colAbsenceSubType.OptionsColumn.ReadOnly = true;
            this.colAbsenceSubType.Visible = true;
            this.colAbsenceSubType.VisibleIndex = 5;
            this.colAbsenceSubType.Width = 116;
            // 
            // colActiveHolidayYear
            // 
            this.colActiveHolidayYear.Caption = "Active Holiday Year";
            this.colActiveHolidayYear.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colActiveHolidayYear.FieldName = "ActiveHolidayYear";
            this.colActiveHolidayYear.Name = "colActiveHolidayYear";
            this.colActiveHolidayYear.OptionsColumn.AllowEdit = false;
            this.colActiveHolidayYear.OptionsColumn.AllowFocus = false;
            this.colActiveHolidayYear.OptionsColumn.ReadOnly = true;
            this.colActiveHolidayYear.Width = 114;
            // 
            // colEmployeeID8
            // 
            this.colEmployeeID8.Caption = "EmployeeID";
            this.colEmployeeID8.FieldName = "EmployeeID";
            this.colEmployeeID8.Name = "colEmployeeID8";
            this.colEmployeeID8.OptionsColumn.AllowEdit = false;
            this.colEmployeeID8.OptionsColumn.AllowFocus = false;
            this.colEmployeeID8.OptionsColumn.ReadOnly = true;
            // 
            // colEWCDate
            // 
            this.colEWCDate.Caption = "EWC Date";
            this.colEWCDate.ColumnEdit = this.repositoryItemTextEditShortDate2;
            this.colEWCDate.FieldName = "EWCDate";
            this.colEWCDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEWCDate.Name = "colEWCDate";
            this.colEWCDate.OptionsColumn.AllowEdit = false;
            this.colEWCDate.OptionsColumn.AllowFocus = false;
            this.colEWCDate.OptionsColumn.ReadOnly = true;
            this.colEWCDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEWCDate.Visible = true;
            this.colEWCDate.VisibleIndex = 12;
            // 
            // colExpectedReturnDate
            // 
            this.colExpectedReturnDate.Caption = "Expected Return Date";
            this.colExpectedReturnDate.ColumnEdit = this.repositoryItemTextEditShortDate2;
            this.colExpectedReturnDate.FieldName = "ExpectedReturnDate";
            this.colExpectedReturnDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedReturnDate.Name = "colExpectedReturnDate";
            this.colExpectedReturnDate.OptionsColumn.AllowEdit = false;
            this.colExpectedReturnDate.OptionsColumn.AllowFocus = false;
            this.colExpectedReturnDate.OptionsColumn.ReadOnly = true;
            this.colExpectedReturnDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedReturnDate.Visible = true;
            this.colExpectedReturnDate.VisibleIndex = 11;
            this.colExpectedReturnDate.Width = 128;
            // 
            // colHalfDayEnd
            // 
            this.colHalfDayEnd.Caption = "Half Day End";
            this.colHalfDayEnd.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colHalfDayEnd.FieldName = "HalfDayEnd";
            this.colHalfDayEnd.Name = "colHalfDayEnd";
            this.colHalfDayEnd.OptionsColumn.AllowEdit = false;
            this.colHalfDayEnd.OptionsColumn.AllowFocus = false;
            this.colHalfDayEnd.OptionsColumn.ReadOnly = true;
            this.colHalfDayEnd.Visible = true;
            this.colHalfDayEnd.VisibleIndex = 10;
            this.colHalfDayEnd.Width = 83;
            // 
            // colHalfDayStart
            // 
            this.colHalfDayStart.Caption = "Half Day Start";
            this.colHalfDayStart.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colHalfDayStart.FieldName = "HalfDayStart";
            this.colHalfDayStart.Name = "colHalfDayStart";
            this.colHalfDayStart.OptionsColumn.AllowEdit = false;
            this.colHalfDayStart.OptionsColumn.AllowFocus = false;
            this.colHalfDayStart.OptionsColumn.ReadOnly = true;
            this.colHalfDayStart.Visible = true;
            this.colHalfDayStart.VisibleIndex = 9;
            this.colHalfDayStart.Width = 89;
            // 
            // colAdditionalDaysPaid
            // 
            this.colAdditionalDaysPaid.Caption = "Additional Days Paid";
            this.colAdditionalDaysPaid.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAdditionalDaysPaid.FieldName = "AdditionalDaysPaid";
            this.colAdditionalDaysPaid.Name = "colAdditionalDaysPaid";
            this.colAdditionalDaysPaid.OptionsColumn.AllowEdit = false;
            this.colAdditionalDaysPaid.OptionsColumn.AllowFocus = false;
            this.colAdditionalDaysPaid.OptionsColumn.ReadOnly = true;
            this.colAdditionalDaysPaid.Visible = true;
            this.colAdditionalDaysPaid.VisibleIndex = 13;
            this.colAdditionalDaysPaid.Width = 118;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "####0.00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colLinkedDocumentCount6
            // 
            this.colLinkedDocumentCount6.Caption = "Linked Documents";
            this.colLinkedDocumentCount6.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsAbsences;
            this.colLinkedDocumentCount6.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount6.Name = "colLinkedDocumentCount6";
            this.colLinkedDocumentCount6.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount6.Visible = true;
            this.colLinkedDocumentCount6.VisibleIndex = 26;
            this.colLinkedDocumentCount6.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsAbsences
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsAbsences.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsAbsences.Name = "repositoryItemHyperLinkEditLinkedDocumentsAbsences";
            this.repositoryItemHyperLinkEditLinkedDocumentsAbsences.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsAbsences.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsAbsences_OpenLink);
            // 
            // colUnitsPaidCSP
            // 
            this.colUnitsPaidCSP.Caption = "Units Paid CSP";
            this.colUnitsPaidCSP.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colUnitsPaidCSP.FieldName = "UnitsPaidCSP";
            this.colUnitsPaidCSP.Name = "colUnitsPaidCSP";
            this.colUnitsPaidCSP.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidCSP.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidCSP.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidCSP.Visible = true;
            this.colUnitsPaidCSP.VisibleIndex = 14;
            this.colUnitsPaidCSP.Width = 88;
            // 
            // colUnitsPaidHalfPay
            // 
            this.colUnitsPaidHalfPay.Caption = "Units Paid Half Pay";
            this.colUnitsPaidHalfPay.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colUnitsPaidHalfPay.FieldName = "UnitsPaidHalfPay";
            this.colUnitsPaidHalfPay.Name = "colUnitsPaidHalfPay";
            this.colUnitsPaidHalfPay.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidHalfPay.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidHalfPay.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidHalfPay.Visible = true;
            this.colUnitsPaidHalfPay.VisibleIndex = 15;
            this.colUnitsPaidHalfPay.Width = 111;
            // 
            // colUnitsPaidOther
            // 
            this.colUnitsPaidOther.Caption = "Units Paid Other";
            this.colUnitsPaidOther.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colUnitsPaidOther.FieldName = "UnitsPaidOther";
            this.colUnitsPaidOther.Name = "colUnitsPaidOther";
            this.colUnitsPaidOther.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidOther.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidOther.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidOther.Visible = true;
            this.colUnitsPaidOther.VisibleIndex = 16;
            this.colUnitsPaidOther.Width = 112;
            // 
            // colUnitsPaidUnpaid
            // 
            this.colUnitsPaidUnpaid.Caption = "Units Unpaid";
            this.colUnitsPaidUnpaid.ColumnEdit = this.repositoryItemTextEditNumeric2DP2;
            this.colUnitsPaidUnpaid.FieldName = "UnitsPaidUnpaid";
            this.colUnitsPaidUnpaid.Name = "colUnitsPaidUnpaid";
            this.colUnitsPaidUnpaid.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidUnpaid.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidUnpaid.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidUnpaid.Visible = true;
            this.colUnitsPaidUnpaid.VisibleIndex = 17;
            this.colUnitsPaidUnpaid.Width = 81;
            // 
            // colAbsenceComment
            // 
            this.colAbsenceComment.Caption = "Absence Comment";
            this.colAbsenceComment.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colAbsenceComment.FieldName = "AbsenceComment";
            this.colAbsenceComment.Name = "colAbsenceComment";
            this.colAbsenceComment.OptionsColumn.ReadOnly = true;
            this.colAbsenceComment.Visible = true;
            this.colAbsenceComment.VisibleIndex = 25;
            this.colAbsenceComment.Width = 110;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(-1, 0);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1190, 26);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabPageVetting
            // 
            this.xtraTabPageVetting.AutoScroll = true;
            this.xtraTabPageVetting.Controls.Add(this.gridSplitContainer5);
            this.xtraTabPageVetting.Name = "xtraTabPageVetting";
            this.xtraTabPageVetting.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageVetting.Text = "Vetting";
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer5.Grid = this.gridControlVetting;
            this.gridSplitContainer5.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControlVetting);
            this.gridSplitContainer5.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer5.TabIndex = 0;
            // 
            // gridControlVetting
            // 
            this.gridControlVetting.DataSource = this.spHR00131GetEmployeeVettingBindingSource;
            this.gridControlVetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlVetting.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlVetting.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlVetting.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlVetting.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlVetting.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVetting_EmbeddedNavigator_ButtonClick);
            this.gridControlVetting.Location = new System.Drawing.Point(0, 0);
            this.gridControlVetting.MainView = this.gridViewVetting;
            this.gridControlVetting.MenuManager = this.barManager1;
            this.gridControlVetting.Name = "gridControlVetting";
            this.gridControlVetting.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryIteDateTime4,
            this.repositoryItemTextEdit2DPDays,
            this.repositoryItemCheckEdit8,
            this.repositoryItemHyperLinkEditVettingLinkedDocs});
            this.gridControlVetting.Size = new System.Drawing.Size(1189, 150);
            this.gridControlVetting.TabIndex = 1;
            this.gridControlVetting.UseEmbeddedNavigator = true;
            this.gridControlVetting.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewVetting});
            // 
            // spHR00131GetEmployeeVettingBindingSource
            // 
            this.spHR00131GetEmployeeVettingBindingSource.DataMember = "sp_HR_00131_Get_Employee_Vetting";
            this.spHR00131GetEmployeeVettingBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewVetting
            // 
            this.gridViewVetting.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVettingID,
            this.colEmployeeID5,
            this.colEmployeeName1,
            this.colEmployeeSurname4,
            this.colEmployeeFirstname1,
            this.colEmployeeNumber7,
            this.colVettingTypeID,
            this.colVettingType,
            this.colVettingSubTypeID,
            this.colVettingSubType,
            this.colDateIssued,
            this.colExpiryDate1,
            this.colReferenceNumber,
            this.colIssuedByID,
            this.colIssuedBy,
            this.colStatusID1,
            this.colStatus1,
            this.colNotificationPeriodDays,
            this.colRequestedByID,
            this.colRequestedBy,
            this.colRequestedDate,
            this.colEssentialForRole,
            this.colRemarks3,
            this.colRestrictionCount,
            this.colExpiredStatus,
            this.colLinkedDocumentCount1,
            this.colVisaRequired,
            this.colVisaType});
            this.gridViewVetting.GridControl = this.gridControlVetting;
            this.gridViewVetting.GroupCount = 1;
            this.gridViewVetting.Name = "gridViewVetting";
            this.gridViewVetting.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewVetting.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewVetting.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewVetting.OptionsLayout.StoreAppearance = true;
            this.gridViewVetting.OptionsLayout.StoreFormatRules = true;
            this.gridViewVetting.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewVetting.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewVetting.OptionsSelection.MultiSelect = true;
            this.gridViewVetting.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewVetting.OptionsView.ColumnAutoWidth = false;
            this.gridViewVetting.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewVetting.OptionsView.ShowGroupPanel = false;
            this.gridViewVetting.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpiryDate1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewVetting.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewVetting_CustomDrawCell);
            this.gridViewVetting.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewVetting_CustomRowCellEdit);
            this.gridViewVetting.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewVetting.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewVetting.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewVetting.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewVetting_ShowingEditor);
            this.gridViewVetting.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewVetting.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewVetting.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewVetting.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewVetting_MouseUp);
            this.gridViewVetting.DoubleClick += new System.EventHandler(this.gridViewVetting_DoubleClick);
            this.gridViewVetting.GotFocus += new System.EventHandler(this.gridViewVetting_GotFocus);
            // 
            // colVettingID
            // 
            this.colVettingID.Caption = "Vetting ID";
            this.colVettingID.FieldName = "VettingID";
            this.colVettingID.Name = "colVettingID";
            this.colVettingID.OptionsColumn.AllowEdit = false;
            this.colVettingID.OptionsColumn.AllowFocus = false;
            this.colVettingID.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeID5
            // 
            this.colEmployeeID5.Caption = "Employee ID";
            this.colEmployeeID5.FieldName = "EmployeeID";
            this.colEmployeeID5.Name = "colEmployeeID5";
            this.colEmployeeID5.OptionsColumn.AllowEdit = false;
            this.colEmployeeID5.OptionsColumn.AllowFocus = false;
            this.colEmployeeID5.OptionsColumn.ReadOnly = true;
            this.colEmployeeID5.Width = 81;
            // 
            // colEmployeeName1
            // 
            this.colEmployeeName1.Caption = "Linked To Employee";
            this.colEmployeeName1.FieldName = "EmployeeName";
            this.colEmployeeName1.Name = "colEmployeeName1";
            this.colEmployeeName1.OptionsColumn.AllowEdit = false;
            this.colEmployeeName1.OptionsColumn.AllowFocus = false;
            this.colEmployeeName1.OptionsColumn.ReadOnly = true;
            this.colEmployeeName1.Visible = true;
            this.colEmployeeName1.VisibleIndex = 1;
            this.colEmployeeName1.Width = 167;
            // 
            // colEmployeeSurname4
            // 
            this.colEmployeeSurname4.Caption = "Employee Surname";
            this.colEmployeeSurname4.FieldName = "EmployeeSurname";
            this.colEmployeeSurname4.Name = "colEmployeeSurname4";
            this.colEmployeeSurname4.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname4.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname4.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname4.Width = 112;
            // 
            // colEmployeeFirstname1
            // 
            this.colEmployeeFirstname1.Caption = "Employee Forename";
            this.colEmployeeFirstname1.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname1.Name = "colEmployeeFirstname1";
            this.colEmployeeFirstname1.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname1.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname1.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname1.Width = 118;
            // 
            // colEmployeeNumber7
            // 
            this.colEmployeeNumber7.Caption = "Employee #";
            this.colEmployeeNumber7.FieldName = "EmployeeNumber";
            this.colEmployeeNumber7.Name = "colEmployeeNumber7";
            this.colEmployeeNumber7.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber7.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber7.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber7.Width = 78;
            // 
            // colVettingTypeID
            // 
            this.colVettingTypeID.Caption = "Vetting Type ID";
            this.colVettingTypeID.FieldName = "VettingTypeID";
            this.colVettingTypeID.Name = "colVettingTypeID";
            this.colVettingTypeID.OptionsColumn.AllowEdit = false;
            this.colVettingTypeID.OptionsColumn.AllowFocus = false;
            this.colVettingTypeID.OptionsColumn.ReadOnly = true;
            this.colVettingTypeID.Width = 96;
            // 
            // colVettingType
            // 
            this.colVettingType.Caption = "Vetting Type";
            this.colVettingType.FieldName = "VettingType";
            this.colVettingType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colVettingType.Name = "colVettingType";
            this.colVettingType.OptionsColumn.AllowEdit = false;
            this.colVettingType.OptionsColumn.AllowFocus = false;
            this.colVettingType.OptionsColumn.ReadOnly = true;
            this.colVettingType.Visible = true;
            this.colVettingType.VisibleIndex = 0;
            this.colVettingType.Width = 120;
            // 
            // colVettingSubTypeID
            // 
            this.colVettingSubTypeID.Caption = "Vetting Sub Type ID";
            this.colVettingSubTypeID.FieldName = "VettingSubTypeID";
            this.colVettingSubTypeID.Name = "colVettingSubTypeID";
            this.colVettingSubTypeID.OptionsColumn.AllowEdit = false;
            this.colVettingSubTypeID.OptionsColumn.AllowFocus = false;
            this.colVettingSubTypeID.OptionsColumn.ReadOnly = true;
            this.colVettingSubTypeID.Width = 117;
            // 
            // colVettingSubType
            // 
            this.colVettingSubType.Caption = "Vetting Sub Type";
            this.colVettingSubType.FieldName = "VettingSubType";
            this.colVettingSubType.Name = "colVettingSubType";
            this.colVettingSubType.OptionsColumn.AllowEdit = false;
            this.colVettingSubType.OptionsColumn.AllowFocus = false;
            this.colVettingSubType.OptionsColumn.ReadOnly = true;
            this.colVettingSubType.Visible = true;
            this.colVettingSubType.VisibleIndex = 1;
            this.colVettingSubType.Width = 120;
            // 
            // colDateIssued
            // 
            this.colDateIssued.Caption = "Date Issued";
            this.colDateIssued.ColumnEdit = this.repositoryIteDateTime4;
            this.colDateIssued.FieldName = "DateIssued";
            this.colDateIssued.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateIssued.Name = "colDateIssued";
            this.colDateIssued.OptionsColumn.AllowEdit = false;
            this.colDateIssued.OptionsColumn.AllowFocus = false;
            this.colDateIssued.OptionsColumn.ReadOnly = true;
            this.colDateIssued.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateIssued.Visible = true;
            this.colDateIssued.VisibleIndex = 2;
            this.colDateIssued.Width = 100;
            // 
            // repositoryIteDateTime4
            // 
            this.repositoryIteDateTime4.AutoHeight = false;
            this.repositoryIteDateTime4.Mask.EditMask = "g";
            this.repositoryIteDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryIteDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryIteDateTime4.Name = "repositoryIteDateTime4";
            // 
            // colExpiryDate1
            // 
            this.colExpiryDate1.Caption = "Expiry Date";
            this.colExpiryDate1.ColumnEdit = this.repositoryIteDateTime4;
            this.colExpiryDate1.FieldName = "ExpiryDate";
            this.colExpiryDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpiryDate1.Name = "colExpiryDate1";
            this.colExpiryDate1.OptionsColumn.AllowEdit = false;
            this.colExpiryDate1.OptionsColumn.AllowFocus = false;
            this.colExpiryDate1.OptionsColumn.ReadOnly = true;
            this.colExpiryDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpiryDate1.Visible = true;
            this.colExpiryDate1.VisibleIndex = 3;
            this.colExpiryDate1.Width = 100;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference #";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 4;
            this.colReferenceNumber.Width = 82;
            // 
            // colIssuedByID
            // 
            this.colIssuedByID.Caption = "Issued By ID";
            this.colIssuedByID.FieldName = "IssuedByID";
            this.colIssuedByID.Name = "colIssuedByID";
            this.colIssuedByID.OptionsColumn.AllowEdit = false;
            this.colIssuedByID.OptionsColumn.AllowFocus = false;
            this.colIssuedByID.OptionsColumn.ReadOnly = true;
            this.colIssuedByID.Width = 82;
            // 
            // colIssuedBy
            // 
            this.colIssuedBy.Caption = "Issued By";
            this.colIssuedBy.FieldName = "IssuedBy";
            this.colIssuedBy.Name = "colIssuedBy";
            this.colIssuedBy.OptionsColumn.AllowEdit = false;
            this.colIssuedBy.OptionsColumn.AllowFocus = false;
            this.colIssuedBy.OptionsColumn.ReadOnly = true;
            this.colIssuedBy.Visible = true;
            this.colIssuedBy.VisibleIndex = 5;
            this.colIssuedBy.Width = 123;
            // 
            // colStatusID1
            // 
            this.colStatusID1.Caption = "Status ID";
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            // 
            // colStatus1
            // 
            this.colStatus1.Caption = "Status";
            this.colStatus1.FieldName = "Status";
            this.colStatus1.Name = "colStatus1";
            this.colStatus1.OptionsColumn.AllowEdit = false;
            this.colStatus1.OptionsColumn.AllowFocus = false;
            this.colStatus1.OptionsColumn.ReadOnly = true;
            this.colStatus1.Visible = true;
            this.colStatus1.VisibleIndex = 6;
            // 
            // colNotificationPeriodDays
            // 
            this.colNotificationPeriodDays.Caption = "Notification Period";
            this.colNotificationPeriodDays.ColumnEdit = this.repositoryItemTextEdit2DPDays;
            this.colNotificationPeriodDays.FieldName = "NotificationPeriodDays";
            this.colNotificationPeriodDays.Name = "colNotificationPeriodDays";
            this.colNotificationPeriodDays.OptionsColumn.AllowEdit = false;
            this.colNotificationPeriodDays.OptionsColumn.AllowFocus = false;
            this.colNotificationPeriodDays.OptionsColumn.ReadOnly = true;
            this.colNotificationPeriodDays.Visible = true;
            this.colNotificationPeriodDays.VisibleIndex = 7;
            this.colNotificationPeriodDays.Width = 108;
            // 
            // repositoryItemTextEdit2DPDays
            // 
            this.repositoryItemTextEdit2DPDays.AutoHeight = false;
            this.repositoryItemTextEdit2DPDays.Mask.EditMask = "#####0 Days";
            this.repositoryItemTextEdit2DPDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DPDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DPDays.Name = "repositoryItemTextEdit2DPDays";
            // 
            // colRequestedByID
            // 
            this.colRequestedByID.Caption = "Requested By ID";
            this.colRequestedByID.FieldName = "RequestedByID";
            this.colRequestedByID.Name = "colRequestedByID";
            this.colRequestedByID.OptionsColumn.AllowEdit = false;
            this.colRequestedByID.OptionsColumn.AllowFocus = false;
            this.colRequestedByID.OptionsColumn.ReadOnly = true;
            this.colRequestedByID.Width = 102;
            // 
            // colRequestedBy
            // 
            this.colRequestedBy.Caption = "Requested By";
            this.colRequestedBy.FieldName = "RequestedBy";
            this.colRequestedBy.Name = "colRequestedBy";
            this.colRequestedBy.OptionsColumn.AllowEdit = false;
            this.colRequestedBy.OptionsColumn.AllowFocus = false;
            this.colRequestedBy.OptionsColumn.ReadOnly = true;
            this.colRequestedBy.Visible = true;
            this.colRequestedBy.VisibleIndex = 8;
            this.colRequestedBy.Width = 88;
            // 
            // colRequestedDate
            // 
            this.colRequestedDate.Caption = "Requested Date";
            this.colRequestedDate.ColumnEdit = this.repositoryIteDateTime4;
            this.colRequestedDate.FieldName = "RequestedDate";
            this.colRequestedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRequestedDate.Name = "colRequestedDate";
            this.colRequestedDate.OptionsColumn.AllowEdit = false;
            this.colRequestedDate.OptionsColumn.AllowFocus = false;
            this.colRequestedDate.OptionsColumn.ReadOnly = true;
            this.colRequestedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRequestedDate.Visible = true;
            this.colRequestedDate.VisibleIndex = 9;
            this.colRequestedDate.Width = 100;
            // 
            // colEssentialForRole
            // 
            this.colEssentialForRole.Caption = "Essential For Role";
            this.colEssentialForRole.FieldName = "EssentialForRole";
            this.colEssentialForRole.Name = "colEssentialForRole";
            this.colEssentialForRole.OptionsColumn.AllowEdit = false;
            this.colEssentialForRole.OptionsColumn.AllowFocus = false;
            this.colEssentialForRole.OptionsColumn.ReadOnly = true;
            this.colEssentialForRole.Visible = true;
            this.colEssentialForRole.VisibleIndex = 10;
            this.colEssentialForRole.Width = 106;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ReadOnly = true;
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // colRestrictionCount
            // 
            this.colRestrictionCount.Caption = "Restriction Count";
            this.colRestrictionCount.FieldName = "RestrictionCount";
            this.colRestrictionCount.Name = "colRestrictionCount";
            this.colRestrictionCount.OptionsColumn.AllowEdit = false;
            this.colRestrictionCount.OptionsColumn.AllowFocus = false;
            this.colRestrictionCount.OptionsColumn.ReadOnly = true;
            this.colRestrictionCount.Visible = true;
            this.colRestrictionCount.VisibleIndex = 12;
            this.colRestrictionCount.Width = 104;
            // 
            // colExpiredStatus
            // 
            this.colExpiredStatus.Caption = "Expiry Warning";
            this.colExpiredStatus.ColumnEdit = this.repositoryItemCheckEdit8;
            this.colExpiredStatus.FieldName = "ExpiredStatus";
            this.colExpiredStatus.Name = "colExpiredStatus";
            this.colExpiredStatus.OptionsColumn.AllowEdit = false;
            this.colExpiredStatus.OptionsColumn.AllowFocus = false;
            this.colExpiredStatus.OptionsColumn.ReadOnly = true;
            this.colExpiredStatus.Width = 94;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Caption = "Check";
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = 1;
            this.repositoryItemCheckEdit8.ValueUnchecked = 0;
            // 
            // colLinkedDocumentCount1
            // 
            this.colLinkedDocumentCount1.Caption = "Linked Documents";
            this.colLinkedDocumentCount1.ColumnEdit = this.repositoryItemHyperLinkEditVettingLinkedDocs;
            this.colLinkedDocumentCount1.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount1.Name = "colLinkedDocumentCount1";
            this.colLinkedDocumentCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount1.Visible = true;
            this.colLinkedDocumentCount1.VisibleIndex = 15;
            this.colLinkedDocumentCount1.Width = 107;
            // 
            // repositoryItemHyperLinkEditVettingLinkedDocs
            // 
            this.repositoryItemHyperLinkEditVettingLinkedDocs.AutoHeight = false;
            this.repositoryItemHyperLinkEditVettingLinkedDocs.Name = "repositoryItemHyperLinkEditVettingLinkedDocs";
            this.repositoryItemHyperLinkEditVettingLinkedDocs.SingleClick = true;
            this.repositoryItemHyperLinkEditVettingLinkedDocs.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditVettingLinkedDocs_OpenLink);
            // 
            // colVisaRequired
            // 
            this.colVisaRequired.Caption = "Visa Required";
            this.colVisaRequired.ColumnEdit = this.repositoryItemCheckEdit8;
            this.colVisaRequired.FieldName = "VisaRequired";
            this.colVisaRequired.Name = "colVisaRequired";
            this.colVisaRequired.OptionsColumn.AllowEdit = false;
            this.colVisaRequired.OptionsColumn.AllowFocus = false;
            this.colVisaRequired.OptionsColumn.ReadOnly = true;
            this.colVisaRequired.Visible = true;
            this.colVisaRequired.VisibleIndex = 13;
            this.colVisaRequired.Width = 86;
            // 
            // colVisaType
            // 
            this.colVisaType.Caption = "Visa Type";
            this.colVisaType.FieldName = "VisaType";
            this.colVisaType.Name = "colVisaType";
            this.colVisaType.OptionsColumn.AllowEdit = false;
            this.colVisaType.OptionsColumn.AllowFocus = false;
            this.colVisaType.OptionsColumn.ReadOnly = true;
            this.colVisaType.Visible = true;
            this.colVisaType.VisibleIndex = 14;
            this.colVisaType.Width = 130;
            // 
            // xtraTabPageReferences
            // 
            this.xtraTabPageReferences.Controls.Add(this.gridSplitContainer15);
            this.xtraTabPageReferences.Name = "xtraTabPageReferences";
            this.xtraTabPageReferences.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageReferences.Text = "References";
            // 
            // gridSplitContainer15
            // 
            this.gridSplitContainer15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer15.Grid = this.gridControlReference;
            this.gridSplitContainer15.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer15.Name = "gridSplitContainer15";
            this.gridSplitContainer15.Panel1.Controls.Add(this.gridControlReference);
            this.gridSplitContainer15.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer15.TabIndex = 0;
            // 
            // gridControlReference
            // 
            this.gridControlReference.DataSource = this.spHR00200EmployeeReferencesLinkedToEmployeeBindingSource;
            this.gridControlReference.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlReference.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlReference.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlReference.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlReference.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlReference.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlReference_EmbeddedNavigator_ButtonClick);
            this.gridControlReference.Location = new System.Drawing.Point(0, 0);
            this.gridControlReference.MainView = this.gridViewReference;
            this.gridControlReference.MenuManager = this.barManager1;
            this.gridControlReference.Name = "gridControlReference";
            this.gridControlReference.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEditDateTime15,
            this.repositoryItemCheckEdit14,
            this.repositoryItemMemoExEdit15,
            this.repositoryItemHyperLinkEditLinkedDocumentReferences});
            this.gridControlReference.Size = new System.Drawing.Size(1189, 150);
            this.gridControlReference.TabIndex = 2;
            this.gridControlReference.UseEmbeddedNavigator = true;
            this.gridControlReference.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewReference});
            // 
            // spHR00200EmployeeReferencesLinkedToEmployeeBindingSource
            // 
            this.spHR00200EmployeeReferencesLinkedToEmployeeBindingSource.DataMember = "sp_HR_00200_Employee_References_Linked_To_Employee";
            this.spHR00200EmployeeReferencesLinkedToEmployeeBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewReference
            // 
            this.gridViewReference.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colReferenceID,
            this.colEmployeeID11,
            this.colReferenceTypeID,
            this.colCommunicationMethodID,
            this.colSuppliedByName,
            this.colSuppliedByContactTel,
            this.colSuppliedByContactEmail,
            this.colCompanyName,
            this.colRelationshipTypeID,
            this.colDateRequested,
            this.colRequestedByPersonID,
            this.colDateReceived,
            this.colReferenceSatisfactory,
            this.colRemarks9,
            this.colEmployeeName7,
            this.colEmployeeSurname8,
            this.colEmployeeFirstname6,
            this.colEmployeeNumber11,
            this.colCommunicationMethod,
            this.colRequestedByPerson,
            this.colReferenceType,
            this.colRelationshipType,
            this.colLinkedDocumentCount9});
            this.gridViewReference.GridControl = this.gridControlReference;
            this.gridViewReference.GroupCount = 1;
            this.gridViewReference.Name = "gridViewReference";
            this.gridViewReference.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewReference.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewReference.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewReference.OptionsLayout.StoreAppearance = true;
            this.gridViewReference.OptionsLayout.StoreFormatRules = true;
            this.gridViewReference.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewReference.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewReference.OptionsSelection.MultiSelect = true;
            this.gridViewReference.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewReference.OptionsView.ColumnAutoWidth = false;
            this.gridViewReference.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewReference.OptionsView.ShowGroupPanel = false;
            this.gridViewReference.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName7, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRequested, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewReference.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewReference_CustomDrawCell);
            this.gridViewReference.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewCRM_CustomRowCellEdit);
            this.gridViewReference.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewReference.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewReference.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewReference.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewReference_ShowingEditor);
            this.gridViewReference.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewReference.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewReference.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewReference.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewReference_MouseUp);
            this.gridViewReference.DoubleClick += new System.EventHandler(this.gridViewReference_DoubleClick);
            this.gridViewReference.GotFocus += new System.EventHandler(this.gridViewReference_GotFocus);
            // 
            // colReferenceID
            // 
            this.colReferenceID.Caption = "Reference ID";
            this.colReferenceID.FieldName = "ReferenceID";
            this.colReferenceID.Name = "colReferenceID";
            this.colReferenceID.OptionsColumn.AllowEdit = false;
            this.colReferenceID.OptionsColumn.AllowFocus = false;
            this.colReferenceID.OptionsColumn.ReadOnly = true;
            this.colReferenceID.Width = 85;
            // 
            // colEmployeeID11
            // 
            this.colEmployeeID11.Caption = "Employee ID";
            this.colEmployeeID11.FieldName = "EmployeeID";
            this.colEmployeeID11.Name = "colEmployeeID11";
            this.colEmployeeID11.OptionsColumn.AllowEdit = false;
            this.colEmployeeID11.OptionsColumn.AllowFocus = false;
            this.colEmployeeID11.OptionsColumn.ReadOnly = true;
            this.colEmployeeID11.Width = 81;
            // 
            // colReferenceTypeID
            // 
            this.colReferenceTypeID.Caption = "Reference Type ID";
            this.colReferenceTypeID.FieldName = "ReferenceTypeID";
            this.colReferenceTypeID.Name = "colReferenceTypeID";
            this.colReferenceTypeID.OptionsColumn.AllowEdit = false;
            this.colReferenceTypeID.OptionsColumn.AllowFocus = false;
            this.colReferenceTypeID.OptionsColumn.ReadOnly = true;
            this.colReferenceTypeID.Width = 112;
            // 
            // colCommunicationMethodID
            // 
            this.colCommunicationMethodID.Caption = "Communication Method ID";
            this.colCommunicationMethodID.FieldName = "CommunicationMethodID";
            this.colCommunicationMethodID.Name = "colCommunicationMethodID";
            this.colCommunicationMethodID.OptionsColumn.AllowEdit = false;
            this.colCommunicationMethodID.OptionsColumn.AllowFocus = false;
            this.colCommunicationMethodID.OptionsColumn.ReadOnly = true;
            this.colCommunicationMethodID.Width = 146;
            // 
            // colSuppliedByName
            // 
            this.colSuppliedByName.Caption = "Supplied By Name";
            this.colSuppliedByName.FieldName = "SuppliedByName";
            this.colSuppliedByName.Name = "colSuppliedByName";
            this.colSuppliedByName.OptionsColumn.AllowEdit = false;
            this.colSuppliedByName.OptionsColumn.AllowFocus = false;
            this.colSuppliedByName.OptionsColumn.ReadOnly = true;
            this.colSuppliedByName.Visible = true;
            this.colSuppliedByName.VisibleIndex = 4;
            this.colSuppliedByName.Width = 145;
            // 
            // colSuppliedByContactTel
            // 
            this.colSuppliedByContactTel.Caption = "Supplied By Contact Tel";
            this.colSuppliedByContactTel.FieldName = "SuppliedByContactTel";
            this.colSuppliedByContactTel.Name = "colSuppliedByContactTel";
            this.colSuppliedByContactTel.OptionsColumn.AllowEdit = false;
            this.colSuppliedByContactTel.OptionsColumn.AllowFocus = false;
            this.colSuppliedByContactTel.OptionsColumn.ReadOnly = true;
            this.colSuppliedByContactTel.Visible = true;
            this.colSuppliedByContactTel.VisibleIndex = 7;
            this.colSuppliedByContactTel.Width = 134;
            // 
            // colSuppliedByContactEmail
            // 
            this.colSuppliedByContactEmail.Caption = "Supplied By Contact Email";
            this.colSuppliedByContactEmail.FieldName = "SuppliedByContactEmail";
            this.colSuppliedByContactEmail.Name = "colSuppliedByContactEmail";
            this.colSuppliedByContactEmail.OptionsColumn.AllowEdit = false;
            this.colSuppliedByContactEmail.OptionsColumn.AllowFocus = false;
            this.colSuppliedByContactEmail.OptionsColumn.ReadOnly = true;
            this.colSuppliedByContactEmail.Visible = true;
            this.colSuppliedByContactEmail.VisibleIndex = 8;
            this.colSuppliedByContactEmail.Width = 144;
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Company Name";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.OptionsColumn.AllowEdit = false;
            this.colCompanyName.OptionsColumn.AllowFocus = false;
            this.colCompanyName.OptionsColumn.ReadOnly = true;
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 5;
            this.colCompanyName.Width = 126;
            // 
            // colRelationshipTypeID
            // 
            this.colRelationshipTypeID.Caption = "Relationship Type ID";
            this.colRelationshipTypeID.FieldName = "RelationshipTypeID";
            this.colRelationshipTypeID.Name = "colRelationshipTypeID";
            this.colRelationshipTypeID.OptionsColumn.AllowEdit = false;
            this.colRelationshipTypeID.OptionsColumn.AllowFocus = false;
            this.colRelationshipTypeID.OptionsColumn.ReadOnly = true;
            this.colRelationshipTypeID.Width = 120;
            // 
            // colDateRequested
            // 
            this.colDateRequested.Caption = "Date Requested";
            this.colDateRequested.ColumnEdit = this.repositoryItemDateEditDateTime15;
            this.colDateRequested.FieldName = "DateRequested";
            this.colDateRequested.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRequested.Name = "colDateRequested";
            this.colDateRequested.OptionsColumn.AllowEdit = false;
            this.colDateRequested.OptionsColumn.AllowFocus = false;
            this.colDateRequested.OptionsColumn.ReadOnly = true;
            this.colDateRequested.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRequested.Visible = true;
            this.colDateRequested.VisibleIndex = 1;
            this.colDateRequested.Width = 112;
            // 
            // repositoryItemDateEditDateTime15
            // 
            this.repositoryItemDateEditDateTime15.AutoHeight = false;
            this.repositoryItemDateEditDateTime15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditDateTime15.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEditDateTime15.Mask.EditMask = "g";
            this.repositoryItemDateEditDateTime15.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEditDateTime15.Name = "repositoryItemDateEditDateTime15";
            // 
            // colRequestedByPersonID
            // 
            this.colRequestedByPersonID.Caption = "Requested By Person ID";
            this.colRequestedByPersonID.FieldName = "RequestedByPersonID";
            this.colRequestedByPersonID.Name = "colRequestedByPersonID";
            this.colRequestedByPersonID.OptionsColumn.AllowEdit = false;
            this.colRequestedByPersonID.OptionsColumn.AllowFocus = false;
            this.colRequestedByPersonID.OptionsColumn.ReadOnly = true;
            this.colRequestedByPersonID.Width = 138;
            // 
            // colDateReceived
            // 
            this.colDateReceived.Caption = "Date Received";
            this.colDateReceived.ColumnEdit = this.repositoryItemDateEditDateTime15;
            this.colDateReceived.FieldName = "DateReceived";
            this.colDateReceived.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateReceived.Name = "colDateReceived";
            this.colDateReceived.OptionsColumn.AllowEdit = false;
            this.colDateReceived.OptionsColumn.AllowFocus = false;
            this.colDateReceived.OptionsColumn.ReadOnly = true;
            this.colDateReceived.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateReceived.Visible = true;
            this.colDateReceived.VisibleIndex = 2;
            this.colDateReceived.Width = 100;
            // 
            // colReferenceSatisfactory
            // 
            this.colReferenceSatisfactory.Caption = "Satisfactory";
            this.colReferenceSatisfactory.ColumnEdit = this.repositoryItemCheckEdit14;
            this.colReferenceSatisfactory.FieldName = "ReferenceSatisfactory";
            this.colReferenceSatisfactory.Name = "colReferenceSatisfactory";
            this.colReferenceSatisfactory.OptionsColumn.AllowEdit = false;
            this.colReferenceSatisfactory.OptionsColumn.AllowFocus = false;
            this.colReferenceSatisfactory.OptionsColumn.ReadOnly = true;
            this.colReferenceSatisfactory.Visible = true;
            this.colReferenceSatisfactory.VisibleIndex = 3;
            this.colReferenceSatisfactory.Width = 79;
            // 
            // repositoryItemCheckEdit14
            // 
            this.repositoryItemCheckEdit14.AutoHeight = false;
            this.repositoryItemCheckEdit14.Caption = "Check";
            this.repositoryItemCheckEdit14.Name = "repositoryItemCheckEdit14";
            this.repositoryItemCheckEdit14.ValueChecked = 1;
            this.repositoryItemCheckEdit14.ValueUnchecked = 0;
            // 
            // colRemarks9
            // 
            this.colRemarks9.Caption = "Remarks";
            this.colRemarks9.ColumnEdit = this.repositoryItemMemoExEdit15;
            this.colRemarks9.FieldName = "Remarks";
            this.colRemarks9.Name = "colRemarks9";
            this.colRemarks9.OptionsColumn.ReadOnly = true;
            this.colRemarks9.Visible = true;
            this.colRemarks9.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit15
            // 
            this.repositoryItemMemoExEdit15.AutoHeight = false;
            this.repositoryItemMemoExEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit15.Name = "repositoryItemMemoExEdit15";
            this.repositoryItemMemoExEdit15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit15.ShowIcon = false;
            // 
            // colEmployeeName7
            // 
            this.colEmployeeName7.Caption = "Linked To Employee";
            this.colEmployeeName7.FieldName = "EmployeeName";
            this.colEmployeeName7.Name = "colEmployeeName7";
            this.colEmployeeName7.OptionsColumn.AllowEdit = false;
            this.colEmployeeName7.OptionsColumn.AllowFocus = false;
            this.colEmployeeName7.OptionsColumn.ReadOnly = true;
            this.colEmployeeName7.Visible = true;
            this.colEmployeeName7.VisibleIndex = 9;
            this.colEmployeeName7.Width = 277;
            // 
            // colEmployeeSurname8
            // 
            this.colEmployeeSurname8.Caption = "Employee Surname";
            this.colEmployeeSurname8.FieldName = "EmployeeSurname";
            this.colEmployeeSurname8.Name = "colEmployeeSurname8";
            this.colEmployeeSurname8.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname8.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname8.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname8.Width = 112;
            // 
            // colEmployeeFirstname6
            // 
            this.colEmployeeFirstname6.Caption = "Employee Forename";
            this.colEmployeeFirstname6.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname6.Name = "colEmployeeFirstname6";
            this.colEmployeeFirstname6.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname6.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname6.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname6.Width = 118;
            // 
            // colEmployeeNumber11
            // 
            this.colEmployeeNumber11.Caption = "Employee #";
            this.colEmployeeNumber11.FieldName = "EmployeeNumber";
            this.colEmployeeNumber11.Name = "colEmployeeNumber11";
            this.colEmployeeNumber11.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber11.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber11.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber11.Width = 78;
            // 
            // colCommunicationMethod
            // 
            this.colCommunicationMethod.Caption = "Communication Method";
            this.colCommunicationMethod.FieldName = "CommunicationMethod";
            this.colCommunicationMethod.Name = "colCommunicationMethod";
            this.colCommunicationMethod.OptionsColumn.AllowEdit = false;
            this.colCommunicationMethod.OptionsColumn.AllowFocus = false;
            this.colCommunicationMethod.OptionsColumn.ReadOnly = true;
            this.colCommunicationMethod.Visible = true;
            this.colCommunicationMethod.VisibleIndex = 9;
            this.colCommunicationMethod.Width = 132;
            // 
            // colRequestedByPerson
            // 
            this.colRequestedByPerson.Caption = "Requested By Person";
            this.colRequestedByPerson.FieldName = "RequestedByPerson";
            this.colRequestedByPerson.Name = "colRequestedByPerson";
            this.colRequestedByPerson.OptionsColumn.AllowEdit = false;
            this.colRequestedByPerson.OptionsColumn.AllowFocus = false;
            this.colRequestedByPerson.OptionsColumn.ReadOnly = true;
            this.colRequestedByPerson.Visible = true;
            this.colRequestedByPerson.VisibleIndex = 10;
            this.colRequestedByPerson.Width = 124;
            // 
            // colReferenceType
            // 
            this.colReferenceType.Caption = "Reference Type";
            this.colReferenceType.FieldName = "ReferenceType";
            this.colReferenceType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colReferenceType.Name = "colReferenceType";
            this.colReferenceType.OptionsColumn.AllowEdit = false;
            this.colReferenceType.OptionsColumn.AllowFocus = false;
            this.colReferenceType.OptionsColumn.ReadOnly = true;
            this.colReferenceType.Visible = true;
            this.colReferenceType.VisibleIndex = 0;
            this.colReferenceType.Width = 154;
            // 
            // colRelationshipType
            // 
            this.colRelationshipType.Caption = "Relationship Type";
            this.colRelationshipType.FieldName = "RelationshipType";
            this.colRelationshipType.Name = "colRelationshipType";
            this.colRelationshipType.OptionsColumn.AllowEdit = false;
            this.colRelationshipType.OptionsColumn.AllowFocus = false;
            this.colRelationshipType.OptionsColumn.ReadOnly = true;
            this.colRelationshipType.Visible = true;
            this.colRelationshipType.VisibleIndex = 6;
            this.colRelationshipType.Width = 109;
            // 
            // colLinkedDocumentCount9
            // 
            this.colLinkedDocumentCount9.Caption = "Linked Documents";
            this.colLinkedDocumentCount9.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentReferences;
            this.colLinkedDocumentCount9.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount9.Name = "colLinkedDocumentCount9";
            this.colLinkedDocumentCount9.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount9.Visible = true;
            this.colLinkedDocumentCount9.VisibleIndex = 12;
            this.colLinkedDocumentCount9.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentReferences
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentReferences.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentReferences.Name = "repositoryItemHyperLinkEditLinkedDocumentReferences";
            this.repositoryItemHyperLinkEditLinkedDocumentReferences.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentReferences.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentReferences_OpenLink);
            // 
            // xtraTabPageAddresses
            // 
            this.xtraTabPageAddresses.Controls.Add(this.gridSplitContainer7);
            this.xtraTabPageAddresses.Name = "xtraTabPageAddresses";
            this.xtraTabPageAddresses.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageAddresses.Text = "Addresses";
            // 
            // gridSplitContainer7
            // 
            this.gridSplitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer7.Grid = this.gridControlAddresses;
            this.gridSplitContainer7.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer7.Name = "gridSplitContainer7";
            this.gridSplitContainer7.Panel1.Controls.Add(this.gridControlAddresses);
            this.gridSplitContainer7.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer7.TabIndex = 0;
            // 
            // gridControlAddresses
            // 
            this.gridControlAddresses.DataSource = this.sp_HR_00009_GetEmployeeAddressesBindingSource;
            this.gridControlAddresses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlAddresses.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlAddresses.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Copy Address to Clipboard", "copy")});
            this.gridControlAddresses.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAddresses_EmbeddedNavigator_ButtonClick);
            this.gridControlAddresses.Location = new System.Drawing.Point(0, 0);
            this.gridControlAddresses.MainView = this.gridViewAddresses;
            this.gridControlAddresses.MenuManager = this.barManager1;
            this.gridControlAddresses.Name = "gridControlAddresses";
            this.gridControlAddresses.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemMemoExEdit9,
            this.repositoryItemTextEditDateTime3});
            this.gridControlAddresses.Size = new System.Drawing.Size(1189, 150);
            this.gridControlAddresses.TabIndex = 0;
            this.gridControlAddresses.UseEmbeddedNavigator = true;
            this.gridControlAddresses.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAddresses});
            // 
            // sp_HR_00009_GetEmployeeAddressesBindingSource
            // 
            this.sp_HR_00009_GetEmployeeAddressesBindingSource.DataMember = "sp_HR_00009_GetEmployeeAddresses";
            this.sp_HR_00009_GetEmployeeAddressesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewAddresses
            // 
            this.gridViewAddresses.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeId1,
            this.colFirstname1,
            this.colSurname1,
            this.colAddressId,
            this.colIsCurrentAddress,
            this.colPostCode,
            this.colLandlineNumber,
            this.colComments1,
            this.colLinkedToEmployee,
            this.colAddressLine11,
            this.colAddressLine21,
            this.colAddressLine31,
            this.colAddressLine41,
            this.colAddressLine51,
            this.colEmployeeNumber4,
            this.colFromDate2,
            this.colToDate1});
            this.gridViewAddresses.GridControl = this.gridControlAddresses;
            this.gridViewAddresses.GroupCount = 1;
            this.gridViewAddresses.Name = "gridViewAddresses";
            this.gridViewAddresses.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewAddresses.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewAddresses.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewAddresses.OptionsLayout.StoreAppearance = true;
            this.gridViewAddresses.OptionsLayout.StoreFormatRules = true;
            this.gridViewAddresses.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewAddresses.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewAddresses.OptionsSelection.MultiSelect = true;
            this.gridViewAddresses.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewAddresses.OptionsView.BestFitMaxRowCount = 10;
            this.gridViewAddresses.OptionsView.ColumnAutoWidth = false;
            this.gridViewAddresses.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewAddresses.OptionsView.ShowGroupPanel = false;
            this.gridViewAddresses.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToEmployee, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIsCurrentAddress, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFromDate2, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewAddresses.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewAddresses.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewAddresses.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewAddresses.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewAddresses.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewAddresses.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewAddresses.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewAddresses_MouseUp);
            this.gridViewAddresses.DoubleClick += new System.EventHandler(this.gridViewAddresses_DoubleClick);
            this.gridViewAddresses.GotFocus += new System.EventHandler(this.gridViewAddresses_GotFocus);
            // 
            // colEmployeeId1
            // 
            this.colEmployeeId1.Caption = "EmployeeId";
            this.colEmployeeId1.FieldName = "EmployeeId";
            this.colEmployeeId1.Name = "colEmployeeId1";
            this.colEmployeeId1.OptionsColumn.AllowEdit = false;
            this.colEmployeeId1.OptionsColumn.AllowFocus = false;
            this.colEmployeeId1.OptionsColumn.ReadOnly = true;
            this.colEmployeeId1.OptionsFilter.AllowAutoFilter = false;
            // 
            // colFirstname1
            // 
            this.colFirstname1.Caption = "Firstname";
            this.colFirstname1.FieldName = "Firstname";
            this.colFirstname1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colFirstname1.Name = "colFirstname1";
            this.colFirstname1.OptionsColumn.AllowEdit = false;
            this.colFirstname1.OptionsColumn.AllowFocus = false;
            this.colFirstname1.OptionsColumn.ReadOnly = true;
            this.colFirstname1.OptionsFilter.AllowAutoFilter = false;
            // 
            // colSurname1
            // 
            this.colSurname1.Caption = "Surname";
            this.colSurname1.FieldName = "Surname";
            this.colSurname1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSurname1.Name = "colSurname1";
            this.colSurname1.OptionsColumn.AllowEdit = false;
            this.colSurname1.OptionsColumn.AllowFocus = false;
            this.colSurname1.OptionsColumn.ReadOnly = true;
            this.colSurname1.OptionsFilter.AllowAutoFilter = false;
            // 
            // colAddressId
            // 
            this.colAddressId.Caption = "AddressId";
            this.colAddressId.FieldName = "AddressId";
            this.colAddressId.Name = "colAddressId";
            this.colAddressId.OptionsColumn.AllowEdit = false;
            this.colAddressId.OptionsColumn.AllowFocus = false;
            this.colAddressId.OptionsColumn.ReadOnly = true;
            this.colAddressId.OptionsFilter.AllowAutoFilter = false;
            // 
            // colIsCurrentAddress
            // 
            this.colIsCurrentAddress.Caption = "Current Address";
            this.colIsCurrentAddress.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsCurrentAddress.FieldName = "IsCurrentAddress";
            this.colIsCurrentAddress.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colIsCurrentAddress.Name = "colIsCurrentAddress";
            this.colIsCurrentAddress.OptionsColumn.AllowEdit = false;
            this.colIsCurrentAddress.OptionsColumn.AllowFocus = false;
            this.colIsCurrentAddress.OptionsColumn.ReadOnly = true;
            this.colIsCurrentAddress.OptionsFilter.AllowAutoFilter = false;
            this.colIsCurrentAddress.Visible = true;
            this.colIsCurrentAddress.VisibleIndex = 0;
            this.colIsCurrentAddress.Width = 113;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit1.ReadOnly = true;
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colPostCode
            // 
            this.colPostCode.Caption = "PostCode";
            this.colPostCode.FieldName = "PostCode";
            this.colPostCode.Name = "colPostCode";
            this.colPostCode.OptionsColumn.AllowEdit = false;
            this.colPostCode.OptionsColumn.AllowFocus = false;
            this.colPostCode.OptionsColumn.ReadOnly = true;
            this.colPostCode.OptionsFilter.AllowAutoFilter = false;
            this.colPostCode.Visible = true;
            this.colPostCode.VisibleIndex = 6;
            // 
            // colLandlineNumber
            // 
            this.colLandlineNumber.Caption = "Landline Number";
            this.colLandlineNumber.FieldName = "LandlineNumber";
            this.colLandlineNumber.Name = "colLandlineNumber";
            this.colLandlineNumber.OptionsColumn.AllowEdit = false;
            this.colLandlineNumber.OptionsColumn.AllowFocus = false;
            this.colLandlineNumber.OptionsColumn.ReadOnly = true;
            this.colLandlineNumber.OptionsFilter.AllowAutoFilter = false;
            this.colLandlineNumber.Visible = true;
            this.colLandlineNumber.VisibleIndex = 7;
            this.colLandlineNumber.Width = 100;
            // 
            // colComments1
            // 
            this.colComments1.Caption = "Remarks";
            this.colComments1.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colComments1.FieldName = "Comments";
            this.colComments1.Name = "colComments1";
            this.colComments1.OptionsColumn.ReadOnly = true;
            this.colComments1.OptionsFilter.AllowAutoFilter = false;
            this.colComments1.Visible = true;
            this.colComments1.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // colLinkedToEmployee
            // 
            this.colLinkedToEmployee.Caption = "Linked To Employee";
            this.colLinkedToEmployee.FieldName = "LinkedToEmployee";
            this.colLinkedToEmployee.Name = "colLinkedToEmployee";
            this.colLinkedToEmployee.OptionsColumn.AllowEdit = false;
            this.colLinkedToEmployee.OptionsColumn.AllowFocus = false;
            this.colLinkedToEmployee.OptionsColumn.ReadOnly = true;
            this.colLinkedToEmployee.Visible = true;
            this.colLinkedToEmployee.VisibleIndex = 6;
            this.colLinkedToEmployee.Width = 136;
            // 
            // colAddressLine11
            // 
            this.colAddressLine11.Caption = "Address Line 1";
            this.colAddressLine11.FieldName = "AddressLine1";
            this.colAddressLine11.Name = "colAddressLine11";
            this.colAddressLine11.OptionsColumn.AllowEdit = false;
            this.colAddressLine11.OptionsColumn.AllowFocus = false;
            this.colAddressLine11.OptionsColumn.ReadOnly = true;
            this.colAddressLine11.Visible = true;
            this.colAddressLine11.VisibleIndex = 1;
            this.colAddressLine11.Width = 100;
            // 
            // colAddressLine21
            // 
            this.colAddressLine21.Caption = "Address Line 2";
            this.colAddressLine21.FieldName = "AddressLine2";
            this.colAddressLine21.Name = "colAddressLine21";
            this.colAddressLine21.OptionsColumn.AllowEdit = false;
            this.colAddressLine21.OptionsColumn.AllowFocus = false;
            this.colAddressLine21.OptionsColumn.ReadOnly = true;
            this.colAddressLine21.Visible = true;
            this.colAddressLine21.VisibleIndex = 2;
            this.colAddressLine21.Width = 100;
            // 
            // colAddressLine31
            // 
            this.colAddressLine31.Caption = "Address Line 3";
            this.colAddressLine31.FieldName = "AddressLine3";
            this.colAddressLine31.Name = "colAddressLine31";
            this.colAddressLine31.OptionsColumn.AllowEdit = false;
            this.colAddressLine31.OptionsColumn.AllowFocus = false;
            this.colAddressLine31.OptionsColumn.ReadOnly = true;
            this.colAddressLine31.Visible = true;
            this.colAddressLine31.VisibleIndex = 3;
            this.colAddressLine31.Width = 100;
            // 
            // colAddressLine41
            // 
            this.colAddressLine41.Caption = "Address Line 4";
            this.colAddressLine41.FieldName = "AddressLine4";
            this.colAddressLine41.Name = "colAddressLine41";
            this.colAddressLine41.OptionsColumn.AllowEdit = false;
            this.colAddressLine41.OptionsColumn.AllowFocus = false;
            this.colAddressLine41.OptionsColumn.ReadOnly = true;
            this.colAddressLine41.Visible = true;
            this.colAddressLine41.VisibleIndex = 4;
            this.colAddressLine41.Width = 100;
            // 
            // colAddressLine51
            // 
            this.colAddressLine51.Caption = "Address Line 5";
            this.colAddressLine51.FieldName = "AddressLine5";
            this.colAddressLine51.Name = "colAddressLine51";
            this.colAddressLine51.OptionsColumn.AllowEdit = false;
            this.colAddressLine51.OptionsColumn.AllowFocus = false;
            this.colAddressLine51.OptionsColumn.ReadOnly = true;
            this.colAddressLine51.Visible = true;
            this.colAddressLine51.VisibleIndex = 5;
            this.colAddressLine51.Width = 100;
            // 
            // colEmployeeNumber4
            // 
            this.colEmployeeNumber4.Caption = "Employee Number";
            this.colEmployeeNumber4.FieldName = "EmployeeNumber";
            this.colEmployeeNumber4.Name = "colEmployeeNumber4";
            this.colEmployeeNumber4.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber4.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber4.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber4.Visible = true;
            this.colEmployeeNumber4.VisibleIndex = 8;
            this.colEmployeeNumber4.Width = 107;
            // 
            // colFromDate2
            // 
            this.colFromDate2.Caption = "From Date";
            this.colFromDate2.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colFromDate2.FieldName = "FromDate";
            this.colFromDate2.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colFromDate2.Name = "colFromDate2";
            this.colFromDate2.OptionsColumn.AllowEdit = false;
            this.colFromDate2.OptionsColumn.AllowFocus = false;
            this.colFromDate2.OptionsColumn.ReadOnly = true;
            this.colFromDate2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colFromDate2.Visible = true;
            this.colFromDate2.VisibleIndex = 9;
            this.colFromDate2.Width = 84;
            // 
            // repositoryItemTextEditDateTime3
            // 
            this.repositoryItemTextEditDateTime3.AutoHeight = false;
            this.repositoryItemTextEditDateTime3.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime3.Name = "repositoryItemTextEditDateTime3";
            // 
            // colToDate1
            // 
            this.colToDate1.Caption = "To Date";
            this.colToDate1.ColumnEdit = this.repositoryItemTextEditDateTime3;
            this.colToDate1.FieldName = "ToDate";
            this.colToDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colToDate1.Name = "colToDate1";
            this.colToDate1.OptionsColumn.AllowEdit = false;
            this.colToDate1.OptionsColumn.AllowFocus = false;
            this.colToDate1.OptionsColumn.ReadOnly = true;
            this.colToDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colToDate1.Visible = true;
            this.colToDate1.VisibleIndex = 10;
            // 
            // xtraTabPageNextOfKin
            // 
            this.xtraTabPageNextOfKin.Controls.Add(this.gridSplitContainer6);
            this.xtraTabPageNextOfKin.Name = "xtraTabPageNextOfKin";
            this.xtraTabPageNextOfKin.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageNextOfKin.Text = "Next of Kin";
            // 
            // gridSplitContainer6
            // 
            this.gridSplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer6.Grid = this.gridControlNextOfKin;
            this.gridSplitContainer6.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer6.Name = "gridSplitContainer6";
            this.gridSplitContainer6.Panel1.Controls.Add(this.gridControlNextOfKin);
            this.gridSplitContainer6.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer6.TabIndex = 0;
            // 
            // gridControlNextOfKin
            // 
            this.gridControlNextOfKin.DataSource = this.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesBindingSource;
            this.gridControlNextOfKin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlNextOfKin.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlNextOfKin.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view")});
            this.gridControlNextOfKin.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlNextOfKin_EmbeddedNavigator_ButtonClick);
            this.gridControlNextOfKin.Location = new System.Drawing.Point(0, 0);
            this.gridControlNextOfKin.MainView = this.gridViewNextOfKin;
            this.gridControlNextOfKin.MenuManager = this.barManager1;
            this.gridControlNextOfKin.Name = "gridControlNextOfKin";
            this.gridControlNextOfKin.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControlNextOfKin.Size = new System.Drawing.Size(1189, 150);
            this.gridControlNextOfKin.TabIndex = 0;
            this.gridControlNextOfKin.UseEmbeddedNavigator = true;
            this.gridControlNextOfKin.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewNextOfKin});
            // 
            // sp_HR_00021_Get_Next_Of_Kin_For_EmployeesBindingSource
            // 
            this.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesBindingSource.DataMember = "sp_HR_00021_Get_Next_Of_Kin_For_Employees";
            this.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewNextOfKin
            // 
            this.gridViewNextOfKin.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colEmployeeId2,
            this.colEmployeeNumber1,
            this.colEmployeeName2,
            this.colTitle1,
            this.colFirstname2,
            this.colSurname2,
            this.colPostCode1,
            this.colLandline,
            this.colMobile,
            this.colWorkNumber,
            this.colPriorityOrder,
            this.colRelationshipId,
            this.colComments2,
            this.colRelationship,
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colAddressLine4,
            this.colAddressLine5});
            this.gridViewNextOfKin.GridControl = this.gridControlNextOfKin;
            this.gridViewNextOfKin.GroupCount = 1;
            this.gridViewNextOfKin.Name = "gridViewNextOfKin";
            this.gridViewNextOfKin.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewNextOfKin.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewNextOfKin.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewNextOfKin.OptionsLayout.StoreAppearance = true;
            this.gridViewNextOfKin.OptionsLayout.StoreFormatRules = true;
            this.gridViewNextOfKin.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewNextOfKin.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewNextOfKin.OptionsSelection.MultiSelect = true;
            this.gridViewNextOfKin.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewNextOfKin.OptionsView.BestFitMaxRowCount = 10;
            this.gridViewNextOfKin.OptionsView.ColumnAutoWidth = false;
            this.gridViewNextOfKin.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewNextOfKin.OptionsView.ShowGroupPanel = false;
            this.gridViewNextOfKin.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewNextOfKin.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewNextOfKin.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewNextOfKin.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewNextOfKin.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewNextOfKin.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewNextOfKin.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewNextOfKin.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewNextOfKin_MouseUp);
            this.gridViewNextOfKin.DoubleClick += new System.EventHandler(this.gridViewNextOfKin_DoubleClick);
            this.gridViewNextOfKin.GotFocus += new System.EventHandler(this.gridViewNextOfKin_GotFocus);
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeId2
            // 
            this.colEmployeeId2.Caption = "EmployeeId";
            this.colEmployeeId2.FieldName = "EmployeeId";
            this.colEmployeeId2.Name = "colEmployeeId2";
            this.colEmployeeId2.OptionsColumn.AllowEdit = false;
            this.colEmployeeId2.OptionsColumn.AllowFocus = false;
            this.colEmployeeId2.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeNumber1
            // 
            this.colEmployeeNumber1.Caption = "Employee Number";
            this.colEmployeeNumber1.FieldName = "EmployeeNumber";
            this.colEmployeeNumber1.Name = "colEmployeeNumber1";
            this.colEmployeeNumber1.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber1.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber1.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber1.Width = 104;
            // 
            // colEmployeeName2
            // 
            this.colEmployeeName2.Caption = "Linked To Employee";
            this.colEmployeeName2.FieldName = "EmployeeName";
            this.colEmployeeName2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeName2.Name = "colEmployeeName2";
            this.colEmployeeName2.OptionsColumn.AllowEdit = false;
            this.colEmployeeName2.OptionsColumn.AllowFocus = false;
            this.colEmployeeName2.OptionsColumn.ReadOnly = true;
            this.colEmployeeName2.Visible = true;
            this.colEmployeeName2.VisibleIndex = 1;
            this.colEmployeeName2.Width = 149;
            // 
            // colTitle1
            // 
            this.colTitle1.Caption = "Title";
            this.colTitle1.FieldName = "Title";
            this.colTitle1.Name = "colTitle1";
            this.colTitle1.OptionsColumn.AllowEdit = false;
            this.colTitle1.OptionsColumn.AllowFocus = false;
            this.colTitle1.OptionsColumn.ReadOnly = true;
            this.colTitle1.Visible = true;
            this.colTitle1.VisibleIndex = 2;
            // 
            // colFirstname2
            // 
            this.colFirstname2.Caption = "Firstname";
            this.colFirstname2.FieldName = "Firstname";
            this.colFirstname2.Name = "colFirstname2";
            this.colFirstname2.OptionsColumn.AllowEdit = false;
            this.colFirstname2.OptionsColumn.AllowFocus = false;
            this.colFirstname2.OptionsColumn.ReadOnly = true;
            this.colFirstname2.Visible = true;
            this.colFirstname2.VisibleIndex = 3;
            this.colFirstname2.Width = 89;
            // 
            // colSurname2
            // 
            this.colSurname2.Caption = "Surname";
            this.colSurname2.FieldName = "Surname";
            this.colSurname2.Name = "colSurname2";
            this.colSurname2.OptionsColumn.AllowEdit = false;
            this.colSurname2.OptionsColumn.AllowFocus = false;
            this.colSurname2.OptionsColumn.ReadOnly = true;
            this.colSurname2.Visible = true;
            this.colSurname2.VisibleIndex = 4;
            this.colSurname2.Width = 90;
            // 
            // colPostCode1
            // 
            this.colPostCode1.Caption = "PostCode";
            this.colPostCode1.FieldName = "PostCode";
            this.colPostCode1.Name = "colPostCode1";
            this.colPostCode1.OptionsColumn.AllowEdit = false;
            this.colPostCode1.OptionsColumn.AllowFocus = false;
            this.colPostCode1.OptionsColumn.ReadOnly = true;
            this.colPostCode1.Visible = true;
            this.colPostCode1.VisibleIndex = 10;
            // 
            // colLandline
            // 
            this.colLandline.Caption = "Landline";
            this.colLandline.FieldName = "Landline";
            this.colLandline.Name = "colLandline";
            this.colLandline.OptionsColumn.AllowEdit = false;
            this.colLandline.OptionsColumn.AllowFocus = false;
            this.colLandline.OptionsColumn.ReadOnly = true;
            this.colLandline.Visible = true;
            this.colLandline.VisibleIndex = 11;
            // 
            // colMobile
            // 
            this.colMobile.Caption = "Mobile";
            this.colMobile.FieldName = "Mobile";
            this.colMobile.Name = "colMobile";
            this.colMobile.OptionsColumn.AllowEdit = false;
            this.colMobile.OptionsColumn.AllowFocus = false;
            this.colMobile.OptionsColumn.ReadOnly = true;
            this.colMobile.Visible = true;
            this.colMobile.VisibleIndex = 12;
            // 
            // colWorkNumber
            // 
            this.colWorkNumber.Caption = "Work Number";
            this.colWorkNumber.FieldName = "WorkNumber";
            this.colWorkNumber.Name = "colWorkNumber";
            this.colWorkNumber.OptionsColumn.AllowEdit = false;
            this.colWorkNumber.OptionsColumn.AllowFocus = false;
            this.colWorkNumber.OptionsColumn.ReadOnly = true;
            this.colWorkNumber.Visible = true;
            this.colWorkNumber.VisibleIndex = 13;
            this.colWorkNumber.Width = 86;
            // 
            // colPriorityOrder
            // 
            this.colPriorityOrder.Caption = "Priority Order";
            this.colPriorityOrder.FieldName = "PriorityOrder";
            this.colPriorityOrder.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPriorityOrder.Name = "colPriorityOrder";
            this.colPriorityOrder.OptionsColumn.AllowEdit = false;
            this.colPriorityOrder.OptionsColumn.AllowFocus = false;
            this.colPriorityOrder.OptionsColumn.ReadOnly = true;
            this.colPriorityOrder.Visible = true;
            this.colPriorityOrder.VisibleIndex = 0;
            this.colPriorityOrder.Width = 86;
            // 
            // colRelationshipId
            // 
            this.colRelationshipId.Caption = "RelationshipId";
            this.colRelationshipId.FieldName = "RelationshipId";
            this.colRelationshipId.Name = "colRelationshipId";
            this.colRelationshipId.OptionsColumn.AllowEdit = false;
            this.colRelationshipId.OptionsColumn.AllowFocus = false;
            this.colRelationshipId.OptionsColumn.ReadOnly = true;
            // 
            // colComments2
            // 
            this.colComments2.Caption = "Remarks";
            this.colComments2.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colComments2.FieldName = "Comments";
            this.colComments2.Name = "colComments2";
            this.colComments2.OptionsColumn.ReadOnly = true;
            this.colComments2.Visible = true;
            this.colComments2.VisibleIndex = 14;
            this.colComments2.Width = 92;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colRelationship
            // 
            this.colRelationship.Caption = "Relationship";
            this.colRelationship.FieldName = "Relationship";
            this.colRelationship.Name = "colRelationship";
            this.colRelationship.OptionsColumn.AllowEdit = false;
            this.colRelationship.OptionsColumn.AllowFocus = false;
            this.colRelationship.OptionsColumn.ReadOnly = true;
            this.colRelationship.Visible = true;
            this.colRelationship.VisibleIndex = 1;
            this.colRelationship.Width = 79;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.Caption = "Address Line 1";
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 5;
            this.colAddressLine1.Width = 100;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.Caption = "Address Line 2";
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Visible = true;
            this.colAddressLine2.VisibleIndex = 6;
            this.colAddressLine2.Width = 100;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.Caption = "Address Line 3";
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Visible = true;
            this.colAddressLine3.VisibleIndex = 7;
            this.colAddressLine3.Width = 100;
            // 
            // colAddressLine4
            // 
            this.colAddressLine4.Caption = "Address Line 4";
            this.colAddressLine4.FieldName = "AddressLine4";
            this.colAddressLine4.Name = "colAddressLine4";
            this.colAddressLine4.OptionsColumn.AllowEdit = false;
            this.colAddressLine4.OptionsColumn.AllowFocus = false;
            this.colAddressLine4.OptionsColumn.ReadOnly = true;
            this.colAddressLine4.Visible = true;
            this.colAddressLine4.VisibleIndex = 8;
            this.colAddressLine4.Width = 100;
            // 
            // colAddressLine5
            // 
            this.colAddressLine5.Caption = "Address Line 5";
            this.colAddressLine5.FieldName = "AddressLine5";
            this.colAddressLine5.Name = "colAddressLine5";
            this.colAddressLine5.OptionsColumn.AllowEdit = false;
            this.colAddressLine5.OptionsColumn.AllowFocus = false;
            this.colAddressLine5.OptionsColumn.ReadOnly = true;
            this.colAddressLine5.Visible = true;
            this.colAddressLine5.VisibleIndex = 9;
            this.colAddressLine5.Width = 100;
            // 
            // xtraTabPageDeptsWorked
            // 
            this.xtraTabPageDeptsWorked.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPageDeptsWorked.Name = "xtraTabPageDeptsWorked";
            this.xtraTabPageDeptsWorked.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageDeptsWorked.Text = "Depts Worked";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControlDepartmentWorked;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControlDepartmentWorked);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControlDepartmentWorked
            // 
            this.gridControlDepartmentWorked.DataSource = this.spHR00001EmployeeDeptsWorkedLinkedToContractBindingSource;
            this.gridControlDepartmentWorked.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDepartmentWorked.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlDepartmentWorked.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlDepartmentWorked_EmbeddedNavigator_ButtonClick);
            this.gridControlDepartmentWorked.Location = new System.Drawing.Point(0, 0);
            this.gridControlDepartmentWorked.MainView = this.gridViewDepartmentWorked;
            this.gridControlDepartmentWorked.MenuManager = this.barManager1;
            this.gridControlDepartmentWorked.Name = "gridControlDepartmentWorked";
            this.gridControlDepartmentWorked.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit10,
            this.repositoryItemTextEditDate2,
            this.repositoryItemCheckEdit7,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked});
            this.gridControlDepartmentWorked.Size = new System.Drawing.Size(1189, 150);
            this.gridControlDepartmentWorked.TabIndex = 3;
            this.gridControlDepartmentWorked.UseEmbeddedNavigator = true;
            this.gridControlDepartmentWorked.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDepartmentWorked});
            // 
            // spHR00001EmployeeDeptsWorkedLinkedToContractBindingSource
            // 
            this.spHR00001EmployeeDeptsWorkedLinkedToContractBindingSource.DataMember = "sp_HR_00001_Employee_Depts_Worked_Linked_To_Contract";
            this.spHR00001EmployeeDeptsWorkedLinkedToContractBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewDepartmentWorked
            // 
            this.gridViewDepartmentWorked.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeDeptWorkedID,
            this.colDepartmentLocationID,
            this.colRegionID,
            this.colBaseTypeID,
            this.colPositionID,
            this.colReportsToStaffID,
            this.colTimePercentage,
            this.colFromDate1,
            this.colToDate,
            this.colActive,
            this.colRemarks,
            this.colBaseType,
            this.colEmployeeSurnameForename2,
            this.colEmployeeSurname3,
            this.colEmployeeForename2,
            this.colEmployeeNumber6,
            this.colContractNumber2,
            this.colReportsToStaff,
            this.colDepartmentName,
            this.colLocationName,
            this.colBusinessAreaName,
            this.colRegionName,
            this.colCostCentreCode,
            this.colEmployeeID6,
            this.colLinkedDocumentCount5,
            this.colPositionDescription});
            styleFormatCondition5.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition5.Appearance.Options.UseForeColor = true;
            styleFormatCondition5.ApplyToRow = true;
            styleFormatCondition5.Column = this.colActive;
            styleFormatCondition5.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition5.Value1 = 0;
            this.gridViewDepartmentWorked.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition5});
            this.gridViewDepartmentWorked.GridControl = this.gridControlDepartmentWorked;
            this.gridViewDepartmentWorked.GroupCount = 1;
            this.gridViewDepartmentWorked.Name = "gridViewDepartmentWorked";
            this.gridViewDepartmentWorked.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewDepartmentWorked.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewDepartmentWorked.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewDepartmentWorked.OptionsLayout.StoreAppearance = true;
            this.gridViewDepartmentWorked.OptionsLayout.StoreFormatRules = true;
            this.gridViewDepartmentWorked.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewDepartmentWorked.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewDepartmentWorked.OptionsSelection.MultiSelect = true;
            this.gridViewDepartmentWorked.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewDepartmentWorked.OptionsView.ColumnAutoWidth = false;
            this.gridViewDepartmentWorked.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewDepartmentWorked.OptionsView.ShowGroupPanel = false;
            this.gridViewDepartmentWorked.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurnameForename2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewDepartmentWorked.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewDepartmentWorked_CustomRowCellEdit);
            this.gridViewDepartmentWorked.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewDepartmentWorked.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewDepartmentWorked.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewDepartmentWorked.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewDepartmentWorked_ShowingEditor);
            this.gridViewDepartmentWorked.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewDepartmentWorked.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewDepartmentWorked.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewDepartmentWorked.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewDepartmentWorked_MouseUp);
            this.gridViewDepartmentWorked.DoubleClick += new System.EventHandler(this.gridViewDepartmentWorked_DoubleClick);
            this.gridViewDepartmentWorked.GotFocus += new System.EventHandler(this.gridViewDepartmentWorked_GotFocus);
            // 
            // colEmployeeDeptWorkedID
            // 
            this.colEmployeeDeptWorkedID.Caption = "Employee Department Worked ID";
            this.colEmployeeDeptWorkedID.FieldName = "EmployeeDeptWorkedID";
            this.colEmployeeDeptWorkedID.Name = "colEmployeeDeptWorkedID";
            this.colEmployeeDeptWorkedID.OptionsColumn.AllowEdit = false;
            this.colEmployeeDeptWorkedID.OptionsColumn.AllowFocus = false;
            this.colEmployeeDeptWorkedID.OptionsColumn.ReadOnly = true;
            this.colEmployeeDeptWorkedID.Width = 181;
            // 
            // colDepartmentLocationID
            // 
            this.colDepartmentLocationID.Caption = "Department Location ID";
            this.colDepartmentLocationID.FieldName = "DepartmentLocationID";
            this.colDepartmentLocationID.Name = "colDepartmentLocationID";
            this.colDepartmentLocationID.OptionsColumn.AllowEdit = false;
            this.colDepartmentLocationID.OptionsColumn.AllowFocus = false;
            this.colDepartmentLocationID.OptionsColumn.ReadOnly = true;
            this.colDepartmentLocationID.Width = 135;
            // 
            // colRegionID
            // 
            this.colRegionID.Caption = "Region ID";
            this.colRegionID.FieldName = "RegionID";
            this.colRegionID.Name = "colRegionID";
            this.colRegionID.OptionsColumn.AllowEdit = false;
            this.colRegionID.OptionsColumn.AllowFocus = false;
            this.colRegionID.OptionsColumn.ReadOnly = true;
            // 
            // colBaseTypeID
            // 
            this.colBaseTypeID.Caption = "Base Type ID";
            this.colBaseTypeID.FieldName = "BaseTypeID";
            this.colBaseTypeID.Name = "colBaseTypeID";
            this.colBaseTypeID.OptionsColumn.AllowEdit = false;
            this.colBaseTypeID.OptionsColumn.AllowFocus = false;
            this.colBaseTypeID.OptionsColumn.ReadOnly = true;
            this.colBaseTypeID.Width = 85;
            // 
            // colPositionID
            // 
            this.colPositionID.Caption = "Position ID";
            this.colPositionID.FieldName = "PositionID";
            this.colPositionID.Name = "colPositionID";
            this.colPositionID.OptionsColumn.AllowEdit = false;
            this.colPositionID.OptionsColumn.AllowFocus = false;
            this.colPositionID.OptionsColumn.ReadOnly = true;
            // 
            // colReportsToStaffID
            // 
            this.colReportsToStaffID.Caption = "Report to Staff ID";
            this.colReportsToStaffID.FieldName = "ReportsToStaffID";
            this.colReportsToStaffID.Name = "colReportsToStaffID";
            this.colReportsToStaffID.OptionsColumn.AllowEdit = false;
            this.colReportsToStaffID.OptionsColumn.AllowFocus = false;
            this.colReportsToStaffID.OptionsColumn.ReadOnly = true;
            this.colReportsToStaffID.Width = 108;
            // 
            // colTimePercentage
            // 
            this.colTimePercentage.Caption = "Time Percentage";
            this.colTimePercentage.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colTimePercentage.FieldName = "TimePercentage";
            this.colTimePercentage.Name = "colTimePercentage";
            this.colTimePercentage.OptionsColumn.AllowEdit = false;
            this.colTimePercentage.OptionsColumn.AllowFocus = false;
            this.colTimePercentage.OptionsColumn.ReadOnly = true;
            this.colTimePercentage.Visible = true;
            this.colTimePercentage.VisibleIndex = 10;
            this.colTimePercentage.Width = 101;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colFromDate1
            // 
            this.colFromDate1.Caption = "From Date";
            this.colFromDate1.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colFromDate1.FieldName = "FromDate";
            this.colFromDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colFromDate1.Name = "colFromDate1";
            this.colFromDate1.OptionsColumn.AllowEdit = false;
            this.colFromDate1.OptionsColumn.AllowFocus = false;
            this.colFromDate1.OptionsColumn.ReadOnly = true;
            this.colFromDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colFromDate1.Visible = true;
            this.colFromDate1.VisibleIndex = 6;
            // 
            // repositoryItemTextEditDate2
            // 
            this.repositoryItemTextEditDate2.AutoHeight = false;
            this.repositoryItemTextEditDate2.Mask.EditMask = "d";
            this.repositoryItemTextEditDate2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate2.Name = "repositoryItemTextEditDate2";
            // 
            // colToDate
            // 
            this.colToDate.Caption = "To Date";
            this.colToDate.ColumnEdit = this.repositoryItemTextEditDate2;
            this.colToDate.FieldName = "ToDate";
            this.colToDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colToDate.Name = "colToDate";
            this.colToDate.OptionsColumn.AllowEdit = false;
            this.colToDate.OptionsColumn.AllowFocus = false;
            this.colToDate.OptionsColumn.ReadOnly = true;
            this.colToDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colToDate.Visible = true;
            this.colToDate.VisibleIndex = 7;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit10;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 12;
            // 
            // repositoryItemMemoExEdit10
            // 
            this.repositoryItemMemoExEdit10.AutoHeight = false;
            this.repositoryItemMemoExEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit10.Name = "repositoryItemMemoExEdit10";
            this.repositoryItemMemoExEdit10.ReadOnly = true;
            this.repositoryItemMemoExEdit10.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit10.ShowIcon = false;
            // 
            // colBaseType
            // 
            this.colBaseType.Caption = "Base Type";
            this.colBaseType.FieldName = "BaseType";
            this.colBaseType.Name = "colBaseType";
            this.colBaseType.OptionsColumn.AllowEdit = false;
            this.colBaseType.OptionsColumn.AllowFocus = false;
            this.colBaseType.OptionsColumn.ReadOnly = true;
            this.colBaseType.Visible = true;
            this.colBaseType.VisibleIndex = 4;
            this.colBaseType.Width = 71;
            // 
            // colEmployeeSurnameForename2
            // 
            this.colEmployeeSurnameForename2.Caption = "Linked To Employee";
            this.colEmployeeSurnameForename2.FieldName = "EmployeeSurnameForename";
            this.colEmployeeSurnameForename2.Name = "colEmployeeSurnameForename2";
            this.colEmployeeSurnameForename2.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurnameForename2.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurnameForename2.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurnameForename2.Visible = true;
            this.colEmployeeSurnameForename2.VisibleIndex = 3;
            this.colEmployeeSurnameForename2.Width = 206;
            // 
            // colEmployeeSurname3
            // 
            this.colEmployeeSurname3.Caption = "Surname";
            this.colEmployeeSurname3.FieldName = "EmployeeSurname";
            this.colEmployeeSurname3.Name = "colEmployeeSurname3";
            this.colEmployeeSurname3.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname3.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname3.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeForename2
            // 
            this.colEmployeeForename2.Caption = "Forename";
            this.colEmployeeForename2.FieldName = "EmployeeForename";
            this.colEmployeeForename2.Name = "colEmployeeForename2";
            this.colEmployeeForename2.OptionsColumn.AllowEdit = false;
            this.colEmployeeForename2.OptionsColumn.AllowFocus = false;
            this.colEmployeeForename2.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeNumber6
            // 
            this.colEmployeeNumber6.Caption = "Employee #";
            this.colEmployeeNumber6.FieldName = "EmployeeNumber";
            this.colEmployeeNumber6.Name = "colEmployeeNumber6";
            this.colEmployeeNumber6.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber6.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber6.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber6.Width = 78;
            // 
            // colContractNumber2
            // 
            this.colContractNumber2.Caption = "Contract #";
            this.colContractNumber2.FieldName = "ContractNumber";
            this.colContractNumber2.Name = "colContractNumber2";
            this.colContractNumber2.OptionsColumn.AllowEdit = false;
            this.colContractNumber2.OptionsColumn.AllowFocus = false;
            this.colContractNumber2.OptionsColumn.ReadOnly = true;
            // 
            // colReportsToStaff
            // 
            this.colReportsToStaff.Caption = "Reports To";
            this.colReportsToStaff.FieldName = "ReportsToStaff";
            this.colReportsToStaff.Name = "colReportsToStaff";
            this.colReportsToStaff.OptionsColumn.AllowEdit = false;
            this.colReportsToStaff.OptionsColumn.AllowFocus = false;
            this.colReportsToStaff.OptionsColumn.ReadOnly = true;
            this.colReportsToStaff.Visible = true;
            this.colReportsToStaff.VisibleIndex = 9;
            this.colReportsToStaff.Width = 141;
            // 
            // colDepartmentName
            // 
            this.colDepartmentName.Caption = "Dept Name";
            this.colDepartmentName.FieldName = "DepartmentName";
            this.colDepartmentName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDepartmentName.Name = "colDepartmentName";
            this.colDepartmentName.OptionsColumn.AllowEdit = false;
            this.colDepartmentName.OptionsColumn.AllowFocus = false;
            this.colDepartmentName.OptionsColumn.ReadOnly = true;
            this.colDepartmentName.Visible = true;
            this.colDepartmentName.VisibleIndex = 1;
            this.colDepartmentName.Width = 117;
            // 
            // colLocationName
            // 
            this.colLocationName.Caption = "Location";
            this.colLocationName.FieldName = "LocationName";
            this.colLocationName.Name = "colLocationName";
            this.colLocationName.OptionsColumn.AllowEdit = false;
            this.colLocationName.OptionsColumn.AllowFocus = false;
            this.colLocationName.OptionsColumn.ReadOnly = true;
            this.colLocationName.Visible = true;
            this.colLocationName.VisibleIndex = 3;
            this.colLocationName.Width = 111;
            // 
            // colBusinessAreaName
            // 
            this.colBusinessAreaName.Caption = "Business Area";
            this.colBusinessAreaName.FieldName = "BusinessAreaName";
            this.colBusinessAreaName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colBusinessAreaName.Name = "colBusinessAreaName";
            this.colBusinessAreaName.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName.Visible = true;
            this.colBusinessAreaName.VisibleIndex = 0;
            this.colBusinessAreaName.Width = 100;
            // 
            // colRegionName
            // 
            this.colRegionName.Caption = "Region";
            this.colRegionName.FieldName = "RegionName";
            this.colRegionName.Name = "colRegionName";
            this.colRegionName.OptionsColumn.AllowEdit = false;
            this.colRegionName.OptionsColumn.AllowFocus = false;
            this.colRegionName.OptionsColumn.ReadOnly = true;
            this.colRegionName.Visible = true;
            this.colRegionName.VisibleIndex = 2;
            this.colRegionName.Width = 102;
            // 
            // colCostCentreCode
            // 
            this.colCostCentreCode.Caption = "Cost Centre Code";
            this.colCostCentreCode.FieldName = "CostCentreCode";
            this.colCostCentreCode.Name = "colCostCentreCode";
            this.colCostCentreCode.OptionsColumn.AllowEdit = false;
            this.colCostCentreCode.OptionsColumn.AllowFocus = false;
            this.colCostCentreCode.OptionsColumn.ReadOnly = true;
            this.colCostCentreCode.Visible = true;
            this.colCostCentreCode.VisibleIndex = 11;
            this.colCostCentreCode.Width = 107;
            // 
            // colEmployeeID6
            // 
            this.colEmployeeID6.Caption = "Employee ID";
            this.colEmployeeID6.FieldName = "EmployeeID";
            this.colEmployeeID6.Name = "colEmployeeID6";
            this.colEmployeeID6.OptionsColumn.AllowEdit = false;
            this.colEmployeeID6.OptionsColumn.AllowFocus = false;
            this.colEmployeeID6.OptionsColumn.ReadOnly = true;
            this.colEmployeeID6.Width = 81;
            // 
            // colLinkedDocumentCount5
            // 
            this.colLinkedDocumentCount5.Caption = "Linked Documents";
            this.colLinkedDocumentCount5.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked;
            this.colLinkedDocumentCount5.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount5.Name = "colLinkedDocumentCount5";
            this.colLinkedDocumentCount5.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount5.Visible = true;
            this.colLinkedDocumentCount5.VisibleIndex = 13;
            this.colLinkedDocumentCount5.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentDeptWorked
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked.Name = "repositoryItemHyperLinkEditLinkedDocumentDeptWorked";
            this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked_OpenLink);
            // 
            // colPositionDescription
            // 
            this.colPositionDescription.Caption = "Position";
            this.colPositionDescription.FieldName = "PositionDescription";
            this.colPositionDescription.Name = "colPositionDescription";
            this.colPositionDescription.OptionsColumn.AllowEdit = false;
            this.colPositionDescription.OptionsColumn.AllowFocus = false;
            this.colPositionDescription.OptionsColumn.ReadOnly = true;
            this.colPositionDescription.Visible = true;
            this.colPositionDescription.VisibleIndex = 8;
            this.colPositionDescription.Width = 150;
            // 
            // xtraTabPageWorkPatterns
            // 
            this.xtraTabPageWorkPatterns.Controls.Add(this.splitContainerControl3);
            this.xtraTabPageWorkPatterns.Name = "xtraTabPageWorkPatterns";
            this.xtraTabPageWorkPatterns.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageWorkPatterns.Text = "Work Patterns";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.gridSplitContainer10);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.gridSplitContainer3);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(1189, 150);
            this.splitContainerControl3.SplitterPosition = 462;
            this.splitContainerControl3.TabIndex = 3;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridSplitContainer10
            // 
            this.gridSplitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer10.Grid = this.gridControlWorkingPatternHeader;
            this.gridSplitContainer10.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer10.Name = "gridSplitContainer10";
            this.gridSplitContainer10.Panel1.Controls.Add(this.gridControlWorkingPatternHeader);
            this.gridSplitContainer10.Size = new System.Drawing.Size(462, 150);
            this.gridSplitContainer10.TabIndex = 0;
            // 
            // gridControlWorkingPatternHeader
            // 
            this.gridControlWorkingPatternHeader.DataSource = this.spHR00154EmployeeWorkingPatternHeadersBindingSource;
            this.gridControlWorkingPatternHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Add Work Pattern from Master Pattern", "add master")});
            this.gridControlWorkingPatternHeader.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlWorkingPatternHeader_EmbeddedNavigator_ButtonClick);
            this.gridControlWorkingPatternHeader.Location = new System.Drawing.Point(0, 0);
            this.gridControlWorkingPatternHeader.MainView = this.gridViewWorkingPatternHeader;
            this.gridControlWorkingPatternHeader.MenuManager = this.barManager1;
            this.gridControlWorkingPatternHeader.Name = "gridControlWorkingPatternHeader";
            this.gridControlWorkingPatternHeader.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemDateEdit1,
            this.repositoryItemMemoExEdit8,
            this.repositoryItemCheckEdit10});
            this.gridControlWorkingPatternHeader.Size = new System.Drawing.Size(462, 150);
            this.gridControlWorkingPatternHeader.TabIndex = 3;
            this.gridControlWorkingPatternHeader.UseEmbeddedNavigator = true;
            this.gridControlWorkingPatternHeader.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWorkingPatternHeader});
            // 
            // spHR00154EmployeeWorkingPatternHeadersBindingSource
            // 
            this.spHR00154EmployeeWorkingPatternHeadersBindingSource.DataMember = "sp_HR_00154_Employee_Working_Pattern_Headers";
            this.spHR00154EmployeeWorkingPatternHeadersBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewWorkingPatternHeader
            // 
            this.gridViewWorkingPatternHeader.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.colEmployeeNumber8,
            this.colEmployeeFirstname2,
            this.colEmployeeSurname5,
            this.colDescription1,
            this.colStartDate1,
            this.colEndDate1,
            this.colRemarks5,
            this.colActive2});
            styleFormatCondition6.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition6.Appearance.Options.UseForeColor = true;
            styleFormatCondition6.ApplyToRow = true;
            styleFormatCondition6.Column = this.colActive2;
            styleFormatCondition6.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition6.Value1 = 0;
            this.gridViewWorkingPatternHeader.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition6});
            this.gridViewWorkingPatternHeader.GridControl = this.gridControlWorkingPatternHeader;
            this.gridViewWorkingPatternHeader.GroupCount = 1;
            this.gridViewWorkingPatternHeader.Name = "gridViewWorkingPatternHeader";
            this.gridViewWorkingPatternHeader.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewWorkingPatternHeader.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewWorkingPatternHeader.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewWorkingPatternHeader.OptionsLayout.StoreAppearance = true;
            this.gridViewWorkingPatternHeader.OptionsLayout.StoreFormatRules = true;
            this.gridViewWorkingPatternHeader.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewWorkingPatternHeader.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewWorkingPatternHeader.OptionsSelection.MultiSelect = true;
            this.gridViewWorkingPatternHeader.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewWorkingPatternHeader.OptionsView.BestFitMaxRowCount = 20;
            this.gridViewWorkingPatternHeader.OptionsView.ColumnAutoWidth = false;
            this.gridViewWorkingPatternHeader.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewWorkingPatternHeader.OptionsView.ShowGroupPanel = false;
            this.gridViewWorkingPatternHeader.OptionsView.ShowViewCaption = true;
            this.gridViewWorkingPatternHeader.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEndDate1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewWorkingPatternHeader.ViewCaption = "Working Pattern Headers";
            this.gridViewWorkingPatternHeader.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewWorkingPatternHeader_CustomDrawCell);
            this.gridViewWorkingPatternHeader.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewWorkingPatternHeader.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewWorkingPatternHeader.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewWorkingPatternHeader.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewWorkingPatternHeader.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewWorkingPatternHeader.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewWorkingPatternHeader.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewWorkingPatternHeader_MouseUp);
            this.gridViewWorkingPatternHeader.DoubleClick += new System.EventHandler(this.gridViewWorkingPatternHeader_DoubleClick);
            this.gridViewWorkingPatternHeader.GotFocus += new System.EventHandler(this.gridViewWorkingPatternHeader_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Working Pattern Header ID";
            this.gridColumn1.FieldName = "WorkingPatternHeaderID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 113;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Employee ID";
            this.gridColumn2.FieldName = "EmployeeID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 81;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Linked To Employee";
            this.gridColumn4.FieldName = "EmployeeName";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            this.gridColumn4.Width = 216;
            // 
            // colEmployeeNumber8
            // 
            this.colEmployeeNumber8.Caption = "Employee #";
            this.colEmployeeNumber8.FieldName = "EmployeeNumber";
            this.colEmployeeNumber8.Name = "colEmployeeNumber8";
            this.colEmployeeNumber8.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber8.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber8.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber8.Width = 78;
            // 
            // colEmployeeFirstname2
            // 
            this.colEmployeeFirstname2.Caption = "Employee Forename";
            this.colEmployeeFirstname2.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname2.Name = "colEmployeeFirstname2";
            this.colEmployeeFirstname2.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname2.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname2.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname2.Width = 153;
            // 
            // colEmployeeSurname5
            // 
            this.colEmployeeSurname5.Caption = "Employee Surname";
            this.colEmployeeSurname5.FieldName = "EmployeeSurname";
            this.colEmployeeSurname5.Name = "colEmployeeSurname5";
            this.colEmployeeSurname5.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname5.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname5.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname5.Width = 146;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Header Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 204;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemDateEdit1;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 1;
            this.colStartDate1.Width = 84;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemDateEdit1;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 2;
            this.colEndDate1.Width = 87;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.OptionsColumn.ReadOnly = true;
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 3;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControlWorkingPattern;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControlWorkingPattern);
            this.gridSplitContainer3.Size = new System.Drawing.Size(721, 150);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControlWorkingPattern
            // 
            this.gridControlWorkingPattern.DataSource = this.sp_HR_00046_Get_Employee_Working_PatternsBindingSource;
            this.gridControlWorkingPattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlWorkingPattern.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record(s)", "view")});
            this.gridControlWorkingPattern.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlWorkingPattern_EmbeddedNavigator_ButtonClick);
            this.gridControlWorkingPattern.Location = new System.Drawing.Point(0, 0);
            this.gridControlWorkingPattern.MainView = this.gridViewWorkingPattern;
            this.gridControlWorkingPattern.MenuManager = this.barManager1;
            this.gridControlWorkingPattern.Name = "gridControlWorkingPattern";
            this.gridControlWorkingPattern.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTime,
            this.repositoryItemTextEditMinutes,
            this.repositoryItemCheckEdit9,
            this.repositoryItemTextEditWeekNumber,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemMemoExEdit12});
            this.gridControlWorkingPattern.Size = new System.Drawing.Size(721, 150);
            this.gridControlWorkingPattern.TabIndex = 2;
            this.gridControlWorkingPattern.UseEmbeddedNavigator = true;
            this.gridControlWorkingPattern.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewWorkingPattern});
            // 
            // sp_HR_00046_Get_Employee_Working_PatternsBindingSource
            // 
            this.sp_HR_00046_Get_Employee_Working_PatternsBindingSource.DataMember = "sp_HR_00046_Get_Employee_Working_Patterns";
            this.sp_HR_00046_Get_Employee_Working_PatternsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewWorkingPattern
            // 
            this.gridViewWorkingPattern.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colWorkingPatternID,
            this.colEmployeeID7,
            this.colEmployeeName3,
            this.colWeekNumber,
            this.colDayOfWeek,
            this.colDayOfTheWeek,
            this.colStartTime,
            this.colEndTime,
            this.colBreakLength,
            this.colPaidForBreaks,
            this.colCalculatedHours,
            this.colEmployeeDepartmentWorkedID,
            this.colRemarks4,
            this.colDepartmentName1,
            this.colLocationName1,
            this.colBusinessAreaName1,
            this.colRegionName1,
            this.colHeaderDates,
            this.colHeaderEndDate,
            this.colHeaderStartDate,
            this.colHeaderDescription,
            this.colDummyShift});
            styleFormatCondition7.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition7.Appearance.Options.UseForeColor = true;
            styleFormatCondition7.ApplyToRow = true;
            styleFormatCondition7.Column = this.colDummyShift;
            styleFormatCondition7.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition7.Value1 = 1;
            this.gridViewWorkingPattern.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition7});
            this.gridViewWorkingPattern.GridControl = this.gridControlWorkingPattern;
            this.gridViewWorkingPattern.GroupCount = 2;
            this.gridViewWorkingPattern.Name = "gridViewWorkingPattern";
            this.gridViewWorkingPattern.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewWorkingPattern.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewWorkingPattern.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewWorkingPattern.OptionsLayout.StoreAppearance = true;
            this.gridViewWorkingPattern.OptionsLayout.StoreFormatRules = true;
            this.gridViewWorkingPattern.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewWorkingPattern.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewWorkingPattern.OptionsSelection.MultiSelect = true;
            this.gridViewWorkingPattern.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewWorkingPattern.OptionsView.BestFitMaxRowCount = 20;
            this.gridViewWorkingPattern.OptionsView.ColumnAutoWidth = false;
            this.gridViewWorkingPattern.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewWorkingPattern.OptionsView.ShowGroupPanel = false;
            this.gridViewWorkingPattern.OptionsView.ShowViewCaption = true;
            this.gridViewWorkingPattern.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHeaderDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colWeekNumber, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDayOfWeek, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartTime, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewWorkingPattern.ViewCaption = "Working Patterns Linked to Selected Header(s)";
            this.gridViewWorkingPattern.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewWorkingPattern.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewWorkingPattern.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewWorkingPattern.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewWorkingPattern.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewWorkingPattern.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewWorkingPattern.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewWorkingPattern_MouseUp);
            this.gridViewWorkingPattern.DoubleClick += new System.EventHandler(this.gridViewWorkingPattern_DoubleClick);
            this.gridViewWorkingPattern.GotFocus += new System.EventHandler(this.gridViewWorkingPattern_GotFocus);
            // 
            // colWorkingPatternID
            // 
            this.colWorkingPatternID.Caption = "Working Pattern ID";
            this.colWorkingPatternID.FieldName = "WorkingPatternID";
            this.colWorkingPatternID.Name = "colWorkingPatternID";
            this.colWorkingPatternID.OptionsColumn.AllowEdit = false;
            this.colWorkingPatternID.OptionsColumn.AllowFocus = false;
            this.colWorkingPatternID.OptionsColumn.ReadOnly = true;
            this.colWorkingPatternID.Width = 113;
            // 
            // colEmployeeID7
            // 
            this.colEmployeeID7.Caption = "Working Pattern Header ID";
            this.colEmployeeID7.FieldName = "WorkingPatternHeaderID";
            this.colEmployeeID7.Name = "colEmployeeID7";
            this.colEmployeeID7.OptionsColumn.AllowEdit = false;
            this.colEmployeeID7.OptionsColumn.AllowFocus = false;
            this.colEmployeeID7.OptionsColumn.ReadOnly = true;
            this.colEmployeeID7.Width = 81;
            // 
            // colEmployeeName3
            // 
            this.colEmployeeName3.Caption = "Linked To Employee";
            this.colEmployeeName3.FieldName = "EmployeeName";
            this.colEmployeeName3.Name = "colEmployeeName3";
            this.colEmployeeName3.OptionsColumn.AllowEdit = false;
            this.colEmployeeName3.OptionsColumn.AllowFocus = false;
            this.colEmployeeName3.OptionsColumn.ReadOnly = true;
            this.colEmployeeName3.Visible = true;
            this.colEmployeeName3.VisibleIndex = 12;
            this.colEmployeeName3.Width = 216;
            // 
            // colWeekNumber
            // 
            this.colWeekNumber.Caption = "Week Number";
            this.colWeekNumber.ColumnEdit = this.repositoryItemTextEditWeekNumber;
            this.colWeekNumber.FieldName = "WeekNumber";
            this.colWeekNumber.Name = "colWeekNumber";
            this.colWeekNumber.OptionsColumn.AllowEdit = false;
            this.colWeekNumber.OptionsColumn.AllowFocus = false;
            this.colWeekNumber.OptionsColumn.ReadOnly = true;
            this.colWeekNumber.Visible = true;
            this.colWeekNumber.VisibleIndex = 0;
            this.colWeekNumber.Width = 101;
            // 
            // repositoryItemTextEditWeekNumber
            // 
            this.repositoryItemTextEditWeekNumber.AutoHeight = false;
            this.repositoryItemTextEditWeekNumber.Mask.EditMask = "Week ####0";
            this.repositoryItemTextEditWeekNumber.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditWeekNumber.Mask.ShowPlaceHolders = false;
            this.repositoryItemTextEditWeekNumber.Name = "repositoryItemTextEditWeekNumber";
            // 
            // colDayOfWeek
            // 
            this.colDayOfWeek.Caption = "Day Of Week ID";
            this.colDayOfWeek.FieldName = "DayOfWeek";
            this.colDayOfWeek.Name = "colDayOfWeek";
            this.colDayOfWeek.OptionsColumn.AllowEdit = false;
            this.colDayOfWeek.OptionsColumn.AllowFocus = false;
            this.colDayOfWeek.OptionsColumn.ReadOnly = true;
            this.colDayOfWeek.Width = 99;
            // 
            // colDayOfTheWeek
            // 
            this.colDayOfTheWeek.Caption = "Day Of Week";
            this.colDayOfTheWeek.FieldName = "DayOfTheWeek";
            this.colDayOfTheWeek.Name = "colDayOfTheWeek";
            this.colDayOfTheWeek.OptionsColumn.AllowEdit = false;
            this.colDayOfTheWeek.OptionsColumn.AllowFocus = false;
            this.colDayOfTheWeek.OptionsColumn.ReadOnly = true;
            this.colDayOfTheWeek.Visible = true;
            this.colDayOfTheWeek.VisibleIndex = 1;
            this.colDayOfTheWeek.Width = 91;
            // 
            // colStartTime
            // 
            this.colStartTime.Caption = "Start Time";
            this.colStartTime.ColumnEdit = this.repositoryItemTime;
            this.colStartTime.FieldName = "StartTime";
            this.colStartTime.Name = "colStartTime";
            this.colStartTime.OptionsColumn.AllowEdit = false;
            this.colStartTime.OptionsColumn.AllowFocus = false;
            this.colStartTime.OptionsColumn.ReadOnly = true;
            this.colStartTime.Visible = true;
            this.colStartTime.VisibleIndex = 2;
            // 
            // repositoryItemTime
            // 
            this.repositoryItemTime.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.True;
            this.repositoryItemTime.AutoHeight = false;
            this.repositoryItemTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTime.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemTime.CalendarTimeProperties.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTime.Mask.EditMask = "HH:mm";
            this.repositoryItemTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTime.Name = "repositoryItemTime";
            // 
            // colEndTime
            // 
            this.colEndTime.Caption = "End Date";
            this.colEndTime.ColumnEdit = this.repositoryItemTime;
            this.colEndTime.FieldName = "EndTime";
            this.colEndTime.Name = "colEndTime";
            this.colEndTime.OptionsColumn.AllowEdit = false;
            this.colEndTime.OptionsColumn.AllowFocus = false;
            this.colEndTime.OptionsColumn.ReadOnly = true;
            this.colEndTime.Visible = true;
            this.colEndTime.VisibleIndex = 3;
            // 
            // colBreakLength
            // 
            this.colBreakLength.Caption = "Break Length";
            this.colBreakLength.ColumnEdit = this.repositoryItemTextEditMinutes;
            this.colBreakLength.FieldName = "BreakLength";
            this.colBreakLength.Name = "colBreakLength";
            this.colBreakLength.OptionsColumn.AllowEdit = false;
            this.colBreakLength.OptionsColumn.AllowFocus = false;
            this.colBreakLength.OptionsColumn.ReadOnly = true;
            this.colBreakLength.Visible = true;
            this.colBreakLength.VisibleIndex = 4;
            this.colBreakLength.Width = 84;
            // 
            // repositoryItemTextEditMinutes
            // 
            this.repositoryItemTextEditMinutes.AutoHeight = false;
            this.repositoryItemTextEditMinutes.Mask.EditMask = "####0 Minutes";
            this.repositoryItemTextEditMinutes.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMinutes.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMinutes.Name = "repositoryItemTextEditMinutes";
            // 
            // colPaidForBreaks
            // 
            this.colPaidForBreaks.Caption = "Paid For Breaks";
            this.colPaidForBreaks.ColumnEdit = this.repositoryItemTime;
            this.colPaidForBreaks.FieldName = "PaidForBreaks";
            this.colPaidForBreaks.Name = "colPaidForBreaks";
            this.colPaidForBreaks.OptionsColumn.AllowEdit = false;
            this.colPaidForBreaks.OptionsColumn.AllowFocus = false;
            this.colPaidForBreaks.OptionsColumn.ReadOnly = true;
            this.colPaidForBreaks.Visible = true;
            this.colPaidForBreaks.VisibleIndex = 5;
            this.colPaidForBreaks.Width = 95;
            // 
            // colCalculatedHours
            // 
            this.colCalculatedHours.Caption = "Calculated Hours";
            this.colCalculatedHours.FieldName = "CalculatedHours";
            this.colCalculatedHours.Name = "colCalculatedHours";
            this.colCalculatedHours.OptionsColumn.AllowEdit = false;
            this.colCalculatedHours.OptionsColumn.AllowFocus = false;
            this.colCalculatedHours.OptionsColumn.ReadOnly = true;
            this.colCalculatedHours.Visible = true;
            this.colCalculatedHours.VisibleIndex = 6;
            this.colCalculatedHours.Width = 102;
            // 
            // colEmployeeDepartmentWorkedID
            // 
            this.colEmployeeDepartmentWorkedID.Caption = "Department Worked In ID";
            this.colEmployeeDepartmentWorkedID.FieldName = "EmployeeDepartmentWorkedID";
            this.colEmployeeDepartmentWorkedID.Name = "colEmployeeDepartmentWorkedID";
            this.colEmployeeDepartmentWorkedID.OptionsColumn.AllowEdit = false;
            this.colEmployeeDepartmentWorkedID.OptionsColumn.AllowFocus = false;
            this.colEmployeeDepartmentWorkedID.OptionsColumn.ReadOnly = true;
            this.colEmployeeDepartmentWorkedID.Width = 150;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit12;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 7;
            // 
            // repositoryItemMemoExEdit12
            // 
            this.repositoryItemMemoExEdit12.AutoHeight = false;
            this.repositoryItemMemoExEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit12.Name = "repositoryItemMemoExEdit12";
            this.repositoryItemMemoExEdit12.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit12.ShowIcon = false;
            // 
            // colDepartmentName1
            // 
            this.colDepartmentName1.Caption = "Department";
            this.colDepartmentName1.FieldName = "DepartmentName";
            this.colDepartmentName1.Name = "colDepartmentName1";
            this.colDepartmentName1.OptionsColumn.AllowEdit = false;
            this.colDepartmentName1.OptionsColumn.AllowFocus = false;
            this.colDepartmentName1.OptionsColumn.ReadOnly = true;
            this.colDepartmentName1.Visible = true;
            this.colDepartmentName1.VisibleIndex = 8;
            this.colDepartmentName1.Width = 100;
            // 
            // colLocationName1
            // 
            this.colLocationName1.Caption = "Location";
            this.colLocationName1.FieldName = "LocationName";
            this.colLocationName1.Name = "colLocationName1";
            this.colLocationName1.OptionsColumn.AllowEdit = false;
            this.colLocationName1.OptionsColumn.AllowFocus = false;
            this.colLocationName1.OptionsColumn.ReadOnly = true;
            this.colLocationName1.Visible = true;
            this.colLocationName1.VisibleIndex = 9;
            this.colLocationName1.Width = 125;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.Visible = true;
            this.colBusinessAreaName1.VisibleIndex = 10;
            this.colBusinessAreaName1.Width = 115;
            // 
            // colRegionName1
            // 
            this.colRegionName1.Caption = "Region";
            this.colRegionName1.FieldName = "RegionName";
            this.colRegionName1.Name = "colRegionName1";
            this.colRegionName1.OptionsColumn.AllowEdit = false;
            this.colRegionName1.OptionsColumn.AllowFocus = false;
            this.colRegionName1.OptionsColumn.ReadOnly = true;
            this.colRegionName1.Visible = true;
            this.colRegionName1.VisibleIndex = 11;
            this.colRegionName1.Width = 90;
            // 
            // colHeaderDates
            // 
            this.colHeaderDates.FieldName = "HeaderDates";
            this.colHeaderDates.Name = "colHeaderDates";
            this.colHeaderDates.OptionsColumn.AllowEdit = false;
            this.colHeaderDates.OptionsColumn.AllowFocus = false;
            this.colHeaderDates.OptionsColumn.ReadOnly = true;
            this.colHeaderDates.Width = 138;
            // 
            // colHeaderEndDate
            // 
            this.colHeaderEndDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colHeaderEndDate.FieldName = "HeaderEndDate";
            this.colHeaderEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHeaderEndDate.Name = "colHeaderEndDate";
            this.colHeaderEndDate.OptionsColumn.AllowEdit = false;
            this.colHeaderEndDate.OptionsColumn.AllowFocus = false;
            this.colHeaderEndDate.OptionsColumn.ReadOnly = true;
            this.colHeaderEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHeaderEndDate.Visible = true;
            this.colHeaderEndDate.VisibleIndex = 13;
            this.colHeaderEndDate.Width = 103;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colHeaderStartDate
            // 
            this.colHeaderStartDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colHeaderStartDate.FieldName = "HeaderStartDate";
            this.colHeaderStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHeaderStartDate.Name = "colHeaderStartDate";
            this.colHeaderStartDate.OptionsColumn.AllowEdit = false;
            this.colHeaderStartDate.OptionsColumn.AllowFocus = false;
            this.colHeaderStartDate.OptionsColumn.ReadOnly = true;
            this.colHeaderStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHeaderStartDate.Visible = true;
            this.colHeaderStartDate.VisibleIndex = 12;
            this.colHeaderStartDate.Width = 109;
            // 
            // colHeaderDescription
            // 
            this.colHeaderDescription.Caption = "Header Description";
            this.colHeaderDescription.FieldName = "HeaderDescription";
            this.colHeaderDescription.Name = "colHeaderDescription";
            this.colHeaderDescription.OptionsColumn.AllowEdit = false;
            this.colHeaderDescription.OptionsColumn.AllowFocus = false;
            this.colHeaderDescription.OptionsColumn.ReadOnly = true;
            this.colHeaderDescription.Visible = true;
            this.colHeaderDescription.VisibleIndex = 16;
            this.colHeaderDescription.Width = 239;
            // 
            // xtraTabPageShares
            // 
            this.xtraTabPageShares.Controls.Add(this.gridSplitContainer17);
            this.xtraTabPageShares.Name = "xtraTabPageShares";
            this.xtraTabPageShares.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageShares.Text = "Shares";
            // 
            // gridSplitContainer17
            // 
            this.gridSplitContainer17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer17.Grid = this.gridControlShares;
            this.gridSplitContainer17.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer17.Name = "gridSplitContainer17";
            this.gridSplitContainer17.Panel1.Controls.Add(this.gridControlShares);
            this.gridSplitContainer17.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer17.TabIndex = 0;
            // 
            // gridControlShares
            // 
            this.gridControlShares.DataSource = this.spHR00224EmployeeSharesBindingSource;
            this.gridControlShares.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlShares.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlShares.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlShares.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlShares.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlShares.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlShares_EmbeddedNavigator_ButtonClick);
            this.gridControlShares.Location = new System.Drawing.Point(0, 0);
            this.gridControlShares.MainView = this.gridViewShares;
            this.gridControlShares.MenuManager = this.barManager1;
            this.gridControlShares.Name = "gridControlShares";
            this.gridControlShares.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit19,
            this.repositoryItemCheckEdit16,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEditShareUnitsUnknown,
            this.repositoryItemHyperLinkEditSharesLinkedDocuments,
            this.repositoryItemTextEditShareUnitsCurrency,
            this.repositoryItemTextEditShareUnitsUnits,
            this.repositoryItemTextEditShareUnitsPercentage});
            this.gridControlShares.Size = new System.Drawing.Size(1189, 150);
            this.gridControlShares.TabIndex = 1;
            this.gridControlShares.UseEmbeddedNavigator = true;
            this.gridControlShares.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewShares});
            // 
            // spHR00224EmployeeSharesBindingSource
            // 
            this.spHR00224EmployeeSharesBindingSource.DataMember = "sp_HR_00224_Employee_Shares";
            this.spHR00224EmployeeSharesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewShares
            // 
            this.gridViewShares.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSharesID,
            this.colEmployeeID13,
            this.colShareTypeID,
            this.colActive3,
            this.colDateReceived1,
            this.colShareUnits,
            this.colShareUnitDescriptorID,
            this.colUnitValue,
            this.colRemarks11,
            this.colEmployeeName9,
            this.colEmployeeSurname10,
            this.colEmployeeFirstname8,
            this.colEmployeeNumber13,
            this.colShareType,
            this.colShareUnitDescriptor,
            this.colLinkedDocumentCount11});
            styleFormatCondition8.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition8.Appearance.Options.UseForeColor = true;
            styleFormatCondition8.ApplyToRow = true;
            styleFormatCondition8.Column = this.colActive3;
            styleFormatCondition8.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition8.Value1 = 0;
            this.gridViewShares.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition8});
            this.gridViewShares.GridControl = this.gridControlShares;
            this.gridViewShares.GroupCount = 1;
            this.gridViewShares.Name = "gridViewShares";
            this.gridViewShares.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewShares.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewShares.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewShares.OptionsLayout.StoreAppearance = true;
            this.gridViewShares.OptionsLayout.StoreFormatRules = true;
            this.gridViewShares.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewShares.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewShares.OptionsSelection.MultiSelect = true;
            this.gridViewShares.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewShares.OptionsView.BestFitMaxRowCount = 5;
            this.gridViewShares.OptionsView.ColumnAutoWidth = false;
            this.gridViewShares.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewShares.OptionsView.ShowGroupPanel = false;
            this.gridViewShares.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName9, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateReceived1, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewShares.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewShares_CustomDrawCell);
            this.gridViewShares.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewShares_CustomRowCellEdit);
            this.gridViewShares.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewShares.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewShares.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewShares.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewShares_ShowingEditor);
            this.gridViewShares.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewShares.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewShares.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewShares.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewShares_MouseUp);
            this.gridViewShares.DoubleClick += new System.EventHandler(this.gridViewShares_DoubleClick);
            this.gridViewShares.GotFocus += new System.EventHandler(this.gridViewShares_GotFocus);
            // 
            // colSharesID
            // 
            this.colSharesID.Caption = "Shares ID";
            this.colSharesID.FieldName = "SharesID";
            this.colSharesID.Name = "colSharesID";
            this.colSharesID.OptionsColumn.AllowEdit = false;
            this.colSharesID.OptionsColumn.AllowFocus = false;
            this.colSharesID.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeID13
            // 
            this.colEmployeeID13.Caption = "Employee ID";
            this.colEmployeeID13.FieldName = "EmployeeID";
            this.colEmployeeID13.Name = "colEmployeeID13";
            this.colEmployeeID13.OptionsColumn.AllowEdit = false;
            this.colEmployeeID13.OptionsColumn.AllowFocus = false;
            this.colEmployeeID13.OptionsColumn.ReadOnly = true;
            this.colEmployeeID13.Width = 81;
            // 
            // colShareTypeID
            // 
            this.colShareTypeID.Caption = "Share Type ID";
            this.colShareTypeID.FieldName = "ShareTypeID";
            this.colShareTypeID.Name = "colShareTypeID";
            this.colShareTypeID.OptionsColumn.AllowEdit = false;
            this.colShareTypeID.OptionsColumn.AllowFocus = false;
            this.colShareTypeID.OptionsColumn.ReadOnly = true;
            this.colShareTypeID.Width = 90;
            // 
            // colDateReceived1
            // 
            this.colDateReceived1.Caption = "Date Received";
            this.colDateReceived1.ColumnEdit = this.repositoryItemTextEdit4;
            this.colDateReceived1.FieldName = "DateReceived";
            this.colDateReceived1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateReceived1.Name = "colDateReceived1";
            this.colDateReceived1.OptionsColumn.AllowEdit = false;
            this.colDateReceived1.OptionsColumn.AllowFocus = false;
            this.colDateReceived1.OptionsColumn.ReadOnly = true;
            this.colDateReceived1.Visible = true;
            this.colDateReceived1.VisibleIndex = 0;
            this.colDateReceived1.Width = 108;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Mask.EditMask = "d";
            this.repositoryItemTextEdit4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEdit4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // colShareUnits
            // 
            this.colShareUnits.Caption = "Share Units";
            this.colShareUnits.ColumnEdit = this.repositoryItemTextEditShareUnitsUnits;
            this.colShareUnits.FieldName = "ShareUnits";
            this.colShareUnits.Name = "colShareUnits";
            this.colShareUnits.OptionsColumn.AllowEdit = false;
            this.colShareUnits.OptionsColumn.AllowFocus = false;
            this.colShareUnits.OptionsColumn.ReadOnly = true;
            this.colShareUnits.Visible = true;
            this.colShareUnits.VisibleIndex = 3;
            this.colShareUnits.Width = 103;
            // 
            // repositoryItemTextEditShareUnitsUnits
            // 
            this.repositoryItemTextEditShareUnitsUnits.AutoHeight = false;
            this.repositoryItemTextEditShareUnitsUnits.Mask.EditMask = "######0.00 Units";
            this.repositoryItemTextEditShareUnitsUnits.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditShareUnitsUnits.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShareUnitsUnits.Name = "repositoryItemTextEditShareUnitsUnits";
            // 
            // colShareUnitDescriptorID
            // 
            this.colShareUnitDescriptorID.Caption = "Unit Descriptor ID";
            this.colShareUnitDescriptorID.FieldName = "ShareUnitDescriptorID";
            this.colShareUnitDescriptorID.Name = "colShareUnitDescriptorID";
            this.colShareUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colShareUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colShareUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colShareUnitDescriptorID.Width = 106;
            // 
            // colUnitValue
            // 
            this.colUnitValue.AppearanceCell.Options.UseTextOptions = true;
            this.colUnitValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colUnitValue.Caption = "Unit Value";
            this.colUnitValue.ColumnEdit = this.repositoryItemTextEditShareUnitsCurrency;
            this.colUnitValue.FieldName = "UnitValue";
            this.colUnitValue.Name = "colUnitValue";
            this.colUnitValue.OptionsColumn.AllowEdit = false;
            this.colUnitValue.OptionsColumn.AllowFocus = false;
            this.colUnitValue.OptionsColumn.ReadOnly = true;
            this.colUnitValue.Visible = true;
            this.colUnitValue.VisibleIndex = 4;
            this.colUnitValue.Width = 112;
            // 
            // repositoryItemTextEditShareUnitsCurrency
            // 
            this.repositoryItemTextEditShareUnitsCurrency.AutoHeight = false;
            this.repositoryItemTextEditShareUnitsCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditShareUnitsCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditShareUnitsCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShareUnitsCurrency.Name = "repositoryItemTextEditShareUnitsCurrency";
            // 
            // colRemarks11
            // 
            this.colRemarks11.Caption = "Remarks";
            this.colRemarks11.ColumnEdit = this.repositoryItemMemoExEdit19;
            this.colRemarks11.FieldName = "Remarks";
            this.colRemarks11.Name = "colRemarks11";
            this.colRemarks11.OptionsColumn.ReadOnly = true;
            this.colRemarks11.Visible = true;
            this.colRemarks11.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit19
            // 
            this.repositoryItemMemoExEdit19.AutoHeight = false;
            this.repositoryItemMemoExEdit19.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit19.Name = "repositoryItemMemoExEdit19";
            this.repositoryItemMemoExEdit19.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit19.ShowIcon = false;
            // 
            // colEmployeeName9
            // 
            this.colEmployeeName9.Caption = "Linked To Employee";
            this.colEmployeeName9.FieldName = "EmployeeName";
            this.colEmployeeName9.Name = "colEmployeeName9";
            this.colEmployeeName9.OptionsColumn.AllowEdit = false;
            this.colEmployeeName9.OptionsColumn.AllowFocus = false;
            this.colEmployeeName9.OptionsColumn.ReadOnly = true;
            this.colEmployeeName9.Visible = true;
            this.colEmployeeName9.VisibleIndex = 7;
            this.colEmployeeName9.Width = 229;
            // 
            // colEmployeeSurname10
            // 
            this.colEmployeeSurname10.Caption = "Surname";
            this.colEmployeeSurname10.FieldName = "EmployeeSurname";
            this.colEmployeeSurname10.Name = "colEmployeeSurname10";
            this.colEmployeeSurname10.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname10.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname10.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname10.Width = 147;
            // 
            // colEmployeeFirstname8
            // 
            this.colEmployeeFirstname8.Caption = "Forename";
            this.colEmployeeFirstname8.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname8.Name = "colEmployeeFirstname8";
            this.colEmployeeFirstname8.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname8.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname8.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname8.Width = 157;
            // 
            // colEmployeeNumber13
            // 
            this.colEmployeeNumber13.Caption = "Employee #";
            this.colEmployeeNumber13.FieldName = "EmployeeNumber";
            this.colEmployeeNumber13.Name = "colEmployeeNumber13";
            this.colEmployeeNumber13.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber13.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber13.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber13.Width = 121;
            // 
            // colShareType
            // 
            this.colShareType.Caption = "Share Type";
            this.colShareType.FieldName = "ShareType";
            this.colShareType.Name = "colShareType";
            this.colShareType.OptionsColumn.AllowEdit = false;
            this.colShareType.OptionsColumn.AllowFocus = false;
            this.colShareType.OptionsColumn.ReadOnly = true;
            this.colShareType.Visible = true;
            this.colShareType.VisibleIndex = 1;
            this.colShareType.Width = 238;
            // 
            // colShareUnitDescriptor
            // 
            this.colShareUnitDescriptor.Caption = "Unit Descriptor";
            this.colShareUnitDescriptor.FieldName = "ShareUnitDescriptor";
            this.colShareUnitDescriptor.Name = "colShareUnitDescriptor";
            this.colShareUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colShareUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colShareUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colShareUnitDescriptor.Width = 92;
            // 
            // colLinkedDocumentCount11
            // 
            this.colLinkedDocumentCount11.Caption = "Linked Documents";
            this.colLinkedDocumentCount11.ColumnEdit = this.repositoryItemHyperLinkEditSharesLinkedDocuments;
            this.colLinkedDocumentCount11.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount11.Name = "colLinkedDocumentCount11";
            this.colLinkedDocumentCount11.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount11.Visible = true;
            this.colLinkedDocumentCount11.VisibleIndex = 6;
            this.colLinkedDocumentCount11.Width = 107;
            // 
            // repositoryItemHyperLinkEditSharesLinkedDocuments
            // 
            this.repositoryItemHyperLinkEditSharesLinkedDocuments.AutoHeight = false;
            this.repositoryItemHyperLinkEditSharesLinkedDocuments.Name = "repositoryItemHyperLinkEditSharesLinkedDocuments";
            this.repositoryItemHyperLinkEditSharesLinkedDocuments.SingleClick = true;
            this.repositoryItemHyperLinkEditSharesLinkedDocuments.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditSharesLinkedDocuments_OpenLink);
            // 
            // repositoryItemTextEditShareUnitsUnknown
            // 
            this.repositoryItemTextEditShareUnitsUnknown.AutoHeight = false;
            this.repositoryItemTextEditShareUnitsUnknown.Mask.EditMask = "######0.00";
            this.repositoryItemTextEditShareUnitsUnknown.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditShareUnitsUnknown.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShareUnitsUnknown.Name = "repositoryItemTextEditShareUnitsUnknown";
            // 
            // repositoryItemTextEditShareUnitsPercentage
            // 
            this.repositoryItemTextEditShareUnitsPercentage.AutoHeight = false;
            this.repositoryItemTextEditShareUnitsPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditShareUnitsPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditShareUnitsPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShareUnitsPercentage.Name = "repositoryItemTextEditShareUnitsPercentage";
            // 
            // xtraTabPageBonuses
            // 
            this.xtraTabPageBonuses.Controls.Add(this.gridSplitContainer4);
            this.xtraTabPageBonuses.Name = "xtraTabPageBonuses";
            this.xtraTabPageBonuses.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageBonuses.Text = "Bonuses";
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControlBonuses;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControlBonuses);
            this.gridSplitContainer4.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControlBonuses
            // 
            this.gridControlBonuses.DataSource = this.spHR00172EmployeeBonusesBindingSource;
            this.gridControlBonuses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlBonuses.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlBonuses.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlBonuses.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlBonuses_EmbeddedNavigator_ButtonClick);
            this.gridControlBonuses.Location = new System.Drawing.Point(0, 0);
            this.gridControlBonuses.MainView = this.gridViewBonuses;
            this.gridControlBonuses.MenuManager = this.barManager1;
            this.gridControlBonuses.Name = "gridControlBonuses";
            this.gridControlBonuses.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemHyperLinkEditBonusLinkedDocuments});
            this.gridControlBonuses.Size = new System.Drawing.Size(1189, 150);
            this.gridControlBonuses.TabIndex = 0;
            this.gridControlBonuses.UseEmbeddedNavigator = true;
            this.gridControlBonuses.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBonuses});
            // 
            // spHR00172EmployeeBonusesBindingSource
            // 
            this.spHR00172EmployeeBonusesBindingSource.DataMember = "sp_HR_00172_Employee_Bonuses";
            this.spHR00172EmployeeBonusesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewBonuses
            // 
            this.gridViewBonuses.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBonusID,
            this.colEmployeeID3,
            this.colGuaranteeBonus,
            this.colGuaranteeBonusDueDate,
            this.colGuaranteeBonusAmount,
            this.colBonusDatePaid,
            this.colBonusAmountPaid,
            this.colRemarks6,
            this.colEmployeeName4,
            this.colEmployeeSurname,
            this.colEmployeeFirstname3,
            this.colEmployeeNumber2,
            this.colRecordStatus,
            this.colLinkedDocumentCount4});
            styleFormatCondition9.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition9.Appearance.Options.UseForeColor = true;
            styleFormatCondition9.ApplyToRow = true;
            styleFormatCondition9.Column = this.colRecordStatus;
            styleFormatCondition9.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition9.Value1 = 0;
            this.gridViewBonuses.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition9});
            this.gridViewBonuses.GridControl = this.gridControlBonuses;
            this.gridViewBonuses.GroupCount = 1;
            this.gridViewBonuses.Name = "gridViewBonuses";
            this.gridViewBonuses.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewBonuses.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewBonuses.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewBonuses.OptionsLayout.StoreAppearance = true;
            this.gridViewBonuses.OptionsLayout.StoreFormatRules = true;
            this.gridViewBonuses.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewBonuses.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewBonuses.OptionsSelection.MultiSelect = true;
            this.gridViewBonuses.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewBonuses.OptionsView.BestFitMaxRowCount = 5;
            this.gridViewBonuses.OptionsView.ColumnAutoWidth = false;
            this.gridViewBonuses.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewBonuses.OptionsView.ShowGroupPanel = false;
            this.gridViewBonuses.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGuaranteeBonusDueDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewBonuses.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewBonuses_CustomDrawCell);
            this.gridViewBonuses.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewBonuses_CustomRowCellEdit);
            this.gridViewBonuses.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewBonuses.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewBonuses.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewBonuses.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewBonuses_ShowingEditor);
            this.gridViewBonuses.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewBonuses.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewBonuses.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewBonuses.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewBonuses_MouseUp);
            this.gridViewBonuses.DoubleClick += new System.EventHandler(this.gridViewBonuses_DoubleClick);
            this.gridViewBonuses.GotFocus += new System.EventHandler(this.gridViewBonuses_GotFocus);
            // 
            // colBonusID
            // 
            this.colBonusID.Caption = "Bonus ID";
            this.colBonusID.FieldName = "BonusID";
            this.colBonusID.Name = "colBonusID";
            this.colBonusID.OptionsColumn.AllowEdit = false;
            this.colBonusID.OptionsColumn.AllowFocus = false;
            this.colBonusID.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeID3
            // 
            this.colEmployeeID3.Caption = "Employee ID";
            this.colEmployeeID3.FieldName = "EmployeeID";
            this.colEmployeeID3.Name = "colEmployeeID3";
            this.colEmployeeID3.OptionsColumn.AllowEdit = false;
            this.colEmployeeID3.OptionsColumn.AllowFocus = false;
            this.colEmployeeID3.OptionsColumn.ReadOnly = true;
            this.colEmployeeID3.Width = 81;
            // 
            // colGuaranteeBonus
            // 
            this.colGuaranteeBonus.Caption = "Guarantee Bonus";
            this.colGuaranteeBonus.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colGuaranteeBonus.FieldName = "GuaranteeBonus";
            this.colGuaranteeBonus.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colGuaranteeBonus.Name = "colGuaranteeBonus";
            this.colGuaranteeBonus.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonus.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonus.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colGuaranteeBonus.Visible = true;
            this.colGuaranteeBonus.VisibleIndex = 1;
            this.colGuaranteeBonus.Width = 104;
            // 
            // colGuaranteeBonusDueDate
            // 
            this.colGuaranteeBonusDueDate.Caption = "Bonus Due Date";
            this.colGuaranteeBonusDueDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colGuaranteeBonusDueDate.FieldName = "BonusDueDate";
            this.colGuaranteeBonusDueDate.Name = "colGuaranteeBonusDueDate";
            this.colGuaranteeBonusDueDate.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonusDueDate.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonusDueDate.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonusDueDate.Visible = true;
            this.colGuaranteeBonusDueDate.VisibleIndex = 0;
            this.colGuaranteeBonusDueDate.Width = 126;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colGuaranteeBonusAmount
            // 
            this.colGuaranteeBonusAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colGuaranteeBonusAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colGuaranteeBonusAmount.Caption = "Guarantee Bonus Amount";
            this.colGuaranteeBonusAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colGuaranteeBonusAmount.FieldName = "GuaranteeBonusAmount";
            this.colGuaranteeBonusAmount.Name = "colGuaranteeBonusAmount";
            this.colGuaranteeBonusAmount.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonusAmount.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonusAmount.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonusAmount.Visible = true;
            this.colGuaranteeBonusAmount.VisibleIndex = 2;
            this.colGuaranteeBonusAmount.Width = 144;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colBonusDatePaid
            // 
            this.colBonusDatePaid.Caption = "Date Paid";
            this.colBonusDatePaid.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colBonusDatePaid.FieldName = "BonusDatePaid";
            this.colBonusDatePaid.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colBonusDatePaid.Name = "colBonusDatePaid";
            this.colBonusDatePaid.OptionsColumn.AllowEdit = false;
            this.colBonusDatePaid.OptionsColumn.AllowFocus = false;
            this.colBonusDatePaid.OptionsColumn.ReadOnly = true;
            this.colBonusDatePaid.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colBonusDatePaid.Visible = true;
            this.colBonusDatePaid.VisibleIndex = 3;
            // 
            // colBonusAmountPaid
            // 
            this.colBonusAmountPaid.AppearanceCell.Options.UseTextOptions = true;
            this.colBonusAmountPaid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colBonusAmountPaid.Caption = "Amount Paid";
            this.colBonusAmountPaid.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colBonusAmountPaid.FieldName = "BonusAmountPaid";
            this.colBonusAmountPaid.Name = "colBonusAmountPaid";
            this.colBonusAmountPaid.OptionsColumn.AllowEdit = false;
            this.colBonusAmountPaid.OptionsColumn.AllowFocus = false;
            this.colBonusAmountPaid.OptionsColumn.ReadOnly = true;
            this.colBonusAmountPaid.Visible = true;
            this.colBonusAmountPaid.VisibleIndex = 4;
            this.colBonusAmountPaid.Width = 81;
            // 
            // colRemarks6
            // 
            this.colRemarks6.Caption = "Remarks";
            this.colRemarks6.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks6.FieldName = "Remarks";
            this.colRemarks6.Name = "colRemarks6";
            this.colRemarks6.OptionsColumn.ReadOnly = true;
            this.colRemarks6.Visible = true;
            this.colRemarks6.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colEmployeeName4
            // 
            this.colEmployeeName4.Caption = "Linked To Employee";
            this.colEmployeeName4.FieldName = "EmployeeName";
            this.colEmployeeName4.Name = "colEmployeeName4";
            this.colEmployeeName4.OptionsColumn.AllowEdit = false;
            this.colEmployeeName4.OptionsColumn.AllowFocus = false;
            this.colEmployeeName4.OptionsColumn.ReadOnly = true;
            this.colEmployeeName4.Visible = true;
            this.colEmployeeName4.VisibleIndex = 8;
            this.colEmployeeName4.Width = 227;
            // 
            // colEmployeeSurname
            // 
            this.colEmployeeSurname.Caption = "Employee Surname";
            this.colEmployeeSurname.FieldName = "EmployeeSurname";
            this.colEmployeeSurname.Name = "colEmployeeSurname";
            this.colEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname.Width = 112;
            // 
            // colEmployeeFirstname3
            // 
            this.colEmployeeFirstname3.Caption = "Employee Forename";
            this.colEmployeeFirstname3.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname3.Name = "colEmployeeFirstname3";
            this.colEmployeeFirstname3.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname3.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname3.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname3.Width = 118;
            // 
            // colEmployeeNumber2
            // 
            this.colEmployeeNumber2.Caption = "Employee #";
            this.colEmployeeNumber2.FieldName = "EmployeeNumber";
            this.colEmployeeNumber2.Name = "colEmployeeNumber2";
            this.colEmployeeNumber2.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber2.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber2.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber2.Width = 78;
            // 
            // colLinkedDocumentCount4
            // 
            this.colLinkedDocumentCount4.Caption = "Linked Documents";
            this.colLinkedDocumentCount4.ColumnEdit = this.repositoryItemHyperLinkEditBonusLinkedDocuments;
            this.colLinkedDocumentCount4.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount4.Name = "colLinkedDocumentCount4";
            this.colLinkedDocumentCount4.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount4.Visible = true;
            this.colLinkedDocumentCount4.VisibleIndex = 6;
            this.colLinkedDocumentCount4.Width = 107;
            // 
            // repositoryItemHyperLinkEditBonusLinkedDocuments
            // 
            this.repositoryItemHyperLinkEditBonusLinkedDocuments.AutoHeight = false;
            this.repositoryItemHyperLinkEditBonusLinkedDocuments.Name = "repositoryItemHyperLinkEditBonusLinkedDocuments";
            this.repositoryItemHyperLinkEditBonusLinkedDocuments.SingleClick = true;
            this.repositoryItemHyperLinkEditBonusLinkedDocuments.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditBonusLinkedDocuments_OpenLink);
            // 
            // xtraTabPagePayRises
            // 
            this.xtraTabPagePayRises.Controls.Add(this.gridSplitContainer16);
            this.xtraTabPagePayRises.Name = "xtraTabPagePayRises";
            this.xtraTabPagePayRises.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPagePayRises.Text = "Pay Rises";
            // 
            // gridSplitContainer16
            // 
            this.gridSplitContainer16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer16.Grid = this.gridControlPayRise;
            this.gridSplitContainer16.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer16.Name = "gridSplitContainer16";
            this.gridSplitContainer16.Panel1.Controls.Add(this.gridControlPayRise);
            this.gridSplitContainer16.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer16.TabIndex = 0;
            // 
            // gridControlPayRise
            // 
            this.gridControlPayRise.DataSource = this.spHR00213EmployeePayRisesBindingSource;
            this.gridControlPayRise.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlPayRise.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPayRise.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlPayRise.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPayRise_EmbeddedNavigator_ButtonClick);
            this.gridControlPayRise.Location = new System.Drawing.Point(0, 0);
            this.gridControlPayRise.MainView = this.gridViewPayRise;
            this.gridControlPayRise.MenuManager = this.barManager1;
            this.gridControlPayRise.Name = "gridControlPayRise";
            this.gridControlPayRise.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit18,
            this.repositoryItemTextEditDateTime6,
            this.repositoryItemTextEditCurrency5,
            this.repositoryItemTextEditPercentage5,
            this.repositoryItemHyperLinkEditPayRiseLinkedDocuments});
            this.gridControlPayRise.Size = new System.Drawing.Size(1189, 150);
            this.gridControlPayRise.TabIndex = 1;
            this.gridControlPayRise.UseEmbeddedNavigator = true;
            this.gridControlPayRise.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPayRise});
            // 
            // spHR00213EmployeePayRisesBindingSource
            // 
            this.spHR00213EmployeePayRisesBindingSource.DataMember = "sp_HR_00213_Employee_PayRises";
            this.spHR00213EmployeePayRisesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewPayRise
            // 
            this.gridViewPayRise.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPayRiseID,
            this.colEmployeeID12,
            this.colPreviousPayAmount,
            this.colPayRiseAmount,
            this.colPayRisePercentage,
            this.colNewPayAmount,
            this.colPayRiseDate,
            this.colAppliedByPersonID,
            this.colRemarks10,
            this.colEmployeeName8,
            this.colEmployeeSurname9,
            this.colEmployeeFirstname7,
            this.colEmployeeNumber12,
            this.colAppliedByPersonName,
            this.colLinkedDocumentCount10});
            this.gridViewPayRise.GridControl = this.gridControlPayRise;
            this.gridViewPayRise.GroupCount = 1;
            this.gridViewPayRise.Name = "gridViewPayRise";
            this.gridViewPayRise.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewPayRise.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewPayRise.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewPayRise.OptionsLayout.StoreAppearance = true;
            this.gridViewPayRise.OptionsLayout.StoreFormatRules = true;
            this.gridViewPayRise.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewPayRise.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewPayRise.OptionsSelection.MultiSelect = true;
            this.gridViewPayRise.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewPayRise.OptionsView.BestFitMaxRowCount = 5;
            this.gridViewPayRise.OptionsView.ColumnAutoWidth = false;
            this.gridViewPayRise.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewPayRise.OptionsView.ShowGroupPanel = false;
            this.gridViewPayRise.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName8, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPayRiseDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewPayRise.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewPayRise_CustomDrawCell);
            this.gridViewPayRise.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewPayRise_CustomRowCellEdit);
            this.gridViewPayRise.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewPayRise.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewPayRise.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewPayRise.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewPayRise_ShowingEditor);
            this.gridViewPayRise.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewPayRise.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewPayRise.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewPayRise.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewPayRise_MouseUp);
            this.gridViewPayRise.DoubleClick += new System.EventHandler(this.gridViewPayRise_DoubleClick);
            this.gridViewPayRise.GotFocus += new System.EventHandler(this.gridViewPayRise_GotFocus);
            // 
            // colPayRiseID
            // 
            this.colPayRiseID.Caption = "Pay Rise ID";
            this.colPayRiseID.FieldName = "PayRiseID";
            this.colPayRiseID.Name = "colPayRiseID";
            this.colPayRiseID.OptionsColumn.AllowEdit = false;
            this.colPayRiseID.OptionsColumn.AllowFocus = false;
            this.colPayRiseID.OptionsColumn.ReadOnly = true;
            this.colPayRiseID.Width = 76;
            // 
            // colEmployeeID12
            // 
            this.colEmployeeID12.Caption = "Employee ID";
            this.colEmployeeID12.FieldName = "EmployeeID";
            this.colEmployeeID12.Name = "colEmployeeID12";
            this.colEmployeeID12.OptionsColumn.AllowEdit = false;
            this.colEmployeeID12.OptionsColumn.AllowFocus = false;
            this.colEmployeeID12.OptionsColumn.ReadOnly = true;
            this.colEmployeeID12.Width = 81;
            // 
            // colPreviousPayAmount
            // 
            this.colPreviousPayAmount.Caption = "Previous Pay Amount";
            this.colPreviousPayAmount.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colPreviousPayAmount.FieldName = "PreviousPayAmount";
            this.colPreviousPayAmount.Name = "colPreviousPayAmount";
            this.colPreviousPayAmount.OptionsColumn.AllowEdit = false;
            this.colPreviousPayAmount.OptionsColumn.AllowFocus = false;
            this.colPreviousPayAmount.OptionsColumn.ReadOnly = true;
            this.colPreviousPayAmount.Visible = true;
            this.colPreviousPayAmount.VisibleIndex = 1;
            this.colPreviousPayAmount.Width = 123;
            // 
            // repositoryItemTextEditCurrency5
            // 
            this.repositoryItemTextEditCurrency5.AutoHeight = false;
            this.repositoryItemTextEditCurrency5.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency5.Name = "repositoryItemTextEditCurrency5";
            // 
            // colPayRiseAmount
            // 
            this.colPayRiseAmount.Caption = "Pay Rise Amount";
            this.colPayRiseAmount.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colPayRiseAmount.FieldName = "PayRiseAmount";
            this.colPayRiseAmount.Name = "colPayRiseAmount";
            this.colPayRiseAmount.OptionsColumn.AllowEdit = false;
            this.colPayRiseAmount.OptionsColumn.AllowFocus = false;
            this.colPayRiseAmount.OptionsColumn.ReadOnly = true;
            this.colPayRiseAmount.Visible = true;
            this.colPayRiseAmount.VisibleIndex = 2;
            this.colPayRiseAmount.Width = 102;
            // 
            // colPayRisePercentage
            // 
            this.colPayRisePercentage.Caption = "Pay Rise %";
            this.colPayRisePercentage.ColumnEdit = this.repositoryItemTextEditPercentage5;
            this.colPayRisePercentage.FieldName = "PayRisePercentage";
            this.colPayRisePercentage.Name = "colPayRisePercentage";
            this.colPayRisePercentage.OptionsColumn.AllowEdit = false;
            this.colPayRisePercentage.OptionsColumn.AllowFocus = false;
            this.colPayRisePercentage.OptionsColumn.ReadOnly = true;
            this.colPayRisePercentage.Visible = true;
            this.colPayRisePercentage.VisibleIndex = 3;
            this.colPayRisePercentage.Width = 76;
            // 
            // repositoryItemTextEditPercentage5
            // 
            this.repositoryItemTextEditPercentage5.AutoHeight = false;
            this.repositoryItemTextEditPercentage5.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage5.Name = "repositoryItemTextEditPercentage5";
            // 
            // colNewPayAmount
            // 
            this.colNewPayAmount.Caption = "New Pay Amount";
            this.colNewPayAmount.ColumnEdit = this.repositoryItemTextEditCurrency5;
            this.colNewPayAmount.FieldName = "NewPayAmount";
            this.colNewPayAmount.Name = "colNewPayAmount";
            this.colNewPayAmount.OptionsColumn.AllowEdit = false;
            this.colNewPayAmount.OptionsColumn.AllowFocus = false;
            this.colNewPayAmount.OptionsColumn.ReadOnly = true;
            this.colNewPayAmount.Visible = true;
            this.colNewPayAmount.VisibleIndex = 4;
            this.colNewPayAmount.Width = 103;
            // 
            // colPayRiseDate
            // 
            this.colPayRiseDate.Caption = "Pay Rise Date";
            this.colPayRiseDate.ColumnEdit = this.repositoryItemTextEditDateTime6;
            this.colPayRiseDate.FieldName = "PayRiseDate";
            this.colPayRiseDate.Name = "colPayRiseDate";
            this.colPayRiseDate.OptionsColumn.AllowEdit = false;
            this.colPayRiseDate.OptionsColumn.AllowFocus = false;
            this.colPayRiseDate.OptionsColumn.ReadOnly = true;
            this.colPayRiseDate.Visible = true;
            this.colPayRiseDate.VisibleIndex = 0;
            this.colPayRiseDate.Width = 101;
            // 
            // repositoryItemTextEditDateTime6
            // 
            this.repositoryItemTextEditDateTime6.AutoHeight = false;
            this.repositoryItemTextEditDateTime6.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime6.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime6.Name = "repositoryItemTextEditDateTime6";
            // 
            // colAppliedByPersonID
            // 
            this.colAppliedByPersonID.Caption = "Applied By Persion ID";
            this.colAppliedByPersonID.FieldName = "AppliedByPersonID";
            this.colAppliedByPersonID.Name = "colAppliedByPersonID";
            this.colAppliedByPersonID.OptionsColumn.AllowEdit = false;
            this.colAppliedByPersonID.OptionsColumn.AllowFocus = false;
            this.colAppliedByPersonID.OptionsColumn.ReadOnly = true;
            this.colAppliedByPersonID.Width = 123;
            // 
            // colRemarks10
            // 
            this.colRemarks10.Caption = "Remarks";
            this.colRemarks10.ColumnEdit = this.repositoryItemMemoExEdit18;
            this.colRemarks10.FieldName = "Remarks";
            this.colRemarks10.Name = "colRemarks10";
            this.colRemarks10.OptionsColumn.ReadOnly = true;
            this.colRemarks10.Visible = true;
            this.colRemarks10.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit18
            // 
            this.repositoryItemMemoExEdit18.AutoHeight = false;
            this.repositoryItemMemoExEdit18.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit18.Name = "repositoryItemMemoExEdit18";
            this.repositoryItemMemoExEdit18.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit18.ShowIcon = false;
            // 
            // colEmployeeName8
            // 
            this.colEmployeeName8.Caption = "Linked To Employee";
            this.colEmployeeName8.FieldName = "EmployeeName";
            this.colEmployeeName8.Name = "colEmployeeName8";
            this.colEmployeeName8.OptionsColumn.AllowEdit = false;
            this.colEmployeeName8.OptionsColumn.AllowFocus = false;
            this.colEmployeeName8.OptionsColumn.ReadOnly = true;
            this.colEmployeeName8.Visible = true;
            this.colEmployeeName8.VisibleIndex = 6;
            this.colEmployeeName8.Width = 192;
            // 
            // colEmployeeSurname9
            // 
            this.colEmployeeSurname9.Caption = "Surname";
            this.colEmployeeSurname9.FieldName = "EmployeeSurname";
            this.colEmployeeSurname9.Name = "colEmployeeSurname9";
            this.colEmployeeSurname9.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname9.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname9.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname9.Width = 119;
            // 
            // colEmployeeFirstname7
            // 
            this.colEmployeeFirstname7.Caption = "Forename";
            this.colEmployeeFirstname7.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname7.Name = "colEmployeeFirstname7";
            this.colEmployeeFirstname7.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname7.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname7.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname7.Width = 110;
            // 
            // colEmployeeNumber12
            // 
            this.colEmployeeNumber12.Caption = "Employee #";
            this.colEmployeeNumber12.FieldName = "EmployeeNumber";
            this.colEmployeeNumber12.Name = "colEmployeeNumber12";
            this.colEmployeeNumber12.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber12.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber12.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber12.Width = 93;
            // 
            // colAppliedByPersonName
            // 
            this.colAppliedByPersonName.Caption = "Applied By Person";
            this.colAppliedByPersonName.FieldName = "AppliedByPersonName";
            this.colAppliedByPersonName.Name = "colAppliedByPersonName";
            this.colAppliedByPersonName.OptionsColumn.AllowEdit = false;
            this.colAppliedByPersonName.OptionsColumn.AllowFocus = false;
            this.colAppliedByPersonName.OptionsColumn.ReadOnly = true;
            this.colAppliedByPersonName.Visible = true;
            this.colAppliedByPersonName.VisibleIndex = 6;
            this.colAppliedByPersonName.Width = 173;
            // 
            // colLinkedDocumentCount10
            // 
            this.colLinkedDocumentCount10.Caption = "Linked Documents";
            this.colLinkedDocumentCount10.ColumnEdit = this.repositoryItemHyperLinkEditPayRiseLinkedDocuments;
            this.colLinkedDocumentCount10.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount10.Name = "colLinkedDocumentCount10";
            this.colLinkedDocumentCount10.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount10.Visible = true;
            this.colLinkedDocumentCount10.VisibleIndex = 7;
            this.colLinkedDocumentCount10.Width = 107;
            // 
            // repositoryItemHyperLinkEditPayRiseLinkedDocuments
            // 
            this.repositoryItemHyperLinkEditPayRiseLinkedDocuments.AutoHeight = false;
            this.repositoryItemHyperLinkEditPayRiseLinkedDocuments.Name = "repositoryItemHyperLinkEditPayRiseLinkedDocuments";
            this.repositoryItemHyperLinkEditPayRiseLinkedDocuments.SingleClick = true;
            this.repositoryItemHyperLinkEditPayRiseLinkedDocuments.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditPayRiseLinkedDocuments_OpenLink);
            // 
            // xtraTabPagePensions
            // 
            this.xtraTabPagePensions.Controls.Add(this.gridSplitContainer13);
            this.xtraTabPagePensions.Name = "xtraTabPagePensions";
            this.xtraTabPagePensions.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPagePensions.Text = "Pensions";
            // 
            // gridSplitContainer13
            // 
            this.gridSplitContainer13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer13.Grid = this.gridControlPension;
            this.gridSplitContainer13.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer13.Name = "gridSplitContainer13";
            this.gridSplitContainer13.Panel1.Controls.Add(this.gridControlPension);
            this.gridSplitContainer13.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer13.TabIndex = 0;
            // 
            // gridControlPension
            // 
            this.gridControlPension.DataSource = this.spHR00016PensionsForEmployeeBindingSource;
            this.gridControlPension.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPension.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlPension.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlPension.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPension.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlPension.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPension_EmbeddedNavigator_ButtonClick);
            this.gridControlPension.Location = new System.Drawing.Point(0, 0);
            this.gridControlPension.MainView = this.gridViewPension;
            this.gridControlPension.MenuManager = this.barManager1;
            this.gridControlPension.Name = "gridControlPension";
            this.gridControlPension.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit13,
            this.repositoryItemCheckEdit12,
            this.repositoryItemTextEditDateTime13,
            this.repositoryItemTextEditCurrency13,
            this.repositoryItemTextEditMonths,
            this.repositoryItemHyperLinkEditLinkedDocumentPension});
            this.gridControlPension.Size = new System.Drawing.Size(1189, 150);
            this.gridControlPension.TabIndex = 1;
            this.gridControlPension.UseEmbeddedNavigator = true;
            this.gridControlPension.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPension});
            // 
            // spHR00016PensionsForEmployeeBindingSource
            // 
            this.spHR00016PensionsForEmployeeBindingSource.DataMember = "sp_HR_00016_Pensions_For_Employee";
            this.spHR00016PensionsForEmployeeBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewPension
            // 
            this.gridViewPension.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPensionID,
            this.colEmployeeID9,
            this.colSchemeNumber,
            this.colSchemeTypeID,
            this.colPayrollTypeID,
            this.colContributionTypeID,
            this.colMatchedContribution,
            this.colMinimumContributionAmount,
            this.colMaximumContributionAmount,
            this.colCurrentContributionAmount,
            this.colProviderID,
            this.colStartDate2,
            this.colEndDate2,
            this.colFixedInitialTermMonths,
            this.colTotalEmployeeContributionToDate,
            this.colTotalEmployerContributionToDate,
            this.colRemarks7,
            this.colEmployeeName5,
            this.colEmployeeSurname6,
            this.colEmployeeFirstname4,
            this.colEmployeeNumber9,
            this.colSchemeType,
            this.colPayrollType1,
            this.colContributionType,
            this.colProvider,
            this.colActive1,
            this.colContributionCount,
            this.colLinkedDocumentCount2});
            styleFormatCondition10.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition10.Appearance.Options.UseForeColor = true;
            styleFormatCondition10.ApplyToRow = true;
            styleFormatCondition10.Column = this.colActive1;
            styleFormatCondition10.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition10.Value1 = 0;
            this.gridViewPension.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition10});
            this.gridViewPension.GridControl = this.gridControlPension;
            this.gridViewPension.GroupCount = 1;
            this.gridViewPension.Name = "gridViewPension";
            this.gridViewPension.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewPension.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewPension.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewPension.OptionsLayout.StoreAppearance = true;
            this.gridViewPension.OptionsLayout.StoreFormatRules = true;
            this.gridViewPension.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewPension.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewPension.OptionsSelection.MultiSelect = true;
            this.gridViewPension.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewPension.OptionsView.BestFitMaxRowCount = 5;
            this.gridViewPension.OptionsView.ColumnAutoWidth = false;
            this.gridViewPension.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewPension.OptionsView.ShowGroupPanel = false;
            this.gridViewPension.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActive1, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate2, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewPension.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewPension_CustomDrawCell);
            this.gridViewPension.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewPension_CustomRowCellEdit);
            this.gridViewPension.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewPension.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewPension.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewPension.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewPension_ShowingEditor);
            this.gridViewPension.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewPension.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewPension.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewPension.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewPension_MouseUp);
            this.gridViewPension.DoubleClick += new System.EventHandler(this.gridViewPension_DoubleClick);
            this.gridViewPension.GotFocus += new System.EventHandler(this.gridViewPension_GotFocus);
            // 
            // colPensionID
            // 
            this.colPensionID.Caption = "Pension ID";
            this.colPensionID.FieldName = "PensionID";
            this.colPensionID.Name = "colPensionID";
            this.colPensionID.OptionsColumn.AllowEdit = false;
            this.colPensionID.OptionsColumn.AllowFocus = false;
            this.colPensionID.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeID9
            // 
            this.colEmployeeID9.Caption = "Employee ID";
            this.colEmployeeID9.FieldName = "EmployeeID";
            this.colEmployeeID9.Name = "colEmployeeID9";
            this.colEmployeeID9.OptionsColumn.AllowEdit = false;
            this.colEmployeeID9.OptionsColumn.AllowFocus = false;
            this.colEmployeeID9.OptionsColumn.ReadOnly = true;
            this.colEmployeeID9.Width = 81;
            // 
            // colSchemeNumber
            // 
            this.colSchemeNumber.Caption = "Scheme #";
            this.colSchemeNumber.FieldName = "SchemeNumber";
            this.colSchemeNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSchemeNumber.Name = "colSchemeNumber";
            this.colSchemeNumber.OptionsColumn.AllowEdit = false;
            this.colSchemeNumber.OptionsColumn.AllowFocus = false;
            this.colSchemeNumber.OptionsColumn.ReadOnly = true;
            this.colSchemeNumber.Visible = true;
            this.colSchemeNumber.VisibleIndex = 1;
            this.colSchemeNumber.Width = 110;
            // 
            // colSchemeTypeID
            // 
            this.colSchemeTypeID.Caption = "Scheme Type ID";
            this.colSchemeTypeID.FieldName = "SchemeTypeID";
            this.colSchemeTypeID.Name = "colSchemeTypeID";
            this.colSchemeTypeID.OptionsColumn.AllowEdit = false;
            this.colSchemeTypeID.OptionsColumn.AllowFocus = false;
            this.colSchemeTypeID.OptionsColumn.ReadOnly = true;
            this.colSchemeTypeID.Width = 99;
            // 
            // colPayrollTypeID
            // 
            this.colPayrollTypeID.Caption = "Payroll Type ID";
            this.colPayrollTypeID.FieldName = "PayrollTypeID";
            this.colPayrollTypeID.Name = "colPayrollTypeID";
            this.colPayrollTypeID.OptionsColumn.AllowEdit = false;
            this.colPayrollTypeID.OptionsColumn.AllowFocus = false;
            this.colPayrollTypeID.OptionsColumn.ReadOnly = true;
            this.colPayrollTypeID.Width = 94;
            // 
            // colContributionTypeID
            // 
            this.colContributionTypeID.Caption = "Contribution Type ID";
            this.colContributionTypeID.FieldName = "ContributionTypeID";
            this.colContributionTypeID.Name = "colContributionTypeID";
            this.colContributionTypeID.OptionsColumn.AllowEdit = false;
            this.colContributionTypeID.OptionsColumn.AllowFocus = false;
            this.colContributionTypeID.OptionsColumn.ReadOnly = true;
            this.colContributionTypeID.Width = 121;
            // 
            // colMatchedContribution
            // 
            this.colMatchedContribution.Caption = "Matched Contribution";
            this.colMatchedContribution.ColumnEdit = this.repositoryItemCheckEdit12;
            this.colMatchedContribution.FieldName = "MatchedContribution";
            this.colMatchedContribution.Name = "colMatchedContribution";
            this.colMatchedContribution.OptionsColumn.AllowEdit = false;
            this.colMatchedContribution.OptionsColumn.AllowFocus = false;
            this.colMatchedContribution.OptionsColumn.ReadOnly = true;
            this.colMatchedContribution.Visible = true;
            this.colMatchedContribution.VisibleIndex = 9;
            this.colMatchedContribution.Width = 124;
            // 
            // colMinimumContributionAmount
            // 
            this.colMinimumContributionAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colMinimumContributionAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colMinimumContributionAmount.Caption = "Min Contribution";
            this.colMinimumContributionAmount.ColumnEdit = this.repositoryItemTextEditCurrency13;
            this.colMinimumContributionAmount.FieldName = "MinimumContributionAmount";
            this.colMinimumContributionAmount.Name = "colMinimumContributionAmount";
            this.colMinimumContributionAmount.OptionsColumn.AllowEdit = false;
            this.colMinimumContributionAmount.OptionsColumn.AllowFocus = false;
            this.colMinimumContributionAmount.OptionsColumn.ReadOnly = true;
            this.colMinimumContributionAmount.Visible = true;
            this.colMinimumContributionAmount.VisibleIndex = 10;
            this.colMinimumContributionAmount.Width = 99;
            // 
            // repositoryItemTextEditCurrency13
            // 
            this.repositoryItemTextEditCurrency13.AutoHeight = false;
            this.repositoryItemTextEditCurrency13.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency13.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency13.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency13.Name = "repositoryItemTextEditCurrency13";
            // 
            // colMaximumContributionAmount
            // 
            this.colMaximumContributionAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colMaximumContributionAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colMaximumContributionAmount.Caption = "Max Contribution";
            this.colMaximumContributionAmount.ColumnEdit = this.repositoryItemTextEditCurrency13;
            this.colMaximumContributionAmount.FieldName = "MaximumContributionAmount";
            this.colMaximumContributionAmount.Name = "colMaximumContributionAmount";
            this.colMaximumContributionAmount.OptionsColumn.AllowEdit = false;
            this.colMaximumContributionAmount.OptionsColumn.AllowFocus = false;
            this.colMaximumContributionAmount.OptionsColumn.ReadOnly = true;
            this.colMaximumContributionAmount.Visible = true;
            this.colMaximumContributionAmount.VisibleIndex = 11;
            this.colMaximumContributionAmount.Width = 103;
            // 
            // colCurrentContributionAmount
            // 
            this.colCurrentContributionAmount.AppearanceCell.Options.UseTextOptions = true;
            this.colCurrentContributionAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colCurrentContributionAmount.Caption = "Current Contribution";
            this.colCurrentContributionAmount.ColumnEdit = this.repositoryItemTextEditCurrency13;
            this.colCurrentContributionAmount.FieldName = "CurrentContributionAmount";
            this.colCurrentContributionAmount.Name = "colCurrentContributionAmount";
            this.colCurrentContributionAmount.OptionsColumn.AllowEdit = false;
            this.colCurrentContributionAmount.OptionsColumn.AllowFocus = false;
            this.colCurrentContributionAmount.OptionsColumn.ReadOnly = true;
            this.colCurrentContributionAmount.Visible = true;
            this.colCurrentContributionAmount.VisibleIndex = 12;
            this.colCurrentContributionAmount.Width = 120;
            // 
            // colProviderID
            // 
            this.colProviderID.Caption = "Provider ID";
            this.colProviderID.FieldName = "ProviderID";
            this.colProviderID.Name = "colProviderID";
            this.colProviderID.OptionsColumn.AllowEdit = false;
            this.colProviderID.OptionsColumn.AllowFocus = false;
            this.colProviderID.OptionsColumn.ReadOnly = true;
            // 
            // colStartDate2
            // 
            this.colStartDate2.Caption = "Start Date";
            this.colStartDate2.ColumnEdit = this.repositoryItemTextEditDateTime13;
            this.colStartDate2.FieldName = "StartDate";
            this.colStartDate2.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate2.Name = "colStartDate2";
            this.colStartDate2.OptionsColumn.AllowEdit = false;
            this.colStartDate2.OptionsColumn.AllowFocus = false;
            this.colStartDate2.OptionsColumn.ReadOnly = true;
            this.colStartDate2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate2.Visible = true;
            this.colStartDate2.VisibleIndex = 5;
            this.colStartDate2.Width = 84;
            // 
            // repositoryItemTextEditDateTime13
            // 
            this.repositoryItemTextEditDateTime13.AutoHeight = false;
            this.repositoryItemTextEditDateTime13.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime13.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime13.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime13.Name = "repositoryItemTextEditDateTime13";
            // 
            // colEndDate2
            // 
            this.colEndDate2.Caption = "End Date";
            this.colEndDate2.ColumnEdit = this.repositoryItemTextEditDateTime13;
            this.colEndDate2.FieldName = "EndDate";
            this.colEndDate2.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate2.Name = "colEndDate2";
            this.colEndDate2.OptionsColumn.AllowEdit = false;
            this.colEndDate2.OptionsColumn.AllowFocus = false;
            this.colEndDate2.OptionsColumn.ReadOnly = true;
            this.colEndDate2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate2.Visible = true;
            this.colEndDate2.VisibleIndex = 6;
            // 
            // colFixedInitialTermMonths
            // 
            this.colFixedInitialTermMonths.Caption = "Fixed Initial Term";
            this.colFixedInitialTermMonths.ColumnEdit = this.repositoryItemTextEditMonths;
            this.colFixedInitialTermMonths.FieldName = "FixedInitialTermMonths";
            this.colFixedInitialTermMonths.Name = "colFixedInitialTermMonths";
            this.colFixedInitialTermMonths.OptionsColumn.AllowEdit = false;
            this.colFixedInitialTermMonths.OptionsColumn.AllowFocus = false;
            this.colFixedInitialTermMonths.OptionsColumn.ReadOnly = true;
            this.colFixedInitialTermMonths.Visible = true;
            this.colFixedInitialTermMonths.VisibleIndex = 7;
            this.colFixedInitialTermMonths.Width = 103;
            // 
            // repositoryItemTextEditMonths
            // 
            this.repositoryItemTextEditMonths.AccessibleRole = System.Windows.Forms.AccessibleRole.Grip;
            this.repositoryItemTextEditMonths.AutoHeight = false;
            this.repositoryItemTextEditMonths.Mask.EditMask = "####0 Months";
            this.repositoryItemTextEditMonths.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditMonths.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditMonths.Name = "repositoryItemTextEditMonths";
            // 
            // colTotalEmployeeContributionToDate
            // 
            this.colTotalEmployeeContributionToDate.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalEmployeeContributionToDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTotalEmployeeContributionToDate.Caption = "Total Employee Contributions";
            this.colTotalEmployeeContributionToDate.ColumnEdit = this.repositoryItemTextEditCurrency13;
            this.colTotalEmployeeContributionToDate.FieldName = "TotalEmployeeContributionToDate";
            this.colTotalEmployeeContributionToDate.Name = "colTotalEmployeeContributionToDate";
            this.colTotalEmployeeContributionToDate.OptionsColumn.AllowEdit = false;
            this.colTotalEmployeeContributionToDate.OptionsColumn.AllowFocus = false;
            this.colTotalEmployeeContributionToDate.OptionsColumn.ReadOnly = true;
            this.colTotalEmployeeContributionToDate.Visible = true;
            this.colTotalEmployeeContributionToDate.VisibleIndex = 13;
            this.colTotalEmployeeContributionToDate.Width = 161;
            // 
            // colTotalEmployerContributionToDate
            // 
            this.colTotalEmployerContributionToDate.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalEmployerContributionToDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colTotalEmployerContributionToDate.Caption = "Total Employer Contributions";
            this.colTotalEmployerContributionToDate.ColumnEdit = this.repositoryItemTextEditCurrency13;
            this.colTotalEmployerContributionToDate.FieldName = "TotalEmployerContributionToDate";
            this.colTotalEmployerContributionToDate.Name = "colTotalEmployerContributionToDate";
            this.colTotalEmployerContributionToDate.OptionsColumn.AllowEdit = false;
            this.colTotalEmployerContributionToDate.OptionsColumn.AllowFocus = false;
            this.colTotalEmployerContributionToDate.OptionsColumn.ReadOnly = true;
            this.colTotalEmployerContributionToDate.Visible = true;
            this.colTotalEmployerContributionToDate.VisibleIndex = 14;
            this.colTotalEmployerContributionToDate.Width = 159;
            // 
            // colRemarks7
            // 
            this.colRemarks7.Caption = "Remarks";
            this.colRemarks7.ColumnEdit = this.repositoryItemMemoExEdit13;
            this.colRemarks7.FieldName = "Remarks";
            this.colRemarks7.Name = "colRemarks7";
            this.colRemarks7.OptionsColumn.ReadOnly = true;
            this.colRemarks7.Visible = true;
            this.colRemarks7.VisibleIndex = 16;
            // 
            // repositoryItemMemoExEdit13
            // 
            this.repositoryItemMemoExEdit13.AutoHeight = false;
            this.repositoryItemMemoExEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit13.Name = "repositoryItemMemoExEdit13";
            this.repositoryItemMemoExEdit13.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit13.ShowIcon = false;
            // 
            // colEmployeeName5
            // 
            this.colEmployeeName5.Caption = "Linked To Employee";
            this.colEmployeeName5.FieldName = "EmployeeName";
            this.colEmployeeName5.Name = "colEmployeeName5";
            this.colEmployeeName5.OptionsColumn.AllowEdit = false;
            this.colEmployeeName5.OptionsColumn.AllowFocus = false;
            this.colEmployeeName5.OptionsColumn.ReadOnly = true;
            this.colEmployeeName5.Visible = true;
            this.colEmployeeName5.VisibleIndex = 11;
            this.colEmployeeName5.Width = 215;
            // 
            // colEmployeeSurname6
            // 
            this.colEmployeeSurname6.Caption = "Surname";
            this.colEmployeeSurname6.FieldName = "EmployeeSurname";
            this.colEmployeeSurname6.Name = "colEmployeeSurname6";
            this.colEmployeeSurname6.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname6.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname6.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeFirstname4
            // 
            this.colEmployeeFirstname4.Caption = "Forename";
            this.colEmployeeFirstname4.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname4.Name = "colEmployeeFirstname4";
            this.colEmployeeFirstname4.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname4.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname4.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeNumber9
            // 
            this.colEmployeeNumber9.Caption = "Employee #";
            this.colEmployeeNumber9.FieldName = "EmployeeNumber";
            this.colEmployeeNumber9.Name = "colEmployeeNumber9";
            this.colEmployeeNumber9.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber9.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber9.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber9.Width = 78;
            // 
            // colSchemeType
            // 
            this.colSchemeType.Caption = "Scheme Type";
            this.colSchemeType.FieldName = "SchemeType";
            this.colSchemeType.Name = "colSchemeType";
            this.colSchemeType.OptionsColumn.AllowEdit = false;
            this.colSchemeType.OptionsColumn.AllowFocus = false;
            this.colSchemeType.OptionsColumn.ReadOnly = true;
            this.colSchemeType.Visible = true;
            this.colSchemeType.VisibleIndex = 2;
            this.colSchemeType.Width = 109;
            // 
            // colPayrollType1
            // 
            this.colPayrollType1.Caption = "Payroll Type";
            this.colPayrollType1.FieldName = "PayrollType";
            this.colPayrollType1.Name = "colPayrollType1";
            this.colPayrollType1.OptionsColumn.AllowEdit = false;
            this.colPayrollType1.OptionsColumn.AllowFocus = false;
            this.colPayrollType1.OptionsColumn.ReadOnly = true;
            this.colPayrollType1.Visible = true;
            this.colPayrollType1.VisibleIndex = 4;
            this.colPayrollType1.Width = 116;
            // 
            // colContributionType
            // 
            this.colContributionType.Caption = "Contribution Type";
            this.colContributionType.FieldName = "ContributionType";
            this.colContributionType.Name = "colContributionType";
            this.colContributionType.OptionsColumn.AllowEdit = false;
            this.colContributionType.OptionsColumn.AllowFocus = false;
            this.colContributionType.OptionsColumn.ReadOnly = true;
            this.colContributionType.Visible = true;
            this.colContributionType.VisibleIndex = 8;
            this.colContributionType.Width = 122;
            // 
            // colProvider
            // 
            this.colProvider.Caption = "Provider";
            this.colProvider.FieldName = "Provider";
            this.colProvider.Name = "colProvider";
            this.colProvider.OptionsColumn.AllowEdit = false;
            this.colProvider.OptionsColumn.AllowFocus = false;
            this.colProvider.OptionsColumn.ReadOnly = true;
            this.colProvider.Visible = true;
            this.colProvider.VisibleIndex = 3;
            this.colProvider.Width = 138;
            // 
            // colContributionCount
            // 
            this.colContributionCount.Caption = "Contribution Count";
            this.colContributionCount.FieldName = "ContributionCount";
            this.colContributionCount.Name = "colContributionCount";
            this.colContributionCount.OptionsColumn.AllowEdit = false;
            this.colContributionCount.OptionsColumn.AllowFocus = false;
            this.colContributionCount.OptionsColumn.ReadOnly = true;
            this.colContributionCount.Visible = true;
            this.colContributionCount.VisibleIndex = 15;
            this.colContributionCount.Width = 112;
            // 
            // colLinkedDocumentCount2
            // 
            this.colLinkedDocumentCount2.Caption = "Linked Documents";
            this.colLinkedDocumentCount2.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentPension;
            this.colLinkedDocumentCount2.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount2.Name = "colLinkedDocumentCount2";
            this.colLinkedDocumentCount2.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount2.Visible = true;
            this.colLinkedDocumentCount2.VisibleIndex = 17;
            this.colLinkedDocumentCount2.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentPension
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentPension.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentPension.Name = "repositoryItemHyperLinkEditLinkedDocumentPension";
            this.repositoryItemHyperLinkEditLinkedDocumentPension.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentPension.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentPension_OpenLink);
            // 
            // xtraTabPageSubsistence
            // 
            this.xtraTabPageSubsistence.Controls.Add(this.gridControlSubsistence);
            this.xtraTabPageSubsistence.Name = "xtraTabPageSubsistence";
            this.xtraTabPageSubsistence.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageSubsistence.Text = "Subsistence";
            // 
            // gridControlSubsistence
            // 
            this.gridControlSubsistence.DataSource = this.spHR00257EmployeeSubsistenceBindingSource;
            this.gridControlSubsistence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlSubsistence.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlSubsistence.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlSubsistence.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlSubsistence_EmbeddedNavigator_ButtonClick);
            this.gridControlSubsistence.Location = new System.Drawing.Point(0, 0);
            this.gridControlSubsistence.MainView = this.gridViewSubsistence;
            this.gridControlSubsistence.MenuManager = this.barManager1;
            this.gridControlSubsistence.Name = "gridControlSubsistence";
            this.gridControlSubsistence.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit20,
            this.repositoryItemTextEditDateTime14,
            this.repositoryItemTextEditCurrency14,
            this.repositoryItemTextEditNumeric2DP_14,
            this.repositoryItemCheckEdit17});
            this.gridControlSubsistence.Size = new System.Drawing.Size(1189, 150);
            this.gridControlSubsistence.TabIndex = 1;
            this.gridControlSubsistence.UseEmbeddedNavigator = true;
            this.gridControlSubsistence.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSubsistence});
            // 
            // spHR00257EmployeeSubsistenceBindingSource
            // 
            this.spHR00257EmployeeSubsistenceBindingSource.DataMember = "sp_HR_00257_Employee_Subsistence";
            this.spHR00257EmployeeSubsistenceBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewSubsistence
            // 
            this.gridViewSubsistence.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSubsistenceID,
            this.colEmployeeID15,
            this.colSubsistenceTypeID,
            this.colStartDate3,
            this.colEndDate3,
            this.colRate1,
            this.colRate1Units,
            this.colRate1UnitDescriptorID,
            this.colRate2,
            this.colRate2Units,
            this.colRate2UnitDescriptorID,
            this.colSalarySacrificeAmount,
            this.colRemarks12,
            this.colEmployeeName11,
            this.colEmployeeSurname12,
            this.colEmployeeFirstname10,
            this.colEmployeeNumber15,
            this.colRate1UnitDescriptor,
            this.colRate2UnitDescriptor,
            this.colSubsistenceType,
            this.colActive5});
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colActive5;
            gridFormatRule2.Name = "FormatInActiveSubsistence";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridViewSubsistence.FormatRules.Add(gridFormatRule2);
            this.gridViewSubsistence.GridControl = this.gridControlSubsistence;
            this.gridViewSubsistence.GroupCount = 1;
            this.gridViewSubsistence.Name = "gridViewSubsistence";
            this.gridViewSubsistence.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewSubsistence.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewSubsistence.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewSubsistence.OptionsLayout.StoreAppearance = true;
            this.gridViewSubsistence.OptionsLayout.StoreFormatRules = true;
            this.gridViewSubsistence.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewSubsistence.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewSubsistence.OptionsSelection.MultiSelect = true;
            this.gridViewSubsistence.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewSubsistence.OptionsView.BestFitMaxRowCount = 5;
            this.gridViewSubsistence.OptionsView.ColumnAutoWidth = false;
            this.gridViewSubsistence.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewSubsistence.OptionsView.ShowGroupPanel = false;
            this.gridViewSubsistence.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName11, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubsistenceType, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate3, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEndDate3, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colActive5, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewSubsistence.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewSubsistence.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewSubsistence.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewSubsistence.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewSubsistence.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewSubsistence.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewSubsistence.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewSubsistence_MouseUp);
            this.gridViewSubsistence.DoubleClick += new System.EventHandler(this.gridViewSubsistence_DoubleClick);
            this.gridViewSubsistence.GotFocus += new System.EventHandler(this.gridViewSubsistence_GotFocus);
            // 
            // colSubsistenceID
            // 
            this.colSubsistenceID.Caption = "Subsistence ID";
            this.colSubsistenceID.FieldName = "SubsistenceID";
            this.colSubsistenceID.Name = "colSubsistenceID";
            this.colSubsistenceID.OptionsColumn.AllowEdit = false;
            this.colSubsistenceID.OptionsColumn.AllowFocus = false;
            this.colSubsistenceID.OptionsColumn.ReadOnly = true;
            this.colSubsistenceID.Width = 90;
            // 
            // colEmployeeID15
            // 
            this.colEmployeeID15.Caption = "Employee ID";
            this.colEmployeeID15.FieldName = "EmployeeID";
            this.colEmployeeID15.Name = "colEmployeeID15";
            this.colEmployeeID15.OptionsColumn.AllowEdit = false;
            this.colEmployeeID15.OptionsColumn.AllowFocus = false;
            this.colEmployeeID15.OptionsColumn.ReadOnly = true;
            this.colEmployeeID15.Width = 79;
            // 
            // colSubsistenceTypeID
            // 
            this.colSubsistenceTypeID.Caption = "Subsistence Type ID";
            this.colSubsistenceTypeID.FieldName = "SubsistenceTypeID";
            this.colSubsistenceTypeID.Name = "colSubsistenceTypeID";
            this.colSubsistenceTypeID.OptionsColumn.AllowEdit = false;
            this.colSubsistenceTypeID.OptionsColumn.AllowFocus = false;
            this.colSubsistenceTypeID.OptionsColumn.ReadOnly = true;
            this.colSubsistenceTypeID.Width = 117;
            // 
            // colStartDate3
            // 
            this.colStartDate3.Caption = "Start Date";
            this.colStartDate3.ColumnEdit = this.repositoryItemTextEditDateTime14;
            this.colStartDate3.FieldName = "StartDate";
            this.colStartDate3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate3.Name = "colStartDate3";
            this.colStartDate3.OptionsColumn.AllowEdit = false;
            this.colStartDate3.OptionsColumn.AllowFocus = false;
            this.colStartDate3.OptionsColumn.ReadOnly = true;
            this.colStartDate3.Visible = true;
            this.colStartDate3.VisibleIndex = 1;
            this.colStartDate3.Width = 100;
            // 
            // repositoryItemTextEditDateTime14
            // 
            this.repositoryItemTextEditDateTime14.AutoHeight = false;
            this.repositoryItemTextEditDateTime14.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime14.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime14.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime14.Name = "repositoryItemTextEditDateTime14";
            // 
            // colEndDate3
            // 
            this.colEndDate3.Caption = "End Date";
            this.colEndDate3.ColumnEdit = this.repositoryItemTextEditDateTime14;
            this.colEndDate3.FieldName = "EndDate";
            this.colEndDate3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate3.Name = "colEndDate3";
            this.colEndDate3.OptionsColumn.AllowEdit = false;
            this.colEndDate3.OptionsColumn.AllowFocus = false;
            this.colEndDate3.OptionsColumn.ReadOnly = true;
            this.colEndDate3.Visible = true;
            this.colEndDate3.VisibleIndex = 2;
            this.colEndDate3.Width = 100;
            // 
            // colRate1
            // 
            this.colRate1.Caption = "Rate 1";
            this.colRate1.ColumnEdit = this.repositoryItemTextEditCurrency14;
            this.colRate1.FieldName = "Rate1";
            this.colRate1.Name = "colRate1";
            this.colRate1.OptionsColumn.AllowEdit = false;
            this.colRate1.OptionsColumn.AllowFocus = false;
            this.colRate1.OptionsColumn.ReadOnly = true;
            this.colRate1.Visible = true;
            this.colRate1.VisibleIndex = 4;
            this.colRate1.Width = 60;
            // 
            // repositoryItemTextEditCurrency14
            // 
            this.repositoryItemTextEditCurrency14.AutoHeight = false;
            this.repositoryItemTextEditCurrency14.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency14.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency14.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency14.Name = "repositoryItemTextEditCurrency14";
            // 
            // colRate1Units
            // 
            this.colRate1Units.Caption = "Rate 1 Units";
            this.colRate1Units.ColumnEdit = this.repositoryItemTextEditNumeric2DP_14;
            this.colRate1Units.FieldName = "Rate1Units";
            this.colRate1Units.Name = "colRate1Units";
            this.colRate1Units.OptionsColumn.AllowEdit = false;
            this.colRate1Units.OptionsColumn.AllowFocus = false;
            this.colRate1Units.OptionsColumn.ReadOnly = true;
            this.colRate1Units.Visible = true;
            this.colRate1Units.VisibleIndex = 5;
            this.colRate1Units.Width = 78;
            // 
            // repositoryItemTextEditNumeric2DP_14
            // 
            this.repositoryItemTextEditNumeric2DP_14.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP_14.Mask.EditMask = "n2";
            this.repositoryItemTextEditNumeric2DP_14.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP_14.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP_14.Name = "repositoryItemTextEditNumeric2DP_14";
            // 
            // colRate1UnitDescriptorID
            // 
            this.colRate1UnitDescriptorID.Caption = "Rate 1 Descriptor ID";
            this.colRate1UnitDescriptorID.FieldName = "Rate1UnitDescriptorID";
            this.colRate1UnitDescriptorID.Name = "colRate1UnitDescriptorID";
            this.colRate1UnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colRate1UnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colRate1UnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colRate1UnitDescriptorID.Width = 117;
            // 
            // colRate2
            // 
            this.colRate2.Caption = "Rate 2";
            this.colRate2.ColumnEdit = this.repositoryItemTextEditCurrency14;
            this.colRate2.FieldName = "Rate2";
            this.colRate2.Name = "colRate2";
            this.colRate2.OptionsColumn.AllowEdit = false;
            this.colRate2.OptionsColumn.AllowFocus = false;
            this.colRate2.OptionsColumn.ReadOnly = true;
            this.colRate2.Visible = true;
            this.colRate2.VisibleIndex = 7;
            this.colRate2.Width = 60;
            // 
            // colRate2Units
            // 
            this.colRate2Units.Caption = "Rate 2 Units";
            this.colRate2Units.ColumnEdit = this.repositoryItemTextEditNumeric2DP_14;
            this.colRate2Units.FieldName = "Rate2Units";
            this.colRate2Units.Name = "colRate2Units";
            this.colRate2Units.OptionsColumn.AllowEdit = false;
            this.colRate2Units.OptionsColumn.AllowFocus = false;
            this.colRate2Units.OptionsColumn.ReadOnly = true;
            this.colRate2Units.Visible = true;
            this.colRate2Units.VisibleIndex = 8;
            this.colRate2Units.Width = 78;
            // 
            // colRate2UnitDescriptorID
            // 
            this.colRate2UnitDescriptorID.Caption = "Rate 2 Unit Descriptor ID";
            this.colRate2UnitDescriptorID.FieldName = "Rate2UnitDescriptorID";
            this.colRate2UnitDescriptorID.Name = "colRate2UnitDescriptorID";
            this.colRate2UnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colRate2UnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colRate2UnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colRate2UnitDescriptorID.Width = 139;
            // 
            // colSalarySacrificeAmount
            // 
            this.colSalarySacrificeAmount.Caption = "Salary Sacrifice";
            this.colSalarySacrificeAmount.ColumnEdit = this.repositoryItemTextEditCurrency14;
            this.colSalarySacrificeAmount.FieldName = "SalarySacrificeAmount";
            this.colSalarySacrificeAmount.Name = "colSalarySacrificeAmount";
            this.colSalarySacrificeAmount.OptionsColumn.AllowEdit = false;
            this.colSalarySacrificeAmount.OptionsColumn.AllowFocus = false;
            this.colSalarySacrificeAmount.OptionsColumn.ReadOnly = true;
            this.colSalarySacrificeAmount.Visible = true;
            this.colSalarySacrificeAmount.VisibleIndex = 10;
            this.colSalarySacrificeAmount.Width = 92;
            // 
            // colRemarks12
            // 
            this.colRemarks12.Caption = "Remarks";
            this.colRemarks12.ColumnEdit = this.repositoryItemMemoExEdit20;
            this.colRemarks12.FieldName = "Remarks";
            this.colRemarks12.Name = "colRemarks12";
            this.colRemarks12.OptionsColumn.ReadOnly = true;
            this.colRemarks12.Visible = true;
            this.colRemarks12.VisibleIndex = 11;
            // 
            // repositoryItemMemoExEdit20
            // 
            this.repositoryItemMemoExEdit20.AutoHeight = false;
            this.repositoryItemMemoExEdit20.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit20.Name = "repositoryItemMemoExEdit20";
            this.repositoryItemMemoExEdit20.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit20.ShowIcon = false;
            // 
            // colEmployeeName11
            // 
            this.colEmployeeName11.Caption = "Employee Name";
            this.colEmployeeName11.FieldName = "EmployeeName";
            this.colEmployeeName11.Name = "colEmployeeName11";
            this.colEmployeeName11.OptionsColumn.AllowEdit = false;
            this.colEmployeeName11.OptionsColumn.AllowFocus = false;
            this.colEmployeeName11.OptionsColumn.ReadOnly = true;
            this.colEmployeeName11.Visible = true;
            this.colEmployeeName11.VisibleIndex = 11;
            this.colEmployeeName11.Width = 262;
            // 
            // colEmployeeSurname12
            // 
            this.colEmployeeSurname12.Caption = "Employee Surname";
            this.colEmployeeSurname12.FieldName = "EmployeeSurname";
            this.colEmployeeSurname12.Name = "colEmployeeSurname12";
            this.colEmployeeSurname12.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname12.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname12.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname12.Width = 110;
            // 
            // colEmployeeFirstname10
            // 
            this.colEmployeeFirstname10.Caption = "Employee Forename";
            this.colEmployeeFirstname10.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname10.Name = "colEmployeeFirstname10";
            this.colEmployeeFirstname10.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname10.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname10.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname10.Width = 116;
            // 
            // colEmployeeNumber15
            // 
            this.colEmployeeNumber15.Caption = "Employee #";
            this.colEmployeeNumber15.FieldName = "EmployeeNumber";
            this.colEmployeeNumber15.Name = "colEmployeeNumber15";
            this.colEmployeeNumber15.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber15.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber15.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber15.Width = 76;
            // 
            // colRate1UnitDescriptor
            // 
            this.colRate1UnitDescriptor.Caption = "Rate 1 Descriptor";
            this.colRate1UnitDescriptor.FieldName = "Rate1UnitDescriptor";
            this.colRate1UnitDescriptor.Name = "colRate1UnitDescriptor";
            this.colRate1UnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colRate1UnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colRate1UnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colRate1UnitDescriptor.Visible = true;
            this.colRate1UnitDescriptor.VisibleIndex = 6;
            this.colRate1UnitDescriptor.Width = 103;
            // 
            // colRate2UnitDescriptor
            // 
            this.colRate2UnitDescriptor.Caption = "Rate 2 Descriptor";
            this.colRate2UnitDescriptor.FieldName = "Rate2UnitDescriptor";
            this.colRate2UnitDescriptor.Name = "colRate2UnitDescriptor";
            this.colRate2UnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colRate2UnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colRate2UnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colRate2UnitDescriptor.Visible = true;
            this.colRate2UnitDescriptor.VisibleIndex = 9;
            this.colRate2UnitDescriptor.Width = 103;
            // 
            // colSubsistenceType
            // 
            this.colSubsistenceType.Caption = "Subsistence Type";
            this.colSubsistenceType.FieldName = "SubsistenceType";
            this.colSubsistenceType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSubsistenceType.Name = "colSubsistenceType";
            this.colSubsistenceType.OptionsColumn.AllowEdit = false;
            this.colSubsistenceType.OptionsColumn.AllowFocus = false;
            this.colSubsistenceType.OptionsColumn.ReadOnly = true;
            this.colSubsistenceType.Visible = true;
            this.colSubsistenceType.VisibleIndex = 0;
            this.colSubsistenceType.Width = 136;
            // 
            // xtraTabPageTraining
            // 
            this.xtraTabPageTraining.Controls.Add(this.gridSplitContainer8);
            this.xtraTabPageTraining.Name = "xtraTabPageTraining";
            this.xtraTabPageTraining.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageTraining.Text = "Training";
            // 
            // gridSplitContainer8
            // 
            this.gridSplitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer8.Grid = this.gridControlTraining;
            this.gridSplitContainer8.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer8.Name = "gridSplitContainer8";
            this.gridSplitContainer8.Panel1.Controls.Add(this.gridControlTraining);
            this.gridSplitContainer8.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer8.TabIndex = 0;
            // 
            // gridControlTraining
            // 
            this.gridControlTraining.DataSource = this.sp09113HRQualificationsForEmployeeBindingSource;
            this.gridControlTraining.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlTraining.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlTraining.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlTraining.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlTraining_EmbeddedNavigator_ButtonClick);
            this.gridControlTraining.Location = new System.Drawing.Point(0, 0);
            this.gridControlTraining.MainView = this.gridViewTraining;
            this.gridControlTraining.MenuManager = this.barManager1;
            this.gridControlTraining.Name = "gridControlTraining";
            this.gridControlTraining.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining,
            this.repositoryItemTextEditDateTime12,
            this.repositoryItemMemoExEdit14,
            this.repositoryItemCheckEdit13,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEditDateOnky});
            this.gridControlTraining.Size = new System.Drawing.Size(1189, 150);
            this.gridControlTraining.TabIndex = 0;
            this.gridControlTraining.UseEmbeddedNavigator = true;
            this.gridControlTraining.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewTraining});
            // 
            // sp09113HRQualificationsForEmployeeBindingSource
            // 
            this.sp09113HRQualificationsForEmployeeBindingSource.DataMember = "sp09113_HR_Qualifications_For_Employee";
            this.sp09113HRQualificationsForEmployeeBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewTraining
            // 
            this.gridViewTraining.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQualificationID,
            this.colLinkedToPersonID,
            this.colLinkedToPersonTypeID,
            this.colQualificationTypeID,
            this.colQualificationFromID,
            this.colCourseNumber,
            this.colCourseName,
            this.colCostToCompany,
            this.gridColumn8,
            this.gridColumn9,
            this.gridColumn10,
            this.colRefreshDueDate,
            this.colRemarks1,
            this.colSummerMaintenance,
            this.colWinterMaintenance,
            this.colUtilityArb,
            this.colUtilityRail,
            this.colWoodPlan,
            this.colGUID,
            this.colLinkedToPersonType,
            this.colLinkedToPersonName,
            this.colQualificationType,
            this.colQualificationFrom,
            this.colDaysUntilExpiry,
            this.colLinkedDocumentCount7,
            this.colQualificationSubType,
            this.colQualificationSubTypeID,
            this.colArchived,
            this.colAssessmentDate,
            this.colCourseEndDate,
            this.colCourseStartDate});
            this.gridViewTraining.GridControl = this.gridControlTraining;
            this.gridViewTraining.GroupCount = 1;
            this.gridViewTraining.Name = "gridViewTraining";
            this.gridViewTraining.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewTraining.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewTraining.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewTraining.OptionsLayout.StoreAppearance = true;
            this.gridViewTraining.OptionsLayout.StoreFormatRules = true;
            this.gridViewTraining.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewTraining.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewTraining.OptionsSelection.MultiSelect = true;
            this.gridViewTraining.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewTraining.OptionsView.ColumnAutoWidth = false;
            this.gridViewTraining.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewTraining.OptionsView.ShowGroupPanel = false;
            this.gridViewTraining.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDaysUntilExpiry, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonType, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewTraining.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewTraining_CustomDrawCell);
            this.gridViewTraining.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewTraining_CustomRowCellEdit);
            this.gridViewTraining.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewTraining.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewTraining.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewTraining.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewTraining_ShowingEditor);
            this.gridViewTraining.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewTraining.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewTraining.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewTraining.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewTraining_MouseUp);
            this.gridViewTraining.DoubleClick += new System.EventHandler(this.gridViewTraining_DoubleClick);
            this.gridViewTraining.GotFocus += new System.EventHandler(this.gridViewTraining_GotFocus);
            // 
            // colQualificationID
            // 
            this.colQualificationID.Caption = "Qualification ID";
            this.colQualificationID.FieldName = "QualificationID";
            this.colQualificationID.Name = "colQualificationID";
            this.colQualificationID.OptionsColumn.AllowEdit = false;
            this.colQualificationID.OptionsColumn.AllowFocus = false;
            this.colQualificationID.OptionsColumn.ReadOnly = true;
            this.colQualificationID.Width = 94;
            // 
            // colLinkedToPersonID
            // 
            this.colLinkedToPersonID.Caption = "Linked To Person ID";
            this.colLinkedToPersonID.FieldName = "LinkedToPersonID";
            this.colLinkedToPersonID.Name = "colLinkedToPersonID";
            this.colLinkedToPersonID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonID.Width = 116;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Linked To Person Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 143;
            // 
            // colQualificationTypeID
            // 
            this.colQualificationTypeID.Caption = "Qualification Type ID";
            this.colQualificationTypeID.FieldName = "QualificationTypeID";
            this.colQualificationTypeID.Name = "colQualificationTypeID";
            this.colQualificationTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeID.Width = 121;
            // 
            // colQualificationFromID
            // 
            this.colQualificationFromID.Caption = "Qualification From ID";
            this.colQualificationFromID.FieldName = "QualificationFromID";
            this.colQualificationFromID.Name = "colQualificationFromID";
            this.colQualificationFromID.OptionsColumn.AllowEdit = false;
            this.colQualificationFromID.OptionsColumn.AllowFocus = false;
            this.colQualificationFromID.OptionsColumn.ReadOnly = true;
            this.colQualificationFromID.Width = 121;
            // 
            // colCourseNumber
            // 
            this.colCourseNumber.Caption = "Course #";
            this.colCourseNumber.FieldName = "CourseNumber";
            this.colCourseNumber.Name = "colCourseNumber";
            this.colCourseNumber.OptionsColumn.AllowEdit = false;
            this.colCourseNumber.OptionsColumn.AllowFocus = false;
            this.colCourseNumber.OptionsColumn.ReadOnly = true;
            this.colCourseNumber.Visible = true;
            this.colCourseNumber.VisibleIndex = 12;
            // 
            // colCourseName
            // 
            this.colCourseName.Caption = "Course Name";
            this.colCourseName.FieldName = "CourseName";
            this.colCourseName.Name = "colCourseName";
            this.colCourseName.OptionsColumn.AllowEdit = false;
            this.colCourseName.OptionsColumn.AllowFocus = false;
            this.colCourseName.OptionsColumn.ReadOnly = true;
            this.colCourseName.Visible = true;
            this.colCourseName.VisibleIndex = 11;
            this.colCourseName.Width = 120;
            // 
            // colCostToCompany
            // 
            this.colCostToCompany.Caption = "Cost to Company";
            this.colCostToCompany.ColumnEdit = this.repositoryItemTextEdit2;
            this.colCostToCompany.FieldName = "CostToCompany";
            this.colCostToCompany.Name = "colCostToCompany";
            this.colCostToCompany.OptionsColumn.AllowEdit = false;
            this.colCostToCompany.OptionsColumn.AllowFocus = false;
            this.colCostToCompany.OptionsColumn.ReadOnly = true;
            this.colCostToCompany.Visible = true;
            this.colCostToCompany.VisibleIndex = 14;
            this.colCostToCompany.Width = 104;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Qualification Start";
            this.gridColumn8.ColumnEdit = this.repositoryItemTextEditDateOnky;
            this.gridColumn8.DisplayFormat.FormatString = "d";
            this.gridColumn8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn8.FieldName = "StartDate";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 6;
            this.gridColumn8.Width = 107;
            // 
            // repositoryItemTextEditDateOnky
            // 
            this.repositoryItemTextEditDateOnky.AutoHeight = false;
            this.repositoryItemTextEditDateOnky.DisplayFormat.FormatString = "d";
            this.repositoryItemTextEditDateOnky.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.repositoryItemTextEditDateOnky.Mask.EditMask = "d";
            this.repositoryItemTextEditDateOnky.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateOnky.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateOnky.Name = "repositoryItemTextEditDateOnky";
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Qualification End";
            this.gridColumn9.ColumnEdit = this.repositoryItemTextEditDateOnky;
            this.gridColumn9.DisplayFormat.FormatString = "d";
            this.gridColumn9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn9.FieldName = "EndDate";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 9;
            this.gridColumn9.Width = 101;
            // 
            // gridColumn10
            // 
            this.gridColumn10.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn10.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn10.Caption = "Qualification Expiry";
            this.gridColumn10.ColumnEdit = this.repositoryItemTextEditDateOnky;
            this.gridColumn10.DisplayFormat.FormatString = "d";
            this.gridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn10.FieldName = "ExpiryDate";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 5;
            this.gridColumn10.Width = 113;
            // 
            // colRefreshDueDate
            // 
            this.colRefreshDueDate.AppearanceCell.Options.UseTextOptions = true;
            this.colRefreshDueDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefreshDueDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colRefreshDueDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefreshDueDate.Caption = "Qualification Lapse";
            this.colRefreshDueDate.ColumnEdit = this.repositoryItemTextEditDateOnky;
            this.colRefreshDueDate.DisplayFormat.FormatString = "d";
            this.colRefreshDueDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colRefreshDueDate.FieldName = "RefreshDueDate";
            this.colRefreshDueDate.Name = "colRefreshDueDate";
            this.colRefreshDueDate.OptionsColumn.AllowEdit = false;
            this.colRefreshDueDate.OptionsColumn.AllowFocus = false;
            this.colRefreshDueDate.OptionsColumn.ReadOnly = true;
            this.colRefreshDueDate.Visible = true;
            this.colRefreshDueDate.VisibleIndex = 4;
            this.colRefreshDueDate.Width = 109;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit14;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 15;
            // 
            // repositoryItemMemoExEdit14
            // 
            this.repositoryItemMemoExEdit14.AutoHeight = false;
            this.repositoryItemMemoExEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit14.Name = "repositoryItemMemoExEdit14";
            this.repositoryItemMemoExEdit14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit14.ShowIcon = false;
            // 
            // colSummerMaintenance
            // 
            this.colSummerMaintenance.Caption = "Summer Maintenance";
            this.colSummerMaintenance.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colSummerMaintenance.FieldName = "SummerMaintenance";
            this.colSummerMaintenance.Name = "colSummerMaintenance";
            this.colSummerMaintenance.OptionsColumn.AllowEdit = false;
            this.colSummerMaintenance.OptionsColumn.AllowFocus = false;
            this.colSummerMaintenance.OptionsColumn.ReadOnly = true;
            this.colSummerMaintenance.Visible = true;
            this.colSummerMaintenance.VisibleIndex = 16;
            this.colSummerMaintenance.Width = 123;
            // 
            // repositoryItemCheckEdit13
            // 
            this.repositoryItemCheckEdit13.AutoHeight = false;
            this.repositoryItemCheckEdit13.Caption = "Check";
            this.repositoryItemCheckEdit13.Name = "repositoryItemCheckEdit13";
            this.repositoryItemCheckEdit13.ValueChecked = 1;
            this.repositoryItemCheckEdit13.ValueUnchecked = 0;
            // 
            // colWinterMaintenance
            // 
            this.colWinterMaintenance.Caption = "Winter Maintenance";
            this.colWinterMaintenance.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colWinterMaintenance.FieldName = "WinterMaintenance";
            this.colWinterMaintenance.Name = "colWinterMaintenance";
            this.colWinterMaintenance.OptionsColumn.AllowEdit = false;
            this.colWinterMaintenance.OptionsColumn.AllowFocus = false;
            this.colWinterMaintenance.OptionsColumn.ReadOnly = true;
            this.colWinterMaintenance.Visible = true;
            this.colWinterMaintenance.VisibleIndex = 17;
            this.colWinterMaintenance.Width = 117;
            // 
            // colUtilityArb
            // 
            this.colUtilityArb.Caption = "Utility ARB";
            this.colUtilityArb.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colUtilityArb.FieldName = "UtilityArb";
            this.colUtilityArb.Name = "colUtilityArb";
            this.colUtilityArb.OptionsColumn.AllowEdit = false;
            this.colUtilityArb.OptionsColumn.AllowFocus = false;
            this.colUtilityArb.OptionsColumn.ReadOnly = true;
            this.colUtilityArb.Visible = true;
            this.colUtilityArb.VisibleIndex = 18;
            // 
            // colUtilityRail
            // 
            this.colUtilityRail.Caption = "Utility Rail";
            this.colUtilityRail.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colUtilityRail.FieldName = "UtilityRail";
            this.colUtilityRail.Name = "colUtilityRail";
            this.colUtilityRail.OptionsColumn.AllowEdit = false;
            this.colUtilityRail.OptionsColumn.AllowFocus = false;
            this.colUtilityRail.OptionsColumn.ReadOnly = true;
            this.colUtilityRail.Visible = true;
            this.colUtilityRail.VisibleIndex = 19;
            // 
            // colWoodPlan
            // 
            this.colWoodPlan.Caption = "WoodPlan";
            this.colWoodPlan.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colWoodPlan.FieldName = "WoodPlan";
            this.colWoodPlan.Name = "colWoodPlan";
            this.colWoodPlan.OptionsColumn.AllowEdit = false;
            this.colWoodPlan.OptionsColumn.AllowFocus = false;
            this.colWoodPlan.OptionsColumn.ReadOnly = true;
            this.colWoodPlan.Visible = true;
            this.colWoodPlan.VisibleIndex = 20;
            // 
            // colGUID
            // 
            this.colGUID.Caption = "GUID";
            this.colGUID.FieldName = "GUID";
            this.colGUID.Name = "colGUID";
            this.colGUID.OptionsColumn.AllowEdit = false;
            this.colGUID.OptionsColumn.AllowFocus = false;
            this.colGUID.OptionsColumn.ReadOnly = true;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Owner Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 0;
            this.colLinkedToPersonType.Width = 102;
            // 
            // colLinkedToPersonName
            // 
            this.colLinkedToPersonName.Caption = "Owner";
            this.colLinkedToPersonName.FieldName = "LinkedToPersonName";
            this.colLinkedToPersonName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLinkedToPersonName.Name = "colLinkedToPersonName";
            this.colLinkedToPersonName.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonName.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonName.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonName.Visible = true;
            this.colLinkedToPersonName.VisibleIndex = 1;
            this.colLinkedToPersonName.Width = 151;
            // 
            // colQualificationType
            // 
            this.colQualificationType.Caption = "Qualification Type";
            this.colQualificationType.FieldName = "QualificationType";
            this.colQualificationType.Name = "colQualificationType";
            this.colQualificationType.OptionsColumn.AllowEdit = false;
            this.colQualificationType.OptionsColumn.AllowFocus = false;
            this.colQualificationType.OptionsColumn.ReadOnly = true;
            this.colQualificationType.Visible = true;
            this.colQualificationType.VisibleIndex = 1;
            this.colQualificationType.Width = 133;
            // 
            // colQualificationFrom
            // 
            this.colQualificationFrom.Caption = "Qualification Source";
            this.colQualificationFrom.FieldName = "QualificationFrom";
            this.colQualificationFrom.Name = "colQualificationFrom";
            this.colQualificationFrom.OptionsColumn.AllowEdit = false;
            this.colQualificationFrom.OptionsColumn.AllowFocus = false;
            this.colQualificationFrom.OptionsColumn.ReadOnly = true;
            this.colQualificationFrom.Visible = true;
            this.colQualificationFrom.VisibleIndex = 13;
            this.colQualificationFrom.Width = 147;
            // 
            // colDaysUntilExpiry
            // 
            this.colDaysUntilExpiry.Caption = "Expires / Lapses In";
            this.colDaysUntilExpiry.ColumnEdit = this.repositoryItemTextEdit3;
            this.colDaysUntilExpiry.FieldName = "DaysUntilExpiry";
            this.colDaysUntilExpiry.Name = "colDaysUntilExpiry";
            this.colDaysUntilExpiry.OptionsColumn.AllowEdit = false;
            this.colDaysUntilExpiry.OptionsColumn.AllowFocus = false;
            this.colDaysUntilExpiry.OptionsColumn.ReadOnly = true;
            this.colDaysUntilExpiry.Visible = true;
            this.colDaysUntilExpiry.VisibleIndex = 3;
            this.colDaysUntilExpiry.Width = 115;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Mask.EditMask = "#####0 Days";
            this.repositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colLinkedDocumentCount7
            // 
            this.colLinkedDocumentCount7.Caption = "Linked Documents";
            this.colLinkedDocumentCount7.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsTraining;
            this.colLinkedDocumentCount7.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount7.Name = "colLinkedDocumentCount7";
            this.colLinkedDocumentCount7.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount7.Visible = true;
            this.colLinkedDocumentCount7.VisibleIndex = 21;
            this.colLinkedDocumentCount7.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsTraining
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining.Name = "repositoryItemHyperLinkEditLinkedDocumentsTraining";
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsTraining.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsTraining_OpenLink);
            // 
            // colQualificationSubType
            // 
            this.colQualificationSubType.Caption = "Qualification Sub Type";
            this.colQualificationSubType.FieldName = "QualificationSubType";
            this.colQualificationSubType.Name = "colQualificationSubType";
            this.colQualificationSubType.OptionsColumn.AllowEdit = false;
            this.colQualificationSubType.OptionsColumn.AllowFocus = false;
            this.colQualificationSubType.OptionsColumn.ReadOnly = true;
            this.colQualificationSubType.Visible = true;
            this.colQualificationSubType.VisibleIndex = 2;
            this.colQualificationSubType.Width = 128;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.Caption = "Qualification Sub Type ID";
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeID.Width = 142;
            // 
            // colArchived
            // 
            this.colArchived.Caption = "Archived";
            this.colArchived.ColumnEdit = this.repositoryItemCheckEdit13;
            this.colArchived.FieldName = "Archived";
            this.colArchived.Name = "colArchived";
            this.colArchived.OptionsColumn.AllowEdit = false;
            this.colArchived.OptionsColumn.AllowFocus = false;
            this.colArchived.OptionsColumn.ReadOnly = true;
            this.colArchived.Visible = true;
            this.colArchived.VisibleIndex = 22;
            // 
            // colAssessmentDate
            // 
            this.colAssessmentDate.Caption = "Assessment Date";
            this.colAssessmentDate.ColumnEdit = this.repositoryItemTextEditDateOnky;
            this.colAssessmentDate.FieldName = "AssessmentDate";
            this.colAssessmentDate.Name = "colAssessmentDate";
            this.colAssessmentDate.OptionsColumn.AllowEdit = false;
            this.colAssessmentDate.OptionsColumn.AllowFocus = false;
            this.colAssessmentDate.OptionsColumn.ReadOnly = true;
            this.colAssessmentDate.Visible = true;
            this.colAssessmentDate.VisibleIndex = 10;
            this.colAssessmentDate.Width = 104;
            // 
            // colCourseEndDate
            // 
            this.colCourseEndDate.Caption = "Course End";
            this.colCourseEndDate.ColumnEdit = this.repositoryItemTextEditDateOnky;
            this.colCourseEndDate.FieldName = "CourseEndDate";
            this.colCourseEndDate.Name = "colCourseEndDate";
            this.colCourseEndDate.OptionsColumn.AllowEdit = false;
            this.colCourseEndDate.OptionsColumn.AllowFocus = false;
            this.colCourseEndDate.OptionsColumn.ReadOnly = true;
            this.colCourseEndDate.Visible = true;
            this.colCourseEndDate.VisibleIndex = 8;
            this.colCourseEndDate.Width = 100;
            // 
            // colCourseStartDate
            // 
            this.colCourseStartDate.Caption = "Course Start";
            this.colCourseStartDate.ColumnEdit = this.repositoryItemTextEditDateOnky;
            this.colCourseStartDate.FieldName = "CourseStartDate";
            this.colCourseStartDate.Name = "colCourseStartDate";
            this.colCourseStartDate.OptionsColumn.AllowEdit = false;
            this.colCourseStartDate.OptionsColumn.AllowFocus = false;
            this.colCourseStartDate.OptionsColumn.ReadOnly = true;
            this.colCourseStartDate.Visible = true;
            this.colCourseStartDate.VisibleIndex = 7;
            this.colCourseStartDate.Width = 100;
            // 
            // repositoryItemTextEditDateTime12
            // 
            this.repositoryItemTextEditDateTime12.AutoHeight = false;
            this.repositoryItemTextEditDateTime12.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime12.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime12.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime12.Name = "repositoryItemTextEditDateTime12";
            // 
            // xtraTabPageDiscipline
            // 
            this.xtraTabPageDiscipline.AutoScroll = true;
            this.xtraTabPageDiscipline.Controls.Add(this.gridSplitContainer9);
            this.xtraTabPageDiscipline.Name = "xtraTabPageDiscipline";
            this.xtraTabPageDiscipline.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageDiscipline.Text = "Discipline && Grievances";
            // 
            // gridSplitContainer9
            // 
            this.gridSplitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer9.Grid = this.gridControlDiscipline;
            this.gridSplitContainer9.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer9.Name = "gridSplitContainer9";
            this.gridSplitContainer9.Panel1.Controls.Add(this.gridControlDiscipline);
            this.gridSplitContainer9.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer9.TabIndex = 0;
            // 
            // gridControlDiscipline
            // 
            this.gridControlDiscipline.DataSource = this.spHR00053GetEmployeeSanctionsBindingSource;
            this.gridControlDiscipline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlDiscipline.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlDiscipline.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Record", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlDiscipline.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlDiscipline_EmbeddedNavigator_ButtonClick);
            this.gridControlDiscipline.Location = new System.Drawing.Point(0, 0);
            this.gridControlDiscipline.MainView = this.gridViewDiscipline;
            this.gridControlDiscipline.MenuManager = this.barManager1;
            this.gridControlDiscipline.Name = "gridControlDiscipline";
            this.gridControlDiscipline.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditNumeric0DP2,
            this.repositoryItemMemoExEdit5,
            this.repositoryItemHyperLinkEditLinkedDocumentSanction});
            this.gridControlDiscipline.Size = new System.Drawing.Size(1189, 150);
            this.gridControlDiscipline.TabIndex = 0;
            this.gridControlDiscipline.UseEmbeddedNavigator = true;
            this.gridControlDiscipline.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDiscipline});
            // 
            // spHR00053GetEmployeeSanctionsBindingSource
            // 
            this.spHR00053GetEmployeeSanctionsBindingSource.DataMember = "sp_HR_00053_Get_Employee_Sanctions";
            this.spHR00053GetEmployeeSanctionsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewDiscipline
            // 
            this.gridViewDiscipline.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSanctionID,
            this.colEmployeeID4,
            this.colEmployeeName,
            this.colEmployeeSurname2,
            this.colEmployeeFirstname,
            this.colEmployeeNumber5,
            this.colERIssueTypeID,
            this.colERIssueType,
            this.colHRManagerID,
            this.colHRManagerName,
            this.colDateRaised,
            this.colRaisedByPersonID,
            this.colRaisedByPersonName,
            this.colStatusID,
            this.colStatusValue,
            this.colInvestigationStartDate,
            this.colInvestigationEndDate,
            this.colInvestigatedByPersonID,
            this.colInvestigatedByPersonName,
            this.colInvestigationOutcomeID,
            this.colInvestigationOutcome,
            this.colHearingDate,
            this.colHeardByPersonID,
            this.colHeardByPersonName,
            this.colHearingNoteTakerName,
            this.colHearingEmployeeRepresentativeName,
            this.colHearingOutcomeID,
            this.colHearingOutcome,
            this.colHearingOutcomeDate,
            this.colHearingOutcomeIssuedByPersonID,
            this.colHearingOutcomeIssuedByPersonName,
            this.colAppealDate,
            this.colAppealHeardByPersonID,
            this.colAppealHeardByPersonName,
            this.colAppealOutcomeID,
            this.colAppealOutcome,
            this.colAppealNoteTakerName,
            this.colAppealEmployeeRepresentativeName,
            this.colExpiryDurationUnits,
            this.colExpiryDurationUnitDescriptorID,
            this.colExpiryDurationUnitDescriptor,
            this.colExpiryDate,
            this.colRemarks2,
            this.colLive,
            this.colSanctionOutcome,
            this.colLinkedDocumentCount3});
            this.gridViewDiscipline.GridControl = this.gridControlDiscipline;
            this.gridViewDiscipline.GroupCount = 1;
            this.gridViewDiscipline.Name = "gridViewDiscipline";
            this.gridViewDiscipline.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewDiscipline.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewDiscipline.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewDiscipline.OptionsLayout.StoreAppearance = true;
            this.gridViewDiscipline.OptionsLayout.StoreFormatRules = true;
            this.gridViewDiscipline.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewDiscipline.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewDiscipline.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridViewDiscipline.OptionsSelection.MultiSelect = true;
            this.gridViewDiscipline.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewDiscipline.OptionsView.BestFitMaxRowCount = 20;
            this.gridViewDiscipline.OptionsView.ColumnAutoWidth = false;
            this.gridViewDiscipline.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewDiscipline.OptionsView.ShowGroupPanel = false;
            this.gridViewDiscipline.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewDiscipline.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewDiscipline_CustomDrawCell);
            this.gridViewDiscipline.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewDiscipline_CustomRowCellEdit);
            this.gridViewDiscipline.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewDiscipline.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewDiscipline.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewDiscipline.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewDiscipline_ShowingEditor);
            this.gridViewDiscipline.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewDiscipline.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewDiscipline.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewDiscipline.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewDiscipline_MouseUp);
            this.gridViewDiscipline.DoubleClick += new System.EventHandler(this.gridViewDiscipline_DoubleClick);
            this.gridViewDiscipline.GotFocus += new System.EventHandler(this.gridViewDiscipline_GotFocus);
            // 
            // colSanctionID
            // 
            this.colSanctionID.Caption = "Sanction ID";
            this.colSanctionID.FieldName = "SanctionID";
            this.colSanctionID.Name = "colSanctionID";
            this.colSanctionID.OptionsColumn.AllowEdit = false;
            this.colSanctionID.OptionsColumn.AllowFocus = false;
            this.colSanctionID.OptionsColumn.ReadOnly = true;
            this.colSanctionID.Width = 76;
            // 
            // colEmployeeID4
            // 
            this.colEmployeeID4.Caption = "Employee ID";
            this.colEmployeeID4.FieldName = "EmployeeID";
            this.colEmployeeID4.Name = "colEmployeeID4";
            this.colEmployeeID4.OptionsColumn.AllowEdit = false;
            this.colEmployeeID4.OptionsColumn.AllowFocus = false;
            this.colEmployeeID4.OptionsColumn.ReadOnly = true;
            this.colEmployeeID4.Width = 81;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.Caption = "Employee Surname: Forename";
            this.colEmployeeName.FieldName = "EmployeeName";
            this.colEmployeeName.Name = "colEmployeeName";
            this.colEmployeeName.OptionsColumn.AllowEdit = false;
            this.colEmployeeName.OptionsColumn.AllowFocus = false;
            this.colEmployeeName.OptionsColumn.ReadOnly = true;
            this.colEmployeeName.Visible = true;
            this.colEmployeeName.VisibleIndex = 2;
            this.colEmployeeName.Width = 167;
            // 
            // colEmployeeSurname2
            // 
            this.colEmployeeSurname2.Caption = "Employee Surname";
            this.colEmployeeSurname2.FieldName = "EmployeeSurname";
            this.colEmployeeSurname2.Name = "colEmployeeSurname2";
            this.colEmployeeSurname2.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname2.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname2.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname2.Width = 112;
            // 
            // colEmployeeFirstname
            // 
            this.colEmployeeFirstname.Caption = "Employee Forename";
            this.colEmployeeFirstname.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname.Name = "colEmployeeFirstname";
            this.colEmployeeFirstname.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname.Width = 118;
            // 
            // colEmployeeNumber5
            // 
            this.colEmployeeNumber5.Caption = "Employee #";
            this.colEmployeeNumber5.FieldName = "EmployeeNumber";
            this.colEmployeeNumber5.Name = "colEmployeeNumber5";
            this.colEmployeeNumber5.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber5.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber5.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber5.Width = 78;
            // 
            // colERIssueTypeID
            // 
            this.colERIssueTypeID.Caption = "ER Issue Type ID";
            this.colERIssueTypeID.FieldName = "ERIssueTypeID";
            this.colERIssueTypeID.Name = "colERIssueTypeID";
            this.colERIssueTypeID.OptionsColumn.AllowEdit = false;
            this.colERIssueTypeID.OptionsColumn.AllowFocus = false;
            this.colERIssueTypeID.OptionsColumn.ReadOnly = true;
            this.colERIssueTypeID.Width = 104;
            // 
            // colERIssueType
            // 
            this.colERIssueType.Caption = "ER Issue Type";
            this.colERIssueType.FieldName = "ERIssueType";
            this.colERIssueType.Name = "colERIssueType";
            this.colERIssueType.OptionsColumn.AllowEdit = false;
            this.colERIssueType.OptionsColumn.AllowFocus = false;
            this.colERIssueType.OptionsColumn.ReadOnly = true;
            this.colERIssueType.Visible = true;
            this.colERIssueType.VisibleIndex = 2;
            this.colERIssueType.Width = 90;
            // 
            // colHRManagerID
            // 
            this.colHRManagerID.Caption = "HR Manager ID";
            this.colHRManagerID.FieldName = "HRManagerID";
            this.colHRManagerID.Name = "colHRManagerID";
            this.colHRManagerID.OptionsColumn.AllowEdit = false;
            this.colHRManagerID.OptionsColumn.AllowFocus = false;
            this.colHRManagerID.OptionsColumn.ReadOnly = true;
            this.colHRManagerID.Width = 94;
            // 
            // colHRManagerName
            // 
            this.colHRManagerName.Caption = "HR Manager";
            this.colHRManagerName.FieldName = "HRManagerName";
            this.colHRManagerName.Name = "colHRManagerName";
            this.colHRManagerName.OptionsColumn.AllowEdit = false;
            this.colHRManagerName.OptionsColumn.AllowFocus = false;
            this.colHRManagerName.OptionsColumn.ReadOnly = true;
            this.colHRManagerName.Visible = true;
            this.colHRManagerName.VisibleIndex = 3;
            this.colHRManagerName.Width = 80;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 0;
            this.colDateRaised.Width = 100;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colRaisedByPersonID
            // 
            this.colRaisedByPersonID.Caption = "Raised By Person ID";
            this.colRaisedByPersonID.FieldName = "RaisedByPersonID";
            this.colRaisedByPersonID.Name = "colRaisedByPersonID";
            this.colRaisedByPersonID.OptionsColumn.AllowEdit = false;
            this.colRaisedByPersonID.OptionsColumn.AllowFocus = false;
            this.colRaisedByPersonID.OptionsColumn.ReadOnly = true;
            this.colRaisedByPersonID.Width = 118;
            // 
            // colRaisedByPersonName
            // 
            this.colRaisedByPersonName.Caption = "Raised By Person Name";
            this.colRaisedByPersonName.FieldName = "RaisedByPersonName";
            this.colRaisedByPersonName.Name = "colRaisedByPersonName";
            this.colRaisedByPersonName.OptionsColumn.AllowEdit = false;
            this.colRaisedByPersonName.OptionsColumn.AllowFocus = false;
            this.colRaisedByPersonName.OptionsColumn.ReadOnly = true;
            this.colRaisedByPersonName.Visible = true;
            this.colRaisedByPersonName.VisibleIndex = 4;
            this.colRaisedByPersonName.Width = 134;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colStatusValue
            // 
            this.colStatusValue.Caption = "Status";
            this.colStatusValue.FieldName = "StatusValue";
            this.colStatusValue.Name = "colStatusValue";
            this.colStatusValue.OptionsColumn.AllowEdit = false;
            this.colStatusValue.OptionsColumn.AllowFocus = false;
            this.colStatusValue.OptionsColumn.ReadOnly = true;
            this.colStatusValue.Visible = true;
            this.colStatusValue.VisibleIndex = 5;
            // 
            // colInvestigationStartDate
            // 
            this.colInvestigationStartDate.Caption = "Investigation Start Date";
            this.colInvestigationStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colInvestigationStartDate.FieldName = "InvestigationStartDate";
            this.colInvestigationStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colInvestigationStartDate.Name = "colInvestigationStartDate";
            this.colInvestigationStartDate.OptionsColumn.AllowEdit = false;
            this.colInvestigationStartDate.OptionsColumn.AllowFocus = false;
            this.colInvestigationStartDate.OptionsColumn.ReadOnly = true;
            this.colInvestigationStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colInvestigationStartDate.Visible = true;
            this.colInvestigationStartDate.VisibleIndex = 7;
            this.colInvestigationStartDate.Width = 137;
            // 
            // colInvestigationEndDate
            // 
            this.colInvestigationEndDate.Caption = "Investigation End Date";
            this.colInvestigationEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colInvestigationEndDate.FieldName = "InvestigationEndDate";
            this.colInvestigationEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colInvestigationEndDate.Name = "colInvestigationEndDate";
            this.colInvestigationEndDate.OptionsColumn.AllowEdit = false;
            this.colInvestigationEndDate.OptionsColumn.AllowFocus = false;
            this.colInvestigationEndDate.OptionsColumn.ReadOnly = true;
            this.colInvestigationEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colInvestigationEndDate.Visible = true;
            this.colInvestigationEndDate.VisibleIndex = 8;
            this.colInvestigationEndDate.Width = 131;
            // 
            // colInvestigatedByPersonID
            // 
            this.colInvestigatedByPersonID.Caption = "Investigating Officer ID";
            this.colInvestigatedByPersonID.FieldName = "InvestigatedByPersonID";
            this.colInvestigatedByPersonID.Name = "colInvestigatedByPersonID";
            this.colInvestigatedByPersonID.OptionsColumn.AllowEdit = false;
            this.colInvestigatedByPersonID.OptionsColumn.AllowFocus = false;
            this.colInvestigatedByPersonID.OptionsColumn.ReadOnly = true;
            this.colInvestigatedByPersonID.Width = 134;
            // 
            // colInvestigatedByPersonName
            // 
            this.colInvestigatedByPersonName.Caption = "Investigating Officer";
            this.colInvestigatedByPersonName.FieldName = "InvestigatedByPersonName";
            this.colInvestigatedByPersonName.Name = "colInvestigatedByPersonName";
            this.colInvestigatedByPersonName.OptionsColumn.AllowEdit = false;
            this.colInvestigatedByPersonName.OptionsColumn.AllowFocus = false;
            this.colInvestigatedByPersonName.OptionsColumn.ReadOnly = true;
            this.colInvestigatedByPersonName.Visible = true;
            this.colInvestigatedByPersonName.VisibleIndex = 9;
            this.colInvestigatedByPersonName.Width = 120;
            // 
            // colInvestigationOutcomeID
            // 
            this.colInvestigationOutcomeID.Caption = "Investigation Outcome ID";
            this.colInvestigationOutcomeID.FieldName = "InvestigationOutcomeID";
            this.colInvestigationOutcomeID.Name = "colInvestigationOutcomeID";
            this.colInvestigationOutcomeID.OptionsColumn.AllowEdit = false;
            this.colInvestigationOutcomeID.OptionsColumn.AllowFocus = false;
            this.colInvestigationOutcomeID.OptionsColumn.ReadOnly = true;
            this.colInvestigationOutcomeID.Width = 144;
            // 
            // colInvestigationOutcome
            // 
            this.colInvestigationOutcome.Caption = "Investigation Outcome";
            this.colInvestigationOutcome.FieldName = "InvestigationOutcome";
            this.colInvestigationOutcome.Name = "colInvestigationOutcome";
            this.colInvestigationOutcome.OptionsColumn.AllowEdit = false;
            this.colInvestigationOutcome.OptionsColumn.AllowFocus = false;
            this.colInvestigationOutcome.OptionsColumn.ReadOnly = true;
            this.colInvestigationOutcome.Visible = true;
            this.colInvestigationOutcome.VisibleIndex = 10;
            this.colInvestigationOutcome.Width = 130;
            // 
            // colHearingDate
            // 
            this.colHearingDate.Caption = "Hearing Date";
            this.colHearingDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colHearingDate.FieldName = "HearingDate";
            this.colHearingDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHearingDate.Name = "colHearingDate";
            this.colHearingDate.OptionsColumn.AllowEdit = false;
            this.colHearingDate.OptionsColumn.AllowFocus = false;
            this.colHearingDate.OptionsColumn.ReadOnly = true;
            this.colHearingDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHearingDate.Visible = true;
            this.colHearingDate.VisibleIndex = 11;
            this.colHearingDate.Width = 84;
            // 
            // colHeardByPersonID
            // 
            this.colHeardByPersonID.Caption = "Heard By Person ID";
            this.colHeardByPersonID.FieldName = "HeardByPersonID";
            this.colHeardByPersonID.Name = "colHeardByPersonID";
            this.colHeardByPersonID.OptionsColumn.AllowEdit = false;
            this.colHeardByPersonID.OptionsColumn.AllowFocus = false;
            this.colHeardByPersonID.OptionsColumn.ReadOnly = true;
            this.colHeardByPersonID.Width = 115;
            // 
            // colHeardByPersonName
            // 
            this.colHeardByPersonName.Caption = "Heard By Person";
            this.colHeardByPersonName.FieldName = "HeardByPersonName";
            this.colHeardByPersonName.Name = "colHeardByPersonName";
            this.colHeardByPersonName.OptionsColumn.AllowEdit = false;
            this.colHeardByPersonName.OptionsColumn.AllowFocus = false;
            this.colHeardByPersonName.OptionsColumn.ReadOnly = true;
            this.colHeardByPersonName.Visible = true;
            this.colHeardByPersonName.VisibleIndex = 12;
            this.colHeardByPersonName.Width = 101;
            // 
            // colHearingNoteTakerName
            // 
            this.colHearingNoteTakerName.Caption = "Hearing Note Taker";
            this.colHearingNoteTakerName.FieldName = "HearingNoteTakerName";
            this.colHearingNoteTakerName.Name = "colHearingNoteTakerName";
            this.colHearingNoteTakerName.OptionsColumn.AllowEdit = false;
            this.colHearingNoteTakerName.OptionsColumn.AllowFocus = false;
            this.colHearingNoteTakerName.OptionsColumn.ReadOnly = true;
            this.colHearingNoteTakerName.Visible = true;
            this.colHearingNoteTakerName.VisibleIndex = 13;
            this.colHearingNoteTakerName.Width = 114;
            // 
            // colHearingEmployeeRepresentativeName
            // 
            this.colHearingEmployeeRepresentativeName.Caption = "Hearing Employee Representative";
            this.colHearingEmployeeRepresentativeName.FieldName = "HearingEmployeeRepresentativeName";
            this.colHearingEmployeeRepresentativeName.Name = "colHearingEmployeeRepresentativeName";
            this.colHearingEmployeeRepresentativeName.OptionsColumn.AllowEdit = false;
            this.colHearingEmployeeRepresentativeName.OptionsColumn.AllowFocus = false;
            this.colHearingEmployeeRepresentativeName.OptionsColumn.ReadOnly = true;
            this.colHearingEmployeeRepresentativeName.Visible = true;
            this.colHearingEmployeeRepresentativeName.VisibleIndex = 14;
            this.colHearingEmployeeRepresentativeName.Width = 184;
            // 
            // colHearingOutcomeID
            // 
            this.colHearingOutcomeID.Caption = "Hearing Outcome ID";
            this.colHearingOutcomeID.FieldName = "HearingOutcomeID";
            this.colHearingOutcomeID.Name = "colHearingOutcomeID";
            this.colHearingOutcomeID.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeID.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeID.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeID.Width = 118;
            // 
            // colHearingOutcome
            // 
            this.colHearingOutcome.Caption = "Hearing Outcome";
            this.colHearingOutcome.FieldName = "HearingOutcome";
            this.colHearingOutcome.Name = "colHearingOutcome";
            this.colHearingOutcome.OptionsColumn.AllowEdit = false;
            this.colHearingOutcome.OptionsColumn.AllowFocus = false;
            this.colHearingOutcome.OptionsColumn.ReadOnly = true;
            this.colHearingOutcome.Visible = true;
            this.colHearingOutcome.VisibleIndex = 15;
            this.colHearingOutcome.Width = 104;
            // 
            // colHearingOutcomeDate
            // 
            this.colHearingOutcomeDate.Caption = "Hearing Outcome Date";
            this.colHearingOutcomeDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colHearingOutcomeDate.FieldName = "HearingOutcomeDate";
            this.colHearingOutcomeDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHearingOutcomeDate.Name = "colHearingOutcomeDate";
            this.colHearingOutcomeDate.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeDate.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeDate.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHearingOutcomeDate.Visible = true;
            this.colHearingOutcomeDate.VisibleIndex = 16;
            this.colHearingOutcomeDate.Width = 130;
            // 
            // colHearingOutcomeIssuedByPersonID
            // 
            this.colHearingOutcomeIssuedByPersonID.Caption = "Hearing Outcome Issued By Person ID";
            this.colHearingOutcomeIssuedByPersonID.FieldName = "HearingOutcomeIssuedByPersonID";
            this.colHearingOutcomeIssuedByPersonID.Name = "colHearingOutcomeIssuedByPersonID";
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeIssuedByPersonID.Width = 204;
            // 
            // colHearingOutcomeIssuedByPersonName
            // 
            this.colHearingOutcomeIssuedByPersonName.Caption = "Hearing Outcome Issued By Person";
            this.colHearingOutcomeIssuedByPersonName.FieldName = "HearingOutcomeIssuedByPersonName";
            this.colHearingOutcomeIssuedByPersonName.Name = "colHearingOutcomeIssuedByPersonName";
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeIssuedByPersonName.Visible = true;
            this.colHearingOutcomeIssuedByPersonName.VisibleIndex = 17;
            this.colHearingOutcomeIssuedByPersonName.Width = 190;
            // 
            // colAppealDate
            // 
            this.colAppealDate.Caption = "Appeal Date";
            this.colAppealDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colAppealDate.FieldName = "AppealDate";
            this.colAppealDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colAppealDate.Name = "colAppealDate";
            this.colAppealDate.OptionsColumn.AllowEdit = false;
            this.colAppealDate.OptionsColumn.AllowFocus = false;
            this.colAppealDate.OptionsColumn.ReadOnly = true;
            this.colAppealDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colAppealDate.Visible = true;
            this.colAppealDate.VisibleIndex = 18;
            this.colAppealDate.Width = 80;
            // 
            // colAppealHeardByPersonID
            // 
            this.colAppealHeardByPersonID.Caption = "Appeal Heard By Person ID";
            this.colAppealHeardByPersonID.FieldName = "AppealHeardByPersonID";
            this.colAppealHeardByPersonID.Name = "colAppealHeardByPersonID";
            this.colAppealHeardByPersonID.OptionsColumn.AllowEdit = false;
            this.colAppealHeardByPersonID.OptionsColumn.AllowFocus = false;
            this.colAppealHeardByPersonID.OptionsColumn.ReadOnly = true;
            this.colAppealHeardByPersonID.Width = 151;
            // 
            // colAppealHeardByPersonName
            // 
            this.colAppealHeardByPersonName.Caption = "Appeal Heard By Person";
            this.colAppealHeardByPersonName.FieldName = "AppealHeardByPersonName";
            this.colAppealHeardByPersonName.Name = "colAppealHeardByPersonName";
            this.colAppealHeardByPersonName.OptionsColumn.AllowEdit = false;
            this.colAppealHeardByPersonName.OptionsColumn.AllowFocus = false;
            this.colAppealHeardByPersonName.OptionsColumn.ReadOnly = true;
            this.colAppealHeardByPersonName.Visible = true;
            this.colAppealHeardByPersonName.VisibleIndex = 19;
            this.colAppealHeardByPersonName.Width = 137;
            // 
            // colAppealOutcomeID
            // 
            this.colAppealOutcomeID.Caption = "Appeal Outcome ID";
            this.colAppealOutcomeID.FieldName = "AppealOutcomeID";
            this.colAppealOutcomeID.Name = "colAppealOutcomeID";
            this.colAppealOutcomeID.OptionsColumn.AllowEdit = false;
            this.colAppealOutcomeID.OptionsColumn.AllowFocus = false;
            this.colAppealOutcomeID.OptionsColumn.ReadOnly = true;
            this.colAppealOutcomeID.Width = 114;
            // 
            // colAppealOutcome
            // 
            this.colAppealOutcome.Caption = "Appeal Outcome";
            this.colAppealOutcome.FieldName = "AppealOutcome";
            this.colAppealOutcome.Name = "colAppealOutcome";
            this.colAppealOutcome.OptionsColumn.AllowEdit = false;
            this.colAppealOutcome.OptionsColumn.AllowFocus = false;
            this.colAppealOutcome.OptionsColumn.ReadOnly = true;
            this.colAppealOutcome.Visible = true;
            this.colAppealOutcome.VisibleIndex = 20;
            this.colAppealOutcome.Width = 100;
            // 
            // colAppealNoteTakerName
            // 
            this.colAppealNoteTakerName.Caption = "Appeal Note Taker";
            this.colAppealNoteTakerName.FieldName = "AppealNoteTakerName";
            this.colAppealNoteTakerName.Name = "colAppealNoteTakerName";
            this.colAppealNoteTakerName.OptionsColumn.AllowEdit = false;
            this.colAppealNoteTakerName.OptionsColumn.AllowFocus = false;
            this.colAppealNoteTakerName.OptionsColumn.ReadOnly = true;
            this.colAppealNoteTakerName.Visible = true;
            this.colAppealNoteTakerName.VisibleIndex = 21;
            this.colAppealNoteTakerName.Width = 110;
            // 
            // colAppealEmployeeRepresentativeName
            // 
            this.colAppealEmployeeRepresentativeName.Caption = "Appeal Employee Representative";
            this.colAppealEmployeeRepresentativeName.FieldName = "AppealEmployeeRepresentativeName";
            this.colAppealEmployeeRepresentativeName.Name = "colAppealEmployeeRepresentativeName";
            this.colAppealEmployeeRepresentativeName.OptionsColumn.AllowEdit = false;
            this.colAppealEmployeeRepresentativeName.OptionsColumn.AllowFocus = false;
            this.colAppealEmployeeRepresentativeName.OptionsColumn.ReadOnly = true;
            this.colAppealEmployeeRepresentativeName.Visible = true;
            this.colAppealEmployeeRepresentativeName.VisibleIndex = 22;
            this.colAppealEmployeeRepresentativeName.Width = 180;
            // 
            // colExpiryDurationUnits
            // 
            this.colExpiryDurationUnits.Caption = "Expiry Duration Units";
            this.colExpiryDurationUnits.ColumnEdit = this.repositoryItemTextEditNumeric0DP2;
            this.colExpiryDurationUnits.FieldName = "ExpiryDurationUnits";
            this.colExpiryDurationUnits.Name = "colExpiryDurationUnits";
            this.colExpiryDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnits.Visible = true;
            this.colExpiryDurationUnits.VisibleIndex = 23;
            this.colExpiryDurationUnits.Width = 122;
            // 
            // repositoryItemTextEditNumeric0DP2
            // 
            this.repositoryItemTextEditNumeric0DP2.AutoHeight = false;
            this.repositoryItemTextEditNumeric0DP2.Mask.EditMask = "n0";
            this.repositoryItemTextEditNumeric0DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric0DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric0DP2.Name = "repositoryItemTextEditNumeric0DP2";
            // 
            // colExpiryDurationUnitDescriptorID
            // 
            this.colExpiryDurationUnitDescriptorID.Caption = "Expiry Unit Descriptor ID";
            this.colExpiryDurationUnitDescriptorID.FieldName = "ExpiryDurationUnitDescriptorID";
            this.colExpiryDurationUnitDescriptorID.Name = "colExpiryDurationUnitDescriptorID";
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnitDescriptorID.Width = 139;
            // 
            // colExpiryDurationUnitDescriptor
            // 
            this.colExpiryDurationUnitDescriptor.Caption = "Expiry Unit Descriptor";
            this.colExpiryDurationUnitDescriptor.FieldName = "ExpiryDurationUnitDescriptor";
            this.colExpiryDurationUnitDescriptor.Name = "colExpiryDurationUnitDescriptor";
            this.colExpiryDurationUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnitDescriptor.Visible = true;
            this.colExpiryDurationUnitDescriptor.VisibleIndex = 24;
            this.colExpiryDurationUnitDescriptor.Width = 125;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.Caption = "Expiry Date";
            this.colExpiryDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 25;
            this.colExpiryDate.Width = 77;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 26;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colLive
            // 
            this.colLive.Caption = "Live";
            this.colLive.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colLive.FieldName = "Live";
            this.colLive.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLive.Name = "colLive";
            this.colLive.OptionsColumn.AllowEdit = false;
            this.colLive.OptionsColumn.AllowFocus = false;
            this.colLive.OptionsColumn.ReadOnly = true;
            this.colLive.Visible = true;
            this.colLive.VisibleIndex = 1;
            this.colLive.Width = 40;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colSanctionOutcome
            // 
            this.colSanctionOutcome.Caption = "Outcome";
            this.colSanctionOutcome.FieldName = "SanctionOutcome";
            this.colSanctionOutcome.Name = "colSanctionOutcome";
            this.colSanctionOutcome.OptionsColumn.AllowEdit = false;
            this.colSanctionOutcome.OptionsColumn.AllowFocus = false;
            this.colSanctionOutcome.OptionsColumn.ReadOnly = true;
            this.colSanctionOutcome.Visible = true;
            this.colSanctionOutcome.VisibleIndex = 6;
            // 
            // colLinkedDocumentCount3
            // 
            this.colLinkedDocumentCount3.Caption = "Linked Documents";
            this.colLinkedDocumentCount3.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentSanction;
            this.colLinkedDocumentCount3.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount3.Name = "colLinkedDocumentCount3";
            this.colLinkedDocumentCount3.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount3.Visible = true;
            this.colLinkedDocumentCount3.VisibleIndex = 27;
            this.colLinkedDocumentCount3.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentSanction
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentSanction.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentSanction.Name = "repositoryItemHyperLinkEditLinkedDocumentSanction";
            this.repositoryItemHyperLinkEditLinkedDocumentSanction.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentSanction.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentSanction_OpenLink);
            // 
            // xtraTabPageCRM
            // 
            this.xtraTabPageCRM.Controls.Add(this.gridSplitContainer14);
            this.xtraTabPageCRM.Name = "xtraTabPageCRM";
            this.xtraTabPageCRM.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageCRM.Text = "CRM";
            // 
            // gridSplitContainer14
            // 
            this.gridSplitContainer14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer14.Grid = this.gridControlCRM;
            this.gridSplitContainer14.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer14.Name = "gridSplitContainer14";
            this.gridSplitContainer14.Panel1.Controls.Add(this.gridControlCRM);
            this.gridSplitContainer14.Size = new System.Drawing.Size(1189, 150);
            this.gridSplitContainer14.TabIndex = 0;
            // 
            // gridControlCRM
            // 
            this.gridControlCRM.DataSource = this.spHR00193EmployeeCRMLinkedToEmployeeBindingSource;
            this.gridControlCRM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlCRM.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlCRM.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Record", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Linked Documents", "linked_document")});
            this.gridControlCRM.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlCRM_EmbeddedNavigator_ButtonClick);
            this.gridControlCRM.Location = new System.Drawing.Point(0, 0);
            this.gridControlCRM.MainView = this.gridViewCRM;
            this.gridControlCRM.MenuManager = this.barManager1;
            this.gridControlCRM.Name = "gridControlCRM";
            this.gridControlCRM.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTimeCRM,
            this.repositoryItemMemoExEditCRM,
            this.repositoryItemHyperLinkEditLinkedDocumentsCRM});
            this.gridControlCRM.Size = new System.Drawing.Size(1189, 150);
            this.gridControlCRM.TabIndex = 2;
            this.gridControlCRM.UseEmbeddedNavigator = true;
            this.gridControlCRM.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCRM});
            // 
            // spHR00193EmployeeCRMLinkedToEmployeeBindingSource
            // 
            this.spHR00193EmployeeCRMLinkedToEmployeeBindingSource.DataMember = "sp_HR_00193_Employee_CRM_Linked_To_Employee";
            this.spHR00193EmployeeCRMLinkedToEmployeeBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewCRM
            // 
            this.gridViewCRM.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCRMID,
            this.colEmployeeID10,
            this.colContactDueDate,
            this.colContactMadeDate,
            this.colContactMethodID,
            this.colContactWithEmployeeID,
            this.colRecordedByEmployeeID,
            this.colDateRecorded,
            this.colDescription2,
            this.colContactDirectionID,
            this.colStatusID2,
            this.colRemarks8,
            this.colEmployeeName6,
            this.colEmployeeSurname7,
            this.colEmployeeFirstname5,
            this.colEmployeeNumber10,
            this.colContactWithEmployeeName,
            this.colRecordedByEmployeeName,
            this.colContactDirection,
            this.colStatus2,
            this.colContactMethod,
            this.colLinkedDocumentCount8,
            this.colDaysRemaining});
            this.gridViewCRM.GridControl = this.gridControlCRM;
            this.gridViewCRM.GroupCount = 1;
            this.gridViewCRM.Name = "gridViewCRM";
            this.gridViewCRM.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewCRM.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewCRM.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewCRM.OptionsLayout.StoreAppearance = true;
            this.gridViewCRM.OptionsLayout.StoreFormatRules = true;
            this.gridViewCRM.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewCRM.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewCRM.OptionsSelection.MultiSelect = true;
            this.gridViewCRM.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewCRM.OptionsView.ColumnAutoWidth = false;
            this.gridViewCRM.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewCRM.OptionsView.ShowGroupPanel = false;
            this.gridViewCRM.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContactDueDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewCRM.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewCRM_CustomDrawCell);
            this.gridViewCRM.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewCRM_CustomRowCellEdit);
            this.gridViewCRM.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewCRM.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewCRM.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewCRM.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewCRM_ShowingEditor);
            this.gridViewCRM.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewCRM.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewCRM.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewCRM.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewCRM_MouseUp);
            this.gridViewCRM.DoubleClick += new System.EventHandler(this.gridViewCRM_DoubleClick);
            this.gridViewCRM.GotFocus += new System.EventHandler(this.gridViewCRM_GotFocus);
            // 
            // colCRMID
            // 
            this.colCRMID.Caption = "CRM ID";
            this.colCRMID.FieldName = "CRMID";
            this.colCRMID.Name = "colCRMID";
            this.colCRMID.OptionsColumn.AllowEdit = false;
            this.colCRMID.OptionsColumn.AllowFocus = false;
            this.colCRMID.OptionsColumn.ReadOnly = true;
            this.colCRMID.Width = 57;
            // 
            // colEmployeeID10
            // 
            this.colEmployeeID10.Caption = "Employee ID";
            this.colEmployeeID10.FieldName = "EmployeeID";
            this.colEmployeeID10.Name = "colEmployeeID10";
            this.colEmployeeID10.OptionsColumn.AllowEdit = false;
            this.colEmployeeID10.OptionsColumn.AllowFocus = false;
            this.colEmployeeID10.OptionsColumn.ReadOnly = true;
            this.colEmployeeID10.Width = 81;
            // 
            // colContactDueDate
            // 
            this.colContactDueDate.Caption = "Contact Due";
            this.colContactDueDate.ColumnEdit = this.repositoryItemTextEditDateTimeCRM;
            this.colContactDueDate.FieldName = "ContactDueDate";
            this.colContactDueDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactDueDate.Name = "colContactDueDate";
            this.colContactDueDate.OptionsColumn.AllowEdit = false;
            this.colContactDueDate.OptionsColumn.AllowFocus = false;
            this.colContactDueDate.OptionsColumn.ReadOnly = true;
            this.colContactDueDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactDueDate.Visible = true;
            this.colContactDueDate.VisibleIndex = 0;
            this.colContactDueDate.Width = 120;
            // 
            // repositoryItemTextEditDateTimeCRM
            // 
            this.repositoryItemTextEditDateTimeCRM.AutoHeight = false;
            this.repositoryItemTextEditDateTimeCRM.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTimeCRM.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTimeCRM.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTimeCRM.Name = "repositoryItemTextEditDateTimeCRM";
            // 
            // colContactMadeDate
            // 
            this.colContactMadeDate.Caption = "Contact Made";
            this.colContactMadeDate.ColumnEdit = this.repositoryItemTextEditDateTimeCRM;
            this.colContactMadeDate.FieldName = "ContactMadeDate";
            this.colContactMadeDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactMadeDate.Name = "colContactMadeDate";
            this.colContactMadeDate.OptionsColumn.AllowEdit = false;
            this.colContactMadeDate.OptionsColumn.AllowFocus = false;
            this.colContactMadeDate.OptionsColumn.ReadOnly = true;
            this.colContactMadeDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactMadeDate.Visible = true;
            this.colContactMadeDate.VisibleIndex = 2;
            this.colContactMadeDate.Width = 120;
            // 
            // colContactMethodID
            // 
            this.colContactMethodID.Caption = "Contact Method ID";
            this.colContactMethodID.FieldName = "ContactMethodID";
            this.colContactMethodID.Name = "colContactMethodID";
            this.colContactMethodID.OptionsColumn.AllowEdit = false;
            this.colContactMethodID.OptionsColumn.AllowFocus = false;
            this.colContactMethodID.OptionsColumn.ReadOnly = true;
            this.colContactMethodID.Width = 112;
            // 
            // colContactWithEmployeeID
            // 
            this.colContactWithEmployeeID.Caption = "Contact With Employee ID";
            this.colContactWithEmployeeID.FieldName = "ContactWithEmployeeID";
            this.colContactWithEmployeeID.Name = "colContactWithEmployeeID";
            this.colContactWithEmployeeID.OptionsColumn.AllowEdit = false;
            this.colContactWithEmployeeID.OptionsColumn.AllowFocus = false;
            this.colContactWithEmployeeID.OptionsColumn.ReadOnly = true;
            this.colContactWithEmployeeID.Width = 147;
            // 
            // colRecordedByEmployeeID
            // 
            this.colRecordedByEmployeeID.Caption = "Recorded By Employee ID";
            this.colRecordedByEmployeeID.FieldName = "RecordedByEmployeeID";
            this.colRecordedByEmployeeID.Name = "colRecordedByEmployeeID";
            this.colRecordedByEmployeeID.OptionsColumn.AllowEdit = false;
            this.colRecordedByEmployeeID.OptionsColumn.AllowFocus = false;
            this.colRecordedByEmployeeID.OptionsColumn.ReadOnly = true;
            this.colRecordedByEmployeeID.Width = 145;
            // 
            // colDateRecorded
            // 
            this.colDateRecorded.Caption = "Date Recorded";
            this.colDateRecorded.ColumnEdit = this.repositoryItemTextEditDateTimeCRM;
            this.colDateRecorded.FieldName = "DateRecorded";
            this.colDateRecorded.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRecorded.Name = "colDateRecorded";
            this.colDateRecorded.OptionsColumn.AllowEdit = false;
            this.colDateRecorded.OptionsColumn.AllowFocus = false;
            this.colDateRecorded.OptionsColumn.ReadOnly = true;
            this.colDateRecorded.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRecorded.Visible = true;
            this.colDateRecorded.VisibleIndex = 3;
            this.colDateRecorded.Width = 100;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Description";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.AllowFocus = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 4;
            this.colDescription2.Width = 249;
            // 
            // colContactDirectionID
            // 
            this.colContactDirectionID.Caption = "Contact Direction ID";
            this.colContactDirectionID.FieldName = "ContactDirectionID";
            this.colContactDirectionID.Name = "colContactDirectionID";
            this.colContactDirectionID.OptionsColumn.AllowEdit = false;
            this.colContactDirectionID.OptionsColumn.AllowFocus = false;
            this.colContactDirectionID.OptionsColumn.ReadOnly = true;
            this.colContactDirectionID.Width = 118;
            // 
            // colStatusID2
            // 
            this.colStatusID2.Caption = "Status ID";
            this.colStatusID2.FieldName = "StatusID";
            this.colStatusID2.Name = "colStatusID2";
            this.colStatusID2.OptionsColumn.AllowEdit = false;
            this.colStatusID2.OptionsColumn.AllowFocus = false;
            this.colStatusID2.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks8
            // 
            this.colRemarks8.Caption = "Remarks";
            this.colRemarks8.ColumnEdit = this.repositoryItemMemoExEditCRM;
            this.colRemarks8.FieldName = "Remarks";
            this.colRemarks8.Name = "colRemarks8";
            this.colRemarks8.OptionsColumn.ReadOnly = true;
            this.colRemarks8.Visible = true;
            this.colRemarks8.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEditCRM
            // 
            this.repositoryItemMemoExEditCRM.AutoHeight = false;
            this.repositoryItemMemoExEditCRM.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEditCRM.Name = "repositoryItemMemoExEditCRM";
            this.repositoryItemMemoExEditCRM.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEditCRM.ShowIcon = false;
            // 
            // colEmployeeName6
            // 
            this.colEmployeeName6.Caption = "Linked To Employee";
            this.colEmployeeName6.FieldName = "EmployeeName";
            this.colEmployeeName6.Name = "colEmployeeName6";
            this.colEmployeeName6.OptionsColumn.AllowEdit = false;
            this.colEmployeeName6.OptionsColumn.AllowFocus = false;
            this.colEmployeeName6.OptionsColumn.ReadOnly = true;
            this.colEmployeeName6.Visible = true;
            this.colEmployeeName6.VisibleIndex = 5;
            this.colEmployeeName6.Width = 247;
            // 
            // colEmployeeSurname7
            // 
            this.colEmployeeSurname7.Caption = "Employee Surname";
            this.colEmployeeSurname7.FieldName = "EmployeeSurname";
            this.colEmployeeSurname7.Name = "colEmployeeSurname7";
            this.colEmployeeSurname7.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname7.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname7.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname7.Width = 112;
            // 
            // colEmployeeFirstname5
            // 
            this.colEmployeeFirstname5.Caption = "Employee Forename";
            this.colEmployeeFirstname5.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname5.Name = "colEmployeeFirstname5";
            this.colEmployeeFirstname5.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname5.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname5.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname5.Width = 118;
            // 
            // colEmployeeNumber10
            // 
            this.colEmployeeNumber10.Caption = "Employee #";
            this.colEmployeeNumber10.FieldName = "EmployeeNumber";
            this.colEmployeeNumber10.Name = "colEmployeeNumber10";
            this.colEmployeeNumber10.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber10.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber10.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber10.Width = 78;
            // 
            // colContactWithEmployeeName
            // 
            this.colContactWithEmployeeName.Caption = "Contact with Person";
            this.colContactWithEmployeeName.FieldName = "ContactWithEmployeeName";
            this.colContactWithEmployeeName.Name = "colContactWithEmployeeName";
            this.colContactWithEmployeeName.OptionsColumn.AllowEdit = false;
            this.colContactWithEmployeeName.OptionsColumn.AllowFocus = false;
            this.colContactWithEmployeeName.OptionsColumn.ReadOnly = true;
            this.colContactWithEmployeeName.Visible = true;
            this.colContactWithEmployeeName.VisibleIndex = 5;
            this.colContactWithEmployeeName.Width = 135;
            // 
            // colRecordedByEmployeeName
            // 
            this.colRecordedByEmployeeName.Caption = "Recorded By";
            this.colRecordedByEmployeeName.FieldName = "RecordedByEmployeeName";
            this.colRecordedByEmployeeName.Name = "colRecordedByEmployeeName";
            this.colRecordedByEmployeeName.OptionsColumn.AllowEdit = false;
            this.colRecordedByEmployeeName.OptionsColumn.AllowFocus = false;
            this.colRecordedByEmployeeName.OptionsColumn.ReadOnly = true;
            this.colRecordedByEmployeeName.Visible = true;
            this.colRecordedByEmployeeName.VisibleIndex = 6;
            this.colRecordedByEmployeeName.Width = 135;
            // 
            // colContactDirection
            // 
            this.colContactDirection.Caption = "Contact Direction";
            this.colContactDirection.FieldName = "ContactDirection";
            this.colContactDirection.Name = "colContactDirection";
            this.colContactDirection.OptionsColumn.AllowEdit = false;
            this.colContactDirection.OptionsColumn.AllowFocus = false;
            this.colContactDirection.OptionsColumn.ReadOnly = true;
            this.colContactDirection.Visible = true;
            this.colContactDirection.VisibleIndex = 8;
            this.colContactDirection.Width = 132;
            // 
            // colStatus2
            // 
            this.colStatus2.Caption = "Status";
            this.colStatus2.FieldName = "Status";
            this.colStatus2.Name = "colStatus2";
            this.colStatus2.OptionsColumn.AllowEdit = false;
            this.colStatus2.OptionsColumn.AllowFocus = false;
            this.colStatus2.OptionsColumn.ReadOnly = true;
            this.colStatus2.Visible = true;
            this.colStatus2.VisibleIndex = 7;
            // 
            // colContactMethod
            // 
            this.colContactMethod.Caption = "Contact Method";
            this.colContactMethod.FieldName = "ContactMethod";
            this.colContactMethod.Name = "colContactMethod";
            this.colContactMethod.OptionsColumn.AllowEdit = false;
            this.colContactMethod.OptionsColumn.AllowFocus = false;
            this.colContactMethod.OptionsColumn.ReadOnly = true;
            this.colContactMethod.Visible = true;
            this.colContactMethod.VisibleIndex = 9;
            this.colContactMethod.Width = 98;
            // 
            // colLinkedDocumentCount8
            // 
            this.colLinkedDocumentCount8.Caption = "Linked Documents";
            this.colLinkedDocumentCount8.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentsCRM;
            this.colLinkedDocumentCount8.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount8.Name = "colLinkedDocumentCount8";
            this.colLinkedDocumentCount8.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount8.Visible = true;
            this.colLinkedDocumentCount8.VisibleIndex = 11;
            this.colLinkedDocumentCount8.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentsCRM
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentsCRM.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentsCRM.Name = "repositoryItemHyperLinkEditLinkedDocumentsCRM";
            this.repositoryItemHyperLinkEditLinkedDocumentsCRM.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentsCRM.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentsCRM_OpenLink);
            // 
            // colDaysRemaining
            // 
            this.colDaysRemaining.Caption = "Days Until Due";
            this.colDaysRemaining.FieldName = "DaysRemaining";
            this.colDaysRemaining.Name = "colDaysRemaining";
            this.colDaysRemaining.OptionsColumn.AllowEdit = false;
            this.colDaysRemaining.OptionsColumn.AllowFocus = false;
            this.colDaysRemaining.OptionsColumn.ReadOnly = true;
            this.colDaysRemaining.Visible = true;
            this.colDaysRemaining.VisibleIndex = 1;
            this.colDaysRemaining.Width = 91;
            // 
            // xtraTabPageAdminister
            // 
            this.xtraTabPageAdminister.Controls.Add(this.gridControlAdminister);
            this.xtraTabPageAdminister.Name = "xtraTabPageAdminister";
            this.xtraTabPageAdminister.Size = new System.Drawing.Size(1189, 150);
            this.xtraTabPageAdminister.Text = "Administer";
            // 
            // gridControlAdminister
            // 
            this.gridControlAdminister.DataSource = this.spHR00248EmployeeAdministeringBindingSource;
            this.gridControlAdminister.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlAdminister.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlAdminister.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 9, true, true, "Block Add New Records", "block_add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Toggle Active Status", "toggle_active")});
            this.gridControlAdminister.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAdminister_EmbeddedNavigator_ButtonClick);
            this.gridControlAdminister.Location = new System.Drawing.Point(0, 0);
            this.gridControlAdminister.MainView = this.gridViewAdminister;
            this.gridControlAdminister.MenuManager = this.barManager1;
            this.gridControlAdminister.Name = "gridControlAdminister";
            this.gridControlAdminister.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime18,
            this.repositoryItemCheckEdit18});
            this.gridControlAdminister.Size = new System.Drawing.Size(1189, 150);
            this.gridControlAdminister.TabIndex = 2;
            this.gridControlAdminister.UseEmbeddedNavigator = true;
            this.gridControlAdminister.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAdminister});
            // 
            // spHR00248EmployeeAdministeringBindingSource
            // 
            this.spHR00248EmployeeAdministeringBindingSource.DataMember = "sp_HR_00248_Employee_Administering";
            this.spHR00248EmployeeAdministeringBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridViewAdminister
            // 
            this.gridViewAdminister.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeAdministratorId,
            this.colEmployeeId14,
            this.colAdministratorId,
            this.colDateFrom,
            this.colDateTo,
            this.colActive4,
            this.colTypeId,
            this.colEmployeeName10,
            this.colEmployeeSurname11,
            this.colEmployeeFirstname9,
            this.colEmployeeNumber14,
            this.colAdministratorEmployeeName,
            this.colAdministratorEmployeeSurname,
            this.colAdministratorEmployeeFirstname,
            this.colAdministratorEmployeeNumber,
            this.colAdministrationType});
            this.gridViewAdminister.GridControl = this.gridControlAdminister;
            this.gridViewAdminister.GroupCount = 1;
            this.gridViewAdminister.Name = "gridViewAdminister";
            this.gridViewAdminister.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridViewAdminister.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewAdminister.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewAdminister.OptionsLayout.StoreAppearance = true;
            this.gridViewAdminister.OptionsLayout.StoreFormatRules = true;
            this.gridViewAdminister.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewAdminister.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewAdminister.OptionsSelection.MultiSelect = true;
            this.gridViewAdminister.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewAdminister.OptionsView.BestFitMaxRowCount = 5;
            this.gridViewAdminister.OptionsView.ColumnAutoWidth = false;
            this.gridViewAdminister.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewAdminister.OptionsView.ShowGroupPanel = false;
            this.gridViewAdminister.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAdministratorEmployeeName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateFrom, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTo, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurname11, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeFirstname9, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAdministrationType, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewAdminister.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewAdminister_CustomDrawCell);
            this.gridViewAdminister.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewAdminister_CustomRowCellEdit);
            this.gridViewAdminister.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewAdminister.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewAdminister.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewAdminister.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewAdminister_ShowingEditor);
            this.gridViewAdminister.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewAdminister.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewAdminister.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridViewAdminister.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewAdminister_MouseUp);
            this.gridViewAdminister.DoubleClick += new System.EventHandler(this.gridViewAdminister_DoubleClick);
            this.gridViewAdminister.GotFocus += new System.EventHandler(this.gridViewAdminister_GotFocus);
            // 
            // colEmployeeAdministratorId
            // 
            this.colEmployeeAdministratorId.Caption = "Administrator ID";
            this.colEmployeeAdministratorId.FieldName = "EmployeeAdministratorId";
            this.colEmployeeAdministratorId.Name = "colEmployeeAdministratorId";
            this.colEmployeeAdministratorId.OptionsColumn.AllowEdit = false;
            this.colEmployeeAdministratorId.OptionsColumn.AllowFocus = false;
            this.colEmployeeAdministratorId.OptionsColumn.ReadOnly = true;
            this.colEmployeeAdministratorId.Width = 97;
            // 
            // colEmployeeId14
            // 
            this.colEmployeeId14.Caption = "Employee ID";
            this.colEmployeeId14.FieldName = "EmployeeId";
            this.colEmployeeId14.Name = "colEmployeeId14";
            this.colEmployeeId14.OptionsColumn.AllowEdit = false;
            this.colEmployeeId14.OptionsColumn.AllowFocus = false;
            this.colEmployeeId14.OptionsColumn.ReadOnly = true;
            this.colEmployeeId14.Width = 79;
            // 
            // colAdministratorId
            // 
            this.colAdministratorId.Caption = "Administrator ID";
            this.colAdministratorId.FieldName = "AdministratorId";
            this.colAdministratorId.Name = "colAdministratorId";
            this.colAdministratorId.OptionsColumn.AllowEdit = false;
            this.colAdministratorId.OptionsColumn.AllowFocus = false;
            this.colAdministratorId.OptionsColumn.ReadOnly = true;
            this.colAdministratorId.Width = 97;
            // 
            // colDateFrom
            // 
            this.colDateFrom.Caption = "Date From";
            this.colDateFrom.ColumnEdit = this.repositoryItemTextEditDateTime18;
            this.colDateFrom.FieldName = "DateFrom";
            this.colDateFrom.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateFrom.Name = "colDateFrom";
            this.colDateFrom.OptionsColumn.AllowEdit = false;
            this.colDateFrom.OptionsColumn.AllowFocus = false;
            this.colDateFrom.OptionsColumn.ReadOnly = true;
            this.colDateFrom.Visible = true;
            this.colDateFrom.VisibleIndex = 0;
            this.colDateFrom.Width = 98;
            // 
            // repositoryItemTextEditDateTime18
            // 
            this.repositoryItemTextEditDateTime18.AutoHeight = false;
            this.repositoryItemTextEditDateTime18.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime18.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime18.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime18.Name = "repositoryItemTextEditDateTime18";
            // 
            // colDateTo
            // 
            this.colDateTo.Caption = "Date To";
            this.colDateTo.ColumnEdit = this.repositoryItemTextEditDateTime18;
            this.colDateTo.FieldName = "DateTo";
            this.colDateTo.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateTo.Name = "colDateTo";
            this.colDateTo.OptionsColumn.AllowEdit = false;
            this.colDateTo.OptionsColumn.AllowFocus = false;
            this.colDateTo.OptionsColumn.ReadOnly = true;
            this.colDateTo.Visible = true;
            this.colDateTo.VisibleIndex = 1;
            this.colDateTo.Width = 83;
            // 
            // colActive4
            // 
            this.colActive4.Caption = "Active";
            this.colActive4.ColumnEdit = this.repositoryItemCheckEdit18;
            this.colActive4.FieldName = "Active";
            this.colActive4.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colActive4.Name = "colActive4";
            this.colActive4.OptionsColumn.AllowEdit = false;
            this.colActive4.OptionsColumn.AllowFocus = false;
            this.colActive4.OptionsColumn.ReadOnly = true;
            this.colActive4.Visible = true;
            this.colActive4.VisibleIndex = 2;
            // 
            // repositoryItemCheckEdit18
            // 
            this.repositoryItemCheckEdit18.AutoHeight = false;
            this.repositoryItemCheckEdit18.Name = "repositoryItemCheckEdit18";
            this.repositoryItemCheckEdit18.ValueChecked = 1;
            this.repositoryItemCheckEdit18.ValueUnchecked = 0;
            // 
            // colTypeId
            // 
            this.colTypeId.Caption = "Type ID";
            this.colTypeId.FieldName = "TypeId";
            this.colTypeId.Name = "colTypeId";
            this.colTypeId.OptionsColumn.AllowEdit = false;
            this.colTypeId.OptionsColumn.AllowFocus = false;
            this.colTypeId.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeName10
            // 
            this.colEmployeeName10.Caption = "Employee Name";
            this.colEmployeeName10.FieldName = "EmployeeName";
            this.colEmployeeName10.Name = "colEmployeeName10";
            this.colEmployeeName10.OptionsColumn.AllowEdit = false;
            this.colEmployeeName10.OptionsColumn.AllowFocus = false;
            this.colEmployeeName10.OptionsColumn.ReadOnly = true;
            this.colEmployeeName10.Width = 196;
            // 
            // colEmployeeSurname11
            // 
            this.colEmployeeSurname11.Caption = "Employee Surname";
            this.colEmployeeSurname11.FieldName = "EmployeeSurname";
            this.colEmployeeSurname11.Name = "colEmployeeSurname11";
            this.colEmployeeSurname11.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname11.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname11.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname11.Visible = true;
            this.colEmployeeSurname11.VisibleIndex = 4;
            this.colEmployeeSurname11.Width = 124;
            // 
            // colEmployeeFirstname9
            // 
            this.colEmployeeFirstname9.Caption = "Employee Forename";
            this.colEmployeeFirstname9.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname9.Name = "colEmployeeFirstname9";
            this.colEmployeeFirstname9.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname9.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname9.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname9.Visible = true;
            this.colEmployeeFirstname9.VisibleIndex = 5;
            this.colEmployeeFirstname9.Width = 121;
            // 
            // colEmployeeNumber14
            // 
            this.colEmployeeNumber14.Caption = "Employee #";
            this.colEmployeeNumber14.FieldName = "EmployeeNumber";
            this.colEmployeeNumber14.Name = "colEmployeeNumber14";
            this.colEmployeeNumber14.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber14.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber14.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber14.Visible = true;
            this.colEmployeeNumber14.VisibleIndex = 3;
            this.colEmployeeNumber14.Width = 76;
            // 
            // colAdministratorEmployeeName
            // 
            this.colAdministratorEmployeeName.Caption = "Administrator Name";
            this.colAdministratorEmployeeName.FieldName = "AdministratorEmployeeName";
            this.colAdministratorEmployeeName.Name = "colAdministratorEmployeeName";
            this.colAdministratorEmployeeName.OptionsColumn.AllowEdit = false;
            this.colAdministratorEmployeeName.OptionsColumn.AllowFocus = false;
            this.colAdministratorEmployeeName.OptionsColumn.ReadOnly = true;
            this.colAdministratorEmployeeName.Visible = true;
            this.colAdministratorEmployeeName.VisibleIndex = 7;
            this.colAdministratorEmployeeName.Width = 242;
            // 
            // colAdministratorEmployeeSurname
            // 
            this.colAdministratorEmployeeSurname.Caption = "Administrator Surname";
            this.colAdministratorEmployeeSurname.FieldName = "AdministratorEmployeeSurname";
            this.colAdministratorEmployeeSurname.Name = "colAdministratorEmployeeSurname";
            this.colAdministratorEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colAdministratorEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colAdministratorEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colAdministratorEmployeeSurname.Width = 128;
            // 
            // colAdministratorEmployeeFirstname
            // 
            this.colAdministratorEmployeeFirstname.Caption = "Administrator Forename";
            this.colAdministratorEmployeeFirstname.FieldName = "AdministratorEmployeeFirstname";
            this.colAdministratorEmployeeFirstname.Name = "colAdministratorEmployeeFirstname";
            this.colAdministratorEmployeeFirstname.OptionsColumn.AllowEdit = false;
            this.colAdministratorEmployeeFirstname.OptionsColumn.AllowFocus = false;
            this.colAdministratorEmployeeFirstname.OptionsColumn.ReadOnly = true;
            this.colAdministratorEmployeeFirstname.Width = 134;
            // 
            // colAdministratorEmployeeNumber
            // 
            this.colAdministratorEmployeeNumber.Caption = "Administrator #";
            this.colAdministratorEmployeeNumber.FieldName = "AdministratorEmployeeNumber";
            this.colAdministratorEmployeeNumber.Name = "colAdministratorEmployeeNumber";
            this.colAdministratorEmployeeNumber.OptionsColumn.AllowEdit = false;
            this.colAdministratorEmployeeNumber.OptionsColumn.AllowFocus = false;
            this.colAdministratorEmployeeNumber.OptionsColumn.ReadOnly = true;
            this.colAdministratorEmployeeNumber.Width = 94;
            // 
            // colAdministrationType
            // 
            this.colAdministrationType.Caption = "Administration Type";
            this.colAdministrationType.FieldName = "AdministrationType";
            this.colAdministrationType.Name = "colAdministrationType";
            this.colAdministrationType.OptionsColumn.AllowEdit = false;
            this.colAdministrationType.OptionsColumn.AllowFocus = false;
            this.colAdministrationType.OptionsColumn.ReadOnly = true;
            this.colAdministrationType.Visible = true;
            this.colAdministrationType.VisibleIndex = 6;
            this.colAdministrationType.Width = 127;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp09001_Employee_ManagerTableAdapter
            // 
            this.sp09001_Employee_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingEmployee
            // 
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingEmployee.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingEmployee.GridControl = this.gridControlEmployee;
            // 
            // sp_HR_00009_GetEmployeeAddressesTableAdapter
            // 
            this.sp_HR_00009_GetEmployeeAddressesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_HR_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter
            // 
            this.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00037_Get_Employee_AbsencesTableAdapter
            // 
            this.sp_HR_00037_Get_Employee_AbsencesTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00046_Get_Employee_Working_PatternsTableAdapter
            // 
            this.sp_HR_00046_Get_Employee_Working_PatternsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00053_Get_Employee_SanctionsTableAdapter
            // 
            this.sp_HR_00053_Get_Employee_SanctionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp09113_HR_Qualifications_For_EmployeeTableAdapter
            // 
            this.sp09113_HR_Qualifications_For_EmployeeTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingWorkingPatternHeader
            // 
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingWorkingPatternHeader.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingWorkingPatternHeader.GridControl = this.gridControlWorkingPatternHeader;
            // 
            // xtraGridBlendingDiscipline
            // 
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingDiscipline.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingDiscipline.GridControl = this.gridControlDiscipline;
            // 
            // xtraGridBlendingTraining
            // 
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingTraining.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingTraining.GridControl = this.gridControlTraining;
            // 
            // xtraGridBlendingAddresses
            // 
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingAddresses.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingAddresses.GridControl = this.gridControlAddresses;
            // 
            // xtraGridBlendingNextOfKin
            // 
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingNextOfKin.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingNextOfKin.GridControl = this.gridControlNextOfKin;
            // 
            // xtraGridBlendingVetting
            // 
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingVetting.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingVetting.GridControl = this.gridControlVetting;
            // 
            // xtraGridBlendingWorkingPattern
            // 
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingWorkingPattern.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingWorkingPattern.GridControl = this.gridControlWorkingPattern;
            // 
            // xtraGridBlendingAbsences
            // 
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingAbsences.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingAbsences.GridControl = this.gridControlAbsences;
            // 
            // bar1
            // 
            this.bar1.BarName = "Time and Attendance";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiShowActiveHolidayYearOnly),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiAbsenceType),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefreshHolidays, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Custom 2";
            // 
            // beiShowActiveHolidayYearOnly
            // 
            this.beiShowActiveHolidayYearOnly.Caption = "Active Year Only:";
            this.beiShowActiveHolidayYearOnly.Edit = this.repositoryItemCheckEditShowActiveHoilidayYearOnly;
            this.beiShowActiveHolidayYearOnly.EditValue = 1;
            this.beiShowActiveHolidayYearOnly.EditWidth = 20;
            this.beiShowActiveHolidayYearOnly.Id = 27;
            this.beiShowActiveHolidayYearOnly.Name = "beiShowActiveHolidayYearOnly";
            this.beiShowActiveHolidayYearOnly.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Text = "Active Year Only - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Tick me to load only the active holiday year data. \r\n\r\nUntick me to load all data" +
    ".";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiShowActiveHolidayYearOnly.SuperTip = superToolTip1;
            // 
            // repositoryItemCheckEditShowActiveHoilidayYearOnly
            // 
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Caption = "(Tick if Yes)";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Name = "repositoryItemCheckEditShowActiveHoilidayYearOnly";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueUnchecked = 0;
            // 
            // beiAbsenceType
            // 
            this.beiAbsenceType.Caption = "Absence Type(s):";
            this.beiAbsenceType.Edit = this.repositoryItemPopupContainerEditAbsenceType;
            this.beiAbsenceType.EditValue = "No Absence Type Filter";
            this.beiAbsenceType.EditWidth = 198;
            this.beiAbsenceType.Id = 30;
            this.beiAbsenceType.Name = "beiAbsenceType";
            this.beiAbsenceType.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Text = "Absence Type(s) - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Select one or more types of absence from me to view just these types of data. \r\n\r" +
    "\nSelect nothing from me to view all absence records.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.beiAbsenceType.SuperTip = superToolTip2;
            // 
            // repositoryItemPopupContainerEditAbsenceType
            // 
            this.repositoryItemPopupContainerEditAbsenceType.AutoHeight = false;
            this.repositoryItemPopupContainerEditAbsenceType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditAbsenceType.Name = "repositoryItemPopupContainerEditAbsenceType";
            this.repositoryItemPopupContainerEditAbsenceType.PopupControl = this.popupContainerControlAbsenceTypes;
            this.repositoryItemPopupContainerEditAbsenceType.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditAbsenceType_QueryResultValue);
            // 
            // bbiRefreshHolidays
            // 
            this.bbiRefreshHolidays.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefreshHolidays.Caption = "Refresh";
            this.bbiRefreshHolidays.Id = 29;
            this.bbiRefreshHolidays.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefreshHolidays.ImageOptions.Image")));
            this.bbiRefreshHolidays.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefreshHolidays.ImageOptions.LargeImage")));
            this.bbiRefreshHolidays.Name = "bbiRefreshHolidays";
            this.bbiRefreshHolidays.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem3.Text = "Refresh - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to reload the Time and Attendance grid with any selected filters.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiRefreshHolidays.SuperTip = superToolTip3;
            this.bbiRefreshHolidays.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshHolidays_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 28;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // sp_HR_00077_Absence_Types_List_With_BlankTableAdapter
            // 
            this.sp_HR_00077_Absence_Types_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter
            // 
            this.sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingDepartmentWorked
            // 
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingDepartmentWorked.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingDepartmentWorked.GridControl = this.gridControlDepartmentWorked;
            // 
            // sp_HR_00131_Get_Employee_VettingTableAdapter
            // 
            this.sp_HR_00131_Get_Employee_VettingTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter
            // 
            this.sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter
            // 
            this.sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingHolidayYear
            // 
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingHolidayYear.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingHolidayYear.GridControl = this.gridControlHolidayYear;
            // 
            // sp_HR_00172_Employee_BonusesTableAdapter
            // 
            this.sp_HR_00172_Employee_BonusesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingBonuses
            // 
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingBonuses.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingBonuses.GridControl = this.gridControlBonuses;
            // 
            // sp_HR_00016_Pensions_For_EmployeeTableAdapter
            // 
            this.sp_HR_00016_Pensions_For_EmployeeTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingPension
            // 
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingPension.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingPension.GridControl = this.gridControlPension;
            // 
            // sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter
            // 
            this.sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingCRM
            // 
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingCRM.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingCRM.GridControl = this.gridControlCRM;
            // 
            // sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter
            // 
            this.sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingReference
            // 
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingReference.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingReference.GridControl = this.gridControlReference;
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDepartment, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiEmployee),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiActive, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterSelected, true)});
            this.bar2.OptionsBar.DrawDragBorder = false;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Custom 3";
            // 
            // beiDepartment
            // 
            this.beiDepartment.Caption = "Department(s):";
            this.beiDepartment.Edit = this.repositoryItemPopupContainerEditDepartment;
            this.beiDepartment.EditValue = "No Department Filter";
            this.beiDepartment.EditWidth = 160;
            this.beiDepartment.Id = 31;
            this.beiDepartment.Name = "beiDepartment";
            this.beiDepartment.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem4.Text = "Department(s) - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Select one or more department from me to view just those departments. \r\n\r\nSelect " +
    "nothing from me to view all departments.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.beiDepartment.SuperTip = superToolTip4;
            // 
            // repositoryItemPopupContainerEditDepartment
            // 
            this.repositoryItemPopupContainerEditDepartment.AutoHeight = false;
            this.repositoryItemPopupContainerEditDepartment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDepartment.Name = "repositoryItemPopupContainerEditDepartment";
            this.repositoryItemPopupContainerEditDepartment.PopupControl = this.popupContainerControlDepartments;
            this.repositoryItemPopupContainerEditDepartment.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditDepartment.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDepartment_QueryResultValue);
            // 
            // beiEmployee
            // 
            this.beiEmployee.Caption = "Employee(s):";
            this.beiEmployee.Edit = this.repositoryItemPopupContainerEditEmployee;
            this.beiEmployee.EditValue = "No Employee Filter";
            this.beiEmployee.EditWidth = 158;
            this.beiEmployee.Id = 32;
            this.beiEmployee.Name = "beiEmployee";
            this.beiEmployee.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem5.Text = "Employee(s) - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Select one or more employees from me to view just those employees.\r\n\r\nSelect noth" +
    "ing from me to view all employees.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.beiEmployee.SuperTip = superToolTip5;
            // 
            // repositoryItemPopupContainerEditEmployee
            // 
            this.repositoryItemPopupContainerEditEmployee.AutoHeight = false;
            this.repositoryItemPopupContainerEditEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditEmployee.Name = "repositoryItemPopupContainerEditEmployee";
            this.repositoryItemPopupContainerEditEmployee.PopupControl = this.popupContainerControlEmployees;
            this.repositoryItemPopupContainerEditEmployee.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditEmployee.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditEmployee_QueryResultValue);
            // 
            // beiActive
            // 
            this.beiActive.Caption = "Active Only:";
            this.beiActive.Edit = this.repositoryItemCheckEditActiveOnly;
            this.beiActive.EditValue = 1;
            this.beiActive.EditWidth = 20;
            this.beiActive.Id = 33;
            this.beiActive.Name = "beiActive";
            this.beiActive.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEditActiveOnly
            // 
            this.repositoryItemCheckEditActiveOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AutoHeight = false;
            this.repositoryItemCheckEditActiveOnly.Caption = "Check";
            this.repositoryItemCheckEditActiveOnly.Name = "repositoryItemCheckEditActiveOnly";
            this.repositoryItemCheckEditActiveOnly.ValueChecked = 1;
            this.repositoryItemCheckEditActiveOnly.ValueUnchecked = 0;
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 34;
            this.bbiRefresh.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bciFilterSelected
            // 
            this.bciFilterSelected.Caption = "Selected";
            this.bciFilterSelected.Id = 36;
            this.bciFilterSelected.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.ImageOptions.Image")));
            this.bciFilterSelected.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.ImageOptions.LargeImage")));
            this.bciFilterSelected.Name = "bciFilterSelected";
            this.bciFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem6.Appearance.Options.UseImage = true;
            toolTipTitleItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem6.Text = "Filter Selected - Information";
            toolTipItem6.LeftIndent = 6;
            toolTipItem6.Text = resources.GetString("toolTipItem6.Text");
            superToolTip6.Items.Add(toolTipTitleItem6);
            superToolTip6.Items.Add(toolTipItem6);
            this.bciFilterSelected.SuperTip = superToolTip6;
            this.bciFilterSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterSelected_CheckedChanged);
            // 
            // sp_HR_00114_Department_Filter_ListTableAdapter
            // 
            this.sp_HR_00114_Department_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter
            // 
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00213_Employee_PayRisesTableAdapter
            // 
            this.sp_HR_00213_Employee_PayRisesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingPayRise
            // 
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingPayRise.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingPayRise.GridControl = this.gridControlPayRise;
            // 
            // xtraGridBlendingShares
            // 
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingShares.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingShares.GridControl = this.gridControlShares;
            // 
            // sp_HR_00224_Employee_SharesTableAdapter
            // 
            this.sp_HR_00224_Employee_SharesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingAdminister
            // 
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingAdminister.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingAdminister.GridControl = this.gridControlAdminister;
            // 
            // sp_HR_00248_Employee_AdministeringTableAdapter
            // 
            this.sp_HR_00248_Employee_AdministeringTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlendingSubsistence
            // 
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlendingSubsistence.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlendingSubsistence.GridControl = this.gridControlSubsistence;
            // 
            // sp_HR_00257_Employee_SubsistenceTableAdapter
            // 
            this.sp_HR_00257_Employee_SubsistenceTableAdapter.ClearBeforeFill = true;
            // 
            // sp01059_Core_Alphabet_ListTableAdapter
            // 
            this.sp01059_Core_Alphabet_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Employee_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1194, 517);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Manager";
            this.Text = "Employee Manager";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.indexGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01059CoreAlphabetListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.indexGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09001EmployeeManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditCreateStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMoney)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentageMode2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditUnknown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlAbsenceTypes)).EndInit();
            this.popupContainerControlAbsenceTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00077AbsenceTypesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).EndInit();
            this.popupContainerControlEmployees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).EndInit();
            this.popupContainerControlDepartments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageAbsences.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer11)).EndInit();
            this.gridSplitContainer11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHolidayYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00158HolidayYearsForEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHolidayYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DPNoDescriptor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericUnknown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer12)).EndInit();
            this.gridSplitContainer12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAbsences)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_HR_00037_Get_Employee_AbsencesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAbsences)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLongDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsAbsences)).EndInit();
            this.xtraTabPageVetting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlVetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00131GetEmployeeVettingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewVetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryIteDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DPDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditVettingLinkedDocs)).EndInit();
            this.xtraTabPageReferences.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer15)).EndInit();
            this.gridSplitContainer15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00200EmployeeReferencesLinkedToEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewReference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditDateTime15.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEditDateTime15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentReferences)).EndInit();
            this.xtraTabPageAddresses.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).EndInit();
            this.gridSplitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAddresses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_HR_00009_GetEmployeeAddressesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAddresses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime3)).EndInit();
            this.xtraTabPageNextOfKin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).EndInit();
            this.gridSplitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlNextOfKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewNextOfKin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            this.xtraTabPageDeptsWorked.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDepartmentWorked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00001EmployeeDeptsWorkedLinkedToContractBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDepartmentWorked)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentDeptWorked)).EndInit();
            this.xtraTabPageWorkPatterns.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer10)).EndInit();
            this.gridSplitContainer10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWorkingPatternHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00154EmployeeWorkingPatternHeadersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWorkingPatternHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlWorkingPattern)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_HR_00046_Get_Employee_Working_PatternsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewWorkingPattern)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditWeekNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTime.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMinutes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            this.xtraTabPageShares.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer17)).EndInit();
            this.gridSplitContainer17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlShares)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00224EmployeeSharesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewShares)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditSharesLinkedDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsUnknown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShareUnitsPercentage)).EndInit();
            this.xtraTabPageBonuses.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBonuses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00172EmployeeBonusesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBonuses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditBonusLinkedDocuments)).EndInit();
            this.xtraTabPagePayRises.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer16)).EndInit();
            this.gridSplitContainer16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPayRise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00213EmployeePayRisesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPayRise)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditPayRiseLinkedDocuments)).EndInit();
            this.xtraTabPagePensions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer13)).EndInit();
            this.gridSplitContainer13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00016PensionsForEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditMonths)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentPension)).EndInit();
            this.xtraTabPageSubsistence.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSubsistence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00257EmployeeSubsistenceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSubsistence)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit20)).EndInit();
            this.xtraTabPageTraining.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).EndInit();
            this.gridSplitContainer8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlTraining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09113HRQualificationsForEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewTraining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateOnky)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsTraining)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime12)).EndInit();
            this.xtraTabPageDiscipline.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer9)).EndInit();
            this.gridSplitContainer9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDiscipline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00053GetEmployeeSanctionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDiscipline)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric0DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentSanction)).EndInit();
            this.xtraTabPageCRM.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer14)).EndInit();
            this.gridSplitContainer14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlCRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00193EmployeeCRMLinkedToEmployeeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTimeCRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEditCRM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentsCRM)).EndInit();
            this.xtraTabPageAdminister.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAdminister)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00248EmployeeAdministeringBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAdminister)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditAbsenceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageTraining;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageBonuses;
        private DevExpress.XtraGrid.GridControl gridControlEmployee;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewEmployee;
        private DevExpress.XtraGrid.GridControl gridControlTraining;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewTraining;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlBonuses;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBonuses;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp09001EmployeeManagerBindingSource;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_CoreTableAdapters.sp09001_Employee_ManagerTableAdapter sp09001_Employee_ManagerTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeId;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colMiddleNames;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colCommonName;
        private DevExpress.XtraGrid.Columns.GridColumn colComments;
        private DevExpress.XtraGrid.Columns.GridColumn colJoinedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDoB;
        private DevExpress.XtraGrid.Columns.GridColumn colNatInsuranceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkEmailAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colEthnicityId;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonalMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonalEmailAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colDateJoinedPension;
        private DevExpress.XtraGrid.Columns.GridColumn colDateLeftPension;
        private DevExpress.XtraGrid.Columns.GridColumn colYearsToAutoReEnrolment;
        private DevExpress.XtraGrid.Columns.GridColumn colNextDateOfEnrolment;
        private DevExpress.XtraGrid.Columns.GridColumn colGenderID;
        private DevExpress.XtraGrid.Columns.GridColumn colGender;
        private DevExpress.XtraGrid.Columns.GridColumn colEthnicity;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingEmployee;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAddresses;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageNextOfKin;
        private System.Windows.Forms.BindingSource sp_HR_00009_GetEmployeeAddressesBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00009_GetEmployeeAddressesTableAdapter sp_HR_00009_GetEmployeeAddressesTableAdapter;
        private DataSet_HR_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraGrid.GridControl gridControlAddresses;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAddresses;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeId1;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname1;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressId;
        private DevExpress.XtraGrid.Columns.GridColumn colIsCurrentAddress;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colPostCode;
        private DevExpress.XtraGrid.Columns.GridColumn colLandlineNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colComments1;
        private System.Windows.Forms.BindingSource sp_HR_00021_Get_Next_Of_Kin_For_EmployeesBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlNextOfKin;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewNextOfKin;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeId2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle1;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname2;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname2;
        private DevExpress.XtraGrid.Columns.GridColumn colPostCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colLandline;
        private DevExpress.XtraGrid.Columns.GridColumn colMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPriorityOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRelationshipId;
        private DevExpress.XtraGrid.Columns.GridColumn colComments2;
        private DevExpress.XtraGrid.Columns.GridColumn colRelationship;
        private System.Windows.Forms.BindingSource sp_HR_00037_Get_Employee_AbsencesBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00037_Get_Employee_AbsencesTableAdapter sp_HR_00037_Get_Employee_AbsencesTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageDiscipline;
        private DevExpress.XtraGrid.GridControl gridControlDiscipline;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDiscipline;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageVetting;
        private System.Windows.Forms.BindingSource sp_HR_00046_Get_Employee_Working_PatternsBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00046_Get_Employee_Working_PatternsTableAdapter sp_HR_00046_Get_Employee_Working_PatternsTableAdapter;
        private System.Windows.Forms.BindingSource spHR00053GetEmployeeSanctionsBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00053_Get_Employee_SanctionsTableAdapter sp_HR_00053_Get_Employee_SanctionsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.GridControl gridControlAbsences;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAbsences;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.GridControl gridControlVetting;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewVetting;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private System.Windows.Forms.BindingSource sp09113HRQualificationsForEmployeeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationFromID;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostToCompany;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colSummerMaintenance;
        private DevExpress.XtraGrid.Columns.GridColumn colWinterMaintenance;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArb;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityRail;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlan;
        private DevExpress.XtraGrid.Columns.GridColumn colGUID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationType;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationFrom;
        private DataSet_HR_CoreTableAdapters.sp09113_HR_Qualifications_For_EmployeeTableAdapter sp09113_HR_Qualifications_For_EmployeeTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToEmployee;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingWorkingPatternHeader;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingDiscipline;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingTraining;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingAddresses;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingNextOfKin;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingVetting;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingWorkingPattern;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingAbsences;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeHolidayYearId;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceSubTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShortDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsUsed;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP2;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor1;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLongDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDeclaredDisability;
        private DevExpress.XtraGrid.Columns.GridColumn colPayCategoryId;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWorkRelated;
        private DevExpress.XtraGrid.Columns.GridColumn colComments4;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurnameForename1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeForename1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveHolidayYear;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAge;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine21;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine31;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine41;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine51;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber4;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric0DP2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer7;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarEditItem beiShowActiveHolidayYearOnly;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveHoilidayYearOnly;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshHolidays;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarEditItem beiAbsenceType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditAbsenceType;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlAbsenceTypes;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.SimpleButton btnAbsenceTypeFilterOK;
        private System.Windows.Forms.BindingSource spHR00077AbsenceTypesListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00077_Absence_Types_List_With_BlankTableAdapter sp_HR_00077_Absence_Types_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID8;
        private System.Windows.Forms.BindingSource spHR00001EmployeeDeptsWorkedLinkedToContractBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingDepartmentWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colMaritalStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colReligion;
        private DevExpress.XtraGrid.Columns.GridColumn colDisability;
        private DevExpress.XtraGrid.Columns.GridColumn colContinuousServiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPicturePath;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffRecordCreated;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditCreateStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colContractNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colContractType1;
        private DevExpress.XtraGrid.Columns.GridColumn colGenericJobTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colJobType;
        private DevExpress.XtraGrid.Columns.GridColumn colContractBasis;
        private DevExpress.XtraGrid.Columns.GridColumn colContractStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupCommencementDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colAppointmentReason1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecruitmentSourceType1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecruitmentSourceDetail1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecruitmentCost1;
        private DevExpress.XtraGrid.Columns.GridColumn colActualHours;
        private DevExpress.XtraGrid.Columns.GridColumn colFTEHours;
        private DevExpress.XtraGrid.Columns.GridColumn colFTERatio;
        private DevExpress.XtraGrid.Columns.GridColumn colContractIssuedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContractReceivedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedHandbook;
        private DevExpress.XtraGrid.Columns.GridColumn colTUPE1;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferredFromLocalGovernment1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMoney;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage2;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferredFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colNJCConditionsApply1;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginatingCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colInductionDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInductionChecklistReturned;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor2;
        private DevExpress.XtraGrid.Columns.GridColumn colAnnualLeaveAnniversaryDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colBasicLeaveEntitlementFTE;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDays;
        private DevExpress.XtraGrid.Columns.GridColumn colBasicLeaveEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidayEntitlementFTE;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidayEntitlement1;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyLeaveIncrementStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyLeaveIncrementAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumLeaveEntitlement1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastPayReviewDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colNextPayReviewDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastIncreaseAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colLastIncreasePercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colTerminationDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastDateWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colReasonForLastDateWorkedDifference;
        private DevExpress.XtraGrid.Columns.GridColumn colLeavingReason1;
        private DevExpress.XtraGrid.Columns.GridColumn colLeavingRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colConsiderReEmployement;
        private DevExpress.XtraGrid.Columns.GridColumn colConsiderReEmploymentReason1;
        private DevExpress.XtraGrid.Columns.GridColumn colExitInterviewDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colExitInterviewerName1;
        private DevExpress.XtraGrid.Columns.GridColumn colTrainingCostsToRecover;
        private DevExpress.XtraGrid.Columns.GridColumn colSchemeCostsToRecover;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentCostsToRecover;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditUnknown;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentageMode2;
        private DevExpress.XtraGrid.Columns.GridColumn colSalary;
        private DevExpress.XtraGrid.Columns.GridColumn colSalaryBanding1;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollType;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentType1;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentFrequency1;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollReference1;
        private DevExpress.XtraGrid.Columns.GridColumn colPaymentAmount1;
        private DevExpress.XtraGrid.Columns.GridColumn colMonthlyPayDayNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colSickPayEntitlementUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colSickPayUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colElegibleForBonus;
        private DevExpress.XtraGrid.Columns.GridColumn colElegibleForOvertime;
        private DevExpress.XtraGrid.Columns.GridColumn colElegibleForShares;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonalTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colBankName;
        private DevExpress.XtraGrid.Columns.GridColumn colBankSortCode;
        private DevExpress.XtraGrid.Columns.GridColumn colBankAccountNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colBankAccountName;
        private DevExpress.XtraGrid.Columns.GridColumn colPayslipEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colPayslipPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colAnyOtherOwing;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysAccrued;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysBalance;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysTaken1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer6;
        private DevExpress.XtraGrid.Columns.GridColumn colFromDate2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime3;
        private DevExpress.XtraGrid.Columns.GridColumn colToDate1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer8;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer9;
        private DevExpress.XtraGrid.Columns.GridColumn colSanctionID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber5;
        private DevExpress.XtraGrid.Columns.GridColumn colERIssueTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colERIssueType;
        private DevExpress.XtraGrid.Columns.GridColumn colHRManagerID;
        private DevExpress.XtraGrid.Columns.GridColumn colHRManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusValue;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigatedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigatedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHeardByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeardByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingNoteTakerName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingEmployeeRepresentativeName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeIssuedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeIssuedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealHeardByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealHeardByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealNoteTakerName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealEmployeeRepresentativeName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colLive;
        private DevExpress.XtraGrid.Columns.GridColumn colSanctionOutcome;
        private System.Windows.Forms.BindingSource spHR00131GetEmployeeVettingBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID5;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber7;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingType;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colDateIssued;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryIteDateTime4;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationPeriodDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DPDays;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEssentialForRole;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colRestrictionCount;
        private DataSet_HR_CoreTableAdapters.sp_HR_00131_Get_Employee_VettingTableAdapter sp_HR_00131_Get_Employee_VettingTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiredStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageDeptsWorked;
        private DevExpress.XtraGrid.GridControl gridControlDepartmentWorked;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDepartmentWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeDeptWorkedID;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentLocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionID;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPositionID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportsToStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colFromDate1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colToDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn colBaseType;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurnameForename2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname3;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeForename2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber6;
        private DevExpress.XtraGrid.Columns.GridColumn colContractNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colReportsToStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationName;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentreCode;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWorkPatterns;
        private DevExpress.XtraGrid.GridControl gridControlWorkingPattern;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWorkingPattern;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemTime;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkingPatternID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID7;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName3;
        private DevExpress.XtraGrid.Columns.GridColumn colWeekNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colDayOfWeek;
        private DevExpress.XtraGrid.Columns.GridColumn colDayOfTheWeek;
        private DevExpress.XtraGrid.Columns.GridColumn colStartTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colBreakLength;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMinutes;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidForBreaks;
        private DevExpress.XtraGrid.Columns.GridColumn colCalculatedHours;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeDepartmentWorkedID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocationName1;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegionName1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditWeekNumber;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridControl gridControlWorkingPatternHeader;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewWorkingPatternHeader;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private System.Windows.Forms.BindingSource spHR00154EmployeeWorkingPatternHeadersBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber8;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname5;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DataSet_HR_CoreTableAdapters.sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer10;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDates;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderEndDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHeaderDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colActive2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit10;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptorID;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAbsences;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraGrid.GridControl gridControlHolidayYear;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHolidayYear;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DPNoDescriptor;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private System.Windows.Forms.BindingSource spHR00158HolidayYearsForEmployeeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn colMustTakeCertainHolidays;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DataSet_HR_CoreTableAdapters.sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericUnknown;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingHolidayYear;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer11;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer12;
        private DevExpress.XtraGrid.Columns.GridColumn colEWCDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedReturnDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHalfDayEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colHalfDayStart;
        private DevExpress.XtraGrid.Columns.GridColumn colAdditionalDaysPaid;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit12;
        private System.Windows.Forms.BindingSource spHR00172EmployeeBonusesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID3;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonus;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonusDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonusAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusDatePaid;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks6;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname3;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordStatus;
        private DataSet_HR_CoreTableAdapters.sp_HR_00172_Employee_BonusesTableAdapter sp_HR_00172_Employee_BonusesTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingBonuses;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePensions;
        private DevExpress.XtraGrid.GridControl gridControlPension;
        private System.Windows.Forms.BindingSource spHR00016PensionsForEmployeeBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPension;
        private DevExpress.XtraGrid.Columns.GridColumn colPensionID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID9;
        private DevExpress.XtraGrid.Columns.GridColumn colSchemeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSchemeTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colContributionTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colMatchedContribution;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit12;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimumContributionAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency13;
        private DevExpress.XtraGrid.Columns.GridColumn colMaximumContributionAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentContributionAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colProviderID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime13;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colFixedInitialTermMonths;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditMonths;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalEmployeeContributionToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalEmployerContributionToDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks7;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit13;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName5;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname6;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber9;
        private DevExpress.XtraGrid.Columns.GridColumn colSchemeType;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollType1;
        private DevExpress.XtraGrid.Columns.GridColumn colContributionType;
        private DevExpress.XtraGrid.Columns.GridColumn colProvider;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DataSet_HR_CoreTableAdapters.sp_HR_00016_Pensions_For_EmployeeTableAdapter sp_HR_00016_Pensions_For_EmployeeTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colContributionCount;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingPension;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer13;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocuments;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditVettingLinkedDocs;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount2;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentPension;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount3;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentSanction;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount4;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditBonusLinkedDocuments;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount5;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentDeptWorked;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount6;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsAbsences;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount7;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsTraining;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime12;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit14;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageCRM;
        private DevExpress.XtraGrid.GridControl gridControlCRM;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCRM;
        private System.Windows.Forms.BindingSource spHR00193EmployeeCRMLinkedToEmployeeBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colCRMID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID10;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDueDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTimeCRM;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMadeDate;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactWithEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID2;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEditCRM;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName6;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname7;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname5;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber10;
        private DevExpress.XtraGrid.Columns.GridColumn colContactWithEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirection;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus2;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount8;
        private DataSet_HR_CoreTableAdapters.sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentsCRM;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer14;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingCRM;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysRemaining;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageReferences;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private DevExpress.XtraGrid.GridControl gridControlReference;
        private System.Windows.Forms.BindingSource spHR00200EmployeeReferencesLinkedToEmployeeBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewReference;
        private DataSet_HR_CoreTableAdapters.sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID11;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colCommunicationMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn colSuppliedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colSuppliedByContactTel;
        private DevExpress.XtraGrid.Columns.GridColumn colSuppliedByContactEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colRelationshipTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRequested;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEditDateTime15;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceSatisfactory;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit14;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks9;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName7;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname8;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname6;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber11;
        private DevExpress.XtraGrid.Columns.GridColumn colCommunicationMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceType;
        private DevExpress.XtraGrid.Columns.GridColumn colRelationshipType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount9;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentReferences;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer15;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingReference;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarEditItem beiDepartment;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDepartment;
        private DevExpress.XtraBars.BarEditItem beiEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditEmployee;
        private DevExpress.XtraBars.BarEditItem beiActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditActiveOnly;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlEmployees;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn35;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit16;
        private DevExpress.XtraEditors.SimpleButton btnEmployeeFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDepartments;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraEditors.SimpleButton btnDepartmentFilterOK;
        private System.Windows.Forms.BindingSource spHR00114DepartmentFilterListBindingSource;
        private System.Windows.Forms.BindingSource spHR00115EmployeesByDeptFilterListBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter sp_HR_00114_Department_Filter_ListTableAdapter;
        private DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTitleID;
        private DevExpress.XtraGrid.Columns.GridColumn colPositionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAgreedReferenceInPlace;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePayRises;
        private DevExpress.XtraGrid.GridControl gridControlPayRise;
        private System.Windows.Forms.BindingSource spHR00213EmployeePayRisesBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPayRise;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime6;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit18;
        private DataSet_HR_CoreTableAdapters.sp_HR_00213_Employee_PayRisesTableAdapter sp_HR_00213_Employee_PayRisesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPayRiseID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID12;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousPayAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colPayRiseAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colPayRisePercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colNewPayAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colPayRiseDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAppliedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks10;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName8;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname9;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname7;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber12;
        private DevExpress.XtraGrid.Columns.GridColumn colAppliedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount10;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage5;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingPayRise;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditPayRiseLinkedDocuments;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer16;
        private DevExpress.XtraGrid.Columns.GridColumn colEligibleForMedicalCover;
        private DevExpress.XtraGrid.Columns.GridColumn colMedicalCoverProvider;
        private DevExpress.XtraGrid.Columns.GridColumn colMedicalCoverEmployerContribution;
        private DevExpress.XtraGrid.Columns.GridColumn colMedicalCoverEmployerContributionDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colMedicalCoverEmployerContributionFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colCTWScheme;
        private DevExpress.XtraGrid.Columns.GridColumn colCTWSchemeStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCTWSchemeEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCTWSchemeValue;
        private DevExpress.XtraGrid.Columns.GridColumn colCTWSchemeEndOption;
        private DevExpress.XtraGrid.Columns.GridColumn colChildCareVouchers;
        private DevExpress.XtraGrid.Columns.GridColumn colExitInterviewRequired;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageShares;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.GridControl gridControlShares;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewShares;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit16;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShareUnitsUnknown;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit19;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditSharesLinkedDocuments;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingShares;
        private System.Windows.Forms.BindingSource spHR00224EmployeeSharesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSharesID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID13;
        private DevExpress.XtraGrid.Columns.GridColumn colShareTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colActive3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateReceived1;
        private DevExpress.XtraGrid.Columns.GridColumn colShareUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colShareUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShareUnitsCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks11;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName9;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname10;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname8;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber13;
        private DevExpress.XtraGrid.Columns.GridColumn colShareType;
        private DevExpress.XtraGrid.Columns.GridColumn colShareUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount11;
        private DataSet_HR_CoreTableAdapters.sp_HR_00224_Employee_SharesTableAdapter sp_HR_00224_Employee_SharesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShareUnitsUnits;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShareUnitsPercentage;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer17;
        private DevExpress.XtraBars.BarCheckItem bciFilterSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colDummyShift;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colArchived;
        private DevExpress.XtraGrid.Columns.GridColumn colAssessmentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNoticePeriodUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colNoticePeriodUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colInviteSummerBBQ;
        private DevExpress.XtraGrid.Columns.GridColumn colInviteWinterParty;
        private DevExpress.XtraGrid.Columns.GridColumn colInviteManagementConference;
        private DevExpress.XtraGrid.Columns.GridColumn colSickPayPercentage1;
        private DevExpress.XtraGrid.Columns.GridColumn colSickPayUnits2;
        private DevExpress.XtraGrid.Columns.GridColumn colSickPayUnitDescriptor2;
        private DevExpress.XtraGrid.Columns.GridColumn colSickPayPercentage2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInt;
        private DevExpress.XtraGrid.Columns.GridColumn colVisaRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colVisaType;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidCSP;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidHalfPay;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidOther;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidUnpaid;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceComment;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeePassword;
        private DevExpress.XtraGrid.Columns.GridColumn colBiography;
        private DevExpress.XtraGrid.Columns.GridColumn colShiftFTE;
        private DevExpress.XtraGrid.Columns.GridColumn colExtraDaysHolidayYear1;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromWeb;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colSuppressBirthday;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAdminister;
        private DevExpress.XtraGrid.GridControl gridControlAdminister;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAdminister;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime18;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingAdminister;
        private System.Windows.Forms.BindingSource spHR00248EmployeeAdministeringBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeAdministratorId;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeId14;
        private DevExpress.XtraGrid.Columns.GridColumn colAdministratorId;
        private DevExpress.XtraGrid.Columns.GridColumn colDateFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTo;
        private DevExpress.XtraGrid.Columns.GridColumn colActive4;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName10;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname11;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname9;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber14;
        private DevExpress.XtraGrid.Columns.GridColumn colAdministratorEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colAdministratorEmployeeSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colAdministratorEmployeeFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colAdministratorEmployeeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colAdministrationType;
        private DataSet_HR_CoreTableAdapters.sp_HR_00248_Employee_AdministeringTableAdapter sp_HR_00248_Employee_AdministeringTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit18;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousEmploymentSalary;
        private DevExpress.XtraGrid.Columns.GridColumn colSalaryExpectation;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDaysUntilExpiry;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateOnky;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentTermsAndConditions;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentTermsAndConditionsDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOptedIn;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOptedOut;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSubsistence;
        private DevExpress.XtraGrid.GridControl gridControlSubsistence;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSubsistence;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime14;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency14;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit20;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlendingSubsistence;
        private DevExpress.XtraGrid.Columns.GridColumn colOptOutFormIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colOptInToSubsistence;
        private System.Windows.Forms.BindingSource spHR00257EmployeeSubsistenceBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSubsistenceID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID15;
        private DevExpress.XtraGrid.Columns.GridColumn colSubsistenceTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate3;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate3;
        private DevExpress.XtraGrid.Columns.GridColumn colRate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRate1Units;
        private DevExpress.XtraGrid.Columns.GridColumn colRate1UnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colRate2;
        private DevExpress.XtraGrid.Columns.GridColumn colRate2Units;
        private DevExpress.XtraGrid.Columns.GridColumn colRate2UnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colSalarySacrificeAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks12;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName11;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname12;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname10;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber15;
        private DevExpress.XtraGrid.Columns.GridColumn colRate1UnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colRate2UnitDescriptor;
        private DataSet_HR_CoreTableAdapters.sp_HR_00257_Employee_SubsistenceTableAdapter sp_HR_00257_Employee_SubsistenceTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSubsistenceType;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP_14;
        private DevExpress.XtraGrid.Columns.GridColumn colActive5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit17;
        private DevExpress.XtraGrid.GridControl indexGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView indexGridView;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private System.Windows.Forms.BindingSource sp01059CoreAlphabetListBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01059_Core_Alphabet_ListTableAdapter sp01059_Core_Alphabet_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colalpha;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Columns.GridColumn colDISCoverEligible;
        private DevExpress.XtraGrid.Columns.GridColumn colDISLevelCover;
        private DevExpress.XtraGrid.Columns.GridColumn colDISScheme;
        private DevExpress.XtraGrid.Columns.GridColumn colIPEligible;
        private DevExpress.XtraGrid.Columns.GridColumn colIPLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colIPDeferralPeriod;
        private DevExpress.XtraGrid.Columns.GridColumn colIPScheme;
    }
}

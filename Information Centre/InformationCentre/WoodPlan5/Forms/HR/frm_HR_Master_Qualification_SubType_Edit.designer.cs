namespace WoodPlan5
{
    partial class frm_HR_Master_Qualification_SubType_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Master_Qualification_SubType_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule5 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue5 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.alignmentControl1 = new BaseObjects.AlignmentControl();
            this.maintainQualificationReplaces = new WoodPlan5.MaintainQualificationReplaces();
            this.maintainJointMemberQualifications = new WoodPlan5.MaintainJointMemberQualifications();
            this.maintainQualificationAllocation = new WoodPlan5.MaintainQualificationAllocation();
            this.maintainQualificationRefreshedBy = new WoodPlan5.MaintainQualificationRefreshedBy();
            this.maintainQualificationRefreshes = new WoodPlan5.MaintainQualificationRefreshes();
            this.maintainQualificationAwardingBody = new WoodPlan5.MaintainQualificationAwardingBody();
            this.OrderIDSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.spHR00234MasterQualificationSubTypeEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.QualificationSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.QualificationTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00236MasterQualificationTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.startDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.endDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.expiryPeriodSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.QualificationSubTypeStatusTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00001QualificationSubTypeStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ExpiryPeriodTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00003QualificationSubTypeExpiryPeriodBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RefreshIntervalTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00005QualificationSubTypeRefreshIntervalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.refreshIntervalSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.spinEdit3 = new DevExpress.XtraEditors.SpinEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.ValidityTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00002QualificationSubTypeValidityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.refreshTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00004QualificationSubTypeRefreshBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.maintainJointOwnerQualifications = new WoodPlan5.MaintainJointOwnerQualifications();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.ItemForQualificationSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQualificationTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOrderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInUniversityOfGC = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQualificationSubTypeStatus = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValidityTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpiryPeriodType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpiryPeriodValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRefreshTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRefreshIntervalType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForGCRecognised = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForCPDHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRefreshIntervalValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForGCDeliverable = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHideFromTotalview = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCertificateRequired = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.awardingBodyLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.refreshesLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.refreshedByLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.allocationLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.replacesLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter();
            this.sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter();
            this.sp_TR_00003_Qualification_SubType_Expiry_PeriodTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00003_Qualification_SubType_Expiry_PeriodTableAdapter();
            this.sp_TR_00005_Qualification_SubType_Refresh_IntervalTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00005_Qualification_SubType_Refresh_IntervalTableAdapter();
            this.sp_TR_00001_Qualification_SubType_StatusTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00001_Qualification_SubType_StatusTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_TR_CoreTableAdapters.TableAdapterManager();
            this.sp_TR_00002_Qualification_SubType_ValidityTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00002_Qualification_SubType_ValidityTableAdapter();
            this.sp_TR_00004_Qualification_SubType_RefreshTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00004_Qualification_SubType_RefreshTableAdapter();
            this.sp00312_Accident_Body_Areas_With_BlankTableAdapter1 = new WoodPlan5.DataSet_AccidentTableAdapters.sp00312_Accident_Body_Areas_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrderIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00234MasterQualificationSubTypeEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00236MasterQualificationTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.expiryPeriodSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeStatusTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00001QualificationSubTypeStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryPeriodTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00003QualificationSubTypeExpiryPeriodBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshIntervalTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00005QualificationSubTypeRefreshIntervalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshIntervalSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidityTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00002QualificationSubTypeValidityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00004QualificationSubTypeRefreshBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInUniversityOfGC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValidityTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryPeriodType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryPeriodValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshIntervalType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCRecognised)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCPDHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshIntervalValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCDeliverable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHideFromTotalview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCertificateRequired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.awardingBodyLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshesLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshedByLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allocationLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.replacesLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(872, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 555);
            this.barDockControlBottom.Size = new System.Drawing.Size(872, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 529);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(872, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 529);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "StatusTypeID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ExpiryPeriodTypeID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "RefreshIntervalTypeID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Width = 53;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ValidityTypeID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 53;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "ID";
            this.gridColumn13.FieldName = "RefreshTypeID";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.Width = 53;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Expiry Period Type";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Refresh Interval Type";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Validity Type";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(872, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 555);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(872, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 529);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(872, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 529);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.alignmentControl1);
            this.dataLayoutControl1.Controls.Add(this.maintainQualificationReplaces);
            this.dataLayoutControl1.Controls.Add(this.maintainJointMemberQualifications);
            this.dataLayoutControl1.Controls.Add(this.maintainQualificationAllocation);
            this.dataLayoutControl1.Controls.Add(this.maintainQualificationRefreshedBy);
            this.dataLayoutControl1.Controls.Add(this.maintainQualificationRefreshes);
            this.dataLayoutControl1.Controls.Add(this.maintainQualificationAwardingBody);
            this.dataLayoutControl1.Controls.Add(this.OrderIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.QualificationSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QualificationTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.startDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.endDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.expiryPeriodSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.QualificationSubTypeStatusTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ExpiryPeriodTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.RefreshIntervalTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.refreshIntervalSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.spinEdit3);
            this.dataLayoutControl1.Controls.Add(this.checkEdit3);
            this.dataLayoutControl1.Controls.Add(this.checkEdit4);
            this.dataLayoutControl1.Controls.Add(this.checkEdit5);
            this.dataLayoutControl1.Controls.Add(this.ValidityTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.refreshTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.maintainJointOwnerQualifications);
            this.dataLayoutControl1.Controls.Add(this.checkEdit1);
            this.dataLayoutControl1.Controls.Add(this.checkEdit2);
            this.dataLayoutControl1.DataSource = this.spHR00234MasterQualificationSubTypeEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForQualificationSubTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(550, 40, 688, 619);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(872, 529);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // alignmentControl1
            // 
            this.alignmentControl1.Location = new System.Drawing.Point(12, 531);
            this.alignmentControl1.Name = "alignmentControl1";
            this.alignmentControl1.Size = new System.Drawing.Size(831, 1);
            this.alignmentControl1.TabIndex = 46;
            // 
            // maintainQualificationReplaces
            // 
            this.maintainQualificationReplaces.Location = new System.Drawing.Point(36, 461);
            this.maintainQualificationReplaces.Name = "maintainQualificationReplaces";
            this.maintainQualificationReplaces.Size = new System.Drawing.Size(783, 42);
            this.maintainQualificationReplaces.TabIndex = 45;
            // 
            // maintainJointMemberQualifications
            // 
            this.maintainJointMemberQualifications.Location = new System.Drawing.Point(36, 461);
            this.maintainJointMemberQualifications.Name = "maintainJointMemberQualifications";
            this.maintainJointMemberQualifications.Size = new System.Drawing.Size(783, 42);
            this.maintainJointMemberQualifications.TabIndex = 44;
            // 
            // maintainQualificationAllocation
            // 
            this.maintainQualificationAllocation.Location = new System.Drawing.Point(36, 461);
            this.maintainQualificationAllocation.Name = "maintainQualificationAllocation";
            this.maintainQualificationAllocation.Size = new System.Drawing.Size(783, 42);
            this.maintainQualificationAllocation.TabIndex = 13;
            // 
            // maintainQualificationRefreshedBy
            // 
            this.maintainQualificationRefreshedBy.Location = new System.Drawing.Point(36, 461);
            this.maintainQualificationRefreshedBy.Name = "maintainQualificationRefreshedBy";
            this.maintainQualificationRefreshedBy.Size = new System.Drawing.Size(783, 42);
            this.maintainQualificationRefreshedBy.TabIndex = 42;
            // 
            // maintainQualificationRefreshes
            // 
            this.maintainQualificationRefreshes.Location = new System.Drawing.Point(36, 461);
            this.maintainQualificationRefreshes.Name = "maintainQualificationRefreshes";
            this.maintainQualificationRefreshes.Size = new System.Drawing.Size(783, 42);
            this.maintainQualificationRefreshes.TabIndex = 41;
            // 
            // maintainQualificationAwardingBody
            // 
            this.maintainQualificationAwardingBody.Location = new System.Drawing.Point(36, 461);
            this.maintainQualificationAwardingBody.Name = "maintainQualificationAwardingBody";
            this.maintainQualificationAwardingBody.Size = new System.Drawing.Size(783, 42);
            this.maintainQualificationAwardingBody.TabIndex = 40;
            // 
            // OrderIDSpinEdit
            // 
            this.OrderIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "OrderID", true));
            this.OrderIDSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.OrderIDSpinEdit.Location = new System.Drawing.Point(127, 84);
            this.OrderIDSpinEdit.MenuManager = this.barManager1;
            this.OrderIDSpinEdit.Name = "OrderIDSpinEdit";
            this.OrderIDSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.OrderIDSpinEdit.Properties.IsFloatValue = false;
            this.OrderIDSpinEdit.Properties.Mask.EditMask = "N00";
            this.OrderIDSpinEdit.Size = new System.Drawing.Size(299, 20);
            this.OrderIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.OrderIDSpinEdit.TabIndex = 39;
            // 
            // spHR00234MasterQualificationSubTypeEditBindingSource
            // 
            this.spHR00234MasterQualificationSubTypeEditBindingSource.DataMember = "sp_HR_00234_Master_Qualification_SubType_Edit";
            this.spHR00234MasterQualificationSubTypeEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 461);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(783, 42);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling1);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 37;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00234MasterQualificationSubTypeEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(132, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // QualificationSubTypeIDTextEdit
            // 
            this.QualificationSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "QualificationSubTypeID", true));
            this.QualificationSubTypeIDTextEdit.Location = new System.Drawing.Point(141, 107);
            this.QualificationSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.QualificationSubTypeIDTextEdit.Name = "QualificationSubTypeIDTextEdit";
            this.QualificationSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QualificationSubTypeIDTextEdit, true);
            this.QualificationSubTypeIDTextEdit.Size = new System.Drawing.Size(475, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QualificationSubTypeIDTextEdit, optionsSpelling2);
            this.QualificationSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.QualificationSubTypeIDTextEdit.TabIndex = 4;
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "Description", true));
            this.DescriptionTextEdit.Location = new System.Drawing.Point(127, 36);
            this.DescriptionTextEdit.MenuManager = this.barManager1;
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.MaxLength = 100;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DescriptionTextEdit, true);
            this.DescriptionTextEdit.Size = new System.Drawing.Size(716, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DescriptionTextEdit, optionsSpelling3);
            this.DescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.DescriptionTextEdit.TabIndex = 35;
            this.DescriptionTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DescriptionTextEdit_Validating);
            // 
            // QualificationTypeIDGridLookUpEdit
            // 
            this.QualificationTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "QualificationTypeID", true));
            this.QualificationTypeIDGridLookUpEdit.Location = new System.Drawing.Point(127, 60);
            this.QualificationTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.QualificationTypeIDGridLookUpEdit.Name = "QualificationTypeIDGridLookUpEdit";
            this.QualificationTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QualificationTypeIDGridLookUpEdit.Properties.DataSource = this.spHR00236MasterQualificationTypesWithBlankBindingSource;
            this.QualificationTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.QualificationTypeIDGridLookUpEdit.Properties.NullText = "";
            this.QualificationTypeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.QualificationTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.QualificationTypeIDGridLookUpEdit.Size = new System.Drawing.Size(716, 20);
            this.QualificationTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.QualificationTypeIDGridLookUpEdit.TabIndex = 38;
            this.QualificationTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.QualificationTypeIDGridLookUpEdit_Validating);
            // 
            // spHR00236MasterQualificationTypesWithBlankBindingSource
            // 
            this.spHR00236MasterQualificationTypesWithBlankBindingSource.DataMember = "sp_HR_00236_Master_Qualification_Types_With_Blank";
            this.spHR00236MasterQualificationTypesWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Master Qualification Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // startDateDateEdit
            // 
            this.startDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "StartDate", true));
            this.startDateDateEdit.EditValue = null;
            this.startDateDateEdit.Location = new System.Drawing.Point(139, 166);
            this.startDateDateEdit.Name = "startDateDateEdit";
            this.startDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.startDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.startDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.startDateDateEdit.Size = new System.Drawing.Size(235, 20);
            this.startDateDateEdit.StyleController = this.dataLayoutControl1;
            this.startDateDateEdit.TabIndex = 21;
            this.startDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.startDateDateEdit_Validating);
            // 
            // endDateDateEdit
            // 
            this.endDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "EndDate", true));
            this.endDateDateEdit.EditValue = null;
            this.endDateDateEdit.Location = new System.Drawing.Point(493, 166);
            this.endDateDateEdit.Name = "endDateDateEdit";
            this.endDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.endDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.endDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.endDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.endDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.endDateDateEdit.Size = new System.Drawing.Size(234, 20);
            this.endDateDateEdit.StyleController = this.dataLayoutControl1;
            this.endDateDateEdit.TabIndex = 22;
            this.endDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.endDateDateEdit_Validating);
            // 
            // expiryPeriodSpinEdit
            // 
            this.expiryPeriodSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "ExpiryPeriodValue", true));
            this.expiryPeriodSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.expiryPeriodSpinEdit.Location = new System.Drawing.Point(575, 214);
            this.expiryPeriodSpinEdit.Name = "expiryPeriodSpinEdit";
            this.expiryPeriodSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.expiryPeriodSpinEdit.Properties.Mask.EditMask = "N0";
            this.expiryPeriodSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.expiryPeriodSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.expiryPeriodSpinEdit.Size = new System.Drawing.Size(152, 20);
            this.expiryPeriodSpinEdit.StyleController = this.dataLayoutControl1;
            this.expiryPeriodSpinEdit.TabIndex = 20;
            this.expiryPeriodSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.expiryPeriodSpinEdit_Validating);
            // 
            // QualificationSubTypeStatusTypeGridLookUpEdit
            // 
            this.QualificationSubTypeStatusTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "QualificationStatusTypeID", true));
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Location = new System.Drawing.Point(139, 142);
            this.QualificationSubTypeStatusTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Name = "QualificationSubTypeStatusTypeGridLookUpEdit";
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Properties.DataSource = this.spTR00001QualificationSubTypeStatusBindingSource;
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Properties.NullText = "";
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Properties.PopupView = this.gridView1;
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Properties.ValueMember = "StatusTypeID";
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Size = new System.Drawing.Size(588, 20);
            this.QualificationSubTypeStatusTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.QualificationSubTypeStatusTypeGridLookUpEdit.TabIndex = 38;
            this.QualificationSubTypeStatusTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.QualificationSubTypeStatusTypeGridLookUpEdit_Validating);
            // 
            // spTR00001QualificationSubTypeStatusBindingSource
            // 
            this.spTR00001QualificationSubTypeStatusBindingSource.DataMember = "sp_TR_00001_Qualification_SubType_Status";
            this.spTR00001QualificationSubTypeStatusBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn1;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Qualification SubType Status";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // ExpiryPeriodTypeGridLookUpEdit
            // 
            this.ExpiryPeriodTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "ExpiryPeriodTypeID", true));
            this.ExpiryPeriodTypeGridLookUpEdit.Location = new System.Drawing.Point(139, 214);
            this.ExpiryPeriodTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.ExpiryPeriodTypeGridLookUpEdit.Name = "ExpiryPeriodTypeGridLookUpEdit";
            this.ExpiryPeriodTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExpiryPeriodTypeGridLookUpEdit.Properties.DataSource = this.spTR00003QualificationSubTypeExpiryPeriodBindingSource;
            this.ExpiryPeriodTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ExpiryPeriodTypeGridLookUpEdit.Properties.NullText = "";
            this.ExpiryPeriodTypeGridLookUpEdit.Properties.PopupView = this.gridView2;
            this.ExpiryPeriodTypeGridLookUpEdit.Properties.ValueMember = "ExpiryPeriodTypeID";
            this.ExpiryPeriodTypeGridLookUpEdit.Size = new System.Drawing.Size(317, 20);
            this.ExpiryPeriodTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ExpiryPeriodTypeGridLookUpEdit.TabIndex = 38;
            this.ExpiryPeriodTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ExpiryPeriodTypeGridLookUpEdit_Validating);
            // 
            // spTR00003QualificationSubTypeExpiryPeriodBindingSource
            // 
            this.spTR00003QualificationSubTypeExpiryPeriodBindingSource.DataMember = "sp_TR_00003_Qualification_SubType_Expiry_Period";
            this.spTR00003QualificationSubTypeExpiryPeriodBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn4;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // RefreshIntervalTypeGridLookUpEdit
            // 
            this.RefreshIntervalTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "RefreshIntervalTypeID", true));
            this.RefreshIntervalTypeGridLookUpEdit.Location = new System.Drawing.Point(139, 262);
            this.RefreshIntervalTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.RefreshIntervalTypeGridLookUpEdit.Name = "RefreshIntervalTypeGridLookUpEdit";
            this.RefreshIntervalTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RefreshIntervalTypeGridLookUpEdit.Properties.DataSource = this.spTR00005QualificationSubTypeRefreshIntervalBindingSource;
            this.RefreshIntervalTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.RefreshIntervalTypeGridLookUpEdit.Properties.NullText = "";
            this.RefreshIntervalTypeGridLookUpEdit.Properties.PopupView = this.gridView3;
            this.RefreshIntervalTypeGridLookUpEdit.Properties.ValueMember = "RefreshIntervalTypeID";
            this.RefreshIntervalTypeGridLookUpEdit.Size = new System.Drawing.Size(235, 20);
            this.RefreshIntervalTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RefreshIntervalTypeGridLookUpEdit.TabIndex = 38;
            this.RefreshIntervalTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RefreshIntervalTypeGridLookUpEdit_Validating);
            // 
            // spTR00005QualificationSubTypeRefreshIntervalBindingSource
            // 
            this.spTR00005QualificationSubTypeRefreshIntervalBindingSource.DataMember = "sp_TR_00005_Qualification_SubType_Refresh_Interval";
            this.spTR00005QualificationSubTypeRefreshIntervalBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn7;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView3.FormatRules.Add(gridFormatRule3);
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "Order";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // refreshIntervalSpinEdit
            // 
            this.refreshIntervalSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "RefreshIntervalValue", true));
            this.refreshIntervalSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.refreshIntervalSpinEdit.Location = new System.Drawing.Point(493, 262);
            this.refreshIntervalSpinEdit.MenuManager = this.barManager1;
            this.refreshIntervalSpinEdit.Name = "refreshIntervalSpinEdit";
            this.refreshIntervalSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.refreshIntervalSpinEdit.Properties.Mask.EditMask = "N0";
            this.refreshIntervalSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.refreshIntervalSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.refreshIntervalSpinEdit.Size = new System.Drawing.Size(234, 20);
            this.refreshIntervalSpinEdit.StyleController = this.dataLayoutControl1;
            this.refreshIntervalSpinEdit.TabIndex = 20;
            this.refreshIntervalSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.refreshIntervalSpinEdit_Validating);
            // 
            // spinEdit3
            // 
            this.spinEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "CPDHours", true));
            this.spinEdit3.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEdit3.Location = new System.Drawing.Point(139, 286);
            this.spinEdit3.Name = "spinEdit3";
            this.spinEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEdit3.Properties.Mask.EditMask = "N0";
            this.spinEdit3.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.spinEdit3.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.spinEdit3.Size = new System.Drawing.Size(235, 20);
            this.spinEdit3.StyleController = this.dataLayoutControl1;
            this.spinEdit3.TabIndex = 20;
            // 
            // checkEdit3
            // 
            this.checkEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "GCRecognised", true));
            this.checkEdit3.Location = new System.Drawing.Point(139, 310);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "(Tick if Yes)";
            this.checkEdit3.Properties.ValueChecked = 1;
            this.checkEdit3.Properties.ValueUnchecked = 0;
            this.checkEdit3.Size = new System.Drawing.Size(117, 19);
            this.checkEdit3.StyleController = this.dataLayoutControl1;
            this.checkEdit3.TabIndex = 29;
            // 
            // checkEdit4
            // 
            this.checkEdit4.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "InUofGC", true));
            this.checkEdit4.Location = new System.Drawing.Point(375, 310);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "(Tick if Yes)";
            this.checkEdit4.Properties.ValueChecked = 1;
            this.checkEdit4.Properties.ValueUnchecked = 0;
            this.checkEdit4.Size = new System.Drawing.Size(117, 19);
            this.checkEdit4.StyleController = this.dataLayoutControl1;
            this.checkEdit4.TabIndex = 29;
            // 
            // checkEdit5
            // 
            this.checkEdit5.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "CanBeGCDelivered", true));
            this.checkEdit5.Location = new System.Drawing.Point(611, 310);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "(Tick if Yes)";
            this.checkEdit5.Properties.ValueChecked = 1;
            this.checkEdit5.Properties.ValueUnchecked = 0;
            this.checkEdit5.Size = new System.Drawing.Size(116, 19);
            this.checkEdit5.StyleController = this.dataLayoutControl1;
            this.checkEdit5.TabIndex = 29;
            // 
            // ValidityTypeGridLookUpEdit
            // 
            this.ValidityTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "ValidityTypeID", true));
            this.ValidityTypeGridLookUpEdit.Location = new System.Drawing.Point(139, 190);
            this.ValidityTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.ValidityTypeGridLookUpEdit.Name = "ValidityTypeGridLookUpEdit";
            this.ValidityTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ValidityTypeGridLookUpEdit.Properties.DataSource = this.spTR00002QualificationSubTypeValidityBindingSource;
            this.ValidityTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ValidityTypeGridLookUpEdit.Properties.NullText = "";
            this.ValidityTypeGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.ValidityTypeGridLookUpEdit.Properties.ValueMember = "ValidityTypeID";
            this.ValidityTypeGridLookUpEdit.Size = new System.Drawing.Size(235, 20);
            this.ValidityTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ValidityTypeGridLookUpEdit.TabIndex = 38;
            this.ValidityTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ValidityTypeGridLookUpEdit_Validating);
            // 
            // spTR00002QualificationSubTypeValidityBindingSource
            // 
            this.spTR00002QualificationSubTypeValidityBindingSource.DataMember = "sp_TR_00002_Qualification_SubType_Validity";
            this.spTR00002QualificationSubTypeValidityBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.gridColumn10;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = 0;
            formatConditionRuleValue4.Value2 = ((short)(0));
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridView4.FormatRules.Add(gridFormatRule4);
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "Order";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // refreshTypeGridLookUpEdit
            // 
            this.refreshTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "RefreshTypeID", true));
            this.refreshTypeGridLookUpEdit.Location = new System.Drawing.Point(139, 238);
            this.refreshTypeGridLookUpEdit.Name = "refreshTypeGridLookUpEdit";
            this.refreshTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.refreshTypeGridLookUpEdit.Properties.DataSource = this.spTR00004QualificationSubTypeRefreshBindingSource;
            this.refreshTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.refreshTypeGridLookUpEdit.Properties.NullText = "";
            this.refreshTypeGridLookUpEdit.Properties.PopupView = this.gridView5;
            this.refreshTypeGridLookUpEdit.Properties.ValueMember = "RefreshTypeID";
            this.refreshTypeGridLookUpEdit.Size = new System.Drawing.Size(235, 20);
            this.refreshTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.refreshTypeGridLookUpEdit.TabIndex = 38;
            this.refreshTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.refreshTypeGridLookUpEdit_Validating);
            // 
            // spTR00004QualificationSubTypeRefreshBindingSource
            // 
            this.spTR00004QualificationSubTypeRefreshBindingSource.DataMember = "sp_TR_00004_Qualification_SubType_Refresh";
            this.spTR00004QualificationSubTypeRefreshBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15});
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule5.ApplyToRow = true;
            gridFormatRule5.Column = this.gridColumn13;
            gridFormatRule5.Name = "Format0";
            formatConditionRuleValue5.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue5.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue5.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue5.Value1 = 0;
            formatConditionRuleValue5.Value2 = ((short)(0));
            gridFormatRule5.Rule = formatConditionRuleValue5;
            this.gridView5.FormatRules.Add(gridFormatRule5);
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView5.OptionsView.ShowIndicator = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn15, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn14, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Validity Type";
            this.gridColumn14.FieldName = "Description";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            this.gridColumn14.Visible = true;
            this.gridColumn14.VisibleIndex = 0;
            this.gridColumn14.Width = 220;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Order";
            this.gridColumn15.FieldName = "Order";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            // 
            // maintainJointOwnerQualifications
            // 
            this.maintainJointOwnerQualifications.Location = new System.Drawing.Point(36, 461);
            this.maintainJointOwnerQualifications.Name = "maintainJointOwnerQualifications";
            this.maintainJointOwnerQualifications.Size = new System.Drawing.Size(783, 42);
            this.maintainJointOwnerQualifications.TabIndex = 44;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "CertificateRequired", true));
            this.checkEdit1.Location = new System.Drawing.Point(139, 333);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "(Tick if Yes)";
            this.checkEdit1.Properties.ValueChecked = 1;
            this.checkEdit1.Properties.ValueUnchecked = 0;
            this.checkEdit1.Size = new System.Drawing.Size(588, 19);
            this.checkEdit1.StyleController = this.dataLayoutControl1;
            this.checkEdit1.TabIndex = 29;
            // 
            // checkEdit2
            // 
            this.checkEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00234MasterQualificationSubTypeEditBindingSource, "HideFromTotalview", true));
            this.checkEdit2.Location = new System.Drawing.Point(139, 356);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "(Tick if Yes)";
            this.checkEdit2.Properties.ValueChecked = 1;
            this.checkEdit2.Properties.ValueUnchecked = 0;
            this.checkEdit2.Size = new System.Drawing.Size(588, 19);
            this.checkEdit2.StyleController = this.dataLayoutControl1;
            this.checkEdit2.TabIndex = 29;
            // 
            // ItemForQualificationSubTypeID
            // 
            this.ItemForQualificationSubTypeID.Control = this.QualificationSubTypeIDTextEdit;
            this.ItemForQualificationSubTypeID.CustomizationFormText = "Qualification Sub-Type ID:";
            this.ItemForQualificationSubTypeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForQualificationSubTypeID.Name = "ItemForQualificationSubTypeID";
            this.ItemForQualificationSubTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForQualificationSubTypeID.Text = "Qualification Sub-Type ID:";
            this.ItemForQualificationSubTypeID.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3,
            this.layoutControlItem9});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(855, 544);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(835, 24);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(320, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(515, 24);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(120, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 24);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(120, 24);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(120, 24);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(120, 24);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDescription,
            this.ItemForQualificationTypeID,
            this.ItemForOrderID,
            this.layoutControlGroup5,
            this.emptySpaceItem5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 24);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(835, 495);
            // 
            // ItemForDescription
            // 
            this.ItemForDescription.AllowHide = false;
            this.ItemForDescription.Control = this.DescriptionTextEdit;
            this.ItemForDescription.CustomizationFormText = "Department Name:";
            this.ItemForDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForDescription.Name = "ItemForDescription";
            this.ItemForDescription.Size = new System.Drawing.Size(835, 24);
            this.ItemForDescription.Text = "Sub-Type Description:";
            this.ItemForDescription.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForQualificationTypeID
            // 
            this.ItemForQualificationTypeID.Control = this.QualificationTypeIDGridLookUpEdit;
            this.ItemForQualificationTypeID.CustomizationFormText = "Qualification Type:";
            this.ItemForQualificationTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForQualificationTypeID.Name = "ItemForQualificationTypeID";
            this.ItemForQualificationTypeID.Size = new System.Drawing.Size(835, 24);
            this.ItemForQualificationTypeID.Text = "Qualification Type:";
            this.ItemForQualificationTypeID.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForOrderID
            // 
            this.ItemForOrderID.Control = this.OrderIDSpinEdit;
            this.ItemForOrderID.CustomizationFormText = "Record Order:";
            this.ItemForOrderID.Location = new System.Drawing.Point(0, 48);
            this.ItemForOrderID.MinSize = new System.Drawing.Size(169, 24);
            this.ItemForOrderID.Name = "ItemForOrderID";
            this.ItemForOrderID.Size = new System.Drawing.Size(418, 24);
            this.ItemForOrderID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForOrderID.Text = "Record Order:";
            this.ItemForOrderID.TextSize = new System.Drawing.Size(112, 13);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 72);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(835, 423);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForStartDate,
            this.ItemForEndDate,
            this.ItemForInUniversityOfGC,
            this.ItemForQualificationSubTypeStatus,
            this.ItemForValidityTypeID,
            this.ItemForExpiryPeriodType,
            this.ItemForExpiryPeriodValue,
            this.ItemForRefreshTypeID,
            this.ItemForRefreshIntervalType,
            this.ItemForGCRecognised,
            this.emptySpaceItem3,
            this.ItemForCPDHours,
            this.emptySpaceItem7,
            this.emptySpaceItem4,
            this.ItemForRefreshIntervalValue,
            this.emptySpaceItem6,
            this.ItemForGCDeliverable,
            this.ItemForHideFromTotalview,
            this.ItemForCertificateRequired});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(835, 283);
            this.layoutControlGroup7.Text = "Qualification Sub-Type Details";
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.startDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Qualification Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 24);
            this.ItemForStartDate.MinSize = new System.Drawing.Size(169, 24);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(354, 24);
            this.ItemForStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.endDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "Qualification End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(354, 24);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(353, 24);
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForInUniversityOfGC
            // 
            this.ItemForInUniversityOfGC.Control = this.checkEdit4;
            this.ItemForInUniversityOfGC.CustomizationFormText = "In University of GC:";
            this.ItemForInUniversityOfGC.Location = new System.Drawing.Point(236, 168);
            this.ItemForInUniversityOfGC.Name = "ItemForInUniversityOfGC";
            this.ItemForInUniversityOfGC.Size = new System.Drawing.Size(236, 23);
            this.ItemForInUniversityOfGC.Text = "In University of GC:";
            this.ItemForInUniversityOfGC.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForQualificationSubTypeStatus
            // 
            this.ItemForQualificationSubTypeStatus.Control = this.QualificationSubTypeStatusTypeGridLookUpEdit;
            this.ItemForQualificationSubTypeStatus.CustomizationFormText = "Qualification SubType Status:";
            this.ItemForQualificationSubTypeStatus.Location = new System.Drawing.Point(0, 0);
            this.ItemForQualificationSubTypeStatus.Name = "ItemForQualificationSubTypeStatus";
            this.ItemForQualificationSubTypeStatus.Size = new System.Drawing.Size(707, 24);
            this.ItemForQualificationSubTypeStatus.Text = "Status:";
            this.ItemForQualificationSubTypeStatus.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForValidityTypeID
            // 
            this.ItemForValidityTypeID.Control = this.ValidityTypeGridLookUpEdit;
            this.ItemForValidityTypeID.CustomizationFormText = "Validity Type:";
            this.ItemForValidityTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForValidityTypeID.MinSize = new System.Drawing.Size(169, 24);
            this.ItemForValidityTypeID.Name = "ItemForValidityTypeID";
            this.ItemForValidityTypeID.Size = new System.Drawing.Size(354, 24);
            this.ItemForValidityTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForValidityTypeID.Text = "Validity Type:";
            this.ItemForValidityTypeID.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForExpiryPeriodType
            // 
            this.ItemForExpiryPeriodType.Control = this.ExpiryPeriodTypeGridLookUpEdit;
            this.ItemForExpiryPeriodType.CustomizationFormText = "Expiry Period Type:";
            this.ItemForExpiryPeriodType.Location = new System.Drawing.Point(0, 72);
            this.ItemForExpiryPeriodType.MinSize = new System.Drawing.Size(169, 24);
            this.ItemForExpiryPeriodType.Name = "ItemForExpiryPeriodType";
            this.ItemForExpiryPeriodType.Size = new System.Drawing.Size(436, 24);
            this.ItemForExpiryPeriodType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForExpiryPeriodType.Text = "Expiry Period Type:";
            this.ItemForExpiryPeriodType.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForExpiryPeriodValue
            // 
            this.ItemForExpiryPeriodValue.Control = this.expiryPeriodSpinEdit;
            this.ItemForExpiryPeriodValue.CustomizationFormText = "Expiry Period Value:";
            this.ItemForExpiryPeriodValue.Location = new System.Drawing.Point(436, 72);
            this.ItemForExpiryPeriodValue.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForExpiryPeriodValue.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForExpiryPeriodValue.Name = "ItemForExpiryPeriodValue";
            this.ItemForExpiryPeriodValue.Size = new System.Drawing.Size(271, 24);
            this.ItemForExpiryPeriodValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForExpiryPeriodValue.Text = "Expiry Period Value:";
            this.ItemForExpiryPeriodValue.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForRefreshTypeID
            // 
            this.ItemForRefreshTypeID.Control = this.refreshTypeGridLookUpEdit;
            this.ItemForRefreshTypeID.CustomizationFormText = "Refresh Type:";
            this.ItemForRefreshTypeID.Location = new System.Drawing.Point(0, 96);
            this.ItemForRefreshTypeID.MinSize = new System.Drawing.Size(169, 24);
            this.ItemForRefreshTypeID.Name = "ItemForRefreshTypeID";
            this.ItemForRefreshTypeID.Size = new System.Drawing.Size(354, 24);
            this.ItemForRefreshTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRefreshTypeID.Text = "Refresh Type:";
            this.ItemForRefreshTypeID.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForRefreshIntervalType
            // 
            this.ItemForRefreshIntervalType.Control = this.RefreshIntervalTypeGridLookUpEdit;
            this.ItemForRefreshIntervalType.CustomizationFormText = "Refresh Interval Type:";
            this.ItemForRefreshIntervalType.Location = new System.Drawing.Point(0, 120);
            this.ItemForRefreshIntervalType.MinSize = new System.Drawing.Size(169, 24);
            this.ItemForRefreshIntervalType.Name = "ItemForRefreshIntervalType";
            this.ItemForRefreshIntervalType.Size = new System.Drawing.Size(354, 24);
            this.ItemForRefreshIntervalType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRefreshIntervalType.Text = "Refresh Interval Type:";
            this.ItemForRefreshIntervalType.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForGCRecognised
            // 
            this.ItemForGCRecognised.Control = this.checkEdit3;
            this.ItemForGCRecognised.CustomizationFormText = "GC Recognised:";
            this.ItemForGCRecognised.Location = new System.Drawing.Point(0, 168);
            this.ItemForGCRecognised.Name = "ItemForGCRecognised";
            this.ItemForGCRecognised.Size = new System.Drawing.Size(236, 23);
            this.ItemForGCRecognised.Text = "GC Recognised:";
            this.ItemForGCRecognised.TextSize = new System.Drawing.Size(112, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(707, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(104, 191);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(104, 191);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(104, 237);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForCPDHours
            // 
            this.ItemForCPDHours.Control = this.spinEdit3;
            this.ItemForCPDHours.CustomizationFormText = "CPD Hours:";
            this.ItemForCPDHours.Location = new System.Drawing.Point(0, 144);
            this.ItemForCPDHours.MinSize = new System.Drawing.Size(169, 24);
            this.ItemForCPDHours.Name = "ItemForCPDHours";
            this.ItemForCPDHours.Size = new System.Drawing.Size(354, 24);
            this.ItemForCPDHours.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCPDHours.Text = "CPD Hours:";
            this.ItemForCPDHours.TextSize = new System.Drawing.Size(112, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem7.Location = new System.Drawing.Point(354, 48);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(353, 24);
            this.emptySpaceItem7.Text = "emptySpaceItem3";
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(354, 96);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(353, 24);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRefreshIntervalValue
            // 
            this.ItemForRefreshIntervalValue.Control = this.refreshIntervalSpinEdit;
            this.ItemForRefreshIntervalValue.CustomizationFormText = "Refresh Interval Value:";
            this.ItemForRefreshIntervalValue.Location = new System.Drawing.Point(354, 120);
            this.ItemForRefreshIntervalValue.MinSize = new System.Drawing.Size(169, 24);
            this.ItemForRefreshIntervalValue.Name = "ItemForRefreshIntervalValue";
            this.ItemForRefreshIntervalValue.Size = new System.Drawing.Size(353, 24);
            this.ItemForRefreshIntervalValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRefreshIntervalValue.Text = "Refresh Interval Value:";
            this.ItemForRefreshIntervalValue.TextSize = new System.Drawing.Size(112, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(354, 144);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(353, 24);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForGCDeliverable
            // 
            this.ItemForGCDeliverable.Control = this.checkEdit5;
            this.ItemForGCDeliverable.CustomizationFormText = "GC Deliverable:";
            this.ItemForGCDeliverable.Location = new System.Drawing.Point(472, 168);
            this.ItemForGCDeliverable.Name = "ItemForGCDeliverable";
            this.ItemForGCDeliverable.Size = new System.Drawing.Size(235, 23);
            this.ItemForGCDeliverable.Text = "GC Deliverable:";
            this.ItemForGCDeliverable.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForHideFromTotalview
            // 
            this.ItemForHideFromTotalview.Control = this.checkEdit2;
            this.ItemForHideFromTotalview.CustomizationFormText = "Hide from Totalview";
            this.ItemForHideFromTotalview.Location = new System.Drawing.Point(0, 214);
            this.ItemForHideFromTotalview.MinSize = new System.Drawing.Size(195, 23);
            this.ItemForHideFromTotalview.Name = "ItemForHideFromTotalview";
            this.ItemForHideFromTotalview.Size = new System.Drawing.Size(707, 23);
            this.ItemForHideFromTotalview.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForHideFromTotalview.Text = "Hide from Totalview :";
            this.ItemForHideFromTotalview.TextSize = new System.Drawing.Size(112, 13);
            // 
            // ItemForCertificateRequired
            // 
            this.ItemForCertificateRequired.Control = this.checkEdit1;
            this.ItemForCertificateRequired.CustomizationFormText = "Certificate Required";
            this.ItemForCertificateRequired.Location = new System.Drawing.Point(0, 191);
            this.ItemForCertificateRequired.MinSize = new System.Drawing.Size(195, 23);
            this.ItemForCertificateRequired.Name = "ItemForCertificateRequired";
            this.ItemForCertificateRequired.Size = new System.Drawing.Size(707, 23);
            this.ItemForCertificateRequired.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCertificateRequired.Text = "Certificate required :";
            this.ItemForCertificateRequired.TextSize = new System.Drawing.Size(112, 13);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 283);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(835, 140);
            this.layoutControlGroup8.Text = "Remarks and Associated Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.awardingBodyLayoutControlGroup;
            this.tabbedControlGroup1.SelectedTabPageIndex = 1;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(811, 94);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.awardingBodyLayoutControlGroup,
            this.refreshesLayoutControlGroup,
            this.refreshedByLayoutControlGroup,
            this.allocationLayoutControlGroup,
            this.layoutControlGroup9,
            this.layoutControlGroup11,
            this.replacesLayoutControlGroup,
            this.layoutControlGroup12});
            // 
            // awardingBodyLayoutControlGroup
            // 
            this.awardingBodyLayoutControlGroup.CustomizationFormText = "Awarding Body";
            this.awardingBodyLayoutControlGroup.ExpandButtonVisible = true;
            this.awardingBodyLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.awardingBodyLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.awardingBodyLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.awardingBodyLayoutControlGroup.Name = "awardingBodyLayoutControlGroup";
            this.awardingBodyLayoutControlGroup.Size = new System.Drawing.Size(787, 46);
            this.awardingBodyLayoutControlGroup.Text = "Awarding Body";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.maintainQualificationAwardingBody;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(787, 46);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(787, 46);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(787, 46);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // refreshesLayoutControlGroup
            // 
            this.refreshesLayoutControlGroup.ExpandButtonVisible = true;
            this.refreshesLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.refreshesLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.refreshesLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.refreshesLayoutControlGroup.Name = "refreshesLayoutControlGroup";
            this.refreshesLayoutControlGroup.Size = new System.Drawing.Size(787, 46);
            this.refreshesLayoutControlGroup.Text = "Refreshes";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.maintainQualificationRefreshes;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(787, 46);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // refreshedByLayoutControlGroup
            // 
            this.refreshedByLayoutControlGroup.ExpandButtonVisible = true;
            this.refreshedByLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.refreshedByLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.refreshedByLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.refreshedByLayoutControlGroup.Name = "refreshedByLayoutControlGroup";
            this.refreshedByLayoutControlGroup.Size = new System.Drawing.Size(787, 46);
            this.refreshedByLayoutControlGroup.Text = "Refreshed By";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.maintainQualificationRefreshedBy;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(787, 46);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // allocationLayoutControlGroup
            // 
            this.allocationLayoutControlGroup.ExpandButtonVisible = true;
            this.allocationLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.allocationLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.allocationLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.allocationLayoutControlGroup.Name = "allocationLayoutControlGroup";
            this.allocationLayoutControlGroup.Size = new System.Drawing.Size(787, 46);
            this.allocationLayoutControlGroup.Text = "Training Allocation";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.maintainQualificationAllocation;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(787, 46);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.ExpandButtonVisible = true;
            this.layoutControlGroup9.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(787, 46);
            this.layoutControlGroup9.Text = "Equivalent Members";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.maintainJointMemberQualifications;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(787, 46);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.CustomizationFormText = "Equivalent Members";
            this.layoutControlGroup11.ExpandButtonVisible = true;
            this.layoutControlGroup11.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(787, 46);
            this.layoutControlGroup11.Text = "Equivalent Owners";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.maintainJointOwnerQualifications;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(787, 46);
            this.layoutControlItem6.Text = "layoutControlItem7";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // replacesLayoutControlGroup
            // 
            this.replacesLayoutControlGroup.ExpandButtonVisible = true;
            this.replacesLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.replacesLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8});
            this.replacesLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.replacesLayoutControlGroup.Name = "replacesLayoutControlGroup";
            this.replacesLayoutControlGroup.Size = new System.Drawing.Size(787, 46);
            this.replacesLayoutControlGroup.Text = "Replaces";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.maintainQualificationReplaces;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(787, 46);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.ExpandButtonVisible = true;
            this.layoutControlGroup12.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup12.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(787, 46);
            this.layoutControlGroup12.Text = "Prerequisite";
            this.layoutControlGroup12.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(418, 48);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(417, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.alignmentControl1;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 519);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(835, 5);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00234_Master_Qualification_SubType_EditTableAdapter
            // 
            this.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter
            // 
            this.sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00003_Qualification_SubType_Expiry_PeriodTableAdapter
            // 
            this.sp_TR_00003_Qualification_SubType_Expiry_PeriodTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00005_Qualification_SubType_Refresh_IntervalTableAdapter
            // 
            this.sp_TR_00005_Qualification_SubType_Refresh_IntervalTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00001_Qualification_SubType_StatusTableAdapter
            // 
            this.sp_TR_00001_Qualification_SubType_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_TR_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_TR_00002_Qualification_SubType_ValidityTableAdapter
            // 
            this.sp_TR_00002_Qualification_SubType_ValidityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00004_Qualification_SubType_RefreshTableAdapter
            // 
            this.sp_TR_00004_Qualification_SubType_RefreshTableAdapter.ClearBeforeFill = true;
            // 
            // sp00312_Accident_Body_Areas_With_BlankTableAdapter1
            // 
            this.sp00312_Accident_Body_Areas_With_BlankTableAdapter1.ClearBeforeFill = true;
            // 
            // frm_HR_Master_Qualification_SubType_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(872, 585);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Master_Qualification_SubType_Edit";
            this.Text = "Edit Master Qualification Sub-Type";
            this.Activated += new System.EventHandler(this.frm_HR_Master_Qualification_SubType_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Master_Qualification_SubType_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Master_Qualification_SubType_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrderIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00234MasterQualificationSubTypeEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00236MasterQualificationTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.expiryPeriodSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeStatusTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00001QualificationSubTypeStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExpiryPeriodTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00003QualificationSubTypeExpiryPeriodBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RefreshIntervalTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00005QualificationSubTypeRefreshIntervalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshIntervalSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValidityTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00002QualificationSubTypeValidityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00004QualificationSubTypeRefreshBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInUniversityOfGC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValidityTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryPeriodType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryPeriodValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshIntervalType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCRecognised)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCPDHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshIntervalValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForGCDeliverable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHideFromTotalview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCertificateRequired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.awardingBodyLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshesLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshedByLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allocationLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.replacesLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit QualificationSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationSubTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescription;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit DescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationTypeID;
        private DevExpress.XtraEditors.GridLookUpEdit QualificationTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.SpinEdit OrderIDSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOrderID;
        private System.Windows.Forms.BindingSource spHR00234MasterQualificationSubTypeEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter sp_HR_00234_Master_Qualification_SubType_EditTableAdapter;
        private System.Windows.Forms.BindingSource spHR00236MasterQualificationTypesWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.DateEdit startDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraEditors.DateEdit endDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraEditors.SpinEdit expiryPeriodSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpiryPeriodValue;
        private DevExpress.XtraEditors.GridLookUpEdit QualificationSubTypeStatusTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationSubTypeStatus;
        private DevExpress.XtraEditors.GridLookUpEdit ExpiryPeriodTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpiryPeriodType;
        private DevExpress.XtraEditors.GridLookUpEdit RefreshIntervalTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.SpinEdit refreshIntervalSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRefreshIntervalType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRefreshIntervalValue;
        private DevExpress.XtraEditors.SpinEdit spinEdit3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCPDHours;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCRecognised;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInUniversityOfGC;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForGCDeliverable;
        private DataSet_TR_Core dataSet_TR_Core;
        private System.Windows.Forms.BindingSource spTR00003QualificationSubTypeExpiryPeriodBindingSource;
        private System.Windows.Forms.BindingSource spTR00005QualificationSubTypeRefreshIntervalBindingSource;
        private System.Windows.Forms.BindingSource spTR00001QualificationSubTypeStatusBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00003_Qualification_SubType_Expiry_PeriodTableAdapter sp_TR_00003_Qualification_SubType_Expiry_PeriodTableAdapter;
        private DataSet_TR_CoreTableAdapters.sp_TR_00005_Qualification_SubType_Refresh_IntervalTableAdapter sp_TR_00005_Qualification_SubType_Refresh_IntervalTableAdapter;
        private DataSet_TR_CoreTableAdapters.sp_TR_00001_Qualification_SubType_StatusTableAdapter sp_TR_00001_Qualification_SubType_StatusTableAdapter;
        private DataSet_TR_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraLayout.LayoutControlGroup awardingBodyLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlGroup replacesLayoutControlGroup;
        private DevExpress.XtraEditors.GridLookUpEdit ValidityTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValidityTypeID;
        private System.Windows.Forms.BindingSource spTR00002QualificationSubTypeValidityBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00002_Qualification_SubType_ValidityTableAdapter sp_TR_00002_Qualification_SubType_ValidityTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit refreshTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRefreshTypeID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private System.Windows.Forms.BindingSource spTR00004QualificationSubTypeRefreshBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00004_Qualification_SubType_RefreshTableAdapter sp_TR_00004_Qualification_SubType_RefreshTableAdapter;
        private MaintainQualificationAwardingBody maintainQualificationAwardingBody;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private MaintainQualificationRefreshes maintainQualificationRefreshes;
        private MaintainQualificationRefreshedBy maintainQualificationRefreshedBy;
        private DevExpress.XtraLayout.LayoutControlGroup refreshesLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup refreshedByLayoutControlGroup;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlGroup allocationLayoutControlGroup;
        private MaintainQualificationAllocation maintainQualificationAllocation;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DataSet_AccidentTableAdapters.sp00312_Accident_Body_Areas_With_BlankTableAdapter sp00312_Accident_Body_Areas_With_BlankTableAdapter1;
        private MaintainJointMemberQualifications maintainJointMemberQualifications;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private MaintainQualificationReplaces maintainQualificationReplaces;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private MaintainJointOwnerQualifications maintainJointOwnerQualifications;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCertificateRequired;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHideFromTotalview;
        private BaseObjects.AlignmentControl alignmentControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}

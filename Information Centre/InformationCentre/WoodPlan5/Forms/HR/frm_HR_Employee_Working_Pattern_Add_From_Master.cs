using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Working_Pattern_Add_From_Master : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string _PassedInEmployeeIDs = "";
        public string _PassedInEmployeeNames = "";
        public string _NewWorkingPatternHeaderIDs = "";
        GridHitInfo downHitInfo = null;

        #endregion

        public frm_HR_Employee_Working_Pattern_Add_From_Master()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Working_Pattern_Add_From_Master_Load(object sender, EventArgs e)
        {
            this.FormID = 500097;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp_HR_00168_Master_Working_Pattern_Headers_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00163_Master_Working_PatternsTableAdapter.Connection.ConnectionString = strConnectionString;

            EmployeeNameTextEdit.EditValue = _PassedInEmployeeNames;
            dateEditStart.DateTime = DateTime.Today;
            dateEditEnd.DateTime = DateTime.Today.AddYears(5);
            try
            {
                sp_HR_00168_Master_Working_Pattern_Headers_SelectTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00168_Master_Working_Pattern_Headers_Select, 1);
            }
            catch (Exception)
            {
                XtraMessageBox.Show("An error occurred while loading the Master Working Pattern Headers list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Close();
            }

            gridControl1.ForceInitialize();
            gridControl2.ForceInitialize();
        }

        bool internalRowFocusing;


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Master Working Pattern Headers Available";
                    break;
                case "gridView2":
                    message = "No Linked Master Working Patterns Available - Select a different Master Pattern Header to proceed";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            var strSelectedID = "";

            if (intCount == 1)
            {
                //foreach (int intRowHandle in intRowHandles)
                //{
                strSelectedID = view.GetRowCellValue(intRowHandles[0], view.Columns["HeaderID"]).ToString() + ",";
                //}
            }

            //Populate Linked sub types //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00036_Get_Absence_Sub_Types_List_With_Blank.Clear();
            }
            else
            {
                try
                {
                    sp_HR_00163_Master_Working_PatternsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00163_Master_Working_Patterns, strSelectedID);
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related working patterns.\n\nTry selecting an work pattern header again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            int intHeaderID = 0;
            int intPatternCount = 0;
            string strHeaderDescription = "";

            var view1 = (GridView)gridControl1.MainView;
            if (view1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intHeaderID = Convert.ToInt32(view1.GetRowCellValue(view1.FocusedRowHandle, "HeaderID"));
                strHeaderDescription = view1.GetRowCellValue(view1.FocusedRowHandle, "Description").ToString();
                if (string.IsNullOrWhiteSpace(strHeaderDescription)) strHeaderDescription = "Not Specified";
            }
            if (intHeaderID <= 0)
            {
                XtraMessageBox.Show("Select a Work Pattern Header before proceeding.", "Create Work Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var view2 = (GridView)gridControl2.MainView;
            if (view2.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intPatternCount = view2.DataRowCount;
            }
            if (intPatternCount <= 0)
            {
                XtraMessageBox.Show("Select a Work Pattern Header which has an attached shift pattern before proceeding.", "Create Work Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (string.IsNullOrWhiteSpace(_PassedInEmployeeIDs))
            {
                XtraMessageBox.Show("No Employee Selected.\n\nClose the screen and select one or more employees to create the Work Pattern for before trying again.", "Create Work Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            DateTime dtStartDate = dateEditStart.DateTime;
            DateTime dtEndDate = dateEditEnd.DateTime;
            if (dtStartDate == DateTime.MinValue)
            {
                XtraMessageBox.Show("Enter the Start Date for the generated working pattern before proceeding.", "Create Work Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dtEndDate == DateTime.MinValue)
            {
                XtraMessageBox.Show("Enter the End Date for the generated working pattern before proceeding.", "Create Work Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dtEndDate <= dtStartDate)
            {
                XtraMessageBox.Show("The End Date entered must be greated than the Start Date entered. Correct before proceeding.", "Create Work Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Checks passed - Create Working Pattern Header and Working Pattern for Employee(s) (Table Triggers will generate days worked) //
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Adding Work Pattern(s)...");

            _NewWorkingPatternHeaderIDs = "";
            int intCount = 0;
            int intEmployeeID = 0;
            char[] delimiters = new char[] { ',' };
            using (var CreatePattern = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
            {
                CreatePattern.ChangeConnectionString(strConnectionString);
                try
                {
                    string[] strArray = _PassedInEmployeeIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string strElement in strArray)
                    {
                        intCount++;
                        intEmployeeID = Convert.ToInt32(strElement);
                        _NewWorkingPatternHeaderIDs += CreatePattern.sp_HR_00169_Create_Employee_Working_Pattern_From_Master(intHeaderID, strHeaderDescription, dtStartDate, dtEndDate, null, intEmployeeID).ToString() + ";";
                    }
                }
                catch (Exception ex) 
                {
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    Application.DoEvents();   // Allow Form time to repaint itself //
                    XtraMessageBox.Show("An error occurred while generating the Employee Working Pattern.\n\nEmployee ID: " + intEmployeeID.ToString() + "\n\nError: " + ex.Message + "\n\nYou will need to double check which employees had work patterns created and which did not and if neccessary, change the selected employees then repeat the process.", "Create Work Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    this.Close();
                }
            }
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            Application.DoEvents();   // Allow Form time to repaint itself //
            XtraMessageBox.Show(intCount.ToString() + " Employee Working Pattern(s) Created Successfully.", "Create Work Pattern from Master Pattern", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void dateEditStart_EditValueChanged(object sender, EventArgs e)
        {
            // Set end date to 5 years after start date //
            DateEdit dtStart = (DateEdit)sender;
            DateTime dtEnd = dtStart.DateTime.AddYears(5);
            dateEditEnd.DateTime = dtEnd;
        }

 


    }
}


namespace WoodPlan5
{
    partial class frm_HR_Employee_Working_Pattern_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Working_Pattern_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.spHR00047GetEmployeeWorkingPatternItemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.EndTimeTimeEdit = new DevExpress.XtraEditors.TimeSpanEdit();
            this.StartTimeTimeEdit = new DevExpress.XtraEditors.TimeSpanEdit();
            this.InformationLabel = new DevExpress.XtraEditors.LabelControl();
            this.CreateDummyShiftButton = new DevExpress.XtraEditors.SimpleButton();
            this.EmployeeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HeaderEndDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.HeaderStartDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RegionNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LocationNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.BusinessAreaNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeDepartmentWorkedIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DepartmentNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.PaidForBreaksCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DayOfWeekGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00049WorkPatternDaysOfWeekWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.WorkingPatternIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.WorkingPatternHeaderIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.CalculatedHoursTimeEdit = new DevExpress.XtraEditors.TextEdit();
            this.HeaderDescriptionButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.BreakLengthMinsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.WeekNumberSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.ItemForEmployeeDepartmentWorkedID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkingPatternID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWorkingPatternHeaderID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForBreakLength = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCalculatedHours = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWeekNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDayOfWeek = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPaidForBreaks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForDepartmentName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForBusinessAreaName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLocationName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRegionName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForStartTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForHeaderDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHeaderStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForHeaderEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCreateDummyShiftButton = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForInformationLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.TableAdapterManager();
            this.sp_HR_00049_Work_Pattern_Days_Of_Week_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00049_Work_Pattern_Days_Of_Week_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00047GetEmployeeWorkingPatternItemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EndTimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderEndDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderStartDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeDepartmentWorkedIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidForBreaksCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DayOfWeekGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00049WorkPatternDaysOfWeekWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkingPatternIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkingPatternHeaderIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculatedHoursTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderDescriptionButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BreakLengthMinsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeekNumberSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeDepartmentWorkedID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkingPatternID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkingPatternHeaderID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBreakLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculatedHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeekNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDayOfWeek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidForBreaks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeaderDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeaderStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeaderEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreateDummyShiftButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInformationLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(741, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 554);
            this.barDockControlBottom.Size = new System.Drawing.Size(741, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 528);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(741, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 528);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(741, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 554);
            this.barDockControl2.Size = new System.Drawing.Size(741, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 528);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(741, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 528);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // spHR00047GetEmployeeWorkingPatternItemsBindingSource
            // 
            this.spHR00047GetEmployeeWorkingPatternItemsBindingSource.DataMember = "sp_HR_00047_Get_Employee_Working_Pattern_Items";
            this.spHR00047GetEmployeeWorkingPatternItemsBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.EndTimeTimeEdit);
            this.dataLayoutControl1.Controls.Add(this.StartTimeTimeEdit);
            this.dataLayoutControl1.Controls.Add(this.InformationLabel);
            this.dataLayoutControl1.Controls.Add(this.CreateDummyShiftButton);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HeaderEndDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.HeaderStartDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RegionNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LocationNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.BusinessAreaNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeDepartmentWorkedIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DepartmentNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.PaidForBreaksCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.DayOfWeekGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.WorkingPatternIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.WorkingPatternHeaderIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.CalculatedHoursTimeEdit);
            this.dataLayoutControl1.Controls.Add(this.HeaderDescriptionButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.BreakLengthMinsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.WeekNumberSpinEdit);
            this.dataLayoutControl1.DataSource = this.spHR00047GetEmployeeWorkingPatternItemsBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmployeeDepartmentWorkedID,
            this.ItemForWorkingPatternID,
            this.ItemForWorkingPatternHeaderID,
            this.ItemForEmployeeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1390, 184, 460, 571);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(741, 528);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // EndTimeTimeEdit
            // 
            this.EndTimeTimeEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "EndTime", true));
            this.EndTimeTimeEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.EndTimeTimeEdit.Location = new System.Drawing.Point(145, 310);
            this.EndTimeTimeEdit.MenuManager = this.barManager1;
            this.EndTimeTimeEdit.Name = "EndTimeTimeEdit";
            this.EndTimeTimeEdit.Properties.AllowEditDays = false;
            this.EndTimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndTimeTimeEdit.Properties.Mask.EditMask = "HH:mm:ss";
            this.EndTimeTimeEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EndTimeTimeEdit.Size = new System.Drawing.Size(136, 20);
            this.EndTimeTimeEdit.StyleController = this.dataLayoutControl1;
            this.EndTimeTimeEdit.TabIndex = 45;
            this.EndTimeTimeEdit.EditValueChanged += new System.EventHandler(this.EndTimeTimeEdit_EditValueChanged);
            this.EndTimeTimeEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EndTimeTimeEdit_Validating);
            this.EndTimeTimeEdit.Validated += new System.EventHandler(this.EndTimeTimeEdit_Validated);
            // 
            // StartTimeTimeEdit
            // 
            this.StartTimeTimeEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "StartTime", true));
            this.StartTimeTimeEdit.EditValue = System.TimeSpan.Parse("00:00:00");
            this.StartTimeTimeEdit.Location = new System.Drawing.Point(145, 286);
            this.StartTimeTimeEdit.MenuManager = this.barManager1;
            this.StartTimeTimeEdit.Name = "StartTimeTimeEdit";
            this.StartTimeTimeEdit.Properties.AllowEditDays = false;
            this.StartTimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartTimeTimeEdit.Properties.Mask.EditMask = "HH:mm:ss";
            this.StartTimeTimeEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartTimeTimeEdit.Size = new System.Drawing.Size(136, 20);
            this.StartTimeTimeEdit.StyleController = this.dataLayoutControl1;
            this.StartTimeTimeEdit.TabIndex = 44;
            this.StartTimeTimeEdit.EditValueChanged += new System.EventHandler(this.StartTimeTimeEdit_EditValueChanged);
            this.StartTimeTimeEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StartTimeTimeEdit_Validating);
            this.StartTimeTimeEdit.Validated += new System.EventHandler(this.StartTimeTimeEdit_Validated);
            // 
            // InformationLabel
            // 
            this.InformationLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.InformationLabel.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("InformationLabel.Appearance.Image")));
            this.InformationLabel.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.InformationLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.InformationLabel.Location = new System.Drawing.Point(463, 12);
            this.InformationLabel.Name = "InformationLabel";
            this.InformationLabel.Size = new System.Drawing.Size(119, 16);
            this.InformationLabel.StyleController = this.dataLayoutControl1;
            this.InformationLabel.TabIndex = 43;
            this.InformationLabel.Text = "       Dummy Shift";
            // 
            // CreateDummyShiftButton
            // 
            this.CreateDummyShiftButton.Location = new System.Drawing.Point(586, 12);
            this.CreateDummyShiftButton.Name = "CreateDummyShiftButton";
            this.CreateDummyShiftButton.Size = new System.Drawing.Size(126, 22);
            this.CreateDummyShiftButton.StyleController = this.dataLayoutControl1;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Create Dummy Shift - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.CreateDummyShiftButton.SuperTip = superToolTip4;
            this.CreateDummyShiftButton.TabIndex = 42;
            this.CreateDummyShiftButton.Text = "Create Dummy Shift";
            this.CreateDummyShiftButton.Click += new System.EventHandler(this.CreateDummyShiftButton_Click);
            // 
            // EmployeeIDTextEdit
            // 
            this.EmployeeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "EmployeeID", true));
            this.EmployeeIDTextEdit.Location = new System.Drawing.Point(133, 140);
            this.EmployeeIDTextEdit.MenuManager = this.barManager1;
            this.EmployeeIDTextEdit.Name = "EmployeeIDTextEdit";
            this.EmployeeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIDTextEdit, true);
            this.EmployeeIDTextEdit.Size = new System.Drawing.Size(567, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIDTextEdit, optionsSpelling1);
            this.EmployeeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIDTextEdit.TabIndex = 41;
            // 
            // HeaderEndDateTextEdit
            // 
            this.HeaderEndDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "HeaderEndDate", true));
            this.HeaderEndDateTextEdit.Location = new System.Drawing.Point(473, 120);
            this.HeaderEndDateTextEdit.MenuManager = this.barManager1;
            this.HeaderEndDateTextEdit.Name = "HeaderEndDateTextEdit";
            this.HeaderEndDateTextEdit.Properties.Mask.EditMask = "d";
            this.HeaderEndDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.HeaderEndDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HeaderEndDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HeaderEndDateTextEdit, true);
            this.HeaderEndDateTextEdit.Size = new System.Drawing.Size(227, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HeaderEndDateTextEdit, optionsSpelling2);
            this.HeaderEndDateTextEdit.StyleController = this.dataLayoutControl1;
            this.HeaderEndDateTextEdit.TabIndex = 40;
            // 
            // HeaderStartDateTextEdit
            // 
            this.HeaderStartDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "HeaderStartDate", true));
            this.HeaderStartDateTextEdit.Location = new System.Drawing.Point(133, 120);
            this.HeaderStartDateTextEdit.MenuManager = this.barManager1;
            this.HeaderStartDateTextEdit.Name = "HeaderStartDateTextEdit";
            this.HeaderStartDateTextEdit.Properties.Mask.EditMask = "d";
            this.HeaderStartDateTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.HeaderStartDateTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.HeaderStartDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.HeaderStartDateTextEdit, true);
            this.HeaderStartDateTextEdit.Size = new System.Drawing.Size(227, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.HeaderStartDateTextEdit, optionsSpelling3);
            this.HeaderStartDateTextEdit.StyleController = this.dataLayoutControl1;
            this.HeaderStartDateTextEdit.TabIndex = 39;
            // 
            // EmployeeNameTextEdit
            // 
            this.EmployeeNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "EmployeeName", true));
            this.EmployeeNameTextEdit.Location = new System.Drawing.Point(133, 96);
            this.EmployeeNameTextEdit.MenuManager = this.barManager1;
            this.EmployeeNameTextEdit.Name = "EmployeeNameTextEdit";
            this.EmployeeNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNameTextEdit, true);
            this.EmployeeNameTextEdit.Size = new System.Drawing.Size(567, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNameTextEdit, optionsSpelling4);
            this.EmployeeNameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameTextEdit.TabIndex = 38;
            // 
            // RegionNameTextEdit
            // 
            this.RegionNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "RegionName", true));
            this.RegionNameTextEdit.Location = new System.Drawing.Point(157, 497);
            this.RegionNameTextEdit.MenuManager = this.barManager1;
            this.RegionNameTextEdit.Name = "RegionNameTextEdit";
            this.RegionNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RegionNameTextEdit, true);
            this.RegionNameTextEdit.Size = new System.Drawing.Size(519, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.RegionNameTextEdit, optionsSpelling5);
            this.RegionNameTextEdit.StyleController = this.dataLayoutControl1;
            this.RegionNameTextEdit.TabIndex = 37;
            // 
            // LocationNameTextEdit
            // 
            this.LocationNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "LocationName", true));
            this.LocationNameTextEdit.Location = new System.Drawing.Point(157, 521);
            this.LocationNameTextEdit.MenuManager = this.barManager1;
            this.LocationNameTextEdit.Name = "LocationNameTextEdit";
            this.LocationNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LocationNameTextEdit, true);
            this.LocationNameTextEdit.Size = new System.Drawing.Size(519, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LocationNameTextEdit, optionsSpelling6);
            this.LocationNameTextEdit.StyleController = this.dataLayoutControl1;
            this.LocationNameTextEdit.TabIndex = 36;
            // 
            // BusinessAreaNameTextEdit
            // 
            this.BusinessAreaNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "BusinessAreaName", true));
            this.BusinessAreaNameTextEdit.Location = new System.Drawing.Point(157, 473);
            this.BusinessAreaNameTextEdit.MenuManager = this.barManager1;
            this.BusinessAreaNameTextEdit.Name = "BusinessAreaNameTextEdit";
            this.BusinessAreaNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.BusinessAreaNameTextEdit, true);
            this.BusinessAreaNameTextEdit.Size = new System.Drawing.Size(519, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.BusinessAreaNameTextEdit, optionsSpelling7);
            this.BusinessAreaNameTextEdit.StyleController = this.dataLayoutControl1;
            this.BusinessAreaNameTextEdit.TabIndex = 35;
            // 
            // EmployeeDepartmentWorkedIDTextEdit
            // 
            this.EmployeeDepartmentWorkedIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "EmployeeDepartmentWorkedID", true));
            this.EmployeeDepartmentWorkedIDTextEdit.Location = new System.Drawing.Point(166, 373);
            this.EmployeeDepartmentWorkedIDTextEdit.MenuManager = this.barManager1;
            this.EmployeeDepartmentWorkedIDTextEdit.Name = "EmployeeDepartmentWorkedIDTextEdit";
            this.EmployeeDepartmentWorkedIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeDepartmentWorkedIDTextEdit, true);
            this.EmployeeDepartmentWorkedIDTextEdit.Size = new System.Drawing.Size(527, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeDepartmentWorkedIDTextEdit, optionsSpelling8);
            this.EmployeeDepartmentWorkedIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeDepartmentWorkedIDTextEdit.TabIndex = 32;
            // 
            // DepartmentNameButtonEdit
            // 
            this.DepartmentNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "DepartmentName", true));
            this.DepartmentNameButtonEdit.Location = new System.Drawing.Point(157, 449);
            this.DepartmentNameButtonEdit.MenuManager = this.barManager1;
            this.DepartmentNameButtonEdit.Name = "DepartmentNameButtonEdit";
            this.DepartmentNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click to Select Business Area, Department and Office", "choose", null, true)});
            this.DepartmentNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.DepartmentNameButtonEdit.Size = new System.Drawing.Size(519, 20);
            this.DepartmentNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.DepartmentNameButtonEdit.TabIndex = 31;
            this.DepartmentNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.DepartmentNameButtonEdit_ButtonClick);
            // 
            // PaidForBreaksCheckEdit
            // 
            this.PaidForBreaksCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "PaidForBreaks", true));
            this.PaidForBreaksCheckEdit.Location = new System.Drawing.Point(145, 358);
            this.PaidForBreaksCheckEdit.MenuManager = this.barManager1;
            this.PaidForBreaksCheckEdit.Name = "PaidForBreaksCheckEdit";
            this.PaidForBreaksCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.PaidForBreaksCheckEdit.Properties.ValueChecked = 1;
            this.PaidForBreaksCheckEdit.Properties.ValueUnchecked = 0;
            this.PaidForBreaksCheckEdit.Size = new System.Drawing.Size(136, 19);
            this.PaidForBreaksCheckEdit.StyleController = this.dataLayoutControl1;
            this.PaidForBreaksCheckEdit.TabIndex = 30;
            this.PaidForBreaksCheckEdit.CheckedChanged += new System.EventHandler(this.PaidForBreaksCheckEdit_CheckedChanged);
            this.PaidForBreaksCheckEdit.EditValueChanged += new System.EventHandler(this.PaidForBreaksCheckEdit_EditValueChanged);
            // 
            // DayOfWeekGridLookUpEdit
            // 
            this.DayOfWeekGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "DayOfWeek", true));
            this.DayOfWeekGridLookUpEdit.Location = new System.Drawing.Point(145, 262);
            this.DayOfWeekGridLookUpEdit.MenuManager = this.barManager1;
            this.DayOfWeekGridLookUpEdit.Name = "DayOfWeekGridLookUpEdit";
            this.DayOfWeekGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DayOfWeekGridLookUpEdit.Properties.DataSource = this.spHR00049WorkPatternDaysOfWeekWithBlankBindingSource;
            this.DayOfWeekGridLookUpEdit.Properties.DisplayMember = "Description";
            this.DayOfWeekGridLookUpEdit.Properties.NullText = "";
            this.DayOfWeekGridLookUpEdit.Properties.ValueMember = "ID";
            this.DayOfWeekGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.DayOfWeekGridLookUpEdit.Size = new System.Drawing.Size(136, 20);
            this.DayOfWeekGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.DayOfWeekGridLookUpEdit.TabIndex = 29;
            this.DayOfWeekGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DayOfWeekGridLookUpEdit_Validating);
            // 
            // spHR00049WorkPatternDaysOfWeekWithBlankBindingSource
            // 
            this.spHR00049WorkPatternDaysOfWeekWithBlankBindingSource.DataMember = "sp_HR_00049_Work_Pattern_Days_Of_Week_With_Blank";
            this.spHR00049WorkPatternDaysOfWeekWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.colDescription,
            this.colOrder});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = -1;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Day of Week";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 220;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // WorkingPatternIDTextEdit
            // 
            this.WorkingPatternIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "WorkingPatternID", true));
            this.WorkingPatternIDTextEdit.Location = new System.Drawing.Point(149, 95);
            this.WorkingPatternIDTextEdit.MenuManager = this.barManager1;
            this.WorkingPatternIDTextEdit.Name = "WorkingPatternIDTextEdit";
            this.WorkingPatternIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkingPatternIDTextEdit, true);
            this.WorkingPatternIDTextEdit.Size = new System.Drawing.Size(563, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkingPatternIDTextEdit, optionsSpelling9);
            this.WorkingPatternIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkingPatternIDTextEdit.TabIndex = 19;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00047GetEmployeeWorkingPatternItemsBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(132, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(173, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 17;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // WorkingPatternHeaderIDTextEdit
            // 
            this.WorkingPatternHeaderIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "WorkingPatternHeaderID", true));
            this.WorkingPatternHeaderIDTextEdit.Location = new System.Drawing.Point(149, 71);
            this.WorkingPatternHeaderIDTextEdit.MenuManager = this.barManager1;
            this.WorkingPatternHeaderIDTextEdit.Name = "WorkingPatternHeaderIDTextEdit";
            this.WorkingPatternHeaderIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.WorkingPatternHeaderIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.WorkingPatternHeaderIDTextEdit, true);
            this.WorkingPatternHeaderIDTextEdit.Size = new System.Drawing.Size(580, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.WorkingPatternHeaderIDTextEdit, optionsSpelling10);
            this.WorkingPatternHeaderIDTextEdit.StyleController = this.dataLayoutControl1;
            this.WorkingPatternHeaderIDTextEdit.TabIndex = 4;
            this.WorkingPatternHeaderIDTextEdit.TabStop = false;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 238);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(652, 325);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling11);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 27;
            // 
            // CalculatedHoursTimeEdit
            // 
            this.CalculatedHoursTimeEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "CalculatedHours", true));
            this.CalculatedHoursTimeEdit.EditValue = "";
            this.CalculatedHoursTimeEdit.Location = new System.Drawing.Point(145, 381);
            this.CalculatedHoursTimeEdit.MenuManager = this.barManager1;
            this.CalculatedHoursTimeEdit.Name = "CalculatedHoursTimeEdit";
            this.CalculatedHoursTimeEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CalculatedHoursTimeEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalculatedHoursTimeEdit.Properties.Mask.EditMask = "####0.00 Hours";
            this.CalculatedHoursTimeEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CalculatedHoursTimeEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CalculatedHoursTimeEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.CalculatedHoursTimeEdit, true);
            this.CalculatedHoursTimeEdit.Size = new System.Drawing.Size(136, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.CalculatedHoursTimeEdit, optionsSpelling12);
            this.CalculatedHoursTimeEdit.StyleController = this.dataLayoutControl1;
            this.CalculatedHoursTimeEdit.TabIndex = 25;
            // 
            // HeaderDescriptionButtonEdit
            // 
            this.HeaderDescriptionButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "HeaderDescription", true));
            this.HeaderDescriptionButtonEdit.EditValue = "";
            this.HeaderDescriptionButtonEdit.Location = new System.Drawing.Point(133, 72);
            this.HeaderDescriptionButtonEdit.MenuManager = this.barManager1;
            this.HeaderDescriptionButtonEdit.Name = "HeaderDescriptionButtonEdit";
            this.HeaderDescriptionButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click to open Select Working Pattern Header screen", "choose", null, true)});
            this.HeaderDescriptionButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.HeaderDescriptionButtonEdit.Size = new System.Drawing.Size(567, 20);
            this.HeaderDescriptionButtonEdit.StyleController = this.dataLayoutControl1;
            this.HeaderDescriptionButtonEdit.TabIndex = 28;
            this.HeaderDescriptionButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.HeaderDescriptionButtonEdit_ButtonClick);
            this.HeaderDescriptionButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.HeaderDescriptionButtonEdit_Validating);
            // 
            // BreakLengthMinsSpinEdit
            // 
            this.BreakLengthMinsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "BreakLength", true));
            this.BreakLengthMinsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.BreakLengthMinsSpinEdit.Location = new System.Drawing.Point(145, 334);
            this.BreakLengthMinsSpinEdit.MenuManager = this.barManager1;
            this.BreakLengthMinsSpinEdit.Name = "BreakLengthMinsSpinEdit";
            this.BreakLengthMinsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BreakLengthMinsSpinEdit.Properties.IsFloatValue = false;
            this.BreakLengthMinsSpinEdit.Properties.Mask.EditMask = "###0 Minutes";
            this.BreakLengthMinsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.BreakLengthMinsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.BreakLengthMinsSpinEdit.Size = new System.Drawing.Size(136, 20);
            this.BreakLengthMinsSpinEdit.StyleController = this.dataLayoutControl1;
            this.BreakLengthMinsSpinEdit.TabIndex = 24;
            this.BreakLengthMinsSpinEdit.ToolTip = "The combined length of all breaks, in minutes";
            this.BreakLengthMinsSpinEdit.EditValueChanged += new System.EventHandler(this.BreaksLengthMinsSpinEdit_EditValueChanged);
            this.BreakLengthMinsSpinEdit.Validated += new System.EventHandler(this.BreaksLengthMinsSpinEdit_Validated);
            // 
            // WeekNumberSpinEdit
            // 
            this.WeekNumberSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00047GetEmployeeWorkingPatternItemsBindingSource, "WeekNumber", true));
            this.WeekNumberSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.WeekNumberSpinEdit.Location = new System.Drawing.Point(145, 238);
            this.WeekNumberSpinEdit.MenuManager = this.barManager1;
            this.WeekNumberSpinEdit.Name = "WeekNumberSpinEdit";
            this.WeekNumberSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.WeekNumberSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.WeekNumberSpinEdit.Properties.IsFloatValue = false;
            this.WeekNumberSpinEdit.Properties.Mask.EditMask = "Week #0";
            this.WeekNumberSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.WeekNumberSpinEdit.Properties.MaxValue = new decimal(new int[] {
            53,
            0,
            0,
            0});
            this.WeekNumberSpinEdit.Properties.NullText = "1";
            this.WeekNumberSpinEdit.Size = new System.Drawing.Size(136, 20);
            this.WeekNumberSpinEdit.StyleController = this.dataLayoutControl1;
            this.WeekNumberSpinEdit.TabIndex = 20;
            // 
            // ItemForEmployeeDepartmentWorkedID
            // 
            this.ItemForEmployeeDepartmentWorkedID.Control = this.EmployeeDepartmentWorkedIDTextEdit;
            this.ItemForEmployeeDepartmentWorkedID.CustomizationFormText = "Department Worked ID:";
            this.ItemForEmployeeDepartmentWorkedID.Location = new System.Drawing.Point(0, 0);
            this.ItemForEmployeeDepartmentWorkedID.Name = "ItemForEmployeeDepartmentWorkedID";
            this.ItemForEmployeeDepartmentWorkedID.Size = new System.Drawing.Size(649, 24);
            this.ItemForEmployeeDepartmentWorkedID.Text = "Department Worked ID:";
            this.ItemForEmployeeDepartmentWorkedID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForWorkingPatternID
            // 
            this.ItemForWorkingPatternID.Control = this.WorkingPatternIDTextEdit;
            this.ItemForWorkingPatternID.CustomizationFormText = "Working Pattern ID:";
            this.ItemForWorkingPatternID.Location = new System.Drawing.Point(0, 83);
            this.ItemForWorkingPatternID.Name = "ItemForWorkingPatternID";
            this.ItemForWorkingPatternID.Size = new System.Drawing.Size(704, 24);
            this.ItemForWorkingPatternID.Text = "Working Pattern ID:";
            this.ItemForWorkingPatternID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForWorkingPatternHeaderID
            // 
            this.ItemForWorkingPatternHeaderID.Control = this.WorkingPatternHeaderIDTextEdit;
            this.ItemForWorkingPatternHeaderID.CustomizationFormText = "Working Pattern Header ID:";
            this.ItemForWorkingPatternHeaderID.Location = new System.Drawing.Point(0, 59);
            this.ItemForWorkingPatternHeaderID.Name = "ItemForWorkingPatternHeaderID";
            this.ItemForWorkingPatternHeaderID.Size = new System.Drawing.Size(721, 24);
            this.ItemForWorkingPatternHeaderID.Text = "Working Pattern Header ID:";
            this.ItemForWorkingPatternHeaderID.TextSize = new System.Drawing.Size(134, 13);
            // 
            // ItemForEmployeeID
            // 
            this.ItemForEmployeeID.Control = this.EmployeeIDTextEdit;
            this.ItemForEmployeeID.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForEmployeeID.Name = "ItemForEmployeeID";
            this.ItemForEmployeeID.Size = new System.Drawing.Size(680, 24);
            this.ItemForEmployeeID.Text = "Employee ID:";
            this.ItemForEmployeeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(724, 599);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5,
            this.emptySpaceItem1,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.layoutControlGroup7,
            this.ItemForCreateDummyShiftButton,
            this.ItemForInformationLabel,
            this.emptySpaceItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(704, 579);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "Details";
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup2});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 156);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(704, 423);
            this.layoutControlGroup5.Text = "Details";
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.CustomizationFormText = "tabbedControlGroup2";
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup2.SelectedTabPageIndex = 0;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(680, 377);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4,
            this.layoutControlGroup3});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Details:";
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForBreakLength,
            this.ItemForCalculatedHours,
            this.ItemForWeekNumber,
            this.ItemForDayOfWeek,
            this.emptySpaceItem2,
            this.ItemForPaidForBreaks,
            this.layoutControlGroup6,
            this.emptySpaceItem4,
            this.ItemForStartTime,
            this.ItemForEndTime,
            this.emptySpaceItem3});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(656, 329);
            this.layoutControlGroup4.Text = "Details";
            // 
            // ItemForBreakLength
            // 
            this.ItemForBreakLength.Control = this.BreakLengthMinsSpinEdit;
            this.ItemForBreakLength.CustomizationFormText = "Break Length:";
            this.ItemForBreakLength.Location = new System.Drawing.Point(0, 96);
            this.ItemForBreakLength.Name = "ItemForBreakLength";
            this.ItemForBreakLength.Size = new System.Drawing.Size(249, 24);
            this.ItemForBreakLength.Text = "Break Length:";
            this.ItemForBreakLength.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForCalculatedHours
            // 
            this.ItemForCalculatedHours.Control = this.CalculatedHoursTimeEdit;
            this.ItemForCalculatedHours.CustomizationFormText = "Calculated Hours:";
            this.ItemForCalculatedHours.Location = new System.Drawing.Point(0, 143);
            this.ItemForCalculatedHours.Name = "ItemForCalculatedHours";
            this.ItemForCalculatedHours.Size = new System.Drawing.Size(249, 24);
            this.ItemForCalculatedHours.Text = "Calculated Hours:";
            this.ItemForCalculatedHours.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForWeekNumber
            // 
            this.ItemForWeekNumber.Control = this.WeekNumberSpinEdit;
            this.ItemForWeekNumber.CustomizationFormText = "Week Number:";
            this.ItemForWeekNumber.Location = new System.Drawing.Point(0, 0);
            this.ItemForWeekNumber.Name = "ItemForWeekNumber";
            this.ItemForWeekNumber.Size = new System.Drawing.Size(249, 24);
            this.ItemForWeekNumber.Text = "Week Number:";
            this.ItemForWeekNumber.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForDayOfWeek
            // 
            this.ItemForDayOfWeek.Control = this.DayOfWeekGridLookUpEdit;
            this.ItemForDayOfWeek.CustomizationFormText = "Day of Week:";
            this.ItemForDayOfWeek.Location = new System.Drawing.Point(0, 24);
            this.ItemForDayOfWeek.Name = "ItemForDayOfWeek";
            this.ItemForDayOfWeek.Size = new System.Drawing.Size(249, 24);
            this.ItemForDayOfWeek.Text = "Day of Week:";
            this.ItemForDayOfWeek.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 319);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(656, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPaidForBreaks
            // 
            this.ItemForPaidForBreaks.Control = this.PaidForBreaksCheckEdit;
            this.ItemForPaidForBreaks.CustomizationFormText = "Paid For Breaks:";
            this.ItemForPaidForBreaks.Location = new System.Drawing.Point(0, 120);
            this.ItemForPaidForBreaks.Name = "ItemForPaidForBreaks";
            this.ItemForPaidForBreaks.Size = new System.Drawing.Size(249, 23);
            this.ItemForPaidForBreaks.Text = "Paid For Breaks:";
            this.ItemForPaidForBreaks.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Department Worked In";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForDepartmentName,
            this.ItemForBusinessAreaName,
            this.ItemForLocationName,
            this.ItemForRegionName});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 177);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(656, 142);
            this.layoutControlGroup6.Text = "Department Worked In";
            // 
            // ItemForDepartmentName
            // 
            this.ItemForDepartmentName.Control = this.DepartmentNameButtonEdit;
            this.ItemForDepartmentName.CustomizationFormText = "Department Name:";
            this.ItemForDepartmentName.Location = new System.Drawing.Point(0, 0);
            this.ItemForDepartmentName.Name = "ItemForDepartmentName";
            this.ItemForDepartmentName.Size = new System.Drawing.Size(632, 24);
            this.ItemForDepartmentName.Text = "Department Name:";
            this.ItemForDepartmentName.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForBusinessAreaName
            // 
            this.ItemForBusinessAreaName.Control = this.BusinessAreaNameTextEdit;
            this.ItemForBusinessAreaName.CustomizationFormText = "Business Area Name:";
            this.ItemForBusinessAreaName.Location = new System.Drawing.Point(0, 24);
            this.ItemForBusinessAreaName.Name = "ItemForBusinessAreaName";
            this.ItemForBusinessAreaName.Size = new System.Drawing.Size(632, 24);
            this.ItemForBusinessAreaName.Text = "Business Area Name:";
            this.ItemForBusinessAreaName.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForLocationName
            // 
            this.ItemForLocationName.Control = this.LocationNameTextEdit;
            this.ItemForLocationName.CustomizationFormText = "Location Name:";
            this.ItemForLocationName.Location = new System.Drawing.Point(0, 72);
            this.ItemForLocationName.Name = "ItemForLocationName";
            this.ItemForLocationName.Size = new System.Drawing.Size(632, 24);
            this.ItemForLocationName.Text = "Location Name:";
            this.ItemForLocationName.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForRegionName
            // 
            this.ItemForRegionName.Control = this.RegionNameTextEdit;
            this.ItemForRegionName.CustomizationFormText = "Region Name:";
            this.ItemForRegionName.Location = new System.Drawing.Point(0, 48);
            this.ItemForRegionName.Name = "ItemForRegionName";
            this.ItemForRegionName.Size = new System.Drawing.Size(632, 24);
            this.ItemForRegionName.Text = "Region Name:";
            this.ItemForRegionName.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 167);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(656, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForStartTime
            // 
            this.ItemForStartTime.Control = this.StartTimeTimeEdit;
            this.ItemForStartTime.Location = new System.Drawing.Point(0, 48);
            this.ItemForStartTime.Name = "ItemForStartTime";
            this.ItemForStartTime.Size = new System.Drawing.Size(249, 24);
            this.ItemForStartTime.Text = "Start Time:";
            this.ItemForStartTime.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForEndTime
            // 
            this.ItemForEndTime.Control = this.EndTimeTimeEdit;
            this.ItemForEndTime.Location = new System.Drawing.Point(0, 72);
            this.ItemForEndTime.Name = "ItemForEndTime";
            this.ItemForEndTime.Size = new System.Drawing.Size(249, 24);
            this.ItemForEndTime.Text = "End Time:";
            this.ItemForEndTime.TextSize = new System.Drawing.Size(106, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(249, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(407, 167);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks:";
            this.layoutControlGroup3.ExpandButtonVisible = true;
            this.layoutControlGroup3.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(656, 329);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(656, 329);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 144);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(704, 12);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(120, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(177, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(120, 0);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(120, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(120, 26);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Linked To";
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForHeaderDescription,
            this.ItemForHeaderStartDate,
            this.ItemForEmployeeName,
            this.ItemForHeaderEndDate});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 26);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(704, 118);
            this.layoutControlGroup7.Text = "Linked To";
            // 
            // ItemForHeaderDescription
            // 
            this.ItemForHeaderDescription.AllowHide = false;
            this.ItemForHeaderDescription.Control = this.HeaderDescriptionButtonEdit;
            this.ItemForHeaderDescription.CustomizationFormText = "Linked To Pattern Header:";
            this.ItemForHeaderDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForHeaderDescription.Name = "ItemForHeaderDescription";
            this.ItemForHeaderDescription.Size = new System.Drawing.Size(680, 24);
            this.ItemForHeaderDescription.Text = "Work Pattern Header:";
            this.ItemForHeaderDescription.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForHeaderStartDate
            // 
            this.ItemForHeaderStartDate.Control = this.HeaderStartDateTextEdit;
            this.ItemForHeaderStartDate.CustomizationFormText = "Header Start Date:";
            this.ItemForHeaderStartDate.Location = new System.Drawing.Point(0, 48);
            this.ItemForHeaderStartDate.Name = "ItemForHeaderStartDate";
            this.ItemForHeaderStartDate.Size = new System.Drawing.Size(340, 24);
            this.ItemForHeaderStartDate.Text = "Header Start Date:";
            this.ItemForHeaderStartDate.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.Control = this.EmployeeNameTextEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Employee Name:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 24);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(680, 24);
            this.ItemForEmployeeName.Text = "Employee Name:";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForHeaderEndDate
            // 
            this.ItemForHeaderEndDate.Control = this.HeaderEndDateTextEdit;
            this.ItemForHeaderEndDate.CustomizationFormText = "Header End Date:";
            this.ItemForHeaderEndDate.Location = new System.Drawing.Point(340, 48);
            this.ItemForHeaderEndDate.Name = "ItemForHeaderEndDate";
            this.ItemForHeaderEndDate.Size = new System.Drawing.Size(340, 24);
            this.ItemForHeaderEndDate.Text = "Header End Date:";
            this.ItemForHeaderEndDate.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForCreateDummyShiftButton
            // 
            this.ItemForCreateDummyShiftButton.Control = this.CreateDummyShiftButton;
            this.ItemForCreateDummyShiftButton.CustomizationFormText = "ItemForCreateDummyShiftButton";
            this.ItemForCreateDummyShiftButton.Location = new System.Drawing.Point(574, 0);
            this.ItemForCreateDummyShiftButton.MaxSize = new System.Drawing.Size(130, 26);
            this.ItemForCreateDummyShiftButton.MinSize = new System.Drawing.Size(130, 26);
            this.ItemForCreateDummyShiftButton.Name = "ItemForCreateDummyShiftButton";
            this.ItemForCreateDummyShiftButton.Size = new System.Drawing.Size(130, 26);
            this.ItemForCreateDummyShiftButton.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForCreateDummyShiftButton.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForCreateDummyShiftButton.TextVisible = false;
            // 
            // ItemForInformationLabel
            // 
            this.ItemForInformationLabel.Control = this.InformationLabel;
            this.ItemForInformationLabel.CustomizationFormText = "Information:";
            this.ItemForInformationLabel.Location = new System.Drawing.Point(451, 0);
            this.ItemForInformationLabel.MaxSize = new System.Drawing.Size(123, 20);
            this.ItemForInformationLabel.MinSize = new System.Drawing.Size(123, 17);
            this.ItemForInformationLabel.Name = "ItemForInformationLabel";
            this.ItemForInformationLabel.Size = new System.Drawing.Size(123, 26);
            this.ItemForInformationLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForInformationLabel.Text = "Information:";
            this.ItemForInformationLabel.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForInformationLabel.TextVisible = false;
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(297, 0);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(154, 26);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235picklisteditpermissionsBindingSource
            // 
            this.sp00235picklisteditpermissionsBindingSource.DataMember = "sp00235_picklist_edit_permissions";
            this.sp00235picklisteditpermissionsBindingSource.DataSource = this.dataSet_AT_DataEntry;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter
            // 
            this.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.sp_HR_00002_Employee_Depts_Worked_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00010_Get_Employee_Holiday_Year_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00013_Get_Employee_Address_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00017_Pension_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00019_Get_Employee_Next_Of_Kin_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00023_Master_Salary_Bandings_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00040_Employee_Bonus_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter = this.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter;
            this.tableAdapterManager.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00055_Get_Employee_Sanction_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00058_Get_Employee_Sanction_Progress_ItemsTableAdapter = null;
            this.tableAdapterManager.sp_HR_00085_Master_Holiday_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00093_Business_Area_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00095_Location_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00097_Department_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00100_Region_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00102_Department_Location_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00132_Employee_Vetting_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00137_Employee_Vetting_Restriction_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00143_Master_Vetting_Types_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00145_Master_Vetting_SubType_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00148_Master_Vetting_Restriction_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00155_Employee_Working_Pattern_Headers_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00164_Master_Working_Pattern_Headers_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00166_Master_Working_Pattern_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00175_Employee_Pension_Contribution_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00182_Linked_Document_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00194_Employee_CRM_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00201_Employee_Reference_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00214_Employee_PayRise_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00226_Employee_Shares_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00232_Master_Qualification_Types_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00250_Employee_Administer_EditTableAdapter = null;
            this.tableAdapterManager.sp_HR_00255_Business_Area_Insurance_Requirement_ItemTableAdapter = null;
            this.tableAdapterManager.sp_HR_00258_Employee_Subsistence_EditTableAdapter = null;
            this.tableAdapterManager.sp09002_HR_Employee_ItemTableAdapter = null;
            this.tableAdapterManager.sp09105_HR_Qualification_ItemTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_HR_DataEntryTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_HR_00049_Work_Pattern_Days_Of_Week_With_BlankTableAdapter
            // 
            this.sp_HR_00049_Work_Pattern_Days_Of_Week_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Employee_Working_Pattern_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(741, 584);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Working_Pattern_Edit";
            this.Text = "Edit Working Pattern";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Working_Pattern_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Working_Pattern_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Working_Pattern_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00047GetEmployeeWorkingPatternItemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.EndTimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartTimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderEndDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderStartDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RegionNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocationNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BusinessAreaNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeDepartmentWorkedIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DepartmentNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PaidForBreaksCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DayOfWeekGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00049WorkPatternDaysOfWeekWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkingPatternIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WorkingPatternHeaderIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CalculatedHoursTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HeaderDescriptionButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BreakLengthMinsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WeekNumberSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeDepartmentWorkedID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkingPatternID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWorkingPatternHeaderID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBreakLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCalculatedHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWeekNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDayOfWeek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPaidForBreaks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDepartmentName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBusinessAreaName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLocationName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRegionName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeaderDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeaderStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForHeaderEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCreateDummyShiftButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForInformationLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkingPatternHeaderID;
        private DevExpress.XtraEditors.TextEdit WorkingPatternHeaderIDTextEdit;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private WoodPlanDataSet woodPlanDataSet;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.TextEdit WorkingPatternIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWorkingPatternID;
        private System.Windows.Forms.BindingSource spHR00047GetEmployeeWorkingPatternItemsBindingSource;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHeaderDescription;
        private DataSet_HR_DataEntryTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraEditors.TextEdit CalculatedHoursTimeEdit;
        private DevExpress.XtraEditors.ButtonEdit HeaderDescriptionButtonEdit;
        private DevExpress.XtraEditors.SpinEdit BreakLengthMinsSpinEdit;
        private DevExpress.XtraEditors.GridLookUpEdit DayOfWeekGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBreakLength;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCalculatedHours;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWeekNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDayOfWeek;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00049WorkPatternDaysOfWeekWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00049_Work_Pattern_Days_Of_Week_With_BlankTableAdapter sp_HR_00049_Work_Pattern_Days_Of_Week_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraEditors.SpinEdit WeekNumberSpinEdit;
        private DevExpress.XtraEditors.CheckEdit PaidForBreaksCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPaidForBreaks;
        private DevExpress.XtraEditors.ButtonEdit DepartmentNameButtonEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDepartmentName;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraEditors.TextEdit EmployeeDepartmentWorkedIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeDepartmentWorkedID;
        private DevExpress.XtraEditors.TextEdit BusinessAreaNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBusinessAreaName;
        private DevExpress.XtraEditors.TextEdit LocationNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLocationName;
        private DevExpress.XtraEditors.TextEdit RegionNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRegionName;
        private DevExpress.XtraEditors.TextEdit EmployeeNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DevExpress.XtraEditors.TextEdit HeaderEndDateTextEdit;
        private DevExpress.XtraEditors.TextEdit HeaderStartDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHeaderStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForHeaderEndDate;
        private DevExpress.XtraEditors.TextEdit EmployeeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeID;
        private DevExpress.XtraEditors.SimpleButton CreateDummyShiftButton;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCreateDummyShiftButton;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraEditors.LabelControl InformationLabel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForInformationLabel;
        private DevExpress.XtraEditors.TimeSpanEdit EndTimeTimeEdit;
        private DevExpress.XtraEditors.TimeSpanEdit StartTimeTimeEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartTime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndTime;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}

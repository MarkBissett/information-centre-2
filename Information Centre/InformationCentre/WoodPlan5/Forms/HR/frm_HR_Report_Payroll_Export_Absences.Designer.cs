namespace WoodPlan5
{
    partial class frm_HR_Report_Payroll_Export_Absences
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Report_Payroll_Export_Absences));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.colCurrentEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00210ReportPayrollStartersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Reporting = new WoodPlan5.DataSet_HR_Reporting();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSector = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPayrollCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostCentre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCategory = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNINumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSalary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHours = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colWorkingPattern = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSortCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccountNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayslipEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayslipPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiDepartment = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDepartment = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlDepartments = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.spHR00114DepartmentFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDepartmentFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiEmployee = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlEmployees = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.spHR00115EmployeesByDeptFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnEmployeeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlDateRangeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemCheckEditActiveOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp_HR_00114_Department_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter();
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.spHR00212ReportPayrollAbsencesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAbsenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAbsenceEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMed3ExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportingPeriodAbsenceLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHolidayUnitDescriptor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCSPUnitsPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnpaidUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAbsenceLength = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsPaidHalfPay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsPaidOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditNumericUnknown = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.spHR00211ReportPayrollLeaversBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLeaveDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDatetime3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLeaveReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayBalance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumeric2DP = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSchemeCostsToRecover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTrainingCostsToRecover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEquipmentCostsToRecover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOtherCostsToRecover = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCategory1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp_HR_00210_Report_Payroll_StartersTableAdapter = new WoodPlan5.DataSet_HR_ReportingTableAdapters.sp_HR_00210_Report_Payroll_StartersTableAdapter();
            this.sp_HR_00211_Report_Payroll_LeaversTableAdapter = new WoodPlan5.DataSet_HR_ReportingTableAdapters.sp_HR_00211_Report_Payroll_LeaversTableAdapter();
            this.sp_HR_00212_Report_Payroll_AbsencesTableAdapter = new WoodPlan5.DataSet_HR_ReportingTableAdapters.sp_HR_00212_Report_Payroll_AbsencesTableAdapter();
            this.xtraGridBlending3 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending5 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00210ReportPayrollStartersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Reporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).BeginInit();
            this.popupContainerControlDepartments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).BeginInit();
            this.popupContainerControlEmployees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFilter)).BeginInit();
            this.popupContainerControlDateRangeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00212ReportPayrollAbsencesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericUnknown)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00211ReportPayrollLeaversBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1014, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 557);
            this.barDockControlBottom.Size = new System.Drawing.Size(1014, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 515);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1014, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 515);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.beiDateRange,
            this.beiDepartment,
            this.beiEmployee,
            this.bbiExportToExcel});
            this.barManager1.MaxItemId = 74;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly,
            this.repositoryItemPopupContainerEditDepartment,
            this.repositoryItemPopupContainerEditEmployee,
            this.repositoryItemCheckEditActiveOnly,
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colCurrentEmployee
            // 
            this.colCurrentEmployee.Caption = "Current Employee";
            this.colCurrentEmployee.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCurrentEmployee.FieldName = "CurrentEmployee";
            this.colCurrentEmployee.Name = "colCurrentEmployee";
            this.colCurrentEmployee.OptionsColumn.AllowEdit = false;
            this.colCurrentEmployee.OptionsColumn.AllowFocus = false;
            this.colCurrentEmployee.OptionsColumn.ReadOnly = true;
            this.colCurrentEmployee.Visible = true;
            this.colCurrentEmployee.VisibleIndex = 4;
            this.colCurrentEmployee.Width = 107;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00210ReportPayrollStartersBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditHours});
            this.gridControl1.Size = new System.Drawing.Size(1009, 489);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00210ReportPayrollStartersBindingSource
            // 
            this.spHR00210ReportPayrollStartersBindingSource.DataMember = "sp_HR_00210_Report_Payroll_Starters";
            this.spHR00210ReportPayrollStartersBindingSource.DataSource = this.dataSet_HR_Reporting;
            // 
            // dataSet_HR_Reporting
            // 
            this.dataSet_HR_Reporting.DataSetName = "DataSet_HR_Reporting";
            this.dataSet_HR_Reporting.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "linked_documents_16_16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeID1,
            this.colPayrollNumber,
            this.colEmployeeSurname,
            this.colEmployeeFirstname,
            this.colSector,
            this.colPayrollCompany,
            this.colCostCentre,
            this.colJobCategory,
            this.colNINumber,
            this.colStartDate,
            this.colSalary,
            this.colHours,
            this.colWorkingPattern,
            this.colBankName,
            this.colSortCode,
            this.colAccountNumber,
            this.colPayslipEmail,
            this.colPayslipPassword});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeFirstname, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colEmployeeID1
            // 
            this.colEmployeeID1.Caption = "Employee ID";
            this.colEmployeeID1.FieldName = "EmployeeID";
            this.colEmployeeID1.Name = "colEmployeeID1";
            this.colEmployeeID1.OptionsColumn.AllowEdit = false;
            this.colEmployeeID1.OptionsColumn.AllowFocus = false;
            this.colEmployeeID1.OptionsColumn.ReadOnly = true;
            this.colEmployeeID1.Width = 81;
            // 
            // colPayrollNumber
            // 
            this.colPayrollNumber.Caption = "Employee Number";
            this.colPayrollNumber.FieldName = "EmployeeNumber";
            this.colPayrollNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPayrollNumber.Name = "colPayrollNumber";
            this.colPayrollNumber.OptionsColumn.AllowEdit = false;
            this.colPayrollNumber.OptionsColumn.AllowFocus = false;
            this.colPayrollNumber.OptionsColumn.ReadOnly = true;
            this.colPayrollNumber.Visible = true;
            this.colPayrollNumber.VisibleIndex = 0;
            this.colPayrollNumber.Width = 107;
            // 
            // colEmployeeSurname
            // 
            this.colEmployeeSurname.Caption = "Employee Surname";
            this.colEmployeeSurname.FieldName = "EmployeeSurname";
            this.colEmployeeSurname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeSurname.Name = "colEmployeeSurname";
            this.colEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname.Visible = true;
            this.colEmployeeSurname.VisibleIndex = 1;
            this.colEmployeeSurname.Width = 125;
            // 
            // colEmployeeFirstname
            // 
            this.colEmployeeFirstname.Caption = "Employee First Name";
            this.colEmployeeFirstname.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeFirstname.Name = "colEmployeeFirstname";
            this.colEmployeeFirstname.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname.Visible = true;
            this.colEmployeeFirstname.VisibleIndex = 2;
            this.colEmployeeFirstname.Width = 134;
            // 
            // colSector
            // 
            this.colSector.Caption = "Sector";
            this.colSector.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colSector.FieldName = "Sector";
            this.colSector.Name = "colSector";
            this.colSector.OptionsColumn.ReadOnly = true;
            this.colSector.Visible = true;
            this.colSector.VisibleIndex = 3;
            this.colSector.Width = 114;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colPayrollCompany
            // 
            this.colPayrollCompany.Caption = "Payroll Company";
            this.colPayrollCompany.FieldName = "PayrollCompany";
            this.colPayrollCompany.Name = "colPayrollCompany";
            this.colPayrollCompany.OptionsColumn.AllowEdit = false;
            this.colPayrollCompany.OptionsColumn.AllowFocus = false;
            this.colPayrollCompany.OptionsColumn.ReadOnly = true;
            this.colPayrollCompany.Visible = true;
            this.colPayrollCompany.VisibleIndex = 4;
            this.colPayrollCompany.Width = 109;
            // 
            // colCostCentre
            // 
            this.colCostCentre.Caption = "Cost Centre";
            this.colCostCentre.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colCostCentre.FieldName = "CostCentre";
            this.colCostCentre.Name = "colCostCentre";
            this.colCostCentre.OptionsColumn.ReadOnly = true;
            this.colCostCentre.Visible = true;
            this.colCostCentre.VisibleIndex = 5;
            this.colCostCentre.Width = 86;
            // 
            // colJobCategory
            // 
            this.colJobCategory.Caption = "Job Category";
            this.colJobCategory.FieldName = "JobCategory";
            this.colJobCategory.Name = "colJobCategory";
            this.colJobCategory.OptionsColumn.AllowEdit = false;
            this.colJobCategory.OptionsColumn.AllowFocus = false;
            this.colJobCategory.OptionsColumn.ReadOnly = true;
            this.colJobCategory.Visible = true;
            this.colJobCategory.VisibleIndex = 6;
            this.colJobCategory.Width = 101;
            // 
            // colNINumber
            // 
            this.colNINumber.Caption = "NI Number";
            this.colNINumber.FieldName = "NINumber";
            this.colNINumber.Name = "colNINumber";
            this.colNINumber.OptionsColumn.AllowEdit = false;
            this.colNINumber.OptionsColumn.AllowFocus = false;
            this.colNINumber.OptionsColumn.ReadOnly = true;
            this.colNINumber.Visible = true;
            this.colNINumber.VisibleIndex = 7;
            this.colNINumber.Width = 92;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 8;
            this.colStartDate.Width = 80;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colSalary
            // 
            this.colSalary.AppearanceCell.Options.UseTextOptions = true;
            this.colSalary.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colSalary.Caption = "Salary";
            this.colSalary.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSalary.FieldName = "Salary";
            this.colSalary.Name = "colSalary";
            this.colSalary.OptionsColumn.AllowEdit = false;
            this.colSalary.OptionsColumn.AllowFocus = false;
            this.colSalary.OptionsColumn.ReadOnly = true;
            this.colSalary.Visible = true;
            this.colSalary.VisibleIndex = 9;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colHours
            // 
            this.colHours.Caption = "Hours";
            this.colHours.ColumnEdit = this.repositoryItemTextEditHours;
            this.colHours.FieldName = "Hours";
            this.colHours.Name = "colHours";
            this.colHours.OptionsColumn.AllowEdit = false;
            this.colHours.OptionsColumn.AllowFocus = false;
            this.colHours.OptionsColumn.ReadOnly = true;
            this.colHours.Visible = true;
            this.colHours.VisibleIndex = 10;
            // 
            // repositoryItemTextEditHours
            // 
            this.repositoryItemTextEditHours.AutoHeight = false;
            this.repositoryItemTextEditHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHours.Name = "repositoryItemTextEditHours";
            // 
            // colWorkingPattern
            // 
            this.colWorkingPattern.Caption = "Working Pattern";
            this.colWorkingPattern.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colWorkingPattern.FieldName = "WorkingPattern";
            this.colWorkingPattern.Name = "colWorkingPattern";
            this.colWorkingPattern.OptionsColumn.ReadOnly = true;
            this.colWorkingPattern.Visible = true;
            this.colWorkingPattern.VisibleIndex = 11;
            this.colWorkingPattern.Width = 99;
            // 
            // colBankName
            // 
            this.colBankName.Caption = "Bank Name";
            this.colBankName.FieldName = "BankName";
            this.colBankName.Name = "colBankName";
            this.colBankName.OptionsColumn.AllowEdit = false;
            this.colBankName.OptionsColumn.AllowFocus = false;
            this.colBankName.OptionsColumn.ReadOnly = true;
            this.colBankName.Visible = true;
            this.colBankName.VisibleIndex = 12;
            this.colBankName.Width = 111;
            // 
            // colSortCode
            // 
            this.colSortCode.Caption = "Sort Code";
            this.colSortCode.FieldName = "SortCode";
            this.colSortCode.Name = "colSortCode";
            this.colSortCode.OptionsColumn.AllowEdit = false;
            this.colSortCode.OptionsColumn.AllowFocus = false;
            this.colSortCode.OptionsColumn.ReadOnly = true;
            this.colSortCode.Visible = true;
            this.colSortCode.VisibleIndex = 13;
            this.colSortCode.Width = 103;
            // 
            // colAccountNumber
            // 
            this.colAccountNumber.Caption = "Account Number";
            this.colAccountNumber.FieldName = "AccountNumber";
            this.colAccountNumber.Name = "colAccountNumber";
            this.colAccountNumber.OptionsColumn.AllowEdit = false;
            this.colAccountNumber.OptionsColumn.AllowFocus = false;
            this.colAccountNumber.OptionsColumn.ReadOnly = true;
            this.colAccountNumber.Visible = true;
            this.colAccountNumber.VisibleIndex = 14;
            this.colAccountNumber.Width = 100;
            // 
            // colPayslipEmail
            // 
            this.colPayslipEmail.Caption = "Payslip Email Address";
            this.colPayslipEmail.FieldName = "PayslipEmail";
            this.colPayslipEmail.Name = "colPayslipEmail";
            this.colPayslipEmail.OptionsColumn.AllowEdit = false;
            this.colPayslipEmail.OptionsColumn.AllowFocus = false;
            this.colPayslipEmail.OptionsColumn.ReadOnly = true;
            this.colPayslipEmail.Visible = true;
            this.colPayslipEmail.VisibleIndex = 15;
            this.colPayslipEmail.Width = 163;
            // 
            // colPayslipPassword
            // 
            this.colPayslipPassword.Caption = "Payslip Email Password";
            this.colPayslipPassword.FieldName = "PayslipPassword";
            this.colPayslipPassword.Name = "colPayslipPassword";
            this.colPayslipPassword.OptionsColumn.AllowEdit = false;
            this.colPayslipPassword.OptionsColumn.AllowFocus = false;
            this.colPayslipPassword.OptionsColumn.ReadOnly = true;
            this.colPayslipPassword.Visible = true;
            this.colPayslipPassword.VisibleIndex = 16;
            this.colPayslipPassword.Width = 130;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDepartment, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiEmployee),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDateRange, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportToExcel, true)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // beiDepartment
            // 
            this.beiDepartment.Caption = "Department(s):";
            this.beiDepartment.Edit = this.repositoryItemPopupContainerEditDepartment;
            this.beiDepartment.EditValue = "No Department Filter";
            this.beiDepartment.EditWidth = 160;
            this.beiDepartment.Id = 31;
            this.beiDepartment.Name = "beiDepartment";
            this.beiDepartment.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Department(s) - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Select one or more department from me to view just those departments. \r\n\r\nSelect " +
    "nothing from me to view all departments.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiDepartment.SuperTip = superToolTip1;
            // 
            // repositoryItemPopupContainerEditDepartment
            // 
            this.repositoryItemPopupContainerEditDepartment.AutoHeight = false;
            this.repositoryItemPopupContainerEditDepartment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDepartment.Name = "repositoryItemPopupContainerEditDepartment";
            this.repositoryItemPopupContainerEditDepartment.PopupControl = this.popupContainerControlDepartments;
            this.repositoryItemPopupContainerEditDepartment.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDepartment_QueryResultValue);
            // 
            // popupContainerControlDepartments
            // 
            this.popupContainerControlDepartments.Controls.Add(this.gridControl2);
            this.popupContainerControlDepartments.Controls.Add(this.btnDepartmentFilterOK);
            this.popupContainerControlDepartments.Location = new System.Drawing.Point(292, 96);
            this.popupContainerControlDepartments.Name = "popupContainerControlDepartments";
            this.popupContainerControlDepartments.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlDepartments.TabIndex = 3;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.spHR00114DepartmentFilterListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(230, 141);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00114DepartmentFilterListBindingSource
            // 
            this.spHR00114DepartmentFilterListBindingSource.DataMember = "sp_HR_00114_Department_Filter_List";
            this.spHR00114DepartmentFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepartmentID,
            this.colBusinessAreaID1,
            this.colDepartmentName,
            this.gridColumn2,
            this.colBusinessAreaName1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBusinessAreaName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartmentName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.Caption = "Department ID";
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.Width = 92;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.Width = 102;
            // 
            // colDepartmentName
            // 
            this.colDepartmentName.Caption = "Department Name";
            this.colDepartmentName.FieldName = "DepartmentName";
            this.colDepartmentName.Name = "colDepartmentName";
            this.colDepartmentName.OptionsColumn.AllowEdit = false;
            this.colDepartmentName.OptionsColumn.AllowFocus = false;
            this.colDepartmentName.OptionsColumn.ReadOnly = true;
            this.colDepartmentName.Visible = true;
            this.colDepartmentName.VisibleIndex = 0;
            this.colDepartmentName.Width = 224;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Remarks";
            this.gridColumn2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn2.FieldName = "Remarks";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 175;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area Name";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.Width = 224;
            // 
            // btnDepartmentFilterOK
            // 
            this.btnDepartmentFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDepartmentFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnDepartmentFilterOK.Name = "btnDepartmentFilterOK";
            this.btnDepartmentFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDepartmentFilterOK.TabIndex = 2;
            this.btnDepartmentFilterOK.Text = "OK";
            this.btnDepartmentFilterOK.Click += new System.EventHandler(this.btnDepartmentFilterOK_Click);
            // 
            // beiEmployee
            // 
            this.beiEmployee.Caption = "Employee(s):";
            this.beiEmployee.Edit = this.repositoryItemPopupContainerEditEmployee;
            this.beiEmployee.EditValue = "No Employee Filter";
            this.beiEmployee.EditWidth = 158;
            this.beiEmployee.Id = 32;
            this.beiEmployee.Name = "beiEmployee";
            this.beiEmployee.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Employee(s) - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Select one or more employees from me to view just those employees.\r\n\r\nSelect noth" +
    "ing from me to view all employees.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.beiEmployee.SuperTip = superToolTip2;
            // 
            // repositoryItemPopupContainerEditEmployee
            // 
            this.repositoryItemPopupContainerEditEmployee.AutoHeight = false;
            this.repositoryItemPopupContainerEditEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditEmployee.Name = "repositoryItemPopupContainerEditEmployee";
            this.repositoryItemPopupContainerEditEmployee.PopupControl = this.popupContainerControlEmployees;
            this.repositoryItemPopupContainerEditEmployee.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditEmployee_QueryResultValue);
            // 
            // popupContainerControlEmployees
            // 
            this.popupContainerControlEmployees.Controls.Add(this.gridControl4);
            this.popupContainerControlEmployees.Controls.Add(this.btnEmployeeFilterOK);
            this.popupContainerControlEmployees.Location = new System.Drawing.Point(35, 96);
            this.popupContainerControlEmployees.Name = "popupContainerControlEmployees";
            this.popupContainerControlEmployees.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlEmployees.TabIndex = 6;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.spHR00115EmployeesByDeptFilterListBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(2, 2);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(230, 141);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // spHR00115EmployeesByDeptFilterListBindingSource
            // 
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataMember = "sp_HR_00115_Employees_By_Dept_Filter_List";
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeID,
            this.colEmployeeNumber1,
            this.colTitle,
            this.colFirstname,
            this.colSurname,
            this.colCurrentEmployee,
            this.colRemarks1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colCurrentEmployee;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFirstname, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colEmployeeID
            // 
            this.colEmployeeID.Caption = "Employee ID";
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.Name = "colEmployeeID";
            this.colEmployeeID.OptionsColumn.AllowEdit = false;
            this.colEmployeeID.OptionsColumn.AllowFocus = false;
            this.colEmployeeID.OptionsColumn.ReadOnly = true;
            this.colEmployeeID.Width = 81;
            // 
            // colEmployeeNumber1
            // 
            this.colEmployeeNumber1.Caption = "Employee #";
            this.colEmployeeNumber1.FieldName = "EmployeeNumber";
            this.colEmployeeNumber1.Name = "colEmployeeNumber1";
            this.colEmployeeNumber1.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber1.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber1.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber1.Visible = true;
            this.colEmployeeNumber1.VisibleIndex = 2;
            this.colEmployeeNumber1.Width = 91;
            // 
            // colTitle
            // 
            this.colTitle.Caption = "Title";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 3;
            this.colTitle.Width = 59;
            // 
            // colFirstname
            // 
            this.colFirstname.Caption = "Forename";
            this.colFirstname.FieldName = "Firstname";
            this.colFirstname.Name = "colFirstname";
            this.colFirstname.OptionsColumn.AllowEdit = false;
            this.colFirstname.OptionsColumn.AllowFocus = false;
            this.colFirstname.OptionsColumn.ReadOnly = true;
            this.colFirstname.Visible = true;
            this.colFirstname.VisibleIndex = 1;
            this.colFirstname.Width = 110;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 0;
            this.colSurname.Width = 126;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // btnEmployeeFilterOK
            // 
            this.btnEmployeeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEmployeeFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnEmployeeFilterOK.Name = "btnEmployeeFilterOK";
            this.btnEmployeeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnEmployeeFilterOK.TabIndex = 2;
            this.btnEmployeeFilterOK.Text = "OK";
            this.btnEmployeeFilterOK.Click += new System.EventHandler(this.btnEmployeeFilterOK_Click);
            // 
            // beiDateRange
            // 
            this.beiDateRange.Caption = "Date Range:";
            this.beiDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.beiDateRange.EditValue = "No Date Range Filter";
            this.beiDateRange.EditWidth = 228;
            this.beiDateRange.Id = 28;
            this.beiDateRange.Name = "beiDateRange";
            this.beiDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Date Range - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Optionally select a Start and \\ or End Date from me to restrict the date range of" +
    " the data.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.beiDateRange.SuperTip = superToolTip3;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRangeFilter;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditType_QueryResultValue);
            // 
            // popupContainerControlDateRangeFilter
            // 
            this.popupContainerControlDateRangeFilter.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRangeFilter.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRangeFilter.Location = new System.Drawing.Point(543, 96);
            this.popupContainerControlDateRangeFilter.Name = "popupContainerControlDateRangeFilter";
            this.popupContainerControlDateRangeFilter.Size = new System.Drawing.Size(191, 112);
            this.popupContainerControlDateRangeFilter.TabIndex = 7;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditEnd);
            this.groupControl1.Controls.Add(this.dateEditStart);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(185, 78);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Date Range Filter";
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(65, 51);
            this.dateEditEnd.MenuManager = this.barManager1;
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.Mask.EditMask = "g";
            this.dateEditEnd.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditEnd.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditEnd.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditEnd.Size = new System.Drawing.Size(115, 20);
            this.dateEditEnd.TabIndex = 3;
            // 
            // dateEditStart
            // 
            this.dateEditStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(65, 25);
            this.dateEditStart.MenuManager = this.barManager1;
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.Mask.EditMask = "g";
            this.dateEditStart.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditStart.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditStart.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditStart.Size = new System.Drawing.Size(115, 20);
            this.dateEditStart.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(7, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "End Date:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Start Date:";
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 86);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDateRangeFilterOK.TabIndex = 2;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem4.Text = "Refresh - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Click me to refresh the data within all grids. \r\n\r\nAny Department, Employee and D" +
    "ate Range filters will be applied when loading the data.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiRefresh.SuperTip = superToolTip4;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.bbiExportToExcel.Caption = "Export";
            this.bbiExportToExcel.Id = 73;
            this.bbiExportToExcel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiExportToExcel.ImageOptions.Image")));
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            this.bbiExportToExcel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem5.Text = "Export - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to export the contents of all grids into one Excel spreadsheet.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiExportToExcel.SuperTip = superToolTip5;
            this.bbiExportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportToExcel_ItemClick);
            // 
            // repositoryItemCheckEditActiveOnly
            // 
            this.repositoryItemCheckEditActiveOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AutoHeight = false;
            this.repositoryItemCheckEditActiveOnly.Caption = "Check";
            this.repositoryItemCheckEditActiveOnly.Name = "repositoryItemCheckEditActiveOnly";
            this.repositoryItemCheckEditActiveOnly.ValueChecked = 1;
            this.repositoryItemCheckEditActiveOnly.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEditShowActiveHoilidayYearOnly
            // 
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Caption = "(Tick if Yes)";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Name = "repositoryItemCheckEditShowActiveHoilidayYearOnly";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // sp_HR_00114_Department_Filter_ListTableAdapter
            // 
            this.sp_HR_00114_Department_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter
            // 
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 42);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(1014, 515);
            this.xtraTabControl1.TabIndex = 8;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.popupContainerControlDateRangeFilter);
            this.xtraTabPage1.Controls.Add(this.popupContainerControlDepartments);
            this.xtraTabPage1.Controls.Add(this.popupContainerControlEmployees);
            this.xtraTabPage1.Controls.Add(this.gridControl1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1009, 489);
            this.xtraTabPage1.Text = "New Starters";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl3);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1009, 489);
            this.xtraTabPage2.Text = "Absences";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.spHR00212ReportPayrollAbsencesBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditNumericDays,
            this.repositoryItemTextEditNumericHours,
            this.repositoryItemTextEditNumericUnknown});
            this.gridControl3.Size = new System.Drawing.Size(1009, 489);
            this.gridControl3.TabIndex = 1;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // spHR00212ReportPayrollAbsencesBindingSource
            // 
            this.spHR00212ReportPayrollAbsencesBindingSource.DataMember = "sp_HR_00212_Report_Payroll_Absences";
            this.spHR00212ReportPayrollAbsencesBindingSource.DataSource = this.dataSet_HR_Reporting;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAbsenceID,
            this.colHolidayYearID,
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.colAbsenceType,
            this.colAbsenceReason,
            this.colAbsenceStart,
            this.colAbsenceEnd,
            this.colMed3ExpiryDate,
            this.colReportingPeriodAbsenceLength,
            this.colHolidayUnitDescriptor1,
            this.colCSPUnitsPaid,
            this.colUnpaidUnits,
            this.colTotalAbsenceLength,
            this.colUnitsPaidHalfPay,
            this.colUnitsPaidOther});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAbsenceStart, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView3_CustomDrawCell);
            this.gridView3.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView3_CustomRowCellEdit);
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView5_ShowingEditor);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colAbsenceID
            // 
            this.colAbsenceID.Caption = "Absence ID";
            this.colAbsenceID.FieldName = "AbsenceID";
            this.colAbsenceID.Name = "colAbsenceID";
            this.colAbsenceID.Width = 76;
            // 
            // colHolidayYearID
            // 
            this.colHolidayYearID.Caption = "Holiday Year ID";
            this.colHolidayYearID.FieldName = "HolidayYearID";
            this.colHolidayYearID.Name = "colHolidayYearID";
            this.colHolidayYearID.Width = 95;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Employee ID";
            this.gridColumn1.FieldName = "EmployeeID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 81;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Employee Number";
            this.gridColumn3.FieldName = "EmployeeNumber";
            this.gridColumn3.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 107;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Employee Surname";
            this.gridColumn4.FieldName = "EmployeeSurname";
            this.gridColumn4.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 125;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Employee First Name";
            this.gridColumn5.FieldName = "EmployeeFirstname";
            this.gridColumn5.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 134;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Sector";
            this.gridColumn6.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.gridColumn6.FieldName = "Sector";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 3;
            this.gridColumn6.Width = 114;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Payroll Company";
            this.gridColumn7.FieldName = "PayrollCompany";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 4;
            this.gridColumn7.Width = 109;
            // 
            // colAbsenceType
            // 
            this.colAbsenceType.Caption = "Absence Type";
            this.colAbsenceType.FieldName = "AbsenceType";
            this.colAbsenceType.Name = "colAbsenceType";
            this.colAbsenceType.OptionsColumn.AllowEdit = false;
            this.colAbsenceType.OptionsColumn.AllowFocus = false;
            this.colAbsenceType.OptionsColumn.ReadOnly = true;
            this.colAbsenceType.Visible = true;
            this.colAbsenceType.VisibleIndex = 5;
            this.colAbsenceType.Width = 103;
            // 
            // colAbsenceReason
            // 
            this.colAbsenceReason.Caption = "Absence Reason";
            this.colAbsenceReason.FieldName = "AbsenceReason";
            this.colAbsenceReason.Name = "colAbsenceReason";
            this.colAbsenceReason.OptionsColumn.AllowEdit = false;
            this.colAbsenceReason.OptionsColumn.AllowFocus = false;
            this.colAbsenceReason.OptionsColumn.ReadOnly = true;
            this.colAbsenceReason.Visible = true;
            this.colAbsenceReason.VisibleIndex = 6;
            this.colAbsenceReason.Width = 114;
            // 
            // colAbsenceStart
            // 
            this.colAbsenceStart.Caption = "Absence Start";
            this.colAbsenceStart.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colAbsenceStart.FieldName = "AbsenceStart";
            this.colAbsenceStart.Name = "colAbsenceStart";
            this.colAbsenceStart.OptionsColumn.AllowEdit = false;
            this.colAbsenceStart.OptionsColumn.AllowFocus = false;
            this.colAbsenceStart.OptionsColumn.ReadOnly = true;
            this.colAbsenceStart.Visible = true;
            this.colAbsenceStart.VisibleIndex = 7;
            this.colAbsenceStart.Width = 102;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colAbsenceEnd
            // 
            this.colAbsenceEnd.Caption = "Absence End";
            this.colAbsenceEnd.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colAbsenceEnd.FieldName = "AbsenceEnd";
            this.colAbsenceEnd.Name = "colAbsenceEnd";
            this.colAbsenceEnd.OptionsColumn.AllowEdit = false;
            this.colAbsenceEnd.OptionsColumn.AllowFocus = false;
            this.colAbsenceEnd.OptionsColumn.ReadOnly = true;
            this.colAbsenceEnd.Visible = true;
            this.colAbsenceEnd.VisibleIndex = 8;
            this.colAbsenceEnd.Width = 94;
            // 
            // colMed3ExpiryDate
            // 
            this.colMed3ExpiryDate.Caption = "Med 3 Expiry Date";
            this.colMed3ExpiryDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colMed3ExpiryDate.FieldName = "Med3ExpiryDate";
            this.colMed3ExpiryDate.Name = "colMed3ExpiryDate";
            this.colMed3ExpiryDate.OptionsColumn.AllowEdit = false;
            this.colMed3ExpiryDate.OptionsColumn.AllowFocus = false;
            this.colMed3ExpiryDate.OptionsColumn.ReadOnly = true;
            this.colMed3ExpiryDate.Visible = true;
            this.colMed3ExpiryDate.VisibleIndex = 9;
            this.colMed3ExpiryDate.Width = 109;
            // 
            // colReportingPeriodAbsenceLength
            // 
            this.colReportingPeriodAbsenceLength.Caption = "Report Period Absence Duration";
            this.colReportingPeriodAbsenceLength.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colReportingPeriodAbsenceLength.FieldName = "ReportingPeriodAbsenceLength";
            this.colReportingPeriodAbsenceLength.Name = "colReportingPeriodAbsenceLength";
            this.colReportingPeriodAbsenceLength.OptionsColumn.AllowEdit = false;
            this.colReportingPeriodAbsenceLength.OptionsColumn.AllowFocus = false;
            this.colReportingPeriodAbsenceLength.OptionsColumn.ReadOnly = true;
            this.colReportingPeriodAbsenceLength.Visible = true;
            this.colReportingPeriodAbsenceLength.VisibleIndex = 11;
            this.colReportingPeriodAbsenceLength.Width = 175;
            // 
            // repositoryItemTextEditNumericDays
            // 
            this.repositoryItemTextEditNumericDays.AutoHeight = false;
            this.repositoryItemTextEditNumericDays.Mask.EditMask = "#####0.00 Days";
            this.repositoryItemTextEditNumericDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericDays.Name = "repositoryItemTextEditNumericDays";
            // 
            // colHolidayUnitDescriptor1
            // 
            this.colHolidayUnitDescriptor1.Caption = "Unit Descriptor";
            this.colHolidayUnitDescriptor1.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor1.Name = "colHolidayUnitDescriptor1";
            this.colHolidayUnitDescriptor1.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor1.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor1.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor1.Visible = true;
            this.colHolidayUnitDescriptor1.VisibleIndex = 10;
            this.colHolidayUnitDescriptor1.Width = 92;
            // 
            // colCSPUnitsPaid
            // 
            this.colCSPUnitsPaid.Caption = "CSP Paid";
            this.colCSPUnitsPaid.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colCSPUnitsPaid.FieldName = "CSPUnitsPaid";
            this.colCSPUnitsPaid.Name = "colCSPUnitsPaid";
            this.colCSPUnitsPaid.OptionsColumn.AllowEdit = false;
            this.colCSPUnitsPaid.OptionsColumn.AllowFocus = false;
            this.colCSPUnitsPaid.OptionsColumn.ReadOnly = true;
            this.colCSPUnitsPaid.Visible = true;
            this.colCSPUnitsPaid.VisibleIndex = 12;
            // 
            // colUnpaidUnits
            // 
            this.colUnpaidUnits.Caption = "Unpaid";
            this.colUnpaidUnits.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colUnpaidUnits.FieldName = "UnpaidUnits";
            this.colUnpaidUnits.Name = "colUnpaidUnits";
            this.colUnpaidUnits.OptionsColumn.AllowEdit = false;
            this.colUnpaidUnits.OptionsColumn.AllowFocus = false;
            this.colUnpaidUnits.OptionsColumn.ReadOnly = true;
            this.colUnpaidUnits.Visible = true;
            this.colUnpaidUnits.VisibleIndex = 15;
            // 
            // colTotalAbsenceLength
            // 
            this.colTotalAbsenceLength.Caption = "Total Absence Length";
            this.colTotalAbsenceLength.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colTotalAbsenceLength.FieldName = "TotalAbsenceLength";
            this.colTotalAbsenceLength.Name = "colTotalAbsenceLength";
            this.colTotalAbsenceLength.OptionsColumn.AllowEdit = false;
            this.colTotalAbsenceLength.OptionsColumn.AllowFocus = false;
            this.colTotalAbsenceLength.OptionsColumn.ReadOnly = true;
            this.colTotalAbsenceLength.Visible = true;
            this.colTotalAbsenceLength.VisibleIndex = 16;
            this.colTotalAbsenceLength.Width = 125;
            // 
            // colUnitsPaidHalfPay
            // 
            this.colUnitsPaidHalfPay.Caption = "Half Paid";
            this.colUnitsPaidHalfPay.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colUnitsPaidHalfPay.FieldName = "UnitsPaidHalfPay";
            this.colUnitsPaidHalfPay.Name = "colUnitsPaidHalfPay";
            this.colUnitsPaidHalfPay.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidHalfPay.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidHalfPay.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidHalfPay.Visible = true;
            this.colUnitsPaidHalfPay.VisibleIndex = 13;
            this.colUnitsPaidHalfPay.Width = 78;
            // 
            // colUnitsPaidOther
            // 
            this.colUnitsPaidOther.Caption = "Other Paid";
            this.colUnitsPaidOther.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colUnitsPaidOther.FieldName = "UnitsPaidOther";
            this.colUnitsPaidOther.Name = "colUnitsPaidOther";
            this.colUnitsPaidOther.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidOther.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidOther.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidOther.Visible = true;
            this.colUnitsPaidOther.VisibleIndex = 14;
            // 
            // repositoryItemTextEditNumericHours
            // 
            this.repositoryItemTextEditNumericHours.AutoHeight = false;
            this.repositoryItemTextEditNumericHours.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditNumericHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericHours.Name = "repositoryItemTextEditNumericHours";
            // 
            // repositoryItemTextEditNumericUnknown
            // 
            this.repositoryItemTextEditNumericUnknown.AutoHeight = false;
            this.repositoryItemTextEditNumericUnknown.Mask.EditMask = "#####0.00 ???";
            this.repositoryItemTextEditNumericUnknown.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericUnknown.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericUnknown.Name = "repositoryItemTextEditNumericUnknown";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl5);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1009, 489);
            this.xtraTabPage3.Text = "Leavers";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.spHR00211ReportPayrollLeaversBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEditDatetime3,
            this.repositoryItemTextEditCurrency2,
            this.repositoryItemTextEditNumeric2DP});
            this.gridControl5.Size = new System.Drawing.Size(1009, 489);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // spHR00211ReportPayrollLeaversBindingSource
            // 
            this.spHR00211ReportPayrollLeaversBindingSource.DataMember = "sp_HR_00211_Report_Payroll_Leavers";
            this.spHR00211ReportPayrollLeaversBindingSource.DataSource = this.dataSet_HR_Reporting;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.gridColumn24,
            this.gridColumn25,
            this.colLeaveDate,
            this.colLeaveReason,
            this.colHolidayBalance,
            this.colSchemeCostsToRecover,
            this.colTrainingCostsToRecover,
            this.colEquipmentCostsToRecover,
            this.colOtherCostsToRecover,
            this.colHolidayUnitDescriptor,
            this.colGroupStartDate,
            this.colJobCategory1});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsLayout.StoreFormatRules = true;
            this.gridView5.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn22, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn23, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            this.gridView5.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView5_CustomRowCellEdit);
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView5_ShowingEditor);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Employee ID";
            this.gridColumn20.FieldName = "EmployeeID";
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.OptionsColumn.AllowEdit = false;
            this.gridColumn20.OptionsColumn.AllowFocus = false;
            this.gridColumn20.OptionsColumn.ReadOnly = true;
            this.gridColumn20.Width = 81;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Employee Number";
            this.gridColumn21.FieldName = "EmployeeNumber";
            this.gridColumn21.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.OptionsColumn.AllowEdit = false;
            this.gridColumn21.OptionsColumn.AllowFocus = false;
            this.gridColumn21.OptionsColumn.ReadOnly = true;
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 0;
            this.gridColumn21.Width = 107;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Employee Surname";
            this.gridColumn22.FieldName = "EmployeeSurname";
            this.gridColumn22.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.OptionsColumn.AllowEdit = false;
            this.gridColumn22.OptionsColumn.AllowFocus = false;
            this.gridColumn22.OptionsColumn.ReadOnly = true;
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 1;
            this.gridColumn22.Width = 125;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Employee First Name";
            this.gridColumn23.FieldName = "EmployeeFirstname";
            this.gridColumn23.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.OptionsColumn.AllowEdit = false;
            this.gridColumn23.OptionsColumn.AllowFocus = false;
            this.gridColumn23.OptionsColumn.ReadOnly = true;
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 2;
            this.gridColumn23.Width = 134;
            // 
            // gridColumn24
            // 
            this.gridColumn24.Caption = "Sector";
            this.gridColumn24.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn24.FieldName = "Sector";
            this.gridColumn24.Name = "gridColumn24";
            this.gridColumn24.OptionsColumn.ReadOnly = true;
            this.gridColumn24.Visible = true;
            this.gridColumn24.VisibleIndex = 3;
            this.gridColumn24.Width = 114;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // gridColumn25
            // 
            this.gridColumn25.Caption = "Payroll Company";
            this.gridColumn25.FieldName = "PayrollCompany";
            this.gridColumn25.Name = "gridColumn25";
            this.gridColumn25.OptionsColumn.AllowEdit = false;
            this.gridColumn25.OptionsColumn.AllowFocus = false;
            this.gridColumn25.OptionsColumn.ReadOnly = true;
            this.gridColumn25.Visible = true;
            this.gridColumn25.VisibleIndex = 4;
            this.gridColumn25.Width = 109;
            // 
            // colLeaveDate
            // 
            this.colLeaveDate.Caption = "Leave Date";
            this.colLeaveDate.ColumnEdit = this.repositoryItemTextEditDatetime3;
            this.colLeaveDate.FieldName = "LeaveDate";
            this.colLeaveDate.Name = "colLeaveDate";
            this.colLeaveDate.OptionsColumn.AllowEdit = false;
            this.colLeaveDate.OptionsColumn.AllowFocus = false;
            this.colLeaveDate.OptionsColumn.ReadOnly = true;
            this.colLeaveDate.Visible = true;
            this.colLeaveDate.VisibleIndex = 7;
            // 
            // repositoryItemTextEditDatetime3
            // 
            this.repositoryItemTextEditDatetime3.AutoHeight = false;
            this.repositoryItemTextEditDatetime3.Mask.EditMask = "d";
            this.repositoryItemTextEditDatetime3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDatetime3.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDatetime3.Name = "repositoryItemTextEditDatetime3";
            // 
            // colLeaveReason
            // 
            this.colLeaveReason.Caption = "Reason";
            this.colLeaveReason.FieldName = "LeaveReason";
            this.colLeaveReason.Name = "colLeaveReason";
            this.colLeaveReason.OptionsColumn.AllowEdit = false;
            this.colLeaveReason.OptionsColumn.AllowFocus = false;
            this.colLeaveReason.OptionsColumn.ReadOnly = true;
            this.colLeaveReason.Visible = true;
            this.colLeaveReason.VisibleIndex = 8;
            this.colLeaveReason.Width = 124;
            // 
            // colHolidayBalance
            // 
            this.colHolidayBalance.Caption = "Holiday Balance + / -";
            this.colHolidayBalance.ColumnEdit = this.repositoryItemTextEditNumeric2DP;
            this.colHolidayBalance.FieldName = "HolidayBalance";
            this.colHolidayBalance.Name = "colHolidayBalance";
            this.colHolidayBalance.OptionsColumn.AllowEdit = false;
            this.colHolidayBalance.OptionsColumn.AllowFocus = false;
            this.colHolidayBalance.OptionsColumn.ReadOnly = true;
            this.colHolidayBalance.Visible = true;
            this.colHolidayBalance.VisibleIndex = 9;
            this.colHolidayBalance.Width = 121;
            // 
            // repositoryItemTextEditNumeric2DP
            // 
            this.repositoryItemTextEditNumeric2DP.AutoHeight = false;
            this.repositoryItemTextEditNumeric2DP.Mask.EditMask = "#####0.00";
            this.repositoryItemTextEditNumeric2DP.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumeric2DP.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumeric2DP.Name = "repositoryItemTextEditNumeric2DP";
            // 
            // colSchemeCostsToRecover
            // 
            this.colSchemeCostsToRecover.Caption = "Bike Scheme Deductions";
            this.colSchemeCostsToRecover.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colSchemeCostsToRecover.FieldName = "SchemeCostsToRecover";
            this.colSchemeCostsToRecover.Name = "colSchemeCostsToRecover";
            this.colSchemeCostsToRecover.OptionsColumn.AllowEdit = false;
            this.colSchemeCostsToRecover.OptionsColumn.AllowFocus = false;
            this.colSchemeCostsToRecover.OptionsColumn.ReadOnly = true;
            this.colSchemeCostsToRecover.Visible = true;
            this.colSchemeCostsToRecover.VisibleIndex = 11;
            this.colSchemeCostsToRecover.Width = 136;
            // 
            // repositoryItemTextEditCurrency2
            // 
            this.repositoryItemTextEditCurrency2.AutoHeight = false;
            this.repositoryItemTextEditCurrency2.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency2.Name = "repositoryItemTextEditCurrency2";
            // 
            // colTrainingCostsToRecover
            // 
            this.colTrainingCostsToRecover.Caption = "Training Deductions";
            this.colTrainingCostsToRecover.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colTrainingCostsToRecover.FieldName = "TrainingCostsToRecover";
            this.colTrainingCostsToRecover.Name = "colTrainingCostsToRecover";
            this.colTrainingCostsToRecover.OptionsColumn.AllowEdit = false;
            this.colTrainingCostsToRecover.OptionsColumn.AllowFocus = false;
            this.colTrainingCostsToRecover.OptionsColumn.ReadOnly = true;
            this.colTrainingCostsToRecover.Visible = true;
            this.colTrainingCostsToRecover.VisibleIndex = 12;
            this.colTrainingCostsToRecover.Width = 115;
            // 
            // colEquipmentCostsToRecover
            // 
            this.colEquipmentCostsToRecover.Caption = "Equipment Deductions";
            this.colEquipmentCostsToRecover.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colEquipmentCostsToRecover.FieldName = "EquipmentCostsToRecover";
            this.colEquipmentCostsToRecover.Name = "colEquipmentCostsToRecover";
            this.colEquipmentCostsToRecover.OptionsColumn.AllowEdit = false;
            this.colEquipmentCostsToRecover.OptionsColumn.AllowFocus = false;
            this.colEquipmentCostsToRecover.OptionsColumn.ReadOnly = true;
            this.colEquipmentCostsToRecover.Visible = true;
            this.colEquipmentCostsToRecover.VisibleIndex = 13;
            this.colEquipmentCostsToRecover.Width = 127;
            // 
            // colOtherCostsToRecover
            // 
            this.colOtherCostsToRecover.Caption = "Other Deductions";
            this.colOtherCostsToRecover.ColumnEdit = this.repositoryItemTextEditCurrency2;
            this.colOtherCostsToRecover.FieldName = "OtherCostsToRecover";
            this.colOtherCostsToRecover.Name = "colOtherCostsToRecover";
            this.colOtherCostsToRecover.OptionsColumn.AllowEdit = false;
            this.colOtherCostsToRecover.OptionsColumn.AllowFocus = false;
            this.colOtherCostsToRecover.OptionsColumn.ReadOnly = true;
            this.colOtherCostsToRecover.Visible = true;
            this.colOtherCostsToRecover.VisibleIndex = 14;
            this.colOtherCostsToRecover.Width = 105;
            // 
            // colHolidayUnitDescriptor
            // 
            this.colHolidayUnitDescriptor.Caption = "Holiday Unit Descriptor";
            this.colHolidayUnitDescriptor.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor.Name = "colHolidayUnitDescriptor";
            this.colHolidayUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor.Visible = true;
            this.colHolidayUnitDescriptor.VisibleIndex = 10;
            this.colHolidayUnitDescriptor.Width = 130;
            // 
            // colGroupStartDate
            // 
            this.colGroupStartDate.Caption = "Group Start Date";
            this.colGroupStartDate.ColumnEdit = this.repositoryItemTextEditDatetime3;
            this.colGroupStartDate.FieldName = "GroupStartDate";
            this.colGroupStartDate.Name = "colGroupStartDate";
            this.colGroupStartDate.OptionsColumn.AllowEdit = false;
            this.colGroupStartDate.OptionsColumn.AllowFocus = false;
            this.colGroupStartDate.OptionsColumn.ReadOnly = true;
            this.colGroupStartDate.Visible = true;
            this.colGroupStartDate.VisibleIndex = 6;
            this.colGroupStartDate.Width = 101;
            // 
            // colJobCategory1
            // 
            this.colJobCategory1.Caption = "Job Type";
            this.colJobCategory1.FieldName = "JobCategory";
            this.colJobCategory1.Name = "colJobCategory1";
            this.colJobCategory1.OptionsColumn.AllowEdit = false;
            this.colJobCategory1.OptionsColumn.AllowFocus = false;
            this.colJobCategory1.OptionsColumn.ReadOnly = true;
            this.colJobCategory1.Visible = true;
            this.colJobCategory1.VisibleIndex = 5;
            this.colJobCategory1.Width = 101;
            // 
            // sp_HR_00210_Report_Payroll_StartersTableAdapter
            // 
            this.sp_HR_00210_Report_Payroll_StartersTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00211_Report_Payroll_LeaversTableAdapter
            // 
            this.sp_HR_00211_Report_Payroll_LeaversTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00212_Report_Payroll_AbsencesTableAdapter
            // 
            this.sp_HR_00212_Report_Payroll_AbsencesTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending3
            // 
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending3.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending3.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending3.GridControl = this.gridControl3;
            // 
            // xtraGridBlending5
            // 
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending5.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending5.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending5.GridControl = this.gridControl5;
            // 
            // frm_HR_Report_Payroll_Export_Absences
            // 
            this.ClientSize = new System.Drawing.Size(1014, 557);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Report_Payroll_Export_Absences";
            this.Text = "Payroll - Report Export  [Starters \\ Absences \\ Leavers]";
            this.Activated += new System.EventHandler(this.frm_HR_Report_Payroll_Export_Absences_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Report_Payroll_Export_Absences_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Report_Payroll_Export_Absences_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00210ReportPayrollStartersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Reporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).EndInit();
            this.popupContainerControlDepartments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).EndInit();
            this.popupContainerControlEmployees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFilter)).EndInit();
            this.popupContainerControlDateRangeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00212ReportPayrollAbsencesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericUnknown)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00211ReportPayrollLeaversBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDatetime3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumeric2DP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem beiDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveHoilidayYearOnly;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraBars.BarEditItem beiDepartment;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDepartment;
        private DevExpress.XtraBars.BarEditItem beiEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private System.Windows.Forms.BindingSource spHR00114DepartmentFilterListBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter sp_HR_00114_Department_Filter_ListTableAdapter;
        private System.Windows.Forms.BindingSource spHR00115EmployeesByDeptFilterListBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditActiveOnly;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDepartments;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DevExpress.XtraEditors.SimpleButton btnDepartmentFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlEmployees;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.SimpleButton btnEmployeeFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRangeFilter;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private System.Windows.Forms.BindingSource spHR00210ReportPayrollStartersBindingSource;
        private DataSet_HR_Reporting dataSet_HR_Reporting;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colSector;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCentre;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colNINumber;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSalary;
        private DevExpress.XtraGrid.Columns.GridColumn colHours;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkingPattern;
        private DevExpress.XtraGrid.Columns.GridColumn colBankName;
        private DevExpress.XtraGrid.Columns.GridColumn colSortCode;
        private DevExpress.XtraGrid.Columns.GridColumn colAccountNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPayslipEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colPayslipPassword;
        private DataSet_HR_ReportingTableAdapters.sp_HR_00210_Report_Payroll_StartersTableAdapter sp_HR_00210_Report_Payroll_StartersTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHours;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericHours;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDatetime3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumeric2DP;
        private System.Windows.Forms.BindingSource spHR00211ReportPayrollLeaversBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLeaveDate;
        private DevExpress.XtraGrid.Columns.GridColumn colLeaveReason;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayBalance;
        private DevExpress.XtraGrid.Columns.GridColumn colSchemeCostsToRecover;
        private DevExpress.XtraGrid.Columns.GridColumn colTrainingCostsToRecover;
        private DevExpress.XtraGrid.Columns.GridColumn colEquipmentCostsToRecover;
        private DevExpress.XtraGrid.Columns.GridColumn colOtherCostsToRecover;
        private DataSet_HR_ReportingTableAdapters.sp_HR_00211_Report_Payroll_LeaversTableAdapter sp_HR_00211_Report_Payroll_LeaversTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor;
        private System.Windows.Forms.BindingSource spHR00212ReportPayrollAbsencesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceID;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceType;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceReason;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceStart;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colMed3ExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReportingPeriodAbsenceLength;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor1;
        private DevExpress.XtraGrid.Columns.GridColumn colCSPUnitsPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colUnpaidUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAbsenceLength;
        private DataSet_HR_ReportingTableAdapters.sp_HR_00212_Report_Payroll_AbsencesTableAdapter sp_HR_00212_Report_Payroll_AbsencesTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending3;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending5;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericUnknown;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidHalfPay;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidOther;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCategory1;
    }
}

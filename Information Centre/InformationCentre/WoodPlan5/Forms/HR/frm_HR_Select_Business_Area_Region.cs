using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_HR_Select_Business_Area_Region : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public string strPassedInBusinessAreaFilter = "";
        public int intOriginalRegionID = 0;
        public int intOriginalBusinessAreaID = 0;
        public int intSelectedRegionID = 0;
        public int intSelectedBusinessAreaID = 0;
        public string strOriginalRegionIDs = "";
        public string strSelectedRegionIDs = "";
        public string strSelectedRegionNames = "";
        public string strSelectedBusinessAreaIDs = "";
        public string strSelectedBusinessAreaNames = "";

        BaseObjects.GridCheckMarksSelection selection1;

        #endregion

        public frm_HR_Select_Business_Area_Region()
        {
            InitializeComponent();
        }

        private void frm_HR_Select_Business_Area_Region_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 990116;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            sp_HR_00012_Regions_For_Business_AreaTableAdapter.Connection.ConnectionString = strConnectionString;
             GridView view = (GridView)gridControl1.MainView;
            //view.Appearance.FocusedRow.BackColor = Color.FromArgb(60, 0, 0, 240);

            LoadData();
            gridControl1.ForceInitialize();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;

                Array arrayRecords = strOriginalRegionIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["RegionID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalRegionID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["RegionID"], intOriginalRegionID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp_HR_00012_Regions_For_Business_AreaTableAdapter.Fill(dataSet_HR_Core.sp_HR_00012_Regions_For_Business_Area, strPassedInBusinessAreaFilter);
            view.ExpandAllGroups();
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Regions Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedRegionIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Region", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedRegionID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Region", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (_Mode != "single")
            {
                strSelectedRegionIDs = "";    // Reset any prior values first //
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedRegionIDs += Convert.ToString(view.GetRowCellValue(i, "RegionID")) + ",";
                        strSelectedBusinessAreaIDs += Convert.ToString(view.GetRowCellValue(i, "BusinessAreaID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedRegionNames = Convert.ToString(view.GetRowCellValue(i, "RegionName"));
                            strSelectedBusinessAreaNames = Convert.ToString(view.GetRowCellValue(i, "BusinessAreaName"));
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedRegionNames += ", " + Convert.ToString(view.GetRowCellValue(i, "RegionName"));
                            strSelectedBusinessAreaNames += ", " + Convert.ToString(view.GetRowCellValue(i, "BusinessAreaName"));
                        }
                        intCount++;
                    }
                }
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    intSelectedRegionID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "RegionID"));
                    strSelectedRegionNames = view.GetRowCellValue(view.FocusedRowHandle, "RegionName").ToString();
                    intSelectedBusinessAreaID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BusinessAreaID"));
                    strSelectedBusinessAreaNames = view.GetRowCellValue(view.FocusedRowHandle, "BusinessAreaName").ToString();
               }
            }
        }

    }
}


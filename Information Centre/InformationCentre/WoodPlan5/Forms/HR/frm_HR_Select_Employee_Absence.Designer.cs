namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Absence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Select_Employee_Absence));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActiveHolidayYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00187SelectAbsenceHolidayYearsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeHolidayYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayYearStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHolidayYearEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBasicHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditUnknownHoliday = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBankHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchasedHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoursPerHolidayDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMustTakeCertainHolidays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colHolidayYearDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurnameForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceAuthorisedTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceUnauthorisedTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDaysHoliday = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditHoursHoliday = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.spHR00188SelectAbsenceHolidayYearAbsencesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAbsenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceSubTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2dp = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHolidayUnitDescriptor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApprovedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelledDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeclaredDisability = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colPayCategoryId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsWorkRelated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurnameForename1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeForename1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApprovedByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelledByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEWCDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedReturnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHalfDayEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHalfDayStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdditionalDaysPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00187_Select_Absence_Holiday_YearsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00187_Select_Absence_Holiday_YearsTableAdapter();
            this.sp_HR_00188_Select_Absence_Holiday_Year_AbsencesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00188_Select_Absence_Holiday_Year_AbsencesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00187SelectAbsenceHolidayYearsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditUnknownHoliday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDaysHoliday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHoursHoliday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00188SelectAbsenceHolidayYearAbsencesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2dp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(678, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 475);
            this.barDockControlBottom.Size = new System.Drawing.Size(678, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 475);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(678, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 475);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActiveHolidayYear
            // 
            this.colActiveHolidayYear.Caption = "Current Holiday Year";
            this.colActiveHolidayYear.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActiveHolidayYear.FieldName = "ActiveHolidayYear";
            this.colActiveHolidayYear.Name = "colActiveHolidayYear";
            this.colActiveHolidayYear.OptionsColumn.AllowEdit = false;
            this.colActiveHolidayYear.OptionsColumn.AllowFocus = false;
            this.colActiveHolidayYear.OptionsColumn.ReadOnly = true;
            this.colActiveHolidayYear.Visible = true;
            this.colActiveHolidayYear.VisibleIndex = 5;
            this.colActiveHolidayYear.Width = 121;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Employee Holiday Years";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Employee Absences";
            this.splitContainerControl1.Size = new System.Drawing.Size(678, 440);
            this.splitContainerControl1.SplitterPosition = 195;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(653, 192);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.AllowDrop = true;
            this.gridControl1.DataSource = this.spHR00187SelectAbsenceHolidayYearsBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditUnknownHoliday,
            this.repositoryItemTextEditDaysHoliday,
            this.repositoryItemTextEditHoursHoliday});
            this.gridControl1.Size = new System.Drawing.Size(653, 192);
            this.gridControl1.TabIndex = 5;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00187SelectAbsenceHolidayYearsBindingSource
            // 
            this.spHR00187SelectAbsenceHolidayYearsBindingSource.DataMember = "sp_HR_00187_Select_Absence_Holiday_Years";
            this.spHR00187SelectAbsenceHolidayYearsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeHolidayYearID,
            this.colEmployeeContractID,
            this.colHolidayYearStartDate,
            this.colHolidayYearEndDate,
            this.colActualStartDate,
            this.colBasicHolidayEntitlement,
            this.colBankHolidayEntitlement,
            this.colPurchasedHolidayEntitlement,
            this.colHolidayUnitDescriptorID,
            this.colHoursPerHolidayDay,
            this.colMustTakeCertainHolidays,
            this.colRemarks,
            this.colHolidayYearDescription,
            this.colEmployeeSurnameForename,
            this.colEmployeeSurname,
            this.colEmployeeForename,
            this.colEmployeeNumber,
            this.colActiveHolidayYear,
            this.colHolidayUnitDescriptor,
            this.colHolidaysTaken,
            this.colBankHolidaysTaken,
            this.colAbsenceAuthorisedTaken,
            this.colAbsenceUnauthorisedTaken,
            this.colTotalHolidayEntitlement,
            this.colHolidaysRemaining,
            this.colBankHolidaysRemaining,
            this.colTotalHolidaysTaken,
            this.colTotalHolidaysRemaining});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActiveHolidayYear;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeForename, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colEmployeeHolidayYearID
            // 
            this.colEmployeeHolidayYearID.Caption = "Holiday Year ID";
            this.colEmployeeHolidayYearID.FieldName = "EmployeeHolidayYearID";
            this.colEmployeeHolidayYearID.Name = "colEmployeeHolidayYearID";
            this.colEmployeeHolidayYearID.OptionsColumn.AllowEdit = false;
            this.colEmployeeHolidayYearID.OptionsColumn.AllowFocus = false;
            this.colEmployeeHolidayYearID.OptionsColumn.ReadOnly = true;
            this.colEmployeeHolidayYearID.Width = 95;
            // 
            // colEmployeeContractID
            // 
            this.colEmployeeContractID.Caption = "Employee ID";
            this.colEmployeeContractID.FieldName = "EmployeeID";
            this.colEmployeeContractID.Name = "colEmployeeContractID";
            this.colEmployeeContractID.OptionsColumn.AllowEdit = false;
            this.colEmployeeContractID.OptionsColumn.AllowFocus = false;
            this.colEmployeeContractID.OptionsColumn.ReadOnly = true;
            this.colEmployeeContractID.Width = 77;
            // 
            // colHolidayYearStartDate
            // 
            this.colHolidayYearStartDate.Caption = "Year Start Date";
            this.colHolidayYearStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colHolidayYearStartDate.FieldName = "HolidayYearStartDate";
            this.colHolidayYearStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHolidayYearStartDate.Name = "colHolidayYearStartDate";
            this.colHolidayYearStartDate.OptionsColumn.AllowEdit = false;
            this.colHolidayYearStartDate.OptionsColumn.AllowFocus = false;
            this.colHolidayYearStartDate.OptionsColumn.ReadOnly = true;
            this.colHolidayYearStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHolidayYearStartDate.Visible = true;
            this.colHolidayYearStartDate.VisibleIndex = 3;
            this.colHolidayYearStartDate.Width = 96;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colHolidayYearEndDate
            // 
            this.colHolidayYearEndDate.Caption = "Year End Date";
            this.colHolidayYearEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colHolidayYearEndDate.FieldName = "HolidayYearEndDate";
            this.colHolidayYearEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHolidayYearEndDate.Name = "colHolidayYearEndDate";
            this.colHolidayYearEndDate.OptionsColumn.AllowEdit = false;
            this.colHolidayYearEndDate.OptionsColumn.AllowFocus = false;
            this.colHolidayYearEndDate.OptionsColumn.ReadOnly = true;
            this.colHolidayYearEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHolidayYearEndDate.Visible = true;
            this.colHolidayYearEndDate.VisibleIndex = 4;
            this.colHolidayYearEndDate.Width = 90;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start Date";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 6;
            this.colActualStartDate.Width = 104;
            // 
            // colBasicHolidayEntitlement
            // 
            this.colBasicHolidayEntitlement.Caption = "Holiday Entitlement";
            this.colBasicHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colBasicHolidayEntitlement.FieldName = "BasicHolidayEntitlement";
            this.colBasicHolidayEntitlement.Name = "colBasicHolidayEntitlement";
            this.colBasicHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colBasicHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colBasicHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colBasicHolidayEntitlement.Visible = true;
            this.colBasicHolidayEntitlement.VisibleIndex = 7;
            this.colBasicHolidayEntitlement.Width = 113;
            // 
            // repositoryItemTextEditUnknownHoliday
            // 
            this.repositoryItemTextEditUnknownHoliday.AutoHeight = false;
            this.repositoryItemTextEditUnknownHoliday.Mask.EditMask = "#####0.00";
            this.repositoryItemTextEditUnknownHoliday.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditUnknownHoliday.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditUnknownHoliday.Name = "repositoryItemTextEditUnknownHoliday";
            // 
            // colBankHolidayEntitlement
            // 
            this.colBankHolidayEntitlement.Caption = "Bank Holiday Entitlement";
            this.colBankHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colBankHolidayEntitlement.FieldName = "BankHolidayEntitlement";
            this.colBankHolidayEntitlement.Name = "colBankHolidayEntitlement";
            this.colBankHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colBankHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colBankHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colBankHolidayEntitlement.Visible = true;
            this.colBankHolidayEntitlement.VisibleIndex = 8;
            this.colBankHolidayEntitlement.Width = 139;
            // 
            // colPurchasedHolidayEntitlement
            // 
            this.colPurchasedHolidayEntitlement.Caption = "Purchased Holiday Entitlement";
            this.colPurchasedHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colPurchasedHolidayEntitlement.FieldName = "PurchasedHolidayEntitlement";
            this.colPurchasedHolidayEntitlement.Name = "colPurchasedHolidayEntitlement";
            this.colPurchasedHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colPurchasedHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colPurchasedHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colPurchasedHolidayEntitlement.Visible = true;
            this.colPurchasedHolidayEntitlement.VisibleIndex = 9;
            this.colPurchasedHolidayEntitlement.Width = 166;
            // 
            // colHolidayUnitDescriptorID
            // 
            this.colHolidayUnitDescriptorID.Caption = "Holiday Unit Descriptor ID";
            this.colHolidayUnitDescriptorID.FieldName = "HolidayUnitDescriptorID";
            this.colHolidayUnitDescriptorID.Name = "colHolidayUnitDescriptorID";
            this.colHolidayUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptorID.Width = 144;
            // 
            // colHoursPerHolidayDay
            // 
            this.colHoursPerHolidayDay.Caption = "Hours Per Holiday Day";
            this.colHoursPerHolidayDay.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colHoursPerHolidayDay.FieldName = "HoursPerHolidayDay";
            this.colHoursPerHolidayDay.Name = "colHoursPerHolidayDay";
            this.colHoursPerHolidayDay.OptionsColumn.AllowEdit = false;
            this.colHoursPerHolidayDay.OptionsColumn.AllowFocus = false;
            this.colHoursPerHolidayDay.OptionsColumn.ReadOnly = true;
            this.colHoursPerHolidayDay.Visible = true;
            this.colHoursPerHolidayDay.VisibleIndex = 19;
            this.colHoursPerHolidayDay.Width = 128;
            // 
            // colMustTakeCertainHolidays
            // 
            this.colMustTakeCertainHolidays.Caption = "Must Take Default Holidays";
            this.colMustTakeCertainHolidays.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colMustTakeCertainHolidays.FieldName = "MustTakeCertainHolidays";
            this.colMustTakeCertainHolidays.Name = "colMustTakeCertainHolidays";
            this.colMustTakeCertainHolidays.OptionsColumn.AllowEdit = false;
            this.colMustTakeCertainHolidays.OptionsColumn.AllowFocus = false;
            this.colMustTakeCertainHolidays.OptionsColumn.ReadOnly = true;
            this.colMustTakeCertainHolidays.Visible = true;
            this.colMustTakeCertainHolidays.VisibleIndex = 20;
            this.colMustTakeCertainHolidays.Width = 151;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 21;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colHolidayYearDescription
            // 
            this.colHolidayYearDescription.Caption = "Holiday Year Description";
            this.colHolidayYearDescription.FieldName = "HolidayYearDescription";
            this.colHolidayYearDescription.Name = "colHolidayYearDescription";
            this.colHolidayYearDescription.OptionsColumn.AllowEdit = false;
            this.colHolidayYearDescription.OptionsColumn.AllowFocus = false;
            this.colHolidayYearDescription.OptionsColumn.ReadOnly = true;
            this.colHolidayYearDescription.Width = 137;
            // 
            // colEmployeeSurnameForename
            // 
            this.colEmployeeSurnameForename.Caption = "Employee Details";
            this.colEmployeeSurnameForename.FieldName = "EmployeeSurnameForename";
            this.colEmployeeSurnameForename.Name = "colEmployeeSurnameForename";
            this.colEmployeeSurnameForename.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurnameForename.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurnameForename.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurnameForename.Width = 102;
            // 
            // colEmployeeSurname
            // 
            this.colEmployeeSurname.Caption = "Employee Surname";
            this.colEmployeeSurname.FieldName = "EmployeeSurname";
            this.colEmployeeSurname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeSurname.Name = "colEmployeeSurname";
            this.colEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname.Visible = true;
            this.colEmployeeSurname.VisibleIndex = 0;
            this.colEmployeeSurname.Width = 125;
            // 
            // colEmployeeForename
            // 
            this.colEmployeeForename.Caption = "Employee Forename";
            this.colEmployeeForename.FieldName = "EmployeeForename";
            this.colEmployeeForename.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeForename.Name = "colEmployeeForename";
            this.colEmployeeForename.OptionsColumn.AllowEdit = false;
            this.colEmployeeForename.OptionsColumn.AllowFocus = false;
            this.colEmployeeForename.OptionsColumn.ReadOnly = true;
            this.colEmployeeForename.Visible = true;
            this.colEmployeeForename.VisibleIndex = 1;
            this.colEmployeeForename.Width = 131;
            // 
            // colEmployeeNumber
            // 
            this.colEmployeeNumber.Caption = "Employee #";
            this.colEmployeeNumber.FieldName = "EmployeeNumber";
            this.colEmployeeNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeNumber.Name = "colEmployeeNumber";
            this.colEmployeeNumber.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber.Visible = true;
            this.colEmployeeNumber.VisibleIndex = 2;
            this.colEmployeeNumber.Width = 78;
            // 
            // colHolidayUnitDescriptor
            // 
            this.colHolidayUnitDescriptor.Caption = "Holiday Unit Descriptor";
            this.colHolidayUnitDescriptor.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor.Name = "colHolidayUnitDescriptor";
            this.colHolidayUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor.Width = 130;
            // 
            // colHolidaysTaken
            // 
            this.colHolidaysTaken.Caption = "Holidays Used";
            this.colHolidaysTaken.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colHolidaysTaken.FieldName = "HolidaysTaken";
            this.colHolidaysTaken.Name = "colHolidaysTaken";
            this.colHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colHolidaysTaken.Visible = true;
            this.colHolidaysTaken.VisibleIndex = 11;
            this.colHolidaysTaken.Width = 88;
            // 
            // colBankHolidaysTaken
            // 
            this.colBankHolidaysTaken.Caption = "Bank Holidays Used";
            this.colBankHolidaysTaken.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colBankHolidaysTaken.FieldName = "BankHolidaysTaken";
            this.colBankHolidaysTaken.Name = "colBankHolidaysTaken";
            this.colBankHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colBankHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colBankHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colBankHolidaysTaken.Visible = true;
            this.colBankHolidaysTaken.VisibleIndex = 12;
            this.colBankHolidaysTaken.Width = 114;
            // 
            // colAbsenceAuthorisedTaken
            // 
            this.colAbsenceAuthorisedTaken.Caption = "Authorised Holidays Used";
            this.colAbsenceAuthorisedTaken.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colAbsenceAuthorisedTaken.FieldName = "AbsenceAuthorisedTaken";
            this.colAbsenceAuthorisedTaken.Name = "colAbsenceAuthorisedTaken";
            this.colAbsenceAuthorisedTaken.OptionsColumn.AllowEdit = false;
            this.colAbsenceAuthorisedTaken.OptionsColumn.AllowFocus = false;
            this.colAbsenceAuthorisedTaken.OptionsColumn.ReadOnly = true;
            this.colAbsenceAuthorisedTaken.Visible = true;
            this.colAbsenceAuthorisedTaken.VisibleIndex = 13;
            this.colAbsenceAuthorisedTaken.Width = 143;
            // 
            // colAbsenceUnauthorisedTaken
            // 
            this.colAbsenceUnauthorisedTaken.Caption = "Unauthorised Holidays Used";
            this.colAbsenceUnauthorisedTaken.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colAbsenceUnauthorisedTaken.FieldName = "AbsenceUnauthorisedTaken";
            this.colAbsenceUnauthorisedTaken.Name = "colAbsenceUnauthorisedTaken";
            this.colAbsenceUnauthorisedTaken.OptionsColumn.AllowEdit = false;
            this.colAbsenceUnauthorisedTaken.OptionsColumn.AllowFocus = false;
            this.colAbsenceUnauthorisedTaken.OptionsColumn.ReadOnly = true;
            this.colAbsenceUnauthorisedTaken.Visible = true;
            this.colAbsenceUnauthorisedTaken.VisibleIndex = 14;
            this.colAbsenceUnauthorisedTaken.Width = 155;
            // 
            // colTotalHolidayEntitlement
            // 
            this.colTotalHolidayEntitlement.Caption = "Total Holiday Entitlement";
            this.colTotalHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colTotalHolidayEntitlement.FieldName = "TotalHolidayEntitlement";
            this.colTotalHolidayEntitlement.Name = "colTotalHolidayEntitlement";
            this.colTotalHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colTotalHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colTotalHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colTotalHolidayEntitlement.Visible = true;
            this.colTotalHolidayEntitlement.VisibleIndex = 10;
            this.colTotalHolidayEntitlement.Width = 140;
            // 
            // colHolidaysRemaining
            // 
            this.colHolidaysRemaining.Caption = "Holidays Left";
            this.colHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colHolidaysRemaining.FieldName = "HolidaysRemaining";
            this.colHolidaysRemaining.Name = "colHolidaysRemaining";
            this.colHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colHolidaysRemaining.Visible = true;
            this.colHolidaysRemaining.VisibleIndex = 16;
            this.colHolidaysRemaining.Width = 83;
            // 
            // colBankHolidaysRemaining
            // 
            this.colBankHolidaysRemaining.Caption = "Bank Holidays Left";
            this.colBankHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colBankHolidaysRemaining.FieldName = "BankHolidaysRemaining";
            this.colBankHolidaysRemaining.Name = "colBankHolidaysRemaining";
            this.colBankHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colBankHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colBankHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colBankHolidaysRemaining.Visible = true;
            this.colBankHolidaysRemaining.VisibleIndex = 17;
            this.colBankHolidaysRemaining.Width = 109;
            // 
            // colTotalHolidaysTaken
            // 
            this.colTotalHolidaysTaken.Caption = "Total Holidays Used";
            this.colTotalHolidaysTaken.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colTotalHolidaysTaken.FieldName = "TotalHolidaysTaken";
            this.colTotalHolidaysTaken.Name = "colTotalHolidaysTaken";
            this.colTotalHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colTotalHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colTotalHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colTotalHolidaysTaken.Visible = true;
            this.colTotalHolidaysTaken.VisibleIndex = 15;
            this.colTotalHolidaysTaken.Width = 115;
            // 
            // colTotalHolidaysRemaining
            // 
            this.colTotalHolidaysRemaining.Caption = "Total Holidays Left";
            this.colTotalHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditUnknownHoliday;
            this.colTotalHolidaysRemaining.FieldName = "TotalHolidaysRemaining";
            this.colTotalHolidaysRemaining.Name = "colTotalHolidaysRemaining";
            this.colTotalHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colTotalHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colTotalHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colTotalHolidaysRemaining.Visible = true;
            this.colTotalHolidaysRemaining.VisibleIndex = 18;
            this.colTotalHolidaysRemaining.Width = 110;
            // 
            // repositoryItemTextEditDaysHoliday
            // 
            this.repositoryItemTextEditDaysHoliday.AutoHeight = false;
            this.repositoryItemTextEditDaysHoliday.Mask.EditMask = "#####0.00 Days";
            this.repositoryItemTextEditDaysHoliday.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditDaysHoliday.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDaysHoliday.Name = "repositoryItemTextEditDaysHoliday";
            // 
            // repositoryItemTextEditHoursHoliday
            // 
            this.repositoryItemTextEditHoursHoliday.AutoHeight = false;
            this.repositoryItemTextEditHoursHoliday.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHoursHoliday.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHoursHoliday.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHoursHoliday.Name = "repositoryItemTextEditHoursHoliday";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(653, 236);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.spHR00188SelectAbsenceHolidayYearAbsencesBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEdit2dp,
            this.repositoryItemCheckEdit2});
            this.gridControl2.Size = new System.Drawing.Size(653, 236);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00188SelectAbsenceHolidayYearAbsencesBindingSource
            // 
            this.spHR00188SelectAbsenceHolidayYearAbsencesBindingSource.DataMember = "sp_HR_00188_Select_Absence_Holiday_Year_Absences";
            this.spHR00188SelectAbsenceHolidayYearAbsencesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAbsenceID,
            this.gridColumn1,
            this.colRecordTypeId,
            this.colAbsenceTypeId,
            this.colAbsenceSubTypeId,
            this.colStartDate,
            this.colEndDate,
            this.colUnitsUsed,
            this.colHolidayUnitDescriptor1,
            this.colApprovedDate,
            this.colCancelledDate,
            this.colDeclaredDisability,
            this.colPayCategoryId,
            this.colIsWorkRelated,
            this.colComments4,
            this.colRecordType,
            this.colAbsenceType,
            this.colStatus,
            this.gridColumn4,
            this.colEmployeeSurnameForename1,
            this.colEmployeeSurname1,
            this.colEmployeeForename1,
            this.colEmployeeNumber3,
            this.colApprovedByPerson,
            this.colCancelledByPerson,
            this.colAbsenceSubType,
            this.gridColumn5,
            this.colContractNumber1,
            this.colEWCDate,
            this.colExpectedReturnDate,
            this.colHalfDayEnd,
            this.colHalfDayStart,
            this.colAdditionalDaysPaid});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.BestFitMaxRowCount = 10;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurnameForename1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView2_CustomDrawCell);
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colAbsenceID
            // 
            this.colAbsenceID.Caption = "Absence ID";
            this.colAbsenceID.FieldName = "AbsenceID";
            this.colAbsenceID.Name = "colAbsenceID";
            this.colAbsenceID.OptionsColumn.AllowEdit = false;
            this.colAbsenceID.OptionsColumn.AllowFocus = false;
            this.colAbsenceID.OptionsColumn.ReadOnly = true;
            this.colAbsenceID.Width = 76;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Holiday Year ID";
            this.gridColumn1.FieldName = "EmployeeHolidayYearId";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 95;
            // 
            // colRecordTypeId
            // 
            this.colRecordTypeId.Caption = "Record Type ID";
            this.colRecordTypeId.FieldName = "RecordTypeId";
            this.colRecordTypeId.Name = "colRecordTypeId";
            this.colRecordTypeId.OptionsColumn.AllowEdit = false;
            this.colRecordTypeId.OptionsColumn.AllowFocus = false;
            this.colRecordTypeId.OptionsColumn.ReadOnly = true;
            this.colRecordTypeId.Width = 96;
            // 
            // colAbsenceTypeId
            // 
            this.colAbsenceTypeId.Caption = "Absence Type ID";
            this.colAbsenceTypeId.FieldName = "AbsenceTypeId";
            this.colAbsenceTypeId.Name = "colAbsenceTypeId";
            this.colAbsenceTypeId.OptionsColumn.AllowEdit = false;
            this.colAbsenceTypeId.OptionsColumn.AllowFocus = false;
            this.colAbsenceTypeId.OptionsColumn.ReadOnly = true;
            this.colAbsenceTypeId.Width = 103;
            // 
            // colAbsenceSubTypeId
            // 
            this.colAbsenceSubTypeId.Caption = "Absence Sub-Type ID";
            this.colAbsenceSubTypeId.FieldName = "AbsenceSubTypeId";
            this.colAbsenceSubTypeId.Name = "colAbsenceSubTypeId";
            this.colAbsenceSubTypeId.OptionsColumn.AllowEdit = false;
            this.colAbsenceSubTypeId.OptionsColumn.AllowFocus = false;
            this.colAbsenceSubTypeId.OptionsColumn.ReadOnly = true;
            this.colAbsenceSubTypeId.Width = 125;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 0;
            this.colStartDate.Width = 110;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 1;
            this.colEndDate.Width = 88;
            // 
            // colUnitsUsed
            // 
            this.colUnitsUsed.Caption = "Units Used";
            this.colUnitsUsed.ColumnEdit = this.repositoryItemTextEdit2dp;
            this.colUnitsUsed.FieldName = "UnitsUsed";
            this.colUnitsUsed.Name = "colUnitsUsed";
            this.colUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colUnitsUsed.Visible = true;
            this.colUnitsUsed.VisibleIndex = 5;
            // 
            // repositoryItemTextEdit2dp
            // 
            this.repositoryItemTextEdit2dp.AutoHeight = false;
            this.repositoryItemTextEdit2dp.Mask.EditMask = "######0.00";
            this.repositoryItemTextEdit2dp.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2dp.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2dp.Name = "repositoryItemTextEdit2dp";
            // 
            // colHolidayUnitDescriptor1
            // 
            this.colHolidayUnitDescriptor1.Caption = "Unit Descriptor";
            this.colHolidayUnitDescriptor1.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor1.Name = "colHolidayUnitDescriptor1";
            this.colHolidayUnitDescriptor1.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor1.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor1.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor1.Visible = true;
            this.colHolidayUnitDescriptor1.VisibleIndex = 6;
            this.colHolidayUnitDescriptor1.Width = 92;
            // 
            // colApprovedDate
            // 
            this.colApprovedDate.Caption = "Approved Date";
            this.colApprovedDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colApprovedDate.FieldName = "ApprovedDate";
            this.colApprovedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colApprovedDate.Name = "colApprovedDate";
            this.colApprovedDate.OptionsColumn.AllowEdit = false;
            this.colApprovedDate.OptionsColumn.AllowFocus = false;
            this.colApprovedDate.OptionsColumn.ReadOnly = true;
            this.colApprovedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colApprovedDate.Visible = true;
            this.colApprovedDate.VisibleIndex = 13;
            this.colApprovedDate.Width = 94;
            // 
            // colCancelledDate
            // 
            this.colCancelledDate.Caption = "Cancelled Date";
            this.colCancelledDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colCancelledDate.FieldName = "CancelledDate";
            this.colCancelledDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCancelledDate.Name = "colCancelledDate";
            this.colCancelledDate.OptionsColumn.AllowEdit = false;
            this.colCancelledDate.OptionsColumn.AllowFocus = false;
            this.colCancelledDate.OptionsColumn.ReadOnly = true;
            this.colCancelledDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCancelledDate.Visible = true;
            this.colCancelledDate.VisibleIndex = 15;
            this.colCancelledDate.Width = 93;
            // 
            // colDeclaredDisability
            // 
            this.colDeclaredDisability.Caption = "Declared Disability";
            this.colDeclaredDisability.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDeclaredDisability.FieldName = "DeclaredDisability";
            this.colDeclaredDisability.Name = "colDeclaredDisability";
            this.colDeclaredDisability.OptionsColumn.AllowEdit = false;
            this.colDeclaredDisability.OptionsColumn.AllowFocus = false;
            this.colDeclaredDisability.OptionsColumn.ReadOnly = true;
            this.colDeclaredDisability.Visible = true;
            this.colDeclaredDisability.VisibleIndex = 18;
            this.colDeclaredDisability.Width = 108;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colPayCategoryId
            // 
            this.colPayCategoryId.Caption = "Pay Category ID";
            this.colPayCategoryId.FieldName = "PayCategoryId";
            this.colPayCategoryId.Name = "colPayCategoryId";
            this.colPayCategoryId.OptionsColumn.AllowEdit = false;
            this.colPayCategoryId.OptionsColumn.AllowFocus = false;
            this.colPayCategoryId.OptionsColumn.ReadOnly = true;
            this.colPayCategoryId.Width = 101;
            // 
            // colIsWorkRelated
            // 
            this.colIsWorkRelated.Caption = "Work Related";
            this.colIsWorkRelated.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsWorkRelated.FieldName = "IsWorkRelated";
            this.colIsWorkRelated.Name = "colIsWorkRelated";
            this.colIsWorkRelated.OptionsColumn.AllowEdit = false;
            this.colIsWorkRelated.OptionsColumn.AllowFocus = false;
            this.colIsWorkRelated.OptionsColumn.ReadOnly = true;
            this.colIsWorkRelated.Visible = true;
            this.colIsWorkRelated.VisibleIndex = 17;
            this.colIsWorkRelated.Width = 86;
            // 
            // colComments4
            // 
            this.colComments4.Caption = "Remarks";
            this.colComments4.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colComments4.FieldName = "Comments";
            this.colComments4.Name = "colComments4";
            this.colComments4.OptionsColumn.ReadOnly = true;
            this.colComments4.Visible = true;
            this.colComments4.VisibleIndex = 19;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Visible = true;
            this.colRecordType.VisibleIndex = 2;
            this.colRecordType.Width = 94;
            // 
            // colAbsenceType
            // 
            this.colAbsenceType.Caption = "Absence Type";
            this.colAbsenceType.FieldName = "AbsenceType";
            this.colAbsenceType.Name = "colAbsenceType";
            this.colAbsenceType.OptionsColumn.AllowEdit = false;
            this.colAbsenceType.OptionsColumn.AllowFocus = false;
            this.colAbsenceType.OptionsColumn.ReadOnly = true;
            this.colAbsenceType.Visible = true;
            this.colAbsenceType.VisibleIndex = 3;
            this.colAbsenceType.Width = 122;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 7;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Holiday Year";
            this.gridColumn4.FieldName = "HolidayYearDescription";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 11;
            this.gridColumn4.Width = 94;
            // 
            // colEmployeeSurnameForename1
            // 
            this.colEmployeeSurnameForename1.Caption = "Surname: Forename";
            this.colEmployeeSurnameForename1.FieldName = "EmployeeSurnameForename";
            this.colEmployeeSurnameForename1.Name = "colEmployeeSurnameForename1";
            this.colEmployeeSurnameForename1.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurnameForename1.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurnameForename1.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurnameForename1.Visible = true;
            this.colEmployeeSurnameForename1.VisibleIndex = 14;
            this.colEmployeeSurnameForename1.Width = 131;
            // 
            // colEmployeeSurname1
            // 
            this.colEmployeeSurname1.Caption = "Surname";
            this.colEmployeeSurname1.FieldName = "EmployeeSurname";
            this.colEmployeeSurname1.Name = "colEmployeeSurname1";
            this.colEmployeeSurname1.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname1.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname1.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeForename1
            // 
            this.colEmployeeForename1.Caption = "Forename";
            this.colEmployeeForename1.FieldName = "EmployeeForename";
            this.colEmployeeForename1.Name = "colEmployeeForename1";
            this.colEmployeeForename1.OptionsColumn.AllowEdit = false;
            this.colEmployeeForename1.OptionsColumn.AllowFocus = false;
            this.colEmployeeForename1.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeNumber3
            // 
            this.colEmployeeNumber3.Caption = "Employee #";
            this.colEmployeeNumber3.FieldName = "EmployeeNumber";
            this.colEmployeeNumber3.Name = "colEmployeeNumber3";
            this.colEmployeeNumber3.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber3.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber3.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber3.Width = 78;
            // 
            // colApprovedByPerson
            // 
            this.colApprovedByPerson.Caption = "Approved By";
            this.colApprovedByPerson.FieldName = "ApprovedByPerson";
            this.colApprovedByPerson.Name = "colApprovedByPerson";
            this.colApprovedByPerson.OptionsColumn.AllowEdit = false;
            this.colApprovedByPerson.OptionsColumn.AllowFocus = false;
            this.colApprovedByPerson.OptionsColumn.ReadOnly = true;
            this.colApprovedByPerson.Visible = true;
            this.colApprovedByPerson.VisibleIndex = 14;
            this.colApprovedByPerson.Width = 83;
            // 
            // colCancelledByPerson
            // 
            this.colCancelledByPerson.Caption = "Cancelled By";
            this.colCancelledByPerson.FieldName = "CancelledByPerson";
            this.colCancelledByPerson.Name = "colCancelledByPerson";
            this.colCancelledByPerson.OptionsColumn.AllowEdit = false;
            this.colCancelledByPerson.OptionsColumn.AllowFocus = false;
            this.colCancelledByPerson.OptionsColumn.ReadOnly = true;
            this.colCancelledByPerson.Visible = true;
            this.colCancelledByPerson.VisibleIndex = 16;
            this.colCancelledByPerson.Width = 82;
            // 
            // colAbsenceSubType
            // 
            this.colAbsenceSubType.Caption = "Sub-Type";
            this.colAbsenceSubType.FieldName = "AbsenceSubType";
            this.colAbsenceSubType.Name = "colAbsenceSubType";
            this.colAbsenceSubType.OptionsColumn.AllowEdit = false;
            this.colAbsenceSubType.OptionsColumn.AllowFocus = false;
            this.colAbsenceSubType.OptionsColumn.ReadOnly = true;
            this.colAbsenceSubType.Visible = true;
            this.colAbsenceSubType.VisibleIndex = 4;
            this.colAbsenceSubType.Width = 116;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Active Holiday Year";
            this.gridColumn5.FieldName = "ActiveHolidayYear";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 114;
            // 
            // colContractNumber1
            // 
            this.colContractNumber1.Caption = "Contract #";
            this.colContractNumber1.FieldName = "ContractNumber";
            this.colContractNumber1.Name = "colContractNumber1";
            this.colContractNumber1.OptionsColumn.AllowEdit = false;
            this.colContractNumber1.OptionsColumn.AllowFocus = false;
            this.colContractNumber1.OptionsColumn.ReadOnly = true;
            // 
            // colEWCDate
            // 
            this.colEWCDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colEWCDate.FieldName = "EWCDate";
            this.colEWCDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEWCDate.Name = "colEWCDate";
            this.colEWCDate.OptionsColumn.AllowEdit = false;
            this.colEWCDate.OptionsColumn.AllowFocus = false;
            this.colEWCDate.OptionsColumn.ReadOnly = true;
            this.colEWCDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEWCDate.Visible = true;
            this.colEWCDate.VisibleIndex = 10;
            // 
            // colExpectedReturnDate
            // 
            this.colExpectedReturnDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colExpectedReturnDate.FieldName = "ExpectedReturnDate";
            this.colExpectedReturnDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedReturnDate.Name = "colExpectedReturnDate";
            this.colExpectedReturnDate.OptionsColumn.AllowEdit = false;
            this.colExpectedReturnDate.OptionsColumn.AllowFocus = false;
            this.colExpectedReturnDate.OptionsColumn.ReadOnly = true;
            this.colExpectedReturnDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedReturnDate.Visible = true;
            this.colExpectedReturnDate.VisibleIndex = 11;
            this.colExpectedReturnDate.Width = 128;
            // 
            // colHalfDayEnd
            // 
            this.colHalfDayEnd.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHalfDayEnd.FieldName = "HalfDayEnd";
            this.colHalfDayEnd.Name = "colHalfDayEnd";
            this.colHalfDayEnd.OptionsColumn.AllowEdit = false;
            this.colHalfDayEnd.OptionsColumn.AllowFocus = false;
            this.colHalfDayEnd.OptionsColumn.ReadOnly = true;
            this.colHalfDayEnd.Visible = true;
            this.colHalfDayEnd.VisibleIndex = 9;
            this.colHalfDayEnd.Width = 83;
            // 
            // colHalfDayStart
            // 
            this.colHalfDayStart.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHalfDayStart.FieldName = "HalfDayStart";
            this.colHalfDayStart.Name = "colHalfDayStart";
            this.colHalfDayStart.OptionsColumn.AllowEdit = false;
            this.colHalfDayStart.OptionsColumn.AllowFocus = false;
            this.colHalfDayStart.OptionsColumn.ReadOnly = true;
            this.colHalfDayStart.Visible = true;
            this.colHalfDayStart.VisibleIndex = 8;
            this.colHalfDayStart.Width = 89;
            // 
            // colAdditionalDaysPaid
            // 
            this.colAdditionalDaysPaid.Caption = "Additional Days Paid";
            this.colAdditionalDaysPaid.ColumnEdit = this.repositoryItemTextEdit2dp;
            this.colAdditionalDaysPaid.FieldName = "AdditionalDaysPaid";
            this.colAdditionalDaysPaid.Name = "colAdditionalDaysPaid";
            this.colAdditionalDaysPaid.OptionsColumn.AllowEdit = false;
            this.colAdditionalDaysPaid.OptionsColumn.AllowFocus = false;
            this.colAdditionalDaysPaid.OptionsColumn.ReadOnly = true;
            this.colAdditionalDaysPaid.Visible = true;
            this.colAdditionalDaysPaid.VisibleIndex = 12;
            this.colAdditionalDaysPaid.Width = 118;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(597, 446);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(516, 446);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl2;
            // 
            // sp_HR_00187_Select_Absence_Holiday_YearsTableAdapter
            // 
            this.sp_HR_00187_Select_Absence_Holiday_YearsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00188_Select_Absence_Holiday_Year_AbsencesTableAdapter
            // 
            this.sp_HR_00188_Select_Absence_Holiday_Year_AbsencesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_Absence
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(678, 475);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Absence";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Absence";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Absence_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00187SelectAbsenceHolidayYearsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditUnknownHoliday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDaysHoliday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHoursHoliday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00188SelectAbsenceHolidayYearAbsencesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2dp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00187SelectAbsenceHolidayYearsBindingSource;
        private System.Windows.Forms.BindingSource spHR00188SelectAbsenceHolidayYearAbsencesBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00187_Select_Absence_Holiday_YearsTableAdapter sp_HR_00187_Select_Absence_Holiday_YearsTableAdapter;
        private DataSet_HR_CoreTableAdapters.sp_HR_00188_Select_Absence_Holiday_Year_AbsencesTableAdapter sp_HR_00188_Select_Absence_Holiday_Year_AbsencesTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceSubTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsUsed;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2dp;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor1;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDeclaredDisability;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colPayCategoryId;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWorkRelated;
        private DevExpress.XtraGrid.Columns.GridColumn colComments4;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurnameForename1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeForename1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceSubType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colEWCDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedReturnDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHalfDayEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colHalfDayStart;
        private DevExpress.XtraGrid.Columns.GridColumn colAdditionalDaysPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeHolidayYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colBasicHolidayEntitlement;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditUnknownHoliday;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchasedHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursPerHolidayDay;
        private DevExpress.XtraGrid.Columns.GridColumn colMustTakeCertainHolidays;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurnameForename;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeForename;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveHolidayYear;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceAuthorisedTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceUnauthorisedTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidaysRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidaysRemaining;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDaysHoliday;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHoursHoliday;
    }
}

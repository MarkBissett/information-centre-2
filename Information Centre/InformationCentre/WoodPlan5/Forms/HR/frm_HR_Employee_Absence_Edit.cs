using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraEditors.Controls;
using DevExpress.Utils;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Absence_Edit : frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int ParentRecordId { get; set; }
        public string ParentRecordDescription { get; set; }
        public int ParentEmployeeID { get; set; }
        #endregion

        public frm_HR_Employee_Absence_Edit()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Absence_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 990108;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp_HR_00078_Absence_Record_Types_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00078_Absence_Record_Types_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00078_Absence_Record_Types_List_With_Blank);

            sp_HR_00079_Absence_Payment_Types_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00079_Absence_Payment_Types_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00079_Absence_Payment_Types_List_With_Blank);

            sp_HR_00080_Absence_Status_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00080_Absence_Status_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00080_Absence_Status_List_With_Blank);

            // Populate Main Dataset //
            sp_HR_00051_Get_Employee_Absence_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp_HR_00051_Get_Employee_Absence_Item.NewRow();
                        drNewRow["Mode"] = strFormMode;
                        drNewRow["RecordIds"] = strRecordIDs;
                        drNewRow["EmployeeHolidayYearId"] = ParentRecordId;
                        drNewRow["HolidayYearDescription"] = ParentRecordDescription;
                        drNewRow["RecordTypeId"] = 0;
                        drNewRow["AbsenceTypeId"] = 0;
                        drNewRow["AbsenceSubTypeId"] = 0;
                        drNewRow["ApproverId"] = 0;
                        drNewRow["ApprovedByPerson"] = "";
                        drNewRow["CancelledById"] = 0;
                        drNewRow["CancelledByPerson"] = "";
                        drNewRow["PayCategoryId"] = 0;
                        drNewRow["StatusId"] = 0;
                        drNewRow["HalfDayStart"] = 0;
                        drNewRow["HalfDayEnd"] = 0;
                        drNewRow["EmployeeID"] = ParentEmployeeID;
                        Boolean boolHoursStatsSet = false;
                        if (ParentRecordId > 0)  // Get Hours Used Stats //
                        {
                            SqlDataAdapter sdaHoursStats = new SqlDataAdapter();
                            DataSet dsHoursStats = new DataSet("NewDataSet");            
                            SqlConnection SQlConn = new SqlConnection(strConnectionString);
                            SqlCommand cmd = new SqlCommand("sp_HR_00010_Get_Employee_Holiday_Year_Item", SQlConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@RecordIds", ParentRecordId.ToString() + ","));
                            cmd.Parameters.Add(new SqlParameter("@Mode", "edit"));
                            dsHoursStats.Clear();  // Remove old values first //
                            sdaHoursStats = new SqlDataAdapter(cmd);
                            sdaHoursStats.Fill(dsHoursStats, "Table");

                            if (dsHoursStats.Tables[0].Rows.Count == 1)
                            {
                                DataRow dr = dsHoursStats.Tables[0].Rows[0];

                                drNewRow["BasicHolidayEntitlement"] = Convert.ToDecimal(dr["BasicHolidayEntitlement"]);
                                drNewRow["PurchasedHolidayEntitlement"] = Convert.ToDecimal(dr["PurchasedHolidayEntitlement"]);
                                drNewRow["BankHolidayEntitlement"] = Convert.ToDecimal(dr["BankHolidayEntitlement"]);
                                drNewRow["HolidaysTaken"] = Convert.ToDecimal(dr["HolidaysTaken"]);
                                drNewRow["TotalHolidayEntitlement"] = Convert.ToDecimal(dr["TotalHolidayEntitlement"]);
                                drNewRow["BankHolidaysTaken"] = Convert.ToDecimal(dr["BankHolidaysTaken"]);
                                drNewRow["TotalHolidaysTaken"] = Convert.ToDecimal(dr["TotalHolidaysTaken"]);
                                drNewRow["AbsenceAuthorisedTaken"] = Convert.ToDecimal(dr["AbsenceAuthorisedTaken"]);
                                drNewRow["AbsenceUnauthorisedTaken"] = Convert.ToDecimal(dr["AbsenceUnauthorisedTaken"]);
                                drNewRow["HolidaysRemaining"] = Convert.ToDecimal(dr["HolidaysRemaining"]);
                                drNewRow["BankHolidaysRemaining"] = Convert.ToDecimal(dr["BankHolidaysRemaining"]);
                                drNewRow["TotalHolidaysRemaining"] = Convert.ToDecimal(dr["TotalHolidaysRemaining"]);
                                drNewRow["HoursPerHolidayDay"] = Convert.ToDecimal(dr["HoursPerHolidayDay"]);
                                int intHolidayUnitDescriptorId = Convert.ToInt32(dr["HolidayUnitDescriptorId"]);
                                drNewRow["HolidayUnitDescriptor"] = (intHolidayUnitDescriptorId == 1 ? "Days" : (intHolidayUnitDescriptorId == 2 ? "Hours" : "??"));
                                boolHoursStatsSet = true;
                            }
                        }
                        if (!boolHoursStatsSet)
                        {
                            drNewRow["BasicHolidayEntitlement"] = (decimal)0.00;
                            drNewRow["PurchasedHolidayEntitlement"] = (decimal)0.00;
                            drNewRow["BankHolidayEntitlement"] = (decimal)0.00;
                            drNewRow["TotalHolidayEntitlement"] = (decimal)0.00;
                            drNewRow["HolidaysTaken"] = (decimal)0.00;
                            drNewRow["BankHolidaysTaken"] = (decimal)0.00;
                            drNewRow["TotalHolidaysTaken"] = (decimal)0.00;
                            drNewRow["AbsenceAuthorisedTaken"] = (decimal)0.00;
                            drNewRow["AbsenceUnauthorisedTaken"] = (decimal)0.00;
                            drNewRow["HolidaysRemaining"] = (decimal)0.00;
                            drNewRow["BankHolidaysRemaining"] = (decimal)0.00;
                            drNewRow["TotalHolidaysRemaining"] = (decimal)0.00;
                            drNewRow["HoursPerHolidayDay"] = (decimal)0.00;
                            drNewRow["HolidayUnitDescriptor"] = "??";
                        }
                        this.dataSet_HR_DataEntry.sp_HR_00051_Get_Employee_Absence_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp_HR_00051_Get_Employee_Absence_Item.NewRow();
                        drNewRow["Mode"] = strFormMode;
                        drNewRow["RecordIds"] = strRecordIDs;
                        this.dataSet_HR_DataEntry.sp_HR_00051_Get_Employee_Absence_Item.Rows.Add(drNewRow);
                        drNewRow.AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_HR_00051_Get_Employee_Absence_ItemTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00051_Get_Employee_Absence_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00051_Get_Employee_Absence_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Employee Absence", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            Set_Editor_Back_Colours();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
            Set_Editor_Masks();
            Set_Editor_Back_Colours();
        }

        private void ConfigureFormAccordingToMode()
        {
            ibool_FormStillLoading = true;  // Prevent validated code from firing on certain editors //
            
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        HolidayYearDescriptionButtonEdit.Focus();

                        HolidayYearDescriptionButtonEdit.Properties.ReadOnly = false;
                        HolidayYearDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        RecordTypeIdGridLookUpEdit.Focus();

                        HolidayYearDescriptionButtonEdit.Properties.ReadOnly = true;
                        HolidayYearDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        RecordTypeIdGridLookUpEdit.Focus();

                        HolidayYearDescriptionButtonEdit.Properties.ReadOnly = false;
                        HolidayYearDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        RecordTypeIdGridLookUpEdit.Focus();

                        HolidayYearDescriptionButtonEdit.Properties.ReadOnly = true;
                        HolidayYearDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();
            Set_Sickness_Fields_Enabled();
            Set_Absence_Comments_Read_Only();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            this.ValidateChildren();

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DefaultBoolean.True;
            }
            ibool_ignoreValidation = true;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
            //// Get Form Permissions //
            //intNearestHospitalGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            //intNearestHospitalGridLookUpEdit.Properties.Buttons[1].Visible = false;
            //intNearestHospitalGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            //intNearestHospitalGridLookUpEdit.Properties.Buttons[2].Visible = false;
            //intOwnershipGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            //intOwnershipGridLookUpEdit.Properties.Buttons[1].Visible = false;
            //intOwnershipGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            //intOwnershipGridLookUpEdit.Properties.Buttons[2].Visible = false;
            //intManagerGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            //intManagerGridLookUpEdit.Properties.Buttons[1].Visible = false;
            //intManagerGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            //intManagerGridLookUpEdit.Properties.Buttons[2].Visible = false;

            //sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            //sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_HR_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9028,2012,8001", GlobalSettings.ViewedPeriodID);
            //int intPartID = 0;
            //Boolean boolUpdate = false;
            //for (int i = 0; i < this.dataSet_HR_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            //{
            //    intPartID = Convert.ToInt32(this.dataSet_HR_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
            //    boolUpdate = Convert.ToBoolean(this.dataSet_HR_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
            //    switch (intPartID)
            //    {
            //        case 9028:  // Hospitals Picklist //    
            //            {
            //                intNearestHospitalGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
            //                intNearestHospitalGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
            //                intNearestHospitalGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
            //                intNearestHospitalGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
            //            }
            //            break;
            //        case 2012:  // Ownerships/Budgets //    
            //            {
            //                intOwnershipGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
            //                intOwnershipGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
            //                intOwnershipGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
            //                intOwnershipGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
            //            }
            //            break;
            //        case 8001:  // Contractor Manager //    
            //            {
            //                intManagerGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
            //                intManagerGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
            //                intManagerGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
            //                intManagerGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
            //            }
            //            break;
            //    }
            //}
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["Id"] == null ? 0 : Convert.ToInt32(currentRow["Id"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of Linked Documents button //

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_HR_Employee_Absence_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Employee_Absence_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.spHR00051GetEmployeeAbsenceItemBindingSource.EndEdit();
            try
            {
                this.sp_HR_00051_Get_Employee_Absence_ItemTableAdapter.Update(dataSet_HR_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
                if (currentRow != null) strNewIDs = currentRow["Id"].ToString() + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["Mode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        frm_HR_Employee_Manager fParentForm;
                        fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(4, Utils.enmMainGrids.Absences, strNewIDs);
                    }
                    else if (frmChild.Name == "frm_HR_Employee_Edit")
                    {
                        frm_HR_Employee_Edit fParentForm;
                        fParentForm = (frm_HR_Employee_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(10, Utils.enmMainGrids.Absences, strNewIDs);
                    }
                    else if (frmChild.Name == "frm_HR_Holiday_Manager")
                    {
                        frm_HR_Holiday_Manager fParentForm;
                        fParentForm = (frm_HR_Holiday_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", strNewIDs);
                    }
                    else if (frmChild.Name == "frm_HR_Report_Payroll_Export_Absences")
                    {
                        frm_HR_Report_Payroll_Export_Absences fParentForm;
                        fParentForm = (frm_HR_Report_Payroll_Export_Absences)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, "", strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp_HR_00051_Get_Employee_Absence_Item.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp_HR_00051_Get_Employee_Absence_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                Set_Editor_Masks();
                Set_Editor_Back_Colours();
                Set_Sickness_Fields_Enabled();
                Set_Expected_Return_Date_Label();
                Set_Absence_Comments_Read_Only();
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void HolidayYearDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
                if (currentRow == null) return;
                int EmployeeHolidayYearId = (string.IsNullOrEmpty(currentRow["EmployeeHolidayYearId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeHolidayYearId"]));
                string HolidayUnitDescriptor = (string.IsNullOrEmpty(currentRow["HolidayUnitDescriptor"].ToString()) ? "" : currentRow["HolidayUnitDescriptor"].ToString());

                if (EmployeeHolidayYearId > 0)
                {
                    // A contract is already selected. Warn the user of affects of a change will be //
                    if (!ContinueFromParameterChangeWarning()) return;
                }
                var fChildForm = new frm_HR_Select_Employee_Holiday_Year();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalSelectedValue = EmployeeHolidayYearId;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (EmployeeHolidayYearId != fChildForm.intSelectedID)
                    {
                        currentRow["EmployeeHolidayYearId"] = fChildForm.intSelectedID;
                        currentRow["HolidayYearDescription"] = fChildForm.strSelectedValue;
                        currentRow["HolidayUnitDescriptor"] = fChildForm.HolidayUnitDescriptor;
                        currentRow["HolidaysTaken"] = fChildForm.HolidaysTaken;
                        currentRow["BankHolidaysTaken"] = fChildForm.BankHolidaysTaken;
                        currentRow["AbsenceAuthorisedTaken"] = fChildForm.AbsenceAuthorisedTaken;
                        currentRow["AbsenceUnauthorisedTaken"] = fChildForm.AbsenceUnauthorisedTaken;
                        currentRow["BasicHolidayEntitlement"] = fChildForm.BasicHolidayEntitlement;
                        currentRow["BankHolidayEntitlement"] = fChildForm.BankHolidayEntitlement;
                        currentRow["PurchasedHolidayEntitlement"] = fChildForm.PurchasedHolidayEntitlement;
                        currentRow["TotalHolidayEntitlement"] = fChildForm.TotalHolidayEntitlement;
                        currentRow["HolidaysRemaining"] = fChildForm.HolidaysRemaining;
                        currentRow["BankHolidaysRemaining"] = fChildForm.BankHolidaysRemaining;
                        currentRow["TotalHolidaysTaken"] = fChildForm.TotalHolidaysTaken;
                        currentRow["TotalHolidaysRemaining"] = fChildForm.TotalHolidaysRemaining;
                        currentRow["HoursPerHolidayDay"] = fChildForm.HoursPerHolidayDay;
                        currentRow["EmployeeID"] = fChildForm.intEmployeeID;
                        spHR00051GetEmployeeAbsenceItemBindingSource.EndEdit();
                        Calculate_Holiday_Units_Used();
                    }
                    spHR00051GetEmployeeAbsenceItemBindingSource.EndEdit();
                    Set_Editor_Back_Colours();
                    if (HolidayUnitDescriptor != fChildForm.HolidayUnitDescriptor)
                    {
                        Set_Editor_Masks();
                    }
                }
            }

        }
        private void HolidayYearDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(HolidayYearDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(HolidayYearDescriptionButtonEdit, "");
            }
        }

        private void AbsenceTypeDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
                if (currentRow == null) return;
                int absenceTypeId = (string.IsNullOrEmpty(currentRow["AbsenceTypeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["AbsenceTypeId"]));
                int absenceSubTypeId = (string.IsNullOrEmpty(currentRow["AbsenceSubTypeId"].ToString()) ? 0 :  Convert.ToInt32(currentRow["AbsenceSubTypeId"]));
                int RecordTypeId = (string.IsNullOrEmpty(currentRow["RecordTypeId"].ToString()) ? 0 :  Convert.ToInt32(currentRow["RecordTypeId"]));

                frm_HR_Select_Absence_Type fChildForm = new frm_HR_Select_Absence_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intPassedInFilterID = RecordTypeId;
                fChildForm.intPassedInTypeId = absenceTypeId;
                fChildForm.intPassedInSubTypeId = absenceSubTypeId;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["AbsenceTypeId"] = fChildForm.intSelectedTypeId;
                    currentRow["AbsenceSubTypeId"] = fChildForm.intSelectedSubTypeId;
                    currentRow["AbsenceTypeDescription"] = fChildForm.strSelectedTypeName;
                    currentRow["AbsenceSubTypeDescription"] = fChildForm.strSelectedSubTypeName;

                    switch (fChildForm.intSelectedTypeId)
                    {
                        case -1:
                        case -2:
                        case -3:
                            if (RecordTypeId != 1) currentRow["RecordTypeId"] = 1;  // Holiday //
                            break;
                        case 0:
                            break;
                        default:
                            if (RecordTypeId != 2) currentRow["RecordTypeId"] = 2;  // Absence //
                            break;
                    }
                    if (fChildForm.strSelectedTypeName != "Maternity")
                    {
                        currentRow["EWCDate"] = DBNull.Value;
                        currentRow["ExpectedReturnDate"] = DBNull.Value;
                    }

                    spHR00051GetEmployeeAbsenceItemBindingSource.EndEdit();
                    Set_Sickness_Fields_Enabled();
                    Set_Expected_Return_Date_Label();
                    Set_Absence_Comments_Read_Only();
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                }
            }
        }
        private void AbsenceTypeDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(AbsenceTypeDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AbsenceTypeDescriptionButtonEdit, "");
            }
        }

        private void RecordTypeIdGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void RecordTypeIdGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(RecordTypeIdGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(RecordTypeIdGridLookUpEdit, "");
            }
        }
        private void RecordTypeIdGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;

            currentRow["AbsenceTypeDescription"] = "";
            currentRow["AbsenceTypeId"] = 0;
            currentRow["AbsenceSubTypeDescription"] = "";
            currentRow["AbsenceSubTypeId"] = 0;
            currentRow["EWCDate"] = DBNull.Value;
            currentRow["ExpectedReturnDate"] = DBNull.Value;

            // See if we can set a value for Payment Category //
            int intPaid = 0;
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (!(glue.EditValue == DBNull.Value || glue.EditValue.ToString() == "0"))
            {
                if (Convert.ToInt32(RecordTypeIdGridLookUpEdit.EditValue) == 1)
                {
                    glue = (GridLookUpEdit)PayCategoryIdGridLookUpEdit;
                    GridView view = glue.Properties.View;
                    int intFoundRow = view.LocateByDisplayText(0, view.Columns["Description"], "Paid");
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        intPaid = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "ID"));
                    }
                }
                else  // Get Unpaid value //
                {
                    glue = (GridLookUpEdit)PayCategoryIdGridLookUpEdit;
                    GridView view = glue.Properties.View;
                    int intFoundRow = view.LocateByDisplayText(0, view.Columns["Description"], "Unpaid");
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        intPaid = Convert.ToInt32(view.GetRowCellValue(intFoundRow, "ID"));
                    }
                }
                currentRow["PayCategoryId"] = intPaid;
                dxErrorProvider1.SetError(PayCategoryIdGridLookUpEdit, "");
           }

            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            Set_Expected_Return_Date_Label();
            Set_Absence_Comments_Read_Only();
        }
        
        private void ApprovedByPersonButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
                if (currentRow == null) return;
                int approverId = (string.IsNullOrEmpty(currentRow["ApproverId"].ToString()) ? 0 : Convert.ToInt32(currentRow["ApproverId"]));

                frm_HR_Select_Employee fChildForm = new frm_HR_Select_Employee();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = approverId;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["ApproverId"] = fChildForm.intSelectedEmployeeID;
                    currentRow["ApprovedByPerson"] = fChildForm.strSelectedEmployeeName;
                    currentRow["ApprovedDate"] = DateTime.Now;
                    spHR00051GetEmployeeAbsenceItemBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")  // Clear Button //
            {
                DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
                if (currentRow == null) return;
                currentRow["ApproverId"] = 0;
                currentRow["ApprovedByPerson"] = "";
                currentRow["ApprovedDate"] = DBNull.Value;
                spHR00051GetEmployeeAbsenceItemBindingSource.EndEdit();
            }
        }

        private void CancelledByPersonButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
                if (currentRow == null) return;
                int cancelledById = (string.IsNullOrEmpty(currentRow["CancelledById"].ToString()) ? 0 : Convert.ToInt32(currentRow["CancelledById"]));

                frm_HR_Select_Employee fChildForm = new frm_HR_Select_Employee();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = cancelledById;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["CancelledById"] = fChildForm.intSelectedEmployeeID;
                    currentRow["CancelledByPerson"] = fChildForm.strSelectedEmployeeName;
                    currentRow["CancelledDate"] = DateTime.Now;
                    spHR00051GetEmployeeAbsenceItemBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")  // Clear Button //
            {
                DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
                if (currentRow == null) return;
                currentRow["CancelledById"] = 0;
                currentRow["CancelledByPerson"] = "";
                currentRow["CancelledDate"] = DBNull.Value;
                spHR00051GetEmployeeAbsenceItemBindingSource.EndEdit();
            }
      }

        private bool ContinueFromParameterChangeWarning()
        {
            var msg = "Warning! Changing this value will mean that any changes you have made will be lost.\nClick OK to continue the edit or Cancel to return.";
            if (XtraMessageBox.Show(msg, "WARNING!", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Cancel) return false;
            return true;
        }

        private void AbsenceYearIdGridLookUpEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            /*if (e.OldValue == e.NewValue) return;
            if (!ContinueFromParameterChangeWarning()) e.Cancel = true; */
        }

        private void StatusIdGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(StatusIdGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(StatusIdGridLookUpEdit, "");
            }
        }

        private void StartDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void StartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "");
            }
        }
        private void StartDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Holiday_Units_Used();
        }

        private void EndDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void EndDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Holiday_Units_Used();
        }

        private void HalfDayStartCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void HalfDayStartCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Holiday_Units_Used();
        }

        private void HalfDayEndCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void HalfDayEndCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Holiday_Units_Used();
        }

        private void PayCategoryIdGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(PayCategoryIdGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(PayCategoryIdGridLookUpEdit, "");
            }
        }


        private void Calculate_Holiday_Units_Used()
        {
            decimal decCalculatedUnits = (decimal)0.00;

            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;

            if ((String.IsNullOrEmpty(StartDateDateEdit.DateTime.ToString()) || StartDateDateEdit.DateTime == DateTime.MinValue) || (String.IsNullOrEmpty(EndDateDateEdit.DateTime.ToString()) || EndDateDateEdit.DateTime == DateTime.MinValue)) return;
            if (string.IsNullOrEmpty(currentRow["HolidayUnitDescriptor"].ToString())) return;

            DateTime StartDate = StartDateDateEdit.DateTime;
            DateTime EndDate = EndDateDateEdit.DateTime;
            int intHolidayUnitDescriptorID = (currentRow["HolidayUnitDescriptor"].ToString() == "Days" ? 1 : 0);
            int intEmployeeID = (string.IsNullOrWhiteSpace(currentRow["EmployeeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeID"]));
            int intHalfDayStart = (string.IsNullOrWhiteSpace(currentRow["HalfDayStart"].ToString()) ? 0 : Convert.ToInt32(currentRow["HalfDayStart"]));
            int intHalfDayEnd = (string.IsNullOrWhiteSpace(currentRow["HalfDayEnd"].ToString()) ? 0 : Convert.ToInt32(currentRow["HalfDayEnd"]));


            SqlConnection conn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp_HR_00161_Calculate_Absence_Duration", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@EmployeeID", intEmployeeID));
            cmd.Parameters.Add(new SqlParameter("@HolidayUnitID", intHolidayUnitDescriptorID));
            cmd.Parameters.Add(new SqlParameter("@StartDate", StartDate));
            cmd.Parameters.Add(new SqlParameter("@EndDate", EndDate));
            cmd.Parameters.Add(new SqlParameter("@HalfDayStart", intHalfDayStart));
            cmd.Parameters.Add(new SqlParameter("@HalfDayEnd", intHalfDayEnd));
            //cmd.Parameters.Add("@AbsenceDuration", SqlDbType.Decimal).Direction = ParameterDirection.Output;
            // ***** Need the following as opposed to the commented out line above to stop rounding as it will round to 0 DP if the scale and precision is not set ***** //
            SqlParameter outputParameter = new SqlParameter("@AbsenceDuration", SqlDbType.Decimal);
            outputParameter.Precision = 18;
            outputParameter.Scale = 2;
            outputParameter.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(outputParameter);

            conn.Open();
            try
            {
                cmd.ExecuteNonQuery();
                decCalculatedUnits = Convert.ToDecimal(cmd.Parameters["@AbsenceDuration"].Value);
                //decCalculatedUnits = Convert.ToDecimal(returnParameter.Value);
            }
            catch (Exception) { }
            conn.Close();
            conn.Dispose();
            cmd = null;

            if (decCalculatedUnits <= (decimal)0.00)  // Unable to calculate absence length from System Generated Employee Working Days so try it manually //
            {
                // Calculate Work Days First //
                decCalculatedUnits = Convert.ToDecimal(WorkDays(Convert.ToDateTime(currentRow["StartDate"]), EndDate));
                decCalculatedUnits = Math.Floor(decCalculatedUnits);

                // Factor in Half Day Start or End. If both set and only one day holiday then it's just subtract 0.5 days, not 2 * 0.5 days) //
                if (intHalfDayStart == 1 || intHalfDayEnd == 1)
                {
                    if (decCalculatedUnits <= (decimal)1.00)
                    {
                        decCalculatedUnits = decCalculatedUnits - (decimal)0.50;
                    }
                    else
                    {
                        decCalculatedUnits = decCalculatedUnits - (intHalfDayStart == 1 ? (decimal)0.50 : (decimal)0.00) - (intHalfDayEnd == 1 ? (decimal)0.50 : (decimal)0.00);
                    }
                }

                if (currentRow["HolidayUnitDescriptor"].ToString() == "??") return;
                if (currentRow["HolidayUnitDescriptor"].ToString() == "Hours")  // Convert Days to Hours //
                {
                    if (Convert.ToDecimal(currentRow["HoursPerHolidayDay"]) <= (decimal)0.00)
                    {
                        decCalculatedUnits = (decimal)0.00;
                    }
                    else
                    {
                        decCalculatedUnits = decCalculatedUnits * Convert.ToDecimal(currentRow["HoursPerHolidayDay"]);

                        // This needs further Work - need to load shift Pattern then process each holiday day against the appropriate shift pattern to get accumulated holiday hours //
                    }
                }
            }
            currentRow["UnitsUsed"] = decCalculatedUnits;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            Calculate_Unpaid_Units();
        }

        private void ApprovedDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {

        }

        private void CancelledDateDateEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {

        }

        private void EWCDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void EWCDateDateEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            DateEdit de = (DateEdit)sender;
            if ((String.IsNullOrEmpty(de.DateTime.ToString()) || de.DateTime == DateTime.MinValue)) return;

            Calculate_Expected_Return_Date(de.DateTime);
        }
        private void Calculate_Expected_Return_Date(DateTime startDate)
        {
            DateTime dtCalculatedEndDate = startDate.AddDays(7 * 52);
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;
            currentRow["ExpectedReturnDate"] = dtCalculatedEndDate;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
        }


        private void UnitsUsedSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void UnitsUsedSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Unpaid_Units();
        }

        private void UnitsPaidCSPSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void UnitsPaidCSPSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Unpaid_Units();
        }

        private void UnitsPaidHalfPaySpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void UnitsPaidHalfPaySpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Unpaid_Units();
        }

        private void UnitsPaidOtherSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void UnitsPaidOtherSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            Calculate_Unpaid_Units();
        }
        
        private void Calculate_Unpaid_Units()
        {
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;
            if (currentRow["RecordTypeId"].ToString() != "2") return;

            decimal decHolidayUnits = (string.IsNullOrWhiteSpace(currentRow["UnitsUsed"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["UnitsUsed"]));
            decimal decUnitsPaidCSP = (string.IsNullOrWhiteSpace(currentRow["UnitsPaidCSP"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["UnitsPaidCSP"]));
            decimal decUnitsPaidHalfPay = (string.IsNullOrWhiteSpace(currentRow["UnitsPaidHalfPay"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["UnitsPaidHalfPay"]));
            decimal decUnitsPaidOther = (string.IsNullOrWhiteSpace(currentRow["UnitsPaidOther"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["UnitsPaidOther"]));

            currentRow["UnitsPaidUnpaid"] = decHolidayUnits - (decUnitsPaidCSP + decUnitsPaidHalfPay + decUnitsPaidOther);
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
        }

        #endregion


        private void Set_Editor_Masks()
        {
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;         
            string strMaskDescription = "####0.00 " + currentRow["HolidayUnitDescriptor"].ToString() ?? "";

            BasicHolidayEntitlementTextEdit.Properties.Mask.EditMask = strMaskDescription;
            PurchasedHolidayEntitlementTextEdit.Properties.Mask.EditMask = strMaskDescription;
            BankHolidayEntitlementTextEdit.Properties.Mask.EditMask = strMaskDescription;
            TotalHolidayEntitlementTextEdit.Properties.Mask.EditMask = strMaskDescription;
            HolidaysTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            BankHolidaysTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            TotalHolidaysTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            AbsenceAuthorisedTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            AbsenceUnauthorisedTakenTextEdit.Properties.Mask.EditMask = strMaskDescription;
            HolidaysRemainingTextEdit.Properties.Mask.EditMask = strMaskDescription;
            BankHolidaysRemainingTextEdit.Properties.Mask.EditMask = strMaskDescription;
            TotalHolidaysRemainingTextEdit.Properties.Mask.EditMask = strMaskDescription;
        }

        private void Set_Editor_Back_Colours()
        {
            if (this.strFormMode == "blockedit") return;
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;

            /*DevExpress.Skins.Skin currentSkin;
            DevExpress.Skins.SkinElement element;
            currentSkin = DevExpress.Skins.CommonSkins.GetSkin(dataLayoutControl1.LookAndFeel);
            Color foreColor =  currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.WindowText);
            Color backColor = currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.Window);
            Color disabledForeColor = currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.DisabledText);
            Color disabledBackColor =  currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.DisabledControl);*/

            Color foreColorGood = Color.Black;
            Color backColorGood = Color.PaleGreen; ;
            Color foreColorBad = Color.Black;
            Color backColorBad = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);


            HolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(HolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            HolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(HolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
            BankHolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(BankHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            BankHolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(BankHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
            TotalHolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(TotalHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            TotalHolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(TotalHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
        }

        private double WorkDays(DateTime startDate, DateTime endDate)
        {
            double weekendDays;
            double days = endDate.Subtract(startDate).TotalDays;
            if (days < 0) return 0;
            days++;  // Add day to include end date //
            DateTime startMonday = startDate.AddDays(DayOfWeek.Monday - startDate.DayOfWeek).Date;
            DateTime endMonday = endDate.AddDays(DayOfWeek.Monday - endDate.DayOfWeek).Date;
            weekendDays = ((endMonday.Subtract(startMonday).TotalDays) / 7) * 2;

            // Compute fractionary part of weekend days //
            double diffStart = startDate.Subtract(startMonday).TotalDays - 5;
            double diffEnd = endDate.Subtract(endMonday).TotalDays - 5;

            // Compensate weekenddays //
            if (diffStart > 0) weekendDays -= diffStart;
            if (diffEnd > 0) weekendDays += diffEnd;

            return days - weekendDays;
        }

        private void Set_Sickness_Fields_Enabled()
        {
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;
            string strAbsenceTypeDescription = currentRow["AbsenceTypeDescription"].ToString();
            EWCDateDateEdit.Properties.ReadOnly = strAbsenceTypeDescription != "Sickness";
            IsWorkRelatedCheckEdit.Properties.ReadOnly = strAbsenceTypeDescription != "Sickness";
            DeclaredDisabilityCheckEdit.Properties.ReadOnly = strAbsenceTypeDescription != "Sickness";
            EWCDateDateEdit.Properties.ReadOnly = strAbsenceTypeDescription != "Maternity";
        }

        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["Id"].ToString()) ? 0 : Convert.ToInt32(currentRow["Id"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 6;  // Absence //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = currentRow["EmployeeSurnameForename"].ToString() + " - " + Convert.ToDateTime(currentRow["StartDate"]).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date" + " - " + Convert.ToDateTime(currentRow["EndDate"]).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Set_Expected_Return_Date_Label()
        {
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;
            ItemForExpectedReturnDate.Text = (currentRow["AbsenceTypeDescription"].ToString() == "Sickness" ? "Med 3 Expiry Date:" : "Expected Return Date:");
        }

        private void Set_Absence_Comments_Read_Only()
        {
            DataRowView currentRow = (DataRowView)spHR00051GetEmployeeAbsenceItemBindingSource.Current;
            if (currentRow == null) return;
            string strAbsenceTypeDescription = currentRow["RecordTypeId"].ToString();
            AbsenceCommentMemoEdit.Properties.ReadOnly = strAbsenceTypeDescription != "2";
            UnitsPaidCSPSpinEdit.Properties.ReadOnly = strAbsenceTypeDescription != "2";
            UnitsPaidHalfPaySpinEdit.Properties.ReadOnly = strAbsenceTypeDescription != "2";
            UnitsPaidOtherSpinEdit.Properties.ReadOnly = strAbsenceTypeDescription != "2";
        }

 
 


    }
}


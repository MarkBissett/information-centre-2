namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Vetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00189SelectVettingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVettingID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVettingTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVettingType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVettingSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVettingSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateIssued = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpiryDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIssuedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNotificationPeriodDays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRequestedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestedBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequestedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEssentialForRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRestrictionCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiredStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentEmployee1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00189_Select_VettingTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00189_Select_VettingTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00189SelectVettingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00189SelectVettingBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditNumericDays,
            this.repositoryItemMemoExEdit2});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00189SelectVettingBindingSource
            // 
            this.spHR00189SelectVettingBindingSource.DataMember = "sp_HR_00189_Select_Vetting";
            this.spHR00189SelectVettingBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVettingID,
            this.colEmployeeID5,
            this.colEmployeeName1,
            this.colEmployeeSurname4,
            this.colEmployeeFirstname1,
            this.colEmployeeNumber7,
            this.colVettingTypeID,
            this.colVettingType,
            this.colVettingSubTypeID,
            this.colVettingSubType,
            this.colDateIssued,
            this.colExpiryDate1,
            this.colReferenceNumber,
            this.colIssuedByID,
            this.colIssuedBy,
            this.colStatusID1,
            this.colStatus1,
            this.colNotificationPeriodDays,
            this.colRequestedByID,
            this.colRequestedBy,
            this.colRequestedDate,
            this.colEssentialForRole,
            this.colRemarks3,
            this.colRestrictionCount,
            this.colExpiredStatus,
            this.colCurrentEmployee1});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colExpiryDate1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colVettingID
            // 
            this.colVettingID.Caption = "Vetting ID";
            this.colVettingID.FieldName = "VettingID";
            this.colVettingID.Name = "colVettingID";
            this.colVettingID.OptionsColumn.AllowEdit = false;
            this.colVettingID.OptionsColumn.AllowFocus = false;
            this.colVettingID.OptionsColumn.ReadOnly = true;
            this.colVettingID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colEmployeeID5
            // 
            this.colEmployeeID5.Caption = "Employee ID";
            this.colEmployeeID5.FieldName = "EmployeeID";
            this.colEmployeeID5.Name = "colEmployeeID5";
            this.colEmployeeID5.OptionsColumn.AllowEdit = false;
            this.colEmployeeID5.OptionsColumn.AllowFocus = false;
            this.colEmployeeID5.OptionsColumn.ReadOnly = true;
            this.colEmployeeID5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID5.Width = 81;
            // 
            // colEmployeeName1
            // 
            this.colEmployeeName1.Caption = "Linked To Employee";
            this.colEmployeeName1.FieldName = "EmployeeName";
            this.colEmployeeName1.Name = "colEmployeeName1";
            this.colEmployeeName1.OptionsColumn.AllowEdit = false;
            this.colEmployeeName1.OptionsColumn.AllowFocus = false;
            this.colEmployeeName1.OptionsColumn.ReadOnly = true;
            this.colEmployeeName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName1.Width = 167;
            // 
            // colEmployeeSurname4
            // 
            this.colEmployeeSurname4.Caption = "Employee Surname";
            this.colEmployeeSurname4.FieldName = "EmployeeSurname";
            this.colEmployeeSurname4.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeSurname4.Name = "colEmployeeSurname4";
            this.colEmployeeSurname4.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname4.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname4.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname4.Visible = true;
            this.colEmployeeSurname4.VisibleIndex = 0;
            this.colEmployeeSurname4.Width = 112;
            // 
            // colEmployeeFirstname1
            // 
            this.colEmployeeFirstname1.Caption = "Employee Forename";
            this.colEmployeeFirstname1.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeFirstname1.Name = "colEmployeeFirstname1";
            this.colEmployeeFirstname1.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname1.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname1.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname1.Visible = true;
            this.colEmployeeFirstname1.VisibleIndex = 1;
            this.colEmployeeFirstname1.Width = 118;
            // 
            // colEmployeeNumber7
            // 
            this.colEmployeeNumber7.Caption = "Employee #";
            this.colEmployeeNumber7.FieldName = "EmployeeNumber";
            this.colEmployeeNumber7.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeNumber7.Name = "colEmployeeNumber7";
            this.colEmployeeNumber7.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber7.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber7.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber7.Visible = true;
            this.colEmployeeNumber7.VisibleIndex = 2;
            this.colEmployeeNumber7.Width = 78;
            // 
            // colVettingTypeID
            // 
            this.colVettingTypeID.Caption = "Vetting Type ID";
            this.colVettingTypeID.FieldName = "VettingTypeID";
            this.colVettingTypeID.Name = "colVettingTypeID";
            this.colVettingTypeID.OptionsColumn.AllowEdit = false;
            this.colVettingTypeID.OptionsColumn.AllowFocus = false;
            this.colVettingTypeID.OptionsColumn.ReadOnly = true;
            this.colVettingTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVettingTypeID.Width = 96;
            // 
            // colVettingType
            // 
            this.colVettingType.Caption = "Vetting Type";
            this.colVettingType.FieldName = "VettingType";
            this.colVettingType.Name = "colVettingType";
            this.colVettingType.OptionsColumn.AllowEdit = false;
            this.colVettingType.OptionsColumn.AllowFocus = false;
            this.colVettingType.OptionsColumn.ReadOnly = true;
            this.colVettingType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVettingType.Visible = true;
            this.colVettingType.VisibleIndex = 4;
            this.colVettingType.Width = 82;
            // 
            // colVettingSubTypeID
            // 
            this.colVettingSubTypeID.Caption = "Vetting Sub Type ID";
            this.colVettingSubTypeID.FieldName = "VettingSubTypeID";
            this.colVettingSubTypeID.Name = "colVettingSubTypeID";
            this.colVettingSubTypeID.OptionsColumn.AllowEdit = false;
            this.colVettingSubTypeID.OptionsColumn.AllowFocus = false;
            this.colVettingSubTypeID.OptionsColumn.ReadOnly = true;
            this.colVettingSubTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVettingSubTypeID.Width = 117;
            // 
            // colVettingSubType
            // 
            this.colVettingSubType.Caption = "Vetting Sub Type";
            this.colVettingSubType.FieldName = "VettingSubType";
            this.colVettingSubType.Name = "colVettingSubType";
            this.colVettingSubType.OptionsColumn.AllowEdit = false;
            this.colVettingSubType.OptionsColumn.AllowFocus = false;
            this.colVettingSubType.OptionsColumn.ReadOnly = true;
            this.colVettingSubType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colVettingSubType.Visible = true;
            this.colVettingSubType.VisibleIndex = 5;
            this.colVettingSubType.Width = 103;
            // 
            // colDateIssued
            // 
            this.colDateIssued.Caption = "Date Issued";
            this.colDateIssued.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateIssued.FieldName = "DateIssued";
            this.colDateIssued.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateIssued.Name = "colDateIssued";
            this.colDateIssued.OptionsColumn.AllowEdit = false;
            this.colDateIssued.OptionsColumn.AllowFocus = false;
            this.colDateIssued.OptionsColumn.ReadOnly = true;
            this.colDateIssued.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateIssued.Visible = true;
            this.colDateIssued.VisibleIndex = 6;
            this.colDateIssued.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colExpiryDate1
            // 
            this.colExpiryDate1.Caption = "Expiry Date";
            this.colExpiryDate1.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colExpiryDate1.FieldName = "ExpiryDate";
            this.colExpiryDate1.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpiryDate1.Name = "colExpiryDate1";
            this.colExpiryDate1.OptionsColumn.AllowEdit = false;
            this.colExpiryDate1.OptionsColumn.AllowFocus = false;
            this.colExpiryDate1.OptionsColumn.ReadOnly = true;
            this.colExpiryDate1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpiryDate1.Visible = true;
            this.colExpiryDate1.VisibleIndex = 7;
            this.colExpiryDate1.Width = 100;
            // 
            // colReferenceNumber
            // 
            this.colReferenceNumber.Caption = "Reference #";
            this.colReferenceNumber.FieldName = "ReferenceNumber";
            this.colReferenceNumber.Name = "colReferenceNumber";
            this.colReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colReferenceNumber.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colReferenceNumber.Visible = true;
            this.colReferenceNumber.VisibleIndex = 8;
            this.colReferenceNumber.Width = 82;
            // 
            // colIssuedByID
            // 
            this.colIssuedByID.Caption = "Issued By ID";
            this.colIssuedByID.FieldName = "IssuedByID";
            this.colIssuedByID.Name = "colIssuedByID";
            this.colIssuedByID.OptionsColumn.AllowEdit = false;
            this.colIssuedByID.OptionsColumn.AllowFocus = false;
            this.colIssuedByID.OptionsColumn.ReadOnly = true;
            this.colIssuedByID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIssuedByID.Width = 82;
            // 
            // colIssuedBy
            // 
            this.colIssuedBy.Caption = "Issued By";
            this.colIssuedBy.FieldName = "IssuedBy";
            this.colIssuedBy.Name = "colIssuedBy";
            this.colIssuedBy.OptionsColumn.AllowEdit = false;
            this.colIssuedBy.OptionsColumn.AllowFocus = false;
            this.colIssuedBy.OptionsColumn.ReadOnly = true;
            this.colIssuedBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colIssuedBy.Visible = true;
            this.colIssuedBy.VisibleIndex = 9;
            this.colIssuedBy.Width = 123;
            // 
            // colStatusID1
            // 
            this.colStatusID1.Caption = "Status ID";
            this.colStatusID1.FieldName = "StatusID";
            this.colStatusID1.Name = "colStatusID1";
            this.colStatusID1.OptionsColumn.AllowEdit = false;
            this.colStatusID1.OptionsColumn.AllowFocus = false;
            this.colStatusID1.OptionsColumn.ReadOnly = true;
            this.colStatusID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatus1
            // 
            this.colStatus1.Caption = "Status";
            this.colStatus1.FieldName = "Status";
            this.colStatus1.Name = "colStatus1";
            this.colStatus1.OptionsColumn.AllowEdit = false;
            this.colStatus1.OptionsColumn.AllowFocus = false;
            this.colStatus1.OptionsColumn.ReadOnly = true;
            this.colStatus1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatus1.Visible = true;
            this.colStatus1.VisibleIndex = 10;
            // 
            // colNotificationPeriodDays
            // 
            this.colNotificationPeriodDays.Caption = "Notification Period";
            this.colNotificationPeriodDays.ColumnEdit = this.repositoryItemTextEditNumericDays;
            this.colNotificationPeriodDays.FieldName = "NotificationPeriodDays";
            this.colNotificationPeriodDays.Name = "colNotificationPeriodDays";
            this.colNotificationPeriodDays.OptionsColumn.AllowEdit = false;
            this.colNotificationPeriodDays.OptionsColumn.AllowFocus = false;
            this.colNotificationPeriodDays.OptionsColumn.ReadOnly = true;
            this.colNotificationPeriodDays.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNotificationPeriodDays.Visible = true;
            this.colNotificationPeriodDays.VisibleIndex = 11;
            this.colNotificationPeriodDays.Width = 108;
            // 
            // repositoryItemTextEditNumericDays
            // 
            this.repositoryItemTextEditNumericDays.AutoHeight = false;
            this.repositoryItemTextEditNumericDays.Mask.EditMask = "#####0 Days";
            this.repositoryItemTextEditNumericDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericDays.Name = "repositoryItemTextEditNumericDays";
            // 
            // colRequestedByID
            // 
            this.colRequestedByID.Caption = "Requested By ID";
            this.colRequestedByID.FieldName = "RequestedByID";
            this.colRequestedByID.Name = "colRequestedByID";
            this.colRequestedByID.OptionsColumn.AllowEdit = false;
            this.colRequestedByID.OptionsColumn.AllowFocus = false;
            this.colRequestedByID.OptionsColumn.ReadOnly = true;
            this.colRequestedByID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRequestedByID.Width = 102;
            // 
            // colRequestedBy
            // 
            this.colRequestedBy.Caption = "Requested By";
            this.colRequestedBy.FieldName = "RequestedBy";
            this.colRequestedBy.Name = "colRequestedBy";
            this.colRequestedBy.OptionsColumn.AllowEdit = false;
            this.colRequestedBy.OptionsColumn.AllowFocus = false;
            this.colRequestedBy.OptionsColumn.ReadOnly = true;
            this.colRequestedBy.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRequestedBy.Visible = true;
            this.colRequestedBy.VisibleIndex = 12;
            this.colRequestedBy.Width = 88;
            // 
            // colRequestedDate
            // 
            this.colRequestedDate.Caption = "Requested Date";
            this.colRequestedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colRequestedDate.FieldName = "RequestedDate";
            this.colRequestedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colRequestedDate.Name = "colRequestedDate";
            this.colRequestedDate.OptionsColumn.AllowEdit = false;
            this.colRequestedDate.OptionsColumn.AllowFocus = false;
            this.colRequestedDate.OptionsColumn.ReadOnly = true;
            this.colRequestedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colRequestedDate.Visible = true;
            this.colRequestedDate.VisibleIndex = 13;
            this.colRequestedDate.Width = 100;
            // 
            // colEssentialForRole
            // 
            this.colEssentialForRole.Caption = "Essential For Role";
            this.colEssentialForRole.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colEssentialForRole.FieldName = "EssentialForRole";
            this.colEssentialForRole.Name = "colEssentialForRole";
            this.colEssentialForRole.OptionsColumn.AllowEdit = false;
            this.colEssentialForRole.OptionsColumn.AllowFocus = false;
            this.colEssentialForRole.OptionsColumn.ReadOnly = true;
            this.colEssentialForRole.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEssentialForRole.Visible = true;
            this.colEssentialForRole.VisibleIndex = 14;
            this.colEssentialForRole.Width = 106;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 15;
            // 
            // colRestrictionCount
            // 
            this.colRestrictionCount.Caption = "Restriction Count";
            this.colRestrictionCount.FieldName = "RestrictionCount";
            this.colRestrictionCount.Name = "colRestrictionCount";
            this.colRestrictionCount.OptionsColumn.AllowEdit = false;
            this.colRestrictionCount.OptionsColumn.AllowFocus = false;
            this.colRestrictionCount.OptionsColumn.ReadOnly = true;
            this.colRestrictionCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRestrictionCount.Visible = true;
            this.colRestrictionCount.VisibleIndex = 16;
            this.colRestrictionCount.Width = 104;
            // 
            // colExpiredStatus
            // 
            this.colExpiredStatus.Caption = "Expiry Warning";
            this.colExpiredStatus.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colExpiredStatus.FieldName = "ExpiredStatus";
            this.colExpiredStatus.Name = "colExpiredStatus";
            this.colExpiredStatus.OptionsColumn.AllowEdit = false;
            this.colExpiredStatus.OptionsColumn.AllowFocus = false;
            this.colExpiredStatus.OptionsColumn.ReadOnly = true;
            this.colExpiredStatus.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiredStatus.Width = 94;
            // 
            // colCurrentEmployee1
            // 
            this.colCurrentEmployee1.Caption = "Current Employee";
            this.colCurrentEmployee1.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colCurrentEmployee1.FieldName = "CurrentEmployee";
            this.colCurrentEmployee1.Name = "colCurrentEmployee1";
            this.colCurrentEmployee1.OptionsColumn.AllowEdit = false;
            this.colCurrentEmployee1.OptionsColumn.AllowFocus = false;
            this.colCurrentEmployee1.OptionsColumn.ReadOnly = true;
            this.colCurrentEmployee1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentEmployee1.Visible = true;
            this.colCurrentEmployee1.VisibleIndex = 3;
            this.colCurrentEmployee1.Width = 107;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00189_Select_VettingTableAdapter
            // 
            this.sp_HR_00189_Select_VettingTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_Vetting
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Vetting";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Vetting";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Vetting_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00189SelectVettingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource spHR00189SelectVettingBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00189_Select_VettingTableAdapter sp_HR_00189_Select_VettingTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID5;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber7;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingType;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colVettingSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colDateIssued;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colIssuedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colNotificationPeriodDays;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedBy;
        private DevExpress.XtraGrid.Columns.GridColumn colRequestedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEssentialForRole;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colRestrictionCount;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiredStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentEmployee1;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.HR;
using DevExpress.XtraEditors.Controls;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Working_Pattern_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //

        public int ParentRecordId { get; set; }
        public string ParentRecordDescription { get; set; }
        public string ParentRecordEmployeeName { get; set; }
        public DateTime ParentRecordStartDate { get; set; }
        public DateTime ParentRecordEndDate { get; set; }
        public int ParentEmployeeID { get; set; }

        #endregion


        public frm_HR_Employee_Working_Pattern_Edit()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Working_Pattern_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 990107;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            sp_HR_00049_Work_Pattern_Days_Of_Week_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00049_Work_Pattern_Days_Of_Week_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00049_Work_Pattern_Days_Of_Week_With_Blank);

            // Populate Main Dataset //
            sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        var drNewRow = this.dataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_Items.Newsp_HR_00047_Get_Employee_Working_Pattern_ItemsRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordIds = strRecordIDs;
                        drNewRow.WorkingPatternHeaderID = ParentRecordId;
                        drNewRow.HeaderDescription = ParentRecordDescription;
                        drNewRow.EmployeeName = ParentRecordEmployeeName;
                        drNewRow.EmployeeID = ParentEmployeeID;
                        drNewRow.HeaderStartDate = ParentRecordStartDate;
                        drNewRow.HeaderEndDate = ParentRecordEndDate;
                        drNewRow.WeekNumber = 1;
                        drNewRow.DayOfWeek = 2;
                        drNewRow.StartTime = new TimeSpan(9, 0, 0);
                        drNewRow.EndTime = new TimeSpan(17, 30, 0);
                        drNewRow.PaidForBreaks = 0;
                        drNewRow.BreakLength = 30;
                        drNewRow.CalculatedHours = (decimal)8.00;
                        drNewRow.EmployeeDepartmentWorkedID = 0;
                        this.dataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_Items.Addsp_HR_00047_Get_Employee_Working_Pattern_ItemsRow(drNewRow);
                        
                        PopulateDepartmentsWorked();
                        spHR00047GetEmployeeWorkingPatternItemsBindingSource.EndEdit();
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        var drNewRow = dataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_Items.Newsp_HR_00047_Get_Employee_Working_Pattern_ItemsRow();
                        drNewRow.Mode = strFormMode;
                        drNewRow.RecordIds = strRecordIDs;
                        dataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_Items.Addsp_HR_00047_Get_Employee_Working_Pattern_ItemsRow(drNewRow);
                        drNewRow.AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_Items, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_Items.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Work Pattern", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                // Following 3 lines allow form to kill the validation to allow it to close //
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
         }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            CreateDummyShiftButton.Enabled = true;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        WeekNumberSpinEdit.Focus();

                        HeaderDescriptionButtonEdit.Properties.ReadOnly = false;
                        HeaderDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount;
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        WeekNumberSpinEdit.Focus();

                        HeaderDescriptionButtonEdit.Properties.ReadOnly = true;
                        HeaderDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        WeekNumberSpinEdit.Focus();

                        HeaderDescriptionButtonEdit.Properties.ReadOnly = false;
                        HeaderDescriptionButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        WeekNumberSpinEdit.Focus();

                        HeaderDescriptionButtonEdit.Properties.ReadOnly = true;
                        HeaderDescriptionButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();
            SetInformationLabelVisibility();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
                CreateDummyShiftButton.Enabled = false;

            }
            ibool_ignoreValidation = false;
        }

        private void SetEditorButtons()
        {
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void frm_HR_Employee_Working_Pattern_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Employee_Working_Pattern_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.spHR00047GetEmployeeWorkingPatternItemsBindingSource.EndEdit();
            try
            {
                this.sp_HR_00047_Get_Employee_Working_Pattern_ItemsTableAdapter.Update(dataSet_HR_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)spHR00047GetEmployeeWorkingPatternItemsBindingSource.Current;
                var currentRow = (DataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_ItemsRow)currentRowView.Row;
                if (currentRow != null)
                    strNewIDs = currentRow.WorkingPatternID + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow.Mode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }

            // Notify any open instances of Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        var fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(3, Utils.enmMainGrids.WorkPattern, strNewIDs);
                    }
                    if (frmChild.Name == "frm_HR_Employee_Edit")
                    {
                        var fParentForm = (frm_HR_Employee_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(7, Utils.enmMainGrids.WorkPattern, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_Items.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp_HR_00047_Get_Employee_Working_Pattern_Items.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    SetInformationLabelVisibility();
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void HeaderDescriptionButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00047GetEmployeeWorkingPatternItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Working_Pattern_Header();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._OriginalWorkingPatternHeaderID = (string.IsNullOrEmpty(currentRow["WorkingPatternHeaderID"].ToString()) ? 0 : Convert.ToInt32(currentRow["WorkingPatternHeaderID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["WorkingPatternHeaderID"] = fChildForm._SelectedWorkingPatternID;
                    currentRow["HeaderDescription"] = fChildForm._SelectedWorkingPatternDescription;
                    currentRow["EmployeeName"] = fChildForm._SelectedEmployeeName;
                    currentRow["HeaderStartDate"] = fChildForm._SelectStartDate;
                    currentRow["HeaderEndDate"] = fChildForm._SelectEndDate;
                    currentRow["EmployeeID"] = fChildForm._SelectedEmployeeID;
                    PopulateDepartmentsWorked();
                    spHR00047GetEmployeeWorkingPatternItemsBindingSource.EndEdit();
                }
            }
        }
        private void HeaderDescriptionButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(HeaderDescriptionButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(HeaderDescriptionButtonEdit, "");
            }
        }

        private void DayOfWeekGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            var glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(DayOfWeekGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(DayOfWeekGridLookUpEdit, "");
            }
        }

        private void StartTimeTimeEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void StartTimeTimeEdit_Validating(object sender, CancelEventArgs e)
        {
            var te = (DevExpress.XtraEditors.TimeSpanEdit)sender;
            if (this.strFormMode != "blockedit" && (te.EditValue is DBNull) || (string.IsNullOrEmpty(te.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(StartTimeTimeEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(StartTimeTimeEdit, "");
            }
        }
        private void StartTimeTimeEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation) return;
            CalculateHours();
            ibool_ignoreValidation = true;
        }

        private void EndTimeTimeEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void EndTimeTimeEdit_Validating(object sender, CancelEventArgs e)
        {
            var te = (DevExpress.XtraEditors.TimeSpanEdit)sender;
            if (this.strFormMode != "blockedit" && (te.EditValue is DBNull) || (string.IsNullOrEmpty(te.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(EndTimeTimeEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(EndTimeTimeEdit, "");
            }
        }
        private void EndTimeTimeEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation) return;
            CalculateHours();
            ibool_ignoreValidation = true;
        }

        private void BreaksLengthMinsSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void BreaksLengthMinsSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation) return;
            CalculateHours();
            ibool_ignoreValidation = true;
        }

        private void PaidForBreaksCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void PaidForBreaksCheckEdit_CheckedChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation) return;
            CalculateHours();
            ibool_ignoreValidation = true;
        }

        private void DepartmentNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)spHR00047GetEmployeeWorkingPatternItemsBindingSource.Current;
                if (currentRow == null) return;

                var fChildForm = new frm_HR_Select_Department_Worked();
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "single";
                fChildForm._PassedInEmployeeIDs = (string.IsNullOrEmpty(currentRow["EmployeeID"].ToString()) ? "" : currentRow["EmployeeID"].ToString() + ",");
                fChildForm._OriginalEmployeeDeptWorkedID = (string.IsNullOrEmpty(currentRow["EmployeeDepartmentWorkedID"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeDepartmentWorkedID"]));

                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["EmployeeDepartmentWorkedID"] = fChildForm._SelectedDepartmentWorkedID;
                    currentRow["DepartmentName"] = fChildForm._SelectedDepartmentName;
                    currentRow["LocationName"] = fChildForm._LocationName;
                    currentRow["BusinessAreaName"] = fChildForm._BusinessAreaName;
                    currentRow["RegionName"] = fChildForm._RegionName;
                    spHR00047GetEmployeeWorkingPatternItemsBindingSource.EndEdit();
                }
            }
        }

        private void CalculateHours()
        {
            var startTime = TimeSpan.MaxValue;
            var endTime = TimeSpan.MinValue;
            var duration = TimeSpan.MinValue;

            if (StartTimeTimeEdit.Time != null) startTime = StartTimeTimeEdit.Time.TimeOfDay;
            if (EndTimeTimeEdit.Time != null) endTime = EndTimeTimeEdit.Time.TimeOfDay;

            if ((startTime != TimeSpan.MaxValue) && (endTime != TimeSpan.MinValue))
            {
                duration = endTime;
                duration = duration.Subtract(startTime);
            }

            // Calculate Hours //
            if (BreakLengthMinsSpinEdit.Value > 0 && !PaidForBreaksCheckEdit.Checked)
            {
                var mins = (int)BreakLengthMinsSpinEdit.Value;
                if (mins > 0)
                {
                    var breaks = new TimeSpan(0, mins, 0);
                    duration = duration.Subtract(breaks);
                }
            }
            if (startTime >= endTime) duration = duration.Add(new TimeSpan(24, 0, 0));  // Job is overnight so add 24 hours //
            if (duration.TotalHours >= 0) CalculatedHoursTimeEdit.Text = duration.TotalHours.ToString();
            spHR00047GetEmployeeWorkingPatternItemsBindingSource.EndEdit();
            SetInformationLabelVisibility();
        }

        #endregion


        private void PopulateDepartmentsWorked()
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)spHR00047GetEmployeeWorkingPatternItemsBindingSource.Current;
            if (currentRow == null) return;
            string strEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeID"].ToString()) ? "" : currentRow["EmployeeID"].ToString() + ",");
            if (string.IsNullOrWhiteSpace(strEmployeeID) || strEmployeeID == ",") return;

            SqlDataAdapter sdaDepartmentsWorked = new SqlDataAdapter();
            DataSet dsDepartmentsWorked = new DataSet("NewDataSet");
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = new SqlCommand("sp_HR_00153_Employee_Depts_Worked", SQlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@RecordIds", strEmployeeID));
            sdaDepartmentsWorked = new SqlDataAdapter(cmd);
            sdaDepartmentsWorked.Fill(dsDepartmentsWorked, "Table");
            SQlConn.Close();
            SQlConn.Dispose();

            if (dsDepartmentsWorked.Tables[0].Rows.Count == 1)
            {
                DataRow dr = dsDepartmentsWorked.Tables[0].Rows[0];
                currentRow["EmployeeDepartmentWorkedID"] = dr["EmployeeDeptWorkedID"];
                currentRow["DepartmentName"] = dr["DepartmentName"];
                currentRow["LocationName"] = dr["LocationName"];
                currentRow["BusinessAreaName"] = dr["BusinessAreaName"];
                currentRow["RegionName"] = dr["RegionName"];
            }
            else
            {
                DataRow dr = dsDepartmentsWorked.Tables[0].Rows[0];
                currentRow["EmployeeDepartmentWorkedID"] = 0;
                currentRow["DepartmentName"] = "";
                currentRow["LocationName"] = "";
                currentRow["BusinessAreaName"] = "";
                currentRow["RegionName"] = "";
            }
        }

        private void CreateDummyShiftButton_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)spHR00047GetEmployeeWorkingPatternItemsBindingSource.Current;
            if (currentRow == null) return;
            currentRow["StartTime"] = new TimeSpan(0, 0, 0);
            currentRow["EndTime"] = new TimeSpan(0, 0, 0);
            currentRow["PaidForBreaks"] = 0;
            currentRow["BreakLength"] = (decimal)1440;
            currentRow["CalculatedHours"] = (decimal)0.00;
            WeekNumberSpinEdit.Focus();
            spHR00047GetEmployeeWorkingPatternItemsBindingSource.EndEdit();
            ItemForInformationLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
        }

        private void SetInformationLabelVisibility()
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd")
            {
                ItemForInformationLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                return;
            }
            DataRowView currentRow = (DataRowView)spHR00047GetEmployeeWorkingPatternItemsBindingSource.Current;
            if (currentRow == null) return;

            string strCalculatedHours = currentRow["CalculatedHours"].ToString();
            if (string.IsNullOrWhiteSpace(strCalculatedHours) || strCalculatedHours == "0" || strCalculatedHours == "0.00")
            {
                ItemForInformationLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            else
            {
                ItemForInformationLabel.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            }
        }

 



    }
}


namespace WoodPlan5
{
    partial class frm_HR_Holiday_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Holiday_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActiveHolidayYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCurrentEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCreatedFromWeb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlAbsenceTypes = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.spHR00077AbsenceTypesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnAbsenceTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00113HolidayManagerHolidayYearsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeHolidayYearID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayYearStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHolidayYearEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBasicHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDaysHoliday = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBankHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchasedHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoursPerHolidayDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditHoursHoliday = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colMustTakeCertainHolidays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colHolidayYearDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurnameForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceAuthorisedTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceUnauthorisedTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.repositoryItemTextEditUnknownHoliday = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiShowActiveHolidayYearOnly = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.beiDepartment = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDepartment = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlDepartments = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.spHR00114DepartmentFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDepartmentFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiEmployee = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlEmployees = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.spHR00115EmployeesByDeptFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnEmployeeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiAbsenceType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditAbsenceType = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterSelected = new DevExpress.XtraBars.BarCheckItem();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.spHR00116HolidayManagerAbsencesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAbsenceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceSubTypeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditShortDate2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsUsed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHolidayUnitDescriptor1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApprovedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditLongDate2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCancelledDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeclaredDisability = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayCategoryId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsWorkRelated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComments4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurnameForename1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeForename1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApprovedByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCancelledByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEWCDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedReturnDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHalfDayEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHalfDayStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdditionalDaysPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentAbsences = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colUnitsPaidCSP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsPaidHalfPay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsPaidOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUnitsPaidUnpaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp_HR_00077_Absence_Types_List_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00077_Absence_Types_List_With_BlankTableAdapter();
            this.sp_HR_00113_Holiday_Manager_Holiday_YearsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00113_Holiday_Manager_Holiday_YearsTableAdapter();
            this.sp_HR_00114_Department_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter();
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.xtraGridBlending2 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00116_Holiday_Manager_AbsencesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00116_Holiday_Manager_AbsencesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlAbsenceTypes)).BeginInit();
            this.popupContainerControlAbsenceTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00077AbsenceTypesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00113HolidayManagerHolidayYearsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDaysHoliday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHoursHoliday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditUnknownHoliday)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).BeginInit();
            this.popupContainerControlDepartments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).BeginInit();
            this.popupContainerControlEmployees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditAbsenceType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00116HolidayManagerAbsencesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLongDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentAbsences)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1131, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 557);
            this.barDockControlBottom.Size = new System.Drawing.Size(1131, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 515);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1131, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 515);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.beiAbsenceType,
            this.beiShowActiveHolidayYearOnly,
            this.beiDepartment,
            this.beiEmployee,
            this.bciFilterSelected});
            this.barManager1.MaxItemId = 34;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditAbsenceType,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly,
            this.repositoryItemPopupContainerEditDepartment,
            this.repositoryItemPopupContainerEditEmployee});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActiveHolidayYear
            // 
            this.colActiveHolidayYear.Caption = "Current Holiday Year";
            this.colActiveHolidayYear.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActiveHolidayYear.FieldName = "ActiveHolidayYear";
            this.colActiveHolidayYear.Name = "colActiveHolidayYear";
            this.colActiveHolidayYear.OptionsColumn.AllowEdit = false;
            this.colActiveHolidayYear.OptionsColumn.AllowFocus = false;
            this.colActiveHolidayYear.OptionsColumn.ReadOnly = true;
            this.colActiveHolidayYear.Visible = true;
            this.colActiveHolidayYear.VisibleIndex = 5;
            this.colActiveHolidayYear.Width = 121;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colCurrentEmployee
            // 
            this.colCurrentEmployee.Caption = "Current Employee";
            this.colCurrentEmployee.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCurrentEmployee.FieldName = "CurrentEmployee";
            this.colCurrentEmployee.Name = "colCurrentEmployee";
            this.colCurrentEmployee.OptionsColumn.AllowEdit = false;
            this.colCurrentEmployee.OptionsColumn.AllowFocus = false;
            this.colCurrentEmployee.OptionsColumn.ReadOnly = true;
            this.colCurrentEmployee.Visible = true;
            this.colCurrentEmployee.VisibleIndex = 4;
            this.colCurrentEmployee.Width = 107;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colCreatedFromWeb
            // 
            this.colCreatedFromWeb.Caption = "Created From Web";
            this.colCreatedFromWeb.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colCreatedFromWeb.FieldName = "CreatedFromWeb";
            this.colCreatedFromWeb.Name = "colCreatedFromWeb";
            this.colCreatedFromWeb.OptionsColumn.AllowEdit = false;
            this.colCreatedFromWeb.OptionsColumn.AllowFocus = false;
            this.colCreatedFromWeb.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.List;
            this.colCreatedFromWeb.Visible = true;
            this.colCreatedFromWeb.VisibleIndex = 2;
            this.colCreatedFromWeb.Width = 110;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ID";
            this.gridColumn3.FieldName = "ID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 53;
            // 
            // popupContainerControlAbsenceTypes
            // 
            this.popupContainerControlAbsenceTypes.Controls.Add(this.gridControl3);
            this.popupContainerControlAbsenceTypes.Controls.Add(this.btnAbsenceTypeFilterOK);
            this.popupContainerControlAbsenceTypes.Location = new System.Drawing.Point(483, 96);
            this.popupContainerControlAbsenceTypes.Name = "popupContainerControlAbsenceTypes";
            this.popupContainerControlAbsenceTypes.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlAbsenceTypes.TabIndex = 1;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.spHR00077AbsenceTypesListWithBlankBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(2, 2);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(230, 141);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // spHR00077AbsenceTypesListWithBlankBindingSource
            // 
            this.spHR00077AbsenceTypesListWithBlankBindingSource.DataMember = "sp_HR_00077_Absence_Types_List_With_Blank";
            this.spHR00077AbsenceTypesListWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.colDescription,
            this.colOrder});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Absence Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 173;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // btnAbsenceTypeFilterOK
            // 
            this.btnAbsenceTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAbsenceTypeFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnAbsenceTypeFilterOK.Name = "btnAbsenceTypeFilterOK";
            this.btnAbsenceTypeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnAbsenceTypeFilterOK.TabIndex = 2;
            this.btnAbsenceTypeFilterOK.Text = "OK";
            this.btnAbsenceTypeFilterOK.Click += new System.EventHandler(this.btnAbsenceTypeFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00113HolidayManagerHolidayYearsBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Show Active Employees with No Active Holiday Year", "missing")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDate,
            this.repositoryItemTextEditDaysHoliday,
            this.repositoryItemTextEditHoursHoliday,
            this.repositoryItemTextEditUnknownHoliday});
            this.gridControl1.Size = new System.Drawing.Size(1106, 312);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00113HolidayManagerHolidayYearsBindingSource
            // 
            this.spHR00113HolidayManagerHolidayYearsBindingSource.DataMember = "sp_HR_00113_Holiday_Manager_Holiday_Years";
            this.spHR00113HolidayManagerHolidayYearsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("find_16x16.png", "images/find/find_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/find/find_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "find_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "linked_documents_16_16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeHolidayYearID,
            this.colEmployeeContractID,
            this.colHolidayYearStartDate,
            this.colHolidayYearEndDate,
            this.colActualStartDate,
            this.colBasicHolidayEntitlement,
            this.colBankHolidayEntitlement,
            this.colPurchasedHolidayEntitlement,
            this.colHolidayUnitDescriptorID,
            this.colHoursPerHolidayDay,
            this.colMustTakeCertainHolidays,
            this.colRemarks,
            this.colHolidayYearDescription,
            this.colEmployeeSurnameForename,
            this.colEmployeeSurname,
            this.colEmployeeForename,
            this.colEmployeeNumber,
            this.colActiveHolidayYear,
            this.colHolidayUnitDescriptor,
            this.colHolidaysTaken,
            this.colBankHolidaysTaken,
            this.colAbsenceAuthorisedTaken,
            this.colAbsenceUnauthorisedTaken,
            this.colTotalHolidayEntitlement,
            this.colHolidaysRemaining,
            this.colBankHolidaysRemaining,
            this.colTotalHolidaysTaken,
            this.colTotalHolidaysRemaining,
            this.colSelected});
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colActiveHolidayYear;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeForename, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colEmployeeHolidayYearID
            // 
            this.colEmployeeHolidayYearID.Caption = "Holiday Year ID";
            this.colEmployeeHolidayYearID.FieldName = "EmployeeHolidayYearID";
            this.colEmployeeHolidayYearID.Name = "colEmployeeHolidayYearID";
            this.colEmployeeHolidayYearID.OptionsColumn.AllowEdit = false;
            this.colEmployeeHolidayYearID.OptionsColumn.AllowFocus = false;
            this.colEmployeeHolidayYearID.OptionsColumn.ReadOnly = true;
            this.colEmployeeHolidayYearID.Width = 95;
            // 
            // colEmployeeContractID
            // 
            this.colEmployeeContractID.Caption = "Employee ID";
            this.colEmployeeContractID.FieldName = "EmployeeID";
            this.colEmployeeContractID.Name = "colEmployeeContractID";
            this.colEmployeeContractID.OptionsColumn.AllowEdit = false;
            this.colEmployeeContractID.OptionsColumn.AllowFocus = false;
            this.colEmployeeContractID.OptionsColumn.ReadOnly = true;
            this.colEmployeeContractID.Width = 77;
            // 
            // colHolidayYearStartDate
            // 
            this.colHolidayYearStartDate.Caption = "Year Start Date";
            this.colHolidayYearStartDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colHolidayYearStartDate.FieldName = "HolidayYearStartDate";
            this.colHolidayYearStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHolidayYearStartDate.Name = "colHolidayYearStartDate";
            this.colHolidayYearStartDate.OptionsColumn.AllowEdit = false;
            this.colHolidayYearStartDate.OptionsColumn.AllowFocus = false;
            this.colHolidayYearStartDate.OptionsColumn.ReadOnly = true;
            this.colHolidayYearStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHolidayYearStartDate.Visible = true;
            this.colHolidayYearStartDate.VisibleIndex = 3;
            this.colHolidayYearStartDate.Width = 96;
            // 
            // repositoryItemTextEditDate
            // 
            this.repositoryItemTextEditDate.AutoHeight = false;
            this.repositoryItemTextEditDate.Mask.EditMask = "d";
            this.repositoryItemTextEditDate.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDate.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDate.Name = "repositoryItemTextEditDate";
            // 
            // colHolidayYearEndDate
            // 
            this.colHolidayYearEndDate.Caption = "Year End Date";
            this.colHolidayYearEndDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colHolidayYearEndDate.FieldName = "HolidayYearEndDate";
            this.colHolidayYearEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHolidayYearEndDate.Name = "colHolidayYearEndDate";
            this.colHolidayYearEndDate.OptionsColumn.AllowEdit = false;
            this.colHolidayYearEndDate.OptionsColumn.AllowFocus = false;
            this.colHolidayYearEndDate.OptionsColumn.ReadOnly = true;
            this.colHolidayYearEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHolidayYearEndDate.Visible = true;
            this.colHolidayYearEndDate.VisibleIndex = 4;
            this.colHolidayYearEndDate.Width = 90;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start Date";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDate;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 6;
            this.colActualStartDate.Width = 104;
            // 
            // colBasicHolidayEntitlement
            // 
            this.colBasicHolidayEntitlement.Caption = "Holiday Entitlement";
            this.colBasicHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colBasicHolidayEntitlement.FieldName = "BasicHolidayEntitlement";
            this.colBasicHolidayEntitlement.Name = "colBasicHolidayEntitlement";
            this.colBasicHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colBasicHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colBasicHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colBasicHolidayEntitlement.Visible = true;
            this.colBasicHolidayEntitlement.VisibleIndex = 7;
            this.colBasicHolidayEntitlement.Width = 113;
            // 
            // repositoryItemTextEditDaysHoliday
            // 
            this.repositoryItemTextEditDaysHoliday.AutoHeight = false;
            this.repositoryItemTextEditDaysHoliday.Mask.EditMask = "#####0.00 Days";
            this.repositoryItemTextEditDaysHoliday.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditDaysHoliday.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDaysHoliday.Name = "repositoryItemTextEditDaysHoliday";
            // 
            // colBankHolidayEntitlement
            // 
            this.colBankHolidayEntitlement.Caption = "Bank Holiday Entitlement";
            this.colBankHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colBankHolidayEntitlement.FieldName = "BankHolidayEntitlement";
            this.colBankHolidayEntitlement.Name = "colBankHolidayEntitlement";
            this.colBankHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colBankHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colBankHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colBankHolidayEntitlement.Visible = true;
            this.colBankHolidayEntitlement.VisibleIndex = 8;
            this.colBankHolidayEntitlement.Width = 139;
            // 
            // colPurchasedHolidayEntitlement
            // 
            this.colPurchasedHolidayEntitlement.Caption = "Purchased Holiday Entitlement";
            this.colPurchasedHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colPurchasedHolidayEntitlement.FieldName = "PurchasedHolidayEntitlement";
            this.colPurchasedHolidayEntitlement.Name = "colPurchasedHolidayEntitlement";
            this.colPurchasedHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colPurchasedHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colPurchasedHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colPurchasedHolidayEntitlement.Visible = true;
            this.colPurchasedHolidayEntitlement.VisibleIndex = 9;
            this.colPurchasedHolidayEntitlement.Width = 166;
            // 
            // colHolidayUnitDescriptorID
            // 
            this.colHolidayUnitDescriptorID.Caption = "Holiday Unit Descriptor ID";
            this.colHolidayUnitDescriptorID.FieldName = "HolidayUnitDescriptorID";
            this.colHolidayUnitDescriptorID.Name = "colHolidayUnitDescriptorID";
            this.colHolidayUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptorID.Width = 144;
            // 
            // colHoursPerHolidayDay
            // 
            this.colHoursPerHolidayDay.Caption = "Hours Per Holiday Day";
            this.colHoursPerHolidayDay.ColumnEdit = this.repositoryItemTextEditHoursHoliday;
            this.colHoursPerHolidayDay.FieldName = "HoursPerHolidayDay";
            this.colHoursPerHolidayDay.Name = "colHoursPerHolidayDay";
            this.colHoursPerHolidayDay.OptionsColumn.AllowEdit = false;
            this.colHoursPerHolidayDay.OptionsColumn.AllowFocus = false;
            this.colHoursPerHolidayDay.OptionsColumn.ReadOnly = true;
            this.colHoursPerHolidayDay.Visible = true;
            this.colHoursPerHolidayDay.VisibleIndex = 19;
            this.colHoursPerHolidayDay.Width = 128;
            // 
            // repositoryItemTextEditHoursHoliday
            // 
            this.repositoryItemTextEditHoursHoliday.AutoHeight = false;
            this.repositoryItemTextEditHoursHoliday.Mask.EditMask = "#####0.00 Hours";
            this.repositoryItemTextEditHoursHoliday.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditHoursHoliday.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditHoursHoliday.Name = "repositoryItemTextEditHoursHoliday";
            // 
            // colMustTakeCertainHolidays
            // 
            this.colMustTakeCertainHolidays.Caption = "Must Take Default Holidays";
            this.colMustTakeCertainHolidays.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colMustTakeCertainHolidays.FieldName = "MustTakeCertainHolidays";
            this.colMustTakeCertainHolidays.Name = "colMustTakeCertainHolidays";
            this.colMustTakeCertainHolidays.OptionsColumn.AllowEdit = false;
            this.colMustTakeCertainHolidays.OptionsColumn.AllowFocus = false;
            this.colMustTakeCertainHolidays.OptionsColumn.ReadOnly = true;
            this.colMustTakeCertainHolidays.Visible = true;
            this.colMustTakeCertainHolidays.VisibleIndex = 20;
            this.colMustTakeCertainHolidays.Width = 151;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 21;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colHolidayYearDescription
            // 
            this.colHolidayYearDescription.Caption = "Holiday Year Description";
            this.colHolidayYearDescription.FieldName = "HolidayYearDescription";
            this.colHolidayYearDescription.Name = "colHolidayYearDescription";
            this.colHolidayYearDescription.OptionsColumn.AllowEdit = false;
            this.colHolidayYearDescription.OptionsColumn.AllowFocus = false;
            this.colHolidayYearDescription.OptionsColumn.ReadOnly = true;
            this.colHolidayYearDescription.Width = 137;
            // 
            // colEmployeeSurnameForename
            // 
            this.colEmployeeSurnameForename.Caption = "Employee Details";
            this.colEmployeeSurnameForename.FieldName = "EmployeeSurnameForename";
            this.colEmployeeSurnameForename.Name = "colEmployeeSurnameForename";
            this.colEmployeeSurnameForename.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurnameForename.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurnameForename.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurnameForename.Width = 102;
            // 
            // colEmployeeSurname
            // 
            this.colEmployeeSurname.Caption = "Employee Surname";
            this.colEmployeeSurname.FieldName = "EmployeeSurname";
            this.colEmployeeSurname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeSurname.Name = "colEmployeeSurname";
            this.colEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname.Visible = true;
            this.colEmployeeSurname.VisibleIndex = 0;
            this.colEmployeeSurname.Width = 125;
            // 
            // colEmployeeForename
            // 
            this.colEmployeeForename.Caption = "Employee Forename";
            this.colEmployeeForename.FieldName = "EmployeeForename";
            this.colEmployeeForename.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeForename.Name = "colEmployeeForename";
            this.colEmployeeForename.OptionsColumn.AllowEdit = false;
            this.colEmployeeForename.OptionsColumn.AllowFocus = false;
            this.colEmployeeForename.OptionsColumn.ReadOnly = true;
            this.colEmployeeForename.Visible = true;
            this.colEmployeeForename.VisibleIndex = 1;
            this.colEmployeeForename.Width = 131;
            // 
            // colEmployeeNumber
            // 
            this.colEmployeeNumber.Caption = "Employee #";
            this.colEmployeeNumber.FieldName = "EmployeeNumber";
            this.colEmployeeNumber.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeNumber.Name = "colEmployeeNumber";
            this.colEmployeeNumber.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber.Visible = true;
            this.colEmployeeNumber.VisibleIndex = 2;
            this.colEmployeeNumber.Width = 78;
            // 
            // colHolidayUnitDescriptor
            // 
            this.colHolidayUnitDescriptor.Caption = "Holiday Unit Descriptor";
            this.colHolidayUnitDescriptor.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor.Name = "colHolidayUnitDescriptor";
            this.colHolidayUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor.Width = 130;
            // 
            // colHolidaysTaken
            // 
            this.colHolidaysTaken.Caption = "Holidays Used";
            this.colHolidaysTaken.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colHolidaysTaken.FieldName = "HolidaysTaken";
            this.colHolidaysTaken.Name = "colHolidaysTaken";
            this.colHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colHolidaysTaken.Visible = true;
            this.colHolidaysTaken.VisibleIndex = 11;
            this.colHolidaysTaken.Width = 88;
            // 
            // colBankHolidaysTaken
            // 
            this.colBankHolidaysTaken.Caption = "Bank Holidays Used";
            this.colBankHolidaysTaken.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colBankHolidaysTaken.FieldName = "BankHolidaysTaken";
            this.colBankHolidaysTaken.Name = "colBankHolidaysTaken";
            this.colBankHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colBankHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colBankHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colBankHolidaysTaken.Visible = true;
            this.colBankHolidaysTaken.VisibleIndex = 12;
            this.colBankHolidaysTaken.Width = 114;
            // 
            // colAbsenceAuthorisedTaken
            // 
            this.colAbsenceAuthorisedTaken.Caption = "Authorised Holidays Used";
            this.colAbsenceAuthorisedTaken.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colAbsenceAuthorisedTaken.FieldName = "AbsenceAuthorisedTaken";
            this.colAbsenceAuthorisedTaken.Name = "colAbsenceAuthorisedTaken";
            this.colAbsenceAuthorisedTaken.OptionsColumn.AllowEdit = false;
            this.colAbsenceAuthorisedTaken.OptionsColumn.AllowFocus = false;
            this.colAbsenceAuthorisedTaken.OptionsColumn.ReadOnly = true;
            this.colAbsenceAuthorisedTaken.Visible = true;
            this.colAbsenceAuthorisedTaken.VisibleIndex = 13;
            this.colAbsenceAuthorisedTaken.Width = 143;
            // 
            // colAbsenceUnauthorisedTaken
            // 
            this.colAbsenceUnauthorisedTaken.Caption = "Unauthorised Holidays Used";
            this.colAbsenceUnauthorisedTaken.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colAbsenceUnauthorisedTaken.FieldName = "AbsenceUnauthorisedTaken";
            this.colAbsenceUnauthorisedTaken.Name = "colAbsenceUnauthorisedTaken";
            this.colAbsenceUnauthorisedTaken.OptionsColumn.AllowEdit = false;
            this.colAbsenceUnauthorisedTaken.OptionsColumn.AllowFocus = false;
            this.colAbsenceUnauthorisedTaken.OptionsColumn.ReadOnly = true;
            this.colAbsenceUnauthorisedTaken.Visible = true;
            this.colAbsenceUnauthorisedTaken.VisibleIndex = 14;
            this.colAbsenceUnauthorisedTaken.Width = 155;
            // 
            // colTotalHolidayEntitlement
            // 
            this.colTotalHolidayEntitlement.Caption = "Total Holiday Entitlement";
            this.colTotalHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colTotalHolidayEntitlement.FieldName = "TotalHolidayEntitlement";
            this.colTotalHolidayEntitlement.Name = "colTotalHolidayEntitlement";
            this.colTotalHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colTotalHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colTotalHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colTotalHolidayEntitlement.Visible = true;
            this.colTotalHolidayEntitlement.VisibleIndex = 10;
            this.colTotalHolidayEntitlement.Width = 140;
            // 
            // colHolidaysRemaining
            // 
            this.colHolidaysRemaining.Caption = "Holidays Left";
            this.colHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colHolidaysRemaining.FieldName = "HolidaysRemaining";
            this.colHolidaysRemaining.Name = "colHolidaysRemaining";
            this.colHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colHolidaysRemaining.Visible = true;
            this.colHolidaysRemaining.VisibleIndex = 16;
            this.colHolidaysRemaining.Width = 83;
            // 
            // colBankHolidaysRemaining
            // 
            this.colBankHolidaysRemaining.Caption = "Bank Holidays Left";
            this.colBankHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colBankHolidaysRemaining.FieldName = "BankHolidaysRemaining";
            this.colBankHolidaysRemaining.Name = "colBankHolidaysRemaining";
            this.colBankHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colBankHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colBankHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colBankHolidaysRemaining.Visible = true;
            this.colBankHolidaysRemaining.VisibleIndex = 17;
            this.colBankHolidaysRemaining.Width = 109;
            // 
            // colTotalHolidaysTaken
            // 
            this.colTotalHolidaysTaken.Caption = "Total Holidays Used";
            this.colTotalHolidaysTaken.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colTotalHolidaysTaken.FieldName = "TotalHolidaysTaken";
            this.colTotalHolidaysTaken.Name = "colTotalHolidaysTaken";
            this.colTotalHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colTotalHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colTotalHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colTotalHolidaysTaken.Visible = true;
            this.colTotalHolidaysTaken.VisibleIndex = 15;
            this.colTotalHolidaysTaken.Width = 115;
            // 
            // colTotalHolidaysRemaining
            // 
            this.colTotalHolidaysRemaining.Caption = "Total Holidays Left";
            this.colTotalHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditDaysHoliday;
            this.colTotalHolidaysRemaining.FieldName = "TotalHolidaysRemaining";
            this.colTotalHolidaysRemaining.Name = "colTotalHolidaysRemaining";
            this.colTotalHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colTotalHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colTotalHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colTotalHolidaysRemaining.Visible = true;
            this.colTotalHolidaysRemaining.VisibleIndex = 18;
            this.colTotalHolidaysRemaining.Width = 110;
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // repositoryItemTextEditUnknownHoliday
            // 
            this.repositoryItemTextEditUnknownHoliday.AutoHeight = false;
            this.repositoryItemTextEditUnknownHoliday.Mask.EditMask = "#####0.00 ??";
            this.repositoryItemTextEditUnknownHoliday.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditUnknownHoliday.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditUnknownHoliday.Name = "repositoryItemTextEditUnknownHoliday";
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiShowActiveHolidayYearOnly, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDepartment, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiEmployee),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiAbsenceType, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterSelected, true)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // beiShowActiveHolidayYearOnly
            // 
            this.beiShowActiveHolidayYearOnly.Caption = "Active Year Only:";
            this.beiShowActiveHolidayYearOnly.Edit = this.repositoryItemCheckEditShowActiveHoilidayYearOnly;
            this.beiShowActiveHolidayYearOnly.EditValue = 1;
            this.beiShowActiveHolidayYearOnly.EditWidth = 20;
            this.beiShowActiveHolidayYearOnly.Id = 30;
            this.beiShowActiveHolidayYearOnly.Name = "beiShowActiveHolidayYearOnly";
            this.beiShowActiveHolidayYearOnly.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Active Year Only - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Tick me to load only the active holiday year data. \r\n\r\nUntick me to load all data" +
    ".";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiShowActiveHolidayYearOnly.SuperTip = superToolTip1;
            // 
            // repositoryItemCheckEditShowActiveHoilidayYearOnly
            // 
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Caption = "";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Name = "repositoryItemCheckEditShowActiveHoilidayYearOnly";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueUnchecked = 0;
            // 
            // beiDepartment
            // 
            this.beiDepartment.Caption = "Department(s):";
            this.beiDepartment.Edit = this.repositoryItemPopupContainerEditDepartment;
            this.beiDepartment.EditValue = "No Department Filter";
            this.beiDepartment.EditWidth = 160;
            this.beiDepartment.Id = 31;
            this.beiDepartment.Name = "beiDepartment";
            this.beiDepartment.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem2.Text = "Department(s) - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Select one or more department from me to view just those departments. \r\n\r\nSelect " +
    "nothing from me to view all departments.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.beiDepartment.SuperTip = superToolTip2;
            // 
            // repositoryItemPopupContainerEditDepartment
            // 
            this.repositoryItemPopupContainerEditDepartment.AutoHeight = false;
            this.repositoryItemPopupContainerEditDepartment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDepartment.Name = "repositoryItemPopupContainerEditDepartment";
            this.repositoryItemPopupContainerEditDepartment.PopupControl = this.popupContainerControlDepartments;
            this.repositoryItemPopupContainerEditDepartment.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDepartment_QueryResultValue);
            // 
            // popupContainerControlDepartments
            // 
            this.popupContainerControlDepartments.Controls.Add(this.gridControl2);
            this.popupContainerControlDepartments.Controls.Add(this.btnDepartmentFilterOK);
            this.popupContainerControlDepartments.Location = new System.Drawing.Point(3, 96);
            this.popupContainerControlDepartments.Name = "popupContainerControlDepartments";
            this.popupContainerControlDepartments.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlDepartments.TabIndex = 2;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.spHR00114DepartmentFilterListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(230, 141);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00114DepartmentFilterListBindingSource
            // 
            this.spHR00114DepartmentFilterListBindingSource.DataMember = "sp_HR_00114_Department_Filter_List";
            this.spHR00114DepartmentFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepartmentID,
            this.colBusinessAreaID1,
            this.colDepartmentName,
            this.gridColumn2,
            this.colBusinessAreaName1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBusinessAreaName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartmentName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.Caption = "Department ID";
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.Width = 92;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.Width = 102;
            // 
            // colDepartmentName
            // 
            this.colDepartmentName.Caption = "Department Name";
            this.colDepartmentName.FieldName = "DepartmentName";
            this.colDepartmentName.Name = "colDepartmentName";
            this.colDepartmentName.OptionsColumn.AllowEdit = false;
            this.colDepartmentName.OptionsColumn.AllowFocus = false;
            this.colDepartmentName.OptionsColumn.ReadOnly = true;
            this.colDepartmentName.Visible = true;
            this.colDepartmentName.VisibleIndex = 0;
            this.colDepartmentName.Width = 224;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Remarks";
            this.gridColumn2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn2.FieldName = "Remarks";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 175;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area Name";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.Width = 224;
            // 
            // btnDepartmentFilterOK
            // 
            this.btnDepartmentFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDepartmentFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnDepartmentFilterOK.Name = "btnDepartmentFilterOK";
            this.btnDepartmentFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDepartmentFilterOK.TabIndex = 2;
            this.btnDepartmentFilterOK.Text = "OK";
            this.btnDepartmentFilterOK.Click += new System.EventHandler(this.btnDepartmentFilterOK_Click);
            // 
            // beiEmployee
            // 
            this.beiEmployee.Caption = "Employee(s):";
            this.beiEmployee.Edit = this.repositoryItemPopupContainerEditEmployee;
            this.beiEmployee.EditValue = "No Employee Filter";
            this.beiEmployee.EditWidth = 158;
            this.beiEmployee.Id = 32;
            this.beiEmployee.Name = "beiEmployee";
            this.beiEmployee.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            toolTipTitleItem3.Text = "Employee(s) - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Select one or more employees from me to view just those employees.\r\n\r\nSelect noth" +
    "ing from me to view all employees.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.beiEmployee.SuperTip = superToolTip3;
            // 
            // repositoryItemPopupContainerEditEmployee
            // 
            this.repositoryItemPopupContainerEditEmployee.AutoHeight = false;
            this.repositoryItemPopupContainerEditEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditEmployee.Name = "repositoryItemPopupContainerEditEmployee";
            this.repositoryItemPopupContainerEditEmployee.PopupControl = this.popupContainerControlEmployees;
            this.repositoryItemPopupContainerEditEmployee.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditEmployee_QueryResultValue);
            // 
            // popupContainerControlEmployees
            // 
            this.popupContainerControlEmployees.Controls.Add(this.gridControl4);
            this.popupContainerControlEmployees.Controls.Add(this.btnEmployeeFilterOK);
            this.popupContainerControlEmployees.Location = new System.Drawing.Point(243, 96);
            this.popupContainerControlEmployees.Name = "popupContainerControlEmployees";
            this.popupContainerControlEmployees.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlEmployees.TabIndex = 5;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.spHR00115EmployeesByDeptFilterListBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(2, 2);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(230, 141);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // spHR00115EmployeesByDeptFilterListBindingSource
            // 
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataMember = "sp_HR_00115_Employees_By_Dept_Filter_List";
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeID,
            this.colEmployeeNumber1,
            this.colTitle,
            this.colFirstname,
            this.colSurname,
            this.colCurrentEmployee,
            this.colRemarks1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colCurrentEmployee;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFirstname, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colEmployeeID
            // 
            this.colEmployeeID.Caption = "Employee ID";
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.Name = "colEmployeeID";
            this.colEmployeeID.OptionsColumn.AllowEdit = false;
            this.colEmployeeID.OptionsColumn.AllowFocus = false;
            this.colEmployeeID.OptionsColumn.ReadOnly = true;
            this.colEmployeeID.Width = 81;
            // 
            // colEmployeeNumber1
            // 
            this.colEmployeeNumber1.Caption = "Employee #";
            this.colEmployeeNumber1.FieldName = "EmployeeNumber";
            this.colEmployeeNumber1.Name = "colEmployeeNumber1";
            this.colEmployeeNumber1.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber1.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber1.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber1.Visible = true;
            this.colEmployeeNumber1.VisibleIndex = 2;
            this.colEmployeeNumber1.Width = 91;
            // 
            // colTitle
            // 
            this.colTitle.Caption = "Title";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 3;
            this.colTitle.Width = 59;
            // 
            // colFirstname
            // 
            this.colFirstname.Caption = "Forename";
            this.colFirstname.FieldName = "Firstname";
            this.colFirstname.Name = "colFirstname";
            this.colFirstname.OptionsColumn.AllowEdit = false;
            this.colFirstname.OptionsColumn.AllowFocus = false;
            this.colFirstname.OptionsColumn.ReadOnly = true;
            this.colFirstname.Visible = true;
            this.colFirstname.VisibleIndex = 1;
            this.colFirstname.Width = 110;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 0;
            this.colSurname.Width = 126;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // btnEmployeeFilterOK
            // 
            this.btnEmployeeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEmployeeFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnEmployeeFilterOK.Name = "btnEmployeeFilterOK";
            this.btnEmployeeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnEmployeeFilterOK.TabIndex = 2;
            this.btnEmployeeFilterOK.Text = "OK";
            this.btnEmployeeFilterOK.Click += new System.EventHandler(this.btnEmployeeFilterOK_Click);
            // 
            // beiAbsenceType
            // 
            this.beiAbsenceType.Caption = "Absence Type(s):";
            this.beiAbsenceType.Edit = this.repositoryItemPopupContainerEditAbsenceType;
            this.beiAbsenceType.EditValue = "No Absence Type Filter";
            this.beiAbsenceType.EditWidth = 175;
            this.beiAbsenceType.Id = 28;
            this.beiAbsenceType.Name = "beiAbsenceType";
            this.beiAbsenceType.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image6")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image7")));
            toolTipTitleItem4.Text = "Absence Type(s) - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "Select one or more types of absence from me to view just those types of data. \r\n\r" +
    "\nSelect nothing from me to view all absence records.";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.beiAbsenceType.SuperTip = superToolTip4;
            // 
            // repositoryItemPopupContainerEditAbsenceType
            // 
            this.repositoryItemPopupContainerEditAbsenceType.AutoHeight = false;
            this.repositoryItemPopupContainerEditAbsenceType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditAbsenceType.Name = "repositoryItemPopupContainerEditAbsenceType";
            this.repositoryItemPopupContainerEditAbsenceType.PopupControl = this.popupContainerControlAbsenceTypes;
            this.repositoryItemPopupContainerEditAbsenceType.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditAbsenceType_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.ImageOptions.Image = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bciFilterSelected
            // 
            this.bciFilterSelected.Caption = "Selected";
            this.bciFilterSelected.Id = 33;
            this.bciFilterSelected.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.ImageOptions.Image")));
            this.bciFilterSelected.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.ImageOptions.LargeImage")));
            this.bciFilterSelected.Name = "bciFilterSelected";
            this.bciFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image8")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image9")));
            toolTipTitleItem5.Text = "Filter Selected - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = resources.GetString("toolTipItem5.Text");
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bciFilterSelected.SuperTip = superToolTip5;
            this.bciFilterSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterSelected_CheckedChanged);
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 42);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Holiday Years ";
            this.splitContainerControl1.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl1.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridSplitContainer2);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Holidays \\ Absences ";
            this.splitContainerControl1.Size = new System.Drawing.Size(1131, 515);
            this.splitContainerControl1.SplitterPosition = 194;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlEmployees);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDepartments);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlAbsenceTypes);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1106, 312);
            this.gridSplitContainer1.TabIndex = 2;
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl8;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl8);
            this.gridSplitContainer2.Size = new System.Drawing.Size(1106, 191);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.spHR00116HolidayManagerAbsencesBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Ad New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Linked Documents", "linked_document")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEditShortDate2,
            this.repositoryItemTextEdit2DP2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemTextEditLongDate2,
            this.repositoryItemTextEdit1,
            this.repositoryItemHyperLinkEditLinkedDocumentAbsences});
            this.gridControl8.Size = new System.Drawing.Size(1106, 191);
            this.gridControl8.TabIndex = 3;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // spHR00116HolidayManagerAbsencesBindingSource
            // 
            this.spHR00116HolidayManagerAbsencesBindingSource.DataMember = "sp_HR_00116_Holiday_Manager_Absences";
            this.spHR00116HolidayManagerAbsencesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAbsenceID,
            this.gridColumn1,
            this.colRecordTypeId,
            this.colAbsenceTypeId,
            this.colAbsenceSubTypeId,
            this.colStartDate,
            this.colEndDate,
            this.colUnitsUsed,
            this.colHolidayUnitDescriptor1,
            this.colApprovedDate,
            this.colCancelledDate,
            this.colDeclaredDisability,
            this.colPayCategoryId,
            this.colIsWorkRelated,
            this.colComments4,
            this.colRecordType,
            this.colAbsenceType,
            this.colStatus,
            this.gridColumn4,
            this.colEmployeeSurnameForename1,
            this.colEmployeeSurname1,
            this.colEmployeeForename1,
            this.colEmployeeNumber3,
            this.colApprovedByPerson,
            this.colCancelledByPerson,
            this.colAbsenceSubType,
            this.gridColumn5,
            this.colContractNumber1,
            this.colEWCDate,
            this.colExpectedReturnDate,
            this.colHalfDayEnd,
            this.colHalfDayStart,
            this.colAdditionalDaysPaid,
            this.colLinkedDocumentCount,
            this.colUnitsPaidCSP,
            this.colUnitsPaidHalfPay,
            this.colUnitsPaidOther,
            this.colUnitsPaidUnpaid,
            this.colAbsenceComment,
            this.colCreatedFromWeb});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colCreatedFromWeb;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            formatConditionRuleValue1.Appearance.Options.UseBackColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 1;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView8.FormatRules.Add(gridFormatRule1);
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 2;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsLayout.StoreFormatRules = true;
            this.gridView8.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.BestFitMaxRowCount = 10;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurnameForename1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colStartDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView8.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView8_CustomDrawCell);
            this.gridView8.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView8_CustomRowCellEdit);
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView8_ShowingEditor);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.DoubleClick += new System.EventHandler(this.gridView8_DoubleClick);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colAbsenceID
            // 
            this.colAbsenceID.Caption = "Absence ID";
            this.colAbsenceID.FieldName = "AbsenceID";
            this.colAbsenceID.Name = "colAbsenceID";
            this.colAbsenceID.OptionsColumn.AllowEdit = false;
            this.colAbsenceID.OptionsColumn.AllowFocus = false;
            this.colAbsenceID.OptionsColumn.ReadOnly = true;
            this.colAbsenceID.Width = 76;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Holiday Year ID";
            this.gridColumn1.FieldName = "EmployeeHolidayYearId";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 95;
            // 
            // colRecordTypeId
            // 
            this.colRecordTypeId.Caption = "Record Type ID";
            this.colRecordTypeId.FieldName = "RecordTypeId";
            this.colRecordTypeId.Name = "colRecordTypeId";
            this.colRecordTypeId.OptionsColumn.AllowEdit = false;
            this.colRecordTypeId.OptionsColumn.AllowFocus = false;
            this.colRecordTypeId.OptionsColumn.ReadOnly = true;
            this.colRecordTypeId.Width = 96;
            // 
            // colAbsenceTypeId
            // 
            this.colAbsenceTypeId.Caption = "Absence Type ID";
            this.colAbsenceTypeId.FieldName = "AbsenceTypeId";
            this.colAbsenceTypeId.Name = "colAbsenceTypeId";
            this.colAbsenceTypeId.OptionsColumn.AllowEdit = false;
            this.colAbsenceTypeId.OptionsColumn.AllowFocus = false;
            this.colAbsenceTypeId.OptionsColumn.ReadOnly = true;
            this.colAbsenceTypeId.Width = 103;
            // 
            // colAbsenceSubTypeId
            // 
            this.colAbsenceSubTypeId.Caption = "Absence Sub-Type ID";
            this.colAbsenceSubTypeId.FieldName = "AbsenceSubTypeId";
            this.colAbsenceSubTypeId.Name = "colAbsenceSubTypeId";
            this.colAbsenceSubTypeId.OptionsColumn.AllowEdit = false;
            this.colAbsenceSubTypeId.OptionsColumn.AllowFocus = false;
            this.colAbsenceSubTypeId.OptionsColumn.ReadOnly = true;
            this.colAbsenceSubTypeId.Width = 125;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditShortDate2;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 0;
            this.colStartDate.Width = 110;
            // 
            // repositoryItemTextEditShortDate2
            // 
            this.repositoryItemTextEditShortDate2.AutoHeight = false;
            this.repositoryItemTextEditShortDate2.Mask.EditMask = "d";
            this.repositoryItemTextEditShortDate2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditShortDate2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditShortDate2.Name = "repositoryItemTextEditShortDate2";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditShortDate2;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 1;
            this.colEndDate.Width = 88;
            // 
            // colUnitsUsed
            // 
            this.colUnitsUsed.Caption = "Units Used";
            this.colUnitsUsed.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colUnitsUsed.FieldName = "UnitsUsed";
            this.colUnitsUsed.Name = "colUnitsUsed";
            this.colUnitsUsed.OptionsColumn.AllowEdit = false;
            this.colUnitsUsed.OptionsColumn.AllowFocus = false;
            this.colUnitsUsed.OptionsColumn.ReadOnly = true;
            this.colUnitsUsed.Visible = true;
            this.colUnitsUsed.VisibleIndex = 6;
            // 
            // repositoryItemTextEdit2DP2
            // 
            this.repositoryItemTextEdit2DP2.AutoHeight = false;
            this.repositoryItemTextEdit2DP2.Mask.EditMask = "n2";
            this.repositoryItemTextEdit2DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit2DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit2DP2.Name = "repositoryItemTextEdit2DP2";
            // 
            // colHolidayUnitDescriptor1
            // 
            this.colHolidayUnitDescriptor1.Caption = "Unit Descriptor";
            this.colHolidayUnitDescriptor1.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor1.Name = "colHolidayUnitDescriptor1";
            this.colHolidayUnitDescriptor1.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor1.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor1.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor1.Visible = true;
            this.colHolidayUnitDescriptor1.VisibleIndex = 7;
            this.colHolidayUnitDescriptor1.Width = 92;
            // 
            // colApprovedDate
            // 
            this.colApprovedDate.Caption = "Approved Date";
            this.colApprovedDate.ColumnEdit = this.repositoryItemTextEditLongDate2;
            this.colApprovedDate.FieldName = "ApprovedDate";
            this.colApprovedDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colApprovedDate.Name = "colApprovedDate";
            this.colApprovedDate.OptionsColumn.AllowEdit = false;
            this.colApprovedDate.OptionsColumn.AllowFocus = false;
            this.colApprovedDate.OptionsColumn.ReadOnly = true;
            this.colApprovedDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colApprovedDate.Visible = true;
            this.colApprovedDate.VisibleIndex = 18;
            this.colApprovedDate.Width = 94;
            // 
            // repositoryItemTextEditLongDate2
            // 
            this.repositoryItemTextEditLongDate2.AutoHeight = false;
            this.repositoryItemTextEditLongDate2.Mask.EditMask = "g";
            this.repositoryItemTextEditLongDate2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditLongDate2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditLongDate2.Name = "repositoryItemTextEditLongDate2";
            // 
            // colCancelledDate
            // 
            this.colCancelledDate.Caption = "Cancelled Date";
            this.colCancelledDate.ColumnEdit = this.repositoryItemTextEditLongDate2;
            this.colCancelledDate.FieldName = "CancelledDate";
            this.colCancelledDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colCancelledDate.Name = "colCancelledDate";
            this.colCancelledDate.OptionsColumn.AllowEdit = false;
            this.colCancelledDate.OptionsColumn.AllowFocus = false;
            this.colCancelledDate.OptionsColumn.ReadOnly = true;
            this.colCancelledDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colCancelledDate.Visible = true;
            this.colCancelledDate.VisibleIndex = 20;
            this.colCancelledDate.Width = 93;
            // 
            // colDeclaredDisability
            // 
            this.colDeclaredDisability.Caption = "Declared Disability";
            this.colDeclaredDisability.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colDeclaredDisability.FieldName = "DeclaredDisability";
            this.colDeclaredDisability.Name = "colDeclaredDisability";
            this.colDeclaredDisability.OptionsColumn.AllowEdit = false;
            this.colDeclaredDisability.OptionsColumn.AllowFocus = false;
            this.colDeclaredDisability.OptionsColumn.ReadOnly = true;
            this.colDeclaredDisability.Visible = true;
            this.colDeclaredDisability.VisibleIndex = 23;
            this.colDeclaredDisability.Width = 108;
            // 
            // colPayCategoryId
            // 
            this.colPayCategoryId.Caption = "Pay Category ID";
            this.colPayCategoryId.FieldName = "PayCategoryId";
            this.colPayCategoryId.Name = "colPayCategoryId";
            this.colPayCategoryId.OptionsColumn.AllowEdit = false;
            this.colPayCategoryId.OptionsColumn.AllowFocus = false;
            this.colPayCategoryId.OptionsColumn.ReadOnly = true;
            this.colPayCategoryId.Width = 101;
            // 
            // colIsWorkRelated
            // 
            this.colIsWorkRelated.Caption = "Work Related";
            this.colIsWorkRelated.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIsWorkRelated.FieldName = "IsWorkRelated";
            this.colIsWorkRelated.Name = "colIsWorkRelated";
            this.colIsWorkRelated.OptionsColumn.AllowEdit = false;
            this.colIsWorkRelated.OptionsColumn.AllowFocus = false;
            this.colIsWorkRelated.OptionsColumn.ReadOnly = true;
            this.colIsWorkRelated.Visible = true;
            this.colIsWorkRelated.VisibleIndex = 22;
            this.colIsWorkRelated.Width = 86;
            // 
            // colComments4
            // 
            this.colComments4.Caption = "Remarks";
            this.colComments4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colComments4.FieldName = "Comments";
            this.colComments4.Name = "colComments4";
            this.colComments4.OptionsColumn.ReadOnly = true;
            this.colComments4.Visible = true;
            this.colComments4.VisibleIndex = 24;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colRecordType
            // 
            this.colRecordType.Caption = "Record Type";
            this.colRecordType.FieldName = "RecordType";
            this.colRecordType.Name = "colRecordType";
            this.colRecordType.OptionsColumn.AllowEdit = false;
            this.colRecordType.OptionsColumn.AllowFocus = false;
            this.colRecordType.OptionsColumn.ReadOnly = true;
            this.colRecordType.Visible = true;
            this.colRecordType.VisibleIndex = 3;
            this.colRecordType.Width = 94;
            // 
            // colAbsenceType
            // 
            this.colAbsenceType.Caption = "Absence Type";
            this.colAbsenceType.FieldName = "AbsenceType";
            this.colAbsenceType.Name = "colAbsenceType";
            this.colAbsenceType.OptionsColumn.AllowEdit = false;
            this.colAbsenceType.OptionsColumn.AllowFocus = false;
            this.colAbsenceType.OptionsColumn.ReadOnly = true;
            this.colAbsenceType.Visible = true;
            this.colAbsenceType.VisibleIndex = 4;
            this.colAbsenceType.Width = 122;
            // 
            // colStatus
            // 
            this.colStatus.Caption = "Status";
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 8;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Holiday Year";
            this.gridColumn4.FieldName = "HolidayYearDescription";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 11;
            this.gridColumn4.Width = 94;
            // 
            // colEmployeeSurnameForename1
            // 
            this.colEmployeeSurnameForename1.Caption = "Surname: Forename";
            this.colEmployeeSurnameForename1.FieldName = "EmployeeSurnameForename";
            this.colEmployeeSurnameForename1.Name = "colEmployeeSurnameForename1";
            this.colEmployeeSurnameForename1.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurnameForename1.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurnameForename1.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurnameForename1.Visible = true;
            this.colEmployeeSurnameForename1.VisibleIndex = 22;
            this.colEmployeeSurnameForename1.Width = 131;
            // 
            // colEmployeeSurname1
            // 
            this.colEmployeeSurname1.Caption = "Surname";
            this.colEmployeeSurname1.FieldName = "EmployeeSurname";
            this.colEmployeeSurname1.Name = "colEmployeeSurname1";
            this.colEmployeeSurname1.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname1.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname1.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeForename1
            // 
            this.colEmployeeForename1.Caption = "Forename";
            this.colEmployeeForename1.FieldName = "EmployeeForename";
            this.colEmployeeForename1.Name = "colEmployeeForename1";
            this.colEmployeeForename1.OptionsColumn.AllowEdit = false;
            this.colEmployeeForename1.OptionsColumn.AllowFocus = false;
            this.colEmployeeForename1.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeNumber3
            // 
            this.colEmployeeNumber3.Caption = "Employee #";
            this.colEmployeeNumber3.FieldName = "EmployeeNumber";
            this.colEmployeeNumber3.Name = "colEmployeeNumber3";
            this.colEmployeeNumber3.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber3.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber3.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber3.Width = 78;
            // 
            // colApprovedByPerson
            // 
            this.colApprovedByPerson.Caption = "Approved By";
            this.colApprovedByPerson.FieldName = "ApprovedByPerson";
            this.colApprovedByPerson.Name = "colApprovedByPerson";
            this.colApprovedByPerson.OptionsColumn.AllowEdit = false;
            this.colApprovedByPerson.OptionsColumn.AllowFocus = false;
            this.colApprovedByPerson.OptionsColumn.ReadOnly = true;
            this.colApprovedByPerson.Visible = true;
            this.colApprovedByPerson.VisibleIndex = 19;
            this.colApprovedByPerson.Width = 83;
            // 
            // colCancelledByPerson
            // 
            this.colCancelledByPerson.Caption = "Cancelled By";
            this.colCancelledByPerson.FieldName = "CancelledByPerson";
            this.colCancelledByPerson.Name = "colCancelledByPerson";
            this.colCancelledByPerson.OptionsColumn.AllowEdit = false;
            this.colCancelledByPerson.OptionsColumn.AllowFocus = false;
            this.colCancelledByPerson.OptionsColumn.ReadOnly = true;
            this.colCancelledByPerson.Visible = true;
            this.colCancelledByPerson.VisibleIndex = 21;
            this.colCancelledByPerson.Width = 82;
            // 
            // colAbsenceSubType
            // 
            this.colAbsenceSubType.Caption = "Sub-Type";
            this.colAbsenceSubType.FieldName = "AbsenceSubType";
            this.colAbsenceSubType.Name = "colAbsenceSubType";
            this.colAbsenceSubType.OptionsColumn.AllowEdit = false;
            this.colAbsenceSubType.OptionsColumn.AllowFocus = false;
            this.colAbsenceSubType.OptionsColumn.ReadOnly = true;
            this.colAbsenceSubType.Visible = true;
            this.colAbsenceSubType.VisibleIndex = 5;
            this.colAbsenceSubType.Width = 116;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Active Holiday Year";
            this.gridColumn5.FieldName = "ActiveHolidayYear";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Width = 114;
            // 
            // colContractNumber1
            // 
            this.colContractNumber1.Caption = "Contract #";
            this.colContractNumber1.FieldName = "ContractNumber";
            this.colContractNumber1.Name = "colContractNumber1";
            this.colContractNumber1.OptionsColumn.AllowEdit = false;
            this.colContractNumber1.OptionsColumn.AllowFocus = false;
            this.colContractNumber1.OptionsColumn.ReadOnly = true;
            // 
            // colEWCDate
            // 
            this.colEWCDate.ColumnEdit = this.repositoryItemTextEditShortDate2;
            this.colEWCDate.FieldName = "EWCDate";
            this.colEWCDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colEWCDate.Name = "colEWCDate";
            this.colEWCDate.OptionsColumn.AllowEdit = false;
            this.colEWCDate.OptionsColumn.AllowFocus = false;
            this.colEWCDate.OptionsColumn.ReadOnly = true;
            this.colEWCDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colEWCDate.Visible = true;
            this.colEWCDate.VisibleIndex = 11;
            // 
            // colExpectedReturnDate
            // 
            this.colExpectedReturnDate.ColumnEdit = this.repositoryItemTextEditShortDate2;
            this.colExpectedReturnDate.FieldName = "ExpectedReturnDate";
            this.colExpectedReturnDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpectedReturnDate.Name = "colExpectedReturnDate";
            this.colExpectedReturnDate.OptionsColumn.AllowEdit = false;
            this.colExpectedReturnDate.OptionsColumn.AllowFocus = false;
            this.colExpectedReturnDate.OptionsColumn.ReadOnly = true;
            this.colExpectedReturnDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpectedReturnDate.Visible = true;
            this.colExpectedReturnDate.VisibleIndex = 16;
            this.colExpectedReturnDate.Width = 128;
            // 
            // colHalfDayEnd
            // 
            this.colHalfDayEnd.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHalfDayEnd.FieldName = "HalfDayEnd";
            this.colHalfDayEnd.Name = "colHalfDayEnd";
            this.colHalfDayEnd.OptionsColumn.AllowEdit = false;
            this.colHalfDayEnd.OptionsColumn.AllowFocus = false;
            this.colHalfDayEnd.OptionsColumn.ReadOnly = true;
            this.colHalfDayEnd.Visible = true;
            this.colHalfDayEnd.VisibleIndex = 10;
            this.colHalfDayEnd.Width = 83;
            // 
            // colHalfDayStart
            // 
            this.colHalfDayStart.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colHalfDayStart.FieldName = "HalfDayStart";
            this.colHalfDayStart.Name = "colHalfDayStart";
            this.colHalfDayStart.OptionsColumn.AllowEdit = false;
            this.colHalfDayStart.OptionsColumn.AllowFocus = false;
            this.colHalfDayStart.OptionsColumn.ReadOnly = true;
            this.colHalfDayStart.Visible = true;
            this.colHalfDayStart.VisibleIndex = 9;
            this.colHalfDayStart.Width = 89;
            // 
            // colAdditionalDaysPaid
            // 
            this.colAdditionalDaysPaid.Caption = "Additional Days Paid";
            this.colAdditionalDaysPaid.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAdditionalDaysPaid.FieldName = "AdditionalDaysPaid";
            this.colAdditionalDaysPaid.Name = "colAdditionalDaysPaid";
            this.colAdditionalDaysPaid.OptionsColumn.AllowEdit = false;
            this.colAdditionalDaysPaid.OptionsColumn.AllowFocus = false;
            this.colAdditionalDaysPaid.OptionsColumn.ReadOnly = true;
            this.colAdditionalDaysPaid.Visible = true;
            this.colAdditionalDaysPaid.VisibleIndex = 17;
            this.colAdditionalDaysPaid.Width = 118;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "####0.00";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.Caption = "Linked Documents";
            this.colLinkedDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentAbsences;
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.Visible = true;
            this.colLinkedDocumentCount.VisibleIndex = 26;
            this.colLinkedDocumentCount.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentAbsences
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentAbsences.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentAbsences.Name = "repositoryItemHyperLinkEditLinkedDocumentAbsences";
            this.repositoryItemHyperLinkEditLinkedDocumentAbsences.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentAbsences.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentAbsences_OpenLink);
            // 
            // colUnitsPaidCSP
            // 
            this.colUnitsPaidCSP.Caption = "Units Paid CSP";
            this.colUnitsPaidCSP.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colUnitsPaidCSP.FieldName = "UnitsPaidCSP";
            this.colUnitsPaidCSP.Name = "colUnitsPaidCSP";
            this.colUnitsPaidCSP.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidCSP.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidCSP.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidCSP.Visible = true;
            this.colUnitsPaidCSP.VisibleIndex = 12;
            this.colUnitsPaidCSP.Width = 90;
            // 
            // colUnitsPaidHalfPay
            // 
            this.colUnitsPaidHalfPay.Caption = "Units Paid Half Day";
            this.colUnitsPaidHalfPay.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colUnitsPaidHalfPay.FieldName = "UnitsPaidHalfPay";
            this.colUnitsPaidHalfPay.Name = "colUnitsPaidHalfPay";
            this.colUnitsPaidHalfPay.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidHalfPay.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidHalfPay.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidHalfPay.Visible = true;
            this.colUnitsPaidHalfPay.VisibleIndex = 13;
            this.colUnitsPaidHalfPay.Width = 112;
            // 
            // colUnitsPaidOther
            // 
            this.colUnitsPaidOther.Caption = "Units Paid Other";
            this.colUnitsPaidOther.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colUnitsPaidOther.FieldName = "UnitsPaidOther";
            this.colUnitsPaidOther.Name = "colUnitsPaidOther";
            this.colUnitsPaidOther.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidOther.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidOther.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidOther.Visible = true;
            this.colUnitsPaidOther.VisibleIndex = 14;
            this.colUnitsPaidOther.Width = 99;
            // 
            // colUnitsPaidUnpaid
            // 
            this.colUnitsPaidUnpaid.Caption = "Units Unpaid";
            this.colUnitsPaidUnpaid.ColumnEdit = this.repositoryItemTextEdit2DP2;
            this.colUnitsPaidUnpaid.FieldName = "UnitsPaidUnpaid";
            this.colUnitsPaidUnpaid.Name = "colUnitsPaidUnpaid";
            this.colUnitsPaidUnpaid.OptionsColumn.AllowEdit = false;
            this.colUnitsPaidUnpaid.OptionsColumn.AllowFocus = false;
            this.colUnitsPaidUnpaid.OptionsColumn.ReadOnly = true;
            this.colUnitsPaidUnpaid.Visible = true;
            this.colUnitsPaidUnpaid.VisibleIndex = 15;
            this.colUnitsPaidUnpaid.Width = 81;
            // 
            // colAbsenceComment
            // 
            this.colAbsenceComment.Caption = "Absence Comment";
            this.colAbsenceComment.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colAbsenceComment.FieldName = "AbsenceComment";
            this.colAbsenceComment.Name = "colAbsenceComment";
            this.colAbsenceComment.OptionsColumn.ReadOnly = true;
            this.colAbsenceComment.Visible = true;
            this.colAbsenceComment.VisibleIndex = 25;
            this.colAbsenceComment.Width = 110;
            // 
            // sp_HR_00077_Absence_Types_List_With_BlankTableAdapter
            // 
            this.sp_HR_00077_Absence_Types_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00113_Holiday_Manager_Holiday_YearsTableAdapter
            // 
            this.sp_HR_00113_Holiday_Manager_Holiday_YearsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00114_Department_Filter_ListTableAdapter
            // 
            this.sp_HR_00114_Department_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter
            // 
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // xtraGridBlending2
            // 
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending2.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending2.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending2.GridControl = this.gridControl8;
            // 
            // sp_HR_00116_Holiday_Manager_AbsencesTableAdapter
            // 
            this.sp_HR_00116_Holiday_Manager_AbsencesTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Holiday_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1131, 557);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Holiday_Manager";
            this.Text = "Holidays & Absences Manager";
            this.Activated += new System.EventHandler(this.frm_HR_Holiday_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Holiday_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Holiday_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlAbsenceTypes)).EndInit();
            this.popupContainerControlAbsenceTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00077AbsenceTypesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00113HolidayManagerHolidayYearsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDaysHoliday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditHoursHoliday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditUnknownHoliday)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).EndInit();
            this.popupContainerControlDepartments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).EndInit();
            this.popupContainerControlEmployees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditAbsenceType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00116HolidayManagerAbsencesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditShortDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditLongDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentAbsences)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlAbsenceTypes;
        private DevExpress.XtraEditors.SimpleButton btnAbsenceTypeFilterOK;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem beiAbsenceType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditAbsenceType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditShortDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2DP2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraBars.BarEditItem beiShowActiveHolidayYearOnly;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveHoilidayYearOnly;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00077AbsenceTypesListWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00077_Absence_Types_List_With_BlankTableAdapter sp_HR_00077_Absence_Types_List_With_BlankTableAdapter;
        private DevExpress.XtraBars.BarEditItem beiDepartment;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDepartment;
        private DevExpress.XtraBars.BarEditItem beiEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditEmployee;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlEmployees;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton btnEmployeeFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDepartments;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton btnDepartmentFilterOK;
        private System.Windows.Forms.BindingSource spHR00113HolidayManagerHolidayYearsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeHolidayYearID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colBasicHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchasedHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursPerHolidayDay;
        private DevExpress.XtraGrid.Columns.GridColumn colMustTakeCertainHolidays;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurnameForename;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveHolidayYear;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceAuthorisedTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceUnauthorisedTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidaysRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidaysRemaining;
        private DataSet_HR_CoreTableAdapters.sp_HR_00113_Holiday_Manager_Holiday_YearsTableAdapter sp_HR_00113_Holiday_Manager_Holiday_YearsTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDaysHoliday;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditHoursHoliday;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeForename;
        private System.Windows.Forms.BindingSource spHR00114DepartmentFilterListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter sp_HR_00114_Department_Filter_ListTableAdapter;
        private System.Windows.Forms.BindingSource spHR00115EmployeesByDeptFilterListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditUnknownHoliday;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditLongDate2;
        private System.Windows.Forms.BindingSource spHR00116HolidayManagerAbsencesBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceSubTypeId;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsUsed;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor1;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledDate;
        private DevExpress.XtraGrid.Columns.GridColumn colDeclaredDisability;
        private DevExpress.XtraGrid.Columns.GridColumn colPayCategoryId;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWorkRelated;
        private DevExpress.XtraGrid.Columns.GridColumn colComments4;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurnameForename1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeForename1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber3;
        private DevExpress.XtraGrid.Columns.GridColumn colApprovedByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colCancelledByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceSubType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn colContractNumber1;
        private DataSet_HR_CoreTableAdapters.sp_HR_00116_Holiday_Manager_AbsencesTableAdapter sp_HR_00116_Holiday_Manager_AbsencesTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colEWCDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedReturnDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHalfDayEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colHalfDayStart;
        private DevExpress.XtraGrid.Columns.GridColumn colAdditionalDaysPaid;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentAbsences;
        private DevExpress.XtraBars.BarCheckItem bciFilterSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidCSP;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidHalfPay;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidOther;
        private DevExpress.XtraGrid.Columns.GridColumn colUnitsPaidUnpaid;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceComment;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedFromWeb;
    }
}

namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Holiday_Year
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Select_Employee_Holiday_Year));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colActiveHolidayYear = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00030SelectEmployeeHolidayYearsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmpContractId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayYearStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colHolidayYearEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActualStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHoursPerHolidayDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMustTakeCertainHolidays = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colHolidayYearDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurnameForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidayUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colBankHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceAuthorisedTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAbsenceUnauthorisedTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBasicHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPurchasedHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidayEntitlement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidaysTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalHolidaysRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditTotalCost = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditDummyHours = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEditDummyDays = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiActiveHolidayYearOnly = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditActiveHolidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.sp_HR_00030_Select_Employee_Holiday_YearsTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00030_Select_Employee_Holiday_YearsTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00030SelectEmployeeHolidayYearsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDummyHours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDummyDays)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveHolidayYearOnly)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(899, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 432);
            this.barDockControlBottom.Size = new System.Drawing.Size(899, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 406);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(899, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 406);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.beiActiveHolidayYearOnly});
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEditActiveHolidayYearOnly});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActiveHolidayYear
            // 
            this.colActiveHolidayYear.Caption = "Active Year";
            this.colActiveHolidayYear.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActiveHolidayYear.FieldName = "ActiveHolidayYear";
            this.colActiveHolidayYear.Name = "colActiveHolidayYear";
            this.colActiveHolidayYear.OptionsColumn.AllowEdit = false;
            this.colActiveHolidayYear.OptionsColumn.AllowFocus = false;
            this.colActiveHolidayYear.OptionsColumn.ReadOnly = true;
            this.colActiveHolidayYear.Visible = true;
            this.colActiveHolidayYear.VisibleIndex = 7;
            this.colActiveHolidayYear.Width = 76;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.spHR00030SelectEmployeeHolidayYearsBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 23);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditTotalCost,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDays,
            this.repositoryItemTextEditDummyHours,
            this.repositoryItemTextEditDummyDays});
            this.gridControl1.Size = new System.Drawing.Size(899, 371);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00030SelectEmployeeHolidayYearsBindingSource
            // 
            this.spHR00030SelectEmployeeHolidayYearsBindingSource.DataMember = "sp_HR_00030_Select_Employee_Holiday_Years";
            this.spHR00030SelectEmployeeHolidayYearsBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colEmpContractId,
            this.colHolidayYearStartDate,
            this.colHolidayYearEndDate,
            this.colActualStartDate,
            this.colHolidayUnitDescriptorId,
            this.colHoursPerHolidayDay,
            this.colMustTakeCertainHolidays,
            this.colRemarks,
            this.colHolidayYearDescription,
            this.colEmployeeSurnameForename,
            this.colEmployeeSurname,
            this.colEmployeeForename,
            this.colEmployeeNumber,
            this.colActiveHolidayYear,
            this.colHolidayUnitDescriptor,
            this.colHolidaysTaken,
            this.colBankHolidaysTaken,
            this.colAbsenceAuthorisedTaken,
            this.colAbsenceUnauthorisedTaken,
            this.colBasicHolidayEntitlement,
            this.colBankHolidayEntitlement,
            this.colPurchasedHolidayEntitlement,
            this.colTotalHolidayEntitlement,
            this.colHolidaysRemaining,
            this.colBankHolidaysRemaining,
            this.colTotalHolidaysTaken,
            this.colTotalHolidaysRemaining});
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colActiveHolidayYear;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Gray;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colHolidayYearDescription, DevExpress.Data.ColumnSortOrder.Descending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeForename, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colId
            // 
            this.colId.Caption = "Employee Holiday Year ID";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.ReadOnly = true;
            this.colId.Width = 144;
            // 
            // colEmpContractId
            // 
            this.colEmpContractId.Caption = "Employee ID";
            this.colEmpContractId.FieldName = "EmployeeID";
            this.colEmpContractId.Name = "colEmpContractId";
            this.colEmpContractId.OptionsColumn.AllowEdit = false;
            this.colEmpContractId.OptionsColumn.AllowFocus = false;
            this.colEmpContractId.OptionsColumn.ReadOnly = true;
            this.colEmpContractId.Width = 126;
            // 
            // colHolidayYearStartDate
            // 
            this.colHolidayYearStartDate.Caption = "Start Date";
            this.colHolidayYearStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colHolidayYearStartDate.FieldName = "HolidayYearStartDate";
            this.colHolidayYearStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHolidayYearStartDate.Name = "colHolidayYearStartDate";
            this.colHolidayYearStartDate.OptionsColumn.AllowEdit = false;
            this.colHolidayYearStartDate.OptionsColumn.AllowFocus = false;
            this.colHolidayYearStartDate.OptionsColumn.ReadOnly = true;
            this.colHolidayYearStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHolidayYearStartDate.Visible = true;
            this.colHolidayYearStartDate.VisibleIndex = 3;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colHolidayYearEndDate
            // 
            this.colHolidayYearEndDate.Caption = "End Date";
            this.colHolidayYearEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colHolidayYearEndDate.FieldName = "HolidayYearEndDate";
            this.colHolidayYearEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHolidayYearEndDate.Name = "colHolidayYearEndDate";
            this.colHolidayYearEndDate.OptionsColumn.AllowEdit = false;
            this.colHolidayYearEndDate.OptionsColumn.AllowFocus = false;
            this.colHolidayYearEndDate.OptionsColumn.ReadOnly = true;
            this.colHolidayYearEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHolidayYearEndDate.Visible = true;
            this.colHolidayYearEndDate.VisibleIndex = 4;
            // 
            // colActualStartDate
            // 
            this.colActualStartDate.Caption = "Actual Start Date";
            this.colActualStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colActualStartDate.FieldName = "ActualStartDate";
            this.colActualStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colActualStartDate.Name = "colActualStartDate";
            this.colActualStartDate.OptionsColumn.AllowEdit = false;
            this.colActualStartDate.OptionsColumn.AllowFocus = false;
            this.colActualStartDate.OptionsColumn.ReadOnly = true;
            this.colActualStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colActualStartDate.Visible = true;
            this.colActualStartDate.VisibleIndex = 5;
            this.colActualStartDate.Width = 104;
            // 
            // colHolidayUnitDescriptorId
            // 
            this.colHolidayUnitDescriptorId.Caption = "Unit Descriptor ID";
            this.colHolidayUnitDescriptorId.FieldName = "HolidayUnitDescriptorId";
            this.colHolidayUnitDescriptorId.Name = "colHolidayUnitDescriptorId";
            this.colHolidayUnitDescriptorId.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptorId.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptorId.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptorId.Width = 106;
            // 
            // colHoursPerHolidayDay
            // 
            this.colHoursPerHolidayDay.Caption = "Hours Per Holiday Day";
            this.colHoursPerHolidayDay.FieldName = "HoursPerHolidayDay";
            this.colHoursPerHolidayDay.Name = "colHoursPerHolidayDay";
            this.colHoursPerHolidayDay.OptionsColumn.AllowEdit = false;
            this.colHoursPerHolidayDay.OptionsColumn.AllowFocus = false;
            this.colHoursPerHolidayDay.OptionsColumn.ReadOnly = true;
            this.colHoursPerHolidayDay.Width = 128;
            // 
            // colMustTakeCertainHolidays
            // 
            this.colMustTakeCertainHolidays.Caption = "Must Take Certain Holidays";
            this.colMustTakeCertainHolidays.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colMustTakeCertainHolidays.FieldName = "MustTakeCertainHolidays";
            this.colMustTakeCertainHolidays.Name = "colMustTakeCertainHolidays";
            this.colMustTakeCertainHolidays.OptionsColumn.AllowEdit = false;
            this.colMustTakeCertainHolidays.OptionsColumn.AllowFocus = false;
            this.colMustTakeCertainHolidays.OptionsColumn.ReadOnly = true;
            this.colMustTakeCertainHolidays.Visible = true;
            this.colMustTakeCertainHolidays.VisibleIndex = 6;
            this.colMustTakeCertainHolidays.Width = 151;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.AllowEdit = false;
            this.colRemarks.OptionsColumn.AllowFocus = false;
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 20;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colHolidayYearDescription
            // 
            this.colHolidayYearDescription.Caption = "Holiday Year";
            this.colHolidayYearDescription.FieldName = "HolidayYearDescription";
            this.colHolidayYearDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colHolidayYearDescription.Name = "colHolidayYearDescription";
            this.colHolidayYearDescription.OptionsColumn.AllowEdit = false;
            this.colHolidayYearDescription.OptionsColumn.AllowFocus = false;
            this.colHolidayYearDescription.OptionsColumn.ReadOnly = true;
            this.colHolidayYearDescription.Visible = true;
            this.colHolidayYearDescription.VisibleIndex = 0;
            this.colHolidayYearDescription.Width = 129;
            // 
            // colEmployeeSurnameForename
            // 
            this.colEmployeeSurnameForename.Caption = "Employee Surname, Forename";
            this.colEmployeeSurnameForename.FieldName = "EmployeeSurnameForename";
            this.colEmployeeSurnameForename.Name = "colEmployeeSurnameForename";
            this.colEmployeeSurnameForename.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurnameForename.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurnameForename.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurnameForename.Width = 281;
            // 
            // colEmployeeSurname
            // 
            this.colEmployeeSurname.Caption = "Surname";
            this.colEmployeeSurname.FieldName = "EmployeeSurname";
            this.colEmployeeSurname.Name = "colEmployeeSurname";
            this.colEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname.Visible = true;
            this.colEmployeeSurname.VisibleIndex = 1;
            this.colEmployeeSurname.Width = 121;
            // 
            // colEmployeeForename
            // 
            this.colEmployeeForename.Caption = "Forename";
            this.colEmployeeForename.FieldName = "EmployeeForename";
            this.colEmployeeForename.Name = "colEmployeeForename";
            this.colEmployeeForename.OptionsColumn.AllowEdit = false;
            this.colEmployeeForename.OptionsColumn.AllowFocus = false;
            this.colEmployeeForename.OptionsColumn.ReadOnly = true;
            this.colEmployeeForename.Visible = true;
            this.colEmployeeForename.VisibleIndex = 2;
            this.colEmployeeForename.Width = 113;
            // 
            // colEmployeeNumber
            // 
            this.colEmployeeNumber.Caption = "Employee #";
            this.colEmployeeNumber.FieldName = "EmployeeNumber";
            this.colEmployeeNumber.Name = "colEmployeeNumber";
            this.colEmployeeNumber.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber.Width = 78;
            // 
            // colHolidayUnitDescriptor
            // 
            this.colHolidayUnitDescriptor.Caption = "Unit Descriptor";
            this.colHolidayUnitDescriptor.FieldName = "HolidayUnitDescriptor";
            this.colHolidayUnitDescriptor.Name = "colHolidayUnitDescriptor";
            this.colHolidayUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colHolidayUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colHolidayUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colHolidayUnitDescriptor.Width = 92;
            // 
            // colHolidaysTaken
            // 
            this.colHolidaysTaken.Caption = "Holidays Used";
            this.colHolidaysTaken.ColumnEdit = this.repositoryItemTextEditDays;
            this.colHolidaysTaken.FieldName = "HolidaysTaken";
            this.colHolidaysTaken.Name = "colHolidaysTaken";
            this.colHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colHolidaysTaken.Visible = true;
            this.colHolidaysTaken.VisibleIndex = 9;
            this.colHolidaysTaken.Width = 93;
            // 
            // repositoryItemTextEditDays
            // 
            this.repositoryItemTextEditDays.AutoHeight = false;
            this.repositoryItemTextEditDays.Mask.EditMask = "####0.00";
            this.repositoryItemTextEditDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDays.Name = "repositoryItemTextEditDays";
            // 
            // colBankHolidaysTaken
            // 
            this.colBankHolidaysTaken.Caption = "Bank Holidays Used";
            this.colBankHolidaysTaken.ColumnEdit = this.repositoryItemTextEditDays;
            this.colBankHolidaysTaken.FieldName = "BankHolidaysTaken";
            this.colBankHolidaysTaken.Name = "colBankHolidaysTaken";
            this.colBankHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colBankHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colBankHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colBankHolidaysTaken.Visible = true;
            this.colBankHolidaysTaken.VisibleIndex = 13;
            this.colBankHolidaysTaken.Width = 114;
            // 
            // colAbsenceAuthorisedTaken
            // 
            this.colAbsenceAuthorisedTaken.Caption = "Authorised Absence Used";
            this.colAbsenceAuthorisedTaken.ColumnEdit = this.repositoryItemTextEditDays;
            this.colAbsenceAuthorisedTaken.FieldName = "AbsenceAuthorisedTaken";
            this.colAbsenceAuthorisedTaken.Name = "colAbsenceAuthorisedTaken";
            this.colAbsenceAuthorisedTaken.OptionsColumn.AllowEdit = false;
            this.colAbsenceAuthorisedTaken.OptionsColumn.AllowFocus = false;
            this.colAbsenceAuthorisedTaken.OptionsColumn.ReadOnly = true;
            this.colAbsenceAuthorisedTaken.Visible = true;
            this.colAbsenceAuthorisedTaken.VisibleIndex = 14;
            this.colAbsenceAuthorisedTaken.Width = 149;
            // 
            // colAbsenceUnauthorisedTaken
            // 
            this.colAbsenceUnauthorisedTaken.Caption = "Unauthorised Absence Used";
            this.colAbsenceUnauthorisedTaken.ColumnEdit = this.repositoryItemTextEditDays;
            this.colAbsenceUnauthorisedTaken.FieldName = "AbsenceUnauthorisedTaken";
            this.colAbsenceUnauthorisedTaken.Name = "colAbsenceUnauthorisedTaken";
            this.colAbsenceUnauthorisedTaken.OptionsColumn.AllowEdit = false;
            this.colAbsenceUnauthorisedTaken.OptionsColumn.AllowFocus = false;
            this.colAbsenceUnauthorisedTaken.OptionsColumn.ReadOnly = true;
            this.colAbsenceUnauthorisedTaken.Visible = true;
            this.colAbsenceUnauthorisedTaken.VisibleIndex = 16;
            this.colAbsenceUnauthorisedTaken.Width = 161;
            // 
            // colBasicHolidayEntitlement
            // 
            this.colBasicHolidayEntitlement.Caption = "Holiday Entitlement";
            this.colBasicHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditDays;
            this.colBasicHolidayEntitlement.FieldName = "BasicHolidayEntitlement";
            this.colBasicHolidayEntitlement.Name = "colBasicHolidayEntitlement";
            this.colBasicHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colBasicHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colBasicHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colBasicHolidayEntitlement.Visible = true;
            this.colBasicHolidayEntitlement.VisibleIndex = 8;
            this.colBasicHolidayEntitlement.Width = 113;
            // 
            // colBankHolidayEntitlement
            // 
            this.colBankHolidayEntitlement.Caption = "Bank Holiday Entitlement";
            this.colBankHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditDays;
            this.colBankHolidayEntitlement.FieldName = "BankHolidayEntitlement";
            this.colBankHolidayEntitlement.Name = "colBankHolidayEntitlement";
            this.colBankHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colBankHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colBankHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colBankHolidayEntitlement.Visible = true;
            this.colBankHolidayEntitlement.VisibleIndex = 12;
            this.colBankHolidayEntitlement.Width = 139;
            // 
            // colPurchasedHolidayEntitlement
            // 
            this.colPurchasedHolidayEntitlement.Caption = "Purchased Holiday Entitlement";
            this.colPurchasedHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditDays;
            this.colPurchasedHolidayEntitlement.FieldName = "PurchasedHolidayEntitlement";
            this.colPurchasedHolidayEntitlement.Name = "colPurchasedHolidayEntitlement";
            this.colPurchasedHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colPurchasedHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colPurchasedHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colPurchasedHolidayEntitlement.Visible = true;
            this.colPurchasedHolidayEntitlement.VisibleIndex = 11;
            this.colPurchasedHolidayEntitlement.Width = 166;
            // 
            // colTotalHolidayEntitlement
            // 
            this.colTotalHolidayEntitlement.Caption = "Total Holiday Entitlement";
            this.colTotalHolidayEntitlement.ColumnEdit = this.repositoryItemTextEditDays;
            this.colTotalHolidayEntitlement.FieldName = "TotalHolidayEntitlement";
            this.colTotalHolidayEntitlement.Name = "colTotalHolidayEntitlement";
            this.colTotalHolidayEntitlement.OptionsColumn.AllowEdit = false;
            this.colTotalHolidayEntitlement.OptionsColumn.AllowFocus = false;
            this.colTotalHolidayEntitlement.OptionsColumn.ReadOnly = true;
            this.colTotalHolidayEntitlement.Visible = true;
            this.colTotalHolidayEntitlement.VisibleIndex = 17;
            this.colTotalHolidayEntitlement.Width = 140;
            // 
            // colHolidaysRemaining
            // 
            this.colHolidaysRemaining.Caption = "Holidays Remaining";
            this.colHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditDays;
            this.colHolidaysRemaining.FieldName = "HolidaysRemaining";
            this.colHolidaysRemaining.Name = "colHolidaysRemaining";
            this.colHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colHolidaysRemaining.Visible = true;
            this.colHolidaysRemaining.VisibleIndex = 10;
            this.colHolidaysRemaining.Width = 113;
            // 
            // colBankHolidaysRemaining
            // 
            this.colBankHolidaysRemaining.Caption = "Bank Holidays Remaining";
            this.colBankHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditDays;
            this.colBankHolidaysRemaining.FieldName = "BankHolidaysRemaining";
            this.colBankHolidaysRemaining.Name = "colBankHolidaysRemaining";
            this.colBankHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colBankHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colBankHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colBankHolidaysRemaining.Visible = true;
            this.colBankHolidaysRemaining.VisibleIndex = 15;
            this.colBankHolidaysRemaining.Width = 139;
            // 
            // colTotalHolidaysTaken
            // 
            this.colTotalHolidaysTaken.Caption = "Total Holidays Used";
            this.colTotalHolidaysTaken.ColumnEdit = this.repositoryItemTextEditDays;
            this.colTotalHolidaysTaken.FieldName = "TotalHolidaysTaken";
            this.colTotalHolidaysTaken.Name = "colTotalHolidaysTaken";
            this.colTotalHolidaysTaken.OptionsColumn.AllowEdit = false;
            this.colTotalHolidaysTaken.OptionsColumn.AllowFocus = false;
            this.colTotalHolidaysTaken.OptionsColumn.ReadOnly = true;
            this.colTotalHolidaysTaken.Visible = true;
            this.colTotalHolidaysTaken.VisibleIndex = 18;
            this.colTotalHolidaysTaken.Width = 120;
            // 
            // colTotalHolidaysRemaining
            // 
            this.colTotalHolidaysRemaining.Caption = "Total Holidays Remaining";
            this.colTotalHolidaysRemaining.ColumnEdit = this.repositoryItemTextEditDays;
            this.colTotalHolidaysRemaining.FieldName = "TotalHolidaysRemaining";
            this.colTotalHolidaysRemaining.Name = "colTotalHolidaysRemaining";
            this.colTotalHolidaysRemaining.OptionsColumn.AllowEdit = false;
            this.colTotalHolidaysRemaining.OptionsColumn.AllowFocus = false;
            this.colTotalHolidaysRemaining.OptionsColumn.ReadOnly = true;
            this.colTotalHolidaysRemaining.Visible = true;
            this.colTotalHolidaysRemaining.VisibleIndex = 19;
            this.colTotalHolidaysRemaining.Width = 140;
            // 
            // repositoryItemTextEditTotalCost
            // 
            this.repositoryItemTextEditTotalCost.AutoHeight = false;
            this.repositoryItemTextEditTotalCost.Mask.EditMask = "c";
            this.repositoryItemTextEditTotalCost.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditTotalCost.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditTotalCost.Name = "repositoryItemTextEditTotalCost";
            // 
            // repositoryItemTextEditDummyHours
            // 
            this.repositoryItemTextEditDummyHours.AutoHeight = false;
            this.repositoryItemTextEditDummyHours.Mask.EditMask = "####0.00 Hours";
            this.repositoryItemTextEditDummyHours.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditDummyHours.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDummyHours.Name = "repositoryItemTextEditDummyHours";
            // 
            // repositoryItemTextEditDummyDays
            // 
            this.repositoryItemTextEditDummyDays.AutoHeight = false;
            this.repositoryItemTextEditDummyDays.Mask.EditMask = "####0.00 Days";
            this.repositoryItemTextEditDummyDays.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditDummyDays.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDummyDays.Name = "repositoryItemTextEditDummyDays";
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(736, 402);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(817, 402);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiActiveHolidayYearOnly, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // beiActiveHolidayYearOnly
            // 
            this.beiActiveHolidayYearOnly.Caption = "Active Holiday Year Only";
            this.beiActiveHolidayYearOnly.Edit = this.repositoryItemCheckEditActiveHolidayYearOnly;
            this.beiActiveHolidayYearOnly.EditValue = 1;
            this.beiActiveHolidayYearOnly.EditWidth = 21;
            this.beiActiveHolidayYearOnly.Id = 29;
            this.beiActiveHolidayYearOnly.Name = "beiActiveHolidayYearOnly";
            this.beiActiveHolidayYearOnly.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEditActiveHolidayYearOnly
            // 
            this.repositoryItemCheckEditActiveHolidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveHolidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveHolidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveHolidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveHolidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveHolidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveHolidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveHolidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveHolidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditActiveHolidayYearOnly.Caption = "";
            this.repositoryItemCheckEditActiveHolidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEditActiveHolidayYearOnly.Name = "repositoryItemCheckEditActiveHolidayYearOnly";
            this.repositoryItemCheckEditActiveHolidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditActiveHolidayYearOnly.ValueUnchecked = 0;
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh Data";
            this.bbiRefresh.Id = 28;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.LargeImage")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // sp_HR_00030_Select_Employee_Holiday_YearsTableAdapter
            // 
            this.sp_HR_00030_Select_Employee_Holiday_YearsTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // frm_HR_Select_Employee_Holiday_Year
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(899, 432);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Holiday_Year";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Holiday Year";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Holiday_Year_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00030SelectEmployeeHolidayYearsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDummyHours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDummyDays)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveHolidayYearOnly)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditTotalCost;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem beiActiveHolidayYearOnly;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditActiveHolidayYearOnly;
        private System.Windows.Forms.BindingSource spHR00030SelectEmployeeHolidayYearsBindingSource;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_CoreTableAdapters.sp_HR_00030_Select_Employee_Holiday_YearsTableAdapter sp_HR_00030_Select_Employee_Holiday_YearsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colEmpContractId;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActualStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptorId;
        private DevExpress.XtraGrid.Columns.GridColumn colHoursPerHolidayDay;
        private DevExpress.XtraGrid.Columns.GridColumn colMustTakeCertainHolidays;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayYearDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurnameForename;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeForename;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActiveHolidayYear;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidayUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceAuthorisedTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colAbsenceUnauthorisedTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colBasicHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colPurchasedHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidayEntitlement;
        private DevExpress.XtraGrid.Columns.GridColumn colHolidaysRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colBankHolidaysRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidaysTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalHolidaysRemaining;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDays;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDummyHours;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDummyDays;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

namespace WoodPlan5
{
    partial class frm_HR_Sanction_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Sanction_Manager));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            this.colCurrentEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupContainerControlTypes = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.spHR00117SanctionTypesListNoBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnTypeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00118SanctionsManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSanctionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colERIssueTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colERIssueType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHRManagerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHRManagerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRaised = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colRaisedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRaisedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigatedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigatedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvestigationOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeardByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeardByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingNoteTakerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingEmployeeRepresentativeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeIssuedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHearingOutcomeIssuedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealHeardByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealHeardByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealOutcomeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealNoteTakerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppealEmployeeRepresentativeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDurationUnits = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit0DP2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpiryDurationUnitDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDurationUnitDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSanctionOutcome = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedDocumentSanction = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiDepartment = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDepartment = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlDepartments = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.spHR00114DepartmentFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDepartmentFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiEmployee = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlEmployees = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.spHR00115EmployeesByDeptFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnEmployeeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiType = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditType = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterSelected = new DevExpress.XtraBars.BarCheckItem();
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp_HR_00114_Department_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter();
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.sp_HR_00117_Sanction_Types_List_No_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00117_Sanction_Types_List_No_BlankTableAdapter();
            this.sp_HR_00118_Sanctions_ManagerTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00118_Sanctions_ManagerTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTypes)).BeginInit();
            this.popupContainerControlTypes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00117SanctionTypesListNoBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00118SanctionsManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit0DP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentSanction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).BeginInit();
            this.popupContainerControlDepartments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).BeginInit();
            this.popupContainerControlEmployees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1041, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 557);
            this.barDockControlBottom.Size = new System.Drawing.Size(1041, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 515);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1041, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 515);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.beiType,
            this.beiDepartment,
            this.beiEmployee,
            this.bciFilterSelected});
            this.barManager1.MaxItemId = 34;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditType,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly,
            this.repositoryItemPopupContainerEditDepartment,
            this.repositoryItemPopupContainerEditEmployee});
            // 
            // colCurrentEmployee
            // 
            this.colCurrentEmployee.Caption = "Current Employee";
            this.colCurrentEmployee.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCurrentEmployee.FieldName = "CurrentEmployee";
            this.colCurrentEmployee.Name = "colCurrentEmployee";
            this.colCurrentEmployee.OptionsColumn.AllowEdit = false;
            this.colCurrentEmployee.OptionsColumn.AllowFocus = false;
            this.colCurrentEmployee.OptionsColumn.ReadOnly = true;
            this.colCurrentEmployee.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentEmployee.Visible = true;
            this.colCurrentEmployee.VisibleIndex = 4;
            this.colCurrentEmployee.Width = 107;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ID";
            this.gridColumn3.FieldName = "ID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn3.Width = 53;
            // 
            // popupContainerControlTypes
            // 
            this.popupContainerControlTypes.Controls.Add(this.gridControl3);
            this.popupContainerControlTypes.Controls.Add(this.btnTypeFilterOK);
            this.popupContainerControlTypes.Location = new System.Drawing.Point(492, 96);
            this.popupContainerControlTypes.Name = "popupContainerControlTypes";
            this.popupContainerControlTypes.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlTypes.TabIndex = 1;
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.spHR00117SanctionTypesListNoBlankBindingSource;
            this.gridControl3.Location = new System.Drawing.Point(2, 2);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(230, 141);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // spHR00117SanctionTypesListNoBlankBindingSource
            // 
            this.spHR00117SanctionTypesListNoBlankBindingSource.DataMember = "sp_HR_00117_Sanction_Types_List_No_Blank";
            this.spHR00117SanctionTypesListNoBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn3,
            this.colDescription,
            this.colOrder});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Sanction Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 173;
            // 
            // colOrder
            // 
            this.colOrder.Caption = "Order";
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.AllowFocus = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            this.colOrder.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // btnTypeFilterOK
            // 
            this.btnTypeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnTypeFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnTypeFilterOK.Name = "btnTypeFilterOK";
            this.btnTypeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnTypeFilterOK.TabIndex = 2;
            this.btnTypeFilterOK.Text = "OK";
            this.btnTypeFilterOK.Click += new System.EventHandler(this.btnTypeFilterOK_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00118SanctionsManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Linked Documents", "linked_document")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEdit0DP2,
            this.repositoryItemHyperLinkEditLinkedDocumentSanction});
            this.gridControl1.Size = new System.Drawing.Size(1041, 515);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00118SanctionsManagerBindingSource
            // 
            this.spHR00118SanctionsManagerBindingSource.DataMember = "sp_HR_00118_Sanctions_Manager";
            this.spHR00118SanctionsManagerBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "linked_documents_16.png");
            this.imageCollection1.Images.SetKeyName(5, "linked_documents_16_16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSanctionID,
            this.colEmployeeID4,
            this.colEmployeeName,
            this.colEmployeeSurname2,
            this.colEmployeeFirstname,
            this.colEmployeeNumber5,
            this.colERIssueTypeID,
            this.colERIssueType,
            this.colHRManagerID,
            this.colHRManagerName,
            this.colDateRaised,
            this.colRaisedByPersonID,
            this.colRaisedByPersonName,
            this.colStatusID,
            this.colStatusValue,
            this.colInvestigationStartDate,
            this.colInvestigationEndDate,
            this.colInvestigatedByPersonID,
            this.colInvestigatedByPersonName,
            this.colInvestigationOutcomeID,
            this.colInvestigationOutcome,
            this.colHearingDate,
            this.colHeardByPersonID,
            this.colHeardByPersonName,
            this.colHearingNoteTakerName,
            this.colHearingEmployeeRepresentativeName,
            this.colHearingOutcomeID,
            this.colHearingOutcome,
            this.colHearingOutcomeDate,
            this.colHearingOutcomeIssuedByPersonID,
            this.colHearingOutcomeIssuedByPersonName,
            this.colAppealDate,
            this.colAppealHeardByPersonID,
            this.colAppealHeardByPersonName,
            this.colAppealOutcomeID,
            this.colAppealOutcome,
            this.colAppealNoteTakerName,
            this.colAppealEmployeeRepresentativeName,
            this.colExpiryDurationUnits,
            this.colExpiryDurationUnitDescriptorID,
            this.colExpiryDurationUnitDescriptor,
            this.colExpiryDate,
            this.colRemarks2,
            this.colLive,
            this.colSanctionOutcome,
            this.colLinkedDocumentCount,
            this.colSelected});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.BestFitMaxRowCount = 20;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeSurname2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeFirstname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRaised, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colSanctionID
            // 
            this.colSanctionID.Caption = "Sanction ID";
            this.colSanctionID.FieldName = "SanctionID";
            this.colSanctionID.Name = "colSanctionID";
            this.colSanctionID.OptionsColumn.AllowEdit = false;
            this.colSanctionID.OptionsColumn.AllowFocus = false;
            this.colSanctionID.OptionsColumn.ReadOnly = true;
            this.colSanctionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSanctionID.Width = 76;
            // 
            // colEmployeeID4
            // 
            this.colEmployeeID4.Caption = "Employee ID";
            this.colEmployeeID4.FieldName = "EmployeeID";
            this.colEmployeeID4.Name = "colEmployeeID4";
            this.colEmployeeID4.OptionsColumn.AllowEdit = false;
            this.colEmployeeID4.OptionsColumn.AllowFocus = false;
            this.colEmployeeID4.OptionsColumn.ReadOnly = true;
            this.colEmployeeID4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID4.Width = 81;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.Caption = "Employee Surname: Forename";
            this.colEmployeeName.FieldName = "EmployeeName";
            this.colEmployeeName.Name = "colEmployeeName";
            this.colEmployeeName.OptionsColumn.AllowEdit = false;
            this.colEmployeeName.OptionsColumn.AllowFocus = false;
            this.colEmployeeName.OptionsColumn.ReadOnly = true;
            this.colEmployeeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName.Width = 167;
            // 
            // colEmployeeSurname2
            // 
            this.colEmployeeSurname2.Caption = "Employee Surname";
            this.colEmployeeSurname2.FieldName = "EmployeeSurname";
            this.colEmployeeSurname2.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeSurname2.Name = "colEmployeeSurname2";
            this.colEmployeeSurname2.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname2.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname2.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname2.Visible = true;
            this.colEmployeeSurname2.VisibleIndex = 0;
            this.colEmployeeSurname2.Width = 125;
            // 
            // colEmployeeFirstname
            // 
            this.colEmployeeFirstname.Caption = "Employee Forename";
            this.colEmployeeFirstname.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colEmployeeFirstname.Name = "colEmployeeFirstname";
            this.colEmployeeFirstname.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname.Visible = true;
            this.colEmployeeFirstname.VisibleIndex = 1;
            this.colEmployeeFirstname.Width = 131;
            // 
            // colEmployeeNumber5
            // 
            this.colEmployeeNumber5.Caption = "Employee #";
            this.colEmployeeNumber5.FieldName = "EmployeeNumber";
            this.colEmployeeNumber5.Name = "colEmployeeNumber5";
            this.colEmployeeNumber5.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber5.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber5.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber5.Width = 78;
            // 
            // colERIssueTypeID
            // 
            this.colERIssueTypeID.Caption = "ER Issue Type ID";
            this.colERIssueTypeID.FieldName = "ERIssueTypeID";
            this.colERIssueTypeID.Name = "colERIssueTypeID";
            this.colERIssueTypeID.OptionsColumn.AllowEdit = false;
            this.colERIssueTypeID.OptionsColumn.AllowFocus = false;
            this.colERIssueTypeID.OptionsColumn.ReadOnly = true;
            this.colERIssueTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colERIssueTypeID.Width = 104;
            // 
            // colERIssueType
            // 
            this.colERIssueType.Caption = "ER Issue Type";
            this.colERIssueType.FieldName = "ERIssueType";
            this.colERIssueType.Name = "colERIssueType";
            this.colERIssueType.OptionsColumn.AllowEdit = false;
            this.colERIssueType.OptionsColumn.AllowFocus = false;
            this.colERIssueType.OptionsColumn.ReadOnly = true;
            this.colERIssueType.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colERIssueType.Visible = true;
            this.colERIssueType.VisibleIndex = 4;
            this.colERIssueType.Width = 90;
            // 
            // colHRManagerID
            // 
            this.colHRManagerID.Caption = "HR Manager ID";
            this.colHRManagerID.FieldName = "HRManagerID";
            this.colHRManagerID.Name = "colHRManagerID";
            this.colHRManagerID.OptionsColumn.AllowEdit = false;
            this.colHRManagerID.OptionsColumn.AllowFocus = false;
            this.colHRManagerID.OptionsColumn.ReadOnly = true;
            this.colHRManagerID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHRManagerID.Width = 94;
            // 
            // colHRManagerName
            // 
            this.colHRManagerName.Caption = "HR Manager";
            this.colHRManagerName.FieldName = "HRManagerName";
            this.colHRManagerName.Name = "colHRManagerName";
            this.colHRManagerName.OptionsColumn.AllowEdit = false;
            this.colHRManagerName.OptionsColumn.AllowFocus = false;
            this.colHRManagerName.OptionsColumn.ReadOnly = true;
            this.colHRManagerName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHRManagerName.Visible = true;
            this.colHRManagerName.VisibleIndex = 5;
            this.colHRManagerName.Width = 80;
            // 
            // colDateRaised
            // 
            this.colDateRaised.Caption = "Date Raised";
            this.colDateRaised.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colDateRaised.FieldName = "DateRaised";
            this.colDateRaised.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateRaised.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDateRaised.Name = "colDateRaised";
            this.colDateRaised.OptionsColumn.AllowEdit = false;
            this.colDateRaised.OptionsColumn.AllowFocus = false;
            this.colDateRaised.OptionsColumn.ReadOnly = true;
            this.colDateRaised.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDateRaised.Visible = true;
            this.colDateRaised.VisibleIndex = 2;
            this.colDateRaised.Width = 100;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colRaisedByPersonID
            // 
            this.colRaisedByPersonID.Caption = "Raised By Person ID";
            this.colRaisedByPersonID.FieldName = "RaisedByPersonID";
            this.colRaisedByPersonID.Name = "colRaisedByPersonID";
            this.colRaisedByPersonID.OptionsColumn.AllowEdit = false;
            this.colRaisedByPersonID.OptionsColumn.AllowFocus = false;
            this.colRaisedByPersonID.OptionsColumn.ReadOnly = true;
            this.colRaisedByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRaisedByPersonID.Width = 118;
            // 
            // colRaisedByPersonName
            // 
            this.colRaisedByPersonName.Caption = "Raised By Person Name";
            this.colRaisedByPersonName.FieldName = "RaisedByPersonName";
            this.colRaisedByPersonName.Name = "colRaisedByPersonName";
            this.colRaisedByPersonName.OptionsColumn.AllowEdit = false;
            this.colRaisedByPersonName.OptionsColumn.AllowFocus = false;
            this.colRaisedByPersonName.OptionsColumn.ReadOnly = true;
            this.colRaisedByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRaisedByPersonName.Visible = true;
            this.colRaisedByPersonName.VisibleIndex = 6;
            this.colRaisedByPersonName.Width = 134;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            this.colStatusID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colStatusValue
            // 
            this.colStatusValue.Caption = "Status";
            this.colStatusValue.FieldName = "StatusValue";
            this.colStatusValue.Name = "colStatusValue";
            this.colStatusValue.OptionsColumn.AllowEdit = false;
            this.colStatusValue.OptionsColumn.AllowFocus = false;
            this.colStatusValue.OptionsColumn.ReadOnly = true;
            this.colStatusValue.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colStatusValue.Visible = true;
            this.colStatusValue.VisibleIndex = 7;
            // 
            // colInvestigationStartDate
            // 
            this.colInvestigationStartDate.Caption = "Investigation Start Date";
            this.colInvestigationStartDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colInvestigationStartDate.FieldName = "InvestigationStartDate";
            this.colInvestigationStartDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colInvestigationStartDate.Name = "colInvestigationStartDate";
            this.colInvestigationStartDate.OptionsColumn.AllowEdit = false;
            this.colInvestigationStartDate.OptionsColumn.AllowFocus = false;
            this.colInvestigationStartDate.OptionsColumn.ReadOnly = true;
            this.colInvestigationStartDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colInvestigationStartDate.Visible = true;
            this.colInvestigationStartDate.VisibleIndex = 9;
            this.colInvestigationStartDate.Width = 137;
            // 
            // colInvestigationEndDate
            // 
            this.colInvestigationEndDate.Caption = "Investigation End Date";
            this.colInvestigationEndDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colInvestigationEndDate.FieldName = "InvestigationEndDate";
            this.colInvestigationEndDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colInvestigationEndDate.Name = "colInvestigationEndDate";
            this.colInvestigationEndDate.OptionsColumn.AllowEdit = false;
            this.colInvestigationEndDate.OptionsColumn.AllowFocus = false;
            this.colInvestigationEndDate.OptionsColumn.ReadOnly = true;
            this.colInvestigationEndDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colInvestigationEndDate.Visible = true;
            this.colInvestigationEndDate.VisibleIndex = 10;
            this.colInvestigationEndDate.Width = 131;
            // 
            // colInvestigatedByPersonID
            // 
            this.colInvestigatedByPersonID.Caption = "Investigating Officer ID";
            this.colInvestigatedByPersonID.FieldName = "InvestigatedByPersonID";
            this.colInvestigatedByPersonID.Name = "colInvestigatedByPersonID";
            this.colInvestigatedByPersonID.OptionsColumn.AllowEdit = false;
            this.colInvestigatedByPersonID.OptionsColumn.AllowFocus = false;
            this.colInvestigatedByPersonID.OptionsColumn.ReadOnly = true;
            this.colInvestigatedByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInvestigatedByPersonID.Width = 134;
            // 
            // colInvestigatedByPersonName
            // 
            this.colInvestigatedByPersonName.Caption = "Investigating Officer";
            this.colInvestigatedByPersonName.FieldName = "InvestigatedByPersonName";
            this.colInvestigatedByPersonName.Name = "colInvestigatedByPersonName";
            this.colInvestigatedByPersonName.OptionsColumn.AllowEdit = false;
            this.colInvestigatedByPersonName.OptionsColumn.AllowFocus = false;
            this.colInvestigatedByPersonName.OptionsColumn.ReadOnly = true;
            this.colInvestigatedByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInvestigatedByPersonName.Visible = true;
            this.colInvestigatedByPersonName.VisibleIndex = 11;
            this.colInvestigatedByPersonName.Width = 120;
            // 
            // colInvestigationOutcomeID
            // 
            this.colInvestigationOutcomeID.Caption = "Investigation Outcome ID";
            this.colInvestigationOutcomeID.FieldName = "InvestigationOutcomeID";
            this.colInvestigationOutcomeID.Name = "colInvestigationOutcomeID";
            this.colInvestigationOutcomeID.OptionsColumn.AllowEdit = false;
            this.colInvestigationOutcomeID.OptionsColumn.AllowFocus = false;
            this.colInvestigationOutcomeID.OptionsColumn.ReadOnly = true;
            this.colInvestigationOutcomeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInvestigationOutcomeID.Width = 144;
            // 
            // colInvestigationOutcome
            // 
            this.colInvestigationOutcome.Caption = "Investigation Outcome";
            this.colInvestigationOutcome.FieldName = "InvestigationOutcome";
            this.colInvestigationOutcome.Name = "colInvestigationOutcome";
            this.colInvestigationOutcome.OptionsColumn.AllowEdit = false;
            this.colInvestigationOutcome.OptionsColumn.AllowFocus = false;
            this.colInvestigationOutcome.OptionsColumn.ReadOnly = true;
            this.colInvestigationOutcome.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colInvestigationOutcome.Visible = true;
            this.colInvestigationOutcome.VisibleIndex = 12;
            this.colInvestigationOutcome.Width = 130;
            // 
            // colHearingDate
            // 
            this.colHearingDate.Caption = "Hearing Date";
            this.colHearingDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colHearingDate.FieldName = "HearingDate";
            this.colHearingDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHearingDate.Name = "colHearingDate";
            this.colHearingDate.OptionsColumn.AllowEdit = false;
            this.colHearingDate.OptionsColumn.AllowFocus = false;
            this.colHearingDate.OptionsColumn.ReadOnly = true;
            this.colHearingDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHearingDate.Visible = true;
            this.colHearingDate.VisibleIndex = 13;
            this.colHearingDate.Width = 84;
            // 
            // colHeardByPersonID
            // 
            this.colHeardByPersonID.Caption = "Heard By Person ID";
            this.colHeardByPersonID.FieldName = "HeardByPersonID";
            this.colHeardByPersonID.Name = "colHeardByPersonID";
            this.colHeardByPersonID.OptionsColumn.AllowEdit = false;
            this.colHeardByPersonID.OptionsColumn.AllowFocus = false;
            this.colHeardByPersonID.OptionsColumn.ReadOnly = true;
            this.colHeardByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeardByPersonID.Width = 115;
            // 
            // colHeardByPersonName
            // 
            this.colHeardByPersonName.Caption = "Heard By Person";
            this.colHeardByPersonName.FieldName = "HeardByPersonName";
            this.colHeardByPersonName.Name = "colHeardByPersonName";
            this.colHeardByPersonName.OptionsColumn.AllowEdit = false;
            this.colHeardByPersonName.OptionsColumn.AllowFocus = false;
            this.colHeardByPersonName.OptionsColumn.ReadOnly = true;
            this.colHeardByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHeardByPersonName.Visible = true;
            this.colHeardByPersonName.VisibleIndex = 14;
            this.colHeardByPersonName.Width = 101;
            // 
            // colHearingNoteTakerName
            // 
            this.colHearingNoteTakerName.Caption = "Hearing Note Taker";
            this.colHearingNoteTakerName.FieldName = "HearingNoteTakerName";
            this.colHearingNoteTakerName.Name = "colHearingNoteTakerName";
            this.colHearingNoteTakerName.OptionsColumn.AllowEdit = false;
            this.colHearingNoteTakerName.OptionsColumn.AllowFocus = false;
            this.colHearingNoteTakerName.OptionsColumn.ReadOnly = true;
            this.colHearingNoteTakerName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingNoteTakerName.Visible = true;
            this.colHearingNoteTakerName.VisibleIndex = 15;
            this.colHearingNoteTakerName.Width = 114;
            // 
            // colHearingEmployeeRepresentativeName
            // 
            this.colHearingEmployeeRepresentativeName.Caption = "Hearing Employee Representative";
            this.colHearingEmployeeRepresentativeName.FieldName = "HearingEmployeeRepresentativeName";
            this.colHearingEmployeeRepresentativeName.Name = "colHearingEmployeeRepresentativeName";
            this.colHearingEmployeeRepresentativeName.OptionsColumn.AllowEdit = false;
            this.colHearingEmployeeRepresentativeName.OptionsColumn.AllowFocus = false;
            this.colHearingEmployeeRepresentativeName.OptionsColumn.ReadOnly = true;
            this.colHearingEmployeeRepresentativeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingEmployeeRepresentativeName.Visible = true;
            this.colHearingEmployeeRepresentativeName.VisibleIndex = 16;
            this.colHearingEmployeeRepresentativeName.Width = 184;
            // 
            // colHearingOutcomeID
            // 
            this.colHearingOutcomeID.Caption = "Hearing Outcome ID";
            this.colHearingOutcomeID.FieldName = "HearingOutcomeID";
            this.colHearingOutcomeID.Name = "colHearingOutcomeID";
            this.colHearingOutcomeID.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeID.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeID.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingOutcomeID.Width = 118;
            // 
            // colHearingOutcome
            // 
            this.colHearingOutcome.Caption = "Hearing Outcome";
            this.colHearingOutcome.FieldName = "HearingOutcome";
            this.colHearingOutcome.Name = "colHearingOutcome";
            this.colHearingOutcome.OptionsColumn.AllowEdit = false;
            this.colHearingOutcome.OptionsColumn.AllowFocus = false;
            this.colHearingOutcome.OptionsColumn.ReadOnly = true;
            this.colHearingOutcome.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingOutcome.Visible = true;
            this.colHearingOutcome.VisibleIndex = 17;
            this.colHearingOutcome.Width = 104;
            // 
            // colHearingOutcomeDate
            // 
            this.colHearingOutcomeDate.Caption = "Hearing Outcome Date";
            this.colHearingOutcomeDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colHearingOutcomeDate.FieldName = "HearingOutcomeDate";
            this.colHearingOutcomeDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colHearingOutcomeDate.Name = "colHearingOutcomeDate";
            this.colHearingOutcomeDate.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeDate.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeDate.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colHearingOutcomeDate.Visible = true;
            this.colHearingOutcomeDate.VisibleIndex = 18;
            this.colHearingOutcomeDate.Width = 130;
            // 
            // colHearingOutcomeIssuedByPersonID
            // 
            this.colHearingOutcomeIssuedByPersonID.Caption = "Hearing Outcome Issued By Person ID";
            this.colHearingOutcomeIssuedByPersonID.FieldName = "HearingOutcomeIssuedByPersonID";
            this.colHearingOutcomeIssuedByPersonID.Name = "colHearingOutcomeIssuedByPersonID";
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeIssuedByPersonID.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeIssuedByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingOutcomeIssuedByPersonID.Width = 204;
            // 
            // colHearingOutcomeIssuedByPersonName
            // 
            this.colHearingOutcomeIssuedByPersonName.Caption = "Hearing Outcome Issued By Person";
            this.colHearingOutcomeIssuedByPersonName.FieldName = "HearingOutcomeIssuedByPersonName";
            this.colHearingOutcomeIssuedByPersonName.Name = "colHearingOutcomeIssuedByPersonName";
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.AllowEdit = false;
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.AllowFocus = false;
            this.colHearingOutcomeIssuedByPersonName.OptionsColumn.ReadOnly = true;
            this.colHearingOutcomeIssuedByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colHearingOutcomeIssuedByPersonName.Visible = true;
            this.colHearingOutcomeIssuedByPersonName.VisibleIndex = 19;
            this.colHearingOutcomeIssuedByPersonName.Width = 190;
            // 
            // colAppealDate
            // 
            this.colAppealDate.Caption = "Appeal Date";
            this.colAppealDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colAppealDate.FieldName = "AppealDate";
            this.colAppealDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colAppealDate.Name = "colAppealDate";
            this.colAppealDate.OptionsColumn.AllowEdit = false;
            this.colAppealDate.OptionsColumn.AllowFocus = false;
            this.colAppealDate.OptionsColumn.ReadOnly = true;
            this.colAppealDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colAppealDate.Visible = true;
            this.colAppealDate.VisibleIndex = 20;
            this.colAppealDate.Width = 80;
            // 
            // colAppealHeardByPersonID
            // 
            this.colAppealHeardByPersonID.Caption = "Appeal Heard By Person ID";
            this.colAppealHeardByPersonID.FieldName = "AppealHeardByPersonID";
            this.colAppealHeardByPersonID.Name = "colAppealHeardByPersonID";
            this.colAppealHeardByPersonID.OptionsColumn.AllowEdit = false;
            this.colAppealHeardByPersonID.OptionsColumn.AllowFocus = false;
            this.colAppealHeardByPersonID.OptionsColumn.ReadOnly = true;
            this.colAppealHeardByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealHeardByPersonID.Width = 151;
            // 
            // colAppealHeardByPersonName
            // 
            this.colAppealHeardByPersonName.Caption = "Appeal Heard By Person";
            this.colAppealHeardByPersonName.FieldName = "AppealHeardByPersonName";
            this.colAppealHeardByPersonName.Name = "colAppealHeardByPersonName";
            this.colAppealHeardByPersonName.OptionsColumn.AllowEdit = false;
            this.colAppealHeardByPersonName.OptionsColumn.AllowFocus = false;
            this.colAppealHeardByPersonName.OptionsColumn.ReadOnly = true;
            this.colAppealHeardByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealHeardByPersonName.Visible = true;
            this.colAppealHeardByPersonName.VisibleIndex = 21;
            this.colAppealHeardByPersonName.Width = 137;
            // 
            // colAppealOutcomeID
            // 
            this.colAppealOutcomeID.Caption = "Appeal Outcome ID";
            this.colAppealOutcomeID.FieldName = "AppealOutcomeID";
            this.colAppealOutcomeID.Name = "colAppealOutcomeID";
            this.colAppealOutcomeID.OptionsColumn.AllowEdit = false;
            this.colAppealOutcomeID.OptionsColumn.AllowFocus = false;
            this.colAppealOutcomeID.OptionsColumn.ReadOnly = true;
            this.colAppealOutcomeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealOutcomeID.Width = 114;
            // 
            // colAppealOutcome
            // 
            this.colAppealOutcome.Caption = "Appeal Outcome";
            this.colAppealOutcome.FieldName = "AppealOutcome";
            this.colAppealOutcome.Name = "colAppealOutcome";
            this.colAppealOutcome.OptionsColumn.AllowEdit = false;
            this.colAppealOutcome.OptionsColumn.AllowFocus = false;
            this.colAppealOutcome.OptionsColumn.ReadOnly = true;
            this.colAppealOutcome.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealOutcome.Visible = true;
            this.colAppealOutcome.VisibleIndex = 22;
            this.colAppealOutcome.Width = 100;
            // 
            // colAppealNoteTakerName
            // 
            this.colAppealNoteTakerName.Caption = "Appeal Note Taker";
            this.colAppealNoteTakerName.FieldName = "AppealNoteTakerName";
            this.colAppealNoteTakerName.Name = "colAppealNoteTakerName";
            this.colAppealNoteTakerName.OptionsColumn.AllowEdit = false;
            this.colAppealNoteTakerName.OptionsColumn.AllowFocus = false;
            this.colAppealNoteTakerName.OptionsColumn.ReadOnly = true;
            this.colAppealNoteTakerName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealNoteTakerName.Visible = true;
            this.colAppealNoteTakerName.VisibleIndex = 23;
            this.colAppealNoteTakerName.Width = 110;
            // 
            // colAppealEmployeeRepresentativeName
            // 
            this.colAppealEmployeeRepresentativeName.Caption = "Appeal Employee Representative";
            this.colAppealEmployeeRepresentativeName.FieldName = "AppealEmployeeRepresentativeName";
            this.colAppealEmployeeRepresentativeName.Name = "colAppealEmployeeRepresentativeName";
            this.colAppealEmployeeRepresentativeName.OptionsColumn.AllowEdit = false;
            this.colAppealEmployeeRepresentativeName.OptionsColumn.AllowFocus = false;
            this.colAppealEmployeeRepresentativeName.OptionsColumn.ReadOnly = true;
            this.colAppealEmployeeRepresentativeName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppealEmployeeRepresentativeName.Visible = true;
            this.colAppealEmployeeRepresentativeName.VisibleIndex = 24;
            this.colAppealEmployeeRepresentativeName.Width = 180;
            // 
            // colExpiryDurationUnits
            // 
            this.colExpiryDurationUnits.Caption = "Expiry Duration Units";
            this.colExpiryDurationUnits.ColumnEdit = this.repositoryItemTextEdit0DP2;
            this.colExpiryDurationUnits.FieldName = "ExpiryDurationUnits";
            this.colExpiryDurationUnits.Name = "colExpiryDurationUnits";
            this.colExpiryDurationUnits.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnits.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnits.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnits.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDurationUnits.Visible = true;
            this.colExpiryDurationUnits.VisibleIndex = 25;
            this.colExpiryDurationUnits.Width = 122;
            // 
            // repositoryItemTextEdit0DP2
            // 
            this.repositoryItemTextEdit0DP2.AutoHeight = false;
            this.repositoryItemTextEdit0DP2.Mask.EditMask = "f0";
            this.repositoryItemTextEdit0DP2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit0DP2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit0DP2.Name = "repositoryItemTextEdit0DP2";
            // 
            // colExpiryDurationUnitDescriptorID
            // 
            this.colExpiryDurationUnitDescriptorID.Caption = "Expiry Unit Descriptor ID";
            this.colExpiryDurationUnitDescriptorID.FieldName = "ExpiryDurationUnitDescriptorID";
            this.colExpiryDurationUnitDescriptorID.Name = "colExpiryDurationUnitDescriptorID";
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnitDescriptorID.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnitDescriptorID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDurationUnitDescriptorID.Width = 139;
            // 
            // colExpiryDurationUnitDescriptor
            // 
            this.colExpiryDurationUnitDescriptor.Caption = "Expiry Unit Descriptor";
            this.colExpiryDurationUnitDescriptor.FieldName = "ExpiryDurationUnitDescriptor";
            this.colExpiryDurationUnitDescriptor.Name = "colExpiryDurationUnitDescriptor";
            this.colExpiryDurationUnitDescriptor.OptionsColumn.AllowEdit = false;
            this.colExpiryDurationUnitDescriptor.OptionsColumn.AllowFocus = false;
            this.colExpiryDurationUnitDescriptor.OptionsColumn.ReadOnly = true;
            this.colExpiryDurationUnitDescriptor.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colExpiryDurationUnitDescriptor.Visible = true;
            this.colExpiryDurationUnitDescriptor.VisibleIndex = 26;
            this.colExpiryDurationUnitDescriptor.Width = 125;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.Caption = "Expiry Date";
            this.colExpiryDate.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 27;
            this.colExpiryDate.Width = 77;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 28;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colLive
            // 
            this.colLive.Caption = "Live";
            this.colLive.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colLive.FieldName = "Live";
            this.colLive.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colLive.Name = "colLive";
            this.colLive.OptionsColumn.AllowEdit = false;
            this.colLive.OptionsColumn.AllowFocus = false;
            this.colLive.OptionsColumn.ReadOnly = true;
            this.colLive.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLive.Visible = true;
            this.colLive.VisibleIndex = 3;
            this.colLive.Width = 40;
            // 
            // colSanctionOutcome
            // 
            this.colSanctionOutcome.Caption = "Outcome";
            this.colSanctionOutcome.FieldName = "SanctionOutcome";
            this.colSanctionOutcome.Name = "colSanctionOutcome";
            this.colSanctionOutcome.OptionsColumn.AllowEdit = false;
            this.colSanctionOutcome.OptionsColumn.AllowFocus = false;
            this.colSanctionOutcome.OptionsColumn.ReadOnly = true;
            this.colSanctionOutcome.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSanctionOutcome.Visible = true;
            this.colSanctionOutcome.VisibleIndex = 8;
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.Caption = "Linked Documents";
            this.colLinkedDocumentCount.ColumnEdit = this.repositoryItemHyperLinkEditLinkedDocumentSanction;
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedDocumentCount.Visible = true;
            this.colLinkedDocumentCount.VisibleIndex = 29;
            this.colLinkedDocumentCount.Width = 107;
            // 
            // repositoryItemHyperLinkEditLinkedDocumentSanction
            // 
            this.repositoryItemHyperLinkEditLinkedDocumentSanction.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocumentSanction.Name = "repositoryItemHyperLinkEditLinkedDocumentSanction";
            this.repositoryItemHyperLinkEditLinkedDocumentSanction.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocumentSanction.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocumentSanction_OpenLink);
            // 
            // colSelected
            // 
            this.colSelected.Caption = "Selected";
            this.colSelected.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDepartment, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiEmployee),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiType, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterSelected, true)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // beiDepartment
            // 
            this.beiDepartment.Caption = "Department(s):";
            this.beiDepartment.Edit = this.repositoryItemPopupContainerEditDepartment;
            this.beiDepartment.EditValue = "No Department Filter";
            this.beiDepartment.EditWidth = 160;
            this.beiDepartment.Id = 31;
            this.beiDepartment.Name = "beiDepartment";
            this.beiDepartment.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Department(s) - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Select one or more department from me to view just those departments. \r\n\r\nSelect " +
    "nothing from me to view all departments.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiDepartment.SuperTip = superToolTip1;
            // 
            // repositoryItemPopupContainerEditDepartment
            // 
            this.repositoryItemPopupContainerEditDepartment.AutoHeight = false;
            this.repositoryItemPopupContainerEditDepartment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDepartment.Name = "repositoryItemPopupContainerEditDepartment";
            this.repositoryItemPopupContainerEditDepartment.PopupControl = this.popupContainerControlDepartments;
            this.repositoryItemPopupContainerEditDepartment.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDepartment_QueryResultValue);
            // 
            // popupContainerControlDepartments
            // 
            this.popupContainerControlDepartments.Controls.Add(this.gridControl2);
            this.popupContainerControlDepartments.Controls.Add(this.btnDepartmentFilterOK);
            this.popupContainerControlDepartments.Location = new System.Drawing.Point(14, 96);
            this.popupContainerControlDepartments.Name = "popupContainerControlDepartments";
            this.popupContainerControlDepartments.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlDepartments.TabIndex = 2;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.spHR00114DepartmentFilterListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(230, 141);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00114DepartmentFilterListBindingSource
            // 
            this.spHR00114DepartmentFilterListBindingSource.DataMember = "sp_HR_00114_Department_Filter_List";
            this.spHR00114DepartmentFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepartmentID,
            this.colBusinessAreaID1,
            this.colDepartmentName,
            this.gridColumn2,
            this.colBusinessAreaName1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBusinessAreaName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartmentName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.Caption = "Department ID";
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentID.Width = 92;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBusinessAreaID1.Width = 102;
            // 
            // colDepartmentName
            // 
            this.colDepartmentName.Caption = "Department Name";
            this.colDepartmentName.FieldName = "DepartmentName";
            this.colDepartmentName.Name = "colDepartmentName";
            this.colDepartmentName.OptionsColumn.AllowEdit = false;
            this.colDepartmentName.OptionsColumn.AllowFocus = false;
            this.colDepartmentName.OptionsColumn.ReadOnly = true;
            this.colDepartmentName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentName.Visible = true;
            this.colDepartmentName.VisibleIndex = 0;
            this.colDepartmentName.Width = 224;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Remarks";
            this.gridColumn2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn2.FieldName = "Remarks";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 175;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area Name";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBusinessAreaName1.Width = 224;
            // 
            // btnDepartmentFilterOK
            // 
            this.btnDepartmentFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDepartmentFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnDepartmentFilterOK.Name = "btnDepartmentFilterOK";
            this.btnDepartmentFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDepartmentFilterOK.TabIndex = 2;
            this.btnDepartmentFilterOK.Text = "OK";
            this.btnDepartmentFilterOK.Click += new System.EventHandler(this.btnDepartmentFilterOK_Click);
            // 
            // beiEmployee
            // 
            this.beiEmployee.Caption = "Employee(s):";
            this.beiEmployee.Edit = this.repositoryItemPopupContainerEditEmployee;
            this.beiEmployee.EditValue = "No Employee Filter";
            this.beiEmployee.EditWidth = 158;
            this.beiEmployee.Id = 32;
            this.beiEmployee.Name = "beiEmployee";
            this.beiEmployee.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Employee(s) - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Select one or more employees from me to view just those employees.\r\n\r\nSelect noth" +
    "ing from me to view all employees.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.beiEmployee.SuperTip = superToolTip2;
            // 
            // repositoryItemPopupContainerEditEmployee
            // 
            this.repositoryItemPopupContainerEditEmployee.AutoHeight = false;
            this.repositoryItemPopupContainerEditEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditEmployee.Name = "repositoryItemPopupContainerEditEmployee";
            this.repositoryItemPopupContainerEditEmployee.PopupControl = this.popupContainerControlEmployees;
            this.repositoryItemPopupContainerEditEmployee.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditEmployee_QueryResultValue);
            // 
            // popupContainerControlEmployees
            // 
            this.popupContainerControlEmployees.Controls.Add(this.gridControl4);
            this.popupContainerControlEmployees.Controls.Add(this.btnEmployeeFilterOK);
            this.popupContainerControlEmployees.Location = new System.Drawing.Point(252, 96);
            this.popupContainerControlEmployees.Name = "popupContainerControlEmployees";
            this.popupContainerControlEmployees.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlEmployees.TabIndex = 5;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.spHR00115EmployeesByDeptFilterListBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(2, 2);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(230, 141);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // spHR00115EmployeesByDeptFilterListBindingSource
            // 
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataMember = "sp_HR_00115_Employees_By_Dept_Filter_List";
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeID,
            this.colEmployeeNumber1,
            this.colTitle,
            this.colFirstname,
            this.colSurname,
            this.colCurrentEmployee,
            this.colRemarks1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colCurrentEmployee;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFirstname, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colEmployeeID
            // 
            this.colEmployeeID.Caption = "Employee ID";
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.Name = "colEmployeeID";
            this.colEmployeeID.OptionsColumn.AllowEdit = false;
            this.colEmployeeID.OptionsColumn.AllowFocus = false;
            this.colEmployeeID.OptionsColumn.ReadOnly = true;
            this.colEmployeeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID.Width = 81;
            // 
            // colEmployeeNumber1
            // 
            this.colEmployeeNumber1.Caption = "Employee #";
            this.colEmployeeNumber1.FieldName = "EmployeeNumber";
            this.colEmployeeNumber1.Name = "colEmployeeNumber1";
            this.colEmployeeNumber1.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber1.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber1.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber1.Visible = true;
            this.colEmployeeNumber1.VisibleIndex = 2;
            this.colEmployeeNumber1.Width = 91;
            // 
            // colTitle
            // 
            this.colTitle.Caption = "Title";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 3;
            this.colTitle.Width = 59;
            // 
            // colFirstname
            // 
            this.colFirstname.Caption = "Forename";
            this.colFirstname.FieldName = "Firstname";
            this.colFirstname.Name = "colFirstname";
            this.colFirstname.OptionsColumn.AllowEdit = false;
            this.colFirstname.OptionsColumn.AllowFocus = false;
            this.colFirstname.OptionsColumn.ReadOnly = true;
            this.colFirstname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFirstname.Visible = true;
            this.colFirstname.VisibleIndex = 1;
            this.colFirstname.Width = 110;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 0;
            this.colSurname.Width = 126;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // btnEmployeeFilterOK
            // 
            this.btnEmployeeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEmployeeFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnEmployeeFilterOK.Name = "btnEmployeeFilterOK";
            this.btnEmployeeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnEmployeeFilterOK.TabIndex = 2;
            this.btnEmployeeFilterOK.Text = "OK";
            this.btnEmployeeFilterOK.Click += new System.EventHandler(this.btnEmployeeFilterOK_Click);
            // 
            // beiType
            // 
            this.beiType.Caption = "Sanction Type(s):";
            this.beiType.Edit = this.repositoryItemPopupContainerEditType;
            this.beiType.EditValue = "No Sanction Type Filter";
            this.beiType.EditWidth = 175;
            this.beiType.Id = 28;
            this.beiType.Name = "beiType";
            this.beiType.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = " Sanction Type(s) - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Select one or more types of discipline \\ grievance from me to view just those typ" +
    "es of data. \r\n\r\nSelect nothing from me to view all records.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.beiType.SuperTip = superToolTip3;
            // 
            // repositoryItemPopupContainerEditType
            // 
            this.repositoryItemPopupContainerEditType.AutoHeight = false;
            this.repositoryItemPopupContainerEditType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditType.Name = "repositoryItemPopupContainerEditType";
            this.repositoryItemPopupContainerEditType.PopupControl = this.popupContainerControlTypes;
            this.repositoryItemPopupContainerEditType.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditType_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Glyph = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bciFilterSelected
            // 
            this.bciFilterSelected.Caption = "Selected";
            this.bciFilterSelected.Glyph = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.Glyph")));
            this.bciFilterSelected.Id = 33;
            this.bciFilterSelected.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.LargeGlyph")));
            this.bciFilterSelected.Name = "bciFilterSelected";
            this.bciFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Filter Selected - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bciFilterSelected.SuperTip = superToolTip4;
            this.bciFilterSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterSelected_CheckedChanged);
            // 
            // repositoryItemCheckEditShowActiveHoilidayYearOnly
            // 
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Caption = "(Tick if Yes)";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Name = "repositoryItemCheckEditShowActiveHoilidayYearOnly";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // sp_HR_00114_Department_Filter_ListTableAdapter
            // 
            this.sp_HR_00114_Department_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter
            // 
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 42);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlEmployees);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlDepartments);
            this.gridSplitContainer1.Panel1.Controls.Add(this.popupContainerControlTypes);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1041, 515);
            this.gridSplitContainer1.TabIndex = 6;
            // 
            // sp_HR_00117_Sanction_Types_List_No_BlankTableAdapter
            // 
            this.sp_HR_00117_Sanction_Types_List_No_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00118_Sanctions_ManagerTableAdapter
            // 
            this.sp_HR_00118_Sanctions_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Sanction_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1041, 557);
            this.Controls.Add(this.gridSplitContainer1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Sanction_Manager";
            this.Text = "Discipline & Grievances Manager";
            this.Activated += new System.EventHandler(this.frm_HR_Sanction_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Sanction_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Sanction_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlTypes)).EndInit();
            this.popupContainerControlTypes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00117SanctionTypesListNoBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00118SanctionsManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit0DP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocumentSanction)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).EndInit();
            this.popupContainerControlDepartments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).EndInit();
            this.popupContainerControlEmployees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlTypes;
        private DevExpress.XtraEditors.SimpleButton btnTypeFilterOK;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem beiType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditType;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveHoilidayYearOnly;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraBars.BarEditItem beiDepartment;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDepartment;
        private DevExpress.XtraBars.BarEditItem beiEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditEmployee;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlEmployees;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.SimpleButton btnEmployeeFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDepartments;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.SimpleButton btnDepartmentFilterOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit0DP2;
        private System.Windows.Forms.BindingSource spHR00114DepartmentFilterListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter sp_HR_00114_Department_Filter_ListTableAdapter;
        private System.Windows.Forms.BindingSource spHR00115EmployeesByDeptFilterListBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private System.Windows.Forms.BindingSource spHR00117SanctionTypesListNoBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00117_Sanction_Types_List_No_BlankTableAdapter sp_HR_00117_Sanction_Types_List_No_BlankTableAdapter;
        private System.Windows.Forms.BindingSource spHR00118SanctionsManagerBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00118_Sanctions_ManagerTableAdapter sp_HR_00118_Sanctions_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSanctionID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber5;
        private DevExpress.XtraGrid.Columns.GridColumn colERIssueTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colERIssueType;
        private DevExpress.XtraGrid.Columns.GridColumn colHRManagerID;
        private DevExpress.XtraGrid.Columns.GridColumn colHRManagerName;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRaised;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRaisedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusValue;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigatedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigatedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colInvestigationOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHeardByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colHeardByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingNoteTakerName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingEmployeeRepresentativeName;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeDate;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeIssuedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colHearingOutcomeIssuedByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealHeardByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealHeardByPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealOutcomeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealNoteTakerName;
        private DevExpress.XtraGrid.Columns.GridColumn colAppealEmployeeRepresentativeName;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnits;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnitDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDurationUnitDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colLive;
        private DevExpress.XtraGrid.Columns.GridColumn colSanctionOutcome;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocumentSanction;
        private DevExpress.XtraBars.BarCheckItem bciFilterSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
    }
}

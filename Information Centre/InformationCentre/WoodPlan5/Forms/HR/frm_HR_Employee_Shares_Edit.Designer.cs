namespace WoodPlan5
{
    partial class frm_HR_Employee_Shares_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Shares_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions3 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.colID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.ShareUnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.spHR00226EmployeeSharesEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ActiveCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.ShareTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00228ShareTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UnitValueSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EmployeeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ShareUnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00229ShareUnitDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EmployeeNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SharesIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00174EmployeePensionContributionListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPensionContributionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPensionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDatePaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEmployeeContributionAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEmployerContributionAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToPension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.EmployeeFirstnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeSurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.DateReceivedDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.ItemForEmployeeFirstname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSharesID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layGrpActions = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpDetails = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForShareTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDateReceived = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForShareUnits = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForShareUnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUnitValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActive = new DevExpress.XtraLayout.LayoutControlItem();
            this.layGrpRemarks = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00226_Employee_Shares_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00226_Employee_Shares_EditTableAdapter();
            this.sp_HR_00228_Share_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00228_Share_Types_With_BlankTableAdapter();
            this.sp_HR_00229_Share_Unit_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00229_Share_Unit_Descriptors_With_BlankTableAdapter();
            this.sp_HR_00174_Employee_Pension_Contribution_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00174_Employee_Pension_Contribution_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShareUnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00226EmployeeSharesEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShareTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00228ShareTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitValueSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShareUnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00229ShareUnitDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SharesIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00174EmployeePensionContributionListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateReceivedDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateReceivedDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSharesID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpActions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShareTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateReceived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShareUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShareUnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(861, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 543);
            this.barDockControlBottom.Size = new System.Drawing.Size(861, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(861, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 517);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colID1
            // 
            this.colID1.Caption = "ID";
            this.colID1.FieldName = "ID";
            this.colID1.Name = "colID1";
            this.colID1.OptionsColumn.AllowEdit = false;
            this.colID1.OptionsColumn.AllowFocus = false;
            this.colID1.OptionsColumn.ReadOnly = true;
            this.colID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colID1.Width = 53;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "ID";
            this.gridColumn2.FieldName = "ID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.ImageOptions.Image")));
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.ImageOptions.Image = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.ImageOptions.Image")));
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageOptions.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageOptions.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Manager = this.barManager2;
            this.barDockControl1.Size = new System.Drawing.Size(861, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 543);
            this.barDockControl2.Manager = this.barManager2;
            this.barDockControl2.Size = new System.Drawing.Size(861, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Manager = this.barManager2;
            this.barDockControl3.Size = new System.Drawing.Size(0, 517);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(861, 26);
            this.barDockControl4.Manager = this.barManager2;
            this.barDockControl4.Size = new System.Drawing.Size(0, 517);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(5, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "BlockAdd_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 7);
            this.imageCollection1.Images.SetKeyName(7, "refresh_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.ShareUnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.ActiveCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.ShareTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.UnitValueSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ShareUnitDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SharesIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.btnSave);
            this.dataLayoutControl1.Controls.Add(this.gridSplitContainer1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.EmployeeFirstnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeSurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.DateReceivedDateEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.DataSource = this.spHR00226EmployeeSharesEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmployeeFirstname,
            this.ItemForEmployeeNumber,
            this.ItemForEmployeeSurname,
            this.ItemForEmployeeID,
            this.ItemForSharesID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(102, 141, 282, 415);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(861, 517);
            this.dataLayoutControl1.TabIndex = 9;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // ShareUnitsSpinEdit
            // 
            this.ShareUnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "ShareUnits", true));
            this.ShareUnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.ShareUnitsSpinEdit.Location = new System.Drawing.Point(145, 189);
            this.ShareUnitsSpinEdit.MenuManager = this.barManager1;
            this.ShareUnitsSpinEdit.Name = "ShareUnitsSpinEdit";
            this.ShareUnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ShareUnitsSpinEdit.Properties.Mask.EditMask = "######0.00";
            this.ShareUnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.ShareUnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.ShareUnitsSpinEdit.Properties.MinValue = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147352576});
            this.ShareUnitsSpinEdit.Size = new System.Drawing.Size(663, 20);
            this.ShareUnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.ShareUnitsSpinEdit.TabIndex = 102;
            // 
            // spHR00226EmployeeSharesEditBindingSource
            // 
            this.spHR00226EmployeeSharesEditBindingSource.DataMember = "sp_HR_00226_Employee_Shares_Edit";
            this.spHR00226EmployeeSharesEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // ActiveCheckEdit
            // 
            this.ActiveCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "Active", true));
            this.ActiveCheckEdit.Location = new System.Drawing.Point(145, 261);
            this.ActiveCheckEdit.MenuManager = this.barManager1;
            this.ActiveCheckEdit.Name = "ActiveCheckEdit";
            this.ActiveCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ActiveCheckEdit.Properties.ValueChecked = 1;
            this.ActiveCheckEdit.Properties.ValueUnchecked = 0;
            this.ActiveCheckEdit.Size = new System.Drawing.Size(663, 19);
            this.ActiveCheckEdit.StyleController = this.dataLayoutControl1;
            this.ActiveCheckEdit.TabIndex = 101;
            // 
            // ShareTypeIDGridLookUpEdit
            // 
            this.ShareTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "ShareTypeID", true));
            this.ShareTypeIDGridLookUpEdit.Location = new System.Drawing.Point(145, 139);
            this.ShareTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ShareTypeIDGridLookUpEdit.Name = "ShareTypeIDGridLookUpEdit";
            editorButtonImageOptions1.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions1.Image")));
            editorButtonImageOptions2.Image = ((System.Drawing.Image)(resources.GetObject("editorButtonImageOptions2.Image")));
            this.ShareTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Edit", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Edit Underlying Data", "edit", null, DevExpress.Utils.ToolTipAnchor.Default),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Reload", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Reload Underlying Data", "reload", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ShareTypeIDGridLookUpEdit.Properties.DataSource = this.spHR00228ShareTypesWithBlankBindingSource;
            this.ShareTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ShareTypeIDGridLookUpEdit.Properties.NullText = "";
            this.ShareTypeIDGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.ShareTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ShareTypeIDGridLookUpEdit.Size = new System.Drawing.Size(663, 22);
            this.ShareTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ShareTypeIDGridLookUpEdit.TabIndex = 100;
            this.ShareTypeIDGridLookUpEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ShareTypeIDGridLookUpEdit_ButtonClick);
            this.ShareTypeIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.ShareTypeIDGridLookUpEdit_Validating);
            // 
            // spHR00228ShareTypesWithBlankBindingSource
            // 
            this.spHR00228ShareTypesWithBlankBindingSource.DataMember = "sp_HR_00228_Share_Types_With_Blank";
            this.spHR00228ShareTypesWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID1,
            this.gridColumn5,
            this.gridColumn6});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colID1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Share Type";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // UnitValueSpinEdit
            // 
            this.UnitValueSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.UnitValueSpinEdit.Location = new System.Drawing.Point(145, 237);
            this.UnitValueSpinEdit.MenuManager = this.barManager1;
            this.UnitValueSpinEdit.Name = "UnitValueSpinEdit";
            this.UnitValueSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UnitValueSpinEdit.Properties.Mask.EditMask = "c";
            this.UnitValueSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.UnitValueSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.UnitValueSpinEdit.Properties.MinValue = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147352576});
            this.UnitValueSpinEdit.Size = new System.Drawing.Size(663, 20);
            this.UnitValueSpinEdit.StyleController = this.dataLayoutControl1;
            this.UnitValueSpinEdit.TabIndex = 95;
            this.UnitValueSpinEdit.EditValueChanged += new System.EventHandler(this.UnitValueSpinEdit_EditValueChanged);
            // 
            // EmployeeIDTextEdit
            // 
            this.EmployeeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "EmployeeID", true));
            this.EmployeeIDTextEdit.Location = new System.Drawing.Point(111, 131);
            this.EmployeeIDTextEdit.MenuManager = this.barManager1;
            this.EmployeeIDTextEdit.Name = "EmployeeIDTextEdit";
            this.EmployeeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIDTextEdit, true);
            this.EmployeeIDTextEdit.Size = new System.Drawing.Size(721, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIDTextEdit, optionsSpelling1);
            this.EmployeeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIDTextEdit.TabIndex = 78;
            // 
            // ShareUnitDescriptorIDGridLookUpEdit
            // 
            this.ShareUnitDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "ShareUnitDescriptorID", true));
            this.ShareUnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(145, 213);
            this.ShareUnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.ShareUnitDescriptorIDGridLookUpEdit.Name = "ShareUnitDescriptorIDGridLookUpEdit";
            this.ShareUnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ShareUnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.spHR00229ShareUnitDescriptorsWithBlankBindingSource;
            this.ShareUnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.ShareUnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.ShareUnitDescriptorIDGridLookUpEdit.Properties.PopupView = this.gridView4;
            this.ShareUnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.ShareUnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(663, 20);
            this.ShareUnitDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.ShareUnitDescriptorIDGridLookUpEdit.TabIndex = 73;
            // 
            // spHR00229ShareUnitDescriptorsWithBlankBindingSource
            // 
            this.spHR00229ShareUnitDescriptorsWithBlankBindingSource.DataMember = "sp_HR_00229_Share_Unit_Descriptors_With_Blank";
            this.spHR00229ShareUnitDescriptorsWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn2;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Unit Descriptor";
            this.gridColumn3.FieldName = "Description";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 220;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Order";
            this.gridColumn4.FieldName = "Order";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // EmployeeNumberTextEdit
            // 
            this.EmployeeNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "EmployeeNumber", true));
            this.EmployeeNumberTextEdit.Location = new System.Drawing.Point(111, 131);
            this.EmployeeNumberTextEdit.MenuManager = this.barManager1;
            this.EmployeeNumberTextEdit.Name = "EmployeeNumberTextEdit";
            this.EmployeeNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNumberTextEdit, true);
            this.EmployeeNumberTextEdit.Size = new System.Drawing.Size(721, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNumberTextEdit, optionsSpelling2);
            this.EmployeeNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNumberTextEdit.TabIndex = 71;
            // 
            // SharesIDTextEdit
            // 
            this.SharesIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "SharesID", true));
            this.SharesIDTextEdit.Location = new System.Drawing.Point(154, 69);
            this.SharesIDTextEdit.MenuManager = this.barManager1;
            this.SharesIDTextEdit.Name = "SharesIDTextEdit";
            this.SharesIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SharesIDTextEdit, true);
            this.SharesIDTextEdit.Size = new System.Drawing.Size(678, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SharesIDTextEdit, optionsSpelling3);
            this.SharesIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SharesIDTextEdit.TabIndex = 70;
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(12, 308);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 22);
            this.btnSave.StyleController = this.dataLayoutControl1;
            this.btnSave.TabIndex = 69;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(24, 368);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(796, 200);
            this.gridSplitContainer1.TabIndex = 79;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00174EmployeePensionContributionListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Reload Data", "reload")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency});
            this.gridControl1.Size = new System.Drawing.Size(796, 200);
            this.gridControl1.TabIndex = 46;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00174EmployeePensionContributionListBindingSource
            // 
            this.spHR00174EmployeePensionContributionListBindingSource.DataMember = "sp_HR_00174_Employee_Pension_Contribution_List";
            this.spHR00174EmployeePensionContributionListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPensionContributionID,
            this.colPensionID,
            this.colDatePaid,
            this.colEmployeeContributionAmount,
            this.colEmployerContributionAmount,
            this.colRemarks,
            this.colLinkedToPension});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.AllowColumnMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowFilterEditor = false;
            this.gridView2.OptionsFilter.AllowFilterIncrementalSearch = false;
            this.gridView2.OptionsFilter.AllowMRUFilterList = false;
            this.gridView2.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;
            this.gridView2.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            this.gridView2.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDatePaid, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView2_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView2_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView2_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView2_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView2_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colPensionContributionID
            // 
            this.colPensionContributionID.Caption = "Pension Contribution ID";
            this.colPensionContributionID.FieldName = "PensionContributionID";
            this.colPensionContributionID.Name = "colPensionContributionID";
            this.colPensionContributionID.OptionsColumn.AllowEdit = false;
            this.colPensionContributionID.OptionsColumn.AllowFocus = false;
            this.colPensionContributionID.OptionsColumn.ReadOnly = true;
            this.colPensionContributionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPensionContributionID.Width = 134;
            // 
            // colPensionID
            // 
            this.colPensionID.Caption = "Pension ID";
            this.colPensionID.FieldName = "PensionID";
            this.colPensionID.Name = "colPensionID";
            this.colPensionID.OptionsColumn.AllowEdit = false;
            this.colPensionID.OptionsColumn.AllowFocus = false;
            this.colPensionID.OptionsColumn.ReadOnly = true;
            this.colPensionID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // colDatePaid
            // 
            this.colDatePaid.Caption = "Date Paid";
            this.colDatePaid.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDatePaid.FieldName = "DatePaid";
            this.colDatePaid.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colDatePaid.Name = "colDatePaid";
            this.colDatePaid.OptionsColumn.AllowEdit = false;
            this.colDatePaid.OptionsColumn.AllowFocus = false;
            this.colDatePaid.OptionsColumn.ReadOnly = true;
            this.colDatePaid.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colDatePaid.Visible = true;
            this.colDatePaid.VisibleIndex = 0;
            this.colDatePaid.Width = 80;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEmployeeContributionAmount
            // 
            this.colEmployeeContributionAmount.Caption = "Employee Contribution";
            this.colEmployeeContributionAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEmployeeContributionAmount.FieldName = "EmployeeContributionAmount";
            this.colEmployeeContributionAmount.Name = "colEmployeeContributionAmount";
            this.colEmployeeContributionAmount.OptionsColumn.AllowEdit = false;
            this.colEmployeeContributionAmount.OptionsColumn.AllowFocus = false;
            this.colEmployeeContributionAmount.OptionsColumn.ReadOnly = true;
            this.colEmployeeContributionAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeContributionAmount.Visible = true;
            this.colEmployeeContributionAmount.VisibleIndex = 1;
            this.colEmployeeContributionAmount.Width = 129;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colEmployerContributionAmount
            // 
            this.colEmployerContributionAmount.Caption = "Employer Contribution";
            this.colEmployerContributionAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colEmployerContributionAmount.FieldName = "EmployerContributionAmount";
            this.colEmployerContributionAmount.Name = "colEmployerContributionAmount";
            this.colEmployerContributionAmount.OptionsColumn.AllowEdit = false;
            this.colEmployerContributionAmount.OptionsColumn.AllowFocus = false;
            this.colEmployerContributionAmount.OptionsColumn.ReadOnly = true;
            this.colEmployerContributionAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployerContributionAmount.Visible = true;
            this.colEmployerContributionAmount.VisibleIndex = 2;
            this.colEmployerContributionAmount.Width = 127;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 3;
            this.colRemarks.Width = 210;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLinkedToPension
            // 
            this.colLinkedToPension.Caption = "Linked To Pension";
            this.colLinkedToPension.FieldName = "LinkedToPension";
            this.colLinkedToPension.Name = "colLinkedToPension";
            this.colLinkedToPension.OptionsColumn.AllowEdit = false;
            this.colLinkedToPension.OptionsColumn.AllowFocus = false;
            this.colLinkedToPension.OptionsColumn.ReadOnly = true;
            this.colLinkedToPension.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colLinkedToPension.Width = 272;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00226EmployeeSharesEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(121, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // EmployeeFirstnameTextEdit
            // 
            this.EmployeeFirstnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "EmployeeFirstname", true));
            this.EmployeeFirstnameTextEdit.Location = new System.Drawing.Point(116, 131);
            this.EmployeeFirstnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeFirstnameTextEdit.Name = "EmployeeFirstnameTextEdit";
            this.EmployeeFirstnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeFirstnameTextEdit, true);
            this.EmployeeFirstnameTextEdit.Size = new System.Drawing.Size(716, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeFirstnameTextEdit, optionsSpelling4);
            this.EmployeeFirstnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeFirstnameTextEdit.TabIndex = 4;
            // 
            // EmployeeSurnameTextEdit
            // 
            this.EmployeeSurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "EmployeeSurname", true));
            this.EmployeeSurnameTextEdit.Location = new System.Drawing.Point(111, 131);
            this.EmployeeSurnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeSurnameTextEdit.Name = "EmployeeSurnameTextEdit";
            this.EmployeeSurnameTextEdit.Properties.MaxLength = 50;
            this.EmployeeSurnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeSurnameTextEdit, true);
            this.EmployeeSurnameTextEdit.Size = new System.Drawing.Size(721, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeSurnameTextEdit, optionsSpelling5);
            this.EmployeeSurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeSurnameTextEdit.TabIndex = 6;
            // 
            // EmployeeNameButtonEdit
            // 
            this.EmployeeNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "EmployeeName", true));
            this.EmployeeNameButtonEdit.Location = new System.Drawing.Point(121, 35);
            this.EmployeeNameButtonEdit.MenuManager = this.barManager1;
            this.EmployeeNameButtonEdit.Name = "EmployeeNameButtonEdit";
            this.EmployeeNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions3, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject9, serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, "Click to Open the Select Employee screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.EmployeeNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EmployeeNameButtonEdit.Size = new System.Drawing.Size(711, 20);
            this.EmployeeNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameButtonEdit.TabIndex = 49;
            // 
            // DateReceivedDateEdit
            // 
            this.DateReceivedDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "DateReceived", true));
            this.DateReceivedDateEdit.EditValue = null;
            this.DateReceivedDateEdit.Location = new System.Drawing.Point(145, 165);
            this.DateReceivedDateEdit.MenuManager = this.barManager1;
            this.DateReceivedDateEdit.Name = "DateReceivedDateEdit";
            this.DateReceivedDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateReceivedDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateReceivedDateEdit.Properties.MaxValue = new System.DateTime(2900, 1, 1, 0, 0, 0, 0);
            this.DateReceivedDateEdit.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.DateReceivedDateEdit.Size = new System.Drawing.Size(663, 20);
            this.DateReceivedDateEdit.StyleController = this.dataLayoutControl1;
            this.DateReceivedDateEdit.TabIndex = 51;
            this.DateReceivedDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.DateReceivedDateEdit_Validating);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00226EmployeeSharesEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 139);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.RemarksMemoEdit.Properties.MaxLength = 32000;
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(772, 141);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling6);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 68;
            // 
            // ItemForEmployeeFirstname
            // 
            this.ItemForEmployeeFirstname.Control = this.EmployeeFirstnameTextEdit;
            this.ItemForEmployeeFirstname.CustomizationFormText = "Employee Forename:";
            this.ItemForEmployeeFirstname.Location = new System.Drawing.Point(0, 119);
            this.ItemForEmployeeFirstname.Name = "ItemForEmployeeFirstname";
            this.ItemForEmployeeFirstname.Size = new System.Drawing.Size(824, 24);
            this.ItemForEmployeeFirstname.Text = "Employee Forename:";
            this.ItemForEmployeeFirstname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeNumber
            // 
            this.ItemForEmployeeNumber.Control = this.EmployeeNumberTextEdit;
            this.ItemForEmployeeNumber.CustomizationFormText = "Employee Number:";
            this.ItemForEmployeeNumber.Location = new System.Drawing.Point(0, 119);
            this.ItemForEmployeeNumber.Name = "ItemForEmployeeNumber";
            this.ItemForEmployeeNumber.Size = new System.Drawing.Size(824, 24);
            this.ItemForEmployeeNumber.Text = "Employee Number:";
            this.ItemForEmployeeNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeSurname
            // 
            this.ItemForEmployeeSurname.Control = this.EmployeeSurnameTextEdit;
            this.ItemForEmployeeSurname.CustomizationFormText = "Employee Surname:";
            this.ItemForEmployeeSurname.Location = new System.Drawing.Point(0, 119);
            this.ItemForEmployeeSurname.Name = "ItemForEmployeeSurname";
            this.ItemForEmployeeSurname.Size = new System.Drawing.Size(824, 24);
            this.ItemForEmployeeSurname.Text = "Employee Surname:";
            this.ItemForEmployeeSurname.TextSize = new System.Drawing.Size(83, 13);
            // 
            // ItemForEmployeeID
            // 
            this.ItemForEmployeeID.Control = this.EmployeeIDTextEdit;
            this.ItemForEmployeeID.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeID.Location = new System.Drawing.Point(0, 119);
            this.ItemForEmployeeID.Name = "ItemForEmployeeID";
            this.ItemForEmployeeID.Size = new System.Drawing.Size(824, 24);
            this.ItemForEmployeeID.Text = "Employee ID:";
            this.ItemForEmployeeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSharesID
            // 
            this.ItemForSharesID.Control = this.SharesIDTextEdit;
            this.ItemForSharesID.CustomizationFormText = "Shares ID:";
            this.ItemForSharesID.Location = new System.Drawing.Point(0, 57);
            this.ItemForSharesID.Name = "ItemForSharesID";
            this.ItemForSharesID.Size = new System.Drawing.Size(824, 24);
            this.ItemForSharesID.Text = "Shares ID:";
            this.ItemForSharesID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(844, 592);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem5,
            this.ItemForEmployeeName,
            this.layoutControlItem3,
            this.layGrpActions,
            this.layoutControlGroup6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(824, 572);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(109, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(109, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(109, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(309, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(515, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(109, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(824, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.AllowHide = false;
            this.ItemForEmployeeName.Control = this.EmployeeNameButtonEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Linked to Employee:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 23);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(824, 24);
            this.ItemForEmployeeName.Text = "Linked to Employee:";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnSave;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 296);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(91, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(824, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layGrpActions
            // 
            this.layGrpActions.AllowHtmlStringInCaption = true;
            this.layGrpActions.CustomizationFormText = "Action / Progress History";
            this.layGrpActions.ExpandButtonVisible = true;
            this.layGrpActions.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpActions.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.layGrpActions.Location = new System.Drawing.Point(0, 322);
            this.layGrpActions.Name = "layGrpActions";
            this.layGrpActions.Size = new System.Drawing.Size(824, 250);
            this.layGrpActions.Text = "Shares Taken  <b>[Future Use]</b>";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridSplitContainer1;
            this.layoutControlItem2.CustomizationFormText = "Linked Restrictions Grid:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(104, 204);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(800, 204);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "Linked Restrictions Grid:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 57);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(824, 239);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpDetails;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(800, 193);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpDetails,
            this.layGrpRemarks});
            // 
            // layGrpDetails
            // 
            this.layGrpDetails.CustomizationFormText = "Details";
            this.layGrpDetails.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForShareTypeID,
            this.ItemForDateReceived,
            this.ItemForShareUnits,
            this.ItemForShareUnitDescriptorID,
            this.ItemForUnitValue,
            this.ItemForActive});
            this.layGrpDetails.Location = new System.Drawing.Point(0, 0);
            this.layGrpDetails.Name = "layGrpDetails";
            this.layGrpDetails.Size = new System.Drawing.Size(776, 145);
            this.layGrpDetails.Text = "Details";
            // 
            // ItemForShareTypeID
            // 
            this.ItemForShareTypeID.Control = this.ShareTypeIDGridLookUpEdit;
            this.ItemForShareTypeID.CustomizationFormText = "Share Type:";
            this.ItemForShareTypeID.Location = new System.Drawing.Point(0, 0);
            this.ItemForShareTypeID.Name = "ItemForShareTypeID";
            this.ItemForShareTypeID.Size = new System.Drawing.Size(776, 26);
            this.ItemForShareTypeID.Text = "Share Type:";
            this.ItemForShareTypeID.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForDateReceived
            // 
            this.ItemForDateReceived.AllowHide = false;
            this.ItemForDateReceived.Control = this.DateReceivedDateEdit;
            this.ItemForDateReceived.CustomizationFormText = "Date Received:";
            this.ItemForDateReceived.Location = new System.Drawing.Point(0, 26);
            this.ItemForDateReceived.Name = "ItemForDateReceived";
            this.ItemForDateReceived.Size = new System.Drawing.Size(776, 24);
            this.ItemForDateReceived.Text = "Date Received:";
            this.ItemForDateReceived.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForShareUnits
            // 
            this.ItemForShareUnits.Control = this.ShareUnitsSpinEdit;
            this.ItemForShareUnits.CustomizationFormText = "Share Units:";
            this.ItemForShareUnits.Location = new System.Drawing.Point(0, 50);
            this.ItemForShareUnits.Name = "ItemForShareUnits";
            this.ItemForShareUnits.Size = new System.Drawing.Size(776, 24);
            this.ItemForShareUnits.Text = "Share Units:";
            this.ItemForShareUnits.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForShareUnitDescriptorID
            // 
            this.ItemForShareUnitDescriptorID.Control = this.ShareUnitDescriptorIDGridLookUpEdit;
            this.ItemForShareUnitDescriptorID.CustomizationFormText = "Share Unit Descriptor:";
            this.ItemForShareUnitDescriptorID.Location = new System.Drawing.Point(0, 74);
            this.ItemForShareUnitDescriptorID.Name = "ItemForShareUnitDescriptorID";
            this.ItemForShareUnitDescriptorID.Size = new System.Drawing.Size(776, 24);
            this.ItemForShareUnitDescriptorID.Text = "Share Unit Descriptor:";
            this.ItemForShareUnitDescriptorID.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForUnitValue
            // 
            this.ItemForUnitValue.Control = this.UnitValueSpinEdit;
            this.ItemForUnitValue.CustomizationFormText = "Minimum Contribution:";
            this.ItemForUnitValue.Location = new System.Drawing.Point(0, 98);
            this.ItemForUnitValue.Name = "ItemForUnitValue";
            this.ItemForUnitValue.Size = new System.Drawing.Size(776, 24);
            this.ItemForUnitValue.Text = "Unit Value:";
            this.ItemForUnitValue.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForActive
            // 
            this.ItemForActive.Control = this.ActiveCheckEdit;
            this.ItemForActive.CustomizationFormText = "Active:";
            this.ItemForActive.Location = new System.Drawing.Point(0, 122);
            this.ItemForActive.Name = "ItemForActive";
            this.ItemForActive.Size = new System.Drawing.Size(776, 23);
            this.ItemForActive.Text = "Active:";
            this.ItemForActive.TextSize = new System.Drawing.Size(106, 13);
            // 
            // layGrpRemarks
            // 
            this.layGrpRemarks.CaptionImageOptions.Image = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layGrpRemarks.CustomizationFormText = "Remarks";
            this.layGrpRemarks.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layGrpRemarks.Location = new System.Drawing.Point(0, 0);
            this.layGrpRemarks.Name = "layGrpRemarks";
            this.layGrpRemarks.Size = new System.Drawing.Size(776, 145);
            this.layGrpRemarks.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(776, 145);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00226_Employee_Shares_EditTableAdapter
            // 
            this.sp_HR_00226_Employee_Shares_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00228_Share_Types_With_BlankTableAdapter
            // 
            this.sp_HR_00228_Share_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00229_Share_Unit_Descriptors_With_BlankTableAdapter
            // 
            this.sp_HR_00229_Share_Unit_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00174_Employee_Pension_Contribution_ListTableAdapter
            // 
            this.sp_HR_00174_Employee_Pension_Contribution_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Employee_Shares_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(861, 573);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Shares_Edit";
            this.Text = "Edit Employee Shares";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Shares_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Shares_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Shares_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ShareUnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00226EmployeeSharesEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActiveCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShareTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00228ShareTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UnitValueSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ShareUnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00229ShareUnitDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SharesIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00174EmployeePensionContributionListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateReceivedDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateReceivedDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSharesID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpActions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShareTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDateReceived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShareUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForShareUnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUnitValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00134_Vetting_Issuers_With_BlankTableAdapter sp_HR_00134_Vetting_Issuers_With_BlankTableAdapter;
        private WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00136_Employee_Vetting_Restriction_ListTableAdapter sp_HR_00136_Employee_Vetting_Restriction_ListTableAdapter;
        private WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00147_Master_Vetting_Types_With_BlankTableAdapter sp_HR_00147_Master_Vetting_Types_With_BlankTableAdapter;
        private WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00150_Master_Vetting_SubTypes_With_BlankTableAdapter sp_HR_00150_Master_Vetting_SubTypes_With_BlankTableAdapter;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.TextEdit EmployeeIDTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit ShareUnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.TextEdit EmployeeNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit SharesIDTextEdit;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraEditors.TextEdit EmployeeFirstnameTextEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeSurnameTextEdit;
        private DevExpress.XtraEditors.ButtonEdit EmployeeNameButtonEdit;
        private DevExpress.XtraEditors.DateEdit DateReceivedDateEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSharesID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeSurname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeFirstname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeNumber;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpDetails;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShareUnitDescriptorID;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpRemarks;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpActions;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDateReceived;
        private DevExpress.XtraEditors.SpinEdit UnitValueSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUnitValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPensionContributionID;
        private DevExpress.XtraGrid.Columns.GridColumn colPensionID;
        private DevExpress.XtraGrid.Columns.GridColumn colDatePaid;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeContributionAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployerContributionAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPension;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.GridLookUpEdit ShareTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colID1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShareTypeID;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private System.Windows.Forms.BindingSource spHR00226EmployeeSharesEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00226_Employee_Shares_EditTableAdapter sp_HR_00226_Employee_Shares_EditTableAdapter;
        private System.Windows.Forms.BindingSource spHR00228ShareTypesWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00228_Share_Types_With_BlankTableAdapter sp_HR_00228_Share_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.CheckEdit ActiveCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActive;
        private DevExpress.XtraEditors.SpinEdit ShareUnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForShareUnits;
        private System.Windows.Forms.BindingSource spHR00229ShareUnitDescriptorsWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00229_Share_Unit_Descriptors_With_BlankTableAdapter sp_HR_00229_Share_Unit_Descriptors_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource spHR00174EmployeePensionContributionListBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00174_Employee_Pension_Contribution_ListTableAdapter sp_HR_00174_Employee_Pension_Contribution_ListTableAdapter;
    }
}

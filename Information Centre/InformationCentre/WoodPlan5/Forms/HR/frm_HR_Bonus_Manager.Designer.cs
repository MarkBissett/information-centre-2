namespace WoodPlan5
{
    partial class frm_HR_Bonus_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Bonus_Manager));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            this.colRecordStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentEmployee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00173EmployeeBonusManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBonusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCounty1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountry1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContinuousServiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupCommencementDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginatingCompany1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferredFrom1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessArea1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTitle1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportsToName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalary = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEligibleForBonus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBonusDatePaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBonusDueDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusAmountPaid2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusAmountPaid3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusAmountPaid4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonusDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonusAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrevBonusDatePaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrevBonusAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextBonusDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextBonusAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasFutureGuranteedBonus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextGuaranteeBonusDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextGuaranteeBonusAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasMultipleDepartments = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemHyperLinkEditLinkedDocuments = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.beiDepartment = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDepartment = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlDepartments = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.spHR00114DepartmentFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDepartmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnDepartmentFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiEmployee = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditEmployee = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlEmployees = new DevExpress.XtraEditors.PopupContainerControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.spHR00115EmployeesByDeptFilterListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.btnEmployeeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.beiActive = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemCheckEditActiveOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.beiDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.popupContainerControlDateRangeFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.dateEditEnd = new DevExpress.XtraEditors.DateEdit();
            this.dateEditStart = new DevExpress.XtraEditors.DateEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnDateRangeFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bciFilterSelected = new DevExpress.XtraBars.BarCheckItem();
            this.bbiExportToExcel = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemPopupContainerEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.sp_HR_00114_Department_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter();
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControlHidden = new DevExpress.XtraGrid.GridControl();
            this.spHR00242ReportsBonusesExportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Reporting = new WoodPlan5.DataSet_HR_Reporting();
            this.gridViewHidden = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colEmployeeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCounty = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountry = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPostCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordStatus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedDocumentCount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSelected1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContinuousServiceDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupCommencementDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOriginatingCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransferredFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartment1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRegion1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayrollType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportsToName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSalary1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.currencyTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEligibleForBonus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBonusDatePaid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBonusDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBonusAmountPaid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusAmountPaid21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusAmountPaid31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusAmountPaid41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonusDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGuaranteeBonusAmount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCurrentBonusRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrevBonusDatePaid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrevBonusAmountPaid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextBonusDueDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextBonusAmountPaid1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasFutureGuranteedBonus1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextGuaranteeBonusDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNextGuaranteeBonusAmount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHasMultipleDepartments1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp_HR_00242_Reports_Bonuses_ExportTableAdapter = new WoodPlan5.DataSet_HR_ReportingTableAdapters.sp_HR_00242_Reports_Bonuses_ExportTableAdapter();
            this.sp_HR_00173_EmployeeBonus_ManagerTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00173_EmployeeBonus_ManagerTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00173EmployeeBonusManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocuments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).BeginInit();
            this.popupContainerControlDepartments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).BeginInit();
            this.popupContainerControlEmployees.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFilter)).BeginInit();
            this.popupContainerControlDateRangeFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHidden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00242ReportsBonusesExportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Reporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHidden)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.currencyTextEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1156, 42);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 557);
            this.barDockControlBottom.Size = new System.Drawing.Size(1156, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 42);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 515);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1156, 42);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 515);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.beiDateRange,
            this.beiDepartment,
            this.beiEmployee,
            this.beiActive,
            this.bciFilterSelected,
            this.bbiExportToExcel});
            this.barManager1.MaxItemId = 76;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemPopupContainerEdit2,
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly,
            this.repositoryItemPopupContainerEditDepartment,
            this.repositoryItemPopupContainerEditEmployee,
            this.repositoryItemCheckEditActiveOnly,
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1});
            // 
            // colRecordStatus
            // 
            this.colRecordStatus.FieldName = "RecordStatus";
            this.colRecordStatus.Name = "colRecordStatus";
            this.colRecordStatus.OptionsColumn.AllowEdit = false;
            this.colRecordStatus.OptionsColumn.AllowFocus = false;
            this.colRecordStatus.OptionsColumn.ReadOnly = true;
            this.colRecordStatus.Visible = true;
            this.colRecordStatus.VisibleIndex = 11;
            this.colRecordStatus.Width = 87;
            // 
            // colCurrentEmployee
            // 
            this.colCurrentEmployee.Caption = "Current Employee";
            this.colCurrentEmployee.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCurrentEmployee.FieldName = "CurrentEmployee";
            this.colCurrentEmployee.Name = "colCurrentEmployee";
            this.colCurrentEmployee.OptionsColumn.AllowEdit = false;
            this.colCurrentEmployee.OptionsColumn.AllowFocus = false;
            this.colCurrentEmployee.OptionsColumn.ReadOnly = true;
            this.colCurrentEmployee.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colCurrentEmployee.Visible = true;
            this.colCurrentEmployee.VisibleIndex = 4;
            this.colCurrentEmployee.Width = 107;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00173EmployeeBonusManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Linked Documents", "linked_document")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5,
            this.repositoryItemCheckEdit4,
            this.repositoryItemTextEditDateTime2,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemHyperLinkEditLinkedDocuments});
            this.gridControl1.Size = new System.Drawing.Size(1156, 515);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00173EmployeeBonusManagerBindingSource
            // 
            this.spHR00173EmployeeBonusManagerBindingSource.DataMember = "sp_HR_00173_EmployeeBonus_Manager";
            this.spHR00173EmployeeBonusManagerBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(4, "linked_documents_16_16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBonusID,
            this.colEmployeeID2,
            this.colEmployeeNumber2,
            this.colTitle2,
            this.colEmployeeName,
            this.colEmployeeSurname,
            this.colEmployeeFirstname,
            this.colAddressLine11,
            this.colAddressLine21,
            this.colAddressLine31,
            this.colCounty1,
            this.colCountry1,
            this.colPostCode1,
            this.colRecordStatus,
            this.colLinkedDocumentCount,
            this.colSelected,
            this.colContinuousServiceDate,
            this.colGroupCommencementDate,
            this.colOriginatingCompany1,
            this.colTransferredFrom1,
            this.colBusinessArea1,
            this.colDepartment,
            this.colLocation,
            this.colRegion,
            this.colPayrollType,
            this.colJobTitle1,
            this.colReportsToName1,
            this.colSalary,
            this.colEligibleForBonus1,
            this.colBonusDatePaid,
            this.colBonusDueDate1,
            this.colCurrentBonusAmountPaid,
            this.colCurrentBonusAmountPaid2,
            this.colCurrentBonusAmountPaid3,
            this.colCurrentBonusAmountPaid4,
            this.colGuaranteeBonusDate,
            this.colGuaranteeBonus,
            this.colGuaranteeBonusAmount,
            this.colCurrentBonusRemarks,
            this.colPrevBonusDatePaid,
            this.colPrevBonusAmountPaid,
            this.colNextBonusDueDate,
            this.colNextBonusAmountPaid,
            this.colHasFutureGuranteedBonus,
            this.colNextGuaranteeBonusDate,
            this.colNextGuaranteeBonusAmount,
            this.colHasMultipleDepartments});
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.colRecordStatus;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.BestFitMaxRowCount = 5;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colBonusID
            // 
            this.colBonusID.FieldName = "BonusID";
            this.colBonusID.Name = "colBonusID";
            this.colBonusID.OptionsColumn.AllowEdit = false;
            this.colBonusID.OptionsColumn.AllowFocus = false;
            this.colBonusID.OptionsColumn.ReadOnly = true;
            // 
            // colEmployeeID2
            // 
            this.colEmployeeID2.FieldName = "EmployeeID";
            this.colEmployeeID2.Name = "colEmployeeID2";
            this.colEmployeeID2.OptionsColumn.AllowEdit = false;
            this.colEmployeeID2.OptionsColumn.AllowFocus = false;
            this.colEmployeeID2.OptionsColumn.ReadOnly = true;
            this.colEmployeeID2.Width = 79;
            // 
            // colEmployeeNumber2
            // 
            this.colEmployeeNumber2.FieldName = "EmployeeNumber";
            this.colEmployeeNumber2.Name = "colEmployeeNumber2";
            this.colEmployeeNumber2.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber2.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber2.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber2.Visible = true;
            this.colEmployeeNumber2.VisibleIndex = 0;
            this.colEmployeeNumber2.Width = 105;
            // 
            // colTitle2
            // 
            this.colTitle2.FieldName = "Title";
            this.colTitle2.Name = "colTitle2";
            this.colTitle2.OptionsColumn.AllowEdit = false;
            this.colTitle2.OptionsColumn.AllowFocus = false;
            this.colTitle2.OptionsColumn.ReadOnly = true;
            this.colTitle2.Visible = true;
            this.colTitle2.VisibleIndex = 1;
            // 
            // colEmployeeName
            // 
            this.colEmployeeName.FieldName = "EmployeeName";
            this.colEmployeeName.Name = "colEmployeeName";
            this.colEmployeeName.OptionsColumn.AllowEdit = false;
            this.colEmployeeName.OptionsColumn.AllowFocus = false;
            this.colEmployeeName.OptionsColumn.ReadOnly = true;
            this.colEmployeeName.Visible = true;
            this.colEmployeeName.VisibleIndex = 2;
            this.colEmployeeName.Width = 95;
            // 
            // colEmployeeSurname
            // 
            this.colEmployeeSurname.FieldName = "EmployeeSurname";
            this.colEmployeeSurname.Name = "colEmployeeSurname";
            this.colEmployeeSurname.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname.Visible = true;
            this.colEmployeeSurname.VisibleIndex = 3;
            this.colEmployeeSurname.Width = 110;
            // 
            // colEmployeeFirstname
            // 
            this.colEmployeeFirstname.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname.Name = "colEmployeeFirstname";
            this.colEmployeeFirstname.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname.Visible = true;
            this.colEmployeeFirstname.VisibleIndex = 4;
            this.colEmployeeFirstname.Width = 115;
            // 
            // colAddressLine11
            // 
            this.colAddressLine11.FieldName = "AddressLine1";
            this.colAddressLine11.Name = "colAddressLine11";
            this.colAddressLine11.OptionsColumn.AllowEdit = false;
            this.colAddressLine11.OptionsColumn.AllowFocus = false;
            this.colAddressLine11.OptionsColumn.ReadOnly = true;
            this.colAddressLine11.Visible = true;
            this.colAddressLine11.VisibleIndex = 5;
            this.colAddressLine11.Width = 86;
            // 
            // colAddressLine21
            // 
            this.colAddressLine21.FieldName = "AddressLine2";
            this.colAddressLine21.Name = "colAddressLine21";
            this.colAddressLine21.OptionsColumn.AllowEdit = false;
            this.colAddressLine21.OptionsColumn.AllowFocus = false;
            this.colAddressLine21.OptionsColumn.ReadOnly = true;
            this.colAddressLine21.Visible = true;
            this.colAddressLine21.VisibleIndex = 6;
            this.colAddressLine21.Width = 86;
            // 
            // colAddressLine31
            // 
            this.colAddressLine31.FieldName = "AddressLine3";
            this.colAddressLine31.Name = "colAddressLine31";
            this.colAddressLine31.OptionsColumn.AllowEdit = false;
            this.colAddressLine31.OptionsColumn.AllowFocus = false;
            this.colAddressLine31.OptionsColumn.ReadOnly = true;
            this.colAddressLine31.Visible = true;
            this.colAddressLine31.VisibleIndex = 7;
            this.colAddressLine31.Width = 86;
            // 
            // colCounty1
            // 
            this.colCounty1.FieldName = "County";
            this.colCounty1.Name = "colCounty1";
            this.colCounty1.OptionsColumn.AllowEdit = false;
            this.colCounty1.OptionsColumn.AllowFocus = false;
            this.colCounty1.OptionsColumn.ReadOnly = true;
            this.colCounty1.Visible = true;
            this.colCounty1.VisibleIndex = 8;
            // 
            // colCountry1
            // 
            this.colCountry1.FieldName = "Country";
            this.colCountry1.Name = "colCountry1";
            this.colCountry1.OptionsColumn.AllowEdit = false;
            this.colCountry1.OptionsColumn.AllowFocus = false;
            this.colCountry1.OptionsColumn.ReadOnly = true;
            this.colCountry1.Visible = true;
            this.colCountry1.VisibleIndex = 9;
            // 
            // colPostCode1
            // 
            this.colPostCode1.FieldName = "PostCode";
            this.colPostCode1.Name = "colPostCode1";
            this.colPostCode1.OptionsColumn.AllowEdit = false;
            this.colPostCode1.OptionsColumn.AllowFocus = false;
            this.colPostCode1.OptionsColumn.ReadOnly = true;
            this.colPostCode1.Visible = true;
            this.colPostCode1.VisibleIndex = 10;
            // 
            // colLinkedDocumentCount
            // 
            this.colLinkedDocumentCount.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount.Name = "colLinkedDocumentCount";
            this.colLinkedDocumentCount.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentCount.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentCount.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount.Visible = true;
            this.colLinkedDocumentCount.VisibleIndex = 12;
            this.colLinkedDocumentCount.Width = 132;
            // 
            // colSelected
            // 
            this.colSelected.FieldName = "Selected";
            this.colSelected.Name = "colSelected";
            this.colSelected.OptionsColumn.AllowEdit = false;
            this.colSelected.OptionsColumn.AllowFocus = false;
            this.colSelected.OptionsColumn.ReadOnly = true;
            this.colSelected.Visible = true;
            this.colSelected.VisibleIndex = 13;
            // 
            // colContinuousServiceDate
            // 
            this.colContinuousServiceDate.FieldName = "ContinuousServiceDate";
            this.colContinuousServiceDate.Name = "colContinuousServiceDate";
            this.colContinuousServiceDate.OptionsColumn.AllowEdit = false;
            this.colContinuousServiceDate.OptionsColumn.AllowFocus = false;
            this.colContinuousServiceDate.OptionsColumn.ReadOnly = true;
            this.colContinuousServiceDate.Visible = true;
            this.colContinuousServiceDate.VisibleIndex = 14;
            this.colContinuousServiceDate.Width = 137;
            // 
            // colGroupCommencementDate
            // 
            this.colGroupCommencementDate.FieldName = "GroupCommencementDate";
            this.colGroupCommencementDate.Name = "colGroupCommencementDate";
            this.colGroupCommencementDate.OptionsColumn.AllowEdit = false;
            this.colGroupCommencementDate.OptionsColumn.AllowFocus = false;
            this.colGroupCommencementDate.OptionsColumn.ReadOnly = true;
            this.colGroupCommencementDate.Visible = true;
            this.colGroupCommencementDate.VisibleIndex = 15;
            this.colGroupCommencementDate.Width = 153;
            // 
            // colOriginatingCompany1
            // 
            this.colOriginatingCompany1.FieldName = "OriginatingCompany";
            this.colOriginatingCompany1.Name = "colOriginatingCompany1";
            this.colOriginatingCompany1.OptionsColumn.AllowEdit = false;
            this.colOriginatingCompany1.OptionsColumn.AllowFocus = false;
            this.colOriginatingCompany1.OptionsColumn.ReadOnly = true;
            this.colOriginatingCompany1.Visible = true;
            this.colOriginatingCompany1.VisibleIndex = 16;
            this.colOriginatingCompany1.Width = 119;
            // 
            // colTransferredFrom1
            // 
            this.colTransferredFrom1.FieldName = "TransferredFrom";
            this.colTransferredFrom1.Name = "colTransferredFrom1";
            this.colTransferredFrom1.OptionsColumn.AllowEdit = false;
            this.colTransferredFrom1.OptionsColumn.AllowFocus = false;
            this.colTransferredFrom1.OptionsColumn.ReadOnly = true;
            this.colTransferredFrom1.Visible = true;
            this.colTransferredFrom1.VisibleIndex = 17;
            this.colTransferredFrom1.Width = 103;
            // 
            // colBusinessArea1
            // 
            this.colBusinessArea1.FieldName = "BusinessArea";
            this.colBusinessArea1.Name = "colBusinessArea1";
            this.colBusinessArea1.OptionsColumn.AllowEdit = false;
            this.colBusinessArea1.OptionsColumn.AllowFocus = false;
            this.colBusinessArea1.OptionsColumn.ReadOnly = true;
            this.colBusinessArea1.Visible = true;
            this.colBusinessArea1.VisibleIndex = 18;
            this.colBusinessArea1.Width = 86;
            // 
            // colDepartment
            // 
            this.colDepartment.FieldName = "Department";
            this.colDepartment.Name = "colDepartment";
            this.colDepartment.OptionsColumn.AllowEdit = false;
            this.colDepartment.OptionsColumn.AllowFocus = false;
            this.colDepartment.OptionsColumn.ReadOnly = true;
            this.colDepartment.Visible = true;
            this.colDepartment.VisibleIndex = 19;
            this.colDepartment.Width = 76;
            // 
            // colLocation
            // 
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 20;
            // 
            // colRegion
            // 
            this.colRegion.FieldName = "Region";
            this.colRegion.Name = "colRegion";
            this.colRegion.OptionsColumn.AllowEdit = false;
            this.colRegion.OptionsColumn.AllowFocus = false;
            this.colRegion.OptionsColumn.ReadOnly = true;
            this.colRegion.Visible = true;
            this.colRegion.VisibleIndex = 21;
            // 
            // colPayrollType
            // 
            this.colPayrollType.FieldName = "PayrollType";
            this.colPayrollType.Name = "colPayrollType";
            this.colPayrollType.OptionsColumn.AllowEdit = false;
            this.colPayrollType.OptionsColumn.AllowFocus = false;
            this.colPayrollType.OptionsColumn.ReadOnly = true;
            this.colPayrollType.Visible = true;
            this.colPayrollType.VisibleIndex = 22;
            this.colPayrollType.Width = 78;
            // 
            // colJobTitle1
            // 
            this.colJobTitle1.FieldName = "JobTitle";
            this.colJobTitle1.Name = "colJobTitle1";
            this.colJobTitle1.OptionsColumn.AllowEdit = false;
            this.colJobTitle1.OptionsColumn.AllowFocus = false;
            this.colJobTitle1.OptionsColumn.ReadOnly = true;
            this.colJobTitle1.Visible = true;
            this.colJobTitle1.VisibleIndex = 23;
            // 
            // colReportsToName1
            // 
            this.colReportsToName1.FieldName = "ReportsToName";
            this.colReportsToName1.Name = "colReportsToName1";
            this.colReportsToName1.OptionsColumn.AllowEdit = false;
            this.colReportsToName1.OptionsColumn.AllowFocus = false;
            this.colReportsToName1.OptionsColumn.ReadOnly = true;
            this.colReportsToName1.Visible = true;
            this.colReportsToName1.VisibleIndex = 24;
            this.colReportsToName1.Width = 102;
            // 
            // colSalary
            // 
            this.colSalary.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colSalary.FieldName = "Salary";
            this.colSalary.Name = "colSalary";
            this.colSalary.OptionsColumn.AllowEdit = false;
            this.colSalary.OptionsColumn.AllowFocus = false;
            this.colSalary.OptionsColumn.ReadOnly = true;
            this.colSalary.Visible = true;
            this.colSalary.VisibleIndex = 25;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colEligibleForBonus1
            // 
            this.colEligibleForBonus1.FieldName = "EligibleForBonus";
            this.colEligibleForBonus1.Name = "colEligibleForBonus1";
            this.colEligibleForBonus1.OptionsColumn.AllowEdit = false;
            this.colEligibleForBonus1.OptionsColumn.AllowFocus = false;
            this.colEligibleForBonus1.OptionsColumn.ReadOnly = true;
            this.colEligibleForBonus1.Visible = true;
            this.colEligibleForBonus1.VisibleIndex = 26;
            this.colEligibleForBonus1.Width = 102;
            // 
            // colBonusDatePaid
            // 
            this.colBonusDatePaid.FieldName = "BonusDatePaid";
            this.colBonusDatePaid.Name = "colBonusDatePaid";
            this.colBonusDatePaid.OptionsColumn.AllowEdit = false;
            this.colBonusDatePaid.OptionsColumn.AllowFocus = false;
            this.colBonusDatePaid.OptionsColumn.ReadOnly = true;
            this.colBonusDatePaid.Visible = true;
            this.colBonusDatePaid.VisibleIndex = 27;
            this.colBonusDatePaid.Width = 97;
            // 
            // colBonusDueDate1
            // 
            this.colBonusDueDate1.FieldName = "BonusDueDate";
            this.colBonusDueDate1.Name = "colBonusDueDate1";
            this.colBonusDueDate1.OptionsColumn.AllowEdit = false;
            this.colBonusDueDate1.OptionsColumn.AllowFocus = false;
            this.colBonusDueDate1.OptionsColumn.ReadOnly = true;
            this.colBonusDueDate1.Visible = true;
            this.colBonusDueDate1.VisibleIndex = 28;
            this.colBonusDueDate1.Width = 96;
            // 
            // colCurrentBonusAmountPaid
            // 
            this.colCurrentBonusAmountPaid.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCurrentBonusAmountPaid.FieldName = "CurrentBonusAmountPaid";
            this.colCurrentBonusAmountPaid.Name = "colCurrentBonusAmountPaid";
            this.colCurrentBonusAmountPaid.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusAmountPaid.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusAmountPaid.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusAmountPaid.Visible = true;
            this.colCurrentBonusAmountPaid.VisibleIndex = 29;
            this.colCurrentBonusAmountPaid.Width = 151;
            // 
            // colCurrentBonusAmountPaid2
            // 
            this.colCurrentBonusAmountPaid2.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCurrentBonusAmountPaid2.FieldName = "CurrentBonusAmountPaid2";
            this.colCurrentBonusAmountPaid2.Name = "colCurrentBonusAmountPaid2";
            this.colCurrentBonusAmountPaid2.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusAmountPaid2.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusAmountPaid2.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusAmountPaid2.Visible = true;
            this.colCurrentBonusAmountPaid2.VisibleIndex = 30;
            this.colCurrentBonusAmountPaid2.Width = 157;
            // 
            // colCurrentBonusAmountPaid3
            // 
            this.colCurrentBonusAmountPaid3.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCurrentBonusAmountPaid3.FieldName = "CurrentBonusAmountPaid3";
            this.colCurrentBonusAmountPaid3.Name = "colCurrentBonusAmountPaid3";
            this.colCurrentBonusAmountPaid3.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusAmountPaid3.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusAmountPaid3.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusAmountPaid3.Visible = true;
            this.colCurrentBonusAmountPaid3.VisibleIndex = 31;
            this.colCurrentBonusAmountPaid3.Width = 157;
            // 
            // colCurrentBonusAmountPaid4
            // 
            this.colCurrentBonusAmountPaid4.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colCurrentBonusAmountPaid4.FieldName = "CurrentBonusAmountPaid4";
            this.colCurrentBonusAmountPaid4.Name = "colCurrentBonusAmountPaid4";
            this.colCurrentBonusAmountPaid4.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusAmountPaid4.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusAmountPaid4.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusAmountPaid4.Visible = true;
            this.colCurrentBonusAmountPaid4.VisibleIndex = 32;
            this.colCurrentBonusAmountPaid4.Width = 157;
            // 
            // colGuaranteeBonusDate
            // 
            this.colGuaranteeBonusDate.FieldName = "GuaranteeBonusDate";
            this.colGuaranteeBonusDate.Name = "colGuaranteeBonusDate";
            this.colGuaranteeBonusDate.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonusDate.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonusDate.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonusDate.Visible = true;
            this.colGuaranteeBonusDate.VisibleIndex = 33;
            this.colGuaranteeBonusDate.Width = 128;
            // 
            // colGuaranteeBonus
            // 
            this.colGuaranteeBonus.FieldName = "GuaranteeBonus";
            this.colGuaranteeBonus.Name = "colGuaranteeBonus";
            this.colGuaranteeBonus.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonus.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonus.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonus.Visible = true;
            this.colGuaranteeBonus.VisibleIndex = 34;
            this.colGuaranteeBonus.Width = 102;
            // 
            // colGuaranteeBonusAmount
            // 
            this.colGuaranteeBonusAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colGuaranteeBonusAmount.FieldName = "GuaranteeBonusAmount";
            this.colGuaranteeBonusAmount.Name = "colGuaranteeBonusAmount";
            this.colGuaranteeBonusAmount.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonusAmount.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonusAmount.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonusAmount.Visible = true;
            this.colGuaranteeBonusAmount.VisibleIndex = 35;
            this.colGuaranteeBonusAmount.Width = 142;
            // 
            // colCurrentBonusRemarks
            // 
            this.colCurrentBonusRemarks.FieldName = "CurrentBonusRemarks";
            this.colCurrentBonusRemarks.Name = "colCurrentBonusRemarks";
            this.colCurrentBonusRemarks.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusRemarks.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusRemarks.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusRemarks.Visible = true;
            this.colCurrentBonusRemarks.VisibleIndex = 36;
            this.colCurrentBonusRemarks.Width = 132;
            // 
            // colPrevBonusDatePaid
            // 
            this.colPrevBonusDatePaid.FieldName = "PrevBonusDatePaid";
            this.colPrevBonusDatePaid.Name = "colPrevBonusDatePaid";
            this.colPrevBonusDatePaid.OptionsColumn.AllowEdit = false;
            this.colPrevBonusDatePaid.OptionsColumn.AllowFocus = false;
            this.colPrevBonusDatePaid.OptionsColumn.ReadOnly = true;
            this.colPrevBonusDatePaid.Visible = true;
            this.colPrevBonusDatePaid.VisibleIndex = 37;
            this.colPrevBonusDatePaid.Width = 122;
            // 
            // colPrevBonusAmountPaid
            // 
            this.colPrevBonusAmountPaid.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colPrevBonusAmountPaid.FieldName = "PrevBonusAmountPaid";
            this.colPrevBonusAmountPaid.Name = "colPrevBonusAmountPaid";
            this.colPrevBonusAmountPaid.OptionsColumn.AllowEdit = false;
            this.colPrevBonusAmountPaid.OptionsColumn.AllowFocus = false;
            this.colPrevBonusAmountPaid.OptionsColumn.ReadOnly = true;
            this.colPrevBonusAmountPaid.Visible = true;
            this.colPrevBonusAmountPaid.VisibleIndex = 38;
            this.colPrevBonusAmountPaid.Width = 136;
            // 
            // colNextBonusDueDate
            // 
            this.colNextBonusDueDate.FieldName = "NextBonusDueDate";
            this.colNextBonusDueDate.Name = "colNextBonusDueDate";
            this.colNextBonusDueDate.OptionsColumn.AllowEdit = false;
            this.colNextBonusDueDate.OptionsColumn.AllowFocus = false;
            this.colNextBonusDueDate.OptionsColumn.ReadOnly = true;
            this.colNextBonusDueDate.Visible = true;
            this.colNextBonusDueDate.VisibleIndex = 39;
            this.colNextBonusDueDate.Width = 122;
            // 
            // colNextBonusAmountPaid
            // 
            this.colNextBonusAmountPaid.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colNextBonusAmountPaid.FieldName = "NextBonusAmountPaid";
            this.colNextBonusAmountPaid.Name = "colNextBonusAmountPaid";
            this.colNextBonusAmountPaid.OptionsColumn.AllowEdit = false;
            this.colNextBonusAmountPaid.OptionsColumn.AllowFocus = false;
            this.colNextBonusAmountPaid.OptionsColumn.ReadOnly = true;
            this.colNextBonusAmountPaid.Visible = true;
            this.colNextBonusAmountPaid.VisibleIndex = 40;
            this.colNextBonusAmountPaid.Width = 137;
            // 
            // colHasFutureGuranteedBonus
            // 
            this.colHasFutureGuranteedBonus.FieldName = "HasFutureGuranteedBonus";
            this.colHasFutureGuranteedBonus.Name = "colHasFutureGuranteedBonus";
            this.colHasFutureGuranteedBonus.OptionsColumn.AllowEdit = false;
            this.colHasFutureGuranteedBonus.OptionsColumn.AllowFocus = false;
            this.colHasFutureGuranteedBonus.OptionsColumn.ReadOnly = true;
            this.colHasFutureGuranteedBonus.Visible = true;
            this.colHasFutureGuranteedBonus.VisibleIndex = 41;
            this.colHasFutureGuranteedBonus.Width = 158;
            // 
            // colNextGuaranteeBonusDate
            // 
            this.colNextGuaranteeBonusDate.FieldName = "NextGuaranteeBonusDate";
            this.colNextGuaranteeBonusDate.Name = "colNextGuaranteeBonusDate";
            this.colNextGuaranteeBonusDate.OptionsColumn.AllowEdit = false;
            this.colNextGuaranteeBonusDate.OptionsColumn.AllowFocus = false;
            this.colNextGuaranteeBonusDate.OptionsColumn.ReadOnly = true;
            this.colNextGuaranteeBonusDate.Visible = true;
            this.colNextGuaranteeBonusDate.VisibleIndex = 42;
            this.colNextGuaranteeBonusDate.Width = 154;
            // 
            // colNextGuaranteeBonusAmount
            // 
            this.colNextGuaranteeBonusAmount.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colNextGuaranteeBonusAmount.FieldName = "NextGuaranteeBonusAmount";
            this.colNextGuaranteeBonusAmount.Name = "colNextGuaranteeBonusAmount";
            this.colNextGuaranteeBonusAmount.OptionsColumn.AllowEdit = false;
            this.colNextGuaranteeBonusAmount.OptionsColumn.AllowFocus = false;
            this.colNextGuaranteeBonusAmount.OptionsColumn.ReadOnly = true;
            this.colNextGuaranteeBonusAmount.Visible = true;
            this.colNextGuaranteeBonusAmount.VisibleIndex = 43;
            this.colNextGuaranteeBonusAmount.Width = 168;
            // 
            // colHasMultipleDepartments
            // 
            this.colHasMultipleDepartments.FieldName = "HasMultipleDepartments";
            this.colHasMultipleDepartments.Name = "colHasMultipleDepartments";
            this.colHasMultipleDepartments.OptionsColumn.AllowEdit = false;
            this.colHasMultipleDepartments.OptionsColumn.AllowFocus = false;
            this.colHasMultipleDepartments.OptionsColumn.ReadOnly = true;
            this.colHasMultipleDepartments.Visible = true;
            this.colHasMultipleDepartments.VisibleIndex = 44;
            this.colHasMultipleDepartments.Width = 141;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // repositoryItemHyperLinkEditLinkedDocuments
            // 
            this.repositoryItemHyperLinkEditLinkedDocuments.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedDocuments.Name = "repositoryItemHyperLinkEditLinkedDocuments";
            this.repositoryItemHyperLinkEditLinkedDocuments.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedDocuments.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedDocuments_OpenLink);
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            this.sp00039GetFormPermissionsForUserBindingSource.DataSource = this.dataSet_AT;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDepartment, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiEmployee),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiActive, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiDateRange, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh),
            new DevExpress.XtraBars.LinkPersistInfo(this.bciFilterSelected, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExportToExcel, true)});
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // beiDepartment
            // 
            this.beiDepartment.Caption = "Department(s):";
            this.beiDepartment.Edit = this.repositoryItemPopupContainerEditDepartment;
            this.beiDepartment.EditValue = "No Department Filter";
            this.beiDepartment.EditWidth = 160;
            this.beiDepartment.Id = 31;
            this.beiDepartment.Name = "beiDepartment";
            this.beiDepartment.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Department(s) - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Select one or more department from me to view just those departments. \r\n\r\nSelect " +
    "nothing from me to view all departments.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.beiDepartment.SuperTip = superToolTip1;
            // 
            // repositoryItemPopupContainerEditDepartment
            // 
            this.repositoryItemPopupContainerEditDepartment.AutoHeight = false;
            this.repositoryItemPopupContainerEditDepartment.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDepartment.Name = "repositoryItemPopupContainerEditDepartment";
            this.repositoryItemPopupContainerEditDepartment.PopupControl = this.popupContainerControlDepartments;
            this.repositoryItemPopupContainerEditDepartment.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDepartment_QueryResultValue);
            // 
            // popupContainerControlDepartments
            // 
            this.popupContainerControlDepartments.Controls.Add(this.gridControl2);
            this.popupContainerControlDepartments.Controls.Add(this.btnDepartmentFilterOK);
            this.popupContainerControlDepartments.Location = new System.Drawing.Point(252, 157);
            this.popupContainerControlDepartments.Name = "popupContainerControlDepartments";
            this.popupContainerControlDepartments.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlDepartments.TabIndex = 3;
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.spHR00114DepartmentFilterListBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(2, 2);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl2.Size = new System.Drawing.Size(230, 141);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // spHR00114DepartmentFilterListBindingSource
            // 
            this.spHR00114DepartmentFilterListBindingSource.DataMember = "sp_HR_00114_Department_Filter_List";
            this.spHR00114DepartmentFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDepartmentID,
            this.colBusinessAreaID1,
            this.colDepartmentName,
            this.gridColumn2,
            this.colBusinessAreaName1});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBusinessAreaName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartmentName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDepartmentID
            // 
            this.colDepartmentID.Caption = "Department ID";
            this.colDepartmentID.FieldName = "DepartmentID";
            this.colDepartmentID.Name = "colDepartmentID";
            this.colDepartmentID.OptionsColumn.AllowEdit = false;
            this.colDepartmentID.OptionsColumn.AllowFocus = false;
            this.colDepartmentID.OptionsColumn.ReadOnly = true;
            this.colDepartmentID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentID.Width = 92;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBusinessAreaID1.Width = 102;
            // 
            // colDepartmentName
            // 
            this.colDepartmentName.Caption = "Department Name";
            this.colDepartmentName.FieldName = "DepartmentName";
            this.colDepartmentName.Name = "colDepartmentName";
            this.colDepartmentName.OptionsColumn.AllowEdit = false;
            this.colDepartmentName.OptionsColumn.AllowFocus = false;
            this.colDepartmentName.OptionsColumn.ReadOnly = true;
            this.colDepartmentName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colDepartmentName.Visible = true;
            this.colDepartmentName.VisibleIndex = 0;
            this.colDepartmentName.Width = 224;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Remarks";
            this.gridColumn2.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.gridColumn2.FieldName = "Remarks";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            this.gridColumn2.Width = 175;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area Name";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colBusinessAreaName1.Width = 224;
            // 
            // btnDepartmentFilterOK
            // 
            this.btnDepartmentFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDepartmentFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnDepartmentFilterOK.Name = "btnDepartmentFilterOK";
            this.btnDepartmentFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDepartmentFilterOK.TabIndex = 2;
            this.btnDepartmentFilterOK.Text = "OK";
            this.btnDepartmentFilterOK.Click += new System.EventHandler(this.btnDepartmentFilterOK_Click);
            // 
            // beiEmployee
            // 
            this.beiEmployee.Caption = "Employee(s):";
            this.beiEmployee.Edit = this.repositoryItemPopupContainerEditEmployee;
            this.beiEmployee.EditValue = "No Employee Filter";
            this.beiEmployee.EditWidth = 158;
            this.beiEmployee.Id = 32;
            this.beiEmployee.Name = "beiEmployee";
            this.beiEmployee.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Employee(s) - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Select one or more employees from me to view just those employees.\r\n\r\nSelect noth" +
    "ing from me to view all employees.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.beiEmployee.SuperTip = superToolTip2;
            // 
            // repositoryItemPopupContainerEditEmployee
            // 
            this.repositoryItemPopupContainerEditEmployee.AutoHeight = false;
            this.repositoryItemPopupContainerEditEmployee.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditEmployee.Name = "repositoryItemPopupContainerEditEmployee";
            this.repositoryItemPopupContainerEditEmployee.PopupControl = this.popupContainerControlEmployees;
            this.repositoryItemPopupContainerEditEmployee.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditEmployee_QueryResultValue);
            // 
            // popupContainerControlEmployees
            // 
            this.popupContainerControlEmployees.Controls.Add(this.gridControl4);
            this.popupContainerControlEmployees.Controls.Add(this.btnEmployeeFilterOK);
            this.popupContainerControlEmployees.Location = new System.Drawing.Point(12, 157);
            this.popupContainerControlEmployees.Name = "popupContainerControlEmployees";
            this.popupContainerControlEmployees.Size = new System.Drawing.Size(234, 172);
            this.popupContainerControlEmployees.TabIndex = 6;
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.spHR00115EmployeesByDeptFilterListBindingSource;
            this.gridControl4.Location = new System.Drawing.Point(2, 2);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit3});
            this.gridControl4.Size = new System.Drawing.Size(230, 141);
            this.gridControl4.TabIndex = 4;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // spHR00115EmployeesByDeptFilterListBindingSource
            // 
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataMember = "sp_HR_00115_Employees_By_Dept_Filter_List";
            this.spHR00115EmployeesByDeptFilterListBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeID,
            this.colEmployeeNumber1,
            this.colTitle,
            this.colFirstname,
            this.colSurname,
            this.colCurrentEmployee,
            this.colRemarks1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colCurrentEmployee;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView4.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFirstname, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colEmployeeID
            // 
            this.colEmployeeID.Caption = "Employee ID";
            this.colEmployeeID.FieldName = "EmployeeID";
            this.colEmployeeID.Name = "colEmployeeID";
            this.colEmployeeID.OptionsColumn.AllowEdit = false;
            this.colEmployeeID.OptionsColumn.AllowFocus = false;
            this.colEmployeeID.OptionsColumn.ReadOnly = true;
            this.colEmployeeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID.Width = 81;
            // 
            // colEmployeeNumber1
            // 
            this.colEmployeeNumber1.Caption = "Employee #";
            this.colEmployeeNumber1.FieldName = "EmployeeNumber";
            this.colEmployeeNumber1.Name = "colEmployeeNumber1";
            this.colEmployeeNumber1.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber1.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber1.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber1.Visible = true;
            this.colEmployeeNumber1.VisibleIndex = 2;
            this.colEmployeeNumber1.Width = 91;
            // 
            // colTitle
            // 
            this.colTitle.Caption = "Title";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 3;
            this.colTitle.Width = 59;
            // 
            // colFirstname
            // 
            this.colFirstname.Caption = "Forename";
            this.colFirstname.FieldName = "Firstname";
            this.colFirstname.Name = "colFirstname";
            this.colFirstname.OptionsColumn.AllowEdit = false;
            this.colFirstname.OptionsColumn.AllowFocus = false;
            this.colFirstname.OptionsColumn.ReadOnly = true;
            this.colFirstname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colFirstname.Visible = true;
            this.colFirstname.VisibleIndex = 1;
            this.colFirstname.Width = 110;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 0;
            this.colSurname.Width = 126;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // btnEmployeeFilterOK
            // 
            this.btnEmployeeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEmployeeFilterOK.Location = new System.Drawing.Point(3, 146);
            this.btnEmployeeFilterOK.Name = "btnEmployeeFilterOK";
            this.btnEmployeeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnEmployeeFilterOK.TabIndex = 2;
            this.btnEmployeeFilterOK.Text = "OK";
            this.btnEmployeeFilterOK.Click += new System.EventHandler(this.btnEmployeeFilterOK_Click);
            // 
            // beiActive
            // 
            this.beiActive.Caption = "Active Only:";
            this.beiActive.Edit = this.repositoryItemCheckEditActiveOnly;
            this.beiActive.EditValue = 1;
            this.beiActive.EditWidth = 20;
            this.beiActive.Id = 33;
            this.beiActive.Name = "beiActive";
            this.beiActive.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // repositoryItemCheckEditActiveOnly
            // 
            this.repositoryItemCheckEditActiveOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditActiveOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditActiveOnly.AutoHeight = false;
            this.repositoryItemCheckEditActiveOnly.Caption = "Check";
            this.repositoryItemCheckEditActiveOnly.Name = "repositoryItemCheckEditActiveOnly";
            this.repositoryItemCheckEditActiveOnly.ValueChecked = 1;
            this.repositoryItemCheckEditActiveOnly.ValueUnchecked = 0;
            // 
            // beiDateRange
            // 
            this.beiDateRange.Caption = "Date Range:";
            this.beiDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.beiDateRange.EditValue = "No Date Range Filter";
            this.beiDateRange.EditWidth = 195;
            this.beiDateRange.Id = 28;
            this.beiDateRange.Name = "beiDateRange";
            this.beiDateRange.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Date Range - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Optionally select a Start and \\ or End Date from me to restrict the date range of" +
    " the data.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.beiDateRange.SuperTip = superToolTip3;
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRangeFilter;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditType_QueryResultValue);
            // 
            // popupContainerControlDateRangeFilter
            // 
            this.popupContainerControlDateRangeFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.popupContainerControlDateRangeFilter.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRangeFilter.Controls.Add(this.btnDateRangeFilterOK);
            this.popupContainerControlDateRangeFilter.Location = new System.Drawing.Point(492, 157);
            this.popupContainerControlDateRangeFilter.Name = "popupContainerControlDateRangeFilter";
            this.popupContainerControlDateRangeFilter.Size = new System.Drawing.Size(278, 112);
            this.popupContainerControlDateRangeFilter.TabIndex = 7;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.dateEditEnd);
            this.groupControl1.Controls.Add(this.dateEditStart);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Location = new System.Drawing.Point(3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(272, 78);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Date Range Filter";
            // 
            // dateEditEnd
            // 
            this.dateEditEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditEnd.EditValue = null;
            this.dateEditEnd.Location = new System.Drawing.Point(65, 51);
            this.dateEditEnd.MenuManager = this.barManager1;
            this.dateEditEnd.Name = "dateEditEnd";
            this.dateEditEnd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditEnd.Properties.Mask.EditMask = "g";
            this.dateEditEnd.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditEnd.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditEnd.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditEnd.Size = new System.Drawing.Size(202, 20);
            this.dateEditEnd.TabIndex = 3;
            // 
            // dateEditStart
            // 
            this.dateEditStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditStart.EditValue = null;
            this.dateEditStart.Location = new System.Drawing.Point(65, 25);
            this.dateEditStart.MenuManager = this.barManager1;
            this.dateEditStart.Name = "dateEditStart";
            this.dateEditStart.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.dateEditStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditStart.Properties.Mask.EditMask = "g";
            this.dateEditStart.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditStart.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.dateEditStart.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditStart.Size = new System.Drawing.Size(202, 20);
            this.dateEditStart.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(7, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(48, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "End Date:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(7, 29);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(54, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Start Date:";
            // 
            // btnDateRangeFilterOK
            // 
            this.btnDateRangeFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeFilterOK.Location = new System.Drawing.Point(3, 86);
            this.btnDateRangeFilterOK.Name = "btnDateRangeFilterOK";
            this.btnDateRangeFilterOK.Size = new System.Drawing.Size(38, 23);
            this.btnDateRangeFilterOK.TabIndex = 2;
            this.btnDateRangeFilterOK.Text = "OK";
            this.btnDateRangeFilterOK.Click += new System.EventHandler(this.btnDateRangeFilterOK_Click);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Glyph = global::WoodPlan5.Properties.Resources.refresh_32x32;
            this.bbiRefresh.Id = 27;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bciFilterSelected
            // 
            this.bciFilterSelected.Caption = "Selected";
            this.bciFilterSelected.Glyph = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.Glyph")));
            this.bciFilterSelected.Id = 74;
            this.bciFilterSelected.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bciFilterSelected.LargeGlyph")));
            this.bciFilterSelected.Name = "bciFilterSelected";
            this.bciFilterSelected.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Filter Selected - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bciFilterSelected.SuperTip = superToolTip4;
            this.bciFilterSelected.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.bciFilterSelected_CheckedChanged);
            // 
            // bbiExportToExcel
            // 
            this.bbiExportToExcel.Caption = "Export";
            this.bbiExportToExcel.Glyph = global::WoodPlan5.Properties.Resources.Export_32x32;
            this.bbiExportToExcel.Id = 75;
            this.bbiExportToExcel.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiExportToExcel.LargeGlyph")));
            this.bbiExportToExcel.Name = "bbiExportToExcel";
            this.bbiExportToExcel.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "Export - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "Click me to Export Bonus Data to an Excel Spreadsheet.";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.bbiExportToExcel.SuperTip = superToolTip5;
            this.bbiExportToExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExportToExcel_ItemClick);
            // 
            // repositoryItemCheckEditShowActiveHoilidayYearOnly
            // 
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.AutoHeight = false;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Caption = "(Tick if Yes)";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.Name = "repositoryItemCheckEditShowActiveHoilidayYearOnly";
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueChecked = 1;
            this.repositoryItemCheckEditShowActiveHoilidayYearOnly.ValueUnchecked = 0;
            // 
            // repositoryItemPopupContainerEdit2
            // 
            this.repositoryItemPopupContainerEdit2.AutoHeight = false;
            this.repositoryItemPopupContainerEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEdit2.Name = "repositoryItemPopupContainerEdit2";
            // 
            // sp_HR_00114_Department_Filter_ListTableAdapter
            // 
            this.sp_HR_00114_Department_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter
            // 
            this.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 42);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControlHidden);
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(1156, 515);
            this.gridSplitContainer1.TabIndex = 8;
            // 
            // gridControlHidden
            // 
            this.gridControlHidden.DataSource = this.spHR00242ReportsBonusesExportBindingSource;
            this.gridControlHidden.Enabled = false;
            this.gridControlHidden.Location = new System.Drawing.Point(10, 300);
            this.gridControlHidden.MainView = this.gridViewHidden;
            this.gridControlHidden.MenuManager = this.barManager1;
            this.gridControlHidden.Name = "gridControlHidden";
            this.gridControlHidden.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.currencyTextEdit});
            this.gridControlHidden.Size = new System.Drawing.Size(300, 150);
            this.gridControlHidden.TabIndex = 1;
            this.gridControlHidden.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewHidden});
            this.gridControlHidden.Visible = false;
            // 
            // spHR00242ReportsBonusesExportBindingSource
            // 
            this.spHR00242ReportsBonusesExportBindingSource.DataMember = "sp_HR_00242_Reports_Bonuses_Export";
            this.spHR00242ReportsBonusesExportBindingSource.DataSource = this.dataSet_HR_Reporting;
            // 
            // dataSet_HR_Reporting
            // 
            this.dataSet_HR_Reporting.DataSetName = "DataSet_HR_Reporting";
            this.dataSet_HR_Reporting.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridViewHidden
            // 
            this.gridViewHidden.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colEmployeeID1,
            this.colEmployeeNumber,
            this.colTitle1,
            this.colEmployeeName1,
            this.colEmployeeSurname1,
            this.colEmployeeFirstname1,
            this.colAddressLine1,
            this.colAddressLine2,
            this.colAddressLine3,
            this.colCounty,
            this.colCountry,
            this.colPostCode,
            this.colRecordStatus1,
            this.colLinkedDocumentCount1,
            this.colSelected1,
            this.colContinuousServiceDate1,
            this.colGroupCommencementDate1,
            this.colOriginatingCompany,
            this.colTransferredFrom,
            this.colBusinessArea,
            this.colDepartment1,
            this.colLocation1,
            this.colRegion1,
            this.colPayrollType1,
            this.colJobTitle,
            this.colReportsToName,
            this.colSalary1,
            this.colEligibleForBonus,
            this.colBonusDatePaid1,
            this.colBonusDueDate,
            this.colBonusAmountPaid,
            this.colCurrentBonusAmountPaid21,
            this.colCurrentBonusAmountPaid31,
            this.colCurrentBonusAmountPaid41,
            this.colGuaranteeBonusDate1,
            this.colGuaranteeBonus1,
            this.colGuaranteeBonusAmount1,
            this.colCurrentBonusRemarks1,
            this.colPrevBonusDatePaid1,
            this.colPrevBonusAmountPaid1,
            this.colNextBonusDueDate1,
            this.colNextBonusAmountPaid1,
            this.colHasFutureGuranteedBonus1,
            this.colNextGuaranteeBonusDate1,
            this.colNextGuaranteeBonusAmount1,
            this.colHasMultipleDepartments1});
            this.gridViewHidden.GridControl = this.gridControlHidden;
            this.gridViewHidden.Name = "gridViewHidden";
            this.gridViewHidden.OptionsView.ColumnAutoWidth = false;
            // 
            // colEmployeeID1
            // 
            this.colEmployeeID1.FieldName = "EmployeeID";
            this.colEmployeeID1.Name = "colEmployeeID1";
            this.colEmployeeID1.OptionsColumn.AllowEdit = false;
            this.colEmployeeID1.OptionsColumn.AllowFocus = false;
            this.colEmployeeID1.OptionsColumn.ReadOnly = true;
            this.colEmployeeID1.Visible = true;
            this.colEmployeeID1.VisibleIndex = 0;
            // 
            // colEmployeeNumber
            // 
            this.colEmployeeNumber.FieldName = "EmployeeNumber";
            this.colEmployeeNumber.Name = "colEmployeeNumber";
            this.colEmployeeNumber.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber.Visible = true;
            this.colEmployeeNumber.VisibleIndex = 1;
            // 
            // colTitle1
            // 
            this.colTitle1.FieldName = "Title";
            this.colTitle1.Name = "colTitle1";
            this.colTitle1.OptionsColumn.AllowEdit = false;
            this.colTitle1.OptionsColumn.AllowFocus = false;
            this.colTitle1.OptionsColumn.ReadOnly = true;
            this.colTitle1.Visible = true;
            this.colTitle1.VisibleIndex = 2;
            // 
            // colEmployeeName1
            // 
            this.colEmployeeName1.FieldName = "EmployeeName";
            this.colEmployeeName1.Name = "colEmployeeName1";
            this.colEmployeeName1.OptionsColumn.AllowEdit = false;
            this.colEmployeeName1.OptionsColumn.AllowFocus = false;
            this.colEmployeeName1.OptionsColumn.ReadOnly = true;
            this.colEmployeeName1.Visible = true;
            this.colEmployeeName1.VisibleIndex = 3;
            // 
            // colEmployeeSurname1
            // 
            this.colEmployeeSurname1.FieldName = "EmployeeSurname";
            this.colEmployeeSurname1.Name = "colEmployeeSurname1";
            this.colEmployeeSurname1.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname1.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname1.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname1.Visible = true;
            this.colEmployeeSurname1.VisibleIndex = 4;
            // 
            // colEmployeeFirstname1
            // 
            this.colEmployeeFirstname1.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname1.Name = "colEmployeeFirstname1";
            this.colEmployeeFirstname1.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname1.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname1.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname1.Visible = true;
            this.colEmployeeFirstname1.VisibleIndex = 5;
            // 
            // colAddressLine1
            // 
            this.colAddressLine1.FieldName = "AddressLine1";
            this.colAddressLine1.Name = "colAddressLine1";
            this.colAddressLine1.OptionsColumn.AllowEdit = false;
            this.colAddressLine1.OptionsColumn.AllowFocus = false;
            this.colAddressLine1.OptionsColumn.ReadOnly = true;
            this.colAddressLine1.Visible = true;
            this.colAddressLine1.VisibleIndex = 6;
            // 
            // colAddressLine2
            // 
            this.colAddressLine2.FieldName = "AddressLine2";
            this.colAddressLine2.Name = "colAddressLine2";
            this.colAddressLine2.OptionsColumn.AllowEdit = false;
            this.colAddressLine2.OptionsColumn.AllowFocus = false;
            this.colAddressLine2.OptionsColumn.ReadOnly = true;
            this.colAddressLine2.Visible = true;
            this.colAddressLine2.VisibleIndex = 7;
            // 
            // colAddressLine3
            // 
            this.colAddressLine3.FieldName = "AddressLine3";
            this.colAddressLine3.Name = "colAddressLine3";
            this.colAddressLine3.OptionsColumn.AllowEdit = false;
            this.colAddressLine3.OptionsColumn.AllowFocus = false;
            this.colAddressLine3.OptionsColumn.ReadOnly = true;
            this.colAddressLine3.Visible = true;
            this.colAddressLine3.VisibleIndex = 8;
            // 
            // colCounty
            // 
            this.colCounty.FieldName = "County";
            this.colCounty.Name = "colCounty";
            this.colCounty.OptionsColumn.AllowEdit = false;
            this.colCounty.OptionsColumn.AllowFocus = false;
            this.colCounty.OptionsColumn.ReadOnly = true;
            this.colCounty.Visible = true;
            this.colCounty.VisibleIndex = 9;
            // 
            // colCountry
            // 
            this.colCountry.FieldName = "Country";
            this.colCountry.Name = "colCountry";
            this.colCountry.OptionsColumn.AllowEdit = false;
            this.colCountry.OptionsColumn.AllowFocus = false;
            this.colCountry.OptionsColumn.ReadOnly = true;
            this.colCountry.Visible = true;
            this.colCountry.VisibleIndex = 10;
            // 
            // colPostCode
            // 
            this.colPostCode.FieldName = "PostCode";
            this.colPostCode.Name = "colPostCode";
            this.colPostCode.OptionsColumn.AllowEdit = false;
            this.colPostCode.OptionsColumn.AllowFocus = false;
            this.colPostCode.OptionsColumn.ReadOnly = true;
            this.colPostCode.Visible = true;
            this.colPostCode.VisibleIndex = 11;
            // 
            // colRecordStatus1
            // 
            this.colRecordStatus1.FieldName = "RecordStatus";
            this.colRecordStatus1.Name = "colRecordStatus1";
            this.colRecordStatus1.OptionsColumn.AllowEdit = false;
            this.colRecordStatus1.OptionsColumn.AllowFocus = false;
            this.colRecordStatus1.OptionsColumn.ReadOnly = true;
            this.colRecordStatus1.Visible = true;
            this.colRecordStatus1.VisibleIndex = 12;
            // 
            // colLinkedDocumentCount1
            // 
            this.colLinkedDocumentCount1.FieldName = "LinkedDocumentCount";
            this.colLinkedDocumentCount1.Name = "colLinkedDocumentCount1";
            this.colLinkedDocumentCount1.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentCount1.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentCount1.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentCount1.Visible = true;
            this.colLinkedDocumentCount1.VisibleIndex = 13;
            // 
            // colSelected1
            // 
            this.colSelected1.FieldName = "Selected";
            this.colSelected1.Name = "colSelected1";
            this.colSelected1.OptionsColumn.AllowEdit = false;
            this.colSelected1.OptionsColumn.AllowFocus = false;
            this.colSelected1.OptionsColumn.ReadOnly = true;
            this.colSelected1.Visible = true;
            this.colSelected1.VisibleIndex = 14;
            // 
            // colContinuousServiceDate1
            // 
            this.colContinuousServiceDate1.FieldName = "ContinuousServiceDate";
            this.colContinuousServiceDate1.Name = "colContinuousServiceDate1";
            this.colContinuousServiceDate1.OptionsColumn.AllowEdit = false;
            this.colContinuousServiceDate1.OptionsColumn.AllowFocus = false;
            this.colContinuousServiceDate1.OptionsColumn.ReadOnly = true;
            this.colContinuousServiceDate1.Visible = true;
            this.colContinuousServiceDate1.VisibleIndex = 15;
            // 
            // colGroupCommencementDate1
            // 
            this.colGroupCommencementDate1.FieldName = "GroupCommencementDate";
            this.colGroupCommencementDate1.Name = "colGroupCommencementDate1";
            this.colGroupCommencementDate1.OptionsColumn.AllowEdit = false;
            this.colGroupCommencementDate1.OptionsColumn.AllowFocus = false;
            this.colGroupCommencementDate1.OptionsColumn.ReadOnly = true;
            this.colGroupCommencementDate1.Visible = true;
            this.colGroupCommencementDate1.VisibleIndex = 16;
            // 
            // colOriginatingCompany
            // 
            this.colOriginatingCompany.FieldName = "OriginatingCompany";
            this.colOriginatingCompany.Name = "colOriginatingCompany";
            this.colOriginatingCompany.OptionsColumn.AllowEdit = false;
            this.colOriginatingCompany.OptionsColumn.AllowFocus = false;
            this.colOriginatingCompany.OptionsColumn.ReadOnly = true;
            this.colOriginatingCompany.Visible = true;
            this.colOriginatingCompany.VisibleIndex = 17;
            // 
            // colTransferredFrom
            // 
            this.colTransferredFrom.FieldName = "TransferredFrom";
            this.colTransferredFrom.Name = "colTransferredFrom";
            this.colTransferredFrom.OptionsColumn.AllowEdit = false;
            this.colTransferredFrom.OptionsColumn.AllowFocus = false;
            this.colTransferredFrom.OptionsColumn.ReadOnly = true;
            this.colTransferredFrom.Visible = true;
            this.colTransferredFrom.VisibleIndex = 18;
            // 
            // colBusinessArea
            // 
            this.colBusinessArea.FieldName = "BusinessArea";
            this.colBusinessArea.Name = "colBusinessArea";
            this.colBusinessArea.OptionsColumn.AllowEdit = false;
            this.colBusinessArea.OptionsColumn.AllowFocus = false;
            this.colBusinessArea.OptionsColumn.ReadOnly = true;
            this.colBusinessArea.Visible = true;
            this.colBusinessArea.VisibleIndex = 19;
            // 
            // colDepartment1
            // 
            this.colDepartment1.FieldName = "Department";
            this.colDepartment1.Name = "colDepartment1";
            this.colDepartment1.OptionsColumn.AllowEdit = false;
            this.colDepartment1.OptionsColumn.AllowFocus = false;
            this.colDepartment1.OptionsColumn.ReadOnly = true;
            this.colDepartment1.Visible = true;
            this.colDepartment1.VisibleIndex = 20;
            // 
            // colLocation1
            // 
            this.colLocation1.FieldName = "Location";
            this.colLocation1.Name = "colLocation1";
            this.colLocation1.OptionsColumn.AllowEdit = false;
            this.colLocation1.OptionsColumn.AllowFocus = false;
            this.colLocation1.OptionsColumn.ReadOnly = true;
            this.colLocation1.Visible = true;
            this.colLocation1.VisibleIndex = 21;
            // 
            // colRegion1
            // 
            this.colRegion1.FieldName = "Region";
            this.colRegion1.Name = "colRegion1";
            this.colRegion1.OptionsColumn.AllowEdit = false;
            this.colRegion1.OptionsColumn.AllowFocus = false;
            this.colRegion1.OptionsColumn.ReadOnly = true;
            this.colRegion1.Visible = true;
            this.colRegion1.VisibleIndex = 22;
            // 
            // colPayrollType1
            // 
            this.colPayrollType1.FieldName = "PayrollType";
            this.colPayrollType1.Name = "colPayrollType1";
            this.colPayrollType1.OptionsColumn.AllowEdit = false;
            this.colPayrollType1.OptionsColumn.AllowFocus = false;
            this.colPayrollType1.OptionsColumn.ReadOnly = true;
            this.colPayrollType1.Visible = true;
            this.colPayrollType1.VisibleIndex = 23;
            // 
            // colJobTitle
            // 
            this.colJobTitle.FieldName = "JobTitle";
            this.colJobTitle.Name = "colJobTitle";
            this.colJobTitle.OptionsColumn.AllowEdit = false;
            this.colJobTitle.OptionsColumn.AllowFocus = false;
            this.colJobTitle.OptionsColumn.ReadOnly = true;
            this.colJobTitle.Visible = true;
            this.colJobTitle.VisibleIndex = 24;
            // 
            // colReportsToName
            // 
            this.colReportsToName.FieldName = "ReportsToName";
            this.colReportsToName.Name = "colReportsToName";
            this.colReportsToName.OptionsColumn.AllowEdit = false;
            this.colReportsToName.OptionsColumn.AllowFocus = false;
            this.colReportsToName.OptionsColumn.ReadOnly = true;
            this.colReportsToName.Visible = true;
            this.colReportsToName.VisibleIndex = 25;
            // 
            // colSalary1
            // 
            this.colSalary1.ColumnEdit = this.currencyTextEdit;
            this.colSalary1.FieldName = "Salary";
            this.colSalary1.Name = "colSalary1";
            this.colSalary1.OptionsColumn.AllowEdit = false;
            this.colSalary1.OptionsColumn.AllowFocus = false;
            this.colSalary1.OptionsColumn.ReadOnly = true;
            this.colSalary1.Visible = true;
            this.colSalary1.VisibleIndex = 26;
            // 
            // currencyTextEdit
            // 
            this.currencyTextEdit.AutoHeight = false;
            this.currencyTextEdit.Mask.EditMask = "c";
            this.currencyTextEdit.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.currencyTextEdit.Mask.UseMaskAsDisplayFormat = true;
            this.currencyTextEdit.Name = "currencyTextEdit";
            // 
            // colEligibleForBonus
            // 
            this.colEligibleForBonus.FieldName = "EligibleForBonus";
            this.colEligibleForBonus.Name = "colEligibleForBonus";
            this.colEligibleForBonus.OptionsColumn.AllowEdit = false;
            this.colEligibleForBonus.OptionsColumn.AllowFocus = false;
            this.colEligibleForBonus.OptionsColumn.ReadOnly = true;
            this.colEligibleForBonus.Visible = true;
            this.colEligibleForBonus.VisibleIndex = 27;
            // 
            // colBonusDatePaid1
            // 
            this.colBonusDatePaid1.FieldName = "BonusDatePaid";
            this.colBonusDatePaid1.Name = "colBonusDatePaid1";
            this.colBonusDatePaid1.OptionsColumn.AllowEdit = false;
            this.colBonusDatePaid1.OptionsColumn.AllowFocus = false;
            this.colBonusDatePaid1.OptionsColumn.ReadOnly = true;
            this.colBonusDatePaid1.Visible = true;
            this.colBonusDatePaid1.VisibleIndex = 28;
            // 
            // colBonusDueDate
            // 
            this.colBonusDueDate.FieldName = "BonusDueDate";
            this.colBonusDueDate.Name = "colBonusDueDate";
            this.colBonusDueDate.OptionsColumn.AllowEdit = false;
            this.colBonusDueDate.OptionsColumn.AllowFocus = false;
            this.colBonusDueDate.OptionsColumn.ReadOnly = true;
            this.colBonusDueDate.Visible = true;
            this.colBonusDueDate.VisibleIndex = 29;
            // 
            // colBonusAmountPaid
            // 
            this.colBonusAmountPaid.ColumnEdit = this.currencyTextEdit;
            this.colBonusAmountPaid.FieldName = "BonusAmountPaid";
            this.colBonusAmountPaid.Name = "colBonusAmountPaid";
            this.colBonusAmountPaid.OptionsColumn.AllowEdit = false;
            this.colBonusAmountPaid.OptionsColumn.AllowFocus = false;
            this.colBonusAmountPaid.OptionsColumn.ReadOnly = true;
            this.colBonusAmountPaid.Visible = true;
            this.colBonusAmountPaid.VisibleIndex = 30;
            // 
            // colCurrentBonusAmountPaid21
            // 
            this.colCurrentBonusAmountPaid21.ColumnEdit = this.currencyTextEdit;
            this.colCurrentBonusAmountPaid21.FieldName = "CurrentBonusAmountPaid2";
            this.colCurrentBonusAmountPaid21.Name = "colCurrentBonusAmountPaid21";
            this.colCurrentBonusAmountPaid21.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusAmountPaid21.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusAmountPaid21.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusAmountPaid21.Visible = true;
            this.colCurrentBonusAmountPaid21.VisibleIndex = 31;
            // 
            // colCurrentBonusAmountPaid31
            // 
            this.colCurrentBonusAmountPaid31.ColumnEdit = this.currencyTextEdit;
            this.colCurrentBonusAmountPaid31.FieldName = "CurrentBonusAmountPaid3";
            this.colCurrentBonusAmountPaid31.Name = "colCurrentBonusAmountPaid31";
            this.colCurrentBonusAmountPaid31.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusAmountPaid31.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusAmountPaid31.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusAmountPaid31.Visible = true;
            this.colCurrentBonusAmountPaid31.VisibleIndex = 32;
            // 
            // colCurrentBonusAmountPaid41
            // 
            this.colCurrentBonusAmountPaid41.ColumnEdit = this.currencyTextEdit;
            this.colCurrentBonusAmountPaid41.FieldName = "CurrentBonusAmountPaid4";
            this.colCurrentBonusAmountPaid41.Name = "colCurrentBonusAmountPaid41";
            this.colCurrentBonusAmountPaid41.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusAmountPaid41.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusAmountPaid41.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusAmountPaid41.Visible = true;
            this.colCurrentBonusAmountPaid41.VisibleIndex = 33;
            // 
            // colGuaranteeBonusDate1
            // 
            this.colGuaranteeBonusDate1.FieldName = "GuaranteeBonusDate";
            this.colGuaranteeBonusDate1.Name = "colGuaranteeBonusDate1";
            this.colGuaranteeBonusDate1.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonusDate1.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonusDate1.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonusDate1.Visible = true;
            this.colGuaranteeBonusDate1.VisibleIndex = 34;
            // 
            // colGuaranteeBonus1
            // 
            this.colGuaranteeBonus1.FieldName = "GuaranteeBonus";
            this.colGuaranteeBonus1.Name = "colGuaranteeBonus1";
            this.colGuaranteeBonus1.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonus1.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonus1.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonus1.Visible = true;
            this.colGuaranteeBonus1.VisibleIndex = 35;
            // 
            // colGuaranteeBonusAmount1
            // 
            this.colGuaranteeBonusAmount1.ColumnEdit = this.currencyTextEdit;
            this.colGuaranteeBonusAmount1.FieldName = "GuaranteeBonusAmount";
            this.colGuaranteeBonusAmount1.Name = "colGuaranteeBonusAmount1";
            this.colGuaranteeBonusAmount1.OptionsColumn.AllowEdit = false;
            this.colGuaranteeBonusAmount1.OptionsColumn.AllowFocus = false;
            this.colGuaranteeBonusAmount1.OptionsColumn.ReadOnly = true;
            this.colGuaranteeBonusAmount1.Visible = true;
            this.colGuaranteeBonusAmount1.VisibleIndex = 36;
            // 
            // colCurrentBonusRemarks1
            // 
            this.colCurrentBonusRemarks1.FieldName = "CurrentBonusRemarks";
            this.colCurrentBonusRemarks1.Name = "colCurrentBonusRemarks1";
            this.colCurrentBonusRemarks1.OptionsColumn.AllowEdit = false;
            this.colCurrentBonusRemarks1.OptionsColumn.AllowFocus = false;
            this.colCurrentBonusRemarks1.OptionsColumn.ReadOnly = true;
            this.colCurrentBonusRemarks1.Visible = true;
            this.colCurrentBonusRemarks1.VisibleIndex = 37;
            // 
            // colPrevBonusDatePaid1
            // 
            this.colPrevBonusDatePaid1.FieldName = "PrevBonusDatePaid";
            this.colPrevBonusDatePaid1.Name = "colPrevBonusDatePaid1";
            this.colPrevBonusDatePaid1.OptionsColumn.AllowEdit = false;
            this.colPrevBonusDatePaid1.OptionsColumn.AllowFocus = false;
            this.colPrevBonusDatePaid1.OptionsColumn.ReadOnly = true;
            this.colPrevBonusDatePaid1.Visible = true;
            this.colPrevBonusDatePaid1.VisibleIndex = 38;
            // 
            // colPrevBonusAmountPaid1
            // 
            this.colPrevBonusAmountPaid1.ColumnEdit = this.currencyTextEdit;
            this.colPrevBonusAmountPaid1.FieldName = "PrevBonusAmountPaid";
            this.colPrevBonusAmountPaid1.Name = "colPrevBonusAmountPaid1";
            this.colPrevBonusAmountPaid1.OptionsColumn.AllowEdit = false;
            this.colPrevBonusAmountPaid1.OptionsColumn.AllowFocus = false;
            this.colPrevBonusAmountPaid1.OptionsColumn.ReadOnly = true;
            this.colPrevBonusAmountPaid1.Visible = true;
            this.colPrevBonusAmountPaid1.VisibleIndex = 39;
            // 
            // colNextBonusDueDate1
            // 
            this.colNextBonusDueDate1.FieldName = "NextBonusDueDate";
            this.colNextBonusDueDate1.Name = "colNextBonusDueDate1";
            this.colNextBonusDueDate1.OptionsColumn.AllowEdit = false;
            this.colNextBonusDueDate1.OptionsColumn.AllowFocus = false;
            this.colNextBonusDueDate1.OptionsColumn.ReadOnly = true;
            this.colNextBonusDueDate1.Visible = true;
            this.colNextBonusDueDate1.VisibleIndex = 40;
            // 
            // colNextBonusAmountPaid1
            // 
            this.colNextBonusAmountPaid1.ColumnEdit = this.currencyTextEdit;
            this.colNextBonusAmountPaid1.FieldName = "NextBonusAmountPaid";
            this.colNextBonusAmountPaid1.Name = "colNextBonusAmountPaid1";
            this.colNextBonusAmountPaid1.OptionsColumn.AllowEdit = false;
            this.colNextBonusAmountPaid1.OptionsColumn.AllowFocus = false;
            this.colNextBonusAmountPaid1.OptionsColumn.ReadOnly = true;
            this.colNextBonusAmountPaid1.Visible = true;
            this.colNextBonusAmountPaid1.VisibleIndex = 41;
            // 
            // colHasFutureGuranteedBonus1
            // 
            this.colHasFutureGuranteedBonus1.FieldName = "HasFutureGuranteedBonus";
            this.colHasFutureGuranteedBonus1.Name = "colHasFutureGuranteedBonus1";
            this.colHasFutureGuranteedBonus1.OptionsColumn.AllowEdit = false;
            this.colHasFutureGuranteedBonus1.OptionsColumn.AllowFocus = false;
            this.colHasFutureGuranteedBonus1.OptionsColumn.ReadOnly = true;
            this.colHasFutureGuranteedBonus1.Visible = true;
            this.colHasFutureGuranteedBonus1.VisibleIndex = 42;
            // 
            // colNextGuaranteeBonusDate1
            // 
            this.colNextGuaranteeBonusDate1.FieldName = "NextGuaranteeBonusDate";
            this.colNextGuaranteeBonusDate1.Name = "colNextGuaranteeBonusDate1";
            this.colNextGuaranteeBonusDate1.OptionsColumn.AllowEdit = false;
            this.colNextGuaranteeBonusDate1.OptionsColumn.AllowFocus = false;
            this.colNextGuaranteeBonusDate1.OptionsColumn.ReadOnly = true;
            this.colNextGuaranteeBonusDate1.Visible = true;
            this.colNextGuaranteeBonusDate1.VisibleIndex = 43;
            // 
            // colNextGuaranteeBonusAmount1
            // 
            this.colNextGuaranteeBonusAmount1.ColumnEdit = this.currencyTextEdit;
            this.colNextGuaranteeBonusAmount1.FieldName = "NextGuaranteeBonusAmount";
            this.colNextGuaranteeBonusAmount1.Name = "colNextGuaranteeBonusAmount1";
            this.colNextGuaranteeBonusAmount1.OptionsColumn.AllowEdit = false;
            this.colNextGuaranteeBonusAmount1.OptionsColumn.AllowFocus = false;
            this.colNextGuaranteeBonusAmount1.OptionsColumn.ReadOnly = true;
            this.colNextGuaranteeBonusAmount1.Visible = true;
            this.colNextGuaranteeBonusAmount1.VisibleIndex = 44;
            // 
            // colHasMultipleDepartments1
            // 
            this.colHasMultipleDepartments1.FieldName = "HasMultipleDepartments";
            this.colHasMultipleDepartments1.Name = "colHasMultipleDepartments1";
            this.colHasMultipleDepartments1.OptionsColumn.AllowEdit = false;
            this.colHasMultipleDepartments1.OptionsColumn.AllowFocus = false;
            this.colHasMultipleDepartments1.OptionsColumn.ReadOnly = true;
            this.colHasMultipleDepartments1.Visible = true;
            this.colHasMultipleDepartments1.VisibleIndex = 45;
            // 
            // sp_HR_00242_Reports_Bonuses_ExportTableAdapter
            // 
            this.sp_HR_00242_Reports_Bonuses_ExportTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00173_EmployeeBonus_ManagerTableAdapter
            // 
            this.sp_HR_00173_EmployeeBonus_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Bonus_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1156, 557);
            this.Controls.Add(this.popupContainerControlDateRangeFilter);
            this.Controls.Add(this.popupContainerControlDepartments);
            this.Controls.Add(this.popupContainerControlEmployees);
            this.Controls.Add(this.gridSplitContainer1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Bonus_Manager";
            this.Text = "Bonus Export Manger";
            this.Activated += new System.EventHandler(this.frm_HR_Bonus_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Bonus_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Bonus_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            this.Controls.SetChildIndex(this.popupContainerControlEmployees, 0);
            this.Controls.SetChildIndex(this.popupContainerControlDepartments, 0);
            this.Controls.SetChildIndex(this.popupContainerControlDateRangeFilter, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00173EmployeeBonusManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedDocuments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDepartment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDepartments)).EndInit();
            this.popupContainerControlDepartments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00114DepartmentFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlEmployees)).EndInit();
            this.popupContainerControlEmployees.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00115EmployeesByDeptFilterListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditActiveOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRangeFilter)).EndInit();
            this.popupContainerControlDateRangeFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditShowActiveHoilidayYearOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHidden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00242ReportsBonusesExportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Reporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewHidden)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.currencyTextEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarEditItem beiDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditShowActiveHoilidayYearOnly;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraBars.BarEditItem beiDepartment;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDepartment;
        private DevExpress.XtraBars.BarEditItem beiEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private System.Windows.Forms.BindingSource spHR00114DepartmentFilterListBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00114_Department_Filter_ListTableAdapter sp_HR_00114_Department_Filter_ListTableAdapter;
        private System.Windows.Forms.BindingSource spHR00115EmployeesByDeptFilterListBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter sp_HR_00115_Employees_By_Dept_Filter_ListTableAdapter;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraBars.BarEditItem beiActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditActiveOnly;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDepartments;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentName;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DevExpress.XtraEditors.SimpleButton btnDepartmentFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlEmployees;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentEmployee;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraEditors.SimpleButton btnEmployeeFilterOK;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRangeFilter;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.DateEdit dateEditEnd;
        private DevExpress.XtraEditors.DateEdit dateEditStart;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeFilterOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedDocuments;
        private DevExpress.XtraBars.BarCheckItem bciFilterSelected;
        private DevExpress.XtraBars.BarButtonItem bbiExportToExcel;
        private DevExpress.XtraGrid.GridControl gridControlHidden;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewHidden;
        private DataSet_HR_Reporting dataSet_HR_Reporting;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber2;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle2;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine11;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine21;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine31;
        private DevExpress.XtraGrid.Columns.GridColumn colCounty1;
        private DevExpress.XtraGrid.Columns.GridColumn colCountry1;
        private DevExpress.XtraGrid.Columns.GridColumn colPostCode1;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colContinuousServiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupCommencementDate;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginatingCompany1;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferredFrom1;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessArea1;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollType;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTitle1;
        private DevExpress.XtraGrid.Columns.GridColumn colReportsToName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSalary;
        private DevExpress.XtraGrid.Columns.GridColumn colEligibleForBonus1;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusDatePaid;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusDueDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusAmountPaid2;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusAmountPaid3;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusAmountPaid4;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonusDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonus;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonusAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colPrevBonusDatePaid;
        private DevExpress.XtraGrid.Columns.GridColumn colPrevBonusAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colNextBonusDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNextBonusAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colHasFutureGuranteedBonus;
        private DevExpress.XtraGrid.Columns.GridColumn colNextGuaranteeBonusDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNextGuaranteeBonusAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colHasMultipleDepartments;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname1;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colCounty;
        private DevExpress.XtraGrid.Columns.GridColumn colCountry;
        private DevExpress.XtraGrid.Columns.GridColumn colPostCode;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordStatus1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentCount1;
        private DevExpress.XtraGrid.Columns.GridColumn colSelected1;
        private DevExpress.XtraGrid.Columns.GridColumn colContinuousServiceDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupCommencementDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colOriginatingCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colTransferredFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessArea;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartment1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation1;
        private DevExpress.XtraGrid.Columns.GridColumn colRegion1;
        private DevExpress.XtraGrid.Columns.GridColumn colPayrollType1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colReportsToName;
        private DevExpress.XtraGrid.Columns.GridColumn colSalary1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit currencyTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colEligibleForBonus;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusDatePaid1;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colBonusAmountPaid;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusAmountPaid21;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusAmountPaid31;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusAmountPaid41;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonusDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonus1;
        private DevExpress.XtraGrid.Columns.GridColumn colGuaranteeBonusAmount1;
        private DevExpress.XtraGrid.Columns.GridColumn colCurrentBonusRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrevBonusDatePaid1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrevBonusAmountPaid1;
        private DevExpress.XtraGrid.Columns.GridColumn colNextBonusDueDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colNextBonusAmountPaid1;
        private DevExpress.XtraGrid.Columns.GridColumn colHasFutureGuranteedBonus1;
        private DevExpress.XtraGrid.Columns.GridColumn colNextGuaranteeBonusDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colNextGuaranteeBonusAmount1;
        private DevExpress.XtraGrid.Columns.GridColumn colHasMultipleDepartments1;
        private System.Windows.Forms.BindingSource spHR00242ReportsBonusesExportBindingSource;
        private DataSet_HR_ReportingTableAdapters.sp_HR_00242_Reports_Bonuses_ExportTableAdapter sp_HR_00242_Reports_Bonuses_ExportTableAdapter;
        private System.Windows.Forms.BindingSource spHR00173EmployeeBonusManagerBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00173_EmployeeBonus_ManagerTableAdapter sp_HR_00173_EmployeeBonus_ManagerTableAdapter;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_HR_Select_Employee_Absence : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int intPassedInParentID = 0;
        public int intPassedInChildID = 0;
        public int intSelectedParentID = 0;
        public int intSelectedChildID = 0;
        public string strSelectedParentName = "";
        public string strSelectedChildName = "";

        GridHitInfo downHitInfo = null;

        #endregion

        public frm_HR_Select_Employee_Absence()
        {
            InitializeComponent();
        }

        private void frm_HR_Select_Employee_Absence_Load(object sender, EventArgs e)
        {
            this.FormID = 500105;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp_HR_00187_Select_Absence_Holiday_YearsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00188_Select_Absence_Holiday_Year_AbsencesTableAdapter.Connection.ConnectionString = strConnectionString;

            try
            {
                sp_HR_00187_Select_Absence_Holiday_YearsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00187_Select_Absence_Holiday_Years, 0);
            }
            catch (Exception)
            {
                XtraMessageBox.Show("An error occurred while loading the holiday years list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInParentID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["EmployeeHolidayYearID"], intPassedInParentID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            view = (GridView)gridControl2.MainView;
            if (intPassedInChildID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["AbsenceID"], intPassedInChildID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
        }

        bool internalRowFocusing;


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Employee Holiday Years Available";
                    break;
                case "gridView2":
                    message = "No Employee Absences - Select an Employee Holiday Year to see Related Absences";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region GridView1

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "TotalHolidaysRemaining":
                    if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, e.Column.FieldName)) <= (decimal)0.00)
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DefaultBankHolidayEntitlement":
                case "DefaultBasicLeaveEntitlement":
                case "DefaultYearlyLeaveIncrement":
                case "HolidaysTaken":
                case "BankHolidaysTaken":
                case "AbsenceAuthorisedTaken":
                case "AbsenceUnauthorisedTaken":
                case "BasicHolidayEntitlement":
                case "BankHolidayEntitlement":
                case "PurchasedHolidayEntitlement":
                case "HoursPerHolidayDay":
                case "TotalHolidayEntitlement":
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "TotalHolidaysRemaining":
                    if (view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptor").ToString() == "Days")
                    {
                        e.RepositoryItem = repositoryItemTextEditDaysHoliday;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptor").ToString() == "Hours")
                    {
                        e.RepositoryItem = repositoryItemTextEditHoursHoliday;
                    }
                    else
                    {
                        e.RepositoryItem = repositoryItemTextEditUnknownHoliday;
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView2

        private void gridView2_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "Status")
            {
                switch (view.GetRowCellValue(e.RowHandle, "Status").ToString())
                {
                    case "Pending":
                        e.Appearance.BackColor = Color.Khaki;
                        e.Appearance.BackColor2 = Color.DarkOrange;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case "Approved":
                        e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                        e.Appearance.BackColor2 = Color.PaleGreen;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case "Rejected":
                        e.Appearance.BackColor = Color.LightSteelBlue;
                        e.Appearance.BackColor2 = Color.CornflowerBlue;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case "Cancelled":
                        e.Appearance.BackColor = Color.Gainsboro;
                        e.Appearance.BackColor2 = Color.DarkGray;
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    case "Unauthorised":
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        break;
                    default:
                        break;
                }
            }
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            var intSelectedID = 0;

            if (intCount == 1)
            {
                intSelectedID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], view.Columns["EmployeeHolidayYearID"]));
            }
            string strSelected = intSelectedID.ToString() + ",";

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_HR_Core.sp_HR_00188_Select_Absence_Holiday_Year_Absences.Clear();
            }
            else
            {
                try
                {
                    sp_HR_00188_Select_Absence_Holiday_Year_AbsencesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00188_Select_Absence_Holiday_Year_Absences, strSelected, "");
                }
                catch (Exception Ex)
                {
                    XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related absences.\n\nTry selecting a holiday year again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedChildID == 0)
            {
                XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            intSelectedParentID = 0;
            strSelectedParentName = "";

            intSelectedChildID = 0;
            strSelectedChildName = "";

            var view1 = (GridView)gridControl1.MainView;
            if (view1.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedParentID = Convert.ToInt32(view1.GetRowCellValue(view1.FocusedRowHandle, "EmployeeHolidayYearID"));
                strSelectedParentName = (String.IsNullOrEmpty(Convert.ToString(view1.GetRowCellValue(view1.FocusedRowHandle, "HolidayYearDescription"))) ? "Unknown Holiday Year" : Convert.ToString(view1.GetRowCellValue(view1.FocusedRowHandle, "HolidayYearDescription")));
            }

            var view2 = (GridView)gridControl2.MainView;
            if (view2.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedChildID = Convert.ToInt32(view2.GetRowCellValue(view2.FocusedRowHandle, "AbsenceID"));
                strSelectedChildName = (String.IsNullOrEmpty(Convert.ToString(view2.GetRowCellValue(view2.FocusedRowHandle, "EmployeeSurnameForename"))) ? "Unknown Employee" : Convert.ToString(view2.GetRowCellValue(view2.FocusedRowHandle, "EmployeeSurnameForename"))) + " - " + view2.GetRowCellValue(view2.FocusedRowHandle, "StartDate").ToString();
            }



        }



    }
}


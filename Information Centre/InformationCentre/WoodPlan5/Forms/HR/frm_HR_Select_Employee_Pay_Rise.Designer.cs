namespace WoodPlan5
{
    partial class frm_HR_Select_Employee_Pay_Rise
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spHR00217EmployeePayRisesSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPayRiseID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeID12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPreviousPayAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditNumericCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPayRiseAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayRisePercentage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colNewPayAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayRiseDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAppliedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colEmployeeName8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeSurname9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeFirstname7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmployeeNumber12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAppliedByPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00217_Employee_PayRises_SelectTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00217_Employee_PayRises_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00217EmployeePayRisesSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(845, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 425);
            this.barDockControlBottom.Size = new System.Drawing.Size(845, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 399);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(845, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 399);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spHR00217EmployeePayRisesSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditNumericCurrency,
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditPercentage,
            this.repositoryItemTextEditDateTime});
            this.gridControl1.Size = new System.Drawing.Size(844, 364);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spHR00217EmployeePayRisesSelectBindingSource
            // 
            this.spHR00217EmployeePayRisesSelectBindingSource.DataMember = "sp_HR_00217_Employee_PayRises_Select";
            this.spHR00217EmployeePayRisesSelectBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPayRiseID,
            this.colEmployeeID12,
            this.colPreviousPayAmount,
            this.colPayRiseAmount,
            this.colPayRisePercentage,
            this.colNewPayAmount,
            this.colPayRiseDate,
            this.colAppliedByPersonID,
            this.colRemarks10,
            this.colEmployeeName8,
            this.colEmployeeSurname9,
            this.colEmployeeFirstname7,
            this.colEmployeeNumber12,
            this.colAppliedByPersonName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.BestFitMaxRowCount = 5;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colEmployeeName8, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPayRiseDate, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView1_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView1_FocusedRowChanged);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView1_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView1_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // colPayRiseID
            // 
            this.colPayRiseID.Caption = "Pay Rise ID";
            this.colPayRiseID.FieldName = "PayRiseID";
            this.colPayRiseID.Name = "colPayRiseID";
            this.colPayRiseID.OptionsColumn.AllowEdit = false;
            this.colPayRiseID.OptionsColumn.AllowFocus = false;
            this.colPayRiseID.OptionsColumn.ReadOnly = true;
            this.colPayRiseID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPayRiseID.Width = 76;
            // 
            // colEmployeeID12
            // 
            this.colEmployeeID12.Caption = "Employee ID";
            this.colEmployeeID12.FieldName = "EmployeeID";
            this.colEmployeeID12.Name = "colEmployeeID12";
            this.colEmployeeID12.OptionsColumn.AllowEdit = false;
            this.colEmployeeID12.OptionsColumn.AllowFocus = false;
            this.colEmployeeID12.OptionsColumn.ReadOnly = true;
            this.colEmployeeID12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeID12.Width = 81;
            // 
            // colPreviousPayAmount
            // 
            this.colPreviousPayAmount.Caption = "Previous Pay Amount";
            this.colPreviousPayAmount.ColumnEdit = this.repositoryItemTextEditNumericCurrency;
            this.colPreviousPayAmount.FieldName = "PreviousPayAmount";
            this.colPreviousPayAmount.Name = "colPreviousPayAmount";
            this.colPreviousPayAmount.OptionsColumn.AllowEdit = false;
            this.colPreviousPayAmount.OptionsColumn.AllowFocus = false;
            this.colPreviousPayAmount.OptionsColumn.ReadOnly = true;
            this.colPreviousPayAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPreviousPayAmount.Visible = true;
            this.colPreviousPayAmount.VisibleIndex = 1;
            this.colPreviousPayAmount.Width = 123;
            // 
            // repositoryItemTextEditNumericCurrency
            // 
            this.repositoryItemTextEditNumericCurrency.AutoHeight = false;
            this.repositoryItemTextEditNumericCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditNumericCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditNumericCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditNumericCurrency.Name = "repositoryItemTextEditNumericCurrency";
            // 
            // colPayRiseAmount
            // 
            this.colPayRiseAmount.Caption = "Pay Rise Amount";
            this.colPayRiseAmount.ColumnEdit = this.repositoryItemTextEditNumericCurrency;
            this.colPayRiseAmount.FieldName = "PayRiseAmount";
            this.colPayRiseAmount.Name = "colPayRiseAmount";
            this.colPayRiseAmount.OptionsColumn.AllowEdit = false;
            this.colPayRiseAmount.OptionsColumn.AllowFocus = false;
            this.colPayRiseAmount.OptionsColumn.ReadOnly = true;
            this.colPayRiseAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPayRiseAmount.Visible = true;
            this.colPayRiseAmount.VisibleIndex = 2;
            this.colPayRiseAmount.Width = 102;
            // 
            // colPayRisePercentage
            // 
            this.colPayRisePercentage.Caption = "Pay Rise %";
            this.colPayRisePercentage.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colPayRisePercentage.FieldName = "PayRisePercentage";
            this.colPayRisePercentage.Name = "colPayRisePercentage";
            this.colPayRisePercentage.OptionsColumn.AllowEdit = false;
            this.colPayRisePercentage.OptionsColumn.AllowFocus = false;
            this.colPayRisePercentage.OptionsColumn.ReadOnly = true;
            this.colPayRisePercentage.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPayRisePercentage.Visible = true;
            this.colPayRisePercentage.VisibleIndex = 3;
            this.colPayRisePercentage.Width = 76;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colNewPayAmount
            // 
            this.colNewPayAmount.Caption = "New Pay Amount";
            this.colNewPayAmount.ColumnEdit = this.repositoryItemTextEditNumericCurrency;
            this.colNewPayAmount.FieldName = "NewPayAmount";
            this.colNewPayAmount.Name = "colNewPayAmount";
            this.colNewPayAmount.OptionsColumn.AllowEdit = false;
            this.colNewPayAmount.OptionsColumn.AllowFocus = false;
            this.colNewPayAmount.OptionsColumn.ReadOnly = true;
            this.colNewPayAmount.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colNewPayAmount.Visible = true;
            this.colNewPayAmount.VisibleIndex = 4;
            this.colNewPayAmount.Width = 103;
            // 
            // colPayRiseDate
            // 
            this.colPayRiseDate.Caption = "Pay Rise Date";
            this.colPayRiseDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colPayRiseDate.FieldName = "PayRiseDate";
            this.colPayRiseDate.Name = "colPayRiseDate";
            this.colPayRiseDate.OptionsColumn.AllowEdit = false;
            this.colPayRiseDate.OptionsColumn.AllowFocus = false;
            this.colPayRiseDate.OptionsColumn.ReadOnly = true;
            this.colPayRiseDate.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colPayRiseDate.Visible = true;
            this.colPayRiseDate.VisibleIndex = 0;
            this.colPayRiseDate.Width = 101;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "D";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colAppliedByPersonID
            // 
            this.colAppliedByPersonID.Caption = "Applied By Persion ID";
            this.colAppliedByPersonID.FieldName = "AppliedByPersonID";
            this.colAppliedByPersonID.Name = "colAppliedByPersonID";
            this.colAppliedByPersonID.OptionsColumn.AllowEdit = false;
            this.colAppliedByPersonID.OptionsColumn.AllowFocus = false;
            this.colAppliedByPersonID.OptionsColumn.ReadOnly = true;
            this.colAppliedByPersonID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppliedByPersonID.Width = 123;
            // 
            // colRemarks10
            // 
            this.colRemarks10.Caption = "Remarks";
            this.colRemarks10.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks10.FieldName = "Remarks";
            this.colRemarks10.Name = "colRemarks10";
            this.colRemarks10.OptionsColumn.ReadOnly = true;
            this.colRemarks10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colRemarks10.Visible = true;
            this.colRemarks10.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colEmployeeName8
            // 
            this.colEmployeeName8.Caption = "Linked To Employee";
            this.colEmployeeName8.FieldName = "EmployeeName";
            this.colEmployeeName8.Name = "colEmployeeName8";
            this.colEmployeeName8.OptionsColumn.AllowEdit = false;
            this.colEmployeeName8.OptionsColumn.AllowFocus = false;
            this.colEmployeeName8.OptionsColumn.ReadOnly = true;
            this.colEmployeeName8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeName8.Visible = true;
            this.colEmployeeName8.VisibleIndex = 6;
            this.colEmployeeName8.Width = 192;
            // 
            // colEmployeeSurname9
            // 
            this.colEmployeeSurname9.Caption = "Surname";
            this.colEmployeeSurname9.FieldName = "EmployeeSurname";
            this.colEmployeeSurname9.Name = "colEmployeeSurname9";
            this.colEmployeeSurname9.OptionsColumn.AllowEdit = false;
            this.colEmployeeSurname9.OptionsColumn.AllowFocus = false;
            this.colEmployeeSurname9.OptionsColumn.ReadOnly = true;
            this.colEmployeeSurname9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeSurname9.Width = 119;
            // 
            // colEmployeeFirstname7
            // 
            this.colEmployeeFirstname7.Caption = "Forename";
            this.colEmployeeFirstname7.FieldName = "EmployeeFirstname";
            this.colEmployeeFirstname7.Name = "colEmployeeFirstname7";
            this.colEmployeeFirstname7.OptionsColumn.AllowEdit = false;
            this.colEmployeeFirstname7.OptionsColumn.AllowFocus = false;
            this.colEmployeeFirstname7.OptionsColumn.ReadOnly = true;
            this.colEmployeeFirstname7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeFirstname7.Width = 110;
            // 
            // colEmployeeNumber12
            // 
            this.colEmployeeNumber12.Caption = "Employee #";
            this.colEmployeeNumber12.FieldName = "EmployeeNumber";
            this.colEmployeeNumber12.Name = "colEmployeeNumber12";
            this.colEmployeeNumber12.OptionsColumn.AllowEdit = false;
            this.colEmployeeNumber12.OptionsColumn.AllowFocus = false;
            this.colEmployeeNumber12.OptionsColumn.ReadOnly = true;
            this.colEmployeeNumber12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colEmployeeNumber12.Width = 93;
            // 
            // colAppliedByPersonName
            // 
            this.colAppliedByPersonName.Caption = "Applied By Person";
            this.colAppliedByPersonName.FieldName = "AppliedByPersonName";
            this.colAppliedByPersonName.Name = "colAppliedByPersonName";
            this.colAppliedByPersonName.OptionsColumn.AllowEdit = false;
            this.colAppliedByPersonName.OptionsColumn.AllowFocus = false;
            this.colAppliedByPersonName.OptionsColumn.ReadOnly = true;
            this.colAppliedByPersonName.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colAppliedByPersonName.Visible = true;
            this.colAppliedByPersonName.VisibleIndex = 6;
            this.colAppliedByPersonName.Width = 173;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(677, 397);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "OK";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(758, 397);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(1, 27);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(844, 364);
            this.gridSplitContainer1.TabIndex = 7;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_HR_00217_Employee_PayRises_SelectTableAdapter
            // 
            this.sp_HR_00217_Employee_PayRises_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Select_Employee_Pay_Rise
            // 
            this.ClientSize = new System.Drawing.Size(845, 425);
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.LookAndFeel.SkinName = "Blue";
            this.MinimizeBox = false;
            this.Name = "frm_HR_Select_Employee_Pay_Rise";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Employee Pay Rise";
            this.Load += new System.EventHandler(this.frm_HR_Select_Employee_Pay_Rise_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.gridSplitContainer1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00217EmployeePayRisesSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditNumericCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditNumericCurrency;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private System.Windows.Forms.BindingSource spHR00217EmployeePayRisesSelectBindingSource;
        private DataSet_HR_Core dataSet_HR_Core;
        private DataSet_HR_CoreTableAdapters.sp_HR_00217_Employee_PayRises_SelectTableAdapter sp_HR_00217_Employee_PayRises_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colPayRiseID;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeID12;
        private DevExpress.XtraGrid.Columns.GridColumn colPreviousPayAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colPayRiseAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colPayRisePercentage;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colNewPayAmount;
        private DevExpress.XtraGrid.Columns.GridColumn colPayRiseDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colAppliedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks10;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeName8;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeSurname9;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeFirstname7;
        private DevExpress.XtraGrid.Columns.GridColumn colEmployeeNumber12;
        private DevExpress.XtraGrid.Columns.GridColumn colAppliedByPersonName;
    }
}

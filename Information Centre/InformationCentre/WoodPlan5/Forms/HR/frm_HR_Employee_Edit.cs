using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using WoodPlan5.Properties;
using WoodPlan5.Classes.HR;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_HR_Employee_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
       
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        string _strLastUsedReferencePrefix = "";
        string _strLastUsedReferencePrefix2 = "";
        public int ShowSalaryData { get; set; }

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        private Utils.enmMainGrids _enmFocusedGrid = Utils.enmMainGrids.NextOfKin;
        public RefreshGridState RefreshGridViewStateAddress;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateNextOfKin;
        public RefreshGridState RefreshGridViewStateTraining;
        public RefreshGridState RefreshGridViewStateDiscipline;
        public RefreshGridState RefreshGridViewStateVetting;
        public RefreshGridState RefreshGridViewStateDeptWorked;
        public RefreshGridState RefreshGridViewStateWorkingPattern;
        public RefreshGridState RefreshGridViewStateWorkingPatternHeader;
        public RefreshGridState RefreshGridViewStateHolidayYear;
        public RefreshGridState RefreshGridViewStateAbsence;
        public RefreshGridState RefreshGridViewStateBonus;
        public RefreshGridState RefreshGridViewStatePension;
        public RefreshGridState RefreshGridViewStateCRM;
        public RefreshGridState RefreshGridViewStateReference;
        public RefreshGridState RefreshGridViewStatePayRise;
        public RefreshGridState RefreshGridViewStateShares;
      
        string i_str_AddedAddressRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedNextOfKinRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedTrainingRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedDisciplineRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedVettingRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedDeptWorkedRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedWorkingPatternRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedWorkingPatternHeaderRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedHolidayYearRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedAbsenceRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedBonusRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedPensionRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedCRMRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedReferenceRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedPayRiseRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedSharesRecordIDs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        BaseObjects.GridCheckMarksSelection selection5;
        private string i_str_select_absence_type_ids = "";
        private string i_str_select_absence_types = "";

        private bool i_boolVettingData = false;
        private bool i_boolReferenceData = false;
        private bool i_boolAddressData = false;
        private bool i_boolNextOfKinData = false;
        private bool i_boolDeptsWorkedData = false;
        private bool i_boolWorkPatternData = false;
        private bool i_boolHolidayData = false;
        private bool i_boolShareData = false;
        private bool i_boolBonusData = false;
        private bool i_boolPayRiseData = false;
        private bool i_boolPensionData = false;
        private bool i_boolTrainingData = false;
        private bool i_boolDisciplineData = false;
        private bool i_boolCRMData = false;

        private DataSet_HR_CoreTableAdapters.sp_HR_00270_Employee_Work_Email_CheckTableAdapter sp_HR_00270_Employee_Work_Email_CheckTableAdapter 
            = new DataSet_HR_CoreTableAdapters.sp_HR_00270_Employee_Work_Email_CheckTableAdapter();

        #endregion

        public frm_HR_Employee_Edit()
        {
            InitializeComponent();
        }

        private void frm_HR_Employee_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 990101;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            // Get Form Permissions   ***** NEED THIS TO CONTROL WHICH BITS OF LINKED DATA ARE SHOWN ***** //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 9901, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            sp_HR_00077_Absence_Types_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00077_Absence_Types_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00077_Absence_Types_List_With_Blank);
            gridControl5.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;

            sp_HR_00060_Gender_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00060_Gender_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00060_Gender_List_With_Blank);

            sp_HR_00061_Ethnicity_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00061_Ethnicity_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00061_Ethnicity_List_With_Blank);

            sp_HR_00108_Marital_Statuses_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00108_Marital_Statuses_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00108_Marital_Statuses_List_With_Blank);

            sp_HR_00109_Religions_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00109_Religions_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00109_Religions_List_With_Blank);

            sp_HR_00110_Disabilities_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00110_Disabilities_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00110_Disabilities_List_With_Blank);

            sp_HR_00062_Contract_Type_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00062_Contract_Type_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00062_Contract_Type_List_With_Blank);

            sp_HR_00124_Job_Titles_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00124_Job_Titles_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00124_Job_Titles_With_Blank);

            sp_HR_00125_Generic_Job_Titles_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00125_Generic_Job_Titles_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00125_Generic_Job_Titles_With_Blank);

            sp_HR_00126_Job_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00126_Job_Types_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00126_Job_Types_With_Blank);

            sp_HR_00127_Contract_Basis_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00127_Contract_Basis_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00127_Contract_Basis_List_With_Blank);

            sp_HR_00064_Appointment_Reason_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00064_Appointment_Reason_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00064_Appointment_Reason_List_With_Blank);

            sp_HR_00111_Recruitment_Sources_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00111_Recruitment_Sources_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00111_Recruitment_Sources_With_Blank);

            sp_HR_00038_Employee_Handbook_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00038_Employee_Handbook_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00038_Employee_Handbook_List_With_Blank);

            sp_HR_00065_Transfer_Companies_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00065_Transfer_Companies_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00065_Transfer_Companies_List_With_Blank);

            sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00082_Holiday_Unit_Descriptors_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00082_Holiday_Unit_Descriptors_With_Blank);

            sp_HR_00070_Leaving_Reason_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00070_Leaving_Reason_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00070_Leaving_Reason_List_With_Blank);

            sp_HR_00066_Salary_Banding_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00066_Salary_Banding_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00066_Salary_Banding_List_With_Blank);

            sp_HR_00069_Payroll_Type_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00069_Payroll_Type_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00069_Payroll_Type_List_With_Blank);

            sp_HR_00067_Payment_Type_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00067_Payment_Type_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00067_Payment_Type_List_With_Blank);

            sp_HR_00128_Payroll_Reference_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00128_Payroll_Reference_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00128_Payroll_Reference_List_With_Blank);

            sp_HR_00068_Payment_Frequency_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00068_Payment_Frequency_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00068_Payment_Frequency_List_With_Blank);

            sp_HR_00216_Pay_Days_List_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00216_Pay_Days_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00216_Pay_Days_List_With_Blank);

            sp_HR_00220_Medical_Cover_Providers_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00220_Medical_Cover_Providers_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00220_Medical_Cover_Providers_With_Blank);

            sp_HR_00221_Medical_Cover_Payment_Frequency_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00221_Medical_Cover_Payment_Frequency_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00221_Medical_Cover_Payment_Frequency_With_Blank);

            sp_HR_00223_Medical_Cover_Contribution_Unit_Descriptor_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00223_Medical_Cover_Contribution_Unit_Descriptor_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00223_Medical_Cover_Contribution_Unit_Descriptor_With_Blank);

            sp_HR_00222_CTW_Scheme_End_Options_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00222_CTW_Scheme_End_Options_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00222_CTW_Scheme_End_Options_With_Blank);


            sp_HR_00271_DIS_Cover_Levels_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00271_DIS_Cover_Levels_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00271_DIS_Cover_Levels_With_Blank);

            sp_HR_00272_DIS_Schemes_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00272_DIS_Schemes_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00272_DIS_Schemes_With_Blank);

            sp_HR_00273_IP_Levels_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00273_IP_Levels_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00273_IP_Levels_With_Blank);

            sp_HR_00274_IP_Deferral_Periods_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00274_IP_Deferral_Periods_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00274_IP_Deferral_Periods_With_Blank);

            sp_HR_00275_IP_Schemes_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00275_IP_Schemes_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00275_IP_Schemes_With_Blank);


            sp_HR_00243_Notice_Period_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00243_Notice_Period_Descriptors_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00243_Notice_Period_Descriptors_With_Blank);

            sp_HR_00009_GetEmployeeAddressesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateAddress = new RefreshGridState(gridViewAddresses, "AddressId");

            sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateNextOfKin = new RefreshGridState(gridViewNextOfKin, "Id");

            sp09113_HR_Qualifications_For_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateTraining = new RefreshGridState(gridViewTraining, "QualificationID");

            sp_HR_00053_Get_Employee_SanctionsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateDiscipline = new RefreshGridState(gridViewDiscipline, "SanctionID");

            sp_HR_00131_Get_Employee_VettingTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateVetting = new RefreshGridState(gridViewVetting, "VettingID");

            sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateDeptWorked = new RefreshGridState(gridViewDeptWorked, "EmployeeDeptWorkedID");

            sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateWorkingPatternHeader = new RefreshGridState(gridViewWorkingPatternHeader, "WorkingPatternHeaderID");

            sp_HR_00046_Get_Employee_Working_PatternsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateWorkingPattern = new RefreshGridState(gridViewWorkingPattern, "WorkingPatternID");

            sp_HR_00037_Get_Employee_AbsencesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateAbsence = new RefreshGridState(gridViewAbsences, "AbsenceID");

            sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateHolidayYear = new RefreshGridState(gridViewHolidayYear, "EmployeeHolidayYearID");

            sp_HR_00172_Employee_BonusesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateBonus = new RefreshGridState(gridViewBonuses, "BonusID");

            sp_HR_00016_Pensions_For_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStatePension = new RefreshGridState(gridViewPension, "PensionID");

            sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateCRM = new RefreshGridState(gridViewCRM, "CRMID");

            sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateReference = new RefreshGridState(gridViewReference, "ReferenceID");

            sp_HR_00213_Employee_PayRisesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStatePayRise = new RefreshGridState(gridViewPayRise, "PayRiseID");

            sp_HR_00224_Employee_SharesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateShares = new RefreshGridState(gridViewShares, "SharesID");

            sp_HR_00270_Employee_Work_Email_CheckTableAdapter.Connection.ConnectionString = strConnectionString;

            LoadLastSavedUserScreenSettings();  // Get any used sequence number prefix //

            ShowSalaryData = (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey) ? 0 : 1);

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(9, "HR_QualificationCertificatePath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Qualification Certificate Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Qualification Certificate Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Populate Main Dataset //
            sp09002_HR_Employee_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["Surname"] = "";
                        drNewRow["Firstname"] = "";
                        drNewRow["Gender"] = 0;
                        drNewRow["EthnicityId"] = 0;
                        drNewRow["Gender"] = 0;
                        drNewRow["YearsToAutoReEnrolment"] = 1;
                        drNewRow["MaritalStatusID"] = 0;
                        drNewRow["ReligionID"] = 0;
                        drNewRow["DisabilityID"] = 0;
                        drNewRow["PicturePath"] = "";
                        drNewRow["CalculatedAge"] = 0;
                        drNewRow["EmployeeNumber"] = _strLastUsedReferencePrefix;
                        drNewRow["ContractNumber"] = _strLastUsedReferencePrefix2;
                        drNewRow["CurrentEmployee"] = 1;
                        drNewRow["JobTitleID"] = 0;
                        drNewRow["GenericJobTitleID"] = 0;
                        drNewRow["JobTypeID"] = 0;
                        drNewRow["ContractBasisID"] = 0;
                        drNewRow["RecruitmentCost"] = (decimal)0.00;
                        drNewRow["ActualHours"] = (decimal)0.00;
                        drNewRow["FTEHours"] = (decimal)0.00;
                        drNewRow["FTERatio"] = (decimal)0.00;
                        drNewRow["IssuedHandbookID"] = 0;
                        drNewRow["TUPE"] = 0;
                        drNewRow["TransferredFromLocalGovernment"] = 0;
                        drNewRow["TransferredFromID"] = 0;
                        drNewRow["NJCConditionsApply"] = 0;
                        drNewRow["OriginatingCompanyID"] = 0;
                        drNewRow["InductionChecklistReturned"] = 0;
                        drNewRow["HolidayUnitDescriptorID"] = 1;  // Days //
                        drNewRow["BasicLeaveEntitlementFTE"] = (decimal)0.00;
                        drNewRow["BasicLeaveEntitlement"] = (decimal)0.00;
                        drNewRow["BankHolidayEntitlementFTE"] = (decimal)0.00;
                        drNewRow["BankHolidayEntitlement"] = (decimal)0.00;
                        drNewRow["YearlyLeaveIncrementAmount"] = (decimal)0.00;
                        drNewRow["MaximumLeaveEntitlement"] = (decimal)0.00;

                        SimplerAES Encryptor = new SimplerAES();
                        string strAmount = Encryptor.Encrypt("0.00");
                        drNewRow["LastIncreaseAmount"] = strAmount;
                        drNewRow["LastIncreasePercentage"] = strAmount;
                        drNewRow["Salary"] = strAmount;
                        drNewRow["PaymentAmount"] = strAmount;
                        drNewRow["MedicalCoverEmployerContribution"] = strAmount;
                        drNewRow["CTWSchemeValue"] = strAmount;
                        drNewRow["PreviousEmploymentSalary"] = strAmount;
                        drNewRow["SalaryExpectation"] = strAmount;
                        LastIncreaseAmountSpinEdit.EditValue = "0.00";
                        LastIncreasePercentageSpinEdit.EditValue = "0.00";
                        SalarySpinEdit.EditValue = "0.00";
                        PaymentAmountSpinEdit.EditValue = "0.00";
                        MedicalCoverEmployerContributionSpinEdit.EditValue = "0.00";
                        CTWSchemeValueSpinEdit.EditValue = "0.00";
                        PreviousEmploymentSalarySpinEdit.EditValue = "0.00";
                        SalaryExpectationSpinEdit.EditValue = "0.00";

                        drNewRow["LeavingReasonID"] = 0;
                        drNewRow["ConsiderReEmployement"] = 0;
                        drNewRow["ExitInterviewerID"] = 0;
                        drNewRow["TrainingCostsToRecover"] = (decimal)0.00;
                        drNewRow["SchemeCostsToRecover"] = (decimal)0.00;
                        drNewRow["EquipmentCostsToRecover"] = (decimal)0.00;
                        drNewRow["SalaryBandingID"] = 0;
                        drNewRow["PayrollTypeID"] = 0;
                        drNewRow["PaymentTypeID"] = 0;
                        drNewRow["PayrollReferenceID"] = 0;
                        drNewRow["PaymentFrequencyID"] = 0;
                        drNewRow["MonthlyPayDayNumber"] = 0;
                        drNewRow["SickPayEntitlementUnits"] = (decimal)0.00;
                        drNewRow["SickPayUnitDescriptorID"] = 1;  // Days //
                        drNewRow["ElegibleForBonus"] = 0;
                        drNewRow["ElegibleForShares"] = 0;
                        drNewRow["ElegibleForOvertime"] = 0;
                        drNewRow["HolidaysAccrued"] = (decimal)0.00;
                        drNewRow["HolidaysTaken"] = (decimal)0.00;
                        drNewRow["HolidaysBalance"] = (decimal)0.00;
                        drNewRow["AnyOtherOwing"] = (decimal)0.00;
                        drNewRow["AgreedReferenceInPlace"] = 0;

                        drNewRow["MedicalCoverProviderID"] = 0;
                        drNewRow["MedicalCoverEmployerContributionDescriptorID"] = 0;
                        drNewRow["MedicalCoverEmployerContributionFrequencyID"] = 0;
                        drNewRow["CTWScheme"] = 0;
                        drNewRow["CTWSchemeEndOptionID"] = 0;
                        drNewRow["ChildCareVouchers"] = 0;
                        drNewRow["ExitInterviewRequired"] = 0;

                        drNewRow["NoticePeriodUnits"] = (decimal)0.00;
                        drNewRow["NoticePeriodUnitDescriptorID"] = 0;
                        drNewRow["InviteSummerBBQ"] = 0;
                        drNewRow["InviteWinterParty"] = 0;
                        drNewRow["InviteManagementConference"] = 0;
                        drNewRow["SickPayPercentage1"] = (decimal)0.00;
                        drNewRow["SickPayUnits2"] = (decimal)0.00;
                        drNewRow["SickPayUnitDescriptorID2"] = 1;  // Days //
                        drNewRow["SickPayPercentage2"] = (decimal)0.00;

                        drNewRow["ShiftFTE"] = (decimal)1.00;
                        drNewRow["ExtraDaysHolidayYear1"] = 0;

                        drNewRow["SuppressBirthday"] = 0;
                        drNewRow["StaffID"] = 0;
                        drNewRow["StaffName"] = "";
                        drNewRow["CurrentTermsAndConditionsID"] = 0;
                        drNewRow["OptOutFormIssued"] = 0;
                        drNewRow["OptInToSubsistence"] = 0;

                        drNewRow["DISCoverEligible"] = 0;
                        drNewRow["DISLevelCoverID"] = 0;
                        drNewRow["DISSchemeID"] = 0;
                        drNewRow["IPEligible"] = 0;
                        drNewRow["IPLevelID"] = 0;
                        drNewRow["IPDeferralPeriodID"] = 0;
                        drNewRow["IPSchemeID"] = 0;

                        this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows.Add(drNewRow);
                        if (strFormMode == "add" && !string.IsNullOrEmpty(_strLastUsedReferencePrefix))  // Add Suffix number to Employee Reference Number //
                        {
                            Get_Suffix_Number();  // Get next value in sequence //
                        }
                        if (strFormMode == "add" && !string.IsNullOrEmpty(_strLastUsedReferencePrefix2))  // Add Suffix number to Contract Reference Number //
                        {
                            Get_Suffix_Number2();  // Get next value in sequence //
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["Surname"] = "";
                        drNewRow["Firstname"] = "";
                        this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows.Add(drNewRow);
                        this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        //var table = DataSet_HR_DataEntry.sp09002_HR_Employee_ItemDataTable;
                        //var row = table.Newsp09002_HR_Employee_ItemRow();
                        //row.

                        sp09002_HR_Employee_ItemTableAdapter.Fill(this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //

            emptyEditor = new RepositoryItem();
        }

        private void frm_HR_Employee_Edit_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedAddressRecordIDs))
                {
                    LoadLinked_Addresses();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedNextOfKinRecordIDs))
                {
                    LoadLinked_NextOfKin();
                }
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedTrainingRecordIDs))
                {
                    LoadLinked_Training();
                }
                if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedDisciplineRecordIDs))
                {
                    LoadLinked_Discipline();
                }
                if (UpdateRefreshStatus == 5 || !string.IsNullOrEmpty(i_str_AddedVettingRecordIDs))
                {
                    LoadLinked_Vetting();
                }
                if (UpdateRefreshStatus == 6 || !string.IsNullOrEmpty(i_str_AddedDeptWorkedRecordIDs))
                {
                    LoadLinked_DeptWorked();
                }
                if (UpdateRefreshStatus == 7 || !string.IsNullOrEmpty(i_str_AddedWorkingPatternRecordIDs))
                {
                    LoadLinked_WorkingPattern();
                }
                if (UpdateRefreshStatus == 8 || !string.IsNullOrEmpty(i_str_AddedWorkingPatternHeaderRecordIDs))
                {
                    LoadLinked_WorkingPatternHeader();
                }
                if (UpdateRefreshStatus == 9 || !string.IsNullOrEmpty(i_str_AddedHolidayYearRecordIDs))
                {
                    LoadLinked_HolidayYears();
                }
                if (UpdateRefreshStatus == 10 || !string.IsNullOrEmpty(i_str_AddedAbsenceRecordIDs))
                {
                    LoadLinked_Absences();
                }
                if (UpdateRefreshStatus == 11 || !string.IsNullOrEmpty(i_str_AddedBonusRecordIDs))
                {
                    LoadLinked_Bonuses();
                }
                if (UpdateRefreshStatus == 12 || !string.IsNullOrEmpty(i_str_AddedPensionRecordIDs))
                {
                    LoadLinked_Pensions();
                }
                if (UpdateRefreshStatus == 13 || !string.IsNullOrEmpty(i_str_AddedCRMRecordIDs))
                {
                    LoadLinked_CRM();
                }
                if (UpdateRefreshStatus == 14 || !string.IsNullOrEmpty(i_str_AddedReferenceRecordIDs))
                {
                    LoadLinked_References();
                }
                if (UpdateRefreshStatus == 15 || !string.IsNullOrEmpty(i_str_AddedPayRiseRecordIDs))
                {
                    LoadLinked_PayRises();
                }
                if (UpdateRefreshStatus == 16 || !string.IsNullOrEmpty(i_str_AddedSharesRecordIDs))
                {
                    LoadLinked_Shares();
                }
            }
            SetMenuStatus();
        }

        private void frm_HR_Employee_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;

            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                if (!string.IsNullOrEmpty(_strLastUsedReferencePrefix) || !string.IsNullOrEmpty(_strLastUsedReferencePrefix2))
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "EmployeeNumberPrefix", _strLastUsedReferencePrefix);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ContractNumberPrefix", _strLastUsedReferencePrefix2);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "AbsenceTypeFilter", i_str_select_absence_type_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ActiveHolidayYear", popupContainerEditAbsenceType.EditValue.ToString());
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }
        
        private void frm_HR_Employee_Edit_SizeChanged(object sender, EventArgs e)
        {
            dataNavigator1.Location = new Point(this.Width - dataNavigator1.Width - 3, dataNavigator1.Location.Y);
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Reference Number Prefix //
                _strLastUsedReferencePrefix = default_screen_settings.RetrieveSetting("EmployeeNumberPrefix");
                _strLastUsedReferencePrefix2 = default_screen_settings.RetrieveSetting("ContractNumberPrefix");

                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("AbsenceTypeFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayFilter = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayFilter)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditAbsenceType.EditValue = PopupContainerEdit1_Get_Selected();
                }

                strFilter = default_screen_settings.RetrieveSetting("ActiveHolidayYear");
                if (!(string.IsNullOrEmpty(strFilter)))
                {
                    checkEditShowActiveHolidayYearOnly.EditValue = (strFilter == "0" ? 0 : 1);
                }

            }
        }


        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            
            if (this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Employee", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
            this.ValidateChildren();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        EmployeeNumberButtonEdit.Focus();

                        FirstnameTextEdit.Properties.ReadOnly = false;
                        SurnameTextEdit.Properties.ReadOnly = false;
                        workEmailAddressTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        EmployeeNumberButtonEdit.Focus();

                        FirstnameTextEdit.Properties.ReadOnly = true;
                        SurnameTextEdit.Properties.ReadOnly = true;
                        workEmailAddressTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        EmployeeNumberButtonEdit.Focus();

                        FirstnameTextEdit.Properties.ReadOnly = false;
                        SurnameTextEdit.Properties.ReadOnly = false;
                        workEmailAddressTextEdit.Properties.ReadOnly = false;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        TitleTextEdit.Focus();

                        FirstnameTextEdit.Properties.ReadOnly = true;
                        SurnameTextEdit.Properties.ReadOnly = true;
                        workEmailAddressTextEdit.Properties.ReadOnly = true;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }

            SalarySpinEdit.Properties.ReadOnly = (ShowSalaryData == 0);
            PaymentAmountSpinEdit.Properties.ReadOnly = (ShowSalaryData == 0);
            LastIncreaseAmountSpinEdit.Properties.ReadOnly = (ShowSalaryData == 0);
            LastIncreasePercentageSpinEdit.Properties.ReadOnly = (ShowSalaryData == 0);
            MedicalCoverEmployerContributionSpinEdit.Properties.ReadOnly = (ShowSalaryData == 0);
            CTWSchemeValueSpinEdit.Properties.ReadOnly = (ShowSalaryData == 0);
            PreviousEmploymentSalarySpinEdit.Properties.ReadOnly = (ShowSalaryData == 0);
            SalaryExpectationSpinEdit.Properties.ReadOnly = (ShowSalaryData == 0);

            SalaryBandingIDGridLookUpEdit.Properties.ReadOnly = (ShowSalaryData == 0);
            SalaryBandingIDGridLookUpEdit.Properties.Buttons[1].Enabled = (ShowSalaryData == 1);
            SalaryBandingIDGridLookUpEdit.Properties.Buttons[2].Enabled = (ShowSalaryData == 1);
            
            SetChangesPendingLabel();
            SetEditorButtons();
            Set_Linked_Page_Visibility();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            //Application.DoEvents();
            dataLayoutControl1.ActiveControl = workEmailAddressTextEdit;
            dataLayoutControl1.ActiveControl = HolidayUnitDescriptorIDGridLookUpEdit;
            dataLayoutControl1.ActiveControl = ActualHoursSpinEdit;
            dataLayoutControl1.ActiveControl = TitleTextEdit;

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            Set_Editor_Masks();
            Set_Editor_Mask_SickPay1();
            Set_Editor_Mask_SickPay2();
            Set_Editor_Masks3();
            Set_Editor_Back_Colours();

            ibool_ignoreValidation = true;
            ibool_FormStillLoading = false;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 2:  // Pension Data //
                        i_boolPensionData = sfpPermissions.blRead;
                        break;
                    case 3:  // Discipline \ Reward Data //
                        i_boolDisciplineData = sfpPermissions.blRead;
                        break;
                    case 4:  // Training Data //
                        i_boolTrainingData = sfpPermissions.blRead;
                        break;
                    case 5:  // CRM Data //
                        i_boolCRMData = sfpPermissions.blRead;
                        break;
                    case 6:  // Holiday Data //
                        i_boolHolidayData = sfpPermissions.blRead;
                        break;
                    case 7:  // Departments Worked Data //
                        i_boolDeptsWorkedData = sfpPermissions.blRead;
                        break;
                    case 8:  // Reference Data //
                        i_boolReferenceData = sfpPermissions.blRead;
                        break;
                    case 9:  // Vetting Data //
                        i_boolVettingData = sfpPermissions.blRead;
                        break;
                    case 10:  // Bonus Data //
                        i_boolBonusData = sfpPermissions.blRead;
                        break;
                    case 11:  // Pay Rise Data //
                        i_boolPayRiseData = sfpPermissions.blRead;
                        break;
                    case 12:  // Share Data //
                        i_boolShareData = sfpPermissions.blRead;
                        break;
                    case 101:  // Address Data //
                        i_boolAddressData = sfpPermissions.blRead;
                        break;
                    case 102:  // /next Of Kin Data //
                        i_boolNextOfKinData = sfpPermissions.blRead;
                        break;
                    case 103:  // Work Pattern Data //
                        i_boolWorkPatternData = sfpPermissions.blRead;
                        break;
                    default:
                        break;
                }
            }
        }


        private void SetEditorButtons()
        {
            // Get Form Permissions //
            genderGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            genderGridLookUpEdit.Properties.Buttons[1].Visible = false;
            genderGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            genderGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            ethnicityIdComboBoxEdit.Properties.Buttons[1].Enabled = false;
            ethnicityIdComboBoxEdit.Properties.Buttons[1].Visible = false;
            ethnicityIdComboBoxEdit.Properties.Buttons[2].Enabled = false;
            ethnicityIdComboBoxEdit.Properties.Buttons[2].Visible = false;
           
            MaritalStatusIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            MaritalStatusIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            MaritalStatusIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            MaritalStatusIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            ReligionIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            ReligionIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            ReligionIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            ReligionIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            DisabilityIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            DisabilityIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            DisabilityIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            DisabilityIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            ContractTypeGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            ContractTypeGridLookUpEdit.Properties.Buttons[1].Visible = false;
            ContractTypeGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            ContractTypeGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            ContractBasisIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            ContractBasisIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            ContractBasisIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            ContractBasisIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            AppointmentReasonGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            AppointmentReasonGridLookUpEdit.Properties.Buttons[1].Visible = false;
            AppointmentReasonGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            AppointmentReasonGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            TransferredFromIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            TransferredFromIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            TransferredFromIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            TransferredFromIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            OriginatingCompanyIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            OriginatingCompanyIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            OriginatingCompanyIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            OriginatingCompanyIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
           
            IssuedHandbookIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            IssuedHandbookIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            IssuedHandbookIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            IssuedHandbookIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            SalaryBandingIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            SalaryBandingIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            SalaryBandingIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            SalaryBandingIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            PayrollTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            PayrollTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            PayrollTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            PayrollTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            PaymentTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            PaymentTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            PaymentTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            PaymentTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            LeavingReasonIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            LeavingReasonIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            LeavingReasonIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            LeavingReasonIDGridLookUpEdit.Properties.Buttons[2].Visible = false;
            
            RecruitmentSourceTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            RecruitmentSourceTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            RecruitmentSourceTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            RecruitmentSourceTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            PayrollReferenceIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            PayrollReferenceIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            PayrollReferenceIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            PayrollReferenceIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            MedicalCoverProviderIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            MedicalCoverProviderIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            MedicalCoverProviderIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            MedicalCoverProviderIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            CTWSchemeEndOptionIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            CTWSchemeEndOptionIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            CTWSchemeEndOptionIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            CTWSchemeEndOptionIDGridLookUpEdit.Properties.Buttons[2].Visible = false;


            DISLevelCoverIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            DISLevelCoverIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            DISLevelCoverIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            DISLevelCoverIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            DISSchemeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            DISSchemeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            DISSchemeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            DISSchemeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            IPLevelIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            IPLevelIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            IPLevelIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            IPLevelIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            IPDeferralPeriodIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            IPDeferralPeriodIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            IPDeferralPeriodIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            IPDeferralPeriodIDGridLookUpEdit.Properties.Buttons[2].Visible = false;

            IPSchemeIDGridLookUpEdit.Properties.Buttons[1].Enabled = false;
            IPSchemeIDGridLookUpEdit.Properties.Buttons[1].Visible = false;
            IPSchemeIDGridLookUpEdit.Properties.Buttons[2].Enabled = false;
            IPSchemeIDGridLookUpEdit.Properties.Buttons[2].Visible = false;


            sp00235_picklist_edit_permissionsTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00235_picklist_edit_permissionsTableAdapter.Fill(dataSet_AT_DataEntry.sp00235_picklist_edit_permissions, GlobalSettings.UserID, "9164,9165,9185,9186,9171,9162,9169,9181,9168,9174,9182,9183,9188,9189,9907,9194,9200,9201,9202,9227,9228,9229,9230,9231", GlobalSettings.ViewedPeriodID);
            int intPartID = 0;
            Boolean boolUpdate = false;
            for (int i = 0; i < this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows.Count; i++)
            {
                intPartID = Convert.ToInt32(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["PartID"]);
                boolUpdate = Convert.ToBoolean(this.dataSet_AT_DataEntry.sp00235_picklist_edit_permissions.Rows[i]["UpdateAccess"]);
                switch (intPartID)
                {
                    case 9164:  // Gender //    
                        {
                            genderGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            genderGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            genderGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            genderGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9165:  // Ethnicity //    
                        {
                            ethnicityIdComboBoxEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            ethnicityIdComboBoxEdit.Properties.Buttons[1].Visible = boolUpdate;
                            ethnicityIdComboBoxEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            ethnicityIdComboBoxEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9185:  // Marital Status //    
                        {
                            MaritalStatusIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            MaritalStatusIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            MaritalStatusIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            MaritalStatusIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9186:  // Religion //    
                        {
                            ReligionIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            ReligionIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            ReligionIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            ReligionIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9171:  // Diability //    
                        {
                            DisabilityIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            DisabilityIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            DisabilityIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            DisabilityIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9162:  // Contract Type //    
                        {
                            ContractTypeGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            ContractTypeGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            ContractTypeGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            ContractTypeGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9169:  // Contract Basis Types //    
                        {
                            ContractBasisIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            ContractBasisIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            ContractBasisIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            ContractBasisIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9181:  // Appointment Reasons //    
                        {
                            AppointmentReasonGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            AppointmentReasonGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            AppointmentReasonGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            AppointmentReasonGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9168:  // Transferred From Companies //    
                        {
                            TransferredFromIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            TransferredFromIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            TransferredFromIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            TransferredFromIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                            OriginatingCompanyIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            OriginatingCompanyIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            OriginatingCompanyIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            OriginatingCompanyIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case -9998:  // Handbook Issue //    ***** NOT WORKING AS NO DATA ENTRY SCREEN YET *****
                        {
                            IssuedHandbookIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            IssuedHandbookIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            IssuedHandbookIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            IssuedHandbookIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9907:  // Salary Bandings //    ***** NOT WORKING AS NO DATA ENTRY SCREEN YET *****
                        {
                            SalaryBandingIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            SalaryBandingIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            SalaryBandingIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            SalaryBandingIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9174:  // Payroll Types //    
                        {
                            PayrollTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            PayrollTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            PayrollTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            PayrollTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9182:  // Payment Types //    
                        {
                            PaymentTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            PaymentTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            PaymentTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            PaymentTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9183:  // Leaving Reasons //    
                        {
                            LeavingReasonIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            LeavingReasonIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            LeavingReasonIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            LeavingReasonIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9188:  // Recruitment Source Types //    
                        {
                            RecruitmentSourceTypeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            RecruitmentSourceTypeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            RecruitmentSourceTypeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            RecruitmentSourceTypeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9194:  // Payroll References //    
                        {
                            PayrollReferenceIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            PayrollReferenceIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            PayrollReferenceIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            PayrollReferenceIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9200:  // Medical Cover Providers //    
                        {
                            MedicalCoverProviderIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            MedicalCoverProviderIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            MedicalCoverProviderIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            MedicalCoverProviderIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9201:  // Medical Cover Contribution Frequency Descriptors //    
                        {
                            MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9202:  // Cycle To Work Scheme End Options //    
                        {
                            CTWSchemeEndOptionIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            CTWSchemeEndOptionIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            CTWSchemeEndOptionIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            CTWSchemeEndOptionIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;


                    case 9227:  // Death In Service Level Cover //    
                        {
                            DISLevelCoverIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            DISLevelCoverIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            DISLevelCoverIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            DISLevelCoverIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9228:  // Death In Service Scheme //    
                        {
                            DISSchemeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            DISSchemeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            DISSchemeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            DISSchemeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9229:  // Income Protection Level //    
                        {
                            IPLevelIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            IPLevelIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            IPLevelIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            IPLevelIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9230:  // Income Protection Deferral Period //    
                        {
                            IPDeferralPeriodIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            IPDeferralPeriodIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            IPDeferralPeriodIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            IPDeferralPeriodIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;
                    case 9231:  // Income Protection Scheme //    
                        {
                            IPSchemeIDGridLookUpEdit.Properties.Buttons[1].Enabled = boolUpdate;
                            IPSchemeIDGridLookUpEdit.Properties.Buttons[1].Visible = boolUpdate;
                            IPSchemeIDGridLookUpEdit.Properties.Buttons[2].Enabled = boolUpdate;
                            IPSchemeIDGridLookUpEdit.Properties.Buttons[2].Visible = boolUpdate;
                        }
                        break;

                }
            }
        }

        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Addresses:
                    view = (GridView)gridControlAddresses.MainView;
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    view = (GridView)gridControlNextOfKin.MainView;
                    break;
                case Utils.enmMainGrids.Training:
                    view = (GridView)gridControlTraining.MainView;
                    break;
                case Utils.enmMainGrids.Discipline:
                    view = (GridView)gridControlDiscipline.MainView;
                    break;
                case Utils.enmMainGrids.Vetting:
                    view = (GridView)gridControlVetting.MainView;
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    view = (GridView)gridControlDeptWorked.MainView;
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    view = (GridView)gridControlWorkingPatternHeader.MainView;
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    view = (GridView)gridControlWorkingPattern.MainView;
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    view = (GridView)gridControlHolidayYear.MainView;
                    break;
                case Utils.enmMainGrids.Absences:
                    view = (GridView)gridControlAbsences.MainView;
                    break;
                case Utils.enmMainGrids.Bonus:
                    view = (GridView)gridControlAbsences.MainView;
                    break;
                case Utils.enmMainGrids.Pensions:
                    view = (GridView)gridControlPension.MainView;
                    break;
                case Utils.enmMainGrids.CRM:
                    view = (GridView)gridControlCRM.MainView;
                    break;
                case Utils.enmMainGrids.Reference:
                    view = (GridView)gridControlReference.MainView;
                    break;
                case Utils.enmMainGrids.PayRise:
                    view = (GridView)gridControlReference.MainView;
                    break;
                case Utils.enmMainGrids.Shares:
                    view = (GridView)gridControlShares.MainView;
                    break;
                default:
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();

            int intEmployeeID = 0;
            int intElegibleForBonus = 0;
            int intElegibleForShares = 0;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow != null)
            {
                intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                intElegibleForBonus = (string.IsNullOrEmpty(currentRow["ElegibleForBonus"].ToString()) ? 0 : Convert.ToInt32(currentRow["ElegibleForBonus"]));
                intElegibleForShares = (string.IsNullOrEmpty(currentRow["ElegibleForShares"].ToString()) ? 0 : Convert.ToInt32(currentRow["ElegibleForShares"]));
            }
            bbiLinkedDocuments.Enabled = intEmployeeID > 0;  // Set status of Linked Documents button //

            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Addresses:
                case Utils.enmMainGrids.Training:
                case Utils.enmMainGrids.Discipline:
                case Utils.enmMainGrids.Vetting:
                case Utils.enmMainGrids.DepartmentsWorked:
                case Utils.enmMainGrids.WorkPatternHeader:
                case Utils.enmMainGrids.WorkPattern:
                case Utils.enmMainGrids.HolidayYear:
                case Utils.enmMainGrids.Absences:
                case Utils.enmMainGrids.Pensions:
                case Utils.enmMainGrids.CRM:
                case Utils.enmMainGrids.Reference:
                    if (strFormMode == "edit")
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        bbiBlockAdd.Enabled = false;
                        if (intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            if (intRowHandles.Length >= 2)
                            {
                                alItems.Add("iBlockEdit");
                                bbiBlockEdit.Enabled = true;
                            }
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    if (strFormMode == "edit")
                    {
                        if (intElegibleForBonus == 1)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                            bbiBlockAdd.Enabled = false;
                            if (intRowHandles.Length >= 1)
                            {
                                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                                bsiEdit.Enabled = true;
                                bbiSingleEdit.Enabled = true;
                                if (intRowHandles.Length >= 2)
                                {
                                    alItems.Add("iBlockEdit");
                                    bbiBlockEdit.Enabled = true;
                                }
                                alItems.Add("iDelete");
                                bbiDelete.Enabled = true;
                            }
                        }
                        else
                        {
                            bsiAdd.Enabled = false;
                            bbiSingleAdd.Enabled = false;
                            bbiBlockAdd.Enabled = false;
                            bsiEdit.Enabled = false;
                            bbiSingleEdit.Enabled = false;
                            bbiBlockEdit.Enabled = false;
                            bbiDelete.Enabled = false;
                        }
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    if (strFormMode == "edit")
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                        bbiBlockAdd.Enabled = false;
                        if (intRowHandles.Length >= 1)
                        {
                            alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                            bsiEdit.Enabled = true;
                            bbiSingleEdit.Enabled = true;
                            //if (intRowHandles.Length >= 2)
                            //{
                            //    alItems.Add("iBlockEdit");
                            //    bbiBlockEdit.Enabled = true;
                            //}
                            alItems.Add("iDelete");
                            bbiDelete.Enabled = true;
                        }
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    if (strFormMode == "edit")
                    {
                        if (intElegibleForShares == 1)
                        {
                            alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                            bsiAdd.Enabled = true;
                            bbiSingleAdd.Enabled = true;
                            bbiBlockAdd.Enabled = false;
                            if (intRowHandles.Length >= 1)
                            {
                                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                                bsiEdit.Enabled = true;
                                bbiSingleEdit.Enabled = true;
                                if (intRowHandles.Length >= 2)
                                {
                                    alItems.Add("iBlockEdit");
                                    bbiBlockEdit.Enabled = true;
                                }
                                alItems.Add("iDelete");
                                bbiDelete.Enabled = true;
                            }
                        }
                        else
                        {
                            bsiAdd.Enabled = false;
                            bbiSingleAdd.Enabled = false;
                            bbiBlockAdd.Enabled = false;
                            bsiEdit.Enabled = false;
                            bbiSingleEdit.Enabled = false;
                            bbiBlockEdit.Enabled = false;
                            bbiDelete.Enabled = false;
                        }
                    }
                    break;

                default:
                    break;
            }
            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            view = (GridView)gridControlAddresses.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0  && i_boolAddressData;
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolAddressData;
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolAddressData;
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolAddressData;
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolAddressData;
            gridControlAddresses.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolAddressData;

            view = (GridView)gridControlNextOfKin.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolNextOfKinData;
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolNextOfKinData;
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolNextOfKinData;
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolNextOfKinData;
            gridControlNextOfKin.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolNextOfKinData;

            view = (GridView)gridControlTraining.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolTrainingData;
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolTrainingData;
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolTrainingData;
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolTrainingData;
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolTrainingData;
            gridControlTraining.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolTrainingData;

            view = (GridView)gridControlDiscipline.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolDisciplineData;
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolDisciplineData;
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolDisciplineData;
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolDisciplineData;
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolDisciplineData;
            gridControlDiscipline.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolDisciplineData;

            view = (GridView)gridControlVetting.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolVettingData;
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolVettingData;
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolVettingData;
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolVettingData;
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolVettingData;
            gridControlVetting.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolVettingData;

            view = (GridView)gridControlDeptWorked.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlDeptWorked.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolDeptsWorkedData;
            gridControlDeptWorked.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolDeptsWorkedData;
            gridControlDeptWorked.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolDeptsWorkedData;
            gridControlDeptWorked.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolDeptsWorkedData;
            gridControlDeptWorked.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolDeptsWorkedData;
            gridControlDeptWorked.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolDeptsWorkedData;

            view = (GridView)gridControlWorkingPatternHeader.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolWorkPatternData;
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolWorkPatternData;
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolWorkPatternData;
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolWorkPatternData;
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolWorkPatternData;
            gridControlWorkingPatternHeader.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intEmployeeID > 0 && i_boolWorkPatternData;

            view = (GridView)gridControlWorkingPattern.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolWorkPatternData;
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolWorkPatternData;
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolWorkPatternData;
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolWorkPatternData;
            gridControlWorkingPattern.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolWorkPatternData;

            view = (GridView)gridControlHolidayYear.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolHolidayData;
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolHolidayData;
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolHolidayData;
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolHolidayData;
            gridControlHolidayYear.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolHolidayData;

            view = (GridView)gridControlAbsences.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolHolidayData;
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolHolidayData;
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolHolidayData;
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolHolidayData;
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolHolidayData;
            gridControlAbsences.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolHolidayData;

            view = (GridView)gridControlBonuses.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && intElegibleForBonus == 1 && i_boolBonusData;
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && intElegibleForBonus == 1 && i_boolBonusData;
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && intElegibleForBonus == 1 && i_boolBonusData;
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolBonusData;
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolBonusData;
            gridControlBonuses.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolBonusData;

            view = (GridView)gridControlPension.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolPensionData;
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolPensionData;
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolPensionData;
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolPensionData;
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolPensionData;
            gridControlPension.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolPensionData;

            view = (GridView)gridControlCRM.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolCRMData;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolCRMData;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolCRMData;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolCRMData;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolCRMData;
            gridControlCRM.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolCRMData;

            view = (GridView)gridControlReference.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolReferenceData;
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolReferenceData;
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolReferenceData;
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolReferenceData;
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolReferenceData;
            gridControlReference.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolReferenceData;

            view = (GridView)gridControlPayRise.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && i_boolPayRiseData;
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolPayRiseData;
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && i_boolPayRiseData;
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolPayRiseData;
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolPayRiseData;
            gridControlPayRise.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolPayRiseData;
            
            view = (GridView)gridControlShares.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = strFormMode == "edit" && intEmployeeID > 0 && intElegibleForShares == 1 && i_boolShareData;
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && intElegibleForShares == 1 && i_boolShareData;
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = strFormMode == "edit" && intRowHandles.Length > 0 && intElegibleForShares == 1 && i_boolShareData;
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0 && i_boolShareData;
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = i_boolShareData;
            gridControlShares.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = intRowHandles.Length == 1 && i_boolShareData;
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmMainGrids grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmMainGrids.Addresses:
                    i_str_AddedAddressRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    i_str_AddedNextOfKinRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.Training:
                    i_str_AddedTrainingRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.Discipline:
                    i_str_AddedDisciplineRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.Vetting:
                    i_str_AddedVettingRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    i_str_AddedDeptWorkedRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    i_str_AddedWorkingPatternHeaderRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    i_str_AddedWorkingPatternRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    i_str_AddedHolidayYearRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.Absences:
                    i_str_AddedAbsenceRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.Bonus:
                    i_str_AddedBonusRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.Pensions:
                    i_str_AddedPensionRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.CRM:
                    i_str_AddedCRMRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.Reference:
                    i_str_AddedReferenceRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.PayRise:
                    i_str_AddedPayRiseRecordIDs = newIds;
                    break;
                case Utils.enmMainGrids.Shares:
                    i_str_AddedSharesRecordIDs = newIds;
                    break;
                default:
                    break;
            }
        }


        private void LoadLinked_Addresses()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolAddressData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlAddresses.MainView;
                view.BeginUpdate();
                RefreshGridViewStateAddress.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00009_GetEmployeeAddresses.Clear();
                }
                else
                {
                    sp_HR_00009_GetEmployeeAddressesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00009_GetEmployeeAddresses, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateAddress.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedAddressRecordIDs != "")
                {
                    strArray = i_str_AddedAddressRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["AddressId"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedAddressRecordIDs = "";
                }
            }
        }

        private void LoadLinked_NextOfKin()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolNextOfKinData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlNextOfKin.MainView;
                view.BeginUpdate();
                RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00021_Get_Next_Of_Kin_For_Employees.Clear();
                }
                else
                {
                    sp_HR_00021_Get_Next_Of_Kin_For_EmployeesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00021_Get_Next_Of_Kin_For_Employees, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateNextOfKin.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedNextOfKinRecordIDs != "")
                {
                    strArray = i_str_AddedNextOfKinRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["Id"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedNextOfKinRecordIDs = "";
                }
            }
        }

        private void LoadLinked_Training()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolTrainingData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlTraining.MainView;
                view.BeginUpdate();
                RefreshGridViewStateTraining.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp09113_HR_Qualifications_For_Employee.Clear();
                }
                else
                {
                    sp09113_HR_Qualifications_For_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp09113_HR_Qualifications_For_Employee, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateTraining.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedTrainingRecordIDs != "")
                {
                    strArray = i_str_AddedTrainingRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["QualificationID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedTrainingRecordIDs = "";
                }
            }
        }

        private void LoadLinked_Discipline()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolDisciplineData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlDiscipline.MainView;
                view.BeginUpdate();
                RefreshGridViewStateDiscipline.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00053_Get_Employee_Sanctions.Clear();
                }
                else
                {
                    sp_HR_00053_Get_Employee_SanctionsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00053_Get_Employee_Sanctions, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateDiscipline.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedDisciplineRecordIDs != "")
                {
                    strArray = i_str_AddedDisciplineRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SanctionID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedDisciplineRecordIDs = "";
                }
            }
        }

        private void LoadLinked_Vetting()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolVettingData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlVetting.MainView;
                view.BeginUpdate();
                RefreshGridViewStateVetting.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00131_Get_Employee_Vetting.Clear();
                }
                else
                {
                    sp_HR_00131_Get_Employee_VettingTableAdapter.Fill(dataSet_HR_Core.sp_HR_00131_Get_Employee_Vetting, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateVetting.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedVettingRecordIDs != "")
                {
                    strArray = i_str_AddedVettingRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["VettingID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedVettingRecordIDs = "";
                }
            }
        }

        private void LoadLinked_DeptWorked()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolDeptsWorkedData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlDeptWorked.MainView;
                view.BeginUpdate();
                RefreshGridViewStateDeptWorked.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00001_Employee_Depts_Worked_Linked_To_Contract.Clear();
                }
                else
                {
                    sp_HR_00001_Employee_Depts_Worked_Linked_To_ContractTableAdapter.Fill(dataSet_HR_Core.sp_HR_00001_Employee_Depts_Worked_Linked_To_Contract, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateDeptWorked.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedDeptWorkedRecordIDs != "")
                {
                    strArray = i_str_AddedDeptWorkedRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["EmployeeDeptWorkedID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedDeptWorkedRecordIDs = "";
                }
            }
        }

        private void LoadLinked_WorkingPatternHeader()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolWorkPatternData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlWorkingPatternHeader.MainView;
                view.BeginUpdate();
                RefreshGridViewStateWorkingPatternHeader.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00154_Employee_Working_Pattern_Headers.Clear();
                }
                else
                {
                    sp_HR_00154_Employee_Working_Pattern_HeadersTableAdapter.Fill(dataSet_HR_Core.sp_HR_00154_Employee_Working_Pattern_Headers, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateWorkingPatternHeader.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedWorkingPatternHeaderRecordIDs != "")
                {
                    strArray = i_str_AddedWorkingPatternHeaderRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["WorkingPatternHeaderID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedWorkingPatternHeaderRecordIDs = "";
                }
            }
        }

        private void LoadLinked_WorkingPattern()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolWorkPatternData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                var view = (GridView)gridControlWorkingPatternHeader.MainView;
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                string strSelectedIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["WorkingPatternHeaderID"])) + ',';
                }

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                view = (GridView)gridControlWorkingPattern.MainView;
                view.BeginUpdate();
                RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strSelectedIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00046_Get_Employee_Working_Patterns.Clear();
                }
                else
                {
                    sp_HR_00046_Get_Employee_Working_PatternsTableAdapter.Fill(dataSet_HR_Core.sp_HR_00046_Get_Employee_Working_Patterns, strSelectedIDs);
                    RefreshGridViewStateWorkingPattern.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedWorkingPatternRecordIDs != "")
                {
                    strArray = i_str_AddedWorkingPatternRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["WorkingPatternID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedWorkingPatternRecordIDs = "";
                }
            }
        }

        private void LoadLinked_HolidayYears()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolHolidayData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlHolidayYear.MainView;
                view.BeginUpdate();
                RefreshGridViewStateHolidayYear.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00158_Holiday_Years_For_Employee.Clear();
                }
                else
                {
                    sp_HR_00158_Holiday_Years_For_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp_HR_00158_Holiday_Years_For_Employee, Convert.ToInt32(checkEditShowActiveHolidayYearOnly.EditValue), intEmployeeID.ToString() + ",");
                    RefreshGridViewStateHolidayYear.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedHolidayYearRecordIDs != "")
                {
                    strArray = i_str_AddedHolidayYearRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["EmployeeHolidayYearID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedHolidayYearRecordIDs = "";
                }
            }
        }

        private void LoadLinked_Absences()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolHolidayData)
            {
                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                var view = (GridView)gridControlHolidayYear.MainView;
                int[] intRowHandles = view.GetSelectedRows();
                int intCount = intRowHandles.Length;
                string strSelectedIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["EmployeeHolidayYearID"])) + ',';
                }

                gridControlAbsences.MainView.BeginUpdate();
                this.RefreshGridViewStateAbsence.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    this.dataSet_HR_Core.sp_HR_00037_Get_Employee_Absences.Clear();
                }
                else
                {
                    sp_HR_00037_Get_Employee_AbsencesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00037_Get_Employee_Absences, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), Convert.ToInt32(checkEditShowActiveHolidayYearOnly.EditValue), i_str_select_absence_type_ids);
                    this.RefreshGridViewStateAbsence.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlAbsences.MainView.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedAbsenceRecordIDs != "")
                {
                    strArray = i_str_AddedAbsenceRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlAbsences.MainView;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["AbsenceID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedAbsenceRecordIDs = "";
                }
            }
        }

        private void LoadLinked_Bonuses()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolBonusData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlBonuses.MainView;
                view.BeginUpdate();
                RefreshGridViewStateBonus.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00172_Employee_Bonuses.Clear();
                }
                else
                {
                    sp_HR_00172_Employee_BonusesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00172_Employee_Bonuses, intEmployeeID.ToString() + ",", ShowSalaryData);
                    RefreshGridViewStateBonus.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                SimplerAES Encryptor = new SimplerAES();
                string strAmount = "";
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00172_Employee_Bonuses.Rows)
                    {
                        strAmount = dr["GuaranteeBonusAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["GuaranteeBonusAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["BonusAmountPaid"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["BonusAmountPaid"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedBonusRecordIDs != "")
                {
                    strArray = i_str_AddedBonusRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["BonusID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedBonusRecordIDs = "";
                }
            }
        }

        private void LoadLinked_Pensions()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolPensionData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlPension.MainView;
                view.BeginUpdate();
                RefreshGridViewStatePension.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00016_Pensions_For_Employee.Clear();
                }
                else
                {
                    sp_HR_00016_Pensions_For_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp_HR_00016_Pensions_For_Employee, intEmployeeID.ToString() + ",", ShowSalaryData);
                    RefreshGridViewStatePension.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                SimplerAES Encryptor = new SimplerAES();
                string strAmount = "";
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00172_Employee_Bonuses.Rows)
                    {
                        strAmount = dr["MinimumContributionAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["MinimumContributionAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["MaximumContributionAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["MaximumContributionAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["CurrentContributionAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["CurrentContributionAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["TotalEmployeeContributionToDate"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["TotalEmployeeContributionToDate"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["TotalEmployerContributionToDate"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["TotalEmployerContributionToDate"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedPensionRecordIDs != "")
                {
                    strArray = i_str_AddedPensionRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["PensionID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedPensionRecordIDs = "";
                }
            }
        }

        private void LoadLinked_CRM()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolCRMData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlCRM.MainView;
                view.BeginUpdate();
                RefreshGridViewStateCRM.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00193_Employee_CRM_Linked_To_Employee.Clear();
                }
                else
                {
                    sp_HR_00193_Employee_CRM_Linked_To_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp_HR_00193_Employee_CRM_Linked_To_Employee, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateCRM.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedCRMRecordIDs != "")
                {
                    strArray = i_str_AddedCRMRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["CRMID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedCRMRecordIDs = "";
                }
            }
        }

        private void LoadLinked_References()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolReferenceData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlReference.MainView;
                view.BeginUpdate();
                RefreshGridViewStateReference.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00200_Employee_References_Linked_To_Employee.Clear();
                }
                else
                {
                    sp_HR_00200_Employee_References_Linked_To_EmployeeTableAdapter.Fill(dataSet_HR_Core.sp_HR_00200_Employee_References_Linked_To_Employee, intEmployeeID.ToString() + ",");
                    RefreshGridViewStateReference.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedReferenceRecordIDs != "")
                {
                    strArray = i_str_AddedReferenceRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["ReferenceID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedReferenceRecordIDs = "";
                }
            }
        }

        private void LoadLinked_PayRises()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolPayRiseData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlPayRise.MainView;
                view.BeginUpdate();
                RefreshGridViewStatePayRise.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00213_Employee_PayRises.Clear();
                }
                else
                {
                    sp_HR_00213_Employee_PayRisesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00213_Employee_PayRises, intEmployeeID.ToString() + ",", ShowSalaryData);
                    RefreshGridViewStatePayRise.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                SimplerAES Encryptor = new SimplerAES();
                string strAmount = "";
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00213_Employee_PayRises.Rows)
                    {
                        strAmount = dr["PreviousPayAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["PreviousPayAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["PayRiseAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["PayRiseAmount"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["PayRisePercentage"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["PayRisePercentage"] = Encryptor.Decrypt(strAmount);
                        strAmount = dr["NewPayAmount"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["NewPayAmount"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedPayRiseRecordIDs != "")
                {
                    strArray = i_str_AddedPayRiseRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["PayRiseID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedPayRiseRecordIDs = "";
                }
            }
        }

        private void LoadLinked_Shares()
        {
            if (strFormMode == "add" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            if (i_boolShareData)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                if (intEmployeeID <= 0) return;

                if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
                GridView view = (GridView)gridControlShares.MainView;
                view.BeginUpdate();
                RefreshGridViewStateShares.SaveViewInfo();  // Store expanded groups and selected rows //
                if (string.IsNullOrWhiteSpace(strRecordIDs) || strFormMode == "blockadd" || strFormMode == "blockedit")
                {
                    this.dataSet_HR_Core.sp_HR_00224_Employee_Shares.Clear();
                }
                else
                {
                    sp_HR_00224_Employee_SharesTableAdapter.Fill(dataSet_HR_Core.sp_HR_00224_Employee_Shares, intEmployeeID.ToString() + ",", ShowSalaryData);
                    RefreshGridViewStateShares.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                // Decrypt all Financial Data //
                SimplerAES Encryptor = new SimplerAES();
                string strAmount = "";
                try
                {
                    foreach (DataRow dr in dataSet_HR_Core.sp_HR_00224_Employee_Shares.Rows)
                    {
                        strAmount = dr["UnitValue"].ToString();
                        if (!string.IsNullOrEmpty(strAmount)) dr["UnitValue"] = Encryptor.Decrypt(strAmount);
                    }
                }
                catch (Exception ex) { }
                view.EndUpdate();

                char[] delimiters = new char[] { ';' };
                string[] strArray = null;
                int intID = 0;
                // Highlight any recently added new rows //
                if (i_str_AddedSharesRecordIDs != "")
                {
                    strArray = i_str_AddedSharesRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["SharesID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedSharesRecordIDs = "";
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp09002HREmployeeItemBindingSource.EndEdit();
            try
            {
                this.sp09002_HR_Employee_ItemTableAdapter.Update(dataSet_HR_DataEntry);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow != null)
                {
                    strNewIDs = Convert.ToInt32(currentRow["EmployeeId"]) + ";";
                    strRecordIDs = Convert.ToInt32(currentRow["EmployeeId"]) + ",";
                }

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["strMode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    
                    LoadLinked_Addresses();
                    GridView view = (GridView)gridControlAddresses.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_NextOfKin();
                    view = (GridView)gridControlAddresses.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_Training();
                    view = (GridView)gridControlAddresses.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_Discipline();
                    view = (GridView)gridControlDiscipline.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_Vetting();
                    view = (GridView)gridControlVetting.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_DeptWorked();
                    view = (GridView)gridControlDeptWorked.MainView;
                    view.ExpandAllGroups();   
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        frm_HR_Employee_Manager fParentForm;
                        fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmMainGrids.Employees, strNewIDs);
                    }
                    else if (frmChild.Name == "frm_HR_Report_Payroll_Export_Absences")
                    {
                        frm_HR_Report_Payroll_Export_Absences fParentForm;
                        fParentForm = (frm_HR_Report_Payroll_Export_Absences)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs, "");
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        
                        // Following commented out code can be used to test to see which columns have changed //
                        /*
                        DataRow row =  dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows[i];
                        foreach (DataColumn col in row.Table.Columns)
                        {
                            if (row[col, DataRowVersion.Original] != DBNull.Value && row[col, DataRowVersion.Current] != DBNull.Value)
                            {
                                if (row[col, DataRowVersion.Original].ToString().Equals(row[col, DataRowVersion.Current].ToString(), StringComparison.InvariantCulture))
                                {
                                    string str = "";
                                }
                                else
                                {
                                    string str2 = "";
                                }
                            }
                        }
                        */
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }

/*            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.First:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveFirst();
                    }
                    e.Handled = true;
                    break;
                case NavigatorButtonType.Prev:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MovePrevious();
                    }
                    e.Handled = true;
                    break;
                case NavigatorButtonType.Next:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveNext();
                    }
                    e.Handled = true;
                    break;
                case NavigatorButtonType.Last:
                    if (dxErrorProvider1.HasErrors)
                    {
                        string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                        XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                    else
                    {
                        BindingSource bs = (BindingSource)dataNavigator1.DataSource;
                        bs.MoveLast();
                    }
                    e.Handled = true;
                    break;
            }*/
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    Load_Employee_Image();
                    Set_Editor_Masks();
                    Set_Editor_Mask_SickPay1();
                    Set_Editor_Mask_SickPay2();
                    Set_Editor_Masks3();
                    Set_Editor_Back_Colours();
                    
                    Set_DIS_Fields_Read_Only();
                    Set_IP_Fields_Read_Only();

                    Decrypt_Financial_Fields();

                    if (this.strFormMode == "add" || this.strFormMode == "blockedit" || this.strFormMode == "blockadd") return;
                    LoadLinked_Addresses();
                    GridView view = (GridView)gridControlAddresses.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_NextOfKin();
                    view = (GridView)gridControlAddresses.MainView;
                    view.ExpandAllGroups();
                    
                    LoadLinked_Training();
                    view = (GridView)gridControlTraining.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_Discipline();
                    view = (GridView)gridControlDiscipline.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_Vetting();
                    view = (GridView)gridControlVetting.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_DeptWorked();
                    view = (GridView)gridControlDeptWorked.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_WorkingPatternHeader();
                    view = (GridView)gridControlWorkingPatternHeader.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_HolidayYears();
                    view = (GridView)gridControlHolidayYear.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_Bonuses();
                    view = (GridView)gridControlBonuses.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_Pensions();
                    view = (GridView)gridControlPension.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_CRM();
                    view = (GridView)gridControlCRM.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_References();
                    view = (GridView)gridControlReference.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_PayRises();
                    view = (GridView)gridControlPayRise.MainView;
                    view.ExpandAllGroups();

                    LoadLinked_Shares();
                    view = (GridView)gridControlShares.MainView;
                    view.ExpandAllGroups();
                }
            }
        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void FirstnameTextEdit_Validating(object sender, CancelEventArgs e)
         {
             TextEdit te = (TextEdit)sender;
             if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
             {
                 dxErrorProvider1.SetError(FirstnameTextEdit, "Enter a value.");
                 e.Cancel = true;  // Show stop icon as field is invalid //
                 return;
             }
             else
             {
                 dxErrorProvider1.SetError(FirstnameTextEdit, "");
             }
         }

        private void SurnameTextEdit_Validating(object sender, CancelEventArgs e)
         {
             TextEdit te = (TextEdit)sender;
             if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(te.EditValue.ToString()))
             {
                 dxErrorProvider1.SetError(SurnameTextEdit, "Enter a value.");
                 e.Cancel = true;  // Show stop icon as field is invalid //
                 return;
             }
             else
             {
                 dxErrorProvider1.SetError(SurnameTextEdit, "");
             }
         }

        private void genderGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
         {
             if (strFormMode == "view") return;
             if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
             {
                 if ("edit".Equals(e.Button.Tag))
                 {
                     Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 302, "Gender Types");
                 }
                 else if ("reload".Equals(e.Button.Tag))
                 {
                     sp_HR_00060_Gender_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00060_Gender_List_With_Blank);
                 }
             }
         }

        private void ethnicityIdComboBoxEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
         {
               if (strFormMode == "view") return;
               if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
               {
                   if ("edit".Equals(e.Button.Tag))
                   {
                       Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 303, "Ethnicity Types");
                   }
                   else if ("reload".Equals(e.Button.Tag))
                   {
                       sp_HR_00061_Ethnicity_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00061_Ethnicity_List_With_Blank);
                   }
               }
         }

        private void MaritalStatusIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
         {
             if (strFormMode == "view") return;
             if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
             {
                 if ("edit".Equals(e.Button.Tag))
                 {
                     Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 323, "Marital Statuses");
                 }
                 else if ("reload".Equals(e.Button.Tag))
                 {
                     sp_HR_00108_Marital_Statuses_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00108_Marital_Statuses_List_With_Blank);
                 }
             }
         }

        private void ReligionIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
         {
             if (strFormMode == "view") return;
             if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
             {
                 if ("edit".Equals(e.Button.Tag))
                 {
                     Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 324, "Religions");
                 }
                 else if ("reload".Equals(e.Button.Tag))
                 {
                     sp_HR_00109_Religions_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00109_Religions_List_With_Blank);
                 }
             }
         }

        private void DisabilityIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 309, "Disability Categories");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00110_Disabilities_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00110_Disabilities_List_With_Blank);
                }
            }
        }

        private void workEmailAddressTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            dxErrorProvider1.SetError((Control)sender, "");
            TextEdit te = (TextEdit)sender;

            string email = te.EditValue.ToString();

            //Block mode disallows entering the work email address as it has to be unique
            if (this.strFormMode == "blockedit")
            {
                if (email.Length != 0)
                {
                    dxErrorProvider1.SetError((Control)sender,
                        "Work Email must be unique so cannot be entered when block editing");
                    e.Cancel = true;
                }
                return;
            }

            //Continue validating the email for othe modes.
            bool mandatory = (CurrentEmployeeCheckEdit.Checked);

            string message = "";
            if (!Classes.FormatCheck.emailIsValid(te.EditValue.ToString(), mandatory, out message))
            {
                dxErrorProvider1.SetError((Control)sender, message);
                e.Cancel = true;
                return;
            }

            //May not be suitable for block mode.
            int employeeID = 0;

            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            int.TryParse(currentRow["EmployeeID"].ToString(), out employeeID);

            sp_HR_00270_Employee_Work_Email_CheckTableAdapter.Fill(dataSet_HR_Core.sp_HR_00270_Employee_Work_Email_Check, employeeID, te.EditValue.ToString());

            DataRow dr = dataSet_HR_Core.sp_HR_00270_Employee_Work_Email_Check.Rows[0];

            int number = 0;
            int.TryParse(dr["Number"].ToString(), out number);

            if (number > 0)
            {
                dxErrorProvider1.SetError((Control)sender, "This Email address is already in use by another employee");
                e.Cancel = true;
                return;
            }
        }

        private void doBDateEdit_EditValueChanged(object sender, EventArgs e)
         {
             if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
             ibool_ignoreValidation = false;  // Toggles validation on and off //         
         }
        private void doBDateEdit_Validated(object sender, EventArgs e)
        {
             if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
             if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
             ibool_ignoreValidation = true;
             Calculate_Age();
         }
        private void Calculate_Age()
         {
             DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
             if (currentRow == null) return;
             DateTime DOB = (!String.IsNullOrEmpty(doBDateEdit.DateTime.ToString()) ? doBDateEdit.DateTime : DateTime.MinValue);

             DateTime today = DateTime.Today;
             int age = today.Year - DOB.Year;
             if (DOB > today.AddYears(-age)) age--;    // Handle where the todays month and day hasn't quite reach the person's birthday month and day //

             currentRow["CalculatedAge"] = (DOB >= today || DOB == DateTime.MinValue ? 0 : age);
             sp09002HREmployeeItemBindingSource.EndEdit();
         }

        private void PicturePathButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;

            if (e.Button.Tag.ToString() == "choose")
            {
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    String strDefaultPath = this.GlobalSettings.StaffPhotoPath;
                    dlg.DefaultExt = "jpg";
                    dlg.Filter = "";
                    if (strDefaultPath != "") dlg.InitialDirectory = strDefaultPath;
                    dlg.Multiselect = false;
                    dlg.ShowDialog();
                    if (dlg.FileNames.Length > 0)
                    {
                        string strTempFileName = "";
                        string strTempResult = "";
                        string strExtension = "";
                        foreach (string filename in dlg.FileNames)
                        {
                            if (strDefaultPath != "")
                                if (!filename.ToLower().StartsWith(strDefaultPath.ToLower()))
                                {
                                    DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the System Configuration Screen - Staff Pictures Folder] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select Image File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                    break;
                                }
                            strTempFileName = filename.Substring(strDefaultPath.Length);
                            if (strTempResult == "")
                            {
                                if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                            }
                            else
                            {
                                if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                            }
                            // Check extension from filename //
                            strExtension = System.IO.Path.GetExtension(filename);
                            if (string.IsNullOrEmpty(strExtension))
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files with an image type extension (JPG, GIF, PNG, BMP).\n\nFile Selection Aborted!", "Select Image File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                break;
                            }
                            strExtension = strExtension.Substring(1).ToUpper();  // Remove the preceeding dot //
                            if (!(strExtension == "JPG" || strExtension == "GIF" || strExtension == "PNG" || strExtension == "BMP"))
                            {
                                DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files with an image type extension (JPG, GIF, PNG, BMP).\n\nFile Selection Aborted!", "Select Image File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                break;
                            }
                        }
                        currentRow["PicturePath"] = strTempResult;
                    }
                    sp09002HREmployeeItemBindingSource.EndEdit();
                }
                Load_Employee_Image();
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                if (XtraMessageBox.Show("You are about to clear the Employee Picture.\n\nProceed?", "Clear Employee Picture", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    currentRow["PicturePath"] = null;
                    sp09002HREmployeeItemBindingSource.EndEdit();
                }
            }
        }
        private void Load_Employee_Image()
        {
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strImagePath = currentRow["PicturePath"].ToString();
            if (string.IsNullOrEmpty(strImagePath))
            {
                try
                {
                    strImagePath = this.GlobalSettings.StaffPhotoPath + "\\" + this.GlobalSettings.DefaultPhoto;
                }
                catch (Exception ex) { } 
            }
            else
            {
                strImagePath = this.GlobalSettings.StaffPhotoPath + strImagePath;
            }
            try
            {
                EmployeePictureEdit.Image = Image.FromFile(strImagePath);
            }
            catch (Exception ex1)
            {
                try
                {
                    EmployeePictureEdit.Image = Image.FromFile(this.GlobalSettings.StaffPhotoPath + "\\" + this.GlobalSettings.DefaultPhoto);
                }
                catch (Exception ex2) { }
            }
        }

        private void EmployeeNumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = "HR_Employee";
                fChildForm.PassedInFieldName = "EmployeeNumber";
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    EmployeeNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    _strLastUsedReferencePrefix = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                Get_Suffix_Number();  // Get next value in sequence //  
            }
        }
        private void Get_Suffix_Number()
        {
            string strSequence = EmployeeNumberButtonEdit.EditValue.ToString() ?? "";

            SequenceNumberGetNext sequence = new SequenceNumberGetNext();
            string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, "EmployeeNumber", "HR_Employee", strSequence);  // Calculate Next in Sequence //

            if (strNextNumber == "")
            {
                return;
            }
            if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

            int intRequiredLength = strNextNumber.Length;
            int intNextNumber = Convert.ToInt32(strNextNumber);
            int intIncrement = 0;
            string strTempNumber = "";


            // Check if value is already present within dataset //
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strCurrentActionID = "";
            if (!string.IsNullOrEmpty(currentRow["EmployeeId"].ToString())) strCurrentActionID = currentRow["EmployeeId"].ToString();

            bool boolUniqueValueFound = false;
            bool boolDuplicateFound = false;
            do
            {
                boolDuplicateFound = false;
                strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                }
                foreach (DataRow dr in this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows)
                {
                    if (dr["EmployeeNumber"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                    {
                        intIncrement++;
                        boolDuplicateFound = true;
                        break;
                    }
                }
                if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
            } while (!boolUniqueValueFound);
            EmployeeNumberButtonEdit.EditValue = strSequence + strTempNumber;
        }

        private void ElegibleForBonusCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ElegibleForBonusCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            SetMenuStatus();
        }

        private void TerminationDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void TerminationDateDateEdit_Validated(object sender, EventArgs e)
        {
            //if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //

            DateEdit de = (DateEdit)sender;
            if (de.DateTime != DateTime.MinValue)
            {
                // Following not required as SQL Server Job will handle this when the date left is reached //
                //DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                //if (currentRow == null) return;
                //currentRow["CurrentEmployee"] = 0;
                //sp09002HREmployeeItemBindingSource.EndEdit();
            }
            ibool_ignoreValidation = true;
            SetMenuStatus();
        }

        private void StaffNameButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            var currentRowView = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            var currentRow = (DataSet_HR_DataEntry.sp09002_HR_Employee_ItemRow)currentRowView.Row;
            if (currentRow == null) return;
            if (e.Button.Tag.ToString() == "choose")
            {
                int intStaffID = (string.IsNullOrEmpty(currentRow.StaffID.ToString()) ? 0 : Convert.ToInt32(currentRow.StaffID));
                string strFilterIDs = (intStaffID > 0 ? intStaffID.ToString() + "," : "");

                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedFilterType = "available_staff_for_linking_to_employee";
                fChildForm.strPassedInFilterRecordIDs = strFilterIDs;
                fChildForm.intOriginalStaffID = intStaffID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.StaffID = fChildForm.intSelectedStaffID;
                    currentRow.StaffName = fChildForm.strSelectedStaffName;
                    sp09002HREmployeeItemBindingSource.EndEdit();
                }
            }
            else if (e.Button.Tag.ToString() == "clear")
            {
                currentRow.StaffID = 0;
                currentRow.StaffName = "";
                sp09002HREmployeeItemBindingSource.EndEdit();
            }
        }

        private void DISCoverEligibleCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void DISCoverEligibleCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            CheckEdit ce = (CheckEdit)sender;
            if (!ce.Checked)
            {
                DISLevelCoverIDGridLookUpEdit.EditValue = 0;
                DISSchemeIDGridLookUpEdit.EditValue = 0;
            }
            Set_DIS_Fields_Read_Only();
        }
        private void Set_DIS_Fields_Read_Only()
        {
            var currentRowView = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            var currentRow = (DataSet_HR_DataEntry.sp09002_HR_Employee_ItemRow)currentRowView.Row;
            if (currentRow == null) return;
            bool ReadOnly = true;
            object value = currentRow["DISCoverEligible"];
            if (value != DBNull.Value)ReadOnly = (Convert.ToInt32(currentRow.DISCoverEligible) == 0 ? true : false);

            DISLevelCoverIDGridLookUpEdit.Properties.ReadOnly = ReadOnly;
            DISSchemeIDGridLookUpEdit.Properties.ReadOnly = ReadOnly;
        }
        
        private void DISLevelCoverIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 377, "DIS Cover Levels");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00271_DIS_Cover_Levels_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00271_DIS_Cover_Levels_With_Blank);
                }
            }
        }

        private void DISSchemeIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 378, "DIS Schemes");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00272_DIS_Schemes_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00272_DIS_Schemes_With_Blank);
                }
            }
        }

        private void IPEligibleCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void IPEligibleCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            CheckEdit ce = (CheckEdit)sender;
            if (!ce.Checked)
            {
                IPLevelIDGridLookUpEdit.EditValue = 0;
                IPDeferralPeriodIDGridLookUpEdit.EditValue = 0;
                IPSchemeIDGridLookUpEdit.EditValue = 0;
            }
            Set_IP_Fields_Read_Only();
        }
        private void Set_IP_Fields_Read_Only()
        {
            var currentRowView = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            var currentRow = (DataSet_HR_DataEntry.sp09002_HR_Employee_ItemRow)currentRowView.Row;
            if (currentRow == null) return;

            bool ReadOnly = true;
            object value = currentRow["IPEligible"];
            if (value != DBNull.Value) ReadOnly = (Convert.ToInt32(currentRow.IPEligible) == 0 ? true : false);

            IPLevelIDGridLookUpEdit.Properties.ReadOnly = ReadOnly;
            IPDeferralPeriodIDGridLookUpEdit.Properties.ReadOnly = ReadOnly;
            IPSchemeIDGridLookUpEdit.Properties.ReadOnly = ReadOnly;
        }

        private void IPLevelIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 379, "IP Levels");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00273_IP_Levels_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00273_IP_Levels_With_Blank);
                }
            }
        }

        private void IPDeferralPeriodIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 380, "IP Deferral Periods");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00274_IP_Deferral_Periods_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00274_IP_Deferral_Periods_With_Blank);
                }
            }
        }

        private void IPSchemeIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 381, "IP Schemes");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00275_IP_Schemes_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp_HR_00275_IP_Schemes_With_Blank);
                }
            }
        }

        private void WorkMobileTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            dxErrorProvider1.SetError((Control)sender, "");
            TextEdit te = (TextEdit)sender;
            string message = "";

            if (!Classes.FormatCheck.mobileIsValid(te.EditValue.ToString(), out message))
            {
                dxErrorProvider1.SetError((Control)sender, message);
            }
        }

        private void personalMobileTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            dxErrorProvider1.SetError((Control)sender, "");
            TextEdit te = (TextEdit)sender;
            string message = "";

            if (!Classes.FormatCheck.mobileIsValid(te.EditValue.ToString(), out message))
            {
                dxErrorProvider1.SetError((Control)sender, message);
                e.Cancel = true;
            }
        }

        private void personalEmailAddressTextEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //

            dxErrorProvider1.SetError((Control)sender, "");
            TextEdit te = (TextEdit)sender;
            string message = "";

            if (!Classes.FormatCheck.emailIsValid(te.EditValue.ToString(), out message))
            {
                dxErrorProvider1.SetError((Control)sender, message);
                e.Cancel = true;
            }
        }

        #endregion


        #region Contract Editors

        private void ContractNumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "Sequence")  // Sequence Button //
            {
                frm_Core_Sequence_Selection fChildForm = new frm_Core_Sequence_Selection();
                fChildForm.PassedInTableName = "HR_Employee";
                fChildForm.PassedInFieldName = "ContractNumber";
                this.ParentForm.AddOwnedForm(fChildForm);
                fChildForm.GlobalSettings = this.GlobalSettings;

                if (fChildForm.ShowDialog() == DialogResult.OK)
                {
                    ContractNumberButtonEdit.EditValue = fChildForm.SelectedSequence;
                    _strLastUsedReferencePrefix2 = fChildForm.SelectedSequence;  // Store Sequence Prefix for saving against Last Saved Record //
                }
            }
            else if (e.Button.Tag.ToString() == "Number")
            {
                Get_Suffix_Number2();  // Get next value in sequence //  
            }
        }
        private void Get_Suffix_Number2()
        {
            string strSequence = ContractNumberButtonEdit.EditValue.ToString() ?? "";

            SequenceNumberGetNext sequence = new SequenceNumberGetNext();
            string strNextNumber = sequence.GetNextNumberInSequence(strConnectionString, "ContractNumber", "HR_Employee", strSequence);  // Calculate Next in Sequence //

            if (strNextNumber == "")
            {
                return;
            }
            if (strSequence.StartsWith("?")) strSequence = strSequence.Remove(0, 2);  // Strip off '?x' when X was the numeric length of sequence to generate [Locality Code as Seed] //

            int intRequiredLength = strNextNumber.Length;
            int intNextNumber = Convert.ToInt32(strNextNumber);
            int intIncrement = 0;
            string strTempNumber = "";


            // Check if value is already present within dataset //
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strCurrentActionID = "";
            if (!string.IsNullOrEmpty(currentRow["ContractNumber"].ToString())) strCurrentActionID = currentRow["ContractNumber"].ToString();

            bool boolUniqueValueFound = false;
            bool boolDuplicateFound = false;
            do
            {
                boolDuplicateFound = false;
                strTempNumber = Convert.ToString(intNextNumber + intIncrement);
                if (strTempNumber.Length > intRequiredLength)  // Abort as the calculated number + sequence will be too big to fit in the field //
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to calculate the next sequence number!\n\nThe length of the sequence plus the calculated sequence number will exceed the length of the field (20 characters).Either reduce the length of the first part of the number or adjust the number of numeric places that the sequence number can span from the Sequence Manager screen.", "Get Next Sequence Number", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                else
                {
                    strTempNumber = strTempNumber.PadLeft(intRequiredLength, '0');
                }
                foreach (DataRow dr in this.dataSet_HR_DataEntry.sp09002_HR_Employee_Item.Rows)
                {
                    if (dr["ContractNumber"].ToString() == (strSequence + strTempNumber))// && dr["intInspectionID"].ToString() != strCurrentInspectionID)
                    {
                        intIncrement++;
                        boolDuplicateFound = true;
                        break;
                    }
                }
                if (!boolDuplicateFound) boolUniqueValueFound = true;  // Exit Loop //
            } while (!boolUniqueValueFound);
            ContractNumberButtonEdit.EditValue = strSequence + strTempNumber;
        }

        private void ContractTypeGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 300, "HR Contract Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00062_Contract_Type_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00062_Contract_Type_List_With_Blank);
                }
            }
        }

        private void JobTitleIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 301, "HR Job Titles");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00124_Job_Titles_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00124_Job_Titles_With_Blank);
                }
            }
        }

        private void GenericJobTitleIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 327, "HR Generic Job Titles");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00125_Generic_Job_Titles_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00125_Generic_Job_Titles_With_Blank);
                }
            }
        }

        private void JobTypeIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 329, "HR Job Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00126_Job_Types_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00126_Job_Types_With_Blank);
                }
            }
        }

        private void ContractBasisIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 307, "HR Contract Basis");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00127_Contract_Basis_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00127_Contract_Basis_List_With_Blank);
                }
            }
        }
        
        private void AppointmentReasonGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 319, "HR Appointment Reasons");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00064_Appointment_Reason_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00064_Appointment_Reason_List_With_Blank);
                }
            }
        }

        private void RecruitmentSourceTypeIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 326, "HR Recruitment Sources");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00111_Recruitment_Sources_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00111_Recruitment_Sources_With_Blank);
                }
            }
        }

        private void ActualHoursSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ActualHoursSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Ratio();
        }
        private void Calculate_Ratio()
        {
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            if (ActualHoursSpinEdit.EditValue == null || FTEHoursSpinEdit.EditValue == null) return;  // These controls were never instantiated, so user never visited them or made changes so ignore following code //
            decimal ActualHours = (!String.IsNullOrEmpty(ActualHoursSpinEdit.EditValue.ToString()) ? Convert.ToDecimal(ActualHoursSpinEdit.EditValue) : (decimal)0.00);
            decimal FTEHours = (!String.IsNullOrEmpty(FTEHoursSpinEdit.EditValue.ToString()) ? Convert.ToDecimal(FTEHoursSpinEdit.EditValue) : (decimal)0.00);
            decimal Ratio = (decimal)0.00;
            if (ActualHours > (decimal)0.00 && FTEHours > (decimal)0.00)
            {
                Ratio = ActualHours / FTEHours;
            }
            currentRow["FTERatio"] = Ratio;
            sp09002HREmployeeItemBindingSource.EndEdit();
            //Calculate_Hours();
        }

        private void FTEHoursSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void FTEHoursSpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Ratio();
        }

        private void IssuedHandbookIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    //Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 319, "HR Appointment Reasons");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00038_Employee_Handbook_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00038_Employee_Handbook_List_With_Blank);
                }
            }
        }

        private void TransferredFromIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 306, "HR Transfer Companies");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00065_Transfer_Companies_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00065_Transfer_Companies_List_With_Blank);
                }
            }
        }

        private void OriginatingCompanyIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 306, "HR Transfer Companies");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00065_Transfer_Companies_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00065_Transfer_Companies_List_With_Blank);
                }
            }
        }

        private void LeavingReasonIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 321, "HR Leaving Reasons");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00070_Leaving_Reason_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00070_Leaving_Reason_List_With_Blank);
                }
            }
        }

        private void ExitInterviewerNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                var currentRow = (DataSet_HR_DataEntry.sp09002_HR_Employee_ItemRow)currentRowView.Row;
                if (currentRow == null) return;
                int intEmployeeID = (string.IsNullOrEmpty(currentRow.ExitInterviewerID.ToString()) ? 0 : Convert.ToInt32(currentRow.ExitInterviewerID));
                var fChildForm = new frm_HR_Select_Employee();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalEmployeeID = intEmployeeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ExitInterviewerID = fChildForm.intSelectedEmployeeID;
                    currentRow.ExitInterviewerName = fChildForm.strSelectedEmployeeName;
                    sp09002HREmployeeItemBindingSource.EndEdit();
                }
            }
        }

        private void BasicLeaveEntitlementFTESpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void BasicLeaveEntitlementFTESpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Hours();
            ibool_ignoreValidation = true;
        }
        private void Calculate_Hours()
        {
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            if (BasicLeaveEntitlementFTESpinEdit.EditValue == null || BankHolidayEntitlementFTESpinEdit.EditValue == null || ShiftFTESpinEdit.EditValue == null) return;  // These controls were never instantiated, so user never visited them or made changes so ignore following code //

            decimal BasicLeaveEntitlementFTE = (!String.IsNullOrEmpty(BasicLeaveEntitlementFTESpinEdit.EditValue.ToString()) ? Convert.ToDecimal(BasicLeaveEntitlementFTESpinEdit.EditValue) : (decimal)0.00);
            decimal BankHolidayEntitlementFTE = (!String.IsNullOrEmpty(BankHolidayEntitlementFTESpinEdit.EditValue.ToString()) ? Convert.ToDecimal(BankHolidayEntitlementFTESpinEdit.EditValue) : (decimal)0.00);
            decimal Ratio = (!String.IsNullOrEmpty(ShiftFTESpinEdit.EditValue.ToString()) ? Convert.ToDecimal(ShiftFTESpinEdit.EditValue) : (decimal)0.00);

            decimal BasicLeaveEntitlement = (decimal)0.00;
            if (BasicLeaveEntitlementFTE > (decimal)0.00 && Ratio > (decimal)0.00)
            {
                BasicLeaveEntitlement = BasicLeaveEntitlementFTE * Ratio;
                BasicLeaveEntitlement = Math.Ceiling(BasicLeaveEntitlement * 2) / 2;  // Round it up to the nearest half day //
            }
            currentRow["BasicLeaveEntitlement"] = BasicLeaveEntitlement;

            decimal BankHolidayEntitlement = (decimal)0.00;
            if (BankHolidayEntitlementFTE > (decimal)0.00 && Ratio > (decimal)0.00)
            {
                BankHolidayEntitlement = BankHolidayEntitlementFTE * Ratio;
                BankHolidayEntitlement = Math.Ceiling(BankHolidayEntitlement * 2) / 2;  // Round it up to the nearest half day //
            }
            currentRow["BankHolidayEntitlement"] = BankHolidayEntitlement;
            sp09002HREmployeeItemBindingSource.EndEdit();
        }
        
        private void BankHolidayEntitlementFTESpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void BankHolidayEntitlementFTESpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Hours();
            ibool_ignoreValidation = true;
        }

        private void HolidayUnitDescriptorIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
        }
        private void HolidayUnitDescriptorIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(HolidayUnitDescriptorIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(HolidayUnitDescriptorIDGridLookUpEdit, "");
            }
        }
        private void HolidayUnitDescriptorIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            Set_Editor_Masks();
        }

        private void ShiftFTESpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ShiftFTESpinEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            Calculate_Hours();
            ibool_ignoreValidation = true;

        }
 

        private void LastIncreaseAmountSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strValue = LastIncreaseAmountSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["LastIncreaseAmount"].ToString() != strValue) currentRow["LastIncreaseAmount"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Last Increase Amount data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void LastIncreasePercentageSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strValue = LastIncreasePercentageSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["LastIncreasePercentage"].ToString() != strValue) currentRow["LastIncreasePercentage"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Last Increase Percentage data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void SalaryBandingIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 9907, "HR Master Salary Bandings");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00066_Salary_Banding_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00066_Salary_Banding_List_With_Blank);
                }
            }

        }

        private void PayrollTypeIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 312, "HR Payroll Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00069_Payroll_Type_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00069_Payroll_Type_List_With_Blank);
                }
            }
        }

        private void PaymentTypeIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 320, "HR Payroll Payment Types");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00067_Payment_Type_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00067_Payment_Type_List_With_Blank);
                }
            }
        }

        private void SalarySpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strValue = SalarySpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["Salary"].ToString() != strValue) currentRow["Salary"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Salary data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void PayrollReferenceIDGridLookUpEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 330, "HR Payroll References");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00128_Payroll_Reference_List_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00128_Payroll_Reference_List_With_Blank);
                }
            }
        }

        private void PaymentAmountSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strValue = PaymentAmountSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["PaymentAmount"].ToString() != strValue) currentRow["PaymentAmount"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Payment Amount data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void SickPayUnitDescriptorIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
        }
        private void SickPayUnitDescriptorIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            /*if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(SickPayUnitDescriptorIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SickPayUnitDescriptorIDGridLookUpEdit, "");
            }*/
        }
        private void SickPayUnitDescriptorIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            Set_Editor_Mask_SickPay1();
        }

        private void SickPayUnitDescriptorID2GridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
        }
        private void SickPayUnitDescriptorID2GridLookUpEdit_Validated(object sender, EventArgs e)
        {
            Set_Editor_Mask_SickPay2();
        }

        private void MedicalCoverEmployerContributionSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strValue = MedicalCoverEmployerContributionSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["MedicalCoverEmployerContribution"].ToString() != strValue) currentRow["MedicalCoverEmployerContribution"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Medical Cover Employer Contribution data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void CTWSchemeValueSpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strValue = CTWSchemeValueSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["CTWSchemeValue"].ToString() != strValue) currentRow["CTWSchemeValue"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Cycle To Work Scheme Value data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void PreviousEmploymentSalarySpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strValue = PreviousEmploymentSalarySpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["PreviousEmploymentSalary"].ToString() != strValue) currentRow["PreviousEmploymentSalary"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Previous Employment Salary data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void SalaryExpectationpinEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            // Encrypt Data //
            if (string.IsNullOrEmpty(GlobalSettings.HRSecurityKey)) return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strValue = SalaryExpectationSpinEdit.EditValue.ToString();
            if (!string.IsNullOrEmpty(strValue))
            {
                try
                {
                    SimplerAES Encryptor = new SimplerAES();
                    strValue = Encryptor.Encrypt(strValue);
                    if (currentRow["SalaryExpectation"].ToString() != strValue) currentRow["SalaryExpectation"] = strValue;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to encrypt the Salary Expectation data: " + ex.Message + "\n\nPlease change the data then tab off the field to try again.", "Encrypt Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }

        private void MedicalCoverProviderIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 342, "HR Medical Cover Providers");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00220_Medical_Cover_Providers_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00220_Medical_Cover_Providers_With_Blank);
                }
            }
        }

        private void MedicalCoverEmployerContributionFrequencyIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 343, "HR Medical Cover Pay Frequency Descriptors");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00221_Medical_Cover_Payment_Frequency_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00221_Medical_Cover_Payment_Frequency_With_Blank);
                }
            }
        }

        private void CTWSchemeEndOptionIDGridLookUpEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)
            {
                if ("edit".Equals(e.Button.Tag))
                {
                    Editor_Picklist_Edit.Edit_picklist(this, this.GlobalSettings, 344, "HR CTW Scheme End Options");
                }
                else if ("reload".Equals(e.Button.Tag))
                {
                    sp_HR_00222_CTW_Scheme_End_Options_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00222_CTW_Scheme_End_Options_With_Blank);
                }
            }
        }

        private void MedicalCoverEmployerContributionDescriptorIDGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
        }
        private void MedicalCoverEmployerContributionDescriptorIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            Set_Editor_Masks3();
        }

        private void EligibleForMedicalCoverCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void EligibleForMedicalCoverCheckEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //

            CheckEdit ce = (CheckEdit)sender;
            if (!ce.Checked)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                currentRow["MedicalCoverProviderID"] = 0;
                currentRow["MedicalCoverEmployerContributionDescriptorID"] = 0;
                currentRow["MedicalCoverEmployerContributionFrequencyID"] = 0;

                SimplerAES Encryptor = new SimplerAES();
                string strAmount = Encryptor.Encrypt("0.00");
                currentRow["MedicalCoverEmployerContribution"] = strAmount;
                MedicalCoverEmployerContributionSpinEdit.EditValue = "0.00";

                sp09002HREmployeeItemBindingSource.EndEdit();
            }
            ibool_ignoreValidation = true;
            SetMenuStatus();
        }

        private void CTWSchemeCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void CTWSchemeCheckEdit_Validated(object sender, EventArgs e)
        {
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //

            CheckEdit ce = (CheckEdit)sender;
            if (!ce.Checked)
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;
                CTWSchemeStartDateDateEdit.EditValue = null;
                CTWSchemeEndDateDateEdit.EditValue = null;

                currentRow["CTWSchemeEndOptionID"] = 0;

                SimplerAES Encryptor = new SimplerAES();
                string strAmount = Encryptor.Encrypt("0.00");
                currentRow["CTWSchemeValue"] = strAmount;
                CTWSchemeValueSpinEdit.EditValue = "0.00";

                sp09002HREmployeeItemBindingSource.EndEdit();
            }
            ibool_ignoreValidation = true;
            SetMenuStatus();
        }

        private void ElegibleForSharesCheckEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void ElegibleForSharesCheckEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "blockadd" || strFormMode == "blockedit") return;
            if (ibool_ignoreValidation) return;  // Toggles validation on and off for Calculation //
            ibool_ignoreValidation = true;
            SetMenuStatus();
        }

        #endregion


        private void Set_Editor_Masks()
        {
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            //string strMaskDescription = "####0.00 " + HolidayUnitDescriptorIDGridLookUpEdit.Text ?? "";
            string strDescriptorID = currentRow["HolidayUnitDescriptorID"].ToString() ?? "";
            string strDescriptor = "??";
            switch (strDescriptorID)
            {
                case "1":
                    strDescriptor = "Days";
                    break;
                case "2":
                    strDescriptor = "Hours";
                    break;
                default:
                    strDescriptor = "??";
                    break;
            }
            string strMaskDescription = "####0.00 " + strDescriptor;

            BasicLeaveEntitlementFTESpinEdit.Properties.Mask.EditMask = strMaskDescription;
            BasicLeaveEntitlementSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            BankHolidayEntitlementFTESpinEdit.Properties.Mask.EditMask = strMaskDescription;
            BankHolidayEntitlementSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            YearlyLeaveIncrementAmountSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            MaximumLeaveEntitlementSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            HolidaysAccruedSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            HolidaysTakenSpinEdit.Properties.Mask.EditMask = strMaskDescription;
            HolidaysBalanceSpinEdit.Properties.Mask.EditMask = strMaskDescription;
        }

        private void Set_Editor_Mask_SickPay1()
        {
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strDescriptorID = currentRow["SickPayUnitDescriptorID"].ToString() ?? "";
            string strDescriptor = "??";
            switch (strDescriptorID)
            {
                case "1":
                    strDescriptor = "Days";
                    break;
                case "2":
                    strDescriptor = "Weeks";
                    break;
                case "3":
                    strDescriptor = "Months";
                    break;
                case "4":
                    strDescriptor = "Years";
                    break;
                default:
                    strDescriptor = "??";
                    break;
            }
            string strMaskDescription = "####0.00 " + strDescriptor;
            SickPayEntitlementUnitsSpinEdit.Properties.Mask.EditMask = strMaskDescription;
        }
        
        private void Set_Editor_Mask_SickPay2()
        {
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strDescriptorID = currentRow["SickPayUnitDescriptorID2"].ToString() ?? "";
            string strDescriptor = "??";
            switch (strDescriptorID)
            {
                case "1":
                    strDescriptor = "Days";
                    break;
                case "2":
                    strDescriptor = "Weeks";
                    break;
                case "3":
                    strDescriptor = "Months";
                    break;
                case "4":
                    strDescriptor = "Years";
                    break;
                default:
                    strDescriptor = "??";
                    break;
            }
            string strMaskDescription = "####0.00 " + strDescriptor;
            SickPayUnits2SpinEdit.Properties.Mask.EditMask = strMaskDescription;
        }

        private void Set_Editor_Masks3()
        {
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            string strDescriptorID = currentRow["MedicalCoverEmployerContributionDescriptorID"].ToString() ?? "";
            string strMaskDescription = "";
            switch (strDescriptorID)
            {
                case "1":
                    strMaskDescription = "c";
                    break;
                case "2":
                    strMaskDescription = "P";
                    break;
                default:
                    strMaskDescription = "####0.00";
                    break;
            }
            MedicalCoverEmployerContributionSpinEdit.Properties.Mask.EditMask = strMaskDescription;
        }

        private void Set_Editor_Back_Colours()
        {
            if (this.strFormMode == "blockedit") return;
            if (ibool_FormStillLoading) return;  // Don't fire if this has been activated by Form's ValidateChildren (Called by ConfigureFormAccordingToMode) //
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;

            /*DevExpress.Skins.Skin currentSkin;
            DevExpress.Skins.SkinElement element;
            currentSkin = DevExpress.Skins.CommonSkins.GetSkin(dataLayoutControl1.LookAndFeel);
            Color foreColor =  currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.WindowText);
            Color backColor = currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.Window);
            Color disabledForeColor = currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.DisabledText);
            Color disabledBackColor =  currentSkin.Colors.GetColor(DevExpress.Skins.CommonColors.DisabledControl);*/

            Color foreColorGood = Color.Black;
            Color backColorGood = Color.PaleGreen; ;
            Color foreColorBad = Color.Black;
            Color backColorBad = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);

            /*
            HolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(HolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            HolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(HolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
            PurchasedHolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(PurchasedHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            PurchasedHolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(PurchasedHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
            BankHolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(BankHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            BankHolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(BankHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
            TotalHolidaysRemainingTextEdit.Properties.Appearance.BackColor = (Convert.ToDecimal(TotalHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? backColorBad : backColorGood);
            TotalHolidaysRemainingTextEdit.Properties.Appearance.ForeColor = (Convert.ToDecimal(TotalHolidaysRemainingTextEdit.EditValue) <= (decimal)0.00 ? foreColorBad : foreColorGood);
        */
        }

        private void Decrypt_Financial_Fields()
        {
            if (!string.IsNullOrEmpty(GlobalSettings.HRSecurityKey))  // Decrypt Employee Payament Info //
            {
                DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                if (currentRow == null) return;

                SimplerAES Encryptor = new SimplerAES();
                try
                {
                    string strValue = currentRow["LastIncreaseAmount"].ToString();
                    LastIncreaseAmountSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");
                    
                    strValue = currentRow["LastIncreasePercentage"].ToString();
                    LastIncreasePercentageSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");
                    
                    strValue = currentRow["Salary"].ToString();
                    SalarySpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");

                    strValue = currentRow["PaymentAmount"].ToString();
                    PaymentAmountSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");

                    strValue = currentRow["MedicalCoverEmployerContribution"].ToString();
                    MedicalCoverEmployerContributionSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");

                    strValue = currentRow["CTWSchemeValue"].ToString();
                    CTWSchemeValueSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");

                    strValue = currentRow["PreviousEmploymentSalary"].ToString();
                    PreviousEmploymentSalarySpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");

                    strValue = currentRow["SalaryExpectation"].ToString();
                    SalaryExpectationSpinEdit.EditValue = (!string.IsNullOrEmpty(strValue) ? Encryptor.Decrypt(strValue) : "0.00");
                }
                catch (Exception ex)
                {
                    LastIncreaseAmountSpinEdit.EditValue = "0.00";
                    LastIncreasePercentageSpinEdit.EditValue = "0.00";
                    SalarySpinEdit.EditValue = "0.00";
                    PaymentAmountSpinEdit.EditValue = "0.00";
                    MedicalCoverEmployerContributionSpinEdit.EditValue = "0.00";
                    CTWSchemeValueSpinEdit.EditValue = "0.00";
                    PreviousEmploymentSalarySpinEdit.EditValue = "0.00";
                    SalaryExpectationSpinEdit.EditValue = "0.00";
                }
            }
            else  // Don't display any employee payment info //
            {
                LastIncreaseAmountSpinEdit.EditValue = "0.00";
                LastIncreasePercentageSpinEdit.EditValue = "0.00";
                SalarySpinEdit.EditValue = "0.00";
                PaymentAmountSpinEdit.EditValue = "0.00";
                MedicalCoverEmployerContributionSpinEdit.EditValue = "0.00";
                CTWSchemeValueSpinEdit.EditValue = "0.00";
                PreviousEmploymentSalarySpinEdit.EditValue = "0.00";
                SalaryExpectationSpinEdit.EditValue = "0.00";
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewAddresses":
                    message = "No Addresses - Click Add button to add linked Addresses";
                    break;
                case "gridViewNextOfKin":
                    message = "No Next of Kin - Click Add button to add linked Next of Kin";
                    break;
                case "gridControlTraining":
                    message = "No Training - Click Add button to add linked Training";
                    break;
                case "gridViewDiscipline":
                    message = "No Discipline \\ Sanctions - Click Add button to add linked Discipline \\ Sanctions";
                    break;
                case "gridViewVetting":
                    message = "No Vetting - Click Add button to add linked Vetting";
                    break;
                case "gridViewDeptWorked":
                    message = "No Departments Worked - Click Add button to add linked Departments Worked";
                    break;
                case "gridViewWorkingPatternHeader":
                    message = "No Working Pattern Header - Click Add button to add linked Working Pattern Header";
                    break;
                case "gridViewWorkingPattern":
                    message = "No Working Pattern - Click Add button to add linked Working Pattern";
                    break;
                case "gridViewHolidayYear":
                    message = "No Holiday Years - Select one or more Employees to see linked Holiday Years";
                    break;
                case "gridViewAbsences":
                    message = "No Absences - Select one or more Employee Holiday Years to see linked Absences";
                    break;
                case "gridViewBonuses":
                    message = "No Bonuses - Click Add button to add linked Bonuses";
                    break;
                case "gridViewPension":
                    message = "No Pensions - Click Add button to add linked Pensions";
                    break;
                case "gridViewCRM":
                    message = "No CRM - Select one or more Employees to see linked CRM";
                    break;
                case "gridViewReferences":
                    message = "No References - Select one or more Employees to see linked References";
                    break;
                case "gridViewPayRise":
                    message = "No Pay Rises - Click Add button to add linked Pay Rises";
                    break;
                case "gridViewShares":
                    message = "No Shares - Select one or more Employees to see linked Shares";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewWorkingPatternHeader":
                    LoadLinked_WorkingPattern();
                    view = (GridView)gridControlWorkingPattern.MainView;
                    view.ExpandAllGroups();
                    break;
                case "gridViewHolidayYear":
                    LoadLinked_Absences();
                    view = (GridView)gridControlAbsences.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView Addresses

        private void gridControlAddresses_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_Addresses();
                    }
                    else if ("copy".Equals(e.Button.Tag))
                    {
                        string strCopiedText = "";
                        string strName = "";
                        GridView view = (GridView)gridControlAddresses.MainView;
                        int[] intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length != 1)
                        {
                            XtraMessageBox.Show("Select just one address record to copy to the Clipbopard before proceeding.", "Copy Address to Clipboard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strName = (string.IsNullOrWhiteSpace(TitleTextEdit.Text.ToString()) ? "" : TitleTextEdit.Text.ToString() + " ") + (string.IsNullOrWhiteSpace(FirstnameTextEdit.Text.ToString()) ? "" : FirstnameTextEdit.Text.ToString() + " ") + (string.IsNullOrWhiteSpace(SurnameTextEdit.Text.ToString()) ? "" : SurnameTextEdit.Text.ToString());
                            strCopiedText += (string.IsNullOrWhiteSpace(strName) ? "" : strName + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine1").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine1").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine2").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine2").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine3").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine3").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine4").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine4").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "AddressLine5").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "AddressLine5").ToString() + "\n");
                            strCopiedText += (string.IsNullOrWhiteSpace(view.GetRowCellValue(intRowHandle, "PostCode").ToString()) ? "" : view.GetRowCellValue(intRowHandle, "PostCode").ToString() + "\n");
                        }
                        if (!string.IsNullOrWhiteSpace(strCopiedText)) strCopiedText.TrimEnd('\n');
                        if (string.IsNullOrWhiteSpace(strCopiedText))
                        {
                            XtraMessageBox.Show("The current address record has no text to copy to the Clipboard.", "Copy Address to Clipboard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        Clipboard.SetText(strCopiedText);
                        XtraMessageBox.Show("Address copied to the Clipboard.\n\nUse Ctrl + V or right click and select Paste to copy the address into applications such as Microsoft Word and Excel.", "Copy Address to Clipboard", MessageBoxButtons.OK, MessageBoxIcon.Information);
                  }
                    break;
                default:
                    break;
            }
        }

        private void gridControlAddresses_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Addresses;
            SetMenuStatus();
        }

        private void gridControlAddresses_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Addresses;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControlAddresses_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }


        #endregion


        #region GridView Next Of Kin

        private void gridControlNextOfKin_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                     else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_NextOfKin();
                    }
                   break;
                default:
                    break;
            }
        }

        private void gridViewNextOfKin_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.NextOfKin;
            SetMenuStatus();
        }

        private void gridViewNextOfKin_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.NextOfKin;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewNextOfKin_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        #endregion


        #region GridView Training

        private void gridControlTraining_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_Training();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlTraining.MainView;
                        int intRecordType = 4;  // Training //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControlTraining_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Training;
            SetMenuStatus();
        }

        private void gridControlTraining_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Training;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridControlTraining_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewTraining_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DaysUntilExpiry")
            {
                int intDaysToDo = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysUntilExpiry"));
                if (intDaysToDo < 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysToDo < 30)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewTraining_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DaysUntilExpiry":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysUntilExpiry")) == 99999) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewTraining_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "CertificatePath":
                    if (string.IsNullOrEmpty(view.GetFocusedRowCellValue("CertificatePath").ToString())) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsTraining_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 4;  // Training //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Discipline

        private void gridControlDiscipline_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_Discipline();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlDiscipline.MainView;
                        int intRecordType = 3;  // Sanctions //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SanctionID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateRaised")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                     break;
                default:
                    break;
            }
        }

        private void gridViewDiscipline_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Discipline;
            SetMenuStatus();
        }

        private void gridViewDiscipline_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Discipline;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewDiscipline_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewDiscipline_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StatusValue")
            {
                string strStatus = view.GetRowCellValue(e.RowHandle, "StatusValue").ToString();
                if (strStatus.ToLower() == "open")
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewDiscipline_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }
        
        private void gridViewDiscipline_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentSanctions_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 3;  // Sanctions //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SanctionID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateRaised")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Vetting

        private void gridControlVetting_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_Vetting();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewVetting;
                        int intRecordType = 9;  // Vetting //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VettingID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "VettingType").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewVetting_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Vetting;
            SetMenuStatus();
        }

        private void gridViewVetting_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Vetting;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewVetting_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewVetting_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "ExpiryDate")
            {
                int intExpiredStatus = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ExpiredStatus"));
                if (intExpiredStatus >= 1)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewVetting_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "RestrictionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RestrictionCount")) <= 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewVetting_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsVetting_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 9;  // Vetting //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "VettingID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "VettingType").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Dept Worked

        private void gridControlDeptWorked_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_DeptWorked();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlDeptWorked.MainView;
                        int intRecordType = 7;  // Dept Worked //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "EmployeeDeptWorkedID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeSurnameForename").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "DepartmentName").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewDeptWorked_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.DepartmentsWorked;
            SetMenuStatus();
        }

        private void gridViewDeptWorked_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.DepartmentsWorked;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewDeptWorked_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewDeptWorked_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewDeptWorked_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsDeptWorked_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 7;  // Dept Worked //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "EmployeeDeptWorkedID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeSurnameForename").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "DepartmentName").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Working Pattern Header

        private void gridControlWorkingPatternHeader_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_WorkingPatternHeader();
                    }
                    else if ("add master".Equals(e.Button.Tag))
                    {
                        if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                        DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                        if (currentRow == null) return;
                        int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
                        if (intEmployeeID <= 0)
                        {
                            XtraMessageBox.Show("Please Save the Employee record before attempting to add linked records.", "Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        string strEmployeeName = (string.IsNullOrEmpty(currentRow["Surname"].ToString()) ? "" : currentRow["Surname"].ToString()) + ": " + (string.IsNullOrEmpty(currentRow["Firstname"].ToString()) ? "" : currentRow["Firstname"].ToString());

                        var fChildForm = new frm_HR_Employee_Working_Pattern_Add_From_Master();
                        this.ParentForm.AddOwnedForm(fChildForm);
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm._PassedInEmployeeIDs = intEmployeeID.ToString();
                        fChildForm._PassedInEmployeeNames = strEmployeeName;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                        {
                            if (!string.IsNullOrWhiteSpace(fChildForm._NewWorkingPatternHeaderIDs))
                            {
                                i_str_AddedWorkingPatternHeaderRecordIDs = fChildForm._NewWorkingPatternHeaderIDs;
                                LoadLinked_WorkingPatternHeader();
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewWorkingPatternHeader_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.WorkPatternHeader;
            SetMenuStatus();
        }

        private void gridViewWorkingPatternHeader_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.WorkPatternHeader;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewWorkingPatternHeader_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewWorkingPatternHeader_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        #endregion


        #region GridView Working Pattern

        private void gridControlWorkingPattern_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_WorkingPattern();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewWorkingPattern_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.WorkPattern;
            SetMenuStatus();
        }

        private void gridViewWorkingPattern_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.WorkPattern;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewWorkingPattern_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        #endregion


        #region GridView Holiday Year

        private void gridControlHolidayYear_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_HolidayYears();
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridViewHolidayYear_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewHolidayYear_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.HolidayYear;
            SetMenuStatus();
        }

        private void gridViewHolidayYear_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.HolidayYear;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewHolidayYear_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "TotalHolidaysRemaining":
                    if (Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, e.Column.FieldName)) <= (decimal)0.00)
                    {
                        e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                        e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                        e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewHolidayYear_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DefaultBankHolidayEntitlement":
                case "DefaultBasicLeaveEntitlement":
                case "DefaultYearlyLeaveIncrement":
                case "HolidaysTaken":
                case "BankHolidaysTaken":
                case "AbsenceAuthorisedTaken":
                case "AbsenceUnauthorisedTaken":
                case "BasicHolidayEntitlement":
                case "BankHolidayEntitlement":
                case "PurchasedHolidayEntitlement":
                case "HoursPerHolidayDay":
                case "TotalHolidayEntitlement":
                case "HolidaysRemaining":
                case "BankHolidaysRemaining":
                case "TotalHolidaysRemaining":
                    if (view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptor").ToString() == "Days")
                    {
                        e.RepositoryItem = repositoryItemTextEditNumericDays;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "HolidayUnitDescriptor").ToString() == "Hours")
                    {
                        e.RepositoryItem = repositoryItemTextEditNumericHours;
                    }
                    else
                    {
                        e.RepositoryItem = repositoryItemTextEditNumericUnknown;
                    }
                    break;
                default:
                    break;
            }
        }


        #endregion


        #region GridView Absences

        private void gridControlAbsences_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_Absences();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlAbsences.MainView;
                        int intRecordType = 6;  // Absences //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "AbsenceID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeSurnameForename").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date" + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                   break;
                default:
                    break;
            }

        }

        private void gridViewAbsences_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewAbsences_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Absences;
            SetMenuStatus();
        }

        private void gridViewAbsences_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Absences;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewAbsences_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "Status")
            {
                try
                {
                    switch (view.GetRowCellValue(e.RowHandle, "Status").ToString())
                    {
                        case "Pending":
                            e.Appearance.BackColor = Color.Khaki;
                            e.Appearance.BackColor2 = Color.DarkOrange;
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                            break;
                        case "Approved":
                            e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                            e.Appearance.BackColor2 = Color.PaleGreen;
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                            break;
                        case "Rejected":
                            e.Appearance.BackColor = Color.LightSteelBlue;
                            e.Appearance.BackColor2 = Color.CornflowerBlue;
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                            break;
                        case "Cancelled":
                            e.Appearance.BackColor = Color.Gainsboro;
                            e.Appearance.BackColor2 = Color.DarkGray;
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                            break;
                        case "Unauthorised":
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                            break;
                        default:
                            break;
                    }
                }
                catch (Exception) { }
            }
        }

        private void gridViewAbsences_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DeclaredDisability":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RecordTypeId")) == 1)
                    {
                        try
                        {
                            e.RepositoryItem = emptyEditor;
                        }
                        catch (Exception) { }
                    }
                    break;
                case "IsWorkRelated":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "RecordTypeId")) == 1)
                    {
                        try
                        {
                            e.RepositoryItem = emptyEditor;
                        }
                        catch (Exception) { }
                    }
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0)
                    {
                        try
                        {
                            e.RepositoryItem = emptyEditor;
                        }
                        catch (Exception) { }
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewAbsences_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "DeclaredDisability":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RecordTypeId")) == 1) e.Cancel = true;
                    break;
                case "IsWorkRelated":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("RecordTypeId")) == 1) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsAbsence_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 6;  // Absence //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "AbsenceID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeSurnameForename").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date" + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }


        #endregion


        #region GridView Bonuses

        private void gridControlBonuses_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_Bonuses();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlBonuses.MainView;
                        int intRecordType = 10;  // Bonuses //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BonusID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "BonusDueDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewBonuses_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewBonuses_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Bonus;
            SetMenuStatus();
        }

        private void gridViewBonuses_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Bonus;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewBonuses_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewBonuses_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewBonuses_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsBonus_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 10;  // Bonus //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BonusID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "BonusDueDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Pensions

        private void gridControlPension_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_Pensions();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlPension.MainView;
                        int intRecordType = 2;  // Pension //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PensionID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "SchemeNumber").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewPension_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewPension_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Pensions;
            SetMenuStatus();
        }

        private void gridViewPension_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Pensions;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewPension_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewPension_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "ContributionCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "ContributionCount")) <= 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewPension_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "ContributionCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("ContributionCount")) == 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentPension_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 2;  // Pension //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PensionID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "SchemeNumber").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView CRM

        private void gridControlCRM_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_CRM();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewCRM;
                        int intRecordType = 5;  // CRM //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CRMID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "Description").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridViewCRM_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewCRM_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.CRM;
            SetMenuStatus();
        }

        private void gridViewCRM_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.CRM;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewCRM_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "DaysRemaining")
            {
                int intDaysRemaining = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysRemaining"));
                if (intDaysRemaining > 7)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xB9, 0xFB, 0xB9);
                    e.Appearance.BackColor2 = Color.PaleGreen;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysRemaining > 0 && intDaysRemaining <= 7)
                {
                    e.Appearance.BackColor = Color.Khaki;
                    e.Appearance.BackColor2 = Color.DarkOrange;
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
                else if (intDaysRemaining < 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewCRM_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DaysRemaining":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DaysRemaining")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewCRM_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "DaysRemaining":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("DaysRemaining")) == 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsCRM_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 5;  // CRM //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "CRMID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "Description").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView References

        private void gridControlReferences_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_References();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewReference;
                        int intRecordType = 8;  // Reference //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ReferenceID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "SuppliedByName");
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }

        }

        private void gridViewReferences_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewReferences_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Reference;
            SetMenuStatus();
        }

        private void gridViewReferences_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Reference;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewReferences_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        private void gridViewReferences_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewReferences_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentReferences_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 8;  // References //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ReferenceID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "SuppliedByName");
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }


        #endregion


        #region GridView Pay Rises

        private void gridControlPayRise_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_PayRises();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlPayRise.MainView;
                        int intRecordType = 11;  // Pay Rises //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PayRiseID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "PayRiseDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewPayRise_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewPayRise_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.PayRise;
            SetMenuStatus();
        }

        private void gridViewPayRise_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.PayRise;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewPayRise_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewPayRise_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewPayRise_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }


        private void repositoryItemHyperLinkEditPayRiseLinkedDocuments_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 11;  // Pay Rise //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "PayRiseID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "PayRiseDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region GridView Shares

        private void gridControlShares_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinked_Shares();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlShares.MainView;
                        int intRecordType = 12;  // Shares //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SharesID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateReceived")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewShares_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void gridViewShares_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmMainGrids.Shares;
            SetMenuStatus();
        }

        private void gridViewShares_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmMainGrids.Shares;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewShares_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewShares_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "ShareUnits":
                    if (view.GetRowCellValue(e.RowHandle, "ShareUnitDescriptor").ToString() == "�")
                    {
                        e.RepositoryItem = repositoryItemTextEditShareUnitsCurrency;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "ShareUnitDescriptor").ToString() == "%")
                    {
                        e.RepositoryItem = repositoryItemTextEditShareUnitsPercentage;
                    }
                    else if (view.GetRowCellValue(e.RowHandle, "ShareUnitDescriptor").ToString() == "Units")
                    {
                        e.RepositoryItem = repositoryItemTextEditShareUnitsUnits;
                    }
                    else
                    {
                        e.RepositoryItem = repositoryItemTextEditShareUnitsUnknown;
                    }
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewShares_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditSharesLinkedDocuments_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 12;  // Shares //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SharesID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "DateReceived")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        #endregion


        #region Absence Type Filter Panel

        private void btnAbsenceTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditAbsenceType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_select_absence_type_ids = "";  // Reset any prior values first //
            i_str_select_absence_types = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_select_absence_type_ids = "";
                return "No Absence Type Filter";

            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_select_absence_type_ids = "";
                return "No Absence Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_select_absence_type_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_select_absence_types = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_select_absence_types += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_select_absence_types;
        }

        private void btnRefreshHolidays_Click(object sender, EventArgs e)
        {
            LoadLinked_HolidayYears();
        }

        #endregion


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            int intEmployeeID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
            if (intEmployeeID <= 0)
            {
                XtraMessageBox.Show("Please Save the Employee record before attempting to add linked records.", "Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strEmployeeNumber = (string.IsNullOrEmpty(currentRow["EmployeeNumber"].ToString()) ? "" : currentRow["EmployeeNumber"].ToString());
            string strFirstname = (string.IsNullOrEmpty(currentRow["Firstname"].ToString()) ? "" : currentRow["Firstname"].ToString());
            string strSurname = (string.IsNullOrEmpty(currentRow["Surname"].ToString()) ? "" : currentRow["Surname"].ToString());
            
            GridView view = null;
            MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Addresses:
                    {
                        view = (GridView)gridControlAddresses.MainView;
                        view.PostEditor();
                        RefreshGridViewStateAddress.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Address_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;
                        
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        view = (GridView)gridControlNextOfKin.MainView;
                        view.PostEditor();
                        RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Next_Kin_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        RefreshGridViewStateTraining.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.intRecordTypeID = 0; // Staff //

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        view = (GridView)gridControlDiscipline.MainView;
                        view.PostEditor();
                        RefreshGridViewStateDiscipline.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Sanction_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    {
                        view = (GridView)gridControlVetting.MainView;
                        view.PostEditor();
                        RefreshGridViewStateVetting.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Vetting_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        view = (GridView)gridControlDeptWorked.MainView;
                        view.PostEditor();
                        RefreshGridViewStateDeptWorked.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Department_Worked_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.ParentRecordId = intEmployeeID;
                        fChildForm.ParentRecordDescription = String.Format("{0}: {1}", strSurname, strFirstname);

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        view.PostEditor();
                        RefreshGridViewStateWorkingPatternHeader.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Working_Pattern_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        view = (GridView)gridControlWorkingPattern.MainView;
                        view.PostEditor();
                        RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        
                        GridView ParentView = (GridView)gridControlWorkingPatternHeader.MainView;
                        int[] intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_HR_Core.sp_HR_00154_Employee_Working_Pattern_HeadersRow)rowView.Row;
                            fChildForm.ParentRecordId = row.WorkingPatternHeaderID;
                            fChildForm.ParentRecordDescription = row.Description;
                            fChildForm.ParentEmployeeID = row.EmployeeID;
                            fChildForm.ParentRecordEmployeeName = row.EmployeeName;
                            fChildForm.ParentRecordStartDate = row.StartDate;
                            fChildForm.ParentRecordEndDate = row.EndDate;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    {
                        view = (GridView)gridControlHolidayYear.MainView;
                        view.PostEditor();
                        RefreshGridViewStateHolidayYear.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Holiday_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.ParentRecordId = intEmployeeID;
                        fChildForm.ParentRecordDescription = String.Format("{0}: {1}", strSurname, strFirstname); ;
                        fChildForm.ParentRecordHolidays = (string.IsNullOrEmpty(currentRow["BasicLeaveEntitlement"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["BasicLeaveEntitlement"])); ;
                        fChildForm.ParentRecordBankHolidays = (string.IsNullOrEmpty(currentRow["BankHolidayEntitlement"].ToString()) ? (decimal)0.00 : Convert.ToDecimal(currentRow["BankHolidayEntitlement"])); ;
                        fChildForm.ParentHolidayUnitDescriptorId = (string.IsNullOrEmpty(currentRow["HolidayUnitDescriptorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["HolidayUnitDescriptorID"])); ;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Absences:
                    {
                        view = (GridView)gridControlAbsences.MainView;
                        view.PostEditor();
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Absence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;

                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        GridView ParentView = (GridView)gridControlHolidayYear.MainView;
                        int[] intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            fChildForm.ParentRecordId = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeHolidayYearID"));
                            fChildForm.ParentRecordDescription = ParentView.GetRowCellValue(intRowHandles[0], "EmployeeSurnameForename").ToString() + "  -  Holiday Year: " + ParentView.GetRowCellValue(intRowHandles[0], "HolidayYearDescription").ToString();
                            fChildForm.ParentEmployeeID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "EmployeeID"));
                        }
                        else
                        {
                            fChildForm.ParentRecordId = 0;
                            fChildForm.ParentRecordDescription = "";
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Bonus_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pension_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        view = (GridView)gridControlReference.MainView;
                        view.PostEditor();
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Reference_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        view = (GridView)gridControlPayRise.MainView;
                        view.PostEditor();
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pay_Rise_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        view = (GridView)gridControlShares.MainView;
                        view.PostEditor();
                        RefreshGridViewStateShares.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Shares_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        fChildForm.intLinkedToRecordID = intEmployeeID;
                        fChildForm.strLinkedToRecordDesc = String.Format("{0}: {1}", strSurname, strFirstname);
                        fChildForm.strLinkedToRecordDesc2 = strSurname;
                        fChildForm.strLinkedToRecordDesc3 = strFirstname;
                        fChildForm.strLinkedToRecordDesc4 = strEmployeeNumber;

                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Addresses:
                    {
                        view = (GridView)gridControlAddresses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AddressId")) + ',';
                        }
                        RefreshGridViewStateAddress.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Address_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        view = (GridView)gridControlNextOfKin.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }
                        RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store Grid View State //
                        frm_HR_Employee_Next_Kin_Edit fChildForm = new frm_HR_Employee_Next_Kin_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }
                        RefreshGridViewStateTraining.SaveViewInfo();  // Store Grid View State //
                        frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm.intRecordTypeID = 1;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        view = (GridView)gridControlDiscipline.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SanctionID")) + ',';
                        }
                        RefreshGridViewStateDiscipline.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Sanction_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    {
                        view = (GridView)gridControlVetting.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VettingID")) + ',';
                        }
                        RefreshGridViewStateVetting.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Vetting_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        view = (GridView)gridControlDeptWorked.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeDeptWorkedID")) + ',';
                        }
                        RefreshGridViewStateDeptWorked.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Department_Worked_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternHeaderID")) + ',';
                        }
                        RefreshGridViewStateWorkingPatternHeader.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Working_Pattern_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        view = (GridView)gridControlWorkingPattern.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternID")) + ',';
                        }
                        RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    {
                        view = (GridView)gridControlHolidayYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeHolidayYearID")) + ',';
                        }
                        RefreshGridViewStateHolidayYear.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Holiday_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Absences:
                    {
                        view = (GridView)gridControlAbsences.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AbsenceID")) + ',';
                        }
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Absence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BonusID")) + ',';
                        }
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Bonus_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionID")) + ',';
                        }
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pension_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        view = (GridView)gridControlReference.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ReferenceID")) + ',';
                        }
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Reference_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        view = (GridView)gridControlPayRise.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PayRiseID")) + ',';
                        }
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pay_Rise_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        view = (GridView)gridControlShares.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SharesID")) + ',';
                        }
                        RefreshGridViewStateShares.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Shares_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Addresses:
                    {
                        view = (GridView)gridControlAddresses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AddressId")) + ',';
                        }
                        RefreshGridViewStateAddress.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Address_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        view = (GridView)gridControlNextOfKin.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }
                        this.RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Next_Kin_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }
                        this.RefreshGridViewStateTraining.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        view = (GridView)gridControlDiscipline.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SanctionID")) + ',';
                        }
                        RefreshGridViewStateDiscipline.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Sanction_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    {
                        view = (GridView)gridControlVetting.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VettingID")) + ',';
                        }
                        RefreshGridViewStateVetting.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Vetting_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        view = (GridView)gridControlDeptWorked.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeDeptWorkedID")) + ',';
                        }
                        RefreshGridViewStateDeptWorked.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Department_Worked_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternHeaderID")) + ',';
                        }
                        RefreshGridViewStateWorkingPatternHeader.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Working_Pattern_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        view = (GridView)gridControlWorkingPattern.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternID")) + ',';
                        }
                        RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    {
                        view = (GridView)gridControlHolidayYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeHolidayYearID")) + ',';
                        }
                        RefreshGridViewStateHolidayYear.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Holiday_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Absences:
                    {
                        view = (GridView)gridControlAbsences.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AbsenceID")) + ',';
                        }
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Absence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        int intElegibleForBonus = 0;
                        DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            intElegibleForBonus = (string.IsNullOrEmpty(currentRow["ElegibleForBonus"].ToString()) ? 0 : Convert.ToInt32(currentRow["ElegibleForBonus"]));
                        }
                        if (intElegibleForBonus != 1) return;

                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BonusID")) + ',';
                        }
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Bonus_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionID")) + ',';
                        }
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pension_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        view = (GridView)gridControlReference.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ReferenceID")) + ',';
                        }
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Reference_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        view = (GridView)gridControlPayRise.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PayRiseID")) + ',';
                        }
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pay_Rise_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        int intElegibleForShares = 0;
                        DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
                        if (currentRow != null)
                        {
                            intElegibleForShares = (string.IsNullOrEmpty(currentRow["ElegibleForShares"].ToString()) ? 0 : Convert.ToInt32(currentRow["ElegibleForShares"]));
                        }
                        if (intElegibleForShares != 1) return;

                        view = (GridView)gridControlShares.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SharesID")) + ',';
                        }
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Shares_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            if (strFormMode == "view" || strFormMode == "blockedit" || strFormMode == "blockadd") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Addresses:
                    {
                        view = (GridView)gridControlAddresses.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Addresses to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Address" : Convert.ToString(intRowHandles.Length) + " Linked Employee Addresses") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Address" : "these Employee Addresses") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "AddressId")) + ",";
                            }

                            RefreshGridViewStateAddress.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_address", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_Addresses();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        view = (GridView)gridControlNextOfKin.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Next of Kin Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Next of Kin Record" : Convert.ToString(intRowHandles.Length) + " Linked Next of Kin Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Next of Kin Record" : "these Linked Next of Kin Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "Id")) + ",";
                            }

                            this.RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("next_of_kin", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinked_NextOfKin();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        view = (GridView)gridControlTraining.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Qualifications to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Qualification" : Convert.ToString(intRowHandles.Length) + " Qualifications") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this OQualification" : "these Qualifications") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "QualificationID")) + ",";
                            }

                            RefreshGridViewStateTraining.SaveViewInfo();  // Store Grid View State //
                            using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("qualification", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_Training();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;
                    }
                case Utils.enmMainGrids.Discipline:
                    {
                        view = (GridView)gridControlDiscipline.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Discipline/Grievance Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Discipline/Grievance" : Convert.ToString(intRowHandles.Length) + " Linked Employee Discipline/Grievances") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Discipline/Grievance" : "these Employee Discipline/Grievances") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SanctionID")) + ",";
                            }

                            RefreshGridViewStateDiscipline.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_sanction", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinked_Discipline();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    {
                        view = (GridView)gridControlVetting.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Vetting Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Vetting Record" : Convert.ToString(intRowHandles.Length) + " Linked Employee Vetting Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Vetting Record" : "these Employee Vetting Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "VettingID")) + ",";
                            }

                            RefreshGridViewStateVetting.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_vetting", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_Vetting();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        view = (GridView)gridControlDeptWorked.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Departments Worked to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Department Worked" : Convert.ToString(intRowHandles.Length) + " Linked Employee Departments Worked") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Department Worked" : "these Employee Departments Worked") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "EmployeeDeptWorkedID")) + ",";
                            }

                            RefreshGridViewStateDeptWorked.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("department_worked", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_DeptWorked();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Working Pattern Headers to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Working Pattern Header" : Convert.ToString(intRowHandles.Length) + " Linked Employee Working Pattern Headers") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Working Pattern Header" : "these Employee Working Pattern Headers") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "WorkingPatternHeaderID")) + ",";
                            }

                            RefreshGridViewStateWorkingPatternHeader.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("work_pattern_header", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_WorkingPatternHeader();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        view = (GridView)gridControlWorkingPattern.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Working Patterns to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Working Pattern" : Convert.ToString(intRowHandles.Length) + " Linked Employee Working Patterns") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Working Pattern" : "these Employee Working Patterns") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "WorkingPatternID")) + ",";
                            }

                            RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("work_pattern", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_WorkingPattern();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    {
                        view = (GridView)gridControlHolidayYear.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Holiday Years to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Holiday Year" : Convert.ToString(intRowHandles.Length) + " Holiday Years") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Holiday Year" : "these Holiday Years") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "EmployeeHolidayYearID")) + ",";
                            }

                            RefreshGridViewStateHolidayYear.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_holiday_year", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_HolidayYears();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Absences:
                    {
                        view = (GridView)gridControlAbsences.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Absences to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Absence" : Convert.ToString(intRowHandles.Length) + " Absences") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Absence" : "these Absences") + " will no longer be available for selection!</color>";
                        if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "AbsenceID")) + ",";
                            }

                            RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                            using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_absence", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_Absences();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        view = (GridView)gridControlBonuses.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Bonuses to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Bonus" : Convert.ToString(intRowHandles.Length) + " Linked Employee Bonuses") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Bonus" : "these Employee Bonuses") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "BonusID")) + ",";
                            }

                            RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_bonus", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_Bonuses();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        view = (GridView)gridControlPension.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Pensions to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Pension" : Convert.ToString(intRowHandles.Length) + " Linked Employee Pensions") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Pension" : "these Employee Pensions") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PensionID")) + ",";
                            }

                            RefreshGridViewStatePension.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_pension", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_Bonuses();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        view = (GridView)gridControlCRM.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee CRM Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee CRM Record" : Convert.ToString(intRowHandles.Length) + " Linked Employee CRM Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee CRM Record" : "these Employee CRM Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "CRMID")) + ",";
                            }

                            RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_crm", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinked_CRM();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        view = (GridView)gridControlReference.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Reference Records to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Reference Record" : Convert.ToString(intRowHandles.Length) + " Linked Employee Reference Records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Reference Record" : "these Employee Reference Records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ReferenceID")) + ",";
                            }

                            RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_reference", strRecordIDs);
                                }
                                catch (Exception) { }
                            }  // Remove the records from the DB in one go //
                            LoadLinked_References();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        view = (GridView)gridControlPayRise.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Pay Rises to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Pay Rise" : Convert.ToString(intRowHandles.Length) + " Linked Employee Pay Rises") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Pay Rise" : "these Employee Pay Rises") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PayRiseID")) + ",";
                            }

                            RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_pay_rise", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_PayRises();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        view = (GridView)gridControlShares.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Employee Shares to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Employee Share" : Convert.ToString(intRowHandles.Length) + " Linked Employee Shares") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Employee Share" : "these Employee Shares") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            string strRecordIDs = "";
                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SharesID")) + ",";
                            }

                            RefreshGridViewStateShares.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                            using (var RemoveRecords = new DataSet_HR_DataEntryTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp09000_HR_Delete("employee_shares", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinked_Shares();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmMainGrids.Addresses:
                    {
                        view = (GridView)gridControlAddresses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AddressId")) + ',';
                        }
                        RefreshGridViewStateAddress.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Address_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.NextOfKin:
                    {
                        view = (GridView)gridControlNextOfKin.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "Id")) + ',';
                        }
                        this.RefreshGridViewStateNextOfKin.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Next_Kin_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Training:
                    {
                        view = (GridView)gridControlTraining.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }
                        this.RefreshGridViewStateTraining.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Discipline:
                    {
                        view = (GridView)gridControlDiscipline.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SanctionID")) + ',';
                        }
                        RefreshGridViewStateDiscipline.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Sanction_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Vetting:
                    {
                        view = (GridView)gridControlVetting.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "VettingID")) + ',';
                        }
                        RefreshGridViewStateVetting.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Vetting_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.DepartmentsWorked:
                    {
                        view = (GridView)gridControlDeptWorked.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeDeptWorkedID")) + ',';
                        }
                        RefreshGridViewStateDeptWorked.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Department_Worked_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPatternHeader:
                    {
                        view = (GridView)gridControlWorkingPatternHeader.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternHeaderID")) + ',';
                        }
                        RefreshGridViewStateWorkingPatternHeader.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Working_Pattern_Header_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.WorkPattern:
                    {
                        view = (GridView)gridControlWorkingPattern.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "WorkingPatternID")) + ',';
                        }
                        RefreshGridViewStateWorkingPattern.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Working_Pattern_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.HolidayYear:
                    {
                        view = (GridView)gridControlHolidayYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "EmployeeHolidayYearID")) + ',';
                        }
                        RefreshGridViewStateHolidayYear.SaveViewInfo();  // Store Grid View State //
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Holiday_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Absences:
                    {
                        view = (GridView)gridControlAbsences.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AbsenceID")) + ',';
                        }
                        RefreshGridViewStateAbsence.SaveViewInfo();  // Store Grid View State //

                        var fChildForm = new frm_HR_Employee_Absence_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Bonus:
                    {
                        view = (GridView)gridControlBonuses.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BonusID")) + ',';
                        }
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Bonus_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Pensions:
                    {
                        view = (GridView)gridControlPension.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PensionID")) + ',';
                        }
                        RefreshGridViewStateBonus.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pension_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.CRM:
                    {
                        view = (GridView)gridControlCRM.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "CRMID")) + ',';
                        }
                        RefreshGridViewStateCRM.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_CRM_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Reference:
                    {
                        view = (GridView)gridControlReference.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ReferenceID")) + ',';
                        }
                        RefreshGridViewStateReference.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Reference_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.PayRise:
                    {
                        view = (GridView)gridControlPayRise.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PayRiseID")) + ',';
                        }
                        RefreshGridViewStatePayRise.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Pay_Rise_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmMainGrids.Shares:
                    {
                        view = (GridView)gridControlShares.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }

                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SharesID")) + ',';
                        }
                        RefreshGridViewStateShares.SaveViewInfo();  // Store Grid View State //
                        var fChildForm = new frm_HR_Employee_Shares_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp09002HREmployeeItemBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["EmployeeId"].ToString()) ? 0 : Convert.ToInt32(currentRow["EmployeeId"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            //int intRecordType = 1;  // Employee //
            int intRecordType = -1;  // Use dummy value so we can run branch in SP to return back all linked documents to do with the employee (employee, bonuses, holidays, discipline etc) //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = currentRow["Surname"].ToString() + ": " + currentRow["Firstname"].ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Set_Linked_Page_Visibility()
        {
            gridControlVetting.Enabled = i_boolVettingData;
            gridControlReference.Enabled = i_boolReferenceData;
            gridControlAddresses.Enabled = i_boolAddressData;
            gridControlNextOfKin.Enabled = i_boolNextOfKinData;
            gridControlDeptWorked.Enabled = i_boolDeptsWorkedData;
            gridControlWorkingPatternHeader.Enabled = i_boolWorkPatternData;
            gridControlWorkingPattern.Enabled = i_boolWorkPatternData;
            gridControlHolidayYear.Enabled = i_boolHolidayData;
            gridControlAbsences.Enabled = i_boolHolidayData;
            gridControlShares.Enabled = i_boolShareData;
            gridControlBonuses.Enabled = i_boolBonusData;
            gridControlPayRise.Enabled = i_boolPayRiseData;
            gridControlPension.Enabled = i_boolPensionData;
            gridControlTraining.Enabled = i_boolTrainingData;
            gridControlDiscipline.Enabled = i_boolDisciplineData;
            gridControlCRM.Enabled = i_boolCRMData;
            
        }





    }
}


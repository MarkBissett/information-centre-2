namespace WoodPlan5
{
    partial class frm_HR_Employee_Subsistence_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Employee_Subsistence_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.Rate2UnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00258EmployeeSubsistenceEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Rate2UnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.Rate2SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.Rate1UnitDescriptorIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Rate1UnitsSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.SubsistenceTypeIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spHR00260SubsistenceTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SalarySacrificeAmountSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.Rate1SpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.EndDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.StartDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.EmployeeNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeFirstnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EmployeeSurnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.EmployeeNameButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.EmployeeIDSpinEdit = new DevExpress.XtraEditors.TextEdit();
            this.SubsistenceIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ItemForEmployeeFirstname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeSurname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubsistenceID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEmployeeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForEmployeeName = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSalarySacrificeAmount = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForSubsistenceTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRate1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRate1Units = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRate2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForRate2Units = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRate2UnitDescriptorID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00258_Employee_Subsistence_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00258_Employee_Subsistence_EditTableAdapter();
            this.sp_HR_00260_Subsistence_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00260_Subsistence_Types_With_BlankTableAdapter();
            this.sp_HR_00261_Subsistence_Unit_Descriptors_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00261_Subsistence_Unit_Descriptors_With_BlankTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Rate2UnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00258EmployeeSubsistenceEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate2UnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate2SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate1UnitDescriptorIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate1UnitsSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubsistenceTypeIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00260SubsistenceTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalarySacrificeAmountSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate1SpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubsistenceIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubsistenceID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSalarySacrificeAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubsistenceTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate1Units)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate2Units)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate2UnitDescriptorID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(555, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 570);
            this.barDockControlBottom.Size = new System.Drawing.Size(555, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 544);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(555, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 544);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "ID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn7.Width = 53;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(555, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 570);
            this.barDockControl2.Size = new System.Drawing.Size(555, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 544);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(555, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 544);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.Rate2UnitDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.Rate2UnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.Rate2SpinEdit);
            this.dataLayoutControl1.Controls.Add(this.Rate1UnitDescriptorIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.Rate1UnitsSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SubsistenceTypeIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SalarySacrificeAmountSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.Rate1SpinEdit);
            this.dataLayoutControl1.Controls.Add(this.EndDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.StartDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeFirstnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeSurnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.EmployeeNameButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.EmployeeIDSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.SubsistenceIDTextEdit);
            this.dataLayoutControl1.DataSource = this.spHR00258EmployeeSubsistenceEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmployeeFirstname,
            this.ItemForEmployeeNumber,
            this.ItemForEmployeeSurname,
            this.ItemForSubsistenceID,
            this.ItemForEmployeeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1128, 186, 604, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(555, 544);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // Rate2UnitDescriptorIDGridLookUpEdit
            // 
            this.Rate2UnitDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "Rate2UnitDescriptorID", true));
            this.Rate2UnitDescriptorIDGridLookUpEdit.EditValue = "";
            this.Rate2UnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(351, 233);
            this.Rate2UnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.Rate2UnitDescriptorIDGridLookUpEdit.Name = "Rate2UnitDescriptorIDGridLookUpEdit";
            this.Rate2UnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Rate2UnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource;
            this.Rate2UnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.Rate2UnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.Rate2UnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.Rate2UnitDescriptorIDGridLookUpEdit.Properties.View = this.gridView2;
            this.Rate2UnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(192, 20);
            this.Rate2UnitDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.Rate2UnitDescriptorIDGridLookUpEdit.TabIndex = 48;
            // 
            // spHR00258EmployeeSubsistenceEditBindingSource
            // 
            this.spHR00258EmployeeSubsistenceEditBindingSource.DataMember = "sp_HR_00258_Employee_Subsistence_Edit";
            this.spHR00258EmployeeSubsistenceEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource
            // 
            this.spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource.DataMember = "sp_HR_00261_Subsistence_Unit_Descriptors_With_Blank";
            this.spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn7;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Unit Descriptor";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "RecordOrder";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // Rate2UnitsSpinEdit
            // 
            this.Rate2UnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "Rate2Units", true));
            this.Rate2UnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Rate2UnitsSpinEdit.Location = new System.Drawing.Point(114, 233);
            this.Rate2UnitsSpinEdit.MenuManager = this.barManager1;
            this.Rate2UnitsSpinEdit.Name = "Rate2UnitsSpinEdit";
            this.Rate2UnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Rate2UnitsSpinEdit.Properties.Mask.EditMask = "n2";
            this.Rate2UnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.Rate2UnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            131072});
            this.Rate2UnitsSpinEdit.Size = new System.Drawing.Size(131, 20);
            this.Rate2UnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.Rate2UnitsSpinEdit.TabIndex = 48;
            // 
            // Rate2SpinEdit
            // 
            this.Rate2SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "Rate2", true));
            this.Rate2SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Rate2SpinEdit.Location = new System.Drawing.Point(114, 209);
            this.Rate2SpinEdit.MenuManager = this.barManager1;
            this.Rate2SpinEdit.Name = "Rate2SpinEdit";
            this.Rate2SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Rate2SpinEdit.Properties.Mask.EditMask = "c";
            this.Rate2SpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.Rate2SpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.Rate2SpinEdit.Properties.MinValue = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147352576});
            this.Rate2SpinEdit.Size = new System.Drawing.Size(131, 20);
            this.Rate2SpinEdit.StyleController = this.dataLayoutControl1;
            this.Rate2SpinEdit.TabIndex = 45;
            this.Rate2SpinEdit.EditValueChanged += new System.EventHandler(this.Rate2SpinEdit_EditValueChanged);
            // 
            // Rate1UnitDescriptorIDGridLookUpEdit
            // 
            this.Rate1UnitDescriptorIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "Rate1UnitDescriptorID", true));
            this.Rate1UnitDescriptorIDGridLookUpEdit.EditValue = "";
            this.Rate1UnitDescriptorIDGridLookUpEdit.Location = new System.Drawing.Point(351, 175);
            this.Rate1UnitDescriptorIDGridLookUpEdit.MenuManager = this.barManager1;
            this.Rate1UnitDescriptorIDGridLookUpEdit.Name = "Rate1UnitDescriptorIDGridLookUpEdit";
            this.Rate1UnitDescriptorIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Rate1UnitDescriptorIDGridLookUpEdit.Properties.DataSource = this.spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource;
            this.Rate1UnitDescriptorIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.Rate1UnitDescriptorIDGridLookUpEdit.Properties.NullText = "";
            this.Rate1UnitDescriptorIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.Rate1UnitDescriptorIDGridLookUpEdit.Properties.View = this.gridView1;
            this.Rate1UnitDescriptorIDGridLookUpEdit.Size = new System.Drawing.Size(192, 20);
            this.Rate1UnitDescriptorIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.Rate1UnitDescriptorIDGridLookUpEdit.TabIndex = 47;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn4;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView1.FormatRules.Add(gridFormatRule2);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Unit Descriptor";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "RecordOrder";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // Rate1UnitsSpinEdit
            // 
            this.Rate1UnitsSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "Rate1Units", true));
            this.Rate1UnitsSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Rate1UnitsSpinEdit.Location = new System.Drawing.Point(114, 175);
            this.Rate1UnitsSpinEdit.MenuManager = this.barManager1;
            this.Rate1UnitsSpinEdit.Name = "Rate1UnitsSpinEdit";
            this.Rate1UnitsSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Rate1UnitsSpinEdit.Properties.Mask.EditMask = "n2";
            this.Rate1UnitsSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.Rate1UnitsSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99999999,
            0,
            0,
            131072});
            this.Rate1UnitsSpinEdit.Size = new System.Drawing.Size(131, 20);
            this.Rate1UnitsSpinEdit.StyleController = this.dataLayoutControl1;
            this.Rate1UnitsSpinEdit.TabIndex = 47;
            // 
            // SubsistenceTypeIDGridLookUpEdit
            // 
            this.SubsistenceTypeIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "SubsistenceTypeID", true));
            this.SubsistenceTypeIDGridLookUpEdit.EditValue = "";
            this.SubsistenceTypeIDGridLookUpEdit.Location = new System.Drawing.Point(114, 69);
            this.SubsistenceTypeIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SubsistenceTypeIDGridLookUpEdit.Name = "SubsistenceTypeIDGridLookUpEdit";
            this.SubsistenceTypeIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SubsistenceTypeIDGridLookUpEdit.Properties.DataSource = this.spHR00260SubsistenceTypesWithBlankBindingSource;
            this.SubsistenceTypeIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.SubsistenceTypeIDGridLookUpEdit.Properties.NullText = "";
            this.SubsistenceTypeIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.SubsistenceTypeIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.SubsistenceTypeIDGridLookUpEdit.Size = new System.Drawing.Size(429, 20);
            this.SubsistenceTypeIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SubsistenceTypeIDGridLookUpEdit.TabIndex = 46;
            // 
            // spHR00260SubsistenceTypesWithBlankBindingSource
            // 
            this.spHR00260SubsistenceTypesWithBlankBindingSource.DataMember = "sp_HR_00260_Subsistence_Types_With_Blank";
            this.spHR00260SubsistenceTypesWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn1;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridLookUpEdit1View.FormatRules.Add(gridFormatRule3);
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Subsustence Type";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "RecordOrder";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // SalarySacrificeAmountSpinEdit
            // 
            this.SalarySacrificeAmountSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "SalarySacrificeAmount", true));
            this.SalarySacrificeAmountSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SalarySacrificeAmountSpinEdit.Location = new System.Drawing.Point(114, 267);
            this.SalarySacrificeAmountSpinEdit.MenuManager = this.barManager1;
            this.SalarySacrificeAmountSpinEdit.Name = "SalarySacrificeAmountSpinEdit";
            this.SalarySacrificeAmountSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SalarySacrificeAmountSpinEdit.Properties.Mask.EditMask = "c";
            this.SalarySacrificeAmountSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.SalarySacrificeAmountSpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.SalarySacrificeAmountSpinEdit.Properties.MinValue = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147352576});
            this.SalarySacrificeAmountSpinEdit.Size = new System.Drawing.Size(131, 20);
            this.SalarySacrificeAmountSpinEdit.StyleController = this.dataLayoutControl1;
            this.SalarySacrificeAmountSpinEdit.TabIndex = 45;
            this.SalarySacrificeAmountSpinEdit.EditValueChanged += new System.EventHandler(this.SalarySacrificeAmountSpinEdit_EditValueChanged);
            // 
            // Rate1SpinEdit
            // 
            this.Rate1SpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "Rate1", true));
            this.Rate1SpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Rate1SpinEdit.Location = new System.Drawing.Point(114, 151);
            this.Rate1SpinEdit.MenuManager = this.barManager1;
            this.Rate1SpinEdit.Name = "Rate1SpinEdit";
            this.Rate1SpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Rate1SpinEdit.Properties.Mask.EditMask = "c";
            this.Rate1SpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.Rate1SpinEdit.Properties.MaxValue = new decimal(new int[] {
            999999999,
            0,
            0,
            131072});
            this.Rate1SpinEdit.Properties.MinValue = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147352576});
            this.Rate1SpinEdit.Size = new System.Drawing.Size(131, 20);
            this.Rate1SpinEdit.StyleController = this.dataLayoutControl1;
            this.Rate1SpinEdit.TabIndex = 44;
            this.Rate1SpinEdit.EditValueChanged += new System.EventHandler(this.Rate1SpinEdit_EditValueChanged);
            // 
            // EndDateDateEdit
            // 
            this.EndDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "EndDate", true));
            this.EndDateDateEdit.EditValue = null;
            this.EndDateDateEdit.Location = new System.Drawing.Point(114, 117);
            this.EndDateDateEdit.MenuManager = this.barManager1;
            this.EndDateDateEdit.Name = "EndDateDateEdit";
            this.EndDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EndDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.EndDateDateEdit.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.EndDateDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.EndDateDateEdit.Size = new System.Drawing.Size(429, 20);
            this.EndDateDateEdit.StyleController = this.dataLayoutControl1;
            this.EndDateDateEdit.TabIndex = 42;
            this.EndDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EndDateDateEdit_Validating);
            // 
            // StartDateDateEdit
            // 
            this.StartDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "StartDate", true));
            this.StartDateDateEdit.EditValue = null;
            this.StartDateDateEdit.Location = new System.Drawing.Point(114, 93);
            this.StartDateDateEdit.MenuManager = this.barManager1;
            this.StartDateDateEdit.Name = "StartDateDateEdit";
            this.StartDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.StartDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StartDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.StartDateDateEdit.Properties.MaxValue = new System.DateTime(2500, 12, 31, 0, 0, 0, 0);
            this.StartDateDateEdit.Properties.MinValue = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.StartDateDateEdit.Size = new System.Drawing.Size(429, 20);
            this.StartDateDateEdit.StyleController = this.dataLayoutControl1;
            this.StartDateDateEdit.TabIndex = 41;
            this.StartDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.StartDateDateEdit_Validating);
            // 
            // EmployeeNumberTextEdit
            // 
            this.EmployeeNumberTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "EmployeeNumber", true));
            this.EmployeeNumberTextEdit.Location = new System.Drawing.Point(114, 69);
            this.EmployeeNumberTextEdit.MenuManager = this.barManager1;
            this.EmployeeNumberTextEdit.Name = "EmployeeNumberTextEdit";
            this.EmployeeNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeNumberTextEdit, true);
            this.EmployeeNumberTextEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeNumberTextEdit, optionsSpelling1);
            this.EmployeeNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNumberTextEdit.TabIndex = 40;
            // 
            // EmployeeFirstnameTextEdit
            // 
            this.EmployeeFirstnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "EmployeeFirstname", true));
            this.EmployeeFirstnameTextEdit.Location = new System.Drawing.Point(116, 69);
            this.EmployeeFirstnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeFirstnameTextEdit.Name = "EmployeeFirstnameTextEdit";
            this.EmployeeFirstnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeFirstnameTextEdit, true);
            this.EmployeeFirstnameTextEdit.Size = new System.Drawing.Size(425, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeFirstnameTextEdit, optionsSpelling2);
            this.EmployeeFirstnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeFirstnameTextEdit.TabIndex = 39;
            // 
            // EmployeeSurnameTextEdit
            // 
            this.EmployeeSurnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "EmployeeSurname", true));
            this.EmployeeSurnameTextEdit.Location = new System.Drawing.Point(114, 69);
            this.EmployeeSurnameTextEdit.MenuManager = this.barManager1;
            this.EmployeeSurnameTextEdit.Name = "EmployeeSurnameTextEdit";
            this.EmployeeSurnameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeSurnameTextEdit, true);
            this.EmployeeSurnameTextEdit.Size = new System.Drawing.Size(427, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeSurnameTextEdit, optionsSpelling3);
            this.EmployeeSurnameTextEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeSurnameTextEdit.TabIndex = 38;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 371);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(483, 137);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling4);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.spHR00258EmployeeSubsistenceEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(114, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(174, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // EmployeeNameButtonEdit
            // 
            this.EmployeeNameButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "EmployeeName", true));
            this.EmployeeNameButtonEdit.EditValue = "";
            this.EmployeeNameButtonEdit.Location = new System.Drawing.Point(114, 35);
            this.EmployeeNameButtonEdit.MenuManager = this.barManager1;
            this.EmployeeNameButtonEdit.Name = "EmployeeNameButtonEdit";
            this.EmployeeNameButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to Open Select Employee screen", "choose", null, true)});
            this.EmployeeNameButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.EmployeeNameButtonEdit.Size = new System.Drawing.Size(429, 20);
            this.EmployeeNameButtonEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeNameButtonEdit.TabIndex = 6;
            this.EmployeeNameButtonEdit.TabStop = false;
            this.EmployeeNameButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.EmployeeNameButtonEdit_ButtonClick);
            this.EmployeeNameButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.EmployeeNameButtonEdit_Validating);
            // 
            // EmployeeIDSpinEdit
            // 
            this.EmployeeIDSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "EmployeeID", true));
            this.EmployeeIDSpinEdit.EditValue = "";
            this.EmployeeIDSpinEdit.Location = new System.Drawing.Point(148, 208);
            this.EmployeeIDSpinEdit.MenuManager = this.barManager1;
            this.EmployeeIDSpinEdit.Name = "EmployeeIDSpinEdit";
            this.EmployeeIDSpinEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.EmployeeIDSpinEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.EmployeeIDSpinEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.EmployeeIDSpinEdit, true);
            this.EmployeeIDSpinEdit.Size = new System.Drawing.Size(393, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.EmployeeIDSpinEdit, optionsSpelling5);
            this.EmployeeIDSpinEdit.StyleController = this.dataLayoutControl1;
            this.EmployeeIDSpinEdit.TabIndex = 25;
            // 
            // SubsistenceIDTextEdit
            // 
            this.SubsistenceIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00258EmployeeSubsistenceEditBindingSource, "SubsistenceID", true));
            this.SubsistenceIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SubsistenceIDTextEdit.Location = new System.Drawing.Point(148, 208);
            this.SubsistenceIDTextEdit.MenuManager = this.barManager1;
            this.SubsistenceIDTextEdit.Name = "SubsistenceIDTextEdit";
            this.SubsistenceIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.SubsistenceIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.SubsistenceIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SubsistenceIDTextEdit, true);
            this.SubsistenceIDTextEdit.Size = new System.Drawing.Size(393, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SubsistenceIDTextEdit, optionsSpelling6);
            this.SubsistenceIDTextEdit.StyleController = this.dataLayoutControl1;
            this.SubsistenceIDTextEdit.TabIndex = 27;
            // 
            // ItemForEmployeeFirstname
            // 
            this.ItemForEmployeeFirstname.Control = this.EmployeeFirstnameTextEdit;
            this.ItemForEmployeeFirstname.CustomizationFormText = "Employee Forename:";
            this.ItemForEmployeeFirstname.Location = new System.Drawing.Point(0, 57);
            this.ItemForEmployeeFirstname.Name = "ItemForEmployeeFirstname";
            this.ItemForEmployeeFirstname.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeFirstname.Text = "Employee Forename:";
            this.ItemForEmployeeFirstname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeNumber
            // 
            this.ItemForEmployeeNumber.Control = this.EmployeeNumberTextEdit;
            this.ItemForEmployeeNumber.CustomizationFormText = "Employee Number:";
            this.ItemForEmployeeNumber.Location = new System.Drawing.Point(0, 57);
            this.ItemForEmployeeNumber.Name = "ItemForEmployeeNumber";
            this.ItemForEmployeeNumber.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeNumber.Text = "Employee Number:";
            this.ItemForEmployeeNumber.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForEmployeeSurname
            // 
            this.ItemForEmployeeSurname.Control = this.EmployeeSurnameTextEdit;
            this.ItemForEmployeeSurname.CustomizationFormText = "Employee Surname:";
            this.ItemForEmployeeSurname.Location = new System.Drawing.Point(0, 57);
            this.ItemForEmployeeSurname.Name = "ItemForEmployeeSurname";
            this.ItemForEmployeeSurname.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeSurname.Text = "Employee Surname:";
            this.ItemForEmployeeSurname.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForSubsistenceID
            // 
            this.ItemForSubsistenceID.Control = this.SubsistenceIDTextEdit;
            this.ItemForSubsistenceID.CustomizationFormText = "Subsistence ID:";
            this.ItemForSubsistenceID.Location = new System.Drawing.Point(0, 196);
            this.ItemForSubsistenceID.Name = "ItemForSubsistenceID";
            this.ItemForSubsistenceID.Size = new System.Drawing.Size(533, 24);
            this.ItemForSubsistenceID.Text = "Subsistence ID:";
            this.ItemForSubsistenceID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // ItemForEmployeeID
            // 
            this.ItemForEmployeeID.Control = this.EmployeeIDSpinEdit;
            this.ItemForEmployeeID.CustomizationFormText = "Employee ID:";
            this.ItemForEmployeeID.Location = new System.Drawing.Point(0, 196);
            this.ItemForEmployeeID.Name = "ItemForEmployeeID";
            this.ItemForEmployeeID.Size = new System.Drawing.Size(533, 24);
            this.ItemForEmployeeID.Text = "Employee ID:";
            this.ItemForEmployeeID.TextSize = new System.Drawing.Size(84, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(555, 544);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForEmployeeName,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.emptySpaceItem4,
            this.layoutControlGroup6,
            this.ItemForSalarySacrificeAmount,
            this.emptySpaceItem3,
            this.ItemForSubsistenceTypeID,
            this.ItemForStartDate,
            this.ItemForEndDate,
            this.ItemForRate1,
            this.emptySpaceItem5,
            this.ItemForRate1Units,
            this.layoutControlItem2,
            this.emptySpaceItem6,
            this.ItemForRate2,
            this.emptySpaceItem7,
            this.ItemForRate2Units,
            this.ItemForRate2UnitDescriptorID,
            this.emptySpaceItem8,
            this.emptySpaceItem9,
            this.emptySpaceItem10});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(535, 524);
            // 
            // ItemForEmployeeName
            // 
            this.ItemForEmployeeName.AllowHide = false;
            this.ItemForEmployeeName.Control = this.EmployeeNameButtonEdit;
            this.ItemForEmployeeName.CustomizationFormText = "Firstname:";
            this.ItemForEmployeeName.Location = new System.Drawing.Point(0, 23);
            this.ItemForEmployeeName.Name = "ItemForEmployeeName";
            this.ItemForEmployeeName.Size = new System.Drawing.Size(535, 24);
            this.ItemForEmployeeName.Text = "Linked to Employee: ";
            this.ItemForEmployeeName.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(102, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(102, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(102, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(280, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(255, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(102, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(178, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 47);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(535, 10);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 289);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(535, 235);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(511, 189);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(487, 141);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(487, 141);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForSalarySacrificeAmount
            // 
            this.ItemForSalarySacrificeAmount.Control = this.SalarySacrificeAmountSpinEdit;
            this.ItemForSalarySacrificeAmount.CustomizationFormText = "Salary Sacrifice:";
            this.ItemForSalarySacrificeAmount.Location = new System.Drawing.Point(0, 255);
            this.ItemForSalarySacrificeAmount.MaxSize = new System.Drawing.Size(237, 24);
            this.ItemForSalarySacrificeAmount.MinSize = new System.Drawing.Size(237, 24);
            this.ItemForSalarySacrificeAmount.Name = "ItemForSalarySacrificeAmount";
            this.ItemForSalarySacrificeAmount.Size = new System.Drawing.Size(237, 24);
            this.ItemForSalarySacrificeAmount.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForSalarySacrificeAmount.Text = "Salary Sacrifice:";
            this.ItemForSalarySacrificeAmount.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 279);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(535, 10);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForSubsistenceTypeID
            // 
            this.ItemForSubsistenceTypeID.Control = this.SubsistenceTypeIDGridLookUpEdit;
            this.ItemForSubsistenceTypeID.Location = new System.Drawing.Point(0, 57);
            this.ItemForSubsistenceTypeID.Name = "ItemForSubsistenceTypeID";
            this.ItemForSubsistenceTypeID.Size = new System.Drawing.Size(535, 24);
            this.ItemForSubsistenceTypeID.Text = "Subsistence Type:";
            this.ItemForSubsistenceTypeID.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.StartDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 81);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(535, 24);
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.EndDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(0, 105);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(535, 24);
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForRate1
            // 
            this.ItemForRate1.Control = this.Rate1SpinEdit;
            this.ItemForRate1.CustomizationFormText = "Rate 1:";
            this.ItemForRate1.Location = new System.Drawing.Point(0, 139);
            this.ItemForRate1.MaxSize = new System.Drawing.Size(237, 24);
            this.ItemForRate1.MinSize = new System.Drawing.Size(237, 24);
            this.ItemForRate1.Name = "ItemForRate1";
            this.ItemForRate1.Size = new System.Drawing.Size(237, 24);
            this.ItemForRate1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRate1.Text = "Rate 1:";
            this.ItemForRate1.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 129);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(535, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRate1Units
            // 
            this.ItemForRate1Units.Control = this.Rate1UnitsSpinEdit;
            this.ItemForRate1Units.Location = new System.Drawing.Point(0, 163);
            this.ItemForRate1Units.MaxSize = new System.Drawing.Size(237, 24);
            this.ItemForRate1Units.MinSize = new System.Drawing.Size(237, 24);
            this.ItemForRate1Units.Name = "ItemForRate1Units";
            this.ItemForRate1Units.Size = new System.Drawing.Size(237, 24);
            this.ItemForRate1Units.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRate1Units.Text = "Rate 1 Units:";
            this.ItemForRate1Units.TextSize = new System.Drawing.Size(99, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.Rate1UnitDescriptorIDGridLookUpEdit;
            this.layoutControlItem2.Location = new System.Drawing.Point(237, 163);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(298, 24);
            this.layoutControlItem2.Text = "Unit Descriptor:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(237, 139);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(298, 24);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRate2
            // 
            this.ItemForRate2.Control = this.Rate2SpinEdit;
            this.ItemForRate2.Location = new System.Drawing.Point(0, 197);
            this.ItemForRate2.MaxSize = new System.Drawing.Size(237, 24);
            this.ItemForRate2.MinSize = new System.Drawing.Size(237, 24);
            this.ItemForRate2.Name = "ItemForRate2";
            this.ItemForRate2.Size = new System.Drawing.Size(237, 24);
            this.ItemForRate2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRate2.Text = "Rate 2:";
            this.ItemForRate2.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(237, 197);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(298, 24);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForRate2Units
            // 
            this.ItemForRate2Units.Control = this.Rate2UnitsSpinEdit;
            this.ItemForRate2Units.Location = new System.Drawing.Point(0, 221);
            this.ItemForRate2Units.MaxSize = new System.Drawing.Size(237, 24);
            this.ItemForRate2Units.MinSize = new System.Drawing.Size(237, 24);
            this.ItemForRate2Units.Name = "ItemForRate2Units";
            this.ItemForRate2Units.Size = new System.Drawing.Size(237, 24);
            this.ItemForRate2Units.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRate2Units.Text = "Rate 2 Units:";
            this.ItemForRate2Units.TextSize = new System.Drawing.Size(99, 13);
            // 
            // ItemForRate2UnitDescriptorID
            // 
            this.ItemForRate2UnitDescriptorID.Control = this.Rate2UnitDescriptorIDGridLookUpEdit;
            this.ItemForRate2UnitDescriptorID.Location = new System.Drawing.Point(237, 221);
            this.ItemForRate2UnitDescriptorID.Name = "ItemForRate2UnitDescriptorID";
            this.ItemForRate2UnitDescriptorID.Size = new System.Drawing.Size(298, 24);
            this.ItemForRate2UnitDescriptorID.Text = "Unit Descriptor:";
            this.ItemForRate2UnitDescriptorID.TextSize = new System.Drawing.Size(99, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 187);
            this.emptySpaceItem8.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem8.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(535, 10);
            this.emptySpaceItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem9
            // 
            this.emptySpaceItem9.AllowHotTrack = false;
            this.emptySpaceItem9.Location = new System.Drawing.Point(0, 245);
            this.emptySpaceItem9.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem9.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem9.Name = "emptySpaceItem9";
            this.emptySpaceItem9.Size = new System.Drawing.Size(535, 10);
            this.emptySpaceItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem10
            // 
            this.emptySpaceItem10.AllowHotTrack = false;
            this.emptySpaceItem10.Location = new System.Drawing.Point(237, 255);
            this.emptySpaceItem10.Name = "emptySpaceItem10";
            this.emptySpaceItem10.Size = new System.Drawing.Size(298, 24);
            this.emptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00258_Employee_Subsistence_EditTableAdapter
            // 
            this.sp_HR_00258_Employee_Subsistence_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00260_Subsistence_Types_With_BlankTableAdapter
            // 
            this.sp_HR_00260_Subsistence_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00261_Subsistence_Unit_Descriptors_With_BlankTableAdapter
            // 
            this.sp_HR_00261_Subsistence_Unit_Descriptors_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Employee_Subsistence_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(555, 600);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Employee_Subsistence_Edit";
            this.Text = "Edit Employee Subsistence";
            this.Activated += new System.EventHandler(this.frm_HR_Employee_Subsistence_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Employee_Subsistence_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Employee_Subsistence_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Rate2UnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00258EmployeeSubsistenceEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate2UnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate2SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate1UnitDescriptorIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate1UnitsSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubsistenceTypeIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00260SubsistenceTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SalarySacrificeAmountSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rate1SpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeFirstnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeSurnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeNameButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmployeeIDSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SubsistenceIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeFirstname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeSurname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubsistenceID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEmployeeName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSalarySacrificeAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubsistenceTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate1Units)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate2Units)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRate2UnitDescriptorID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeName;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeID;
        private DevExpress.XtraEditors.ButtonEdit EmployeeNameButtonEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeIDSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubsistenceID;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.TextEdit SubsistenceIDTextEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeNumberTextEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeFirstnameTextEdit;
        private DevExpress.XtraEditors.TextEdit EmployeeSurnameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeSurname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeFirstname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEmployeeNumber;
        private DevExpress.XtraEditors.DateEdit EndDateDateEdit;
        private DevExpress.XtraEditors.DateEdit StartDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraEditors.SpinEdit Rate1SpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRate1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.SpinEdit SalarySacrificeAmountSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSalarySacrificeAmount;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private System.Windows.Forms.BindingSource spHR00258EmployeeSubsistenceEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00258_Employee_Subsistence_EditTableAdapter sp_HR_00258_Employee_Subsistence_EditTableAdapter;
        private DevExpress.XtraEditors.GridLookUpEdit SubsistenceTypeIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubsistenceTypeID;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00260SubsistenceTypesWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00260_Subsistence_Types_With_BlankTableAdapter sp_HR_00260_Subsistence_Types_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraEditors.GridLookUpEdit Rate1UnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.SpinEdit Rate1UnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRate1Units;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private System.Windows.Forms.BindingSource spHR00261SubsistenceUnitDescriptorsWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00261_Subsistence_Unit_Descriptors_With_BlankTableAdapter sp_HR_00261_Subsistence_Unit_Descriptors_With_BlankTableAdapter;
        private DevExpress.XtraEditors.SpinEdit Rate2SpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRate2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraEditors.GridLookUpEdit Rate2UnitDescriptorIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraEditors.SpinEdit Rate2UnitsSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRate2Units;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRate2UnitDescriptorID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem10;
    }
}

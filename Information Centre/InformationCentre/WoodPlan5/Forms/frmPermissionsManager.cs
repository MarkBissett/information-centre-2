using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using WoodPlan5.Properties;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Data;
using DevExpress.Utils;
using BaseObjects;
using DevExpress.XtraEditors.Repository;


namespace WoodPlan5
{
    public partial class frmPermissionsManager : frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        //bool iBool_AllowAdd = false;
        //bool iBool_AllowEdit = false;
        //bool iBool_AllowDelete = false;


        private int i_int_FocusedGrid = 1;
 
        public bool UpdateRefreshStatus = false; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the checkbox editor //
        #endregion

        public frmPermissionsManager()
        {
            InitializeComponent();
        }

        private void frmPermissionsManager_Load(object sender, EventArgs e)
        {
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            this.LockThisWindow();  // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_Groups.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            sp00093_permission_manager_groups_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00123_permission_manager_functionality_selectedTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            sp00095_permission_manager_functionality_availableTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00094_permission_manager_group_membersTableAdapter.Connection.ConnectionString = strConnectionString;

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            ProcessPermissionsForForm();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //

            RefreshGridViewState = new RefreshGridState(gridView1, "GroupID");

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
            
            gridControl1.BeginUpdate();
            LoadData();
            gridControl1.EndUpdate();

            if (fProgress != null) fProgress.UpdateProgress(10); // Update Progress Bar //
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
        }

        public void UpdateFormRefreshStatus(bool status)
        {
            UpdateRefreshStatus = status;
        }

        private void LoadData()
        {
            if (UpdateRefreshStatus) UpdateRefreshStatus = false;
            sp00093_permission_manager_groups_listTableAdapter.Fill(this.dataSet_Groups.sp00093_permission_manager_groups_list, "2,3,");  // 2 = Screen Access Groups, 3 = Report Access Groups //
            this.RefreshGridViewState.LoadViewInfo();  // Reload any expanded groups and selected rows //

            emptyEditor = new RepositoryItem();
            gridControl3.RepositoryItems.Add(emptyEditor);
        }

        private void frmPermissionsManager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus)
            {
                gridControl1.BeginUpdate();
                LoadData();
                gridControl1.EndUpdate();
            }
            SetMenuStatus();
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_Groups.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
            /*    switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }  */
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;


            alItems.AddRange(new string[] { "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
  /*          if (i_int_FocusedGrid == 1)
            {
                GridView view = (GridView)gridControl1.MainView;
                int[] intRowHandles;
                intRowHandles = view.GetSelectedRows();

                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                else
                {
                    bsiAdd.Enabled = false;
                    bbiSingleAdd.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length > 0)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                }
                else
                {
                    bsiEdit.Enabled = false;
                    bbiSingleEdit.Enabled = false;
                }
                if (iBool_AllowEdit && intRowHandles.Length > 1)
                {
                    alItems.Add("iBlockEdit");
                    bbiBlockEdit.Enabled = true;
                }
                else
                {
                    bbiBlockEdit.Enabled = false;
                }
                if (iBool_AllowDelete && intRowHandles.Length > 0)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                else
                {
                    bbiDelete.Enabled = false;
                }
            }*/
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Permission Groups";
                    break;
                case "gridView2":
                    message = "No Available Functionality - Click on a Permission Group to view linked Available Functionality";
                    break;
                case "gridView3":
                    message = "No Selected Functionality - Click on a Permission Group to view linked Selected Functionality";
                    break;
                case "gridView4":
                    message = "No Staff Members - Click on a Permission Group to view linked Staff Members";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);
            SetMenuStatus();
        }


        #endregion


        #region PermissionGroupsGrid

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                //if (iBool_AllowEdit) EditRecords();
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;

            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add)
            {
                if (View.IsGroupRow(e.ControllerRow)) View.UnselectRow(e.ControllerRow);
                else
                {
                    // Prevent Group Spanning when Control Key is held down for multiple row selection //
                    string strType = "";
                    int[] intRowHandles = View.GetSelectedRows();
                    if (intRowHandles.Length > 0)
                    {
                        if (e.ControllerRow != intRowHandles[0] || intRowHandles.Length == 1)
                        {
                            strType = Convert.ToString(View.GetRowCellValue(intRowHandles[0], "GroupTypeID"));
                        }
                        else
                        {
                            strType = Convert.ToString(View.GetRowCellValue(intRowHandles[1], "GroupTypeID"));
                        }
                        if (Convert.ToString(View.GetRowCellValue(e.ControllerRow, "GroupTypeID")) != strType) View.UnselectRow(e.ControllerRow);
                    }
                }
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                // Prevent Group Spanning when Shift Key is held down for multiple row selection [following 2 lines and code inside ForEach loop below] //
                string strType = "";
                if (View.FocusedRowHandle != GridControl.InvalidRowHandle) strType = Convert.ToString(View.GetRowCellValue(View.FocusedRowHandle, "GroupTypeID"));

                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) View.UnselectRow(Row);
                    else
                    {
                        // Prevent Group Spanning when Shift Key is held down for multiple row selection //
                        if (Convert.ToString(View.GetRowCellValue(Row, "GroupTypeID")) != strType) View.UnselectRow(Row);
                    }
                }
                View.EndSelection();
            }
            isRunning = false;
            LoadGroupMembers();
            SetMenuStatus();
        }

        private void LoadGroupMembers()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            string strSelectedGroups = "";
            int intType = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedGroups += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GroupID"])) + ',';
                intType = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["GroupTypeID"]));
            }

            gridControl2.MainView.BeginUpdate();
            gridControl3.MainView.BeginUpdate();
            gridControl4.MainView.BeginUpdate();
            sp00095_permission_manager_functionality_availableTableAdapter.Fill(this.dataSet_Groups.sp00095_permission_manager_functionality_available, strSelectedGroups, intType);
            sp00123_permission_manager_functionality_selectedTableAdapter.Fill(this.dataSet_Groups.sp00123_permission_manager_functionality_selected, strSelectedGroups);
            sp00094_permission_manager_group_membersTableAdapter.Fill(this.dataSet_Groups.sp00094_permission_manager_group_members, strSelectedGroups);
            gridControl2.MainView.EndUpdate();
            gridControl3.MainView.EndUpdate();
            gridControl4.MainView.EndUpdate();
        }

        #endregion


        #region SelectedFunctionalityGrid

        private void gridView3_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "ReadAccess":
                    if (!Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, "ReadAvailable"))) e.RepositoryItem = emptyEditor;
                    break;
                case "CreateAccess":
                    if (!Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, "CreateAvailable"))) e.RepositoryItem = emptyEditor;
                    break;
                case "UpdateAccess":
                    if (!Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, "UpdateAvailable"))) e.RepositoryItem = emptyEditor;
                    break;
                case "DeleteAccess":
                    if (!Convert.ToBoolean(view.GetRowCellValue(e.RowHandle, "DeleteAvailable"))) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridView3_DoubleClick(object sender, EventArgs e)
        {
            RemoveOne();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 3;
            SetMenuStatus();
        }

        private void gridView3_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region AvailableFunctionalityGrid

        private void gridView2_DoubleClick(object sender, EventArgs e)
        {
            AddOne();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
            i_int_FocusedGrid = 2;
            SetMenuStatus();
        }

        private void gridView2_MouseDown(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void btnAddOne1_Click(object sender, EventArgs e)
        {
            AddOne();
        }
 
        private void btnAddAll1_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl2.MainView;
            int intCount = view.RowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("There are no functionality items to add to the selected group(s).", "Add All Functionality to Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedProgramParts = "";
            for (int i = 0; i < intCount; i++)
            {
                strSelectedProgramParts += Convert.ToString(view.GetRowCellValue(i, view.Columns["ProgramPartID"])) + ',';
            }
            view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more functionality items to add to before proceeding.", "Add All Functionality to Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedGroups = "";
            int intType = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedGroups += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GroupID"])) + ',';
                intType = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["GroupTypeID"]));
            }
            AddFunctionality(strSelectedProgramParts, strSelectedGroups, intType);
        }

        private void AddOne()
        {
            GridView view = (GridView)gridControl2.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more functionality items to add to the selected group(s) before proceeding.", "Add Selected Functionality to Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedProgramParts = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedProgramParts += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ProgramPartID"])) + ',';
            }
            view = (GridView)gridControl1.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Screen \\ Report Access Groups to add to before proceeding.", "Add Selected Functionality to Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedGroups = "";
            int intType = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedGroups += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["GroupID"])) + ',';
                intType = Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["GroupTypeID"]));
            }
            AddFunctionality(strSelectedProgramParts, strSelectedGroups, intType);
        }
        
        private void AddFunctionality(string strSelectedProgramParts, string strSelectedGroups, int intType)
        {
            using (DataSet_GroupsTableAdapters.QueriesTableAdapter AddFunctionalityItems = new DataSet_GroupsTableAdapters.QueriesTableAdapter())
            {
                // Add the selected staff to the selected groups //
                AddFunctionalityItems.ChangeConnectionString(strConnectionString);
                AddFunctionalityItems.sp00124_permission_manager_add_access(strSelectedGroups, strSelectedProgramParts);
            }

            gridControl2.MainView.BeginUpdate();   // Reload Available and Selected Group Members lists
            gridControl3.MainView.BeginUpdate();
            sp00095_permission_manager_functionality_availableTableAdapter.Fill(this.dataSet_Groups.sp00095_permission_manager_functionality_available, strSelectedGroups, intType);
            sp00123_permission_manager_functionality_selectedTableAdapter.Fill(this.dataSet_Groups.sp00123_permission_manager_functionality_selected, strSelectedGroups);
            gridControl2.MainView.EndUpdate();
            gridControl3.MainView.EndUpdate();
        }

        private void btnRemoveOne1_Click(object sender, EventArgs e)
        {
            RemoveOne();
        }

        private void btnRemoveAll1_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl3.MainView;
            int intCount = view.RowCount;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("There are no functionality items to remove from the selected group(s).", "Remove All Functionality from Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedProgramParts = "";
            for (int i = 0; i < intCount; i++)
            {
                strSelectedProgramParts += Convert.ToString(view.GetRowCellValue(i, view.Columns["ProgramAccessID"])) + ',';

            }
            RemoveFunctionality(strSelectedProgramParts);
        }

        private void RemoveOne()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more functionality items to remove from the selected group(s) before proceeding.", "Remove Selected Functionality from Selected Group(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            string strSelectedProgramParts = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedProgramParts += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ProgramAccessID"])) + ',';
            }
            RemoveFunctionality(strSelectedProgramParts);
        }

        private void RemoveFunctionality(string strSelectedProgramParts)
        {
            using (DataSet_GroupsTableAdapters.QueriesTableAdapter RemoveFunctionalityItems = new DataSet_GroupsTableAdapters.QueriesTableAdapter())
            {
                // Add the selected staff to the selected groups //
                RemoveFunctionalityItems.ChangeConnectionString(strConnectionString);
                RemoveFunctionalityItems.sp00125_permission_manager_remove_access("programaccess", strSelectedProgramParts);
            }
            LoadGroupMembers();   // Relaod Available and Selected Group Members lists
        }

        private void gridView3_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.Name)
            {
                case "colReadAccess":
                case "colCreateAccess":
                case "colUpdateAccess":
                case "colDeleteAccess":
                    // Update DB with changes... //
                    Boolean boolValue = Convert.ToBoolean(e.Value);
                    using (DataSet_GroupsTableAdapters.QueriesTableAdapter UpdateDatabase = new DataSet_GroupsTableAdapters.QueriesTableAdapter())
                    {
                        // Add the selected staff to the selected groups //
                        UpdateDatabase.ChangeConnectionString(strConnectionString);
                        UpdateDatabase.sp00126_permission_manager_update_access(view.FocusedColumn.Name.ToLower().Replace("col", ""), Convert.ToInt32(view.GetFocusedRowCellValue("ProgramAccessID")), boolValue);
                    }
                    break;
                default:
                    break;
            }


        }

  




    }
}


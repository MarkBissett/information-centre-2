using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.LookAndFeel;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Drawing;

namespace WoodPlan5
{
    public partial class frm_Core_Select_Site : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;

        public int intOriginalClientID = 0;
        public int intOriginalSiteID = 0;
        public string strSelectedValue = "";
        public int intSelectedID = 0;
        public string strSelectedClientName = "";
        public int intSelectedClientID = 0;
        public string strSelectedSiteCode = "";

        public int intFilterActiveClient = 0;
        public int intFilterUtilityArbClient = 0;
        public int intFilterSummerMaintenanceClient = 0;
        public int intWinterMaintenanceClient = 0;
        public int intAmenityArbClient = 0;

        #endregion

        public frm_Core_Select_Site()
        {
            InitializeComponent();
        }

        private void frm_Core_Select_Site_Load(object sender, EventArgs e)
        {
            fProgress = new frmProgress(10);
            this.AddOwnedForm(fProgress);
            fProgress.Show();  // ***** Closed in PostOpen event ***** //
            Application.DoEvents();

            this.FormID = 30022;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            sp03055_EP_Select_Sites_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            gridControl1.ForceInitialize();
            gridControl2.ForceInitialize();

            try
            {
                sp03054_EP_Select_Client_ListTableAdapter.Connection.ConnectionString = strConnectionString;
                sp03054_EP_Select_Client_ListTableAdapter.Fill(dataSet_EP.sp03054_EP_Select_Client_List, intFilterActiveClient, intFilterUtilityArbClient, intFilterSummerMaintenanceClient, intWinterMaintenanceClient, intAmenityArbClient);
            }
            catch (Exception)
            {
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the client list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
            
            if (intOriginalClientID != 0)  // Client selected so try to find and highlight //
            {
                GridView view = (GridView)gridControl1.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["ClientID"], intOriginalClientID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
       }

        public override void PostLoadView(object objParameter)
        {
        }


        bool internalRowFocusing;

        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available";
                    break;
                case "gridView2":
                    message = "No Sites Available For Selection - Select a Client to see Related Sites";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["ClientID"])) + ',';
            }

            //Populate Linked Records //
            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_EP.sp03055_EP_Select_Sites_List.Clear();
            }
            else
            {
                try
                {
                    sp03055_EP_Select_Sites_ListTableAdapter.Fill(dataSet_EP.sp03055_EP_Select_Sites_List, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related sites.\n\nTry selecting a client again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();

            if (intOriginalSiteID != 0)  // Site selected so try to find and highlight //
            {
                view = (GridView)gridControl2.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["SiteID"], intOriginalSiteID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                    intOriginalSiteID = 0;  // Prevent this code firing again [so that it only fires the once when the form opens //
                }
            }

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (strSelectedValue == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl2.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                strSelectedValue = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "SiteName"));
                intSelectedID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteID"));
                strSelectedClientName = Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"));
                intSelectedClientID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientID"));
                strSelectedSiteCode = view.GetRowCellValue(view.FocusedRowHandle, "SiteCode").ToString();
            }
        }



    }
}


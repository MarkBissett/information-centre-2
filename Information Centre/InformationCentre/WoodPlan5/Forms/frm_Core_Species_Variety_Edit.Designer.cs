namespace WoodPlan5
{
    partial class frm_Core_Species_Variety_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Species_Variety_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.colSpeciesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.sp00175SpeciesVarietyItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.VarietyIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.VarietyNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SpeciesIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00179SpeciesListWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSpeciesCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesRiskFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesScientificName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeciesType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmenityArbVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArbVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ItemForVarietyID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSpeciesID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVarietyName = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00175_Species_Variety_ItemTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00175_Species_Variety_ItemTableAdapter();
            this.sp00179_Species_List_With_BlankTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00179_Species_List_With_BlankTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sp00175SpeciesVarietyItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VarietyIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VarietyNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeciesIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00179SpeciesListWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVarietyID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpeciesID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVarietyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 176);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 150);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 150);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colSpeciesID
            // 
            this.colSpeciesID.Caption = "Species ID";
            this.colSpeciesID.FieldName = "SpeciesID";
            this.colSpeciesID.Name = "colSpeciesID";
            this.colSpeciesID.OptionsColumn.AllowEdit = false;
            this.colSpeciesID.OptionsColumn.AllowFocus = false;
            this.colSpeciesID.OptionsColumn.ReadOnly = true;
            this.colSpeciesID.Width = 71;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 176);
            this.barDockControl2.Size = new System.Drawing.Size(628, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 150);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 150);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.VarietyIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.VarietyNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SpeciesIDGridLookUpEdit);
            this.dataLayoutControl1.DataSource = this.sp00175SpeciesVarietyItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForVarietyID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 150);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00175SpeciesVarietyItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(79, 7);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(180, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 7;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // sp00175SpeciesVarietyItemBindingSource
            // 
            this.sp00175SpeciesVarietyItemBindingSource.DataMember = "sp00175_Species_Variety_Item";
            this.sp00175SpeciesVarietyItemBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // VarietyIDTextEdit
            // 
            this.VarietyIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00175SpeciesVarietyItemBindingSource, "VarietyID", true));
            this.VarietyIDTextEdit.Location = new System.Drawing.Point(84, 35);
            this.VarietyIDTextEdit.MenuManager = this.barManager1;
            this.VarietyIDTextEdit.Name = "VarietyIDTextEdit";
            this.VarietyIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VarietyIDTextEdit, true);
            this.VarietyIDTextEdit.Size = new System.Drawing.Size(532, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VarietyIDTextEdit, optionsSpelling1);
            this.VarietyIDTextEdit.StyleController = this.dataLayoutControl1;
            this.VarietyIDTextEdit.TabIndex = 4;
            // 
            // VarietyNameTextEdit
            // 
            this.VarietyNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00175SpeciesVarietyItemBindingSource, "VarietyName", true));
            this.VarietyNameTextEdit.Location = new System.Drawing.Point(78, 54);
            this.VarietyNameTextEdit.MenuManager = this.barManager1;
            this.VarietyNameTextEdit.Name = "VarietyNameTextEdit";
            this.VarietyNameTextEdit.Properties.MaxLength = 50;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VarietyNameTextEdit, true);
            this.VarietyNameTextEdit.Size = new System.Drawing.Size(543, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VarietyNameTextEdit, optionsSpelling2);
            this.VarietyNameTextEdit.StyleController = this.dataLayoutControl1;
            this.VarietyNameTextEdit.TabIndex = 6;
            this.VarietyNameTextEdit.Validating += new System.ComponentModel.CancelEventHandler(this.VarietyNameTextEdit_Validating);
            // 
            // SpeciesIDGridLookUpEdit
            // 
            this.SpeciesIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00175SpeciesVarietyItemBindingSource, "SpeciesID", true));
            this.SpeciesIDGridLookUpEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.SpeciesIDGridLookUpEdit.Location = new System.Drawing.Point(78, 30);
            this.SpeciesIDGridLookUpEdit.MenuManager = this.barManager1;
            this.SpeciesIDGridLookUpEdit.Name = "SpeciesIDGridLookUpEdit";
            this.SpeciesIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SpeciesIDGridLookUpEdit.Properties.DataSource = this.sp00179SpeciesListWithBlankBindingSource;
            this.SpeciesIDGridLookUpEdit.Properties.DisplayMember = "SpeciesName";
            this.SpeciesIDGridLookUpEdit.Properties.NullText = "";
            this.SpeciesIDGridLookUpEdit.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1});
            this.SpeciesIDGridLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SpeciesIDGridLookUpEdit.Properties.ValueMember = "SpeciesID";
            this.SpeciesIDGridLookUpEdit.Properties.View = this.gridLookUpEdit1View;
            this.SpeciesIDGridLookUpEdit.Size = new System.Drawing.Size(543, 20);
            this.SpeciesIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SpeciesIDGridLookUpEdit.TabIndex = 5;
            this.SpeciesIDGridLookUpEdit.TabStop = false;
            this.SpeciesIDGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.SpeciesIDGridLookUpEdit_Validating);
            // 
            // sp00179SpeciesListWithBlankBindingSource
            // 
            this.sp00179SpeciesListWithBlankBindingSource.DataMember = "sp00179_Species_List_With_Blank";
            this.sp00179SpeciesListWithBlankBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSpeciesCode,
            this.colSpeciesID,
            this.colSpeciesName,
            this.colSpeciesOrder,
            this.colSpeciesRemark,
            this.colSpeciesRiskFactor,
            this.colSpeciesScientificName,
            this.colSpeciesType,
            this.colAmenityArbVisible,
            this.colUtilityArbVisible});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colSpeciesID;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridLookUpEdit1View.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridLookUpEdit1View.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridLookUpEdit1View.OptionsLayout.StoreAppearance = true;
            this.gridLookUpEdit1View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridLookUpEdit1View.OptionsView.ColumnAutoWidth = false;
            this.gridLookUpEdit1View.OptionsView.EnableAppearanceEvenRow = true;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            this.gridLookUpEdit1View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridLookUpEdit1View.OptionsView.ShowIndicator = false;
            this.gridLookUpEdit1View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpeciesOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSpeciesName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colSpeciesCode
            // 
            this.colSpeciesCode.Caption = "Code";
            this.colSpeciesCode.FieldName = "SpeciesCode";
            this.colSpeciesCode.Name = "colSpeciesCode";
            this.colSpeciesCode.OptionsColumn.AllowEdit = false;
            this.colSpeciesCode.OptionsColumn.AllowFocus = false;
            this.colSpeciesCode.OptionsColumn.ReadOnly = true;
            this.colSpeciesCode.Width = 147;
            // 
            // colSpeciesName
            // 
            this.colSpeciesName.Caption = "Name";
            this.colSpeciesName.FieldName = "SpeciesName";
            this.colSpeciesName.Name = "colSpeciesName";
            this.colSpeciesName.OptionsColumn.AllowEdit = false;
            this.colSpeciesName.OptionsColumn.AllowFocus = false;
            this.colSpeciesName.OptionsColumn.ReadOnly = true;
            this.colSpeciesName.Visible = true;
            this.colSpeciesName.VisibleIndex = 0;
            this.colSpeciesName.Width = 212;
            // 
            // colSpeciesOrder
            // 
            this.colSpeciesOrder.Caption = "Order";
            this.colSpeciesOrder.FieldName = "SpeciesOrder";
            this.colSpeciesOrder.Name = "colSpeciesOrder";
            this.colSpeciesOrder.OptionsColumn.AllowEdit = false;
            this.colSpeciesOrder.OptionsColumn.AllowFocus = false;
            this.colSpeciesOrder.OptionsColumn.ReadOnly = true;
            this.colSpeciesOrder.Width = 62;
            // 
            // colSpeciesRemark
            // 
            this.colSpeciesRemark.Caption = "Remarks";
            this.colSpeciesRemark.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSpeciesRemark.FieldName = "SpeciesRemark";
            this.colSpeciesRemark.Name = "colSpeciesRemark";
            this.colSpeciesRemark.OptionsColumn.ReadOnly = true;
            this.colSpeciesRemark.Visible = true;
            this.colSpeciesRemark.VisibleIndex = 4;
            this.colSpeciesRemark.Width = 143;
            // 
            // colSpeciesRiskFactor
            // 
            this.colSpeciesRiskFactor.Caption = "Risk Factor";
            this.colSpeciesRiskFactor.FieldName = "SpeciesRiskFactor";
            this.colSpeciesRiskFactor.Name = "colSpeciesRiskFactor";
            this.colSpeciesRiskFactor.OptionsColumn.AllowEdit = false;
            this.colSpeciesRiskFactor.OptionsColumn.AllowFocus = false;
            this.colSpeciesRiskFactor.OptionsColumn.ReadOnly = true;
            this.colSpeciesRiskFactor.Width = 74;
            // 
            // colSpeciesScientificName
            // 
            this.colSpeciesScientificName.Caption = "Scientific Name";
            this.colSpeciesScientificName.FieldName = "SpeciesScientificName";
            this.colSpeciesScientificName.Name = "colSpeciesScientificName";
            this.colSpeciesScientificName.OptionsColumn.AllowEdit = false;
            this.colSpeciesScientificName.OptionsColumn.AllowFocus = false;
            this.colSpeciesScientificName.OptionsColumn.ReadOnly = true;
            this.colSpeciesScientificName.Visible = true;
            this.colSpeciesScientificName.VisibleIndex = 1;
            this.colSpeciesScientificName.Width = 205;
            // 
            // colSpeciesType
            // 
            this.colSpeciesType.Caption = "Type";
            this.colSpeciesType.FieldName = "SpeciesType";
            this.colSpeciesType.Name = "colSpeciesType";
            this.colSpeciesType.OptionsColumn.AllowEdit = false;
            this.colSpeciesType.OptionsColumn.AllowFocus = false;
            this.colSpeciesType.OptionsColumn.ReadOnly = true;
            this.colSpeciesType.Width = 101;
            // 
            // colAmenityArbVisible
            // 
            this.colAmenityArbVisible.Caption = "Amenity Arb";
            this.colAmenityArbVisible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colAmenityArbVisible.FieldName = "AmenityArbVisible";
            this.colAmenityArbVisible.Name = "colAmenityArbVisible";
            this.colAmenityArbVisible.OptionsColumn.AllowEdit = false;
            this.colAmenityArbVisible.OptionsColumn.AllowFocus = false;
            this.colAmenityArbVisible.OptionsColumn.ReadOnly = true;
            this.colAmenityArbVisible.Visible = true;
            this.colAmenityArbVisible.VisibleIndex = 2;
            this.colAmenityArbVisible.Width = 78;
            // 
            // colUtilityArbVisible
            // 
            this.colUtilityArbVisible.Caption = "Utility Arb";
            this.colUtilityArbVisible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colUtilityArbVisible.FieldName = "UtilityArbVisible";
            this.colUtilityArbVisible.Name = "colUtilityArbVisible";
            this.colUtilityArbVisible.OptionsColumn.AllowEdit = false;
            this.colUtilityArbVisible.OptionsColumn.AllowFocus = false;
            this.colUtilityArbVisible.OptionsColumn.ReadOnly = true;
            this.colUtilityArbVisible.Visible = true;
            this.colUtilityArbVisible.VisibleIndex = 3;
            this.colUtilityArbVisible.Width = 66;
            // 
            // ItemForVarietyID
            // 
            this.ItemForVarietyID.Control = this.VarietyIDTextEdit;
            this.ItemForVarietyID.CustomizationFormText = "Variety ID:";
            this.ItemForVarietyID.Location = new System.Drawing.Point(0, 23);
            this.ItemForVarietyID.Name = "ItemForVarietyID";
            this.ItemForVarietyID.Size = new System.Drawing.Size(608, 24);
            this.ItemForVarietyID.Text = "Variety ID:";
            this.ItemForVarietyID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 150);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSpeciesID,
            this.ItemForVarietyName,
            this.layoutControlItem1,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(618, 140);
            // 
            // ItemForSpeciesID
            // 
            this.ItemForSpeciesID.Control = this.SpeciesIDGridLookUpEdit;
            this.ItemForSpeciesID.CustomizationFormText = "Species:";
            this.ItemForSpeciesID.Location = new System.Drawing.Point(0, 23);
            this.ItemForSpeciesID.Name = "ItemForSpeciesID";
            this.ItemForSpeciesID.Size = new System.Drawing.Size(618, 24);
            this.ItemForSpeciesID.Text = "Species:";
            this.ItemForSpeciesID.TextSize = new System.Drawing.Size(68, 13);
            // 
            // ItemForVarietyName
            // 
            this.ItemForVarietyName.Control = this.VarietyNameTextEdit;
            this.ItemForVarietyName.CustomizationFormText = "Variety Name:";
            this.ItemForVarietyName.Location = new System.Drawing.Point(0, 47);
            this.ItemForVarietyName.Name = "ItemForVarietyName";
            this.ItemForVarietyName.Size = new System.Drawing.Size(618, 93);
            this.ItemForVarietyName.Text = "Variety Name:";
            this.ItemForVarietyName.TextSize = new System.Drawing.Size(68, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(72, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(184, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(72, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(72, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(72, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(256, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(362, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00175_Species_Variety_ItemTableAdapter
            // 
            this.sp00175_Species_Variety_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp00179_Species_List_With_BlankTableAdapter
            // 
            this.sp00179_Species_List_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Species_Variety_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 204);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Species_Variety_Edit";
            this.Text = "Edit Species Variety";
            this.Activated += new System.EventHandler(this.frm_Core_Species_Variety_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Species_Variety_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Species_Variety_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sp00175SpeciesVarietyItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VarietyIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VarietyNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeciesIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00179SpeciesListWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVarietyID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSpeciesID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVarietyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private System.Windows.Forms.BindingSource sp00175SpeciesVarietyItemBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00175_Species_Variety_ItemTableAdapter sp00175_Species_Variety_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit VarietyIDTextEdit;
        private DevExpress.XtraEditors.TextEdit VarietyNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVarietyID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSpeciesID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVarietyName;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.GridLookUpEdit SpeciesIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private System.Windows.Forms.BindingSource sp00179SpeciesListWithBlankBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00179_Species_List_With_BlankTableAdapter sp00179_Species_List_With_BlankTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesID;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesRemark;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesRiskFactor;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesScientificName;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeciesType;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAmenityArbVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArbVisible;
    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Accident_Injury_Select : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public int _OriginalAccidentID = 0;
        public string _OriginalAccidentIDs = "";
        public int _OriginalInjuryID = 0;
        public string _OriginalInjuryIDs = "";
        public int _SelectedAccidentID = 0;
        public string _SelectedAccidentIDs = "";
        public string _SelectedAccidentDescription = "";
        public int _SelectedInjuryID = 0;
        public string _SelectedInjuryIDs = "";
        public string _SelectedInjuryDescription = "";
        public string _FullSelectedInjuryDescription = "";

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;

        #endregion

        public frm_Core_Accident_Injury_Select()
        {
            InitializeComponent();
        }

        private void frm_Core_Accident_Injury_Select_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500214;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp00314_Accident_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00321_Select_InjuryTableAdapter.Connection.ConnectionString = strConnectionString;

            GridView view = (GridView)gridControl1.MainView;
            gridControl1.ForceInitialize();
            gridControl2.ForceInitialize();

            LoadData();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;
                
                // Add record selection checkboxes to popup grid control //
                selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
                selection2.CheckMarkColumn.VisibleIndex = 0;
                selection2.CheckMarkColumn.Width = 30;

                Array arrayRecords = _OriginalAccidentIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["AccidentID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();

                view = (GridView)gridControl2.MainView;
                arrayRecords = _OriginalInjuryIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["AccidentInjuryID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();

            }
            else  // Single selection mode //
            {
                if (_OriginalAccidentID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["AccidentID"], _OriginalAccidentID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                    if (_OriginalInjuryID != 0)  // Injury selected so try to find and highlight //
                    {
                        view = (GridView)gridControl2.MainView;
                        intFoundRow = view.LocateByValue(0, view.Columns["AccidentInjuryID"], _OriginalInjuryID);
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.FocusedRowHandle = intFoundRow;
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            int intActive = Convert.ToInt32(barEditItemActive.EditValue);

            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp00314_Accident_SelectTableAdapter.Fill(dataSet_Accident.sp00314_Accident_Select, intActive);
            view.EndUpdate();
        }

        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["AccidentID"])) + ',';
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_Accident.sp00321_Select_Injury.Clear();
            }
            else
            {
                try
                {
                    sp00321_Select_InjuryTableAdapter.Fill(dataSet_Accident.sp00321_Select_Injury, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Injuries.\n\nTry selecting an Accident again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Accidents Available";
                    break;
                case "gridView2":
                    message = "No Injuries Available - Select an Accident to view linked Injuries";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        bool internalRowFocusing;


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(_SelectedInjuryIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (_SelectedInjuryID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (_Mode != "single")
            {
                _SelectedAccidentIDs = "";    // Reset any prior values first //
                _SelectedAccidentDescription = "";  // Reset any prior values first //
                if (selection1.SelectedCount <= 0) return;
                if (selection2.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        _SelectedAccidentIDs += Convert.ToString(view.GetRowCellValue(i, "AccidentID")) + ",";
                        if (intCount == 0)
                        {
                            _SelectedAccidentDescription = Convert.ToString(view.GetRowCellValue(i, "AccidentType")) + " - " + Convert.ToDateTime(view.GetRowCellValue(i, "AccidentDate")).ToString("dd/MM/yyyy HH:mm");
                        }
                        else if (intCount >= 1)
                        {
                            _SelectedAccidentDescription += ", " + Convert.ToString(view.GetRowCellValue(i, "Description")) + " - " + Convert.ToDateTime(view.GetRowCellValue(i, "AccidentDate")).ToString("dd/MM/yyyy HH:mm");
                        }
                        intCount++;
                    }
                }
                view = (GridView)gridControl2.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        _SelectedInjuryIDs += Convert.ToString(view.GetRowCellValue(i, "InjuryID")) + ",";
                        if (intCount == 0)
                        {
                            _SelectedInjuryDescription = Convert.ToString(view.GetRowCellValue(i, "AccidentBodyArea")) + " - " + Convert.ToString(view.GetRowCellValue(i, "NatureOfInjury"));
                        }
                        else if (intCount >= 1)
                        {
                            _SelectedInjuryDescription += ", " + Convert.ToString(view.GetRowCellValue(i, "AccidentBodyArea")) + " - " + Convert.ToString(view.GetRowCellValue(i, "NatureOfInjury"));
                        }
                        intCount++;
                    }
                }
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    var currentRowView = (DataRowView)sp00314AccidentSelectBindingSource.Current;
                    var currentRow = (DataSet_Accident.sp00314_Accident_SelectRow)currentRowView.Row;
                    if (currentRow == null) return;
                    _SelectedAccidentID = (string.IsNullOrEmpty(currentRow.AccidentID.ToString()) ? 0 : currentRow.AccidentID);
                    string strDate = "Unknown Date";
                    try { strDate = Convert.ToDateTime(currentRow.AccidentDate).ToString("dd/MM/yyyy HH:mm"); }
                    catch (Exception) { }
                    _SelectedAccidentDescription = (string.IsNullOrEmpty(currentRow.AccidentType.ToString()) ? "" : currentRow.AccidentType) + " - " + strDate;

                    var currentRowView2 = (DataRowView)sp00321SelectInjuryBindingSource.Current;
                    var currentRow2 = (DataSet_Accident.sp00321_Select_InjuryRow)currentRowView2.Row;
                    if (currentRow2 == null) return;
                    _SelectedInjuryID = (string.IsNullOrEmpty(currentRow2.AccidentInjuryID.ToString()) ? 0 : currentRow2.AccidentInjuryID);
                    _SelectedInjuryDescription = " Body Area: " + (string.IsNullOrEmpty(currentRow2.AccidentBodyArea.ToString()) ? "" : currentRow2.AccidentBodyArea) + ", Injury Type: " + (string.IsNullOrEmpty(currentRow2.NatureOfInjury.ToString()) ? "" : currentRow2.NatureOfInjury);
                    _FullSelectedInjuryDescription = _SelectedAccidentDescription + ", " + _SelectedInjuryDescription;
                }
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }



    }
}


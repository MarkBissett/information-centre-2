namespace WoodPlan5
{
    partial class frm_Core_Accident_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Accident_Manager));
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControlAccident = new DevExpress.XtraGrid.GridControl();
            this.sp00301AccidentManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Accident = new WoodPlan5.DataSet_Accident();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridViewAccident = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAccidentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAccidentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPersonOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedToHSE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateReportedToHSE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colExternalReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientReferenceNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSituationRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleManagerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHSQETeamLead = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCorrectiveActionRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTargetClosureDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentSubType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReportedByPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colResponsibleManager = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordFullDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContract = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedRecord = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colLinkedToRecordType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedInjuries = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHSQETeamLeadName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.popupContainerControlBusinessAreaFilter = new DevExpress.XtraEditors.PopupContainerControl();
            this.btnBusinessAreaFilterOK = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00302AccidentBusinessAreasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBusinessAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBusinessAreaName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit17 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.popupContainerControlDateRange = new DevExpress.XtraEditors.PopupContainerControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.btnDateRangeOK = new DevExpress.XtraEditors.SimpleButton();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageInjuries = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlInjury = new DevExpress.XtraGrid.GridControl();
            this.sp00309InjuriesLinkedToAccidentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewInjury = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAccidentInjuryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonOther = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentBodyAreaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNatureOfInjuryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToAccident = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAccidentBodyArea = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNatureOfInjury = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPagePictures = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlPicture = new DevExpress.XtraGrid.GridControl();
            this.sp00318PicturesLinkedToAccidentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewPicture = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAccidentPictureID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateTimeTaken = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPicturePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToAccident2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentType3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentDate3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPageComments = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlComments = new DevExpress.XtraGrid.GridControl();
            this.sp00315CommentsLinkedToAccidentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewComments = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAccidentCommentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateRecorded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime5 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToAccident1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentType2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordedByPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barEditItemBusinessArea = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditBusinessAreaFilter = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.barEditItemDateRange = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemPopupContainerEditDateRange = new DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.toolTipController1 = new DevExpress.Utils.ToolTipController(this.components);
            this.imageCollectionTooltips = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.toggleRecurrenceItem1 = new DevExpress.XtraScheduler.UI.ToggleRecurrenceItem();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageAccidents = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabPagePicklists = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl3 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSubType = new DevExpress.XtraGrid.GridControl();
            this.sp00322AccidentSubTypesManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewSubType = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAccidentSubTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccidentTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecordOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colAccidentType4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlBodyArea = new DevExpress.XtraGrid.GridControl();
            this.sp00325AccidentBodyAreasManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewBodyArea = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlInjuryType = new DevExpress.XtraGrid.GridControl();
            this.sp00328AccidentInjuryTypesManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewInjuryType = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp00302_Accident_Business_AreasTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00302_Accident_Business_AreasTableAdapter();
            this.sp00301_Accident_ManagerTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00301_Accident_ManagerTableAdapter();
            this.sp00309_Injuries_Linked_To_AccidentsTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00309_Injuries_Linked_To_AccidentsTableAdapter();
            this.sp00315_Comments_Linked_To_AccidentsTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00315_Comments_Linked_To_AccidentsTableAdapter();
            this.sp00318_Pictures_Linked_To_AccidentsTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00318_Pictures_Linked_To_AccidentsTableAdapter();
            this.sp00322_Accident_Sub_Types_ManagerTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00322_Accident_Sub_Types_ManagerTableAdapter();
            this.sp00325_Accident_Body_Areas_ManagerTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00325_Accident_Body_Areas_ManagerTableAdapter();
            this.sp00328_Accident_Injury_Types_ManagerTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00328_Accident_Injury_Types_ManagerTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAccident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00301AccidentManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedRecord)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlBusinessAreaFilter)).BeginInit();
            this.popupContainerControlBusinessAreaFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00302AccidentBusinessAreasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).BeginInit();
            this.popupContainerControlDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPageInjuries.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInjury)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00309InjuriesLinkedToAccidentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInjury)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).BeginInit();
            this.xtraTabPagePictures.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00318PicturesLinkedToAccidentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            this.xtraTabPageComments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00315CommentsLinkedToAccidentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewComments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditBusinessAreaFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionTooltips)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageAccidents.SuspendLayout();
            this.xtraTabPagePicklists.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).BeginInit();
            this.xtraTabControl3.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00322AccidentSubTypesManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBodyArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00325AccidentBodyAreasManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBodyArea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInjuryType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00328AccidentInjuryTypesManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInjuryType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1286, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 564);
            this.barDockControlBottom.Size = new System.Drawing.Size(1286, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 564);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1286, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 564);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barEditItemDateRange,
            this.bbiRefresh,
            this.barEditItemBusinessArea});
            this.barManager1.MaxItemId = 118;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPopupContainerEditDateRange,
            this.repositoryItemDuration1,
            this.repositoryItemSpinEdit1,
            this.repositoryItemPopupContainerEditBusinessAreaFilter});
            this.barManager1.HighlightedLinkChanged += new DevExpress.XtraBars.HighlightedLinkChangedEventHandler(this.barManager1_HighlightedLinkChanged);
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControlAccident
            // 
            this.gridControlAccident.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControlAccident.DataSource = this.sp00301AccidentManagerBindingSource;
            this.gridControlAccident.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlAccident.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlAccident.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlAccident.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document")});
            this.gridControlAccident.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAccident_EmbeddedNavigator_ButtonClick);
            this.gridControlAccident.Location = new System.Drawing.Point(0, 43);
            this.gridControlAccident.MainView = this.gridViewAccident;
            this.gridControlAccident.MenuManager = this.barManager1;
            this.gridControlAccident.Name = "gridControlAccident";
            this.gridControlAccident.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEditLinkedRecord});
            this.gridControlAccident.Size = new System.Drawing.Size(1277, 240);
            this.gridControlAccident.TabIndex = 4;
            this.gridControlAccident.UseEmbeddedNavigator = true;
            this.gridControlAccident.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAccident});
            // 
            // sp00301AccidentManagerBindingSource
            // 
            this.sp00301AccidentManagerBindingSource.DataMember = "sp00301_Accident_Manager";
            this.sp00301AccidentManagerBindingSource.DataSource = this.dataSet_Accident;
            // 
            // dataSet_Accident
            // 
            this.dataSet_Accident.DataSetName = "DataSet_Accident";
            this.dataSet_Accident.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.Images.SetKeyName(6, "Scheduler_16x16.png");
            this.imageCollection1.Images.SetKeyName(7, "Preview_16x16.png");
            this.imageCollection1.Images.SetKeyName(8, "linked_documents_16_16.png");
            this.imageCollection1.Images.SetKeyName(9, "BlockAdd_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 10);
            this.imageCollection1.Images.SetKeyName(10, "refresh_16x16.png");
            // 
            // gridViewAccident
            // 
            this.gridViewAccident.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAccidentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colBusinessAreaID1,
            this.colAccidentDate,
            this.colAccidentTypeID,
            this.colAccidentSubTypeID,
            this.colAccidentStatusID,
            this.colReportedByPersonID,
            this.colReportedByPersonTypeID,
            this.colReportedByPersonOther,
            this.colReportedToHSE,
            this.colDateReportedToHSE,
            this.colAccidentDescription,
            this.colExternalReferenceNumber,
            this.colClientReferenceNumber,
            this.colSituationRemarks,
            this.colResponsibleManagerID,
            this.colHSQETeamLead,
            this.colCorrectiveActionRemarks,
            this.colTargetClosureDate,
            this.colClosedDate,
            this.colClosedByStaffID,
            this.colRemarks,
            this.colBusinessAreaName1,
            this.colAccidentType,
            this.colAccidentSubType,
            this.colAccidentStatus,
            this.colReportedByPerson,
            this.colReportedByPersonType,
            this.colResponsibleManager,
            this.colLinkedToRecordFullDescription,
            this.colClientName,
            this.colClientContract,
            this.colSiteName,
            this.colLinkedToRecord,
            this.colLinkedToRecordType,
            this.colClosedByStaff,
            this.colLinkedInjuries,
            this.colHSQETeamLeadName});
            this.gridViewAccident.GridControl = this.gridControlAccident;
            this.gridViewAccident.Name = "gridViewAccident";
            this.gridViewAccident.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewAccident.OptionsFind.AlwaysVisible = true;
            this.gridViewAccident.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewAccident.OptionsLayout.StoreAppearance = true;
            this.gridViewAccident.OptionsLayout.StoreFormatRules = true;
            this.gridViewAccident.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewAccident.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewAccident.OptionsSelection.MultiSelect = true;
            this.gridViewAccident.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewAccident.OptionsView.ColumnAutoWidth = false;
            this.gridViewAccident.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewAccident.OptionsView.ShowGroupPanel = false;
            this.gridViewAccident.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewAccident_CustomDrawCell);
            this.gridViewAccident.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewAccident_CustomRowCellEdit);
            this.gridViewAccident.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewAccident.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewAccident.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewAccident.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewAccident_ShowingEditor);
            this.gridViewAccident.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewAccident.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewAccident.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewAccident_MouseUp);
            this.gridViewAccident.DoubleClick += new System.EventHandler(this.gridViewAccident_DoubleClick);
            this.gridViewAccident.GotFocus += new System.EventHandler(this.gridViewAccident_GotFocus);
            // 
            // colAccidentID
            // 
            this.colAccidentID.Caption = "Accident ID";
            this.colAccidentID.FieldName = "AccidentID";
            this.colAccidentID.Name = "colAccidentID";
            this.colAccidentID.OptionsColumn.AllowEdit = false;
            this.colAccidentID.OptionsColumn.AllowFocus = false;
            this.colAccidentID.OptionsColumn.ReadOnly = true;
            this.colAccidentID.Width = 76;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked To Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 117;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 144;
            // 
            // colBusinessAreaID1
            // 
            this.colBusinessAreaID1.Caption = "Business Area ID";
            this.colBusinessAreaID1.FieldName = "BusinessAreaID";
            this.colBusinessAreaID1.Name = "colBusinessAreaID1";
            this.colBusinessAreaID1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID1.Width = 102;
            // 
            // colAccidentDate
            // 
            this.colAccidentDate.Caption = "Accident Date";
            this.colAccidentDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colAccidentDate.FieldName = "AccidentDate";
            this.colAccidentDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colAccidentDate.Name = "colAccidentDate";
            this.colAccidentDate.OptionsColumn.AllowEdit = false;
            this.colAccidentDate.OptionsColumn.AllowFocus = false;
            this.colAccidentDate.OptionsColumn.ReadOnly = true;
            this.colAccidentDate.Visible = true;
            this.colAccidentDate.VisibleIndex = 0;
            this.colAccidentDate.Width = 100;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colAccidentTypeID
            // 
            this.colAccidentTypeID.Caption = "Type ID";
            this.colAccidentTypeID.FieldName = "AccidentTypeID";
            this.colAccidentTypeID.Name = "colAccidentTypeID";
            this.colAccidentTypeID.OptionsColumn.AllowEdit = false;
            this.colAccidentTypeID.OptionsColumn.AllowFocus = false;
            this.colAccidentTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colAccidentSubTypeID
            // 
            this.colAccidentSubTypeID.Caption = "Sub-Type ID";
            this.colAccidentSubTypeID.FieldName = "AccidentSubTypeID";
            this.colAccidentSubTypeID.Name = "colAccidentSubTypeID";
            this.colAccidentSubTypeID.OptionsColumn.AllowEdit = false;
            this.colAccidentSubTypeID.OptionsColumn.AllowFocus = false;
            this.colAccidentSubTypeID.OptionsColumn.ReadOnly = true;
            this.colAccidentSubTypeID.Width = 81;
            // 
            // colAccidentStatusID
            // 
            this.colAccidentStatusID.Caption = "Status ID";
            this.colAccidentStatusID.FieldName = "AccidentStatusID";
            this.colAccidentStatusID.Name = "colAccidentStatusID";
            this.colAccidentStatusID.OptionsColumn.AllowEdit = false;
            this.colAccidentStatusID.OptionsColumn.AllowFocus = false;
            this.colAccidentStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colReportedByPersonID
            // 
            this.colReportedByPersonID.Caption = "Reported By Person ID";
            this.colReportedByPersonID.FieldName = "ReportedByPersonID";
            this.colReportedByPersonID.Name = "colReportedByPersonID";
            this.colReportedByPersonID.OptionsColumn.AllowEdit = false;
            this.colReportedByPersonID.OptionsColumn.AllowFocus = false;
            this.colReportedByPersonID.OptionsColumn.ReadOnly = true;
            this.colReportedByPersonID.Width = 131;
            // 
            // colReportedByPersonTypeID
            // 
            this.colReportedByPersonTypeID.Caption = "Reported By Person Type ID";
            this.colReportedByPersonTypeID.FieldName = "ReportedByPersonTypeID";
            this.colReportedByPersonTypeID.Name = "colReportedByPersonTypeID";
            this.colReportedByPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colReportedByPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colReportedByPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colReportedByPersonTypeID.Width = 158;
            // 
            // colReportedByPersonOther
            // 
            this.colReportedByPersonOther.Caption = "Reported By Person Other";
            this.colReportedByPersonOther.FieldName = "ReportedByPersonOther";
            this.colReportedByPersonOther.Name = "colReportedByPersonOther";
            this.colReportedByPersonOther.OptionsColumn.AllowEdit = false;
            this.colReportedByPersonOther.OptionsColumn.AllowFocus = false;
            this.colReportedByPersonOther.OptionsColumn.ReadOnly = true;
            this.colReportedByPersonOther.Width = 148;
            // 
            // colReportedToHSE
            // 
            this.colReportedToHSE.Caption = "Reported To HSE";
            this.colReportedToHSE.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReportedToHSE.FieldName = "ReportedToHSE";
            this.colReportedToHSE.Name = "colReportedToHSE";
            this.colReportedToHSE.OptionsColumn.AllowEdit = false;
            this.colReportedToHSE.OptionsColumn.AllowFocus = false;
            this.colReportedToHSE.OptionsColumn.ReadOnly = true;
            this.colReportedToHSE.Visible = true;
            this.colReportedToHSE.VisibleIndex = 16;
            this.colReportedToHSE.Width = 103;
            // 
            // colDateReportedToHSE
            // 
            this.colDateReportedToHSE.Caption = "Reported To HSE Date";
            this.colDateReportedToHSE.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateReportedToHSE.FieldName = "DateReportedToHSE";
            this.colDateReportedToHSE.Name = "colDateReportedToHSE";
            this.colDateReportedToHSE.OptionsColumn.AllowEdit = false;
            this.colDateReportedToHSE.OptionsColumn.AllowFocus = false;
            this.colDateReportedToHSE.OptionsColumn.ReadOnly = true;
            this.colDateReportedToHSE.Visible = true;
            this.colDateReportedToHSE.VisibleIndex = 15;
            this.colDateReportedToHSE.Width = 129;
            // 
            // colAccidentDescription
            // 
            this.colAccidentDescription.Caption = "Accident Description";
            this.colAccidentDescription.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colAccidentDescription.FieldName = "AccidentDescription";
            this.colAccidentDescription.Name = "colAccidentDescription";
            this.colAccidentDescription.OptionsColumn.ReadOnly = true;
            this.colAccidentDescription.Visible = true;
            this.colAccidentDescription.VisibleIndex = 5;
            this.colAccidentDescription.Width = 215;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colExternalReferenceNumber
            // 
            this.colExternalReferenceNumber.Caption = "External Reference #";
            this.colExternalReferenceNumber.FieldName = "ExternalReferenceNumber";
            this.colExternalReferenceNumber.Name = "colExternalReferenceNumber";
            this.colExternalReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colExternalReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colExternalReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colExternalReferenceNumber.Visible = true;
            this.colExternalReferenceNumber.VisibleIndex = 6;
            this.colExternalReferenceNumber.Width = 125;
            // 
            // colClientReferenceNumber
            // 
            this.colClientReferenceNumber.Caption = "Client Reference #";
            this.colClientReferenceNumber.FieldName = "ClientReferenceNumber";
            this.colClientReferenceNumber.Name = "colClientReferenceNumber";
            this.colClientReferenceNumber.OptionsColumn.AllowEdit = false;
            this.colClientReferenceNumber.OptionsColumn.AllowFocus = false;
            this.colClientReferenceNumber.OptionsColumn.ReadOnly = true;
            this.colClientReferenceNumber.Visible = true;
            this.colClientReferenceNumber.VisibleIndex = 17;
            this.colClientReferenceNumber.Width = 112;
            // 
            // colSituationRemarks
            // 
            this.colSituationRemarks.Caption = "Situation";
            this.colSituationRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSituationRemarks.FieldName = "SituationRemarks";
            this.colSituationRemarks.Name = "colSituationRemarks";
            this.colSituationRemarks.OptionsColumn.ReadOnly = true;
            this.colSituationRemarks.Visible = true;
            this.colSituationRemarks.VisibleIndex = 7;
            this.colSituationRemarks.Width = 167;
            // 
            // colResponsibleManagerID
            // 
            this.colResponsibleManagerID.Caption = "Responsible Manager ID";
            this.colResponsibleManagerID.FieldName = "ResponsibleManagerID";
            this.colResponsibleManagerID.Name = "colResponsibleManagerID";
            this.colResponsibleManagerID.OptionsColumn.AllowEdit = false;
            this.colResponsibleManagerID.OptionsColumn.AllowFocus = false;
            this.colResponsibleManagerID.OptionsColumn.ReadOnly = true;
            this.colResponsibleManagerID.Width = 137;
            // 
            // colHSQETeamLead
            // 
            this.colHSQETeamLead.Caption = "HSQE Team Lead ID";
            this.colHSQETeamLead.FieldName = "HSQETeamLead";
            this.colHSQETeamLead.Name = "colHSQETeamLead";
            this.colHSQETeamLead.OptionsColumn.AllowEdit = false;
            this.colHSQETeamLead.OptionsColumn.AllowFocus = false;
            this.colHSQETeamLead.OptionsColumn.ReadOnly = true;
            this.colHSQETeamLead.Width = 117;
            // 
            // colCorrectiveActionRemarks
            // 
            this.colCorrectiveActionRemarks.Caption = "Corrective Actions";
            this.colCorrectiveActionRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colCorrectiveActionRemarks.FieldName = "CorrectiveActionRemarks";
            this.colCorrectiveActionRemarks.Name = "colCorrectiveActionRemarks";
            this.colCorrectiveActionRemarks.OptionsColumn.ReadOnly = true;
            this.colCorrectiveActionRemarks.Visible = true;
            this.colCorrectiveActionRemarks.VisibleIndex = 8;
            this.colCorrectiveActionRemarks.Width = 174;
            // 
            // colTargetClosureDate
            // 
            this.colTargetClosureDate.Caption = "Target Closure Date";
            this.colTargetClosureDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colTargetClosureDate.FieldName = "TargetClosureDate";
            this.colTargetClosureDate.Name = "colTargetClosureDate";
            this.colTargetClosureDate.OptionsColumn.AllowEdit = false;
            this.colTargetClosureDate.OptionsColumn.AllowFocus = false;
            this.colTargetClosureDate.OptionsColumn.ReadOnly = true;
            this.colTargetClosureDate.Visible = true;
            this.colTargetClosureDate.VisibleIndex = 11;
            this.colTargetClosureDate.Width = 118;
            // 
            // colClosedDate
            // 
            this.colClosedDate.Caption = "Closed Date";
            this.colClosedDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colClosedDate.FieldName = "ClosedDate";
            this.colClosedDate.Name = "colClosedDate";
            this.colClosedDate.OptionsColumn.AllowEdit = false;
            this.colClosedDate.OptionsColumn.AllowFocus = false;
            this.colClosedDate.OptionsColumn.ReadOnly = true;
            this.colClosedDate.Visible = true;
            this.colClosedDate.VisibleIndex = 12;
            this.colClosedDate.Width = 79;
            // 
            // colClosedByStaffID
            // 
            this.colClosedByStaffID.Caption = "Closed By Staff ID";
            this.colClosedByStaffID.FieldName = "ClosedByStaffID";
            this.colClosedByStaffID.Name = "colClosedByStaffID";
            this.colClosedByStaffID.OptionsColumn.AllowEdit = false;
            this.colClosedByStaffID.OptionsColumn.AllowFocus = false;
            this.colClosedByStaffID.OptionsColumn.ReadOnly = true;
            this.colClosedByStaffID.Width = 109;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 26;
            // 
            // colBusinessAreaName1
            // 
            this.colBusinessAreaName1.Caption = "Business Area";
            this.colBusinessAreaName1.FieldName = "BusinessAreaName";
            this.colBusinessAreaName1.Name = "colBusinessAreaName1";
            this.colBusinessAreaName1.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName1.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName1.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName1.Visible = true;
            this.colBusinessAreaName1.VisibleIndex = 4;
            this.colBusinessAreaName1.Width = 117;
            // 
            // colAccidentType
            // 
            this.colAccidentType.Caption = "Type";
            this.colAccidentType.FieldName = "AccidentType";
            this.colAccidentType.Name = "colAccidentType";
            this.colAccidentType.OptionsColumn.AllowEdit = false;
            this.colAccidentType.OptionsColumn.AllowFocus = false;
            this.colAccidentType.OptionsColumn.ReadOnly = true;
            this.colAccidentType.Visible = true;
            this.colAccidentType.VisibleIndex = 1;
            this.colAccidentType.Width = 66;
            // 
            // colAccidentSubType
            // 
            this.colAccidentSubType.Caption = "Sub-Type";
            this.colAccidentSubType.FieldName = "AccidentSubType";
            this.colAccidentSubType.Name = "colAccidentSubType";
            this.colAccidentSubType.OptionsColumn.AllowEdit = false;
            this.colAccidentSubType.OptionsColumn.AllowFocus = false;
            this.colAccidentSubType.OptionsColumn.ReadOnly = true;
            this.colAccidentSubType.Visible = true;
            this.colAccidentSubType.VisibleIndex = 2;
            this.colAccidentSubType.Width = 115;
            // 
            // colAccidentStatus
            // 
            this.colAccidentStatus.Caption = "Status";
            this.colAccidentStatus.FieldName = "AccidentStatus";
            this.colAccidentStatus.Name = "colAccidentStatus";
            this.colAccidentStatus.OptionsColumn.AllowEdit = false;
            this.colAccidentStatus.OptionsColumn.AllowFocus = false;
            this.colAccidentStatus.OptionsColumn.ReadOnly = true;
            this.colAccidentStatus.Visible = true;
            this.colAccidentStatus.VisibleIndex = 3;
            this.colAccidentStatus.Width = 55;
            // 
            // colReportedByPerson
            // 
            this.colReportedByPerson.Caption = "Reported By Person";
            this.colReportedByPerson.FieldName = "ReportedByPerson";
            this.colReportedByPerson.Name = "colReportedByPerson";
            this.colReportedByPerson.OptionsColumn.AllowEdit = false;
            this.colReportedByPerson.OptionsColumn.AllowFocus = false;
            this.colReportedByPerson.OptionsColumn.ReadOnly = true;
            this.colReportedByPerson.Visible = true;
            this.colReportedByPerson.VisibleIndex = 13;
            this.colReportedByPerson.Width = 117;
            // 
            // colReportedByPersonType
            // 
            this.colReportedByPersonType.Caption = "Reported By Person Type";
            this.colReportedByPersonType.FieldName = "ReportedByPersonType";
            this.colReportedByPersonType.Name = "colReportedByPersonType";
            this.colReportedByPersonType.OptionsColumn.AllowEdit = false;
            this.colReportedByPersonType.OptionsColumn.AllowFocus = false;
            this.colReportedByPersonType.OptionsColumn.ReadOnly = true;
            this.colReportedByPersonType.Visible = true;
            this.colReportedByPersonType.VisibleIndex = 14;
            this.colReportedByPersonType.Width = 144;
            // 
            // colResponsibleManager
            // 
            this.colResponsibleManager.Caption = "Responsible Manager";
            this.colResponsibleManager.FieldName = "ResponsibleManager";
            this.colResponsibleManager.Name = "colResponsibleManager";
            this.colResponsibleManager.OptionsColumn.AllowEdit = false;
            this.colResponsibleManager.OptionsColumn.AllowFocus = false;
            this.colResponsibleManager.OptionsColumn.ReadOnly = true;
            this.colResponsibleManager.Visible = true;
            this.colResponsibleManager.VisibleIndex = 9;
            this.colResponsibleManager.Width = 123;
            // 
            // colLinkedToRecordFullDescription
            // 
            this.colLinkedToRecordFullDescription.Caption = "Linked To Record (Full)";
            this.colLinkedToRecordFullDescription.FieldName = "LinkedToRecordFullDescription";
            this.colLinkedToRecordFullDescription.Name = "colLinkedToRecordFullDescription";
            this.colLinkedToRecordFullDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordFullDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordFullDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordFullDescription.Visible = true;
            this.colLinkedToRecordFullDescription.VisibleIndex = 24;
            this.colLinkedToRecordFullDescription.Width = 161;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 18;
            this.colClientName.Width = 78;
            // 
            // colClientContract
            // 
            this.colClientContract.Caption = "Client Contract";
            this.colClientContract.FieldName = "ClientContract";
            this.colClientContract.Name = "colClientContract";
            this.colClientContract.OptionsColumn.AllowEdit = false;
            this.colClientContract.OptionsColumn.AllowFocus = false;
            this.colClientContract.OptionsColumn.ReadOnly = true;
            this.colClientContract.Visible = true;
            this.colClientContract.VisibleIndex = 19;
            this.colClientContract.Width = 93;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 20;
            // 
            // colLinkedToRecord
            // 
            this.colLinkedToRecord.Caption = "Linked To Record";
            this.colLinkedToRecord.ColumnEdit = this.repositoryItemHyperLinkEditLinkedRecord;
            this.colLinkedToRecord.FieldName = "LinkedToRecord";
            this.colLinkedToRecord.Name = "colLinkedToRecord";
            this.colLinkedToRecord.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecord.Visible = true;
            this.colLinkedToRecord.VisibleIndex = 21;
            this.colLinkedToRecord.Width = 217;
            // 
            // repositoryItemHyperLinkEditLinkedRecord
            // 
            this.repositoryItemHyperLinkEditLinkedRecord.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedRecord.Name = "repositoryItemHyperLinkEditLinkedRecord";
            this.repositoryItemHyperLinkEditLinkedRecord.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedRecord.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedRecord_OpenLink);
            // 
            // colLinkedToRecordType
            // 
            this.colLinkedToRecordType.Caption = "Linked To Record Type";
            this.colLinkedToRecordType.FieldName = "LinkedToRecordType";
            this.colLinkedToRecordType.Name = "colLinkedToRecordType";
            this.colLinkedToRecordType.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordType.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordType.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordType.Visible = true;
            this.colLinkedToRecordType.VisibleIndex = 22;
            this.colLinkedToRecordType.Width = 130;
            // 
            // colClosedByStaff
            // 
            this.colClosedByStaff.Caption = "Closed By Staff";
            this.colClosedByStaff.FieldName = "ClosedByStaff";
            this.colClosedByStaff.Name = "colClosedByStaff";
            this.colClosedByStaff.OptionsColumn.AllowEdit = false;
            this.colClosedByStaff.OptionsColumn.AllowFocus = false;
            this.colClosedByStaff.OptionsColumn.ReadOnly = true;
            this.colClosedByStaff.Visible = true;
            this.colClosedByStaff.VisibleIndex = 23;
            this.colClosedByStaff.Width = 96;
            // 
            // colLinkedInjuries
            // 
            this.colLinkedInjuries.Caption = "Injury Count";
            this.colLinkedInjuries.FieldName = "LinkedInjuries";
            this.colLinkedInjuries.Name = "colLinkedInjuries";
            this.colLinkedInjuries.OptionsColumn.AllowEdit = false;
            this.colLinkedInjuries.OptionsColumn.AllowFocus = false;
            this.colLinkedInjuries.OptionsColumn.ReadOnly = true;
            this.colLinkedInjuries.Visible = true;
            this.colLinkedInjuries.VisibleIndex = 25;
            this.colLinkedInjuries.Width = 82;
            // 
            // colHSQETeamLeadName
            // 
            this.colHSQETeamLeadName.Caption = "HSQE Team Lead";
            this.colHSQETeamLeadName.FieldName = "HSQETeamLeadName";
            this.colHSQETeamLeadName.Name = "colHSQETeamLeadName";
            this.colHSQETeamLeadName.OptionsColumn.AllowEdit = false;
            this.colHSQETeamLeadName.OptionsColumn.AllowFocus = false;
            this.colHSQETeamLeadName.OptionsColumn.ReadOnly = true;
            this.colHSQETeamLeadName.Visible = true;
            this.colHSQETeamLeadName.VisibleIndex = 10;
            this.colHSQETeamLeadName.Width = 103;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlBusinessAreaFilter);
            this.splitContainerControl1.Panel1.Controls.Add(this.popupContainerControlDateRange);
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControlAccident);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Text = "Jobs";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1281, 538);
            this.splitContainerControl1.SplitterPosition = 245;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitGroupPanelCollapsed += new DevExpress.XtraEditors.SplitGroupPanelCollapsedEventHandler(this.splitContainerControl1_SplitGroupPanelCollapsed);
            // 
            // popupContainerControlBusinessAreaFilter
            // 
            this.popupContainerControlBusinessAreaFilter.Controls.Add(this.btnBusinessAreaFilterOK);
            this.popupContainerControlBusinessAreaFilter.Controls.Add(this.gridControl2);
            this.popupContainerControlBusinessAreaFilter.Location = new System.Drawing.Point(567, 55);
            this.popupContainerControlBusinessAreaFilter.Name = "popupContainerControlBusinessAreaFilter";
            this.popupContainerControlBusinessAreaFilter.Size = new System.Drawing.Size(245, 188);
            this.popupContainerControlBusinessAreaFilter.TabIndex = 19;
            // 
            // btnBusinessAreaFilterOK
            // 
            this.btnBusinessAreaFilterOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBusinessAreaFilterOK.Location = new System.Drawing.Point(3, 165);
            this.btnBusinessAreaFilterOK.Name = "btnBusinessAreaFilterOK";
            this.btnBusinessAreaFilterOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnBusinessAreaFilterOK.Size = new System.Drawing.Size(50, 20);
            this.btnBusinessAreaFilterOK.TabIndex = 20;
            this.btnBusinessAreaFilterOK.Text = "OK";
            this.btnBusinessAreaFilterOK.Click += new System.EventHandler(this.btnBusinessAreaFilterOK_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp00302AccidentBusinessAreasBindingSource;
            this.gridControl2.Location = new System.Drawing.Point(3, 3);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit17});
            this.gridControl2.Size = new System.Drawing.Size(239, 160);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp00302AccidentBusinessAreasBindingSource
            // 
            this.sp00302AccidentBusinessAreasBindingSource.DataMember = "sp00302_Accident_Business_Areas";
            this.sp00302AccidentBusinessAreasBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBusinessAreaID,
            this.colBusinessAreaName});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colBusinessAreaName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colBusinessAreaID
            // 
            this.colBusinessAreaID.Caption = "Business Area ID";
            this.colBusinessAreaID.FieldName = "BusinessAreaID";
            this.colBusinessAreaID.Name = "colBusinessAreaID";
            this.colBusinessAreaID.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaID.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaID.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaID.Width = 102;
            // 
            // colBusinessAreaName
            // 
            this.colBusinessAreaName.Caption = "Business Area";
            this.colBusinessAreaName.FieldName = "BusinessAreaName";
            this.colBusinessAreaName.Name = "colBusinessAreaName";
            this.colBusinessAreaName.OptionsColumn.AllowEdit = false;
            this.colBusinessAreaName.OptionsColumn.AllowFocus = false;
            this.colBusinessAreaName.OptionsColumn.ReadOnly = true;
            this.colBusinessAreaName.Visible = true;
            this.colBusinessAreaName.VisibleIndex = 0;
            this.colBusinessAreaName.Width = 188;
            // 
            // repositoryItemMemoExEdit17
            // 
            this.repositoryItemMemoExEdit17.AutoHeight = false;
            this.repositoryItemMemoExEdit17.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit17.Name = "repositoryItemMemoExEdit17";
            this.repositoryItemMemoExEdit17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit17.ShowIcon = false;
            // 
            // popupContainerControlDateRange
            // 
            this.popupContainerControlDateRange.Controls.Add(this.groupControl1);
            this.popupContainerControlDateRange.Controls.Add(this.btnDateRangeOK);
            this.popupContainerControlDateRange.Location = new System.Drawing.Point(326, 140);
            this.popupContainerControlDateRange.Name = "popupContainerControlDateRange";
            this.popupContainerControlDateRange.Size = new System.Drawing.Size(200, 106);
            this.popupContainerControlDateRange.TabIndex = 17;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.dateEditFromDate);
            this.groupControl1.Controls.Add(this.dateEditToDate);
            this.groupControl1.Location = new System.Drawing.Point(3, 4);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(194, 76);
            this.groupControl1.TabIndex = 20;
            this.groupControl1.Text = "Date Range  [Expected Start Date]";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(6, 53);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 13;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(6, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(28, 13);
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditFromDate.EditValue = null;
            this.dateEditFromDate.Location = new System.Drawing.Point(40, 24);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditFromDate.TabIndex = 10;
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(40, 50);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm:ss";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(149, 20);
            this.dateEditToDate.TabIndex = 11;
            // 
            // btnDateRangeOK
            // 
            this.btnDateRangeOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDateRangeOK.Location = new System.Drawing.Point(3, 83);
            this.btnDateRangeOK.Name = "btnDateRangeOK";
            this.btnDateRangeOK.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnDateRangeOK.Size = new System.Drawing.Size(50, 20);
            this.btnDateRangeOK.TabIndex = 19;
            this.btnDateRangeOK.Text = "OK";
            this.btnDateRangeOK.Click += new System.EventHandler(this.btnDateRangeOK_Click);
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 1);
            this.standaloneBarDockControl1.Manager = this.barManager1;
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(1279, 42);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPageInjuries;
            this.xtraTabControl2.Size = new System.Drawing.Size(1281, 245);
            this.xtraTabControl2.TabIndex = 6;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageInjuries,
            this.xtraTabPagePictures,
            this.xtraTabPageComments});
            // 
            // xtraTabPageInjuries
            // 
            this.xtraTabPageInjuries.Controls.Add(this.gridControlInjury);
            this.xtraTabPageInjuries.Name = "xtraTabPageInjuries";
            this.xtraTabPageInjuries.Size = new System.Drawing.Size(1276, 219);
            this.xtraTabPageInjuries.Text = "Linked Injuries";
            // 
            // gridControlInjury
            // 
            this.gridControlInjury.DataSource = this.sp00309InjuriesLinkedToAccidentsBindingSource;
            this.gridControlInjury.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlInjury.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlInjury.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlInjury.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlInjury.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 8, true, true, "Linked Documents", "linked_document"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Reload Data", "reload")});
            this.gridControlInjury.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInjury_EmbeddedNavigator_ButtonClick);
            this.gridControlInjury.Location = new System.Drawing.Point(0, 0);
            this.gridControlInjury.MainView = this.gridViewInjury;
            this.gridControlInjury.MenuManager = this.barManager1;
            this.gridControlInjury.Name = "gridControlInjury";
            this.gridControlInjury.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemTextEditDateTime2});
            this.gridControlInjury.Size = new System.Drawing.Size(1276, 219);
            this.gridControlInjury.TabIndex = 5;
            this.gridControlInjury.UseEmbeddedNavigator = true;
            this.gridControlInjury.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInjury});
            // 
            // sp00309InjuriesLinkedToAccidentsBindingSource
            // 
            this.sp00309InjuriesLinkedToAccidentsBindingSource.DataMember = "sp00309_Injuries_Linked_To_Accidents";
            this.sp00309InjuriesLinkedToAccidentsBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridViewInjury
            // 
            this.gridViewInjury.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAccidentInjuryID,
            this.colAccidentID1,
            this.colLinkedToPersonID,
            this.colLinkedToPersonTypeID,
            this.colLinkedToPersonOther,
            this.colAccidentBodyAreaID,
            this.colNatureOfInjuryID,
            this.colRemarks1,
            this.colLinkedToAccident,
            this.colAccidentType1,
            this.colAccidentDate1,
            this.colAccidentBodyArea,
            this.colNatureOfInjury,
            this.colLinkedToPerson,
            this.colLinkedToPersonType});
            this.gridViewInjury.GridControl = this.gridControlInjury;
            this.gridViewInjury.GroupCount = 1;
            this.gridViewInjury.Name = "gridViewInjury";
            this.gridViewInjury.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewInjury.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewInjury.OptionsLayout.StoreAppearance = true;
            this.gridViewInjury.OptionsLayout.StoreFormatRules = true;
            this.gridViewInjury.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewInjury.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewInjury.OptionsSelection.MultiSelect = true;
            this.gridViewInjury.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewInjury.OptionsView.ColumnAutoWidth = false;
            this.gridViewInjury.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewInjury.OptionsView.ShowGroupPanel = false;
            this.gridViewInjury.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToAccident, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPerson, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAccidentBodyArea, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewInjury.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewInjury.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewInjury.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewInjury.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewInjury.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewInjury.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewInjury_MouseUp);
            this.gridViewInjury.DoubleClick += new System.EventHandler(this.gridViewInjury_DoubleClick);
            this.gridViewInjury.GotFocus += new System.EventHandler(this.gridViewInjury_GotFocus);
            // 
            // colAccidentInjuryID
            // 
            this.colAccidentInjuryID.Caption = "Accident Injury ID";
            this.colAccidentInjuryID.FieldName = "AccidentInjuryID";
            this.colAccidentInjuryID.Name = "colAccidentInjuryID";
            this.colAccidentInjuryID.OptionsColumn.AllowEdit = false;
            this.colAccidentInjuryID.OptionsColumn.AllowFocus = false;
            this.colAccidentInjuryID.OptionsColumn.ReadOnly = true;
            this.colAccidentInjuryID.Width = 108;
            // 
            // colAccidentID1
            // 
            this.colAccidentID1.Caption = "Accident ID";
            this.colAccidentID1.FieldName = "AccidentID";
            this.colAccidentID1.Name = "colAccidentID1";
            this.colAccidentID1.OptionsColumn.AllowEdit = false;
            this.colAccidentID1.OptionsColumn.AllowFocus = false;
            this.colAccidentID1.OptionsColumn.ReadOnly = true;
            this.colAccidentID1.Width = 76;
            // 
            // colLinkedToPersonID
            // 
            this.colLinkedToPersonID.Caption = "Linked To Person ID";
            this.colLinkedToPersonID.FieldName = "LinkedToPersonID";
            this.colLinkedToPersonID.Name = "colLinkedToPersonID";
            this.colLinkedToPersonID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonID.Width = 116;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Linked To Person Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 143;
            // 
            // colLinkedToPersonOther
            // 
            this.colLinkedToPersonOther.Caption = "Linked To Person Other";
            this.colLinkedToPersonOther.FieldName = "LinkedToPersonOther";
            this.colLinkedToPersonOther.Name = "colLinkedToPersonOther";
            this.colLinkedToPersonOther.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonOther.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonOther.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonOther.Width = 133;
            // 
            // colAccidentBodyAreaID
            // 
            this.colAccidentBodyAreaID.Caption = "Accident Body Area ID";
            this.colAccidentBodyAreaID.FieldName = "AccidentBodyAreaID";
            this.colAccidentBodyAreaID.Name = "colAccidentBodyAreaID";
            this.colAccidentBodyAreaID.OptionsColumn.AllowEdit = false;
            this.colAccidentBodyAreaID.OptionsColumn.AllowFocus = false;
            this.colAccidentBodyAreaID.OptionsColumn.ReadOnly = true;
            this.colAccidentBodyAreaID.Width = 129;
            // 
            // colNatureOfInjuryID
            // 
            this.colNatureOfInjuryID.Caption = "Nature Of Injury ID";
            this.colNatureOfInjuryID.FieldName = "NatureOfInjuryID";
            this.colNatureOfInjuryID.Name = "colNatureOfInjuryID";
            this.colNatureOfInjuryID.OptionsColumn.AllowEdit = false;
            this.colNatureOfInjuryID.OptionsColumn.AllowFocus = false;
            this.colNatureOfInjuryID.OptionsColumn.ReadOnly = true;
            this.colNatureOfInjuryID.Width = 115;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colLinkedToAccident
            // 
            this.colLinkedToAccident.Caption = "Linked To Accident";
            this.colLinkedToAccident.FieldName = "LinkedToAccident";
            this.colLinkedToAccident.Name = "colLinkedToAccident";
            this.colLinkedToAccident.OptionsColumn.AllowEdit = false;
            this.colLinkedToAccident.OptionsColumn.AllowFocus = false;
            this.colLinkedToAccident.OptionsColumn.ReadOnly = true;
            this.colLinkedToAccident.Visible = true;
            this.colLinkedToAccident.VisibleIndex = 5;
            this.colLinkedToAccident.Width = 300;
            // 
            // colAccidentType1
            // 
            this.colAccidentType1.Caption = "Accident Type";
            this.colAccidentType1.FieldName = "AccidentType";
            this.colAccidentType1.Name = "colAccidentType1";
            this.colAccidentType1.OptionsColumn.AllowEdit = false;
            this.colAccidentType1.OptionsColumn.AllowFocus = false;
            this.colAccidentType1.OptionsColumn.ReadOnly = true;
            this.colAccidentType1.Width = 89;
            // 
            // colAccidentDate1
            // 
            this.colAccidentDate1.Caption = "Accident Date";
            this.colAccidentDate1.ColumnEdit = this.repositoryItemTextEditDateTime2;
            this.colAccidentDate1.FieldName = "AccidentDate";
            this.colAccidentDate1.Name = "colAccidentDate1";
            this.colAccidentDate1.OptionsColumn.AllowEdit = false;
            this.colAccidentDate1.OptionsColumn.AllowFocus = false;
            this.colAccidentDate1.OptionsColumn.ReadOnly = true;
            this.colAccidentDate1.Width = 88;
            // 
            // repositoryItemTextEditDateTime2
            // 
            this.repositoryItemTextEditDateTime2.AutoHeight = false;
            this.repositoryItemTextEditDateTime2.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime2.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime2.Name = "repositoryItemTextEditDateTime2";
            // 
            // colAccidentBodyArea
            // 
            this.colAccidentBodyArea.Caption = "Accident Body Area";
            this.colAccidentBodyArea.FieldName = "AccidentBodyArea";
            this.colAccidentBodyArea.Name = "colAccidentBodyArea";
            this.colAccidentBodyArea.OptionsColumn.AllowEdit = false;
            this.colAccidentBodyArea.OptionsColumn.AllowFocus = false;
            this.colAccidentBodyArea.OptionsColumn.ReadOnly = true;
            this.colAccidentBodyArea.Visible = true;
            this.colAccidentBodyArea.VisibleIndex = 2;
            this.colAccidentBodyArea.Width = 161;
            // 
            // colNatureOfInjury
            // 
            this.colNatureOfInjury.Caption = "Nature Of Injury";
            this.colNatureOfInjury.FieldName = "NatureOfInjury";
            this.colNatureOfInjury.Name = "colNatureOfInjury";
            this.colNatureOfInjury.OptionsColumn.AllowEdit = false;
            this.colNatureOfInjury.OptionsColumn.AllowFocus = false;
            this.colNatureOfInjury.OptionsColumn.ReadOnly = true;
            this.colNatureOfInjury.Visible = true;
            this.colNatureOfInjury.VisibleIndex = 3;
            this.colNatureOfInjury.Width = 168;
            // 
            // colLinkedToPerson
            // 
            this.colLinkedToPerson.Caption = "Linked To Person";
            this.colLinkedToPerson.FieldName = "LinkedToPerson";
            this.colLinkedToPerson.Name = "colLinkedToPerson";
            this.colLinkedToPerson.OptionsColumn.AllowEdit = false;
            this.colLinkedToPerson.OptionsColumn.AllowFocus = false;
            this.colLinkedToPerson.OptionsColumn.ReadOnly = true;
            this.colLinkedToPerson.Visible = true;
            this.colLinkedToPerson.VisibleIndex = 0;
            this.colLinkedToPerson.Width = 209;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Linked To Person Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Visible = true;
            this.colLinkedToPersonType.VisibleIndex = 1;
            this.colLinkedToPersonType.Width = 129;
            // 
            // xtraTabPagePictures
            // 
            this.xtraTabPagePictures.Controls.Add(this.gridControlPicture);
            this.xtraTabPagePictures.Name = "xtraTabPagePictures";
            this.xtraTabPagePictures.Size = new System.Drawing.Size(1276, 219);
            this.xtraTabPagePictures.Text = "Linked Pictures";
            // 
            // gridControlPicture
            // 
            this.gridControlPicture.DataSource = this.sp00318PicturesLinkedToAccidentsBindingSource;
            this.gridControlPicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlPicture.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlPicture.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Reload Data", "reload")});
            this.gridControlPicture.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlPicture_EmbeddedNavigator_ButtonClick);
            this.gridControlPicture.Location = new System.Drawing.Point(0, 0);
            this.gridControlPicture.MainView = this.gridViewPicture;
            this.gridControlPicture.MenuManager = this.barManager1;
            this.gridControlPicture.Name = "gridControlPicture";
            this.gridControlPicture.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8,
            this.repositoryItemTextEditDateTime7,
            this.repositoryItemHyperLinkEdit1});
            this.gridControlPicture.Size = new System.Drawing.Size(1276, 219);
            this.gridControlPicture.TabIndex = 2;
            this.gridControlPicture.UseEmbeddedNavigator = true;
            this.gridControlPicture.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPicture});
            // 
            // sp00318PicturesLinkedToAccidentsBindingSource
            // 
            this.sp00318PicturesLinkedToAccidentsBindingSource.DataMember = "sp00318_Pictures_Linked_To_Accidents";
            this.sp00318PicturesLinkedToAccidentsBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridViewPicture
            // 
            this.gridViewPicture.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAccidentPictureID,
            this.colAccidentID3,
            this.colDateTimeTaken,
            this.colPicturePath,
            this.colRemarks2,
            this.colLinkedToAccident2,
            this.colAccidentType3,
            this.colAccidentDate3,
            this.colAddedByStaff,
            this.colAddedByStaffID});
            this.gridViewPicture.GridControl = this.gridControlPicture;
            this.gridViewPicture.GroupCount = 1;
            this.gridViewPicture.Name = "gridViewPicture";
            this.gridViewPicture.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewPicture.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewPicture.OptionsLayout.StoreAppearance = true;
            this.gridViewPicture.OptionsLayout.StoreFormatRules = true;
            this.gridViewPicture.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewPicture.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewPicture.OptionsSelection.MultiSelect = true;
            this.gridViewPicture.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewPicture.OptionsView.ColumnAutoWidth = false;
            this.gridViewPicture.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewPicture.OptionsView.ShowGroupPanel = false;
            this.gridViewPicture.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToAccident2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateTimeTaken, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewPicture.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewPicture_CustomRowCellEdit);
            this.gridViewPicture.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewPicture.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewPicture.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewPicture.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewPicture_ShowingEditor);
            this.gridViewPicture.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewPicture.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewPicture.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewPicture_MouseUp);
            this.gridViewPicture.DoubleClick += new System.EventHandler(this.gridViewPicture_DoubleClick);
            this.gridViewPicture.GotFocus += new System.EventHandler(this.gridViewPicture_GotFocus);
            // 
            // colAccidentPictureID
            // 
            this.colAccidentPictureID.Caption = "Accident Picture ID";
            this.colAccidentPictureID.FieldName = "AccidentPictureID";
            this.colAccidentPictureID.Name = "colAccidentPictureID";
            this.colAccidentPictureID.OptionsColumn.AllowEdit = false;
            this.colAccidentPictureID.OptionsColumn.AllowFocus = false;
            this.colAccidentPictureID.OptionsColumn.ReadOnly = true;
            this.colAccidentPictureID.Width = 112;
            // 
            // colAccidentID3
            // 
            this.colAccidentID3.Caption = "Accident ID";
            this.colAccidentID3.FieldName = "AccidentID";
            this.colAccidentID3.Name = "colAccidentID3";
            this.colAccidentID3.OptionsColumn.AllowEdit = false;
            this.colAccidentID3.OptionsColumn.AllowFocus = false;
            this.colAccidentID3.OptionsColumn.ReadOnly = true;
            this.colAccidentID3.Width = 76;
            // 
            // colDateTimeTaken
            // 
            this.colDateTimeTaken.Caption = "Date Taken";
            this.colDateTimeTaken.ColumnEdit = this.repositoryItemTextEditDateTime7;
            this.colDateTimeTaken.FieldName = "DateTimeTaken";
            this.colDateTimeTaken.Name = "colDateTimeTaken";
            this.colDateTimeTaken.OptionsColumn.AllowEdit = false;
            this.colDateTimeTaken.OptionsColumn.AllowFocus = false;
            this.colDateTimeTaken.OptionsColumn.ReadOnly = true;
            this.colDateTimeTaken.Visible = true;
            this.colDateTimeTaken.VisibleIndex = 0;
            this.colDateTimeTaken.Width = 122;
            // 
            // repositoryItemTextEditDateTime7
            // 
            this.repositoryItemTextEditDateTime7.AutoHeight = false;
            this.repositoryItemTextEditDateTime7.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime7.Name = "repositoryItemTextEditDateTime7";
            // 
            // colPicturePath
            // 
            this.colPicturePath.Caption = "Picture";
            this.colPicturePath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colPicturePath.FieldName = "PicturePath";
            this.colPicturePath.Name = "colPicturePath";
            this.colPicturePath.OptionsColumn.ReadOnly = true;
            this.colPicturePath.Visible = true;
            this.colPicturePath.VisibleIndex = 2;
            this.colPicturePath.Width = 394;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 3;
            this.colRemarks2.Width = 166;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // colLinkedToAccident2
            // 
            this.colLinkedToAccident2.Caption = "Linked To Accident";
            this.colLinkedToAccident2.FieldName = "LinkedToAccident";
            this.colLinkedToAccident2.Name = "colLinkedToAccident2";
            this.colLinkedToAccident2.OptionsColumn.AllowEdit = false;
            this.colLinkedToAccident2.OptionsColumn.AllowFocus = false;
            this.colLinkedToAccident2.OptionsColumn.ReadOnly = true;
            this.colLinkedToAccident2.Visible = true;
            this.colLinkedToAccident2.VisibleIndex = 4;
            this.colLinkedToAccident2.Width = 266;
            // 
            // colAccidentType3
            // 
            this.colAccidentType3.Caption = "Accident Type";
            this.colAccidentType3.FieldName = "AccidentType";
            this.colAccidentType3.Name = "colAccidentType3";
            this.colAccidentType3.OptionsColumn.AllowEdit = false;
            this.colAccidentType3.OptionsColumn.AllowFocus = false;
            this.colAccidentType3.OptionsColumn.ReadOnly = true;
            this.colAccidentType3.Width = 89;
            // 
            // colAccidentDate3
            // 
            this.colAccidentDate3.Caption = "Accident Date";
            this.colAccidentDate3.ColumnEdit = this.repositoryItemTextEditDateTime7;
            this.colAccidentDate3.FieldName = "AccidentDate";
            this.colAccidentDate3.Name = "colAccidentDate3";
            this.colAccidentDate3.OptionsColumn.AllowEdit = false;
            this.colAccidentDate3.OptionsColumn.AllowFocus = false;
            this.colAccidentDate3.OptionsColumn.ReadOnly = true;
            this.colAccidentDate3.Width = 88;
            // 
            // colAddedByStaff
            // 
            this.colAddedByStaff.Caption = "Added By Staff";
            this.colAddedByStaff.FieldName = "AddedByStaff";
            this.colAddedByStaff.Name = "colAddedByStaff";
            this.colAddedByStaff.OptionsColumn.AllowEdit = false;
            this.colAddedByStaff.OptionsColumn.AllowFocus = false;
            this.colAddedByStaff.OptionsColumn.ReadOnly = true;
            this.colAddedByStaff.Visible = true;
            this.colAddedByStaff.VisibleIndex = 1;
            this.colAddedByStaff.Width = 140;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // xtraTabPageComments
            // 
            this.xtraTabPageComments.Controls.Add(this.gridControlComments);
            this.xtraTabPageComments.Name = "xtraTabPageComments";
            this.xtraTabPageComments.Size = new System.Drawing.Size(1276, 219);
            this.xtraTabPageComments.Text = "Linked Comments";
            // 
            // gridControlComments
            // 
            this.gridControlComments.DataSource = this.sp00315CommentsLinkedToAccidentsBindingSource;
            this.gridControlComments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlComments.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlComments.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlComments.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlComments.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Reload Data", "reload")});
            this.gridControlComments.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlComments_EmbeddedNavigator_ButtonClick);
            this.gridControlComments.Location = new System.Drawing.Point(0, 0);
            this.gridControlComments.MainView = this.gridViewComments;
            this.gridControlComments.MenuManager = this.barManager1;
            this.gridControlComments.Name = "gridControlComments";
            this.gridControlComments.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemTextEditDateTime5});
            this.gridControlComments.Size = new System.Drawing.Size(1276, 219);
            this.gridControlComments.TabIndex = 2;
            this.gridControlComments.UseEmbeddedNavigator = true;
            this.gridControlComments.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewComments});
            // 
            // sp00315CommentsLinkedToAccidentsBindingSource
            // 
            this.sp00315CommentsLinkedToAccidentsBindingSource.DataMember = "sp00315_Comments_Linked_To_Accidents";
            this.sp00315CommentsLinkedToAccidentsBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridViewComments
            // 
            this.gridViewComments.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAccidentCommentID,
            this.colAccidentID2,
            this.colRecordedByPersonID,
            this.colRecordedByPersonTypeID,
            this.colDateRecorded,
            this.colComment,
            this.colLinkedToAccident1,
            this.colAccidentType2,
            this.colAccidentDate2,
            this.colRecordedByPerson,
            this.colRecordedByPersonType});
            this.gridViewComments.GridControl = this.gridControlComments;
            this.gridViewComments.GroupCount = 1;
            this.gridViewComments.Name = "gridViewComments";
            this.gridViewComments.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewComments.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewComments.OptionsLayout.StoreAppearance = true;
            this.gridViewComments.OptionsLayout.StoreFormatRules = true;
            this.gridViewComments.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewComments.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewComments.OptionsSelection.MultiSelect = true;
            this.gridViewComments.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewComments.OptionsView.ColumnAutoWidth = false;
            this.gridViewComments.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewComments.OptionsView.ShowGroupPanel = false;
            this.gridViewComments.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToAccident1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateRecorded, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridViewComments.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewComments.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewComments.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewComments.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewComments.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewComments.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewComments_MouseUp);
            this.gridViewComments.DoubleClick += new System.EventHandler(this.gridViewComments_DoubleClick);
            this.gridViewComments.GotFocus += new System.EventHandler(this.gridViewComments_GotFocus);
            // 
            // colAccidentCommentID
            // 
            this.colAccidentCommentID.Caption = "Accident Comment ID";
            this.colAccidentCommentID.FieldName = "AccidentCommentID";
            this.colAccidentCommentID.Name = "colAccidentCommentID";
            this.colAccidentCommentID.OptionsColumn.AllowEdit = false;
            this.colAccidentCommentID.OptionsColumn.AllowFocus = false;
            this.colAccidentCommentID.OptionsColumn.ReadOnly = true;
            this.colAccidentCommentID.Width = 124;
            // 
            // colAccidentID2
            // 
            this.colAccidentID2.Caption = "Accident ID";
            this.colAccidentID2.FieldName = "AccidentID";
            this.colAccidentID2.Name = "colAccidentID2";
            this.colAccidentID2.OptionsColumn.AllowEdit = false;
            this.colAccidentID2.OptionsColumn.AllowFocus = false;
            this.colAccidentID2.OptionsColumn.ReadOnly = true;
            this.colAccidentID2.Width = 76;
            // 
            // colRecordedByPersonID
            // 
            this.colRecordedByPersonID.Caption = "Recorded By Person ID";
            this.colRecordedByPersonID.FieldName = "RecordedByPersonID";
            this.colRecordedByPersonID.Name = "colRecordedByPersonID";
            this.colRecordedByPersonID.OptionsColumn.AllowEdit = false;
            this.colRecordedByPersonID.OptionsColumn.AllowFocus = false;
            this.colRecordedByPersonID.OptionsColumn.ReadOnly = true;
            this.colRecordedByPersonID.Width = 132;
            // 
            // colRecordedByPersonTypeID
            // 
            this.colRecordedByPersonTypeID.Caption = "Recorded By Person Type ID";
            this.colRecordedByPersonTypeID.FieldName = "RecordedByPersonTypeID";
            this.colRecordedByPersonTypeID.Name = "colRecordedByPersonTypeID";
            this.colRecordedByPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colRecordedByPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colRecordedByPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colRecordedByPersonTypeID.Width = 159;
            // 
            // colDateRecorded
            // 
            this.colDateRecorded.Caption = "Date Recorded";
            this.colDateRecorded.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colDateRecorded.FieldName = "DateRecorded";
            this.colDateRecorded.Name = "colDateRecorded";
            this.colDateRecorded.OptionsColumn.AllowEdit = false;
            this.colDateRecorded.OptionsColumn.AllowFocus = false;
            this.colDateRecorded.OptionsColumn.ReadOnly = true;
            this.colDateRecorded.Visible = true;
            this.colDateRecorded.VisibleIndex = 0;
            this.colDateRecorded.Width = 122;
            // 
            // repositoryItemTextEditDateTime5
            // 
            this.repositoryItemTextEditDateTime5.AutoHeight = false;
            this.repositoryItemTextEditDateTime5.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime5.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime5.Name = "repositoryItemTextEditDateTime5";
            // 
            // colComment
            // 
            this.colComment.Caption = "Comment";
            this.colComment.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.colComment.FieldName = "Comment";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.ReadOnly = true;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 3;
            this.colComment.Width = 345;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // colLinkedToAccident1
            // 
            this.colLinkedToAccident1.Caption = "Linked To Accident";
            this.colLinkedToAccident1.FieldName = "LinkedToAccident";
            this.colLinkedToAccident1.Name = "colLinkedToAccident1";
            this.colLinkedToAccident1.OptionsColumn.AllowEdit = false;
            this.colLinkedToAccident1.OptionsColumn.AllowFocus = false;
            this.colLinkedToAccident1.OptionsColumn.ReadOnly = true;
            this.colLinkedToAccident1.Visible = true;
            this.colLinkedToAccident1.VisibleIndex = 2;
            this.colLinkedToAccident1.Width = 225;
            // 
            // colAccidentType2
            // 
            this.colAccidentType2.Caption = "Accident Type";
            this.colAccidentType2.FieldName = "AccidentType";
            this.colAccidentType2.Name = "colAccidentType2";
            this.colAccidentType2.OptionsColumn.AllowEdit = false;
            this.colAccidentType2.OptionsColumn.AllowFocus = false;
            this.colAccidentType2.OptionsColumn.ReadOnly = true;
            this.colAccidentType2.Width = 103;
            // 
            // colAccidentDate2
            // 
            this.colAccidentDate2.Caption = "Accident Date";
            this.colAccidentDate2.ColumnEdit = this.repositoryItemTextEditDateTime5;
            this.colAccidentDate2.FieldName = "AccidentDate";
            this.colAccidentDate2.Name = "colAccidentDate2";
            this.colAccidentDate2.OptionsColumn.AllowEdit = false;
            this.colAccidentDate2.OptionsColumn.AllowFocus = false;
            this.colAccidentDate2.OptionsColumn.ReadOnly = true;
            this.colAccidentDate2.Width = 96;
            // 
            // colRecordedByPerson
            // 
            this.colRecordedByPerson.Caption = "Recorded By Person";
            this.colRecordedByPerson.FieldName = "RecordedByPerson";
            this.colRecordedByPerson.Name = "colRecordedByPerson";
            this.colRecordedByPerson.OptionsColumn.AllowEdit = false;
            this.colRecordedByPerson.OptionsColumn.AllowFocus = false;
            this.colRecordedByPerson.OptionsColumn.ReadOnly = true;
            this.colRecordedByPerson.Visible = true;
            this.colRecordedByPerson.VisibleIndex = 1;
            this.colRecordedByPerson.Width = 140;
            // 
            // colRecordedByPersonType
            // 
            this.colRecordedByPersonType.Caption = "Recorded By Person Type";
            this.colRecordedByPersonType.FieldName = "RecordedByPersonType";
            this.colRecordedByPersonType.Name = "colRecordedByPersonType";
            this.colRecordedByPersonType.OptionsColumn.AllowEdit = false;
            this.colRecordedByPersonType.OptionsColumn.AllowFocus = false;
            this.colRecordedByPersonType.OptionsColumn.ReadOnly = true;
            this.colRecordedByPersonType.Visible = true;
            this.colRecordedByPersonType.VisibleIndex = 2;
            this.colRecordedByPersonType.Width = 145;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bar1
            // 
            this.bar1.BarName = "GrittingCalloutToolbar";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(519, 261);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemBusinessArea),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemDateRange),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiRefresh, true)});
            this.bar1.OptionsBar.DisableClose = true;
            this.bar1.OptionsBar.DisableCustomization = true;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "Gritting Callout Toolbar";
            // 
            // barEditItemBusinessArea
            // 
            this.barEditItemBusinessArea.Caption = "Business Area Filter:";
            this.barEditItemBusinessArea.Edit = this.repositoryItemPopupContainerEditBusinessAreaFilter;
            this.barEditItemBusinessArea.EditValue = "No Business Area Filter";
            this.barEditItemBusinessArea.EditWidth = 284;
            this.barEditItemBusinessArea.Id = 117;
            this.barEditItemBusinessArea.Name = "barEditItemBusinessArea";
            // 
            // repositoryItemPopupContainerEditBusinessAreaFilter
            // 
            this.repositoryItemPopupContainerEditBusinessAreaFilter.AutoHeight = false;
            this.repositoryItemPopupContainerEditBusinessAreaFilter.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditBusinessAreaFilter.Name = "repositoryItemPopupContainerEditBusinessAreaFilter";
            this.repositoryItemPopupContainerEditBusinessAreaFilter.PopupControl = this.popupContainerControlBusinessAreaFilter;
            this.repositoryItemPopupContainerEditBusinessAreaFilter.ShowPopupCloseButton = false;
            this.repositoryItemPopupContainerEditBusinessAreaFilter.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditBusinessAreaFilter_QueryResultValue);
            // 
            // barEditItemDateRange
            // 
            this.barEditItemDateRange.Caption = "Visit Date Range Filter";
            this.barEditItemDateRange.Edit = this.repositoryItemPopupContainerEditDateRange;
            this.barEditItemDateRange.EditValue = "No Date Range Filter";
            this.barEditItemDateRange.EditWidth = 240;
            this.barEditItemDateRange.Id = 57;
            this.barEditItemDateRange.Name = "barEditItemDateRange";
            // 
            // repositoryItemPopupContainerEditDateRange
            // 
            this.repositoryItemPopupContainerEditDateRange.AutoHeight = false;
            this.repositoryItemPopupContainerEditDateRange.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemPopupContainerEditDateRange.Name = "repositoryItemPopupContainerEditDateRange";
            this.repositoryItemPopupContainerEditDateRange.PopupControl = this.popupContainerControlDateRange;
            this.repositoryItemPopupContainerEditDateRange.QueryResultValue += new DevExpress.XtraEditors.Controls.QueryResultValueEventHandler(this.repositoryItemPopupContainerEditDateRange_QueryResultValue);
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "Refresh";
            this.bbiRefresh.Id = 58;
            this.bbiRefresh.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.ImageOptions.Image")));
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // toolTipController1
            // 
            this.toolTipController1.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            // 
            // imageCollectionTooltips
            // 
            this.imageCollectionTooltips.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollectionTooltips.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollectionTooltips.ImageStream")));
            this.imageCollectionTooltips.InsertGalleryImage("info_32x32.png", "images/support/info_32x32.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/support/info_32x32.png"), 0);
            this.imageCollectionTooltips.Images.SetKeyName(0, "info_32x32.png");
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // toggleRecurrenceItem1
            // 
            this.toggleRecurrenceItem1.Id = 105;
            this.toggleRecurrenceItem1.Name = "toggleRecurrenceItem1";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageAccidents;
            this.xtraTabControl1.Size = new System.Drawing.Size(1286, 564);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageAccidents,
            this.xtraTabPagePicklists});
            // 
            // xtraTabPageAccidents
            // 
            this.xtraTabPageAccidents.Controls.Add(this.splitContainerControl1);
            this.xtraTabPageAccidents.Name = "xtraTabPageAccidents";
            this.xtraTabPageAccidents.Size = new System.Drawing.Size(1281, 538);
            this.xtraTabPageAccidents.Text = "Accidents";
            // 
            // xtraTabPagePicklists
            // 
            this.xtraTabPagePicklists.Controls.Add(this.xtraTabControl3);
            this.xtraTabPagePicklists.Name = "xtraTabPagePicklists";
            this.xtraTabPagePicklists.Size = new System.Drawing.Size(1281, 538);
            this.xtraTabPagePicklists.Text = "Accident Master Picklist Data";
            // 
            // xtraTabControl3
            // 
            this.xtraTabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl3.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl3.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl3.Name = "xtraTabControl3";
            this.xtraTabControl3.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl3.Size = new System.Drawing.Size(1281, 538);
            this.xtraTabControl3.TabIndex = 0;
            this.xtraTabControl3.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControlSubType);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1254, 533);
            this.xtraTabPage1.Text = "Accident Sub-Types";
            // 
            // gridControlSubType
            // 
            this.gridControlSubType.DataSource = this.sp00322AccidentSubTypesManagerBindingSource;
            this.gridControlSubType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSubType.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlSubType.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlSubType.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlSubType.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Reload Data", "reload")});
            this.gridControlSubType.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlSubType_EmbeddedNavigator_ButtonClick);
            this.gridControlSubType.Location = new System.Drawing.Point(0, 0);
            this.gridControlSubType.MainView = this.gridViewSubType;
            this.gridControlSubType.MenuManager = this.barManager1;
            this.gridControlSubType.Name = "gridControlSubType";
            this.gridControlSubType.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3});
            this.gridControlSubType.Size = new System.Drawing.Size(1254, 533);
            this.gridControlSubType.TabIndex = 5;
            this.gridControlSubType.UseEmbeddedNavigator = true;
            this.gridControlSubType.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSubType});
            // 
            // sp00322AccidentSubTypesManagerBindingSource
            // 
            this.sp00322AccidentSubTypesManagerBindingSource.DataMember = "sp00322_Accident_Sub_Types_Manager";
            this.sp00322AccidentSubTypesManagerBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridViewSubType
            // 
            this.gridViewSubType.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAccidentSubTypeID1,
            this.colAccidentTypeID1,
            this.colDescription,
            this.colRecordOrder,
            this.colRemarks3,
            this.colAccidentType4});
            this.gridViewSubType.GridControl = this.gridControlSubType;
            this.gridViewSubType.Name = "gridViewSubType";
            this.gridViewSubType.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewSubType.OptionsFind.AlwaysVisible = true;
            this.gridViewSubType.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewSubType.OptionsLayout.StoreAppearance = true;
            this.gridViewSubType.OptionsLayout.StoreFormatRules = true;
            this.gridViewSubType.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewSubType.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewSubType.OptionsSelection.MultiSelect = true;
            this.gridViewSubType.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewSubType.OptionsView.ColumnAutoWidth = false;
            this.gridViewSubType.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewSubType.OptionsView.ShowGroupPanel = false;
            this.gridViewSubType.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colAccidentType4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colRecordOrder, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewSubType.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewSubType.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewSubType.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewSubType.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewSubType.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewSubType.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewSubType_MouseUp);
            this.gridViewSubType.DoubleClick += new System.EventHandler(this.gridViewSubType_DoubleClick);
            this.gridViewSubType.GotFocus += new System.EventHandler(this.gridViewSubType_GotFocus);
            // 
            // colAccidentSubTypeID1
            // 
            this.colAccidentSubTypeID1.Caption = "Accident Sub-Type ID";
            this.colAccidentSubTypeID1.FieldName = "AccidentSubTypeID";
            this.colAccidentSubTypeID1.Name = "colAccidentSubTypeID1";
            this.colAccidentSubTypeID1.OptionsColumn.AllowEdit = false;
            this.colAccidentSubTypeID1.OptionsColumn.AllowFocus = false;
            this.colAccidentSubTypeID1.OptionsColumn.ReadOnly = true;
            this.colAccidentSubTypeID1.Width = 125;
            // 
            // colAccidentTypeID1
            // 
            this.colAccidentTypeID1.Caption = "Accident Type ID";
            this.colAccidentTypeID1.FieldName = "AccidentTypeID";
            this.colAccidentTypeID1.Name = "colAccidentTypeID1";
            this.colAccidentTypeID1.OptionsColumn.AllowEdit = false;
            this.colAccidentTypeID1.OptionsColumn.AllowFocus = false;
            this.colAccidentTypeID1.OptionsColumn.ReadOnly = true;
            this.colAccidentTypeID1.Width = 103;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Accident Sub-Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 199;
            // 
            // colRecordOrder
            // 
            this.colRecordOrder.Caption = "Order";
            this.colRecordOrder.FieldName = "RecordOrder";
            this.colRecordOrder.Name = "colRecordOrder";
            this.colRecordOrder.OptionsColumn.AllowEdit = false;
            this.colRecordOrder.OptionsColumn.AllowFocus = false;
            this.colRecordOrder.OptionsColumn.ReadOnly = true;
            this.colRecordOrder.Visible = true;
            this.colRecordOrder.VisibleIndex = 2;
            this.colRecordOrder.Width = 62;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 3;
            this.colRemarks3.Width = 210;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colAccidentType4
            // 
            this.colAccidentType4.Caption = "Accident Type";
            this.colAccidentType4.FieldName = "AccidentType";
            this.colAccidentType4.Name = "colAccidentType4";
            this.colAccidentType4.OptionsColumn.AllowEdit = false;
            this.colAccidentType4.OptionsColumn.AllowFocus = false;
            this.colAccidentType4.OptionsColumn.ReadOnly = true;
            this.colAccidentType4.Visible = true;
            this.colAccidentType4.VisibleIndex = 0;
            this.colAccidentType4.Width = 180;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControlBodyArea);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1254, 533);
            this.xtraTabPage2.Text = "Accident Body Areas";
            // 
            // gridControlBodyArea
            // 
            this.gridControlBodyArea.DataSource = this.sp00325AccidentBodyAreasManagerBindingSource;
            this.gridControlBodyArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlBodyArea.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlBodyArea.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Reload Data", "reload")});
            this.gridControlBodyArea.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlBodyArea_EmbeddedNavigator_ButtonClick);
            this.gridControlBodyArea.Location = new System.Drawing.Point(0, 0);
            this.gridControlBodyArea.MainView = this.gridViewBodyArea;
            this.gridControlBodyArea.MenuManager = this.barManager1;
            this.gridControlBodyArea.Name = "gridControlBodyArea";
            this.gridControlBodyArea.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlBodyArea.Size = new System.Drawing.Size(1254, 533);
            this.gridControlBodyArea.TabIndex = 6;
            this.gridControlBodyArea.UseEmbeddedNavigator = true;
            this.gridControlBodyArea.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBodyArea});
            // 
            // sp00325AccidentBodyAreasManagerBindingSource
            // 
            this.sp00325AccidentBodyAreasManagerBindingSource.DataMember = "sp00325_Accident_Body_Areas_Manager";
            this.sp00325AccidentBodyAreasManagerBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridViewBodyArea
            // 
            this.gridViewBodyArea.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5});
            this.gridViewBodyArea.GridControl = this.gridControlBodyArea;
            this.gridViewBodyArea.Name = "gridViewBodyArea";
            this.gridViewBodyArea.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewBodyArea.OptionsFind.AlwaysVisible = true;
            this.gridViewBodyArea.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewBodyArea.OptionsLayout.StoreAppearance = true;
            this.gridViewBodyArea.OptionsLayout.StoreFormatRules = true;
            this.gridViewBodyArea.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewBodyArea.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewBodyArea.OptionsSelection.MultiSelect = true;
            this.gridViewBodyArea.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewBodyArea.OptionsView.ColumnAutoWidth = false;
            this.gridViewBodyArea.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewBodyArea.OptionsView.ShowGroupPanel = false;
            this.gridViewBodyArea.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewBodyArea.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewBodyArea.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewBodyArea.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewBodyArea.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewBodyArea.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewBodyArea.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewBodyArea_MouseUp);
            this.gridViewBodyArea.DoubleClick += new System.EventHandler(this.gridViewBodyArea_DoubleClick);
            this.gridViewBodyArea.GotFocus += new System.EventHandler(this.gridViewBodyArea_GotFocus);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Accident Body Area ID";
            this.gridColumn1.FieldName = "AccidentBodyAreaID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.Width = 125;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Body Area Description";
            this.gridColumn3.FieldName = "Description";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            this.gridColumn3.Width = 246;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Order";
            this.gridColumn4.FieldName = "RecordOrder";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 62;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Remarks";
            this.gridColumn5.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.gridColumn5.FieldName = "Remarks";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 210;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControlInjuryType);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1254, 533);
            this.xtraTabPage3.Text = "Accident Injury Types";
            // 
            // gridControlInjuryType
            // 
            this.gridControlInjuryType.DataSource = this.sp00328AccidentInjuryTypesManagerBindingSource;
            this.gridControlInjuryType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlInjuryType.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlInjuryType.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "View Selected Record(s)", "view"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 10, true, true, "Reload Data", "reload")});
            this.gridControlInjuryType.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlInjuryType_EmbeddedNavigator_ButtonClick);
            this.gridControlInjuryType.Location = new System.Drawing.Point(0, 0);
            this.gridControlInjuryType.MainView = this.gridViewInjuryType;
            this.gridControlInjuryType.MenuManager = this.barManager1;
            this.gridControlInjuryType.Name = "gridControlInjuryType";
            this.gridControlInjuryType.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit5});
            this.gridControlInjuryType.Size = new System.Drawing.Size(1254, 533);
            this.gridControlInjuryType.TabIndex = 7;
            this.gridControlInjuryType.UseEmbeddedNavigator = true;
            this.gridControlInjuryType.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewInjuryType});
            // 
            // sp00328AccidentInjuryTypesManagerBindingSource
            // 
            this.sp00328AccidentInjuryTypesManagerBindingSource.DataMember = "sp00328_Accident_Injury_Types_Manager";
            this.sp00328AccidentInjuryTypesManagerBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridViewInjuryType
            // 
            this.gridViewInjuryType.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gridViewInjuryType.GridControl = this.gridControlInjuryType;
            this.gridViewInjuryType.Name = "gridViewInjuryType";
            this.gridViewInjuryType.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridViewInjuryType.OptionsFind.AlwaysVisible = true;
            this.gridViewInjuryType.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridViewInjuryType.OptionsLayout.StoreAppearance = true;
            this.gridViewInjuryType.OptionsLayout.StoreFormatRules = true;
            this.gridViewInjuryType.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridViewInjuryType.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridViewInjuryType.OptionsSelection.MultiSelect = true;
            this.gridViewInjuryType.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridViewInjuryType.OptionsView.ColumnAutoWidth = false;
            this.gridViewInjuryType.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewInjuryType.OptionsView.ShowGroupPanel = false;
            this.gridViewInjuryType.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn7, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridViewInjuryType.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridViewInjuryType.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridViewInjuryType.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridViewInjuryType.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridViewInjuryType.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridViewInjuryType.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridViewInjuryType_MouseUp);
            this.gridViewInjuryType.DoubleClick += new System.EventHandler(this.gridViewInjuryType_DoubleClick);
            this.gridViewInjuryType.GotFocus += new System.EventHandler(this.gridViewInjuryType_GotFocus);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Accident Injury Type ID";
            this.gridColumn2.FieldName = "AccidentInjuryNatureID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 125;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Injury Type Description";
            this.gridColumn6.FieldName = "Description";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 246;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Order";
            this.gridColumn7.FieldName = "RecordOrder";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            this.gridColumn7.Width = 62;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Remarks";
            this.gridColumn8.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.gridColumn8.FieldName = "Remarks";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            this.gridColumn8.Width = 210;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // sp00302_Accident_Business_AreasTableAdapter
            // 
            this.sp00302_Accident_Business_AreasTableAdapter.ClearBeforeFill = true;
            // 
            // sp00301_Accident_ManagerTableAdapter
            // 
            this.sp00301_Accident_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp00309_Injuries_Linked_To_AccidentsTableAdapter
            // 
            this.sp00309_Injuries_Linked_To_AccidentsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00315_Comments_Linked_To_AccidentsTableAdapter
            // 
            this.sp00315_Comments_Linked_To_AccidentsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00318_Pictures_Linked_To_AccidentsTableAdapter
            // 
            this.sp00318_Pictures_Linked_To_AccidentsTableAdapter.ClearBeforeFill = true;
            // 
            // sp00322_Accident_Sub_Types_ManagerTableAdapter
            // 
            this.sp00322_Accident_Sub_Types_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp00325_Accident_Body_Areas_ManagerTableAdapter
            // 
            this.sp00325_Accident_Body_Areas_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp00328_Accident_Injury_Types_ManagerTableAdapter
            // 
            this.sp00328_Accident_Injury_Types_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Accident_Manager
            // 
            this.ClientSize = new System.Drawing.Size(1286, 564);
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Accident_Manager";
            this.Text = "Accident Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Accident_Manager_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Accident_Manager_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Accident_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAccident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00301AccidentManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedRecord)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlBusinessAreaFilter)).EndInit();
            this.popupContainerControlBusinessAreaFilter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00302AccidentBusinessAreasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControlDateRange)).EndInit();
            this.popupContainerControlDateRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPageInjuries.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInjury)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00309InjuriesLinkedToAccidentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInjury)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime2)).EndInit();
            this.xtraTabPagePictures.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00318PicturesLinkedToAccidentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            this.xtraTabPageComments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00315CommentsLinkedToAccidentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewComments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditBusinessAreaFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPopupContainerEditDateRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollectionTooltips)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageAccidents.ResumeLayout(false);
            this.xtraTabPagePicklists.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl3)).EndInit();
            this.xtraTabControl3.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00322AccidentSubTypesManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlBodyArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00325AccidentBodyAreasManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBodyArea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlInjuryType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00328AccidentInjuryTypesManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewInjuryType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlAccident;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAccident;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.Utils.ToolTipController toolTipController1;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private WoodPlan5.DataSet_AT dataSet_AT;
        private WoodPlanDataSet woodPlanDataSet;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarEditItem barEditItemDateRange;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditDateRange;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraGrid.GridControl gridControlInjury;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInjury;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlDateRange;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.SimpleButton btnDateRangeOK;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.Utils.ImageCollection imageCollectionTooltips;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraScheduler.UI.ToggleRecurrenceItem toggleRecurrenceItem1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageInjuries;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePictures;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageComments;
        private DevExpress.XtraGrid.GridControl gridControlPicture;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPicture;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime7;
        private DevExpress.XtraGrid.GridControl gridControlComments;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewComments;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime5;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAccidents;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePicklists;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraBars.BarEditItem barEditItemBusinessArea;
        private DevExpress.XtraEditors.Repository.RepositoryItemPopupContainerEdit repositoryItemPopupContainerEditBusinessAreaFilter;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControlBusinessAreaFilter;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit17;
        private DataSet_Accident dataSet_Accident;
        private System.Windows.Forms.BindingSource sp00302AccidentBusinessAreasBindingSource;
        private DataSet_AccidentTableAdapters.sp00302_Accident_Business_AreasTableAdapter sp00302_Accident_Business_AreasTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName;
        private DevExpress.XtraEditors.SimpleButton btnBusinessAreaFilterOK;
        private System.Windows.Forms.BindingSource sp00301AccidentManagerBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPersonOther;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedToHSE;
        private DevExpress.XtraGrid.Columns.GridColumn colDateReportedToHSE;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colExternalReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colClientReferenceNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colSituationRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleManagerID;
        private DevExpress.XtraGrid.Columns.GridColumn colHSQETeamLead;
        private DevExpress.XtraGrid.Columns.GridColumn colCorrectiveActionRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colTargetClosureDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedDate;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraGrid.Columns.GridColumn colBusinessAreaName1;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentType;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentSubType;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colReportedByPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colResponsibleManager;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordFullDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContract;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordType;
        private DevExpress.XtraGrid.Columns.GridColumn colClosedByStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedInjuries;
        private DataSet_AccidentTableAdapters.sp00301_Accident_ManagerTableAdapter sp00301_Accident_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colHSQETeamLeadName;
        private System.Windows.Forms.BindingSource sp00309InjuriesLinkedToAccidentsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentInjuryID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonOther;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentBodyAreaID;
        private DevExpress.XtraGrid.Columns.GridColumn colNatureOfInjuryID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToAccident;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentType1;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentBodyArea;
        private DevExpress.XtraGrid.Columns.GridColumn colNatureOfInjury;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPerson;
        private DataSet_AccidentTableAdapters.sp00309_Injuries_Linked_To_AccidentsTableAdapter sp00309_Injuries_Linked_To_AccidentsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private System.Windows.Forms.BindingSource sp00315CommentsLinkedToAccidentsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentCommentID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentID2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateRecorded;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToAccident1;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentType2;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentDate2;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordedByPersonType;
        private DataSet_AccidentTableAdapters.sp00315_Comments_Linked_To_AccidentsTableAdapter sp00315_Comments_Linked_To_AccidentsTableAdapter;
        private System.Windows.Forms.BindingSource sp00318PicturesLinkedToAccidentsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentPictureID;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeTaken;
        private DevExpress.XtraGrid.Columns.GridColumn colPicturePath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToAccident2;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentType3;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentDate3;
        private DataSet_AccidentTableAdapters.sp00318_Pictures_Linked_To_AccidentsTableAdapter sp00318_Pictures_Linked_To_AccidentsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.GridControl gridControlSubType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSubType;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private System.Windows.Forms.BindingSource sp00322AccidentSubTypesManagerBindingSource;
        private DataSet_AccidentTableAdapters.sp00322_Accident_Sub_Types_ManagerTableAdapter sp00322_Accident_Sub_Types_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentSubTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRecordOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraGrid.Columns.GridColumn colAccidentType4;
        private DevExpress.XtraGrid.GridControl gridControlBodyArea;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBodyArea;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource sp00325AccidentBodyAreasManagerBindingSource;
        private DataSet_AccidentTableAdapters.sp00325_Accident_Body_Areas_ManagerTableAdapter sp00325_Accident_Body_Areas_ManagerTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControlInjuryType;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewInjuryType;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private System.Windows.Forms.BindingSource sp00328AccidentInjuryTypesManagerBindingSource;
        private DataSet_AccidentTableAdapters.sp00328_Accident_Injury_Types_ManagerTableAdapter sp00328_Accident_Injury_Types_ManagerTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedRecord;
    }
}

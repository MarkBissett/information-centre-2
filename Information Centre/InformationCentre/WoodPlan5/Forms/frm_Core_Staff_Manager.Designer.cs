namespace WoodPlan5
{
    partial class frm_Core_Staff_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Staff_Manager));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp00195StaffManagerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNetworkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colForename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSurname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colUserType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colToDoListVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCommentBankVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLoginCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShowTipsOnStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastTipID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserConfig = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colEmailAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShowConfirmations = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colMinimiseToTaskBar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDefaultFontName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultFontSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShowPhotoPopup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colFirstScreenLoaded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstScreenLoadedID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEstatePlanInspectionControlPanel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEditSiteInspectionControlPanel = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colWebSiteUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebSitePassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUtilityArbSurveyor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlanWebActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlanWebAdminUser = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlanWebFileManagerAdmin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlanWebWorkOrderDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWorkPhone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00196StaffManagerLinkedSavedScreensBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLayoutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colScreenName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayoutDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayoutRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWidth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDefaultLayout = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colShareTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShareTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl14 = new DevExpress.XtraGrid.GridControl();
            this.sp06309PersonVisitTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Core = new WoodPlan5.DataSet_OM_Core();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitTypePersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer6 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp04027GCSubContractorAllocatedPDAsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAllocatedPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubContractorName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMake = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaSerialNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaFirmwareVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMobileTelNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaDatePurchased = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime7 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSoftwareVersion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAllocated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colPDAUserName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDAPassword1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDALoginToken1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDACode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToPersonTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl13 = new DevExpress.XtraGrid.GridControl();
            this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAllocatedPdaLinkedTeamMemberID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocatedPDAID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamMemberID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAllocated1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime13 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colPdaMake1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaModel1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaSerialNumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaFirmwareVersion1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaMobileTelNo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPdaDatePurchased1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoftwareVersion1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIMEI1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPDACode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubcontractorName5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl25 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridView25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colDocumentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00195_Staff_ManagerTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00195_Staff_ManagerTableAdapter();
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00196_Staff_Manager_Linked_Saved_ScreensTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00196_Staff_Manager_Linked_Saved_ScreensTableAdapter();
            this.sp06309_Person_Visit_TypesTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06309_Person_Visit_TypesTableAdapter();
            this.sp04027_GC_SubContractor_Allocated_PDAsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04027_GC_SubContractor_Allocated_PDAsTableAdapter();
            this.sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00195StaffManagerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditSiteInspectionControlPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00196StaffManagerLinkedSavedScreensBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06309PersonVisitTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).BeginInit();
            this.gridSplitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04027GCSubContractorAllocatedPDAsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime13)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(874, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 715);
            this.barDockControlBottom.Size = new System.Drawing.Size(874, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 715);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(874, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 715);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Staff";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Records";
            this.splitContainerControl1.Size = new System.Drawing.Size(874, 715);
            this.splitContainerControl1.SplitterPosition = 327;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp00195StaffManagerBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 6, true, true, "Send Push Notification", "push_notification")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckEdit5,
            this.repositoryItemCheckEdit6,
            this.repositoryItemCheckEdit7,
            this.repositoryItemCheckEdit8,
            this.repositoryItemCheckEditSiteInspectionControlPanel});
            this.gridControl1.Size = new System.Drawing.Size(870, 358);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp00195StaffManagerBindingSource
            // 
            this.sp00195StaffManagerBindingSource.DataMember = "sp00195_Staff_Manager";
            this.sp00195StaffManagerBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "user_options_16.png");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("grid_16x16.png", "images/grid/grid_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/grid/grid_16x16.png"), 5);
            this.imageCollection1.Images.SetKeyName(5, "grid_16x16.png");
            this.imageCollection1.InsertGalleryImage("previouscomment_16x16.png", "images/comments/previouscomment_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/comments/previouscomment_16x16.png"), 6);
            this.imageCollection1.Images.SetKeyName(6, "previouscomment_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colStaffID,
            this.colNetworkID,
            this.colForename,
            this.colSurname,
            this.colStaffName,
            this.colUserActive,
            this.colUserType,
            this.colToDoListVisible,
            this.colCommentBankVisible,
            this.colLoginCreated,
            this.colShowTipsOnStart,
            this.colLastTipID,
            this.colUserConfig,
            this.colEmailAddress,
            this.colShowConfirmations,
            this.colMinimiseToTaskBar,
            this.colDefaultFontName,
            this.colDefaultFontSize,
            this.colShowPhotoPopup,
            this.colFirstScreenLoaded,
            this.colFirstScreenLoadedID,
            this.colEstatePlanInspectionControlPanel,
            this.colWebSiteUserName,
            this.colWebSitePassword,
            this.colIsUtilityArbSurveyor,
            this.colWoodPlanWebActionDoneDate,
            this.colWoodPlanWebAdminUser,
            this.colWoodPlanWebFileManagerAdmin,
            this.colWoodPlanWebWorkOrderDoneDate,
            this.colWorkPhone});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Images = this.imageCollection1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSurname, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colForename, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShownEditor += new System.EventHandler(this.gridView1_ShownEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colStaffID
            // 
            this.colStaffID.Caption = "Staff ID";
            this.colStaffID.FieldName = "StaffID";
            this.colStaffID.Name = "colStaffID";
            this.colStaffID.OptionsColumn.AllowEdit = false;
            this.colStaffID.OptionsColumn.AllowFocus = false;
            this.colStaffID.OptionsColumn.ReadOnly = true;
            this.colStaffID.Width = 49;
            // 
            // colNetworkID
            // 
            this.colNetworkID.Caption = "Network ID";
            this.colNetworkID.FieldName = "NetworkID";
            this.colNetworkID.Name = "colNetworkID";
            this.colNetworkID.OptionsColumn.AllowEdit = false;
            this.colNetworkID.OptionsColumn.AllowFocus = false;
            this.colNetworkID.OptionsColumn.ReadOnly = true;
            this.colNetworkID.Visible = true;
            this.colNetworkID.VisibleIndex = 4;
            this.colNetworkID.Width = 110;
            // 
            // colForename
            // 
            this.colForename.Caption = "Forename";
            this.colForename.FieldName = "Forename";
            this.colForename.Name = "colForename";
            this.colForename.OptionsColumn.AllowEdit = false;
            this.colForename.OptionsColumn.AllowFocus = false;
            this.colForename.OptionsColumn.ReadOnly = true;
            this.colForename.Visible = true;
            this.colForename.VisibleIndex = 2;
            this.colForename.Width = 106;
            // 
            // colSurname
            // 
            this.colSurname.Caption = "Surname";
            this.colSurname.FieldName = "Surname";
            this.colSurname.Name = "colSurname";
            this.colSurname.OptionsColumn.AllowEdit = false;
            this.colSurname.OptionsColumn.AllowFocus = false;
            this.colSurname.OptionsColumn.ReadOnly = true;
            this.colSurname.Visible = true;
            this.colSurname.VisibleIndex = 1;
            this.colSurname.Width = 134;
            // 
            // colStaffName
            // 
            this.colStaffName.Caption = "Staff Name";
            this.colStaffName.FieldName = "StaffName";
            this.colStaffName.Name = "colStaffName";
            this.colStaffName.OptionsColumn.AllowEdit = false;
            this.colStaffName.OptionsColumn.AllowFocus = false;
            this.colStaffName.OptionsColumn.ReadOnly = true;
            this.colStaffName.Visible = true;
            this.colStaffName.VisibleIndex = 3;
            this.colStaffName.Width = 175;
            // 
            // colUserActive
            // 
            this.colUserActive.Caption = "Active";
            this.colUserActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colUserActive.FieldName = "UserActive";
            this.colUserActive.Name = "colUserActive";
            this.colUserActive.OptionsColumn.AllowEdit = false;
            this.colUserActive.OptionsColumn.AllowFocus = false;
            this.colUserActive.OptionsColumn.ReadOnly = true;
            this.colUserActive.Visible = true;
            this.colUserActive.VisibleIndex = 8;
            this.colUserActive.Width = 51;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // colUserType
            // 
            this.colUserType.Caption = "User Type";
            this.colUserType.FieldName = "UserType";
            this.colUserType.Name = "colUserType";
            this.colUserType.OptionsColumn.AllowEdit = false;
            this.colUserType.OptionsColumn.AllowFocus = false;
            this.colUserType.OptionsColumn.ReadOnly = true;
            this.colUserType.Visible = true;
            this.colUserType.VisibleIndex = 5;
            this.colUserType.Width = 82;
            // 
            // colToDoListVisible
            // 
            this.colToDoListVisible.Caption = "To Do List";
            this.colToDoListVisible.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colToDoListVisible.FieldName = "ToDoListVisible";
            this.colToDoListVisible.Name = "colToDoListVisible";
            this.colToDoListVisible.OptionsColumn.AllowEdit = false;
            this.colToDoListVisible.OptionsColumn.AllowFocus = false;
            this.colToDoListVisible.OptionsColumn.ReadOnly = true;
            this.colToDoListVisible.Width = 68;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // colCommentBankVisible
            // 
            this.colCommentBankVisible.Caption = "Comment Bank";
            this.colCommentBankVisible.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCommentBankVisible.FieldName = "CommentBankVisible";
            this.colCommentBankVisible.Name = "colCommentBankVisible";
            this.colCommentBankVisible.OptionsColumn.AllowEdit = false;
            this.colCommentBankVisible.OptionsColumn.AllowFocus = false;
            this.colCommentBankVisible.OptionsColumn.ReadOnly = true;
            this.colCommentBankVisible.Width = 92;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            // 
            // colLoginCreated
            // 
            this.colLoginCreated.Caption = "Login";
            this.colLoginCreated.CustomizationCaption = "Create Login \\ Set Password";
            this.colLoginCreated.FieldName = "LoginCreated";
            this.colLoginCreated.ImageOptions.ImageIndex = 3;
            this.colLoginCreated.Name = "colLoginCreated";
            this.colLoginCreated.OptionsColumn.ReadOnly = true;
            this.colLoginCreated.Visible = true;
            this.colLoginCreated.VisibleIndex = 0;
            this.colLoginCreated.Width = 107;
            // 
            // colShowTipsOnStart
            // 
            this.colShowTipsOnStart.Caption = "Show Tips On Start";
            this.colShowTipsOnStart.FieldName = "ShowTipsOnStart";
            this.colShowTipsOnStart.Name = "colShowTipsOnStart";
            this.colShowTipsOnStart.OptionsColumn.AllowEdit = false;
            this.colShowTipsOnStart.OptionsColumn.AllowFocus = false;
            this.colShowTipsOnStart.OptionsColumn.ReadOnly = true;
            this.colShowTipsOnStart.Width = 113;
            // 
            // colLastTipID
            // 
            this.colLastTipID.Caption = "Last Tip ID";
            this.colLastTipID.FieldName = "LastTipID";
            this.colLastTipID.Name = "colLastTipID";
            this.colLastTipID.OptionsColumn.AllowEdit = false;
            this.colLastTipID.OptionsColumn.AllowFocus = false;
            this.colLastTipID.OptionsColumn.ReadOnly = true;
            this.colLastTipID.Width = 62;
            // 
            // colUserConfig
            // 
            this.colUserConfig.Caption = "User Configuration";
            this.colUserConfig.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colUserConfig.FieldName = "UserConfig";
            this.colUserConfig.Name = "colUserConfig";
            this.colUserConfig.OptionsColumn.AllowEdit = false;
            this.colUserConfig.OptionsColumn.AllowFocus = false;
            this.colUserConfig.OptionsColumn.ReadOnly = true;
            this.colUserConfig.Visible = true;
            this.colUserConfig.VisibleIndex = 10;
            this.colUserConfig.Width = 111;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            // 
            // colEmailAddress
            // 
            this.colEmailAddress.Caption = "Email Address";
            this.colEmailAddress.FieldName = "EmailAddress";
            this.colEmailAddress.Name = "colEmailAddress";
            this.colEmailAddress.OptionsColumn.AllowEdit = false;
            this.colEmailAddress.OptionsColumn.AllowFocus = false;
            this.colEmailAddress.OptionsColumn.ReadOnly = true;
            this.colEmailAddress.Visible = true;
            this.colEmailAddress.VisibleIndex = 6;
            this.colEmailAddress.Width = 140;
            // 
            // colShowConfirmations
            // 
            this.colShowConfirmations.Caption = "Show Confirmations";
            this.colShowConfirmations.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colShowConfirmations.FieldName = "ShowConfirmations";
            this.colShowConfirmations.Name = "colShowConfirmations";
            this.colShowConfirmations.OptionsColumn.AllowEdit = false;
            this.colShowConfirmations.OptionsColumn.AllowFocus = false;
            this.colShowConfirmations.OptionsColumn.ReadOnly = true;
            this.colShowConfirmations.Visible = true;
            this.colShowConfirmations.VisibleIndex = 11;
            this.colShowConfirmations.Width = 116;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // colMinimiseToTaskBar
            // 
            this.colMinimiseToTaskBar.Caption = "Minimise To TaskBar";
            this.colMinimiseToTaskBar.ColumnEdit = this.repositoryItemCheckEdit7;
            this.colMinimiseToTaskBar.FieldName = "MinimiseToTaskBar";
            this.colMinimiseToTaskBar.Name = "colMinimiseToTaskBar";
            this.colMinimiseToTaskBar.OptionsColumn.AllowEdit = false;
            this.colMinimiseToTaskBar.OptionsColumn.AllowFocus = false;
            this.colMinimiseToTaskBar.OptionsColumn.ReadOnly = true;
            this.colMinimiseToTaskBar.Visible = true;
            this.colMinimiseToTaskBar.VisibleIndex = 12;
            this.colMinimiseToTaskBar.Width = 116;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.ValueChecked = 1;
            this.repositoryItemCheckEdit7.ValueUnchecked = 0;
            // 
            // colDefaultFontName
            // 
            this.colDefaultFontName.Caption = "Default Font Name";
            this.colDefaultFontName.FieldName = "DefaultFontName";
            this.colDefaultFontName.Name = "colDefaultFontName";
            this.colDefaultFontName.OptionsColumn.AllowEdit = false;
            this.colDefaultFontName.OptionsColumn.AllowFocus = false;
            this.colDefaultFontName.OptionsColumn.ReadOnly = true;
            this.colDefaultFontName.Visible = true;
            this.colDefaultFontName.VisibleIndex = 13;
            this.colDefaultFontName.Width = 111;
            // 
            // colDefaultFontSize
            // 
            this.colDefaultFontSize.Caption = "Default Font Size";
            this.colDefaultFontSize.FieldName = "DefaultFontSize";
            this.colDefaultFontSize.Name = "colDefaultFontSize";
            this.colDefaultFontSize.OptionsColumn.AllowEdit = false;
            this.colDefaultFontSize.OptionsColumn.AllowFocus = false;
            this.colDefaultFontSize.OptionsColumn.ReadOnly = true;
            this.colDefaultFontSize.Visible = true;
            this.colDefaultFontSize.VisibleIndex = 14;
            this.colDefaultFontSize.Width = 103;
            // 
            // colShowPhotoPopup
            // 
            this.colShowPhotoPopup.Caption = "Show Picture Panel";
            this.colShowPhotoPopup.ColumnEdit = this.repositoryItemCheckEdit8;
            this.colShowPhotoPopup.FieldName = "ShowPhotoPopup";
            this.colShowPhotoPopup.Name = "colShowPhotoPopup";
            this.colShowPhotoPopup.OptionsColumn.AllowEdit = false;
            this.colShowPhotoPopup.OptionsColumn.AllowFocus = false;
            this.colShowPhotoPopup.OptionsColumn.ReadOnly = true;
            this.colShowPhotoPopup.Visible = true;
            this.colShowPhotoPopup.VisibleIndex = 15;
            this.colShowPhotoPopup.Width = 112;
            // 
            // repositoryItemCheckEdit8
            // 
            this.repositoryItemCheckEdit8.AutoHeight = false;
            this.repositoryItemCheckEdit8.Caption = "Check";
            this.repositoryItemCheckEdit8.Name = "repositoryItemCheckEdit8";
            this.repositoryItemCheckEdit8.ValueChecked = 1;
            this.repositoryItemCheckEdit8.ValueUnchecked = 0;
            // 
            // colFirstScreenLoaded
            // 
            this.colFirstScreenLoaded.Caption = "First Screen Loaded";
            this.colFirstScreenLoaded.FieldName = "FirstScreenLoaded";
            this.colFirstScreenLoaded.Name = "colFirstScreenLoaded";
            this.colFirstScreenLoaded.OptionsColumn.AllowEdit = false;
            this.colFirstScreenLoaded.OptionsColumn.AllowFocus = false;
            this.colFirstScreenLoaded.OptionsColumn.ReadOnly = true;
            this.colFirstScreenLoaded.Visible = true;
            this.colFirstScreenLoaded.VisibleIndex = 17;
            this.colFirstScreenLoaded.Width = 181;
            // 
            // colFirstScreenLoadedID
            // 
            this.colFirstScreenLoadedID.Caption = "First Screen Loaded ID";
            this.colFirstScreenLoadedID.FieldName = "FirstScreenLoadedID";
            this.colFirstScreenLoadedID.Name = "colFirstScreenLoadedID";
            this.colFirstScreenLoadedID.OptionsColumn.AllowEdit = false;
            this.colFirstScreenLoadedID.OptionsColumn.AllowFocus = false;
            this.colFirstScreenLoadedID.OptionsColumn.ReadOnly = true;
            // 
            // colEstatePlanInspectionControlPanel
            // 
            this.colEstatePlanInspectionControlPanel.Caption = "Site Inspection Control Panel";
            this.colEstatePlanInspectionControlPanel.ColumnEdit = this.repositoryItemCheckEditSiteInspectionControlPanel;
            this.colEstatePlanInspectionControlPanel.FieldName = "EstatePlanInspectionControlPanel";
            this.colEstatePlanInspectionControlPanel.Name = "colEstatePlanInspectionControlPanel";
            this.colEstatePlanInspectionControlPanel.OptionsColumn.AllowEdit = false;
            this.colEstatePlanInspectionControlPanel.OptionsColumn.AllowFocus = false;
            this.colEstatePlanInspectionControlPanel.OptionsColumn.ReadOnly = true;
            this.colEstatePlanInspectionControlPanel.Visible = true;
            this.colEstatePlanInspectionControlPanel.VisibleIndex = 16;
            this.colEstatePlanInspectionControlPanel.Width = 162;
            // 
            // repositoryItemCheckEditSiteInspectionControlPanel
            // 
            this.repositoryItemCheckEditSiteInspectionControlPanel.AutoHeight = false;
            this.repositoryItemCheckEditSiteInspectionControlPanel.Caption = "Check";
            this.repositoryItemCheckEditSiteInspectionControlPanel.Name = "repositoryItemCheckEditSiteInspectionControlPanel";
            this.repositoryItemCheckEditSiteInspectionControlPanel.ValueChecked = 1;
            this.repositoryItemCheckEditSiteInspectionControlPanel.ValueUnchecked = 0;
            // 
            // colWebSiteUserName
            // 
            this.colWebSiteUserName.Caption = "Website Username";
            this.colWebSiteUserName.FieldName = "WebSiteUserName";
            this.colWebSiteUserName.Name = "colWebSiteUserName";
            this.colWebSiteUserName.OptionsColumn.AllowEdit = false;
            this.colWebSiteUserName.OptionsColumn.AllowFocus = false;
            this.colWebSiteUserName.OptionsColumn.ReadOnly = true;
            this.colWebSiteUserName.ToolTip = "Controls if the user can log in (username) to the Winter Maintenance and Woodplan" +
    " Web Sites.";
            this.colWebSiteUserName.Visible = true;
            this.colWebSiteUserName.VisibleIndex = 18;
            this.colWebSiteUserName.Width = 111;
            // 
            // colWebSitePassword
            // 
            this.colWebSitePassword.Caption = "Website Password";
            this.colWebSitePassword.FieldName = "WebSitePassword";
            this.colWebSitePassword.Name = "colWebSitePassword";
            this.colWebSitePassword.OptionsColumn.AllowEdit = false;
            this.colWebSitePassword.OptionsColumn.AllowFocus = false;
            this.colWebSitePassword.OptionsColumn.ReadOnly = true;
            this.colWebSitePassword.ToolTip = "Controls if the user can log in (password) to the Winter Maintenance and Woodplan" +
    " Web Sites.";
            this.colWebSitePassword.Visible = true;
            this.colWebSitePassword.VisibleIndex = 19;
            this.colWebSitePassword.Width = 109;
            // 
            // colIsUtilityArbSurveyor
            // 
            this.colIsUtilityArbSurveyor.Caption = "Utility ARB Surveyor";
            this.colIsUtilityArbSurveyor.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colIsUtilityArbSurveyor.FieldName = "IsUtilityArbSurveyor";
            this.colIsUtilityArbSurveyor.Name = "colIsUtilityArbSurveyor";
            this.colIsUtilityArbSurveyor.OptionsColumn.AllowEdit = false;
            this.colIsUtilityArbSurveyor.OptionsColumn.AllowFocus = false;
            this.colIsUtilityArbSurveyor.OptionsColumn.ReadOnly = true;
            this.colIsUtilityArbSurveyor.Visible = true;
            this.colIsUtilityArbSurveyor.VisibleIndex = 9;
            this.colIsUtilityArbSurveyor.Width = 118;
            // 
            // colWoodPlanWebActionDoneDate
            // 
            this.colWoodPlanWebActionDoneDate.Caption = "Web - Set Actions Completed";
            this.colWoodPlanWebActionDoneDate.ColumnEdit = this.repositoryItemCheckEditSiteInspectionControlPanel;
            this.colWoodPlanWebActionDoneDate.FieldName = "WoodPlanWebActionDoneDate";
            this.colWoodPlanWebActionDoneDate.Name = "colWoodPlanWebActionDoneDate";
            this.colWoodPlanWebActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colWoodPlanWebActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colWoodPlanWebActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colWoodPlanWebActionDoneDate.ToolTip = "Controls if user can Set Actions as Completed from the WoodPlan Web Site.";
            this.colWoodPlanWebActionDoneDate.Visible = true;
            this.colWoodPlanWebActionDoneDate.VisibleIndex = 22;
            this.colWoodPlanWebActionDoneDate.Width = 161;
            // 
            // colWoodPlanWebAdminUser
            // 
            this.colWoodPlanWebAdminUser.Caption = "Web - Administrator Access";
            this.colWoodPlanWebAdminUser.ColumnEdit = this.repositoryItemCheckEditSiteInspectionControlPanel;
            this.colWoodPlanWebAdminUser.FieldName = "WoodPlanWebAdminUser";
            this.colWoodPlanWebAdminUser.Name = "colWoodPlanWebAdminUser";
            this.colWoodPlanWebAdminUser.OptionsColumn.AllowEdit = false;
            this.colWoodPlanWebAdminUser.OptionsColumn.AllowFocus = false;
            this.colWoodPlanWebAdminUser.OptionsColumn.ReadOnly = true;
            this.colWoodPlanWebAdminUser.ToolTip = "Controls if user has Administrator Access (select from list of all clients when l" +
    "ogging in) from the WoodPlan Web Site.";
            this.colWoodPlanWebAdminUser.Visible = true;
            this.colWoodPlanWebAdminUser.VisibleIndex = 20;
            this.colWoodPlanWebAdminUser.Width = 153;
            // 
            // colWoodPlanWebFileManagerAdmin
            // 
            this.colWoodPlanWebFileManagerAdmin.Caption = "Web - File Manger Admin";
            this.colWoodPlanWebFileManagerAdmin.ColumnEdit = this.repositoryItemCheckEditSiteInspectionControlPanel;
            this.colWoodPlanWebFileManagerAdmin.FieldName = "WoodPlanWebFileManagerAdmin";
            this.colWoodPlanWebFileManagerAdmin.Name = "colWoodPlanWebFileManagerAdmin";
            this.colWoodPlanWebFileManagerAdmin.OptionsColumn.AllowEdit = false;
            this.colWoodPlanWebFileManagerAdmin.OptionsColumn.AllowFocus = false;
            this.colWoodPlanWebFileManagerAdmin.OptionsColumn.ReadOnly = true;
            this.colWoodPlanWebFileManagerAdmin.ToolTip = "Controls if user has File Manager Access from the WoodPlan Web Site.";
            this.colWoodPlanWebFileManagerAdmin.Visible = true;
            this.colWoodPlanWebFileManagerAdmin.VisibleIndex = 21;
            this.colWoodPlanWebFileManagerAdmin.Width = 140;
            // 
            // colWoodPlanWebWorkOrderDoneDate
            // 
            this.colWoodPlanWebWorkOrderDoneDate.Caption = "Web - Set Work Orders Completed";
            this.colWoodPlanWebWorkOrderDoneDate.ColumnEdit = this.repositoryItemCheckEditSiteInspectionControlPanel;
            this.colWoodPlanWebWorkOrderDoneDate.FieldName = "WoodPlanWebWorkOrderDoneDate";
            this.colWoodPlanWebWorkOrderDoneDate.Name = "colWoodPlanWebWorkOrderDoneDate";
            this.colWoodPlanWebWorkOrderDoneDate.OptionsColumn.AllowEdit = false;
            this.colWoodPlanWebWorkOrderDoneDate.OptionsColumn.AllowFocus = false;
            this.colWoodPlanWebWorkOrderDoneDate.OptionsColumn.ReadOnly = true;
            this.colWoodPlanWebWorkOrderDoneDate.ToolTip = "Controls if user can Set Work Order as Completed from the WoodPlan Web Site.";
            this.colWoodPlanWebWorkOrderDoneDate.Visible = true;
            this.colWoodPlanWebWorkOrderDoneDate.VisibleIndex = 23;
            this.colWoodPlanWebWorkOrderDoneDate.Width = 187;
            // 
            // colWorkPhone
            // 
            this.colWorkPhone.Caption = "Work Phone #";
            this.colWorkPhone.FieldName = "WorkPhone";
            this.colWorkPhone.Name = "colWorkPhone";
            this.colWorkPhone.OptionsColumn.AllowEdit = false;
            this.colWorkPhone.OptionsColumn.AllowFocus = false;
            this.colWorkPhone.OptionsColumn.ReadOnly = true;
            this.colWorkPhone.Visible = true;
            this.colWorkPhone.VisibleIndex = 7;
            this.colWorkPhone.Width = 90;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage1;
            this.xtraTabControl1.Size = new System.Drawing.Size(870, 303);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridControl2);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(865, 277);
            this.xtraTabPage1.Text = "Screen Layouts";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp00196StaffManagerLinkedSavedScreensBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit9});
            this.gridControl2.Size = new System.Drawing.Size(865, 277);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2,
            this.gridView3});
            // 
            // sp00196StaffManagerLinkedSavedScreensBindingSource
            // 
            this.sp00196StaffManagerLinkedSavedScreensBindingSource.DataMember = "sp00196_Staff_Manager_Linked_Saved_Screens";
            this.sp00196StaffManagerLinkedSavedScreensBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLayoutID,
            this.colScreenID,
            this.colScreenName,
            this.colCreatedByID,
            this.colCreatedByName,
            this.colLayoutDescription,
            this.colLayoutRemarks,
            this.colX,
            this.colY,
            this.colWidth,
            this.colHeight,
            this.colState,
            this.colDefaultLayout,
            this.colShareTypeID,
            this.colShareTypeDescription});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colCreatedByName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colScreenName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLayoutDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_GotFocus);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colLayoutID
            // 
            this.colLayoutID.Caption = "Layout ID";
            this.colLayoutID.FieldName = "LayoutID";
            this.colLayoutID.Name = "colLayoutID";
            this.colLayoutID.OptionsColumn.AllowEdit = false;
            this.colLayoutID.OptionsColumn.AllowFocus = false;
            this.colLayoutID.OptionsColumn.ReadOnly = true;
            this.colLayoutID.Width = 68;
            // 
            // colScreenID
            // 
            this.colScreenID.Caption = "Screen ID";
            this.colScreenID.FieldName = "ScreenID";
            this.colScreenID.Name = "colScreenID";
            this.colScreenID.OptionsColumn.AllowEdit = false;
            this.colScreenID.OptionsColumn.AllowFocus = false;
            this.colScreenID.OptionsColumn.ReadOnly = true;
            this.colScreenID.Width = 68;
            // 
            // colScreenName
            // 
            this.colScreenName.Caption = "Screen Name";
            this.colScreenName.FieldName = "ScreenName";
            this.colScreenName.Name = "colScreenName";
            this.colScreenName.OptionsColumn.AllowEdit = false;
            this.colScreenName.OptionsColumn.AllowFocus = false;
            this.colScreenName.OptionsColumn.ReadOnly = true;
            this.colScreenName.Visible = true;
            this.colScreenName.VisibleIndex = 0;
            this.colScreenName.Width = 279;
            // 
            // colCreatedByID
            // 
            this.colCreatedByID.Caption = "Staff ID";
            this.colCreatedByID.FieldName = "CreatedByID";
            this.colCreatedByID.Name = "colCreatedByID";
            this.colCreatedByID.OptionsColumn.AllowEdit = false;
            this.colCreatedByID.OptionsColumn.AllowFocus = false;
            this.colCreatedByID.OptionsColumn.ReadOnly = true;
            this.colCreatedByID.Width = 59;
            // 
            // colCreatedByName
            // 
            this.colCreatedByName.Caption = "Staff Name";
            this.colCreatedByName.FieldName = "CreatedByName";
            this.colCreatedByName.Name = "colCreatedByName";
            this.colCreatedByName.OptionsColumn.AllowEdit = false;
            this.colCreatedByName.OptionsColumn.AllowFocus = false;
            this.colCreatedByName.OptionsColumn.ReadOnly = true;
            this.colCreatedByName.Width = 254;
            // 
            // colLayoutDescription
            // 
            this.colLayoutDescription.Caption = "Layout Name";
            this.colLayoutDescription.FieldName = "LayoutDescription";
            this.colLayoutDescription.Name = "colLayoutDescription";
            this.colLayoutDescription.OptionsColumn.AllowEdit = false;
            this.colLayoutDescription.OptionsColumn.AllowFocus = false;
            this.colLayoutDescription.OptionsColumn.ReadOnly = true;
            this.colLayoutDescription.Visible = true;
            this.colLayoutDescription.VisibleIndex = 1;
            this.colLayoutDescription.Width = 296;
            // 
            // colLayoutRemarks
            // 
            this.colLayoutRemarks.Caption = "Remarks";
            this.colLayoutRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colLayoutRemarks.FieldName = "LayoutRemarks";
            this.colLayoutRemarks.Name = "colLayoutRemarks";
            this.colLayoutRemarks.OptionsColumn.ReadOnly = true;
            this.colLayoutRemarks.Visible = true;
            this.colLayoutRemarks.VisibleIndex = 4;
            this.colLayoutRemarks.Width = 121;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colX
            // 
            this.colX.Caption = "Screen X";
            this.colX.FieldName = "X";
            this.colX.Name = "colX";
            this.colX.OptionsColumn.AllowEdit = false;
            this.colX.OptionsColumn.AllowFocus = false;
            this.colX.OptionsColumn.ReadOnly = true;
            this.colX.Width = 63;
            // 
            // colY
            // 
            this.colY.Caption = "Screen Y";
            this.colY.FieldName = "Y";
            this.colY.Name = "colY";
            this.colY.OptionsColumn.AllowEdit = false;
            this.colY.OptionsColumn.AllowFocus = false;
            this.colY.OptionsColumn.ReadOnly = true;
            this.colY.Width = 63;
            // 
            // colWidth
            // 
            this.colWidth.Caption = "Screen Width";
            this.colWidth.FieldName = "Width";
            this.colWidth.Name = "colWidth";
            this.colWidth.OptionsColumn.AllowEdit = false;
            this.colWidth.OptionsColumn.AllowFocus = false;
            this.colWidth.OptionsColumn.ReadOnly = true;
            this.colWidth.Width = 85;
            // 
            // colHeight
            // 
            this.colHeight.Caption = "Screen Height";
            this.colHeight.FieldName = "Height";
            this.colHeight.Name = "colHeight";
            this.colHeight.OptionsColumn.AllowEdit = false;
            this.colHeight.OptionsColumn.AllowFocus = false;
            this.colHeight.OptionsColumn.ReadOnly = true;
            this.colHeight.Width = 88;
            // 
            // colState
            // 
            this.colState.Caption = "Screen State";
            this.colState.FieldName = "State";
            this.colState.Name = "colState";
            this.colState.OptionsColumn.AllowEdit = false;
            this.colState.OptionsColumn.AllowFocus = false;
            this.colState.OptionsColumn.ReadOnly = true;
            this.colState.Width = 83;
            // 
            // colDefaultLayout
            // 
            this.colDefaultLayout.Caption = "Default Layout";
            this.colDefaultLayout.ColumnEdit = this.repositoryItemCheckEdit9;
            this.colDefaultLayout.FieldName = "DefaultLayout";
            this.colDefaultLayout.Name = "colDefaultLayout";
            this.colDefaultLayout.OptionsColumn.AllowEdit = false;
            this.colDefaultLayout.OptionsColumn.AllowFocus = false;
            this.colDefaultLayout.OptionsColumn.ReadOnly = true;
            this.colDefaultLayout.Visible = true;
            this.colDefaultLayout.VisibleIndex = 2;
            this.colDefaultLayout.Width = 92;
            // 
            // repositoryItemCheckEdit9
            // 
            this.repositoryItemCheckEdit9.AutoHeight = false;
            this.repositoryItemCheckEdit9.Caption = "Check";
            this.repositoryItemCheckEdit9.Name = "repositoryItemCheckEdit9";
            this.repositoryItemCheckEdit9.ValueChecked = 1;
            this.repositoryItemCheckEdit9.ValueUnchecked = 0;
            // 
            // colShareTypeID
            // 
            this.colShareTypeID.Caption = "Share Type ID";
            this.colShareTypeID.FieldName = "ShareTypeID";
            this.colShareTypeID.Name = "colShareTypeID";
            this.colShareTypeID.OptionsColumn.AllowEdit = false;
            this.colShareTypeID.OptionsColumn.AllowFocus = false;
            this.colShareTypeID.OptionsColumn.ReadOnly = true;
            this.colShareTypeID.Width = 90;
            // 
            // colShareTypeDescription
            // 
            this.colShareTypeDescription.Caption = "Share Type";
            this.colShareTypeDescription.FieldName = "ShareTypeDescription";
            this.colShareTypeDescription.Name = "colShareTypeDescription";
            this.colShareTypeDescription.OptionsColumn.AllowEdit = false;
            this.colShareTypeDescription.OptionsColumn.AllowFocus = false;
            this.colShareTypeDescription.OptionsColumn.ReadOnly = true;
            this.colShareTypeDescription.Visible = true;
            this.colShareTypeDescription.VisibleIndex = 3;
            this.colShareTypeDescription.Width = 159;
            // 
            // gridView3
            // 
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl14);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(865, 277);
            this.xtraTabPage2.Text = "Visit Types";
            // 
            // gridControl14
            // 
            this.gridControl14.DataSource = this.sp06309PersonVisitTypesBindingSource;
            this.gridControl14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl14.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl14.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl14.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.gridControl14.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl14_EmbeddedNavigator_ButtonClick);
            this.gridControl14.Location = new System.Drawing.Point(0, 0);
            this.gridControl14.MainView = this.gridView14;
            this.gridControl14.MenuManager = this.barManager1;
            this.gridControl14.Name = "gridControl14";
            this.gridControl14.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit11});
            this.gridControl14.Size = new System.Drawing.Size(865, 277);
            this.gridControl14.TabIndex = 2;
            this.gridControl14.UseEmbeddedNavigator = true;
            this.gridControl14.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView14});
            // 
            // sp06309PersonVisitTypesBindingSource
            // 
            this.sp06309PersonVisitTypesBindingSource.DataMember = "sp06309_Person_Visit_Types";
            this.sp06309PersonVisitTypesBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // dataSet_OM_Core
            // 
            this.dataSet_OM_Core.DataSetName = "DataSet_OM_Core";
            this.dataSet_OM_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView14
            // 
            this.gridView14.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitTypePersonID,
            this.colLinkedToPersonID,
            this.colLinkedToPersonTypeID,
            this.colVisitTypeID,
            this.colRemarks8,
            this.colLinkedToPersonName,
            this.colVisitTypeDescription});
            this.gridView14.GridControl = this.gridControl14;
            this.gridView14.GroupCount = 1;
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView14.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView14.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView14.OptionsLayout.StoreAppearance = true;
            this.gridView14.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView14.OptionsSelection.MultiSelect = true;
            this.gridView14.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView14.OptionsView.ColumnAutoWidth = false;
            this.gridView14.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            this.gridView14.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToPersonName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colVisitTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView14.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView14.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView14.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView14.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView14.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView14.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView14_MouseUp);
            this.gridView14.DoubleClick += new System.EventHandler(this.gridView14_DoubleClick);
            this.gridView14.GotFocus += new System.EventHandler(this.gridView14_GotFocus);
            // 
            // colVisitTypePersonID
            // 
            this.colVisitTypePersonID.Caption = "Visit Type Person ID";
            this.colVisitTypePersonID.FieldName = "VisitTypePersonID";
            this.colVisitTypePersonID.Name = "colVisitTypePersonID";
            this.colVisitTypePersonID.OptionsColumn.AllowEdit = false;
            this.colVisitTypePersonID.OptionsColumn.AllowFocus = false;
            this.colVisitTypePersonID.OptionsColumn.ReadOnly = true;
            this.colVisitTypePersonID.Width = 115;
            // 
            // colLinkedToPersonID
            // 
            this.colLinkedToPersonID.Caption = "Linked To Person ID";
            this.colLinkedToPersonID.FieldName = "LinkedToPersonID";
            this.colLinkedToPersonID.Name = "colLinkedToPersonID";
            this.colLinkedToPersonID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonID.Width = 114;
            // 
            // colLinkedToPersonTypeID
            // 
            this.colLinkedToPersonTypeID.Caption = "Linked To Person Type ID";
            this.colLinkedToPersonTypeID.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.Name = "colLinkedToPersonTypeID";
            this.colLinkedToPersonTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID.Width = 141;
            // 
            // colVisitTypeID
            // 
            this.colVisitTypeID.Caption = "Visit Type ID";
            this.colVisitTypeID.FieldName = "VisitTypeID";
            this.colVisitTypeID.Name = "colVisitTypeID";
            this.colVisitTypeID.OptionsColumn.AllowEdit = false;
            this.colVisitTypeID.OptionsColumn.AllowFocus = false;
            this.colVisitTypeID.OptionsColumn.ReadOnly = true;
            this.colVisitTypeID.Width = 79;
            // 
            // colRemarks8
            // 
            this.colRemarks8.Caption = "Remarks";
            this.colRemarks8.ColumnEdit = this.repositoryItemMemoExEdit11;
            this.colRemarks8.FieldName = "Remarks";
            this.colRemarks8.Name = "colRemarks8";
            this.colRemarks8.OptionsColumn.ReadOnly = true;
            this.colRemarks8.Visible = true;
            this.colRemarks8.VisibleIndex = 1;
            this.colRemarks8.Width = 382;
            // 
            // repositoryItemMemoExEdit11
            // 
            this.repositoryItemMemoExEdit11.AutoHeight = false;
            this.repositoryItemMemoExEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit11.Name = "repositoryItemMemoExEdit11";
            this.repositoryItemMemoExEdit11.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit11.ShowIcon = false;
            // 
            // colLinkedToPersonName
            // 
            this.colLinkedToPersonName.Caption = "Linked To Staff";
            this.colLinkedToPersonName.FieldName = "LinkedToPersonName";
            this.colLinkedToPersonName.Name = "colLinkedToPersonName";
            this.colLinkedToPersonName.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonName.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonName.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonName.Visible = true;
            this.colLinkedToPersonName.VisibleIndex = 2;
            this.colLinkedToPersonName.Width = 283;
            // 
            // colVisitTypeDescription
            // 
            this.colVisitTypeDescription.Caption = "Visit Type";
            this.colVisitTypeDescription.FieldName = "VisitTypeDescription";
            this.colVisitTypeDescription.Name = "colVisitTypeDescription";
            this.colVisitTypeDescription.OptionsColumn.AllowEdit = false;
            this.colVisitTypeDescription.OptionsColumn.AllowFocus = false;
            this.colVisitTypeDescription.OptionsColumn.ReadOnly = true;
            this.colVisitTypeDescription.Visible = true;
            this.colVisitTypeDescription.VisibleIndex = 0;
            this.colVisitTypeDescription.Width = 224;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.splitContainerControl4);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(865, 277);
            this.xtraTabPage3.Text = "Allocated Devices";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel1.Controls.Add(this.gridSplitContainer6);
            this.splitContainerControl4.Panel1.ShowCaption = true;
            this.splitContainerControl4.Panel1.Text = "Devices";
            this.splitContainerControl4.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl4.Panel2.Controls.Add(this.gridControl13);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Team Members Assigned To Device";
            this.splitContainerControl4.Size = new System.Drawing.Size(865, 277);
            this.splitContainerControl4.SplitterPosition = 491;
            this.splitContainerControl4.TabIndex = 2;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridSplitContainer6
            // 
            this.gridSplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer6.Grid = this.gridControl7;
            this.gridSplitContainer6.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer6.Name = "gridSplitContainer6";
            this.gridSplitContainer6.Panel1.Controls.Add(this.gridControl7);
            this.gridSplitContainer6.Size = new System.Drawing.Size(364, 253);
            this.gridSplitContainer6.TabIndex = 0;
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp04027GCSubContractorAllocatedPDAsBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Open Master Device Manager", "pda_manager")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit6,
            this.repositoryItemTextEditDateTime7});
            this.gridControl7.Size = new System.Drawing.Size(364, 253);
            this.gridControl7.TabIndex = 0;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp04027GCSubContractorAllocatedPDAsBindingSource
            // 
            this.sp04027GCSubContractorAllocatedPDAsBindingSource.DataMember = "sp04027_GC_SubContractor_Allocated_PDAs";
            this.sp04027GCSubContractorAllocatedPDAsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAllocatedPdaID,
            this.colSubContractorID1,
            this.colSubContractorName1,
            this.colPdaID,
            this.colPdaMake,
            this.colPdaModel,
            this.colPdaSerialNumber,
            this.colPdaFirmwareVersion,
            this.colPdaMobileTelNo,
            this.colPdaDatePurchased,
            this.colSoftwareVersion,
            this.colIMEI,
            this.colDateAllocated,
            this.colRemarks4,
            this.colPDAUserName1,
            this.colPDAPassword1,
            this.colPDALoginToken1,
            this.colPDACode,
            this.colLinkedToPersonType,
            this.colLinkedToPersonTypeID1});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubContractorName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIMEI, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colAllocatedPdaID
            // 
            this.colAllocatedPdaID.Caption = "Allocated Device ID";
            this.colAllocatedPdaID.FieldName = "AllocatedPdaID";
            this.colAllocatedPdaID.Name = "colAllocatedPdaID";
            this.colAllocatedPdaID.OptionsColumn.AllowEdit = false;
            this.colAllocatedPdaID.OptionsColumn.AllowFocus = false;
            this.colAllocatedPdaID.OptionsColumn.ReadOnly = true;
            this.colAllocatedPdaID.Width = 102;
            // 
            // colSubContractorID1
            // 
            this.colSubContractorID1.Caption = "Staff ID";
            this.colSubContractorID1.FieldName = "SubContractorID";
            this.colSubContractorID1.Name = "colSubContractorID1";
            this.colSubContractorID1.OptionsColumn.AllowEdit = false;
            this.colSubContractorID1.OptionsColumn.AllowFocus = false;
            this.colSubContractorID1.OptionsColumn.ReadOnly = true;
            // 
            // colSubContractorName1
            // 
            this.colSubContractorName1.Caption = "Staff Name";
            this.colSubContractorName1.FieldName = "SubContractorName";
            this.colSubContractorName1.Name = "colSubContractorName1";
            this.colSubContractorName1.OptionsColumn.AllowEdit = false;
            this.colSubContractorName1.OptionsColumn.AllowFocus = false;
            this.colSubContractorName1.OptionsColumn.ReadOnly = true;
            this.colSubContractorName1.Width = 195;
            // 
            // colPdaID
            // 
            this.colPdaID.Caption = "Device ID";
            this.colPdaID.FieldName = "PdaID";
            this.colPdaID.Name = "colPdaID";
            this.colPdaID.OptionsColumn.AllowEdit = false;
            this.colPdaID.OptionsColumn.AllowFocus = false;
            this.colPdaID.OptionsColumn.ReadOnly = true;
            // 
            // colPdaMake
            // 
            this.colPdaMake.Caption = "Device Make";
            this.colPdaMake.FieldName = "PdaMake";
            this.colPdaMake.Name = "colPdaMake";
            this.colPdaMake.OptionsColumn.AllowEdit = false;
            this.colPdaMake.OptionsColumn.AllowFocus = false;
            this.colPdaMake.OptionsColumn.ReadOnly = true;
            this.colPdaMake.Visible = true;
            this.colPdaMake.VisibleIndex = 4;
            this.colPdaMake.Width = 93;
            // 
            // colPdaModel
            // 
            this.colPdaModel.Caption = "Device Model";
            this.colPdaModel.FieldName = "PdaModel";
            this.colPdaModel.Name = "colPdaModel";
            this.colPdaModel.OptionsColumn.AllowEdit = false;
            this.colPdaModel.OptionsColumn.AllowFocus = false;
            this.colPdaModel.OptionsColumn.ReadOnly = true;
            this.colPdaModel.Visible = true;
            this.colPdaModel.VisibleIndex = 5;
            this.colPdaModel.Width = 95;
            // 
            // colPdaSerialNumber
            // 
            this.colPdaSerialNumber.Caption = "Device Serial No";
            this.colPdaSerialNumber.FieldName = "PdaSerialNumber";
            this.colPdaSerialNumber.Name = "colPdaSerialNumber";
            this.colPdaSerialNumber.OptionsColumn.AllowEdit = false;
            this.colPdaSerialNumber.OptionsColumn.AllowFocus = false;
            this.colPdaSerialNumber.OptionsColumn.ReadOnly = true;
            this.colPdaSerialNumber.Visible = true;
            this.colPdaSerialNumber.VisibleIndex = 6;
            this.colPdaSerialNumber.Width = 111;
            // 
            // colPdaFirmwareVersion
            // 
            this.colPdaFirmwareVersion.Caption = "Device Firmware";
            this.colPdaFirmwareVersion.FieldName = "PdaFirmwareVersion";
            this.colPdaFirmwareVersion.Name = "colPdaFirmwareVersion";
            this.colPdaFirmwareVersion.OptionsColumn.AllowEdit = false;
            this.colPdaFirmwareVersion.OptionsColumn.AllowFocus = false;
            this.colPdaFirmwareVersion.OptionsColumn.ReadOnly = true;
            this.colPdaFirmwareVersion.Visible = true;
            this.colPdaFirmwareVersion.VisibleIndex = 7;
            this.colPdaFirmwareVersion.Width = 99;
            // 
            // colPdaMobileTelNo
            // 
            this.colPdaMobileTelNo.Caption = "Device Mobile Tel";
            this.colPdaMobileTelNo.FieldName = "PdaMobileTelNo";
            this.colPdaMobileTelNo.Name = "colPdaMobileTelNo";
            this.colPdaMobileTelNo.OptionsColumn.AllowEdit = false;
            this.colPdaMobileTelNo.OptionsColumn.AllowFocus = false;
            this.colPdaMobileTelNo.OptionsColumn.ReadOnly = true;
            this.colPdaMobileTelNo.Visible = true;
            this.colPdaMobileTelNo.VisibleIndex = 2;
            this.colPdaMobileTelNo.Width = 101;
            // 
            // colPdaDatePurchased
            // 
            this.colPdaDatePurchased.Caption = "Device Purchase Date";
            this.colPdaDatePurchased.ColumnEdit = this.repositoryItemTextEditDateTime7;
            this.colPdaDatePurchased.FieldName = "PdaDatePurchased";
            this.colPdaDatePurchased.Name = "colPdaDatePurchased";
            this.colPdaDatePurchased.OptionsColumn.AllowEdit = false;
            this.colPdaDatePurchased.OptionsColumn.AllowFocus = false;
            this.colPdaDatePurchased.OptionsColumn.ReadOnly = true;
            this.colPdaDatePurchased.Visible = true;
            this.colPdaDatePurchased.VisibleIndex = 9;
            this.colPdaDatePurchased.Width = 124;
            // 
            // repositoryItemTextEditDateTime7
            // 
            this.repositoryItemTextEditDateTime7.AutoHeight = false;
            this.repositoryItemTextEditDateTime7.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime7.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime7.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime7.Name = "repositoryItemTextEditDateTime7";
            // 
            // colSoftwareVersion
            // 
            this.colSoftwareVersion.Caption = "Device Software Version";
            this.colSoftwareVersion.FieldName = "SoftwareVersion";
            this.colSoftwareVersion.Name = "colSoftwareVersion";
            this.colSoftwareVersion.OptionsColumn.AllowEdit = false;
            this.colSoftwareVersion.OptionsColumn.AllowFocus = false;
            this.colSoftwareVersion.OptionsColumn.ReadOnly = true;
            this.colSoftwareVersion.Visible = true;
            this.colSoftwareVersion.VisibleIndex = 8;
            this.colSoftwareVersion.Width = 136;
            // 
            // colIMEI
            // 
            this.colIMEI.Caption = "IMEI";
            this.colIMEI.FieldName = "IMEI";
            this.colIMEI.Name = "colIMEI";
            this.colIMEI.OptionsColumn.AllowEdit = false;
            this.colIMEI.OptionsColumn.AllowFocus = false;
            this.colIMEI.OptionsColumn.ReadOnly = true;
            this.colIMEI.Visible = true;
            this.colIMEI.VisibleIndex = 3;
            this.colIMEI.Width = 98;
            // 
            // colDateAllocated
            // 
            this.colDateAllocated.Caption = "Date Allocated";
            this.colDateAllocated.ColumnEdit = this.repositoryItemTextEditDateTime7;
            this.colDateAllocated.FieldName = "DateAllocated";
            this.colDateAllocated.Name = "colDateAllocated";
            this.colDateAllocated.OptionsColumn.AllowEdit = false;
            this.colDateAllocated.OptionsColumn.AllowFocus = false;
            this.colDateAllocated.OptionsColumn.ReadOnly = true;
            this.colDateAllocated.Visible = true;
            this.colDateAllocated.VisibleIndex = 0;
            this.colDateAllocated.Width = 91;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 10;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colPDAUserName1
            // 
            this.colPDAUserName1.Caption = "Device Username";
            this.colPDAUserName1.FieldName = "PDAUserName";
            this.colPDAUserName1.Name = "colPDAUserName1";
            this.colPDAUserName1.OptionsColumn.AllowEdit = false;
            this.colPDAUserName1.OptionsColumn.AllowFocus = false;
            this.colPDAUserName1.OptionsColumn.ReadOnly = true;
            this.colPDAUserName1.Visible = true;
            this.colPDAUserName1.VisibleIndex = 11;
            this.colPDAUserName1.Width = 102;
            // 
            // colPDAPassword1
            // 
            this.colPDAPassword1.Caption = "Device Password";
            this.colPDAPassword1.FieldName = "PDAPassword";
            this.colPDAPassword1.Name = "colPDAPassword1";
            this.colPDAPassword1.OptionsColumn.AllowEdit = false;
            this.colPDAPassword1.OptionsColumn.AllowFocus = false;
            this.colPDAPassword1.OptionsColumn.ReadOnly = true;
            this.colPDAPassword1.Visible = true;
            this.colPDAPassword1.VisibleIndex = 12;
            this.colPDAPassword1.Width = 100;
            // 
            // colPDALoginToken1
            // 
            this.colPDALoginToken1.Caption = "Device Token";
            this.colPDALoginToken1.FieldName = "PDALoginToken";
            this.colPDALoginToken1.Name = "colPDALoginToken1";
            this.colPDALoginToken1.OptionsColumn.AllowEdit = false;
            this.colPDALoginToken1.OptionsColumn.AllowFocus = false;
            this.colPDALoginToken1.OptionsColumn.ReadOnly = true;
            this.colPDALoginToken1.Visible = true;
            this.colPDALoginToken1.VisibleIndex = 13;
            this.colPDALoginToken1.Width = 231;
            // 
            // colPDACode
            // 
            this.colPDACode.Caption = "Device Code";
            this.colPDACode.FieldName = "PDACode";
            this.colPDACode.Name = "colPDACode";
            this.colPDACode.OptionsColumn.AllowEdit = false;
            this.colPDACode.OptionsColumn.AllowFocus = false;
            this.colPDACode.OptionsColumn.ReadOnly = true;
            this.colPDACode.Visible = true;
            this.colPDACode.VisibleIndex = 1;
            this.colPDACode.Width = 79;
            // 
            // colLinkedToPersonType
            // 
            this.colLinkedToPersonType.Caption = "Linked To Person Type";
            this.colLinkedToPersonType.FieldName = "LinkedToPersonType";
            this.colLinkedToPersonType.Name = "colLinkedToPersonType";
            this.colLinkedToPersonType.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonType.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonType.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonType.Width = 127;
            // 
            // colLinkedToPersonTypeID1
            // 
            this.colLinkedToPersonTypeID1.Caption = "Linked To Person Type ID";
            this.colLinkedToPersonTypeID1.FieldName = "LinkedToPersonTypeID";
            this.colLinkedToPersonTypeID1.Name = "colLinkedToPersonTypeID1";
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToPersonTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToPersonTypeID1.Width = 141;
            // 
            // gridControl13
            // 
            this.gridControl13.DataSource = this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource;
            this.gridControl13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl13.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl13.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl13.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl13.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl13_EmbeddedNavigator_ButtonClick);
            this.gridControl13.Location = new System.Drawing.Point(0, 0);
            this.gridControl13.MainView = this.gridView13;
            this.gridControl13.MenuManager = this.barManager1;
            this.gridControl13.Name = "gridControl13";
            this.gridControl13.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime13});
            this.gridControl13.Size = new System.Drawing.Size(487, 253);
            this.gridControl13.TabIndex = 0;
            this.gridControl13.UseEmbeddedNavigator = true;
            this.gridControl13.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView13});
            // 
            // sp04344GCAllocatedPDALinkedTeamMembersBindingSource
            // 
            this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource.DataMember = "sp04344_GC_Allocated_PDA_Linked_Team_Members";
            this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView13
            // 
            this.gridView13.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAllocatedPdaLinkedTeamMemberID,
            this.colAllocatedPDAID1,
            this.colTeamMemberID1,
            this.colPdaID1,
            this.colDateAllocated1,
            this.colPdaMake1,
            this.colPdaModel1,
            this.colPdaSerialNumber1,
            this.colPdaFirmwareVersion1,
            this.colPdaMobileTelNo1,
            this.colPdaDatePurchased1,
            this.colSoftwareVersion1,
            this.colIMEI1,
            this.colPDACode1,
            this.colSubcontractorName5});
            this.gridView13.GridControl = this.gridControl13;
            this.gridView13.GroupCount = 1;
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView13.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView13.OptionsLayout.StoreAppearance = true;
            this.gridView13.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView13.OptionsSelection.MultiSelect = true;
            this.gridView13.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView13.OptionsView.ColumnAutoWidth = false;
            this.gridView13.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            this.gridView13.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPdaSerialNumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubcontractorName5, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView13.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView13.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView13.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView13.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView13.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView13.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView13_MouseUp);
            this.gridView13.DoubleClick += new System.EventHandler(this.gridView13_DoubleClick);
            this.gridView13.GotFocus += new System.EventHandler(this.gridView13_GotFocus);
            // 
            // colAllocatedPdaLinkedTeamMemberID
            // 
            this.colAllocatedPdaLinkedTeamMemberID.Caption = "Allocated Device Linked Team Member ID";
            this.colAllocatedPdaLinkedTeamMemberID.FieldName = "AllocatedPdaLinkedTeamMemberID";
            this.colAllocatedPdaLinkedTeamMemberID.Name = "colAllocatedPdaLinkedTeamMemberID";
            this.colAllocatedPdaLinkedTeamMemberID.OptionsColumn.AllowEdit = false;
            this.colAllocatedPdaLinkedTeamMemberID.OptionsColumn.AllowFocus = false;
            this.colAllocatedPdaLinkedTeamMemberID.OptionsColumn.ReadOnly = true;
            this.colAllocatedPdaLinkedTeamMemberID.Width = 205;
            // 
            // colAllocatedPDAID1
            // 
            this.colAllocatedPDAID1.Caption = "Allocated Device ID";
            this.colAllocatedPDAID1.FieldName = "AllocatedPDAID";
            this.colAllocatedPDAID1.Name = "colAllocatedPDAID1";
            this.colAllocatedPDAID1.OptionsColumn.AllowEdit = false;
            this.colAllocatedPDAID1.OptionsColumn.AllowFocus = false;
            this.colAllocatedPDAID1.OptionsColumn.ReadOnly = true;
            this.colAllocatedPDAID1.Width = 102;
            // 
            // colTeamMemberID1
            // 
            this.colTeamMemberID1.Caption = "Team Member ID";
            this.colTeamMemberID1.FieldName = "TeamMemberID";
            this.colTeamMemberID1.Name = "colTeamMemberID1";
            this.colTeamMemberID1.OptionsColumn.AllowEdit = false;
            this.colTeamMemberID1.OptionsColumn.AllowFocus = false;
            this.colTeamMemberID1.OptionsColumn.ReadOnly = true;
            this.colTeamMemberID1.Width = 102;
            // 
            // colPdaID1
            // 
            this.colPdaID1.Caption = "Device ID";
            this.colPdaID1.FieldName = "PdaID";
            this.colPdaID1.Name = "colPdaID1";
            this.colPdaID1.OptionsColumn.AllowEdit = false;
            this.colPdaID1.OptionsColumn.AllowFocus = false;
            this.colPdaID1.OptionsColumn.ReadOnly = true;
            // 
            // colDateAllocated1
            // 
            this.colDateAllocated1.Caption = "Date Allocated";
            this.colDateAllocated1.ColumnEdit = this.repositoryItemTextEditDateTime13;
            this.colDateAllocated1.FieldName = "DateAllocated";
            this.colDateAllocated1.Name = "colDateAllocated1";
            this.colDateAllocated1.OptionsColumn.AllowEdit = false;
            this.colDateAllocated1.OptionsColumn.AllowFocus = false;
            this.colDateAllocated1.OptionsColumn.ReadOnly = true;
            this.colDateAllocated1.Width = 91;
            // 
            // repositoryItemTextEditDateTime13
            // 
            this.repositoryItemTextEditDateTime13.AutoHeight = false;
            this.repositoryItemTextEditDateTime13.Mask.EditMask = "d";
            this.repositoryItemTextEditDateTime13.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime13.Name = "repositoryItemTextEditDateTime13";
            // 
            // colPdaMake1
            // 
            this.colPdaMake1.Caption = "Make";
            this.colPdaMake1.FieldName = "PdaMake";
            this.colPdaMake1.Name = "colPdaMake1";
            this.colPdaMake1.OptionsColumn.AllowEdit = false;
            this.colPdaMake1.OptionsColumn.AllowFocus = false;
            this.colPdaMake1.OptionsColumn.ReadOnly = true;
            this.colPdaMake1.Visible = true;
            this.colPdaMake1.VisibleIndex = 2;
            // 
            // colPdaModel1
            // 
            this.colPdaModel1.Caption = "Model";
            this.colPdaModel1.FieldName = "PdaModel";
            this.colPdaModel1.Name = "colPdaModel1";
            this.colPdaModel1.OptionsColumn.AllowEdit = false;
            this.colPdaModel1.OptionsColumn.AllowFocus = false;
            this.colPdaModel1.OptionsColumn.ReadOnly = true;
            this.colPdaModel1.Visible = true;
            this.colPdaModel1.VisibleIndex = 3;
            // 
            // colPdaSerialNumber1
            // 
            this.colPdaSerialNumber1.Caption = "Serial Number";
            this.colPdaSerialNumber1.FieldName = "PdaSerialNumber";
            this.colPdaSerialNumber1.Name = "colPdaSerialNumber1";
            this.colPdaSerialNumber1.OptionsColumn.AllowEdit = false;
            this.colPdaSerialNumber1.OptionsColumn.AllowFocus = false;
            this.colPdaSerialNumber1.OptionsColumn.ReadOnly = true;
            this.colPdaSerialNumber1.Visible = true;
            this.colPdaSerialNumber1.VisibleIndex = 4;
            this.colPdaSerialNumber1.Width = 87;
            // 
            // colPdaFirmwareVersion1
            // 
            this.colPdaFirmwareVersion1.Caption = "Firmware Version";
            this.colPdaFirmwareVersion1.FieldName = "PdaFirmwareVersion";
            this.colPdaFirmwareVersion1.Name = "colPdaFirmwareVersion1";
            this.colPdaFirmwareVersion1.OptionsColumn.AllowEdit = false;
            this.colPdaFirmwareVersion1.OptionsColumn.AllowFocus = false;
            this.colPdaFirmwareVersion1.OptionsColumn.ReadOnly = true;
            this.colPdaFirmwareVersion1.Visible = true;
            this.colPdaFirmwareVersion1.VisibleIndex = 6;
            this.colPdaFirmwareVersion1.Width = 103;
            // 
            // colPdaMobileTelNo1
            // 
            this.colPdaMobileTelNo1.Caption = "Mobile Tel No";
            this.colPdaMobileTelNo1.FieldName = "PdaMobileTelNo";
            this.colPdaMobileTelNo1.Name = "colPdaMobileTelNo1";
            this.colPdaMobileTelNo1.OptionsColumn.AllowEdit = false;
            this.colPdaMobileTelNo1.OptionsColumn.AllowFocus = false;
            this.colPdaMobileTelNo1.OptionsColumn.ReadOnly = true;
            this.colPdaMobileTelNo1.Visible = true;
            this.colPdaMobileTelNo1.VisibleIndex = 4;
            this.colPdaMobileTelNo1.Width = 84;
            // 
            // colPdaDatePurchased1
            // 
            this.colPdaDatePurchased1.Caption = "Date Purchased";
            this.colPdaDatePurchased1.ColumnEdit = this.repositoryItemTextEditDateTime13;
            this.colPdaDatePurchased1.FieldName = "PdaDatePurchased";
            this.colPdaDatePurchased1.Name = "colPdaDatePurchased1";
            this.colPdaDatePurchased1.OptionsColumn.AllowEdit = false;
            this.colPdaDatePurchased1.OptionsColumn.AllowFocus = false;
            this.colPdaDatePurchased1.OptionsColumn.ReadOnly = true;
            this.colPdaDatePurchased1.Width = 97;
            // 
            // colSoftwareVersion1
            // 
            this.colSoftwareVersion1.Caption = "Software Version";
            this.colSoftwareVersion1.FieldName = "SoftwareVersion";
            this.colSoftwareVersion1.Name = "colSoftwareVersion1";
            this.colSoftwareVersion1.OptionsColumn.AllowEdit = false;
            this.colSoftwareVersion1.OptionsColumn.AllowFocus = false;
            this.colSoftwareVersion1.OptionsColumn.ReadOnly = true;
            this.colSoftwareVersion1.Width = 103;
            // 
            // colIMEI1
            // 
            this.colIMEI1.Caption = "IMEI";
            this.colIMEI1.FieldName = "IMEI";
            this.colIMEI1.Name = "colIMEI1";
            this.colIMEI1.OptionsColumn.AllowEdit = false;
            this.colIMEI1.OptionsColumn.AllowFocus = false;
            this.colIMEI1.OptionsColumn.ReadOnly = true;
            this.colIMEI1.Visible = true;
            this.colIMEI1.VisibleIndex = 5;
            // 
            // colPDACode1
            // 
            this.colPDACode1.Caption = "Device Code";
            this.colPDACode1.FieldName = "PDACode";
            this.colPDACode1.Name = "colPDACode1";
            this.colPDACode1.OptionsColumn.AllowEdit = false;
            this.colPDACode1.OptionsColumn.AllowFocus = false;
            this.colPDACode1.OptionsColumn.ReadOnly = true;
            this.colPDACode1.Visible = true;
            this.colPDACode1.VisibleIndex = 1;
            this.colPDACode1.Width = 79;
            // 
            // colSubcontractorName5
            // 
            this.colSubcontractorName5.Caption = "Team Member Name";
            this.colSubcontractorName5.FieldName = "SubcontractorName";
            this.colSubcontractorName5.Name = "colSubcontractorName5";
            this.colSubcontractorName5.OptionsColumn.AllowEdit = false;
            this.colSubcontractorName5.OptionsColumn.AllowFocus = false;
            this.colSubcontractorName5.OptionsColumn.ReadOnly = true;
            this.colSubcontractorName5.Visible = true;
            this.colSubcontractorName5.VisibleIndex = 0;
            this.colSubcontractorName5.Width = 132;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridControl25);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(865, 277);
            this.xtraTabPage4.Text = "Linked Documents";
            // 
            // gridControl25
            // 
            this.gridControl25.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl25.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl25.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl25.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl25.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl25.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl25.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl25.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl25.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl25.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl25_EmbeddedNavigator_ButtonClick);
            this.gridControl25.Location = new System.Drawing.Point(0, 0);
            this.gridControl25.MainView = this.gridView25;
            this.gridControl25.MenuManager = this.barManager1;
            this.gridControl25.Name = "gridControl25";
            this.gridControl25.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemHyperLinkEdit1});
            this.gridControl25.Size = new System.Drawing.Size(865, 277);
            this.gridControl25.TabIndex = 1;
            this.gridControl25.UseEmbeddedNavigator = true;
            this.gridControl25.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView25});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView25
            // 
            this.gridView25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks,
            this.colDocumentType});
            this.gridView25.GridControl = this.gridControl25;
            this.gridView25.GroupCount = 1;
            this.gridView25.Name = "gridView25";
            this.gridView25.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView25.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView25.OptionsLayout.StoreAppearance = true;
            this.gridView25.OptionsLayout.StoreFormatRules = true;
            this.gridView25.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView25.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView25.OptionsSelection.MultiSelect = true;
            this.gridView25.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView25.OptionsView.ColumnAutoWidth = false;
            this.gridView25.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView25.OptionsView.ShowGroupPanel = false;
            this.gridView25.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView25.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView25_CustomRowCellEdit);
            this.gridView25.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView25.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView25.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView25.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView25_ShowingEditor);
            this.gridView25.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView25.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView25.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView25_MouseUp);
            this.gridView25.DoubleClick += new System.EventHandler(this.gridView25_DoubleClick);
            this.gridView25.GotFocus += new System.EventHandler(this.gridView25_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "File Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 5;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 6;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colDocumentType
            // 
            this.colDocumentType.Caption = "Document Type";
            this.colDocumentType.FieldName = "DocumentType";
            this.colDocumentType.Name = "colDocumentType";
            this.colDocumentType.OptionsColumn.AllowEdit = false;
            this.colDocumentType.OptionsColumn.AllowFocus = false;
            this.colDocumentType.OptionsColumn.ReadOnly = true;
            this.colDocumentType.Visible = true;
            this.colDocumentType.VisibleIndex = 4;
            this.colDocumentType.Width = 150;
            // 
            // sp00195_Staff_ManagerTableAdapter
            // 
            this.sp00195_Staff_ManagerTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // sp00039GetFormPermissionsForUserBindingSource
            // 
            this.sp00039GetFormPermissionsForUserBindingSource.DataMember = "sp00039GetFormPermissionsForUser";
            // 
            // sp00196_Staff_Manager_Linked_Saved_ScreensTableAdapter
            // 
            this.sp00196_Staff_Manager_Linked_Saved_ScreensTableAdapter.ClearBeforeFill = true;
            // 
            // sp06309_Person_Visit_TypesTableAdapter
            // 
            this.sp06309_Person_Visit_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp04027_GC_SubContractor_Allocated_PDAsTableAdapter
            // 
            this.sp04027_GC_SubContractor_Allocated_PDAsTableAdapter.ClearBeforeFill = true;
            // 
            // sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter
            // 
            this.sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter.ClearBeforeFill = true;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Staff_Manager
            // 
            this.ClientSize = new System.Drawing.Size(874, 715);
            this.Controls.Add(this.splitContainerControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Staff_Manager";
            this.Text = "Staff Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Staff_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_Core_Staff_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00195StaffManagerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEditSiteInspectionControlPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00196StaffManagerLinkedSavedScreensBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06309PersonVisitTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit11)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).EndInit();
            this.gridSplitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04027GCSubContractorAllocatedPDAsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04344GCAllocatedPDALinkedTeamMembersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime13)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private WoodPlanDataSet woodPlanDataSet;
        private System.Windows.Forms.BindingSource sp00195StaffManagerBindingSource;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00195_Staff_ManagerTableAdapter sp00195_Staff_ManagerTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colNetworkID;
        private DevExpress.XtraGrid.Columns.GridColumn colForename;
        private DevExpress.XtraGrid.Columns.GridColumn colSurname;
        private DevExpress.XtraGrid.Columns.GridColumn colStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colUserActive;
        private DevExpress.XtraGrid.Columns.GridColumn colUserType;
        private DevExpress.XtraGrid.Columns.GridColumn colToDoListVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colCommentBankVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colLoginCreated;
        private DevExpress.XtraGrid.Columns.GridColumn colShowTipsOnStart;
        private DevExpress.XtraGrid.Columns.GridColumn colLastTipID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserConfig;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailAddress;
        private DevExpress.XtraGrid.Columns.GridColumn colShowConfirmations;
        private DevExpress.XtraGrid.Columns.GridColumn colMinimiseToTaskBar;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultFontName;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultFontSize;
        private DevExpress.XtraGrid.Columns.GridColumn colShowPhotoPopup;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DataSet_AT dataSet_AT;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit8;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstScreenLoaded;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstScreenLoadedID;
        private System.Windows.Forms.BindingSource sp00196StaffManagerLinkedSavedScreensBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colLayoutID;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenID;
        private DevExpress.XtraGrid.Columns.GridColumn colScreenName;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByName;
        private DevExpress.XtraGrid.Columns.GridColumn colLayoutDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colLayoutRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colX;
        private DevExpress.XtraGrid.Columns.GridColumn colY;
        private DevExpress.XtraGrid.Columns.GridColumn colWidth;
        private DevExpress.XtraGrid.Columns.GridColumn colHeight;
        private DevExpress.XtraGrid.Columns.GridColumn colState;
        private DevExpress.XtraGrid.Columns.GridColumn colDefaultLayout;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colShareTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colShareTypeDescription;
        private WoodPlan5.WoodPlanDataSetTableAdapters.sp00196_Staff_Manager_Linked_Saved_ScreensTableAdapter sp00196_Staff_Manager_Linked_Saved_ScreensTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colEstatePlanInspectionControlPanel;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEditSiteInspectionControlPanel;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Columns.GridColumn colWebSiteUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colWebSitePassword;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUtilityArbSurveyor;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlanWebActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlanWebAdminUser;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlanWebFileManagerAdmin;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlanWebWorkOrderDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWorkPhone;
        private DevExpress.XtraGrid.GridControl gridControl14;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypePersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks8;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit11;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitTypeDescription;
        private System.Windows.Forms.BindingSource sp06309PersonVisitTypesBindingSource;
        private DataSet_OM_Core dataSet_OM_Core;
        private DataSet_OM_CoreTableAdapters.sp06309_Person_Visit_TypesTableAdapter sp06309_Person_Visit_TypesTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer6;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocatedPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubContractorName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMake;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaModel;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaSerialNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaFirmwareVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMobileTelNo;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaDatePurchased;
        private DevExpress.XtraGrid.Columns.GridColumn colSoftwareVersion;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAllocated;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAUserName1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDAPassword1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDALoginToken1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDACode;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonType;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToPersonTypeID1;
        private DevExpress.XtraGrid.GridControl gridControl13;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocatedPdaLinkedTeamMemberID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocatedPDAID1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamMemberID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaID1;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAllocated1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMake1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaModel1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaSerialNumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaFirmwareVersion1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaMobileTelNo1;
        private DevExpress.XtraGrid.Columns.GridColumn colPdaDatePurchased1;
        private DevExpress.XtraGrid.Columns.GridColumn colSoftwareVersion1;
        private DevExpress.XtraGrid.Columns.GridColumn colIMEI1;
        private DevExpress.XtraGrid.Columns.GridColumn colPDACode1;
        private DevExpress.XtraGrid.Columns.GridColumn colSubcontractorName5;
        private System.Windows.Forms.BindingSource sp04027GCSubContractorAllocatedPDAsBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime7;
        private System.Windows.Forms.BindingSource sp04344GCAllocatedPDALinkedTeamMembersBindingSource;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime13;
        private DataSet_GC_CoreTableAdapters.sp04027_GC_SubContractor_Allocated_PDAsTableAdapter sp04027_GC_SubContractor_Allocated_PDAsTableAdapter;
        private DataSet_GC_CoreTableAdapters.sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter sp04344_GC_Allocated_PDA_Linked_Team_MembersTableAdapter;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridControl gridControl25;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView25;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentType;
    }
}

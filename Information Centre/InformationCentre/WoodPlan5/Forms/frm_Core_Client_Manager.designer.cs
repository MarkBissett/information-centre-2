namespace WoodPlan5
{
    partial class frm_Core_Client_Manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Client_Manager));
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions2 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.colActive1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp03001EPClientManagerListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_EP = new WoodPlan5.DataSet_EP();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedSiteCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDailyGrittingReport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colClientOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDailyGrittingEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDailyGrittingEmailShowUnGritted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingClientPONumberRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClientPONumberRequired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWinterMaintLastInvoiceDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWinterMaintInvoiceFrequency = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWinterMaintInvoiceFrequencyDescriptor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWinterMaintInvoiceIgnoreSSColumns = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingEveningStartTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditTimeMask = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colGrittingEveningEndTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWinterMaintInvoiceFrequencyDescriptorID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEmailPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsActiveClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerRelationshipDepth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCustomerRelationshipDepthID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsUtilityArbClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArbPermissionDocumentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUtilityArbPermissionDocumentTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colImagesFolderOM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartmentCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinanceCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReceivesExternalGrittingEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsAmenityArbClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsSummerMaintenanceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsWinterMaintenanceClient = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTest = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp03001_EP_Client_Manager_ListTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp03001_EP_Client_Manager_ListTableAdapter();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageLinkedDocuments = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer2 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp00220LinkedDocumentsListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer4 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl10 = new DevExpress.XtraGrid.GridControl();
            this.sp05091GCContactPeopleLinkedToClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContactPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer3 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp04009GCContactsLinkedToClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Core = new WoodPlan5.DataSet_GC_Core();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDetails = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colContactTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncludeForGrittingEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncludeForReports = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebSiteUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWebSitePassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlanWebActionDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlanWebFileManagerAdmin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWoodPlanWebWorkOrderDoneDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactPersonID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPersonName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer5 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp04095GCPOsLinkedToClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientPOID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPONumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colDateTimeReceived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGivenByPerson = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colAmountSpent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAmountRemaining = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer6 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.sp04098GCSitesLinkedToClientPOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientPOSiteLinkID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientPOID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingAllowed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colSnowClearanceAllowed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPONumber1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGrittingDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSnowClearanceDefault = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridSplitContainer7 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.sp04246GCSentReportsLinkedToClientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_GC_Reports = new WoodPlan5.DataSet_GC_Reports();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSentReportID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentReportTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateSent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colSentByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedReportFilename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEditLinkedReportFile = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colReportTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSentByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer8 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.sp04247GCClientInvoiceGroupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInvoiceGroupID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncludeGritting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncludeSnowClearance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientName5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCareOfAddressLine1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCareOfAddressLine2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCareOfAddressLine3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCareOfAddressLine4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCareOfAddressLine5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCareOfPostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCareOfEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAcceptsEmailInvoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridSplitContainer9 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl8 = new DevExpress.XtraGrid.GridControl();
            this.sp04248GCClientInvoiceGroupMembersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInvoiceGroupMember = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInvoiceGroupID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colGroupDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colIncludeGritting1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIncludeSnowClearance1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientCode6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridSplitContainer11 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl11 = new DevExpress.XtraGrid.GridControl();
            this.sp04368GCClientInvoiceGroupClientContactsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInvoiceGroupClientContactID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDetails1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactType1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.btnReloadCRM = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.dateEditFromDate = new DevExpress.XtraEditors.DateEdit();
            this.dateEditToDate = new DevExpress.XtraEditors.DateEdit();
            this.gridSplitContainer10 = new DevExpress.XtraGrid.GridSplitContainer();
            this.gridControl9 = new DevExpress.XtraGrid.GridControl();
            this.sp05080CRMContactListBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCRMID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDueDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colContactMadeDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethodID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactMethod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContactDirectionDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedRecordTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp00220_Linked_Documents_ListTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter();
            this.sp04009_GC_Contacts_Linked_To_ClientTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04009_GC_Contacts_Linked_To_ClientTableAdapter();
            this.sp04095_GC_POs_Linked_To_ClientTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04095_GC_POs_Linked_To_ClientTableAdapter();
            this.sp04098_GC_Sites_Linked_To_Client_POTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04098_GC_Sites_Linked_To_Client_POTableAdapter();
            this.sp04246_GC_Sent_Reports_Linked_To_ClientTableAdapter = new WoodPlan5.DataSet_GC_ReportsTableAdapters.sp04246_GC_Sent_Reports_Linked_To_ClientTableAdapter();
            this.sp04247_GC_Client_Invoice_GroupsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04247_GC_Client_Invoice_GroupsTableAdapter();
            this.sp04248_GC_Client_Invoice_Group_MembersTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04248_GC_Client_Invoice_Group_MembersTableAdapter();
            this.pmInvoicing = new DevExpress.XtraBars.PopupMenu(this.components);
            this.bbiAddOutstandingSitesToInvoiceGroup = new DevExpress.XtraBars.BarButtonItem();
            this.sp05080_CRM_Contact_ListTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp05080_CRM_Contact_ListTableAdapter();
            this.sp05091_GC_Contact_People_Linked_To_ClientTableAdapter = new WoodPlan5.DataSet_EPTableAdapters.sp05091_GC_Contact_People_Linked_To_ClientTableAdapter();
            this.sp04368_GC_Client_Invoice_Group_Client_ContactsTableAdapter = new WoodPlan5.DataSet_GC_CoreTableAdapters.sp04368_GC_Client_Invoice_Group_Client_ContactsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03001EPClientManagerListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTimeMask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageLinkedDocuments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).BeginInit();
            this.gridSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).BeginInit();
            this.gridSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05091GCContactPeopleLinkedToClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).BeginInit();
            this.gridSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04009GCContactsLinkedToClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).BeginInit();
            this.gridSplitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04095GCPOsLinkedToClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).BeginInit();
            this.gridSplitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04098GCSitesLinkedToClientPOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).BeginInit();
            this.gridSplitContainer7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04246GCSentReportsLinkedToClientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedReportFile)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).BeginInit();
            this.gridSplitContainer8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04247GCClientInvoiceGroupsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer9)).BeginInit();
            this.gridSplitContainer9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04248GCClientInvoiceGroupMembersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer11)).BeginInit();
            this.gridSplitContainer11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04368GCClientInvoiceGroupClientContactsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer10)).BeginInit();
            this.gridSplitContainer10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05080CRMContactListBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmInvoicing)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(937, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(937, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(937, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAddOutstandingSitesToInvoiceGroup});
            this.barManager1.MaxItemId = 28;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // colActive1
            // 
            this.colActive1.Caption = "Active";
            this.colActive1.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colActive1.FieldName = "Active";
            this.colActive1.Name = "colActive1";
            this.colActive1.OptionsColumn.AllowEdit = false;
            this.colActive1.OptionsColumn.AllowFocus = false;
            this.colActive1.OptionsColumn.ReadOnly = true;
            this.colActive1.Visible = true;
            this.colActive1.VisibleIndex = 1;
            this.colActive1.Width = 51;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Caption = "Check";
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.ValueChecked = 1;
            this.repositoryItemCheckEdit5.ValueUnchecked = 0;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.sp03001EPClientManagerListBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "Move Item Up", "up"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Move Item Down", "down"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "Preserve Current Sort Order", "set_order")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl1_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemHyperLinkEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEditTimeMask});
            this.gridControl1.Size = new System.Drawing.Size(933, 262);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp03001EPClientManagerListBindingSource
            // 
            this.sp03001EPClientManagerListBindingSource.DataMember = "sp03001_EP_Client_Manager_List";
            this.sp03001EPClientManagerListBindingSource.DataSource = this.dataSet_EP;
            // 
            // dataSet_EP
            // 
            this.dataSet_EP.DataSetName = "DataSet_EP";
            this.dataSet_EP.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName,
            this.colClientCode,
            this.colClientTypeID,
            this.colClientTypeDescription,
            this.colRemarks,
            this.colLinkedSiteCount,
            this.colDailyGrittingReport,
            this.colClientOrder,
            this.colDailyGrittingEmail,
            this.colDailyGrittingEmailShowUnGritted,
            this.colGrittingClientPONumberRequired,
            this.colSnowClientPONumberRequired,
            this.colWinterMaintLastInvoiceDate,
            this.colWinterMaintInvoiceFrequency,
            this.colWinterMaintInvoiceFrequencyDescriptor,
            this.colWinterMaintInvoiceIgnoreSSColumns,
            this.colGrittingEveningStartTime,
            this.colGrittingEveningEndTime,
            this.colWinterMaintInvoiceFrequencyDescriptorID,
            this.colEmailPassword,
            this.colIsActiveClient,
            this.colCustomerRelationshipDepth,
            this.colCustomerRelationshipDepthID,
            this.colIsUtilityArbClient,
            this.colUtilityArbPermissionDocumentType,
            this.colUtilityArbPermissionDocumentTypeID,
            this.colImagesFolderOM,
            this.colDepartmentCode,
            this.colFinanceCode,
            this.colReceivesExternalGrittingEmail,
            this.colIsAmenityArbClient,
            this.colIsSummerMaintenanceClient,
            this.colIsWinterMaintenanceClient,
            this.colIsTest});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientOrder, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView1_ShowingEditor);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseUp);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 305;
            // 
            // colClientCode
            // 
            this.colClientCode.Caption = "Client Code";
            this.colClientCode.FieldName = "ClientCode";
            this.colClientCode.Name = "colClientCode";
            this.colClientCode.OptionsColumn.AllowEdit = false;
            this.colClientCode.OptionsColumn.AllowFocus = false;
            this.colClientCode.OptionsColumn.ReadOnly = true;
            this.colClientCode.Visible = true;
            this.colClientCode.VisibleIndex = 1;
            this.colClientCode.Width = 77;
            // 
            // colClientTypeID
            // 
            this.colClientTypeID.Caption = "Client Type ID";
            this.colClientTypeID.FieldName = "ClientTypeID";
            this.colClientTypeID.Name = "colClientTypeID";
            this.colClientTypeID.OptionsColumn.AllowEdit = false;
            this.colClientTypeID.OptionsColumn.AllowFocus = false;
            this.colClientTypeID.OptionsColumn.ReadOnly = true;
            this.colClientTypeID.Width = 88;
            // 
            // colClientTypeDescription
            // 
            this.colClientTypeDescription.Caption = "Client Type";
            this.colClientTypeDescription.FieldName = "ClientTypeDescription";
            this.colClientTypeDescription.Name = "colClientTypeDescription";
            this.colClientTypeDescription.OptionsColumn.AllowEdit = false;
            this.colClientTypeDescription.OptionsColumn.AllowFocus = false;
            this.colClientTypeDescription.OptionsColumn.ReadOnly = true;
            this.colClientTypeDescription.Visible = true;
            this.colClientTypeDescription.VisibleIndex = 2;
            this.colClientTypeDescription.Width = 196;
            // 
            // colRemarks
            // 
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.OptionsColumn.ReadOnly = true;
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 17;
            this.colRemarks.Width = 143;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colLinkedSiteCount
            // 
            this.colLinkedSiteCount.Caption = "Linked Sites";
            this.colLinkedSiteCount.ColumnEdit = this.repositoryItemHyperLinkEdit1;
            this.colLinkedSiteCount.FieldName = "LinkedSiteCount";
            this.colLinkedSiteCount.Name = "colLinkedSiteCount";
            this.colLinkedSiteCount.OptionsColumn.ReadOnly = true;
            this.colLinkedSiteCount.Visible = true;
            this.colLinkedSiteCount.VisibleIndex = 16;
            this.colLinkedSiteCount.Width = 80;
            // 
            // repositoryItemHyperLinkEdit1
            // 
            this.repositoryItemHyperLinkEdit1.AutoHeight = false;
            this.repositoryItemHyperLinkEdit1.Name = "repositoryItemHyperLinkEdit1";
            this.repositoryItemHyperLinkEdit1.SingleClick = true;
            this.repositoryItemHyperLinkEdit1.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit1_OpenLink);
            // 
            // colDailyGrittingReport
            // 
            this.colDailyGrittingReport.Caption = "Daily Gritting Report";
            this.colDailyGrittingReport.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDailyGrittingReport.FieldName = "DailyGrittingReport";
            this.colDailyGrittingReport.Name = "colDailyGrittingReport";
            this.colDailyGrittingReport.OptionsColumn.AllowEdit = false;
            this.colDailyGrittingReport.OptionsColumn.AllowFocus = false;
            this.colDailyGrittingReport.OptionsColumn.ReadOnly = true;
            this.colDailyGrittingReport.Visible = true;
            this.colDailyGrittingReport.VisibleIndex = 13;
            this.colDailyGrittingReport.Width = 118;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colClientOrder
            // 
            this.colClientOrder.Caption = "Order";
            this.colClientOrder.FieldName = "ClientOrder";
            this.colClientOrder.Name = "colClientOrder";
            this.colClientOrder.OptionsColumn.AllowEdit = false;
            this.colClientOrder.OptionsColumn.AllowFocus = false;
            this.colClientOrder.OptionsColumn.ReadOnly = true;
            this.colClientOrder.Visible = true;
            this.colClientOrder.VisibleIndex = 15;
            this.colClientOrder.Width = 63;
            // 
            // colDailyGrittingEmail
            // 
            this.colDailyGrittingEmail.Caption = "Daily Gritting Email";
            this.colDailyGrittingEmail.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDailyGrittingEmail.FieldName = "DailyGrittingEmail";
            this.colDailyGrittingEmail.Name = "colDailyGrittingEmail";
            this.colDailyGrittingEmail.OptionsColumn.AllowEdit = false;
            this.colDailyGrittingEmail.OptionsColumn.AllowFocus = false;
            this.colDailyGrittingEmail.OptionsColumn.ReadOnly = true;
            this.colDailyGrittingEmail.Visible = true;
            this.colDailyGrittingEmail.VisibleIndex = 11;
            this.colDailyGrittingEmail.Width = 109;
            // 
            // colDailyGrittingEmailShowUnGritted
            // 
            this.colDailyGrittingEmailShowUnGritted.Caption = "Gritting Email Show Un-Gritted";
            this.colDailyGrittingEmailShowUnGritted.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colDailyGrittingEmailShowUnGritted.FieldName = "DailyGrittingEmailShowUnGritted";
            this.colDailyGrittingEmailShowUnGritted.Name = "colDailyGrittingEmailShowUnGritted";
            this.colDailyGrittingEmailShowUnGritted.OptionsColumn.AllowEdit = false;
            this.colDailyGrittingEmailShowUnGritted.OptionsColumn.AllowFocus = false;
            this.colDailyGrittingEmailShowUnGritted.OptionsColumn.ReadOnly = true;
            this.colDailyGrittingEmailShowUnGritted.Visible = true;
            this.colDailyGrittingEmailShowUnGritted.VisibleIndex = 12;
            this.colDailyGrittingEmailShowUnGritted.Width = 165;
            // 
            // colGrittingClientPONumberRequired
            // 
            this.colGrittingClientPONumberRequired.Caption = "Gritting Client PO Required";
            this.colGrittingClientPONumberRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colGrittingClientPONumberRequired.FieldName = "GrittingClientPONumberRequired";
            this.colGrittingClientPONumberRequired.Name = "colGrittingClientPONumberRequired";
            this.colGrittingClientPONumberRequired.OptionsColumn.AllowEdit = false;
            this.colGrittingClientPONumberRequired.OptionsColumn.AllowFocus = false;
            this.colGrittingClientPONumberRequired.OptionsColumn.ReadOnly = true;
            this.colGrittingClientPONumberRequired.Visible = true;
            this.colGrittingClientPONumberRequired.VisibleIndex = 23;
            this.colGrittingClientPONumberRequired.Width = 149;
            // 
            // colSnowClientPONumberRequired
            // 
            this.colSnowClientPONumberRequired.Caption = "Snow Client PO Required";
            this.colSnowClientPONumberRequired.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colSnowClientPONumberRequired.FieldName = "SnowClientPONumberRequired";
            this.colSnowClientPONumberRequired.Name = "colSnowClientPONumberRequired";
            this.colSnowClientPONumberRequired.OptionsColumn.AllowEdit = false;
            this.colSnowClientPONumberRequired.OptionsColumn.AllowFocus = false;
            this.colSnowClientPONumberRequired.OptionsColumn.ReadOnly = true;
            this.colSnowClientPONumberRequired.Visible = true;
            this.colSnowClientPONumberRequired.VisibleIndex = 24;
            this.colSnowClientPONumberRequired.Width = 140;
            // 
            // colWinterMaintLastInvoiceDate
            // 
            this.colWinterMaintLastInvoiceDate.Caption = "Winter Maintenance Last Invoice Date";
            this.colWinterMaintLastInvoiceDate.FieldName = "WinterMaintLastInvoiceDate";
            this.colWinterMaintLastInvoiceDate.Name = "colWinterMaintLastInvoiceDate";
            this.colWinterMaintLastInvoiceDate.OptionsColumn.AllowEdit = false;
            this.colWinterMaintLastInvoiceDate.OptionsColumn.AllowFocus = false;
            this.colWinterMaintLastInvoiceDate.OptionsColumn.ReadOnly = true;
            this.colWinterMaintLastInvoiceDate.Visible = true;
            this.colWinterMaintLastInvoiceDate.VisibleIndex = 25;
            this.colWinterMaintLastInvoiceDate.Width = 204;
            // 
            // colWinterMaintInvoiceFrequency
            // 
            this.colWinterMaintInvoiceFrequency.Caption = "Winter Maintenance Invoice Frequency";
            this.colWinterMaintInvoiceFrequency.FieldName = "WinterMaintInvoiceFrequency";
            this.colWinterMaintInvoiceFrequency.Name = "colWinterMaintInvoiceFrequency";
            this.colWinterMaintInvoiceFrequency.OptionsColumn.AllowEdit = false;
            this.colWinterMaintInvoiceFrequency.OptionsColumn.AllowFocus = false;
            this.colWinterMaintInvoiceFrequency.OptionsColumn.ReadOnly = true;
            this.colWinterMaintInvoiceFrequency.Visible = true;
            this.colWinterMaintInvoiceFrequency.VisibleIndex = 26;
            this.colWinterMaintInvoiceFrequency.Width = 209;
            // 
            // colWinterMaintInvoiceFrequencyDescriptor
            // 
            this.colWinterMaintInvoiceFrequencyDescriptor.Caption = "Winter Maintenance Frequency Descriptor";
            this.colWinterMaintInvoiceFrequencyDescriptor.FieldName = "WinterMaintInvoiceFrequencyDescriptor";
            this.colWinterMaintInvoiceFrequencyDescriptor.Name = "colWinterMaintInvoiceFrequencyDescriptor";
            this.colWinterMaintInvoiceFrequencyDescriptor.OptionsColumn.AllowEdit = false;
            this.colWinterMaintInvoiceFrequencyDescriptor.OptionsColumn.AllowFocus = false;
            this.colWinterMaintInvoiceFrequencyDescriptor.OptionsColumn.ReadOnly = true;
            this.colWinterMaintInvoiceFrequencyDescriptor.Visible = true;
            this.colWinterMaintInvoiceFrequencyDescriptor.VisibleIndex = 27;
            this.colWinterMaintInvoiceFrequencyDescriptor.Width = 223;
            // 
            // colWinterMaintInvoiceIgnoreSSColumns
            // 
            this.colWinterMaintInvoiceIgnoreSSColumns.Caption = "Winter Maintenance Invoice Ignore SS Columns";
            this.colWinterMaintInvoiceIgnoreSSColumns.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colWinterMaintInvoiceIgnoreSSColumns.FieldName = "WinterMaintInvoiceIgnoreSSColumns";
            this.colWinterMaintInvoiceIgnoreSSColumns.Name = "colWinterMaintInvoiceIgnoreSSColumns";
            this.colWinterMaintInvoiceIgnoreSSColumns.OptionsColumn.AllowEdit = false;
            this.colWinterMaintInvoiceIgnoreSSColumns.OptionsColumn.AllowFocus = false;
            this.colWinterMaintInvoiceIgnoreSSColumns.OptionsColumn.ReadOnly = true;
            this.colWinterMaintInvoiceIgnoreSSColumns.Visible = true;
            this.colWinterMaintInvoiceIgnoreSSColumns.VisibleIndex = 28;
            this.colWinterMaintInvoiceIgnoreSSColumns.Width = 248;
            // 
            // colGrittingEveningStartTime
            // 
            this.colGrittingEveningStartTime.Caption = "Gritting Evening Start Time";
            this.colGrittingEveningStartTime.ColumnEdit = this.repositoryItemTextEditTimeMask;
            this.colGrittingEveningStartTime.FieldName = "GrittingEveningStartTime";
            this.colGrittingEveningStartTime.Name = "colGrittingEveningStartTime";
            this.colGrittingEveningStartTime.OptionsColumn.AllowEdit = false;
            this.colGrittingEveningStartTime.OptionsColumn.AllowFocus = false;
            this.colGrittingEveningStartTime.OptionsColumn.ReadOnly = true;
            this.colGrittingEveningStartTime.Visible = true;
            this.colGrittingEveningStartTime.VisibleIndex = 21;
            this.colGrittingEveningStartTime.Width = 149;
            // 
            // repositoryItemTextEditTimeMask
            // 
            this.repositoryItemTextEditTimeMask.AutoHeight = false;
            this.repositoryItemTextEditTimeMask.Mask.EditMask = "HH:mm";
            this.repositoryItemTextEditTimeMask.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditTimeMask.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditTimeMask.Name = "repositoryItemTextEditTimeMask";
            // 
            // colGrittingEveningEndTime
            // 
            this.colGrittingEveningEndTime.Caption = "Gritting Evening End Time";
            this.colGrittingEveningEndTime.ColumnEdit = this.repositoryItemTextEditTimeMask;
            this.colGrittingEveningEndTime.FieldName = "GrittingEveningEndTime";
            this.colGrittingEveningEndTime.Name = "colGrittingEveningEndTime";
            this.colGrittingEveningEndTime.OptionsColumn.AllowEdit = false;
            this.colGrittingEveningEndTime.OptionsColumn.AllowFocus = false;
            this.colGrittingEveningEndTime.OptionsColumn.ReadOnly = true;
            this.colGrittingEveningEndTime.Visible = true;
            this.colGrittingEveningEndTime.VisibleIndex = 22;
            this.colGrittingEveningEndTime.Width = 143;
            // 
            // colWinterMaintInvoiceFrequencyDescriptorID
            // 
            this.colWinterMaintInvoiceFrequencyDescriptorID.Caption = "Winter Maintenance Frequency Descriptor ID";
            this.colWinterMaintInvoiceFrequencyDescriptorID.FieldName = "WinterMaintInvoiceFrequencyDescriptorID";
            this.colWinterMaintInvoiceFrequencyDescriptorID.Name = "colWinterMaintInvoiceFrequencyDescriptorID";
            this.colWinterMaintInvoiceFrequencyDescriptorID.OptionsColumn.AllowEdit = false;
            this.colWinterMaintInvoiceFrequencyDescriptorID.OptionsColumn.AllowFocus = false;
            this.colWinterMaintInvoiceFrequencyDescriptorID.OptionsColumn.ReadOnly = true;
            this.colWinterMaintInvoiceFrequencyDescriptorID.Width = 237;
            // 
            // colEmailPassword
            // 
            this.colEmailPassword.Caption = "Email Password";
            this.colEmailPassword.FieldName = "EmailPassword";
            this.colEmailPassword.Name = "colEmailPassword";
            this.colEmailPassword.OptionsColumn.AllowEdit = false;
            this.colEmailPassword.OptionsColumn.AllowFocus = false;
            this.colEmailPassword.OptionsColumn.ReadOnly = true;
            this.colEmailPassword.Visible = true;
            this.colEmailPassword.VisibleIndex = 20;
            this.colEmailPassword.Width = 94;
            // 
            // colIsActiveClient
            // 
            this.colIsActiveClient.Caption = "Active Client";
            this.colIsActiveClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsActiveClient.FieldName = "IsActiveClient";
            this.colIsActiveClient.Name = "colIsActiveClient";
            this.colIsActiveClient.OptionsColumn.AllowEdit = false;
            this.colIsActiveClient.OptionsColumn.AllowFocus = false;
            this.colIsActiveClient.OptionsColumn.ReadOnly = true;
            this.colIsActiveClient.Visible = true;
            this.colIsActiveClient.VisibleIndex = 3;
            this.colIsActiveClient.Width = 79;
            // 
            // colCustomerRelationshipDepth
            // 
            this.colCustomerRelationshipDepth.Caption = "Customer Relationship Depth";
            this.colCustomerRelationshipDepth.FieldName = "CustomerRelationshipDepth";
            this.colCustomerRelationshipDepth.Name = "colCustomerRelationshipDepth";
            this.colCustomerRelationshipDepth.OptionsColumn.AllowEdit = false;
            this.colCustomerRelationshipDepth.OptionsColumn.AllowFocus = false;
            this.colCustomerRelationshipDepth.OptionsColumn.ReadOnly = true;
            this.colCustomerRelationshipDepth.Visible = true;
            this.colCustomerRelationshipDepth.VisibleIndex = 10;
            this.colCustomerRelationshipDepth.Width = 157;
            // 
            // colCustomerRelationshipDepthID
            // 
            this.colCustomerRelationshipDepthID.Caption = "Customer Relationship Depth ID";
            this.colCustomerRelationshipDepthID.FieldName = "CustomerRelationshipDepthID";
            this.colCustomerRelationshipDepthID.Name = "colCustomerRelationshipDepthID";
            this.colCustomerRelationshipDepthID.OptionsColumn.AllowEdit = false;
            this.colCustomerRelationshipDepthID.OptionsColumn.AllowFocus = false;
            this.colCustomerRelationshipDepthID.OptionsColumn.ReadOnly = true;
            this.colCustomerRelationshipDepthID.Width = 171;
            // 
            // colIsUtilityArbClient
            // 
            this.colIsUtilityArbClient.Caption = "Utilities Arb Client";
            this.colIsUtilityArbClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsUtilityArbClient.FieldName = "IsUtilityArbClient";
            this.colIsUtilityArbClient.Name = "colIsUtilityArbClient";
            this.colIsUtilityArbClient.OptionsColumn.AllowEdit = false;
            this.colIsUtilityArbClient.OptionsColumn.AllowFocus = false;
            this.colIsUtilityArbClient.OptionsColumn.ReadOnly = true;
            this.colIsUtilityArbClient.Visible = true;
            this.colIsUtilityArbClient.VisibleIndex = 8;
            this.colIsUtilityArbClient.Width = 103;
            // 
            // colUtilityArbPermissionDocumentType
            // 
            this.colUtilityArbPermissionDocumentType.Caption = "Utilities ARB Permission Document Type";
            this.colUtilityArbPermissionDocumentType.FieldName = "UtilityArbPermissionDocumentType";
            this.colUtilityArbPermissionDocumentType.Name = "colUtilityArbPermissionDocumentType";
            this.colUtilityArbPermissionDocumentType.OptionsColumn.AllowEdit = false;
            this.colUtilityArbPermissionDocumentType.OptionsColumn.AllowFocus = false;
            this.colUtilityArbPermissionDocumentType.OptionsColumn.ReadOnly = true;
            this.colUtilityArbPermissionDocumentType.Visible = true;
            this.colUtilityArbPermissionDocumentType.VisibleIndex = 9;
            this.colUtilityArbPermissionDocumentType.Width = 209;
            // 
            // colUtilityArbPermissionDocumentTypeID
            // 
            this.colUtilityArbPermissionDocumentTypeID.Caption = "Utilities ARB Permission Document Type ID";
            this.colUtilityArbPermissionDocumentTypeID.FieldName = "UtilityArbPermissionDocumentTypeID";
            this.colUtilityArbPermissionDocumentTypeID.Name = "colUtilityArbPermissionDocumentTypeID";
            this.colUtilityArbPermissionDocumentTypeID.OptionsColumn.AllowEdit = false;
            this.colUtilityArbPermissionDocumentTypeID.OptionsColumn.AllowFocus = false;
            this.colUtilityArbPermissionDocumentTypeID.OptionsColumn.ReadOnly = true;
            this.colUtilityArbPermissionDocumentTypeID.Width = 223;
            // 
            // colImagesFolderOM
            // 
            this.colImagesFolderOM.Caption = "OM - Images Folder ";
            this.colImagesFolderOM.FieldName = "ImagesFolderOM";
            this.colImagesFolderOM.Name = "colImagesFolderOM";
            this.colImagesFolderOM.OptionsColumn.AllowEdit = false;
            this.colImagesFolderOM.OptionsColumn.AllowFocus = false;
            this.colImagesFolderOM.OptionsColumn.ReadOnly = true;
            this.colImagesFolderOM.Visible = true;
            this.colImagesFolderOM.VisibleIndex = 29;
            this.colImagesFolderOM.Width = 124;
            // 
            // colDepartmentCode
            // 
            this.colDepartmentCode.Caption = "Department Code";
            this.colDepartmentCode.FieldName = "DepartmentCode";
            this.colDepartmentCode.Name = "colDepartmentCode";
            this.colDepartmentCode.OptionsColumn.AllowEdit = false;
            this.colDepartmentCode.OptionsColumn.AllowFocus = false;
            this.colDepartmentCode.OptionsColumn.ReadOnly = true;
            this.colDepartmentCode.Visible = true;
            this.colDepartmentCode.VisibleIndex = 18;
            this.colDepartmentCode.Width = 104;
            // 
            // colFinanceCode
            // 
            this.colFinanceCode.Caption = "Finance Code";
            this.colFinanceCode.FieldName = "FinanceCode";
            this.colFinanceCode.Name = "colFinanceCode";
            this.colFinanceCode.OptionsColumn.AllowEdit = false;
            this.colFinanceCode.OptionsColumn.AllowFocus = false;
            this.colFinanceCode.OptionsColumn.ReadOnly = true;
            this.colFinanceCode.Visible = true;
            this.colFinanceCode.VisibleIndex = 19;
            this.colFinanceCode.Width = 84;
            // 
            // colReceivesExternalGrittingEmail
            // 
            this.colReceivesExternalGrittingEmail.Caption = "External Gritting Email";
            this.colReceivesExternalGrittingEmail.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colReceivesExternalGrittingEmail.FieldName = "ReceivesExternalGrittingEmail";
            this.colReceivesExternalGrittingEmail.Name = "colReceivesExternalGrittingEmail";
            this.colReceivesExternalGrittingEmail.OptionsColumn.AllowEdit = false;
            this.colReceivesExternalGrittingEmail.OptionsColumn.AllowFocus = false;
            this.colReceivesExternalGrittingEmail.OptionsColumn.ReadOnly = true;
            this.colReceivesExternalGrittingEmail.Visible = true;
            this.colReceivesExternalGrittingEmail.VisibleIndex = 14;
            this.colReceivesExternalGrittingEmail.Width = 124;
            // 
            // colIsAmenityArbClient
            // 
            this.colIsAmenityArbClient.Caption = "Amenity Arb Client";
            this.colIsAmenityArbClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsAmenityArbClient.FieldName = "IsAmenityArbClient";
            this.colIsAmenityArbClient.Name = "colIsAmenityArbClient";
            this.colIsAmenityArbClient.OptionsColumn.AllowEdit = false;
            this.colIsAmenityArbClient.OptionsColumn.AllowFocus = false;
            this.colIsAmenityArbClient.OptionsColumn.ReadOnly = true;
            this.colIsAmenityArbClient.Visible = true;
            this.colIsAmenityArbClient.VisibleIndex = 7;
            this.colIsAmenityArbClient.Width = 108;
            // 
            // colIsSummerMaintenanceClient
            // 
            this.colIsSummerMaintenanceClient.Caption = "Summer Maintenance Client";
            this.colIsSummerMaintenanceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsSummerMaintenanceClient.FieldName = "IsSummerMaintenanceClient";
            this.colIsSummerMaintenanceClient.Name = "colIsSummerMaintenanceClient";
            this.colIsSummerMaintenanceClient.OptionsColumn.AllowEdit = false;
            this.colIsSummerMaintenanceClient.OptionsColumn.AllowFocus = false;
            this.colIsSummerMaintenanceClient.OptionsColumn.ReadOnly = true;
            this.colIsSummerMaintenanceClient.Visible = true;
            this.colIsSummerMaintenanceClient.VisibleIndex = 6;
            this.colIsSummerMaintenanceClient.Width = 151;
            // 
            // colIsWinterMaintenanceClient
            // 
            this.colIsWinterMaintenanceClient.Caption = "Winter Maintenance Client";
            this.colIsWinterMaintenanceClient.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsWinterMaintenanceClient.FieldName = "IsWinterMaintenanceClient";
            this.colIsWinterMaintenanceClient.Name = "colIsWinterMaintenanceClient";
            this.colIsWinterMaintenanceClient.OptionsColumn.AllowEdit = false;
            this.colIsWinterMaintenanceClient.OptionsColumn.AllowFocus = false;
            this.colIsWinterMaintenanceClient.OptionsColumn.ReadOnly = true;
            this.colIsWinterMaintenanceClient.Visible = true;
            this.colIsWinterMaintenanceClient.VisibleIndex = 5;
            this.colIsWinterMaintenanceClient.Width = 145;
            // 
            // colIsTest
            // 
            this.colIsTest.Caption = "Test Client";
            this.colIsTest.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colIsTest.FieldName = "IsTest";
            this.colIsTest.Name = "colIsTest";
            this.colIsTest.OptionsColumn.AllowEdit = false;
            this.colIsTest.OptionsColumn.AllowFocus = false;
            this.colIsTest.OptionsColumn.ReadOnly = true;
            this.colIsTest.Visible = true;
            this.colIsTest.VisibleIndex = 4;
            this.colIsTest.Width = 70;
            // 
            // sp03001_EP_Client_Manager_ListTableAdapter
            // 
            this.sp03001_EP_Client_Manager_ListTableAdapter.ClearBeforeFill = true;
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl1.Panel1.Controls.Add(this.gridSplitContainer1);
            this.splitContainerControl1.Panel1.Text = "Clients";
            this.splitContainerControl1.Panel2.Controls.Add(this.xtraTabControl1);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(937, 537);
            this.splitContainerControl1.SplitterPosition = 265;
            this.splitContainerControl1.TabIndex = 5;
            this.splitContainerControl1.Text = "splitContainerControl1";
            this.splitContainerControl1.SplitterPositionChanged += new System.EventHandler(this.splitContainerControl1_SplitterPositionChanged);
            // 
            // gridSplitContainer1
            // 
            this.gridSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            this.gridSplitContainer1.Size = new System.Drawing.Size(933, 262);
            this.gridSplitContainer1.TabIndex = 0;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageLinkedDocuments;
            this.xtraTabControl1.Size = new System.Drawing.Size(937, 265);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageLinkedDocuments,
            this.xtraTabPage7,
            this.xtraTabPage1,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6});
            // 
            // xtraTabPageLinkedDocuments
            // 
            this.xtraTabPageLinkedDocuments.Controls.Add(this.gridSplitContainer2);
            this.xtraTabPageLinkedDocuments.ImageOptions.Image = global::WoodPlan5.Properties.Resources.linked_documents_16_16;
            this.xtraTabPageLinkedDocuments.Name = "xtraTabPageLinkedDocuments";
            this.xtraTabPageLinkedDocuments.Size = new System.Drawing.Size(932, 236);
            this.xtraTabPageLinkedDocuments.Text = "Linked Documents";
            // 
            // gridSplitContainer2
            // 
            this.gridSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer2.Grid = this.gridControl2;
            this.gridSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer2.Name = "gridSplitContainer2";
            this.gridSplitContainer2.Panel1.Controls.Add(this.gridControl2);
            this.gridSplitContainer2.Size = new System.Drawing.Size(932, 236);
            this.gridSplitContainer2.TabIndex = 0;
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.sp00220LinkedDocumentsListBindingSource;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2,
            this.repositoryItemHyperLinkEdit2});
            this.gridControl2.Size = new System.Drawing.Size(932, 236);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp00220LinkedDocumentsListBindingSource
            // 
            this.sp00220LinkedDocumentsListBindingSource.DataMember = "sp00220_Linked_Documents_List";
            this.sp00220LinkedDocumentsListBindingSource.DataSource = this.dataSet_AT;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecordDescription,
            this.colAddedByStaffName,
            this.colDocumentRemarks});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedRecordDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.DoubleClick += new System.EventHandler(this.gridView2_DoubleClick);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 91;
            // 
            // colLinkedRecordDescription
            // 
            this.colLinkedRecordDescription.Caption = "Linked To";
            this.colLinkedRecordDescription.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription.Name = "colLinkedRecordDescription";
            this.colLinkedRecordDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription.Width = 131;
            // 
            // colAddedByStaffName
            // 
            this.colAddedByStaffName.Caption = "Added By";
            this.colAddedByStaffName.FieldName = "AddedByStaffName";
            this.colAddedByStaffName.Name = "colAddedByStaffName";
            this.colAddedByStaffName.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffName.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffName.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffName.Visible = true;
            this.colAddedByStaffName.VisibleIndex = 4;
            this.colAddedByStaffName.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colDocumentRemarks.FieldName = "DocumentRemarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.gridSplitContainer4);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(932, 236);
            this.xtraTabPage7.Text = "Client Contact People";
            // 
            // gridSplitContainer4
            // 
            this.gridSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer4.Grid = this.gridControl10;
            this.gridSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer4.Name = "gridSplitContainer4";
            this.gridSplitContainer4.Panel1.Controls.Add(this.gridControl10);
            this.gridSplitContainer4.Size = new System.Drawing.Size(932, 236);
            this.gridSplitContainer4.TabIndex = 0;
            // 
            // gridControl10
            // 
            this.gridControl10.DataSource = this.sp05091GCContactPeopleLinkedToClientBindingSource;
            this.gridControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl10.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl10.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl10.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl10.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl10_EmbeddedNavigator_ButtonClick);
            this.gridControl10.Location = new System.Drawing.Point(0, 0);
            this.gridControl10.MainView = this.gridView10;
            this.gridControl10.MenuManager = this.barManager1;
            this.gridControl10.Name = "gridControl10";
            this.gridControl10.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8});
            this.gridControl10.Size = new System.Drawing.Size(932, 236);
            this.gridControl10.TabIndex = 1;
            this.gridControl10.UseEmbeddedNavigator = true;
            this.gridControl10.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView10});
            // 
            // sp05091GCContactPeopleLinkedToClientBindingSource
            // 
            this.sp05091GCContactPeopleLinkedToClientBindingSource.DataMember = "sp05091_GC_Contact_People_Linked_To_Client";
            this.sp05091GCContactPeopleLinkedToClientBindingSource.DataSource = this.dataSet_EP;
            // 
            // gridView10
            // 
            this.gridView10.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContactPersonID,
            this.colClientID6,
            this.colPersonName,
            this.colTitle,
            this.colPosition,
            this.colRemarks5,
            this.colClientName7,
            this.colClientCode7});
            this.gridView10.GridControl = this.gridControl10;
            this.gridView10.GroupCount = 1;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView10.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView10.OptionsLayout.StoreAppearance = true;
            this.gridView10.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView10.OptionsSelection.MultiSelect = true;
            this.gridView10.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView10.OptionsView.ColumnAutoWidth = false;
            this.gridView10.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            this.gridView10.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName7, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPersonName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView10.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView10.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView10.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView10.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView10.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView10.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView10_MouseUp);
            this.gridView10.DoubleClick += new System.EventHandler(this.gridView10_DoubleClick);
            this.gridView10.GotFocus += new System.EventHandler(this.gridView10_GotFocus);
            // 
            // colClientContactPersonID
            // 
            this.colClientContactPersonID.Caption = "Client Contact Person ID";
            this.colClientContactPersonID.FieldName = "ClientContactPersonID";
            this.colClientContactPersonID.Name = "colClientContactPersonID";
            this.colClientContactPersonID.OptionsColumn.AllowEdit = false;
            this.colClientContactPersonID.OptionsColumn.AllowFocus = false;
            this.colClientContactPersonID.OptionsColumn.ReadOnly = true;
            this.colClientContactPersonID.Width = 136;
            // 
            // colClientID6
            // 
            this.colClientID6.Caption = "Client ID";
            this.colClientID6.FieldName = "ClientID";
            this.colClientID6.Name = "colClientID6";
            this.colClientID6.OptionsColumn.AllowEdit = false;
            this.colClientID6.OptionsColumn.AllowFocus = false;
            this.colClientID6.OptionsColumn.ReadOnly = true;
            // 
            // colPersonName
            // 
            this.colPersonName.Caption = "Person Name";
            this.colPersonName.FieldName = "PersonName";
            this.colPersonName.Name = "colPersonName";
            this.colPersonName.OptionsColumn.AllowEdit = false;
            this.colPersonName.OptionsColumn.AllowFocus = false;
            this.colPersonName.OptionsColumn.ReadOnly = true;
            this.colPersonName.Visible = true;
            this.colPersonName.VisibleIndex = 0;
            this.colPersonName.Width = 236;
            // 
            // colTitle
            // 
            this.colTitle.Caption = "Title";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 2;
            this.colTitle.Width = 163;
            // 
            // colPosition
            // 
            this.colPosition.Caption = "Position";
            this.colPosition.FieldName = "Position";
            this.colPosition.Name = "colPosition";
            this.colPosition.OptionsColumn.AllowEdit = false;
            this.colPosition.OptionsColumn.AllowFocus = false;
            this.colPosition.OptionsColumn.ReadOnly = true;
            this.colPosition.Visible = true;
            this.colPosition.VisibleIndex = 1;
            this.colPosition.Width = 145;
            // 
            // colRemarks5
            // 
            this.colRemarks5.Caption = "Remarks";
            this.colRemarks5.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colRemarks5.FieldName = "Remarks";
            this.colRemarks5.Name = "colRemarks5";
            this.colRemarks5.OptionsColumn.ReadOnly = true;
            this.colRemarks5.Visible = true;
            this.colRemarks5.VisibleIndex = 3;
            this.colRemarks5.Width = 135;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // colClientName7
            // 
            this.colClientName7.Caption = "Client Name";
            this.colClientName7.FieldName = "ClientName";
            this.colClientName7.Name = "colClientName7";
            this.colClientName7.OptionsColumn.AllowEdit = false;
            this.colClientName7.OptionsColumn.AllowFocus = false;
            this.colClientName7.OptionsColumn.ReadOnly = true;
            this.colClientName7.Visible = true;
            this.colClientName7.VisibleIndex = 4;
            this.colClientName7.Width = 301;
            // 
            // colClientCode7
            // 
            this.colClientCode7.Caption = "Client Code";
            this.colClientCode7.FieldName = "ClientCode";
            this.colClientCode7.Name = "colClientCode7";
            this.colClientCode7.OptionsColumn.AllowEdit = false;
            this.colClientCode7.OptionsColumn.AllowFocus = false;
            this.colClientCode7.OptionsColumn.ReadOnly = true;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gridSplitContainer3);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(932, 236);
            this.xtraTabPage1.Text = "Contact Details";
            // 
            // gridSplitContainer3
            // 
            this.gridSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer3.Grid = this.gridControl3;
            this.gridSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer3.Name = "gridSplitContainer3";
            this.gridSplitContainer3.Panel1.Controls.Add(this.gridControl3);
            this.gridSplitContainer3.Size = new System.Drawing.Size(932, 236);
            this.gridSplitContainer3.TabIndex = 0;
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp04009GCContactsLinkedToClientBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl3.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl3_EmbeddedNavigator_ButtonClick);
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit3,
            this.repositoryItemCheckEdit2});
            this.gridControl3.Size = new System.Drawing.Size(932, 236);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp04009GCContactsLinkedToClientBindingSource
            // 
            this.sp04009GCContactsLinkedToClientBindingSource.DataMember = "sp04009_GC_Contacts_Linked_To_Client";
            this.sp04009GCContactsLinkedToClientBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // dataSet_GC_Core
            // 
            this.dataSet_GC_Core.DataSetName = "DataSet_GC_Core";
            this.dataSet_GC_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientContactID,
            this.colClientID1,
            this.colContactDetails,
            this.colContactTypeID,
            this.colIncludeForGrittingEmail,
            this.colRemarks1,
            this.colContactTypeDescription,
            this.colClientName1,
            this.colClientCode1,
            this.colIncludeForReports,
            this.colWebSiteUserName,
            this.colWebSitePassword,
            this.colWoodPlanWebActionDoneDate,
            this.colWoodPlanWebFileManagerAdmin,
            this.colWoodPlanWebWorkOrderDoneDate,
            this.colClientContactPersonID1,
            this.colPersonName1});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.GroupCount = 1;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.MultiSelect = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPersonName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseUp);
            this.gridView3.DoubleClick += new System.EventHandler(this.gridView3_DoubleClick);
            this.gridView3.GotFocus += new System.EventHandler(this.gridView3_GotFocus);
            // 
            // colClientContactID
            // 
            this.colClientContactID.Caption = "Client Contact ID";
            this.colClientContactID.FieldName = "ClientContactID";
            this.colClientContactID.Name = "colClientContactID";
            this.colClientContactID.OptionsColumn.AllowEdit = false;
            this.colClientContactID.OptionsColumn.AllowFocus = false;
            this.colClientContactID.OptionsColumn.ReadOnly = true;
            this.colClientContactID.Width = 103;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colContactDetails
            // 
            this.colContactDetails.Caption = "Contact Details";
            this.colContactDetails.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colContactDetails.FieldName = "ContactDetails";
            this.colContactDetails.Name = "colContactDetails";
            this.colContactDetails.OptionsColumn.AllowEdit = false;
            this.colContactDetails.OptionsColumn.AllowFocus = false;
            this.colContactDetails.OptionsColumn.ReadOnly = true;
            this.colContactDetails.Visible = true;
            this.colContactDetails.VisibleIndex = 2;
            this.colContactDetails.Width = 334;
            // 
            // repositoryItemMemoExEdit3
            // 
            this.repositoryItemMemoExEdit3.AutoHeight = false;
            this.repositoryItemMemoExEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit3.Name = "repositoryItemMemoExEdit3";
            this.repositoryItemMemoExEdit3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit3.ShowIcon = false;
            // 
            // colContactTypeID
            // 
            this.colContactTypeID.Caption = "Contact Type ID";
            this.colContactTypeID.FieldName = "ContactTypeID";
            this.colContactTypeID.Name = "colContactTypeID";
            this.colContactTypeID.OptionsColumn.AllowEdit = false;
            this.colContactTypeID.OptionsColumn.AllowFocus = false;
            this.colContactTypeID.OptionsColumn.ReadOnly = true;
            this.colContactTypeID.Width = 100;
            // 
            // colIncludeForGrittingEmail
            // 
            this.colIncludeForGrittingEmail.Caption = "Include For Gritting Email";
            this.colIncludeForGrittingEmail.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIncludeForGrittingEmail.FieldName = "IncludeForGrittingEmail";
            this.colIncludeForGrittingEmail.Name = "colIncludeForGrittingEmail";
            this.colIncludeForGrittingEmail.OptionsColumn.AllowEdit = false;
            this.colIncludeForGrittingEmail.OptionsColumn.AllowFocus = false;
            this.colIncludeForGrittingEmail.OptionsColumn.ReadOnly = true;
            this.colIncludeForGrittingEmail.Visible = true;
            this.colIncludeForGrittingEmail.VisibleIndex = 3;
            this.colIncludeForGrittingEmail.Width = 140;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Caption = "Check";
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.ValueChecked = 1;
            this.repositoryItemCheckEdit2.ValueUnchecked = 0;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit3;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 5;
            this.colRemarks1.Width = 147;
            // 
            // colContactTypeDescription
            // 
            this.colContactTypeDescription.Caption = "Contact Type";
            this.colContactTypeDescription.FieldName = "ContactTypeDescription";
            this.colContactTypeDescription.Name = "colContactTypeDescription";
            this.colContactTypeDescription.OptionsColumn.AllowEdit = false;
            this.colContactTypeDescription.OptionsColumn.AllowFocus = false;
            this.colContactTypeDescription.OptionsColumn.ReadOnly = true;
            this.colContactTypeDescription.Visible = true;
            this.colContactTypeDescription.VisibleIndex = 1;
            this.colContactTypeDescription.Width = 199;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Width = 182;
            // 
            // colClientCode1
            // 
            this.colClientCode1.Caption = "Client Code";
            this.colClientCode1.FieldName = "ClientCode";
            this.colClientCode1.Name = "colClientCode1";
            this.colClientCode1.OptionsColumn.AllowEdit = false;
            this.colClientCode1.OptionsColumn.AllowFocus = false;
            this.colClientCode1.OptionsColumn.ReadOnly = true;
            this.colClientCode1.Width = 100;
            // 
            // colIncludeForReports
            // 
            this.colIncludeForReports.Caption = "Include for Sent Reports";
            this.colIncludeForReports.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colIncludeForReports.FieldName = "IncludeForReports";
            this.colIncludeForReports.Name = "colIncludeForReports";
            this.colIncludeForReports.OptionsColumn.AllowEdit = false;
            this.colIncludeForReports.OptionsColumn.AllowFocus = false;
            this.colIncludeForReports.OptionsColumn.ReadOnly = true;
            this.colIncludeForReports.Visible = true;
            this.colIncludeForReports.VisibleIndex = 4;
            this.colIncludeForReports.Width = 139;
            // 
            // colWebSiteUserName
            // 
            this.colWebSiteUserName.Caption = "Website Username";
            this.colWebSiteUserName.FieldName = "WebSiteUserName";
            this.colWebSiteUserName.Name = "colWebSiteUserName";
            this.colWebSiteUserName.OptionsColumn.AllowEdit = false;
            this.colWebSiteUserName.OptionsColumn.AllowFocus = false;
            this.colWebSiteUserName.OptionsColumn.ReadOnly = true;
            this.colWebSiteUserName.Visible = true;
            this.colWebSiteUserName.VisibleIndex = 6;
            this.colWebSiteUserName.Width = 111;
            // 
            // colWebSitePassword
            // 
            this.colWebSitePassword.Caption = "Website Password";
            this.colWebSitePassword.FieldName = "WebSitePassword";
            this.colWebSitePassword.Name = "colWebSitePassword";
            this.colWebSitePassword.OptionsColumn.AllowEdit = false;
            this.colWebSitePassword.OptionsColumn.AllowFocus = false;
            this.colWebSitePassword.OptionsColumn.ReadOnly = true;
            this.colWebSitePassword.Visible = true;
            this.colWebSitePassword.VisibleIndex = 7;
            this.colWebSitePassword.Width = 109;
            // 
            // colWoodPlanWebActionDoneDate
            // 
            this.colWoodPlanWebActionDoneDate.Caption = "Web - Set Actions Completed";
            this.colWoodPlanWebActionDoneDate.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colWoodPlanWebActionDoneDate.FieldName = "WoodPlanWebActionDoneDate";
            this.colWoodPlanWebActionDoneDate.Name = "colWoodPlanWebActionDoneDate";
            this.colWoodPlanWebActionDoneDate.OptionsColumn.AllowEdit = false;
            this.colWoodPlanWebActionDoneDate.OptionsColumn.AllowFocus = false;
            this.colWoodPlanWebActionDoneDate.OptionsColumn.ReadOnly = true;
            this.colWoodPlanWebActionDoneDate.Visible = true;
            this.colWoodPlanWebActionDoneDate.VisibleIndex = 8;
            this.colWoodPlanWebActionDoneDate.Width = 161;
            // 
            // colWoodPlanWebFileManagerAdmin
            // 
            this.colWoodPlanWebFileManagerAdmin.Caption = "Web - File Manager Admin";
            this.colWoodPlanWebFileManagerAdmin.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colWoodPlanWebFileManagerAdmin.FieldName = "WoodPlanWebFileManagerAdmin";
            this.colWoodPlanWebFileManagerAdmin.Name = "colWoodPlanWebFileManagerAdmin";
            this.colWoodPlanWebFileManagerAdmin.OptionsColumn.AllowEdit = false;
            this.colWoodPlanWebFileManagerAdmin.OptionsColumn.AllowFocus = false;
            this.colWoodPlanWebFileManagerAdmin.OptionsColumn.ReadOnly = true;
            this.colWoodPlanWebFileManagerAdmin.Visible = true;
            this.colWoodPlanWebFileManagerAdmin.VisibleIndex = 10;
            this.colWoodPlanWebFileManagerAdmin.Width = 146;
            // 
            // colWoodPlanWebWorkOrderDoneDate
            // 
            this.colWoodPlanWebWorkOrderDoneDate.Caption = "Web - Set Work Orders Completed";
            this.colWoodPlanWebWorkOrderDoneDate.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colWoodPlanWebWorkOrderDoneDate.FieldName = "WoodPlanWebWorkOrderDoneDate";
            this.colWoodPlanWebWorkOrderDoneDate.Name = "colWoodPlanWebWorkOrderDoneDate";
            this.colWoodPlanWebWorkOrderDoneDate.OptionsColumn.AllowEdit = false;
            this.colWoodPlanWebWorkOrderDoneDate.OptionsColumn.AllowFocus = false;
            this.colWoodPlanWebWorkOrderDoneDate.OptionsColumn.ReadOnly = true;
            this.colWoodPlanWebWorkOrderDoneDate.Visible = true;
            this.colWoodPlanWebWorkOrderDoneDate.VisibleIndex = 9;
            this.colWoodPlanWebWorkOrderDoneDate.Width = 187;
            // 
            // colClientContactPersonID1
            // 
            this.colClientContactPersonID1.Caption = "Client Contact Person";
            this.colClientContactPersonID1.FieldName = "ClientContactPersonID";
            this.colClientContactPersonID1.Name = "colClientContactPersonID1";
            this.colClientContactPersonID1.OptionsColumn.AllowEdit = false;
            this.colClientContactPersonID1.OptionsColumn.AllowFocus = false;
            this.colClientContactPersonID1.OptionsColumn.ReadOnly = true;
            this.colClientContactPersonID1.Width = 123;
            // 
            // colPersonName1
            // 
            this.colPersonName1.Caption = "Person Name";
            this.colPersonName1.FieldName = "PersonName";
            this.colPersonName1.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colPersonName1.Name = "colPersonName1";
            this.colPersonName1.OptionsColumn.AllowEdit = false;
            this.colPersonName1.OptionsColumn.AllowFocus = false;
            this.colPersonName1.OptionsColumn.ReadOnly = true;
            this.colPersonName1.Visible = true;
            this.colPersonName1.VisibleIndex = 0;
            this.colPersonName1.Width = 141;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(932, 236);
            this.xtraTabPage3.Text = "Purchase Orders";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel1.Controls.Add(this.gridSplitContainer5);
            this.splitContainerControl2.Panel1.ShowCaption = true;
            this.splitContainerControl2.Panel1.Text = "Client Purchase Orders";
            this.splitContainerControl2.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl2.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl2.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl2.Panel2.Controls.Add(this.gridSplitContainer6);
            this.splitContainerControl2.Panel2.ShowCaption = true;
            this.splitContainerControl2.Panel2.Text = "Sites Linked to P.O.";
            this.splitContainerControl2.Size = new System.Drawing.Size(932, 236);
            this.splitContainerControl2.SplitterPosition = 544;
            this.splitContainerControl2.TabIndex = 1;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridSplitContainer5
            // 
            this.gridSplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer5.Grid = this.gridControl4;
            this.gridSplitContainer5.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer5.Name = "gridSplitContainer5";
            this.gridSplitContainer5.Panel1.Controls.Add(this.gridControl4);
            this.gridSplitContainer5.Size = new System.Drawing.Size(357, 233);
            this.gridSplitContainer5.TabIndex = 0;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.sp04095GCPOsLinkedToClientBindingSource;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Select Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl4.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl4_EmbeddedNavigator_ButtonClick);
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit3,
            this.repositoryItemMemoExEdit4,
            this.repositoryItemTextEdit1});
            this.gridControl4.Size = new System.Drawing.Size(357, 233);
            this.gridControl4.TabIndex = 0;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp04095GCPOsLinkedToClientBindingSource
            // 
            this.sp04095GCPOsLinkedToClientBindingSource.DataMember = "sp04095_GC_POs_Linked_To_Client";
            this.sp04095GCPOsLinkedToClientBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientPOID,
            this.colClientID2,
            this.colPONumber,
            this.colActive,
            this.colDateTimeReceived,
            this.colGivenByPerson,
            this.colTotalAmount,
            this.colAmountSpent,
            this.colAmountRemaining,
            this.colRemarks2,
            this.colClientName2,
            this.colClientCode2});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 1;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPONumber, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.DoubleClick += new System.EventHandler(this.gridView4_DoubleClick);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colClientPOID
            // 
            this.colClientPOID.Caption = "Client PO ID";
            this.colClientPOID.FieldName = "ClientPOID";
            this.colClientPOID.Name = "colClientPOID";
            this.colClientPOID.OptionsColumn.AllowEdit = false;
            this.colClientPOID.OptionsColumn.AllowFocus = false;
            this.colClientPOID.OptionsColumn.ReadOnly = true;
            this.colClientPOID.Width = 79;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colPONumber
            // 
            this.colPONumber.Caption = "P.O. Number";
            this.colPONumber.FieldName = "PONumber";
            this.colPONumber.Name = "colPONumber";
            this.colPONumber.OptionsColumn.AllowEdit = false;
            this.colPONumber.OptionsColumn.AllowFocus = false;
            this.colPONumber.OptionsColumn.ReadOnly = true;
            this.colPONumber.Visible = true;
            this.colPONumber.VisibleIndex = 0;
            this.colPONumber.Width = 142;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 58;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Caption = "Check";
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.ValueChecked = 1;
            this.repositoryItemCheckEdit3.ValueUnchecked = 0;
            // 
            // colDateTimeReceived
            // 
            this.colDateTimeReceived.Caption = "Date \\ Time Received";
            this.colDateTimeReceived.FieldName = "DateTimeReceived";
            this.colDateTimeReceived.Name = "colDateTimeReceived";
            this.colDateTimeReceived.OptionsColumn.AllowEdit = false;
            this.colDateTimeReceived.OptionsColumn.AllowFocus = false;
            this.colDateTimeReceived.OptionsColumn.ReadOnly = true;
            this.colDateTimeReceived.Visible = true;
            this.colDateTimeReceived.VisibleIndex = 6;
            this.colDateTimeReceived.Width = 123;
            // 
            // colGivenByPerson
            // 
            this.colGivenByPerson.Caption = "Given By";
            this.colGivenByPerson.FieldName = "GivenByPerson";
            this.colGivenByPerson.Name = "colGivenByPerson";
            this.colGivenByPerson.OptionsColumn.AllowEdit = false;
            this.colGivenByPerson.OptionsColumn.AllowFocus = false;
            this.colGivenByPerson.OptionsColumn.ReadOnly = true;
            this.colGivenByPerson.Visible = true;
            this.colGivenByPerson.VisibleIndex = 7;
            // 
            // colTotalAmount
            // 
            this.colTotalAmount.Caption = "Total Amount";
            this.colTotalAmount.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTotalAmount.FieldName = "TotalAmount";
            this.colTotalAmount.Name = "colTotalAmount";
            this.colTotalAmount.OptionsColumn.AllowEdit = false;
            this.colTotalAmount.OptionsColumn.AllowFocus = false;
            this.colTotalAmount.OptionsColumn.ReadOnly = true;
            this.colTotalAmount.Visible = true;
            this.colTotalAmount.VisibleIndex = 2;
            this.colTotalAmount.Width = 85;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Mask.EditMask = "c";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colAmountSpent
            // 
            this.colAmountSpent.Caption = "Amount Spent";
            this.colAmountSpent.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAmountSpent.FieldName = "AmountSpent";
            this.colAmountSpent.Name = "colAmountSpent";
            this.colAmountSpent.OptionsColumn.AllowEdit = false;
            this.colAmountSpent.OptionsColumn.AllowFocus = false;
            this.colAmountSpent.OptionsColumn.ReadOnly = true;
            this.colAmountSpent.Visible = true;
            this.colAmountSpent.VisibleIndex = 3;
            this.colAmountSpent.Width = 89;
            // 
            // colAmountRemaining
            // 
            this.colAmountRemaining.Caption = "Amount Remaining";
            this.colAmountRemaining.ColumnEdit = this.repositoryItemTextEdit1;
            this.colAmountRemaining.FieldName = "AmountRemaining";
            this.colAmountRemaining.Name = "colAmountRemaining";
            this.colAmountRemaining.OptionsColumn.AllowEdit = false;
            this.colAmountRemaining.OptionsColumn.AllowFocus = false;
            this.colAmountRemaining.OptionsColumn.ReadOnly = true;
            this.colAmountRemaining.Visible = true;
            this.colAmountRemaining.VisibleIndex = 4;
            this.colAmountRemaining.Width = 110;
            // 
            // colRemarks2
            // 
            this.colRemarks2.Caption = "Remarks";
            this.colRemarks2.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colRemarks2.FieldName = "Remarks";
            this.colRemarks2.Name = "colRemarks2";
            this.colRemarks2.OptionsColumn.ReadOnly = true;
            this.colRemarks2.Visible = true;
            this.colRemarks2.VisibleIndex = 5;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Width = 251;
            // 
            // colClientCode2
            // 
            this.colClientCode2.Caption = "Client Code";
            this.colClientCode2.FieldName = "ClientCode";
            this.colClientCode2.Name = "colClientCode2";
            this.colClientCode2.OptionsColumn.AllowEdit = false;
            this.colClientCode2.OptionsColumn.AllowFocus = false;
            this.colClientCode2.OptionsColumn.ReadOnly = true;
            this.colClientCode2.Width = 137;
            // 
            // gridSplitContainer6
            // 
            this.gridSplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer6.Grid = this.gridControl5;
            this.gridSplitContainer6.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer6.Name = "gridSplitContainer6";
            this.gridSplitContainer6.Panel1.Controls.Add(this.gridControl5);
            this.gridSplitContainer6.Size = new System.Drawing.Size(519, 233);
            this.gridSplitContainer6.TabIndex = 0;
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.sp04098GCSitesLinkedToClientPOBindingSource;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl5.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl5.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl5.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl5.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl5_EmbeddedNavigator_ButtonClick);
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView5;
            this.gridControl5.MenuManager = this.barManager1;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4});
            this.gridControl5.Size = new System.Drawing.Size(519, 233);
            this.gridControl5.TabIndex = 0;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView5});
            // 
            // sp04098GCSitesLinkedToClientPOBindingSource
            // 
            this.sp04098GCSitesLinkedToClientPOBindingSource.DataMember = "sp04098_GC_Sites_Linked_To_Client_PO";
            this.sp04098GCSitesLinkedToClientPOBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView5
            // 
            this.gridView5.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientPOSiteLinkID,
            this.colClientPOID1,
            this.colSiteID,
            this.colGrittingAllowed,
            this.colSnowClearanceAllowed,
            this.colPONumber1,
            this.colClientName3,
            this.colClientCode3,
            this.colSiteName,
            this.colGrittingDefault,
            this.colSnowClearanceDefault});
            this.gridView5.GridControl = this.gridControl5;
            this.gridView5.GroupCount = 2;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView5.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView5.OptionsLayout.StoreAppearance = true;
            this.gridView5.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView5.OptionsSelection.MultiSelect = true;
            this.gridView5.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView5.OptionsView.ColumnAutoWidth = false;
            this.gridView5.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            this.gridView5.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName3, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPONumber1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView5.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView5.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView5.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView5.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView5.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView5_MouseUp);
            this.gridView5.DoubleClick += new System.EventHandler(this.gridView5_DoubleClick);
            this.gridView5.GotFocus += new System.EventHandler(this.gridView5_GotFocus);
            // 
            // colClientPOSiteLinkID
            // 
            this.colClientPOSiteLinkID.Caption = "Client PO Site Link ID";
            this.colClientPOSiteLinkID.FieldName = "ClientPOSiteLinkID";
            this.colClientPOSiteLinkID.Name = "colClientPOSiteLinkID";
            this.colClientPOSiteLinkID.OptionsColumn.AllowEdit = false;
            this.colClientPOSiteLinkID.OptionsColumn.AllowFocus = false;
            this.colClientPOSiteLinkID.OptionsColumn.ReadOnly = true;
            this.colClientPOSiteLinkID.Width = 121;
            // 
            // colClientPOID1
            // 
            this.colClientPOID1.Caption = "Client PO ID";
            this.colClientPOID1.FieldName = "ClientPOID";
            this.colClientPOID1.Name = "colClientPOID1";
            this.colClientPOID1.OptionsColumn.AllowEdit = false;
            this.colClientPOID1.OptionsColumn.AllowFocus = false;
            this.colClientPOID1.OptionsColumn.ReadOnly = true;
            this.colClientPOID1.Width = 79;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colGrittingAllowed
            // 
            this.colGrittingAllowed.Caption = "Gritting Allowed";
            this.colGrittingAllowed.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colGrittingAllowed.FieldName = "GrittingAllowed";
            this.colGrittingAllowed.Name = "colGrittingAllowed";
            this.colGrittingAllowed.OptionsColumn.AllowEdit = false;
            this.colGrittingAllowed.OptionsColumn.AllowFocus = false;
            this.colGrittingAllowed.OptionsColumn.ReadOnly = true;
            this.colGrittingAllowed.Visible = true;
            this.colGrittingAllowed.VisibleIndex = 1;
            this.colGrittingAllowed.Width = 96;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "Check";
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // colSnowClearanceAllowed
            // 
            this.colSnowClearanceAllowed.Caption = "Snow Clearance Allowed";
            this.colSnowClearanceAllowed.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSnowClearanceAllowed.FieldName = "SnowClearanceAllowed";
            this.colSnowClearanceAllowed.Name = "colSnowClearanceAllowed";
            this.colSnowClearanceAllowed.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceAllowed.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceAllowed.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceAllowed.Visible = true;
            this.colSnowClearanceAllowed.VisibleIndex = 3;
            this.colSnowClearanceAllowed.Width = 138;
            // 
            // colPONumber1
            // 
            this.colPONumber1.Caption = "Purchase Order Number";
            this.colPONumber1.FieldName = "PONumber";
            this.colPONumber1.Name = "colPONumber1";
            this.colPONumber1.OptionsColumn.AllowEdit = false;
            this.colPONumber1.OptionsColumn.AllowFocus = false;
            this.colPONumber1.OptionsColumn.ReadOnly = true;
            this.colPONumber1.Width = 168;
            // 
            // colClientName3
            // 
            this.colClientName3.Caption = "Client Name";
            this.colClientName3.FieldName = "ClientName";
            this.colClientName3.Name = "colClientName3";
            this.colClientName3.OptionsColumn.AllowEdit = false;
            this.colClientName3.OptionsColumn.AllowFocus = false;
            this.colClientName3.OptionsColumn.ReadOnly = true;
            this.colClientName3.Width = 217;
            // 
            // colClientCode3
            // 
            this.colClientCode3.Caption = "Client Code";
            this.colClientCode3.FieldName = "ClientCode";
            this.colClientCode3.Name = "colClientCode3";
            this.colClientCode3.OptionsColumn.AllowEdit = false;
            this.colClientCode3.OptionsColumn.AllowFocus = false;
            this.colClientCode3.OptionsColumn.ReadOnly = true;
            this.colClientCode3.Width = 151;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 242;
            // 
            // colGrittingDefault
            // 
            this.colGrittingDefault.Caption = "Gritting Default";
            this.colGrittingDefault.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colGrittingDefault.FieldName = "GrittingDefault";
            this.colGrittingDefault.Name = "colGrittingDefault";
            this.colGrittingDefault.OptionsColumn.AllowEdit = false;
            this.colGrittingDefault.OptionsColumn.AllowFocus = false;
            this.colGrittingDefault.OptionsColumn.ReadOnly = true;
            this.colGrittingDefault.Visible = true;
            this.colGrittingDefault.VisibleIndex = 2;
            this.colGrittingDefault.Width = 94;
            // 
            // colSnowClearanceDefault
            // 
            this.colSnowClearanceDefault.Caption = "Snow Clearance Default";
            this.colSnowClearanceDefault.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colSnowClearanceDefault.FieldName = "SnowClearanceDefault";
            this.colSnowClearanceDefault.Name = "colSnowClearanceDefault";
            this.colSnowClearanceDefault.OptionsColumn.AllowEdit = false;
            this.colSnowClearanceDefault.OptionsColumn.AllowFocus = false;
            this.colSnowClearanceDefault.OptionsColumn.ReadOnly = true;
            this.colSnowClearanceDefault.Visible = true;
            this.colSnowClearanceDefault.VisibleIndex = 4;
            this.colSnowClearanceDefault.Width = 136;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridSplitContainer7);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(932, 236);
            this.xtraTabPage4.Text = "Sent Reports";
            // 
            // gridSplitContainer7
            // 
            this.gridSplitContainer7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer7.Grid = this.gridControl6;
            this.gridSplitContainer7.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer7.Name = "gridSplitContainer7";
            this.gridSplitContainer7.Panel1.Controls.Add(this.gridControl6);
            this.gridSplitContainer7.Size = new System.Drawing.Size(932, 236);
            this.gridSplitContainer7.TabIndex = 0;
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.sp04246GCSentReportsLinkedToClientBindingSource;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl6.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl6.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl6.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Records", "delete")});
            this.gridControl6.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl6_EmbeddedNavigator_ButtonClick);
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView6;
            this.gridControl6.MenuManager = this.barManager1;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditDateTime,
            this.repositoryItemHyperLinkEditLinkedReportFile});
            this.gridControl6.Size = new System.Drawing.Size(932, 236);
            this.gridControl6.TabIndex = 0;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // sp04246GCSentReportsLinkedToClientBindingSource
            // 
            this.sp04246GCSentReportsLinkedToClientBindingSource.DataMember = "sp04246_GC_Sent_Reports_Linked_To_Client";
            this.sp04246GCSentReportsLinkedToClientBindingSource.DataSource = this.dataSet_GC_Reports;
            // 
            // dataSet_GC_Reports
            // 
            this.dataSet_GC_Reports.DataSetName = "DataSet_GC_Reports";
            this.dataSet_GC_Reports.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSentReportID,
            this.colSentReportTypeID,
            this.colClientID3,
            this.colDateSent,
            this.colSentByStaffID,
            this.colLinkedReportFilename,
            this.colReportTypeDescription,
            this.colClientName4,
            this.colClientCode4,
            this.colSentByStaffName});
            this.gridView6.GridControl = this.gridControl6;
            this.gridView6.GroupCount = 2;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView6.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView6.OptionsLayout.StoreAppearance = true;
            this.gridView6.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView6.OptionsSelection.MultiSelect = true;
            this.gridView6.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView6.OptionsView.ColumnAutoWidth = false;
            this.gridView6.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName4, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colReportTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateSent, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView6.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView6.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView6.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView6.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView6.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView6_MouseUp);
            this.gridView6.DoubleClick += new System.EventHandler(this.gridView6_DoubleClick);
            this.gridView6.GotFocus += new System.EventHandler(this.gridView6_GotFocus);
            // 
            // colSentReportID
            // 
            this.colSentReportID.Caption = "Sent Report ID";
            this.colSentReportID.FieldName = "SentReportID";
            this.colSentReportID.Name = "colSentReportID";
            this.colSentReportID.OptionsColumn.AllowEdit = false;
            this.colSentReportID.OptionsColumn.AllowFocus = false;
            this.colSentReportID.OptionsColumn.ReadOnly = true;
            this.colSentReportID.Width = 93;
            // 
            // colSentReportTypeID
            // 
            this.colSentReportTypeID.Caption = "Report Type ID";
            this.colSentReportTypeID.FieldName = "SentReportTypeID";
            this.colSentReportTypeID.Name = "colSentReportTypeID";
            this.colSentReportTypeID.OptionsColumn.AllowEdit = false;
            this.colSentReportTypeID.OptionsColumn.AllowFocus = false;
            this.colSentReportTypeID.OptionsColumn.ReadOnly = true;
            this.colSentReportTypeID.Width = 95;
            // 
            // colClientID3
            // 
            this.colClientID3.Caption = "Client ID";
            this.colClientID3.FieldName = "ClientID";
            this.colClientID3.Name = "colClientID3";
            this.colClientID3.OptionsColumn.AllowEdit = false;
            this.colClientID3.OptionsColumn.AllowFocus = false;
            this.colClientID3.OptionsColumn.ReadOnly = true;
            // 
            // colDateSent
            // 
            this.colDateSent.Caption = "Date Sent";
            this.colDateSent.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colDateSent.FieldName = "DateSent";
            this.colDateSent.Name = "colDateSent";
            this.colDateSent.OptionsColumn.AllowEdit = false;
            this.colDateSent.OptionsColumn.AllowFocus = false;
            this.colDateSent.OptionsColumn.ReadOnly = true;
            this.colDateSent.Visible = true;
            this.colDateSent.VisibleIndex = 0;
            this.colDateSent.Width = 176;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "dd/MM/yyyy HH:mm";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colSentByStaffID
            // 
            this.colSentByStaffID.Caption = "Sent By Staff ID";
            this.colSentByStaffID.FieldName = "SentByStaffID";
            this.colSentByStaffID.Name = "colSentByStaffID";
            this.colSentByStaffID.OptionsColumn.AllowEdit = false;
            this.colSentByStaffID.OptionsColumn.AllowFocus = false;
            this.colSentByStaffID.OptionsColumn.ReadOnly = true;
            this.colSentByStaffID.Width = 184;
            // 
            // colLinkedReportFilename
            // 
            this.colLinkedReportFilename.Caption = "Linked Report";
            this.colLinkedReportFilename.ColumnEdit = this.repositoryItemHyperLinkEditLinkedReportFile;
            this.colLinkedReportFilename.FieldName = "LinkedReportFilename";
            this.colLinkedReportFilename.Name = "colLinkedReportFilename";
            this.colLinkedReportFilename.OptionsColumn.ReadOnly = true;
            this.colLinkedReportFilename.Visible = true;
            this.colLinkedReportFilename.VisibleIndex = 1;
            this.colLinkedReportFilename.Width = 378;
            // 
            // repositoryItemHyperLinkEditLinkedReportFile
            // 
            this.repositoryItemHyperLinkEditLinkedReportFile.AutoHeight = false;
            this.repositoryItemHyperLinkEditLinkedReportFile.Name = "repositoryItemHyperLinkEditLinkedReportFile";
            this.repositoryItemHyperLinkEditLinkedReportFile.SingleClick = true;
            this.repositoryItemHyperLinkEditLinkedReportFile.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEditLinkedReportFile_OpenLink);
            // 
            // colReportTypeDescription
            // 
            this.colReportTypeDescription.Caption = "Report Type";
            this.colReportTypeDescription.FieldName = "ReportTypeDescription";
            this.colReportTypeDescription.Name = "colReportTypeDescription";
            this.colReportTypeDescription.OptionsColumn.AllowEdit = false;
            this.colReportTypeDescription.OptionsColumn.AllowFocus = false;
            this.colReportTypeDescription.OptionsColumn.ReadOnly = true;
            this.colReportTypeDescription.Width = 174;
            // 
            // colClientName4
            // 
            this.colClientName4.Caption = "Client Name";
            this.colClientName4.FieldName = "ClientName";
            this.colClientName4.Name = "colClientName4";
            this.colClientName4.OptionsColumn.AllowEdit = false;
            this.colClientName4.OptionsColumn.AllowFocus = false;
            this.colClientName4.OptionsColumn.ReadOnly = true;
            this.colClientName4.Width = 240;
            // 
            // colClientCode4
            // 
            this.colClientCode4.Caption = "Client Code";
            this.colClientCode4.FieldName = "ClientCode";
            this.colClientCode4.Name = "colClientCode4";
            this.colClientCode4.OptionsColumn.AllowEdit = false;
            this.colClientCode4.OptionsColumn.AllowFocus = false;
            this.colClientCode4.OptionsColumn.ReadOnly = true;
            this.colClientCode4.Width = 76;
            // 
            // colSentByStaffName
            // 
            this.colSentByStaffName.Caption = "Sent By Staff";
            this.colSentByStaffName.FieldName = "SentByStaffName";
            this.colSentByStaffName.Name = "colSentByStaffName";
            this.colSentByStaffName.OptionsColumn.AllowEdit = false;
            this.colSentByStaffName.OptionsColumn.AllowFocus = false;
            this.colSentByStaffName.OptionsColumn.ReadOnly = true;
            this.colSentByStaffName.Visible = true;
            this.colSentByStaffName.VisibleIndex = 2;
            this.colSentByStaffName.Width = 144;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.splitContainerControl3);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(932, 236);
            this.xtraTabPage5.Text = "Invoicing";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl3.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridSplitContainer8);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "Invoice Groups";
            this.splitContainerControl3.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl3.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl3.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl3.Panel2.Controls.Add(this.splitContainerControl5);
            this.splitContainerControl3.Size = new System.Drawing.Size(932, 236);
            this.splitContainerControl3.SplitterPosition = 385;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridSplitContainer8
            // 
            this.gridSplitContainer8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer8.Grid = this.gridControl7;
            this.gridSplitContainer8.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer8.Name = "gridSplitContainer8";
            this.gridSplitContainer8.Panel1.Controls.Add(this.gridControl7);
            this.gridSplitContainer8.Size = new System.Drawing.Size(516, 233);
            this.gridSplitContainer8.TabIndex = 0;
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.sp04247GCClientInvoiceGroupsBindingSource;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl7.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl7_EmbeddedNavigator_ButtonClick);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridView7;
            this.gridControl7.MenuManager = this.barManager1;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit5,
            this.repositoryItemMemoExEdit5});
            this.gridControl7.Size = new System.Drawing.Size(516, 233);
            this.gridControl7.TabIndex = 0;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // sp04247GCClientInvoiceGroupsBindingSource
            // 
            this.sp04247GCClientInvoiceGroupsBindingSource.DataMember = "sp04247_GC_Client_Invoice_Groups";
            this.sp04247GCClientInvoiceGroupsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInvoiceGroupID,
            this.colDescription1,
            this.colClientID4,
            this.colActive1,
            this.colIncludeGritting,
            this.colIncludeSnowClearance,
            this.colRemarks3,
            this.colClientName5,
            this.colClientCode5,
            this.colCareOfAddressLine1,
            this.colCareOfAddressLine2,
            this.colCareOfAddressLine3,
            this.colCareOfAddressLine4,
            this.colCareOfAddressLine5,
            this.colCareOfPostcode,
            this.colCareOfEmail,
            this.colAcceptsEmailInvoice});
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Gray;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.colActive1;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView7.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView7.GridControl = this.gridControl7;
            this.gridView7.GroupCount = 1;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView7.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView7.OptionsLayout.StoreAppearance = true;
            this.gridView7.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView7.OptionsSelection.MultiSelect = true;
            this.gridView7.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView7.OptionsView.ColumnAutoWidth = false;
            this.gridView7.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            this.gridView7.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName5, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView7.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView7.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView7.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView7.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView7.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView7_MouseUp);
            this.gridView7.DoubleClick += new System.EventHandler(this.gridView7_DoubleClick);
            this.gridView7.GotFocus += new System.EventHandler(this.gridView7_GotFocus);
            // 
            // colInvoiceGroupID
            // 
            this.colInvoiceGroupID.Caption = "Invoice Group ID";
            this.colInvoiceGroupID.FieldName = "InvoiceGroupID";
            this.colInvoiceGroupID.Name = "colInvoiceGroupID";
            this.colInvoiceGroupID.OptionsColumn.AllowEdit = false;
            this.colInvoiceGroupID.OptionsColumn.AllowFocus = false;
            this.colInvoiceGroupID.OptionsColumn.ReadOnly = true;
            this.colInvoiceGroupID.Width = 102;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Description";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.AllowFocus = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 282;
            // 
            // colClientID4
            // 
            this.colClientID4.Caption = "Client ID";
            this.colClientID4.FieldName = "ClientID";
            this.colClientID4.Name = "colClientID4";
            this.colClientID4.OptionsColumn.AllowEdit = false;
            this.colClientID4.OptionsColumn.AllowFocus = false;
            this.colClientID4.OptionsColumn.ReadOnly = true;
            // 
            // colIncludeGritting
            // 
            this.colIncludeGritting.Caption = "Gritting";
            this.colIncludeGritting.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colIncludeGritting.FieldName = "IncludeGritting";
            this.colIncludeGritting.Name = "colIncludeGritting";
            this.colIncludeGritting.OptionsColumn.AllowEdit = false;
            this.colIncludeGritting.OptionsColumn.AllowFocus = false;
            this.colIncludeGritting.OptionsColumn.ReadOnly = true;
            this.colIncludeGritting.Visible = true;
            this.colIncludeGritting.VisibleIndex = 2;
            this.colIncludeGritting.Width = 56;
            // 
            // colIncludeSnowClearance
            // 
            this.colIncludeSnowClearance.Caption = "Snow Clearance";
            this.colIncludeSnowClearance.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colIncludeSnowClearance.FieldName = "IncludeSnowClearance";
            this.colIncludeSnowClearance.Name = "colIncludeSnowClearance";
            this.colIncludeSnowClearance.OptionsColumn.AllowEdit = false;
            this.colIncludeSnowClearance.OptionsColumn.AllowFocus = false;
            this.colIncludeSnowClearance.OptionsColumn.ReadOnly = true;
            this.colIncludeSnowClearance.Visible = true;
            this.colIncludeSnowClearance.VisibleIndex = 3;
            this.colIncludeSnowClearance.Width = 98;
            // 
            // colRemarks3
            // 
            this.colRemarks3.Caption = "Remarks";
            this.colRemarks3.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colRemarks3.FieldName = "Remarks";
            this.colRemarks3.Name = "colRemarks3";
            this.colRemarks3.OptionsColumn.ReadOnly = true;
            this.colRemarks3.Visible = true;
            this.colRemarks3.VisibleIndex = 4;
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // colClientName5
            // 
            this.colClientName5.Caption = "Client Name";
            this.colClientName5.FieldName = "ClientName";
            this.colClientName5.Name = "colClientName5";
            this.colClientName5.OptionsColumn.AllowEdit = false;
            this.colClientName5.OptionsColumn.AllowFocus = false;
            this.colClientName5.OptionsColumn.ReadOnly = true;
            this.colClientName5.Visible = true;
            this.colClientName5.VisibleIndex = 5;
            this.colClientName5.Width = 217;
            // 
            // colClientCode5
            // 
            this.colClientCode5.Caption = "Client Code";
            this.colClientCode5.FieldName = "ClientCode";
            this.colClientCode5.Name = "colClientCode5";
            this.colClientCode5.OptionsColumn.AllowEdit = false;
            this.colClientCode5.OptionsColumn.AllowFocus = false;
            this.colClientCode5.OptionsColumn.ReadOnly = true;
            this.colClientCode5.Width = 76;
            // 
            // colCareOfAddressLine1
            // 
            this.colCareOfAddressLine1.Caption = "C\\O Address Line 1";
            this.colCareOfAddressLine1.FieldName = "CareOfAddressLine1";
            this.colCareOfAddressLine1.Name = "colCareOfAddressLine1";
            this.colCareOfAddressLine1.OptionsColumn.AllowEdit = false;
            this.colCareOfAddressLine1.OptionsColumn.AllowFocus = false;
            this.colCareOfAddressLine1.OptionsColumn.ReadOnly = true;
            this.colCareOfAddressLine1.Visible = true;
            this.colCareOfAddressLine1.VisibleIndex = 6;
            this.colCareOfAddressLine1.Width = 111;
            // 
            // colCareOfAddressLine2
            // 
            this.colCareOfAddressLine2.Caption = "C\\O Address Line 2";
            this.colCareOfAddressLine2.FieldName = "CareOfAddressLine2";
            this.colCareOfAddressLine2.Name = "colCareOfAddressLine2";
            this.colCareOfAddressLine2.OptionsColumn.AllowEdit = false;
            this.colCareOfAddressLine2.OptionsColumn.AllowFocus = false;
            this.colCareOfAddressLine2.OptionsColumn.ReadOnly = true;
            this.colCareOfAddressLine2.Visible = true;
            this.colCareOfAddressLine2.VisibleIndex = 7;
            this.colCareOfAddressLine2.Width = 111;
            // 
            // colCareOfAddressLine3
            // 
            this.colCareOfAddressLine3.Caption = "C\\O Address Line 3";
            this.colCareOfAddressLine3.FieldName = "CareOfAddressLine3";
            this.colCareOfAddressLine3.Name = "colCareOfAddressLine3";
            this.colCareOfAddressLine3.OptionsColumn.AllowEdit = false;
            this.colCareOfAddressLine3.OptionsColumn.AllowFocus = false;
            this.colCareOfAddressLine3.OptionsColumn.ReadOnly = true;
            this.colCareOfAddressLine3.Visible = true;
            this.colCareOfAddressLine3.VisibleIndex = 8;
            this.colCareOfAddressLine3.Width = 111;
            // 
            // colCareOfAddressLine4
            // 
            this.colCareOfAddressLine4.Caption = "C\\O Address Line 4";
            this.colCareOfAddressLine4.FieldName = "CareOfAddressLine4";
            this.colCareOfAddressLine4.Name = "colCareOfAddressLine4";
            this.colCareOfAddressLine4.OptionsColumn.AllowEdit = false;
            this.colCareOfAddressLine4.OptionsColumn.AllowFocus = false;
            this.colCareOfAddressLine4.OptionsColumn.ReadOnly = true;
            this.colCareOfAddressLine4.Visible = true;
            this.colCareOfAddressLine4.VisibleIndex = 9;
            this.colCareOfAddressLine4.Width = 111;
            // 
            // colCareOfAddressLine5
            // 
            this.colCareOfAddressLine5.Caption = "C\\O Address Line 5";
            this.colCareOfAddressLine5.FieldName = "CareOfAddressLine5";
            this.colCareOfAddressLine5.Name = "colCareOfAddressLine5";
            this.colCareOfAddressLine5.OptionsColumn.AllowEdit = false;
            this.colCareOfAddressLine5.OptionsColumn.AllowFocus = false;
            this.colCareOfAddressLine5.OptionsColumn.ReadOnly = true;
            this.colCareOfAddressLine5.Visible = true;
            this.colCareOfAddressLine5.VisibleIndex = 10;
            this.colCareOfAddressLine5.Width = 111;
            // 
            // colCareOfPostcode
            // 
            this.colCareOfPostcode.Caption = "C\\O Postcode";
            this.colCareOfPostcode.FieldName = "CareOfPostcode";
            this.colCareOfPostcode.Name = "colCareOfPostcode";
            this.colCareOfPostcode.OptionsColumn.AllowEdit = false;
            this.colCareOfPostcode.OptionsColumn.AllowFocus = false;
            this.colCareOfPostcode.OptionsColumn.ReadOnly = true;
            this.colCareOfPostcode.Visible = true;
            this.colCareOfPostcode.VisibleIndex = 11;
            this.colCareOfPostcode.Width = 85;
            // 
            // colCareOfEmail
            // 
            this.colCareOfEmail.Caption = "C\\O Email";
            this.colCareOfEmail.ColumnEdit = this.repositoryItemMemoExEdit5;
            this.colCareOfEmail.FieldName = "CareOfEmail";
            this.colCareOfEmail.Name = "colCareOfEmail";
            this.colCareOfEmail.OptionsColumn.ReadOnly = true;
            this.colCareOfEmail.Visible = true;
            this.colCareOfEmail.VisibleIndex = 12;
            this.colCareOfEmail.Width = 143;
            // 
            // colAcceptsEmailInvoice
            // 
            this.colAcceptsEmailInvoice.Caption = "Accepts Email Invoice";
            this.colAcceptsEmailInvoice.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colAcceptsEmailInvoice.FieldName = "AcceptsEmailInvoice";
            this.colAcceptsEmailInvoice.Name = "colAcceptsEmailInvoice";
            this.colAcceptsEmailInvoice.OptionsColumn.AllowEdit = false;
            this.colAcceptsEmailInvoice.OptionsColumn.AllowFocus = false;
            this.colAcceptsEmailInvoice.OptionsColumn.ReadOnly = true;
            this.colAcceptsEmailInvoice.Visible = true;
            this.colAcceptsEmailInvoice.VisibleIndex = 5;
            this.colAcceptsEmailInvoice.Width = 122;
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel2;
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl5.Horizontal = false;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl5.Panel1.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl5.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl5.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl5.Panel1.Controls.Add(this.gridSplitContainer9);
            this.splitContainerControl5.Panel1.ShowCaption = true;
            this.splitContainerControl5.Panel1.Text = "Linked Sites";
            this.splitContainerControl5.Panel2.AppearanceCaption.Options.UseTextOptions = true;
            this.splitContainerControl5.Panel2.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.splitContainerControl5.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this.splitContainerControl5.Panel2.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl5.Panel2.Controls.Add(this.gridSplitContainer11);
            this.splitContainerControl5.Panel2.ShowCaption = true;
            this.splitContainerControl5.Panel2.Text = "Linked Contacts";
            this.splitContainerControl5.Size = new System.Drawing.Size(381, 232);
            this.splitContainerControl5.SplitterPosition = 115;
            this.splitContainerControl5.TabIndex = 1;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // gridSplitContainer9
            // 
            this.gridSplitContainer9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer9.Grid = this.gridControl8;
            this.gridSplitContainer9.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer9.Name = "gridSplitContainer9";
            this.gridSplitContainer9.Panel1.Controls.Add(this.gridControl8);
            this.gridSplitContainer9.Size = new System.Drawing.Size(356, 108);
            this.gridSplitContainer9.TabIndex = 0;
            // 
            // gridControl8
            // 
            this.gridControl8.DataSource = this.sp04248GCClientInvoiceGroupMembersBindingSource;
            this.gridControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl8.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl8.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl8.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl8.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl8_EmbeddedNavigator_ButtonClick);
            this.gridControl8.Location = new System.Drawing.Point(0, 0);
            this.gridControl8.MainView = this.gridView8;
            this.gridControl8.MenuManager = this.barManager1;
            this.gridControl8.Name = "gridControl8";
            this.gridControl8.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit6,
            this.repositoryItemMemoExEdit6});
            this.gridControl8.Size = new System.Drawing.Size(356, 108);
            this.gridControl8.TabIndex = 0;
            this.gridControl8.UseEmbeddedNavigator = true;
            this.gridControl8.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView8});
            // 
            // sp04248GCClientInvoiceGroupMembersBindingSource
            // 
            this.sp04248GCClientInvoiceGroupMembersBindingSource.DataMember = "sp04248_GC_Client_Invoice_Group_Members";
            this.sp04248GCClientInvoiceGroupMembersBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView8
            // 
            this.gridView8.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInvoiceGroupMember,
            this.colInvoiceGroupID1,
            this.colSiteID1,
            this.colRemarks4,
            this.colGroupDescription,
            this.colClientID5,
            this.colActive2,
            this.colIncludeGritting1,
            this.colIncludeSnowClearance1,
            this.colSiteName1,
            this.colSiteCode,
            this.colClientName6,
            this.colClientCode6});
            this.gridView8.GridControl = this.gridControl8;
            this.gridView8.GroupCount = 2;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView8.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView8.OptionsLayout.StoreAppearance = true;
            this.gridView8.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView8.OptionsSelection.MultiSelect = true;
            this.gridView8.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView8.OptionsView.ColumnAutoWidth = false;
            this.gridView8.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            this.gridView8.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupDescription, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView8.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView8.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView8.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView8.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView8.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView8_MouseUp);
            this.gridView8.DoubleClick += new System.EventHandler(this.gridView8_DoubleClick);
            this.gridView8.GotFocus += new System.EventHandler(this.gridView8_GotFocus);
            // 
            // colInvoiceGroupMember
            // 
            this.colInvoiceGroupMember.Caption = "Invoice Group Member";
            this.colInvoiceGroupMember.FieldName = "InvoiceGroupMember";
            this.colInvoiceGroupMember.Name = "colInvoiceGroupMember";
            this.colInvoiceGroupMember.OptionsColumn.AllowEdit = false;
            this.colInvoiceGroupMember.OptionsColumn.AllowFocus = false;
            this.colInvoiceGroupMember.OptionsColumn.ReadOnly = true;
            this.colInvoiceGroupMember.Width = 129;
            // 
            // colInvoiceGroupID1
            // 
            this.colInvoiceGroupID1.Caption = "Invoice Group ID";
            this.colInvoiceGroupID1.FieldName = "InvoiceGroupID";
            this.colInvoiceGroupID1.Name = "colInvoiceGroupID1";
            this.colInvoiceGroupID1.OptionsColumn.AllowEdit = false;
            this.colInvoiceGroupID1.OptionsColumn.AllowFocus = false;
            this.colInvoiceGroupID1.OptionsColumn.ReadOnly = true;
            this.colInvoiceGroupID1.Width = 102;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            // 
            // colRemarks4
            // 
            this.colRemarks4.Caption = "Remarks";
            this.colRemarks4.ColumnEdit = this.repositoryItemMemoExEdit6;
            this.colRemarks4.FieldName = "Remarks";
            this.colRemarks4.Name = "colRemarks4";
            this.colRemarks4.OptionsColumn.ReadOnly = true;
            this.colRemarks4.Visible = true;
            this.colRemarks4.VisibleIndex = 1;
            // 
            // repositoryItemMemoExEdit6
            // 
            this.repositoryItemMemoExEdit6.AutoHeight = false;
            this.repositoryItemMemoExEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit6.Name = "repositoryItemMemoExEdit6";
            this.repositoryItemMemoExEdit6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit6.ShowIcon = false;
            // 
            // colGroupDescription
            // 
            this.colGroupDescription.Caption = "Group Description";
            this.colGroupDescription.FieldName = "GroupDescription";
            this.colGroupDescription.Name = "colGroupDescription";
            this.colGroupDescription.OptionsColumn.AllowEdit = false;
            this.colGroupDescription.OptionsColumn.AllowFocus = false;
            this.colGroupDescription.OptionsColumn.ReadOnly = true;
            this.colGroupDescription.Width = 211;
            // 
            // colClientID5
            // 
            this.colClientID5.Caption = "Client ID";
            this.colClientID5.FieldName = "ClientID";
            this.colClientID5.Name = "colClientID5";
            this.colClientID5.OptionsColumn.AllowEdit = false;
            this.colClientID5.OptionsColumn.AllowFocus = false;
            this.colClientID5.OptionsColumn.ReadOnly = true;
            // 
            // colActive2
            // 
            this.colActive2.Caption = "Active";
            this.colActive2.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colActive2.FieldName = "Active";
            this.colActive2.Name = "colActive2";
            this.colActive2.OptionsColumn.AllowEdit = false;
            this.colActive2.OptionsColumn.AllowFocus = false;
            this.colActive2.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit6
            // 
            this.repositoryItemCheckEdit6.AutoHeight = false;
            this.repositoryItemCheckEdit6.Caption = "Check";
            this.repositoryItemCheckEdit6.Name = "repositoryItemCheckEdit6";
            this.repositoryItemCheckEdit6.ValueChecked = 1;
            this.repositoryItemCheckEdit6.ValueUnchecked = 0;
            // 
            // colIncludeGritting1
            // 
            this.colIncludeGritting1.Caption = "Gritting";
            this.colIncludeGritting1.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colIncludeGritting1.FieldName = "IncludeGritting";
            this.colIncludeGritting1.Name = "colIncludeGritting1";
            this.colIncludeGritting1.OptionsColumn.AllowEdit = false;
            this.colIncludeGritting1.OptionsColumn.AllowFocus = false;
            this.colIncludeGritting1.OptionsColumn.ReadOnly = true;
            // 
            // colIncludeSnowClearance1
            // 
            this.colIncludeSnowClearance1.Caption = "Snow Clearance";
            this.colIncludeSnowClearance1.ColumnEdit = this.repositoryItemCheckEdit6;
            this.colIncludeSnowClearance1.FieldName = "IncludeSnowClearance";
            this.colIncludeSnowClearance1.Name = "colIncludeSnowClearance1";
            this.colIncludeSnowClearance1.OptionsColumn.AllowEdit = false;
            this.colIncludeSnowClearance1.OptionsColumn.AllowFocus = false;
            this.colIncludeSnowClearance1.OptionsColumn.ReadOnly = true;
            this.colIncludeSnowClearance1.Width = 98;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 0;
            this.colSiteName1.Width = 242;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            // 
            // colClientName6
            // 
            this.colClientName6.Caption = "Client Name";
            this.colClientName6.FieldName = "ClientName";
            this.colClientName6.Name = "colClientName6";
            this.colClientName6.OptionsColumn.AllowEdit = false;
            this.colClientName6.OptionsColumn.AllowFocus = false;
            this.colClientName6.OptionsColumn.ReadOnly = true;
            this.colClientName6.Width = 200;
            // 
            // colClientCode6
            // 
            this.colClientCode6.Caption = "Client Code";
            this.colClientCode6.FieldName = "ClientCode";
            this.colClientCode6.Name = "colClientCode6";
            this.colClientCode6.OptionsColumn.AllowEdit = false;
            this.colClientCode6.OptionsColumn.AllowFocus = false;
            this.colClientCode6.OptionsColumn.ReadOnly = true;
            this.colClientCode6.Width = 76;
            // 
            // gridSplitContainer11
            // 
            this.gridSplitContainer11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer11.Grid = this.gridControl11;
            this.gridSplitContainer11.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer11.Name = "gridSplitContainer11";
            this.gridSplitContainer11.Panel1.Controls.Add(this.gridControl11);
            this.gridSplitContainer11.Size = new System.Drawing.Size(356, 112);
            this.gridSplitContainer11.TabIndex = 0;
            // 
            // gridControl11
            // 
            this.gridControl11.DataSource = this.sp04368GCClientInvoiceGroupClientContactsBindingSource;
            this.gridControl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl11.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl11.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl11.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl11.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl11_EmbeddedNavigator_ButtonClick);
            this.gridControl11.Location = new System.Drawing.Point(0, 0);
            this.gridControl11.MainView = this.gridView11;
            this.gridControl11.MenuManager = this.barManager1;
            this.gridControl11.Name = "gridControl11";
            this.gridControl11.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit7,
            this.repositoryItemMemoExEdit9});
            this.gridControl11.Size = new System.Drawing.Size(356, 112);
            this.gridControl11.TabIndex = 0;
            this.gridControl11.UseEmbeddedNavigator = true;
            this.gridControl11.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // sp04368GCClientInvoiceGroupClientContactsBindingSource
            // 
            this.sp04368GCClientInvoiceGroupClientContactsBindingSource.DataMember = "sp04368_GC_Client_Invoice_Group_Client_Contacts";
            this.sp04368GCClientInvoiceGroupClientContactsBindingSource.DataSource = this.dataSet_GC_Core;
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInvoiceGroupClientContactID,
            this.gridColumn8,
            this.colClientContactID1,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.colContactDetails1,
            this.colContactType1,
            this.colContactTypeID1,
            this.gridColumn18,
            this.gridColumn19});
            this.gridView11.GridControl = this.gridControl11;
            this.gridView11.GroupCount = 2;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView11.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView11.OptionsLayout.StoreAppearance = true;
            this.gridView11.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView11.OptionsSelection.MultiSelect = true;
            this.gridView11.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView11.OptionsView.ColumnAutoWidth = false;
            this.gridView11.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn18, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn11, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContactType1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colContactDetails1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView11.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView11.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView11.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView11.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView11.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView11.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView11_MouseUp);
            this.gridView11.DoubleClick += new System.EventHandler(this.gridView11_DoubleClick);
            this.gridView11.GotFocus += new System.EventHandler(this.gridView11_GotFocus);
            // 
            // colInvoiceGroupClientContactID
            // 
            this.colInvoiceGroupClientContactID.Caption = "Invoice Group Client Contact ID";
            this.colInvoiceGroupClientContactID.FieldName = "InvoiceGroupClientContactID";
            this.colInvoiceGroupClientContactID.Name = "colInvoiceGroupClientContactID";
            this.colInvoiceGroupClientContactID.OptionsColumn.AllowEdit = false;
            this.colInvoiceGroupClientContactID.OptionsColumn.AllowFocus = false;
            this.colInvoiceGroupClientContactID.OptionsColumn.ReadOnly = true;
            this.colInvoiceGroupClientContactID.Width = 171;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Invoice Group ID";
            this.gridColumn8.FieldName = "InvoiceGroupID";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.Width = 102;
            // 
            // colClientContactID1
            // 
            this.colClientContactID1.Caption = "Client Contact ID";
            this.colClientContactID1.FieldName = "ClientContactID";
            this.colClientContactID1.Name = "colClientContactID1";
            this.colClientContactID1.OptionsColumn.AllowEdit = false;
            this.colClientContactID1.OptionsColumn.AllowFocus = false;
            this.colClientContactID1.OptionsColumn.ReadOnly = true;
            this.colClientContactID1.Width = 101;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Remarks";
            this.gridColumn10.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.gridColumn10.FieldName = "Remarks";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 2;
            // 
            // repositoryItemMemoExEdit9
            // 
            this.repositoryItemMemoExEdit9.AutoHeight = false;
            this.repositoryItemMemoExEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit9.Name = "repositoryItemMemoExEdit9";
            this.repositoryItemMemoExEdit9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit9.ShowIcon = false;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "Group Description";
            this.gridColumn11.FieldName = "GroupDescription";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Width = 211;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Client ID";
            this.gridColumn12.FieldName = "ClientID";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Active";
            this.gridColumn13.ColumnEdit = this.repositoryItemCheckEdit7;
            this.gridColumn13.FieldName = "Active";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.AllowEdit = false;
            this.gridColumn13.OptionsColumn.AllowFocus = false;
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemCheckEdit7
            // 
            this.repositoryItemCheckEdit7.AutoHeight = false;
            this.repositoryItemCheckEdit7.Caption = "Check";
            this.repositoryItemCheckEdit7.Name = "repositoryItemCheckEdit7";
            this.repositoryItemCheckEdit7.ValueChecked = 1;
            this.repositoryItemCheckEdit7.ValueUnchecked = 0;
            // 
            // gridColumn14
            // 
            this.gridColumn14.Caption = "Gritting";
            this.gridColumn14.ColumnEdit = this.repositoryItemCheckEdit7;
            this.gridColumn14.FieldName = "IncludeGritting";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsColumn.AllowEdit = false;
            this.gridColumn14.OptionsColumn.AllowFocus = false;
            this.gridColumn14.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Snow Clearance";
            this.gridColumn15.ColumnEdit = this.repositoryItemCheckEdit7;
            this.gridColumn15.FieldName = "IncludeSnowClearance";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.AllowFocus = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.Width = 98;
            // 
            // colContactDetails1
            // 
            this.colContactDetails1.Caption = "Contact Details";
            this.colContactDetails1.ColumnEdit = this.repositoryItemMemoExEdit9;
            this.colContactDetails1.FieldName = "ContactDetails";
            this.colContactDetails1.Name = "colContactDetails1";
            this.colContactDetails1.OptionsColumn.ReadOnly = true;
            this.colContactDetails1.Visible = true;
            this.colContactDetails1.VisibleIndex = 1;
            this.colContactDetails1.Width = 185;
            // 
            // colContactType1
            // 
            this.colContactType1.Caption = "Contact Type";
            this.colContactType1.FieldName = "ContactType";
            this.colContactType1.Name = "colContactType1";
            this.colContactType1.OptionsColumn.AllowEdit = false;
            this.colContactType1.OptionsColumn.AllowFocus = false;
            this.colContactType1.OptionsColumn.ReadOnly = true;
            this.colContactType1.Visible = true;
            this.colContactType1.VisibleIndex = 0;
            this.colContactType1.Width = 126;
            // 
            // colContactTypeID1
            // 
            this.colContactTypeID1.Caption = "Contact Type ID";
            this.colContactTypeID1.FieldName = "ContactTypeID";
            this.colContactTypeID1.Name = "colContactTypeID1";
            this.colContactTypeID1.OptionsColumn.AllowEdit = false;
            this.colContactTypeID1.OptionsColumn.AllowFocus = false;
            this.colContactTypeID1.OptionsColumn.ReadOnly = true;
            this.colContactTypeID1.Width = 98;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Client Name";
            this.gridColumn18.FieldName = "ClientName";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowEdit = false;
            this.gridColumn18.OptionsColumn.AllowFocus = false;
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.Width = 200;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Client Code";
            this.gridColumn19.FieldName = "ClientCode";
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowEdit = false;
            this.gridColumn19.OptionsColumn.AllowFocus = false;
            this.gridColumn19.OptionsColumn.ReadOnly = true;
            this.gridColumn19.Width = 76;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.splitContainerControl4);
            this.xtraTabPage6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("xtraTabPage6.ImageOptions.Image")));
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(932, 236);
            this.xtraTabPage6.Text = "CRM";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.CollapsePanel = DevExpress.XtraEditors.SplitCollapsePanel.Panel1;
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.btnReloadCRM);
            this.splitContainerControl4.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl4.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl4.Panel1.Controls.Add(this.dateEditFromDate);
            this.splitContainerControl4.Panel1.Controls.Add(this.dateEditToDate);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.gridSplitContainer10);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(932, 236);
            this.splitContainerControl4.SplitterPosition = 25;
            this.splitContainerControl4.TabIndex = 22;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // btnReloadCRM
            // 
            this.btnReloadCRM.Location = new System.Drawing.Point(415, 3);
            this.btnReloadCRM.Name = "btnReloadCRM";
            this.btnReloadCRM.Size = new System.Drawing.Size(75, 20);
            this.btnReloadCRM.TabIndex = 24;
            this.btnReloadCRM.Text = "Reload Data";
            this.btnReloadCRM.Click += new System.EventHandler(this.btnReloadCRM_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(254, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(16, 13);
            this.labelControl2.TabIndex = 23;
            this.labelControl2.Text = "To:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(103, 13);
            this.labelControl1.TabIndex = 22;
            this.labelControl1.Text = "Contact Filter - From:";
            // 
            // dateEditFromDate
            // 
            this.dateEditFromDate.EditValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Location = new System.Drawing.Point(111, 3);
            this.dateEditFromDate.MenuManager = this.barManager1;
            this.dateEditFromDate.Name = "dateEditFromDate";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem1.Text = "Clear Date - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Jo" +
    "bs will be loaded.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.dateEditFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "", null, superToolTip1, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFromDate.Properties.Mask.EditMask = "g";
            this.dateEditFromDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditFromDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditFromDate.Properties.NullValuePrompt = "No Date";
            this.dateEditFromDate.Size = new System.Drawing.Size(135, 20);
            this.dateEditFromDate.TabIndex = 20;
            this.dateEditFromDate.ToolTip = "Date Range - Contact Made From";
            this.dateEditFromDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditFromDate_ButtonClick);
            // 
            // dateEditToDate
            // 
            this.dateEditToDate.EditValue = null;
            this.dateEditToDate.Location = new System.Drawing.Point(274, 3);
            this.dateEditToDate.MenuManager = this.barManager1;
            this.dateEditToDate.Name = "dateEditToDate";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem2.Text = "Clear Date - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>Clear</b> the date value.\r\n\r\nIf no date value is specified, all Ac" +
    "tions will be loaded.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.dateEditToDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Delete, "Clear", -1, true, true, false, editorButtonImageOptions2, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "", null, superToolTip2, DevExpress.Utils.ToolTipAnchor.Default)});
            this.dateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditToDate.Properties.Mask.EditMask = "g";
            this.dateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.dateEditToDate.Properties.MaxValue = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.MinValue = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dateEditToDate.Properties.NullValuePrompt = "No Date";
            this.dateEditToDate.Size = new System.Drawing.Size(135, 20);
            this.dateEditToDate.TabIndex = 21;
            this.dateEditToDate.ToolTip = "Date Range - Contact Made To";
            this.dateEditToDate.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.dateEditToDate_ButtonClick);
            // 
            // gridSplitContainer10
            // 
            this.gridSplitContainer10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSplitContainer10.Grid = this.gridControl9;
            this.gridSplitContainer10.Location = new System.Drawing.Point(0, 0);
            this.gridSplitContainer10.Name = "gridSplitContainer10";
            this.gridSplitContainer10.Panel1.Controls.Add(this.gridControl9);
            this.gridSplitContainer10.Size = new System.Drawing.Size(932, 205);
            this.gridSplitContainer10.TabIndex = 0;
            // 
            // gridControl9
            // 
            this.gridControl9.DataSource = this.sp05080CRMContactListBindingSource;
            this.gridControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl9.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl9.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl9.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selcted Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete")});
            this.gridControl9.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl9_EmbeddedNavigator_ButtonClick);
            this.gridControl9.Location = new System.Drawing.Point(0, 0);
            this.gridControl9.MainView = this.gridView9;
            this.gridControl9.MenuManager = this.barManager1;
            this.gridControl9.Name = "gridControl9";
            this.gridControl9.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit7,
            this.repositoryItemHyperLinkEdit3,
            this.repositoryItemTextDateTime});
            this.gridControl9.Size = new System.Drawing.Size(932, 205);
            this.gridControl9.TabIndex = 5;
            this.gridControl9.UseEmbeddedNavigator = true;
            this.gridControl9.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView9});
            // 
            // sp05080CRMContactListBindingSource
            // 
            this.sp05080CRMContactListBindingSource.DataMember = "sp05080_CRM_Contact_List";
            this.sp05080CRMContactListBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCRMID,
            this.gridColumn1,
            this.gridColumn2,
            this.colLinkedToRecordTypeID1,
            this.colLinkedToRecordID1,
            this.colContactDueDateTime,
            this.colContactMadeDateTime,
            this.colContactMethodID,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.colStatusID,
            this.colCreatedByStaffID,
            this.colContactedByStaffID,
            this.colContactDirectionID,
            this.colLinkedRecordDescription1,
            this.colCreatedByStaffName,
            this.colContactedByStaffName,
            this.colContactMethod,
            this.colContactType,
            this.colStatusDescription,
            this.colContactDirectionDescription,
            this.gridColumn6,
            this.colLinkedRecordTypeDescription,
            this.colDateCreated,
            this.colClientContact});
            this.gridView9.GridControl = this.gridControl9;
            this.gridView9.GroupCount = 1;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView9.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView9.OptionsLayout.StoreAppearance = true;
            this.gridView9.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView9.OptionsSelection.MultiSelect = true;
            this.gridView9.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView9.OptionsView.ColumnAutoWidth = false;
            this.gridView9.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            this.gridView9.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateCreated, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView9.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView9_CustomRowCellEdit);
            this.gridView9.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView9.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView9.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView9.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridView9_ShowingEditor);
            this.gridView9.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView9.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView9_MouseUp);
            this.gridView9.DoubleClick += new System.EventHandler(this.gridView9_DoubleClick);
            this.gridView9.GotFocus += new System.EventHandler(this.gridView9_GotFocus);
            // 
            // colCRMID
            // 
            this.colCRMID.Caption = "CRM ID";
            this.colCRMID.FieldName = "CRMID";
            this.colCRMID.Name = "colCRMID";
            this.colCRMID.OptionsColumn.AllowEdit = false;
            this.colCRMID.OptionsColumn.AllowFocus = false;
            this.colCRMID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Client ID";
            this.gridColumn1.FieldName = "ClientID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Client Contact ID";
            this.gridColumn2.FieldName = "ClientContactID";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Width = 100;
            // 
            // colLinkedToRecordTypeID1
            // 
            this.colLinkedToRecordTypeID1.Caption = "Linked To Record Type ID";
            this.colLinkedToRecordTypeID1.FieldName = "LinkedToRecordTypeID";
            this.colLinkedToRecordTypeID1.Name = "colLinkedToRecordTypeID1";
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID1.Width = 141;
            // 
            // colLinkedToRecordID1
            // 
            this.colLinkedToRecordID1.Caption = "Linked To Record ID";
            this.colLinkedToRecordID1.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID1.Name = "colLinkedToRecordID1";
            this.colLinkedToRecordID1.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID1.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID1.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID1.Width = 114;
            // 
            // colContactDueDateTime
            // 
            this.colContactDueDateTime.Caption = "Contact Due";
            this.colContactDueDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactDueDateTime.FieldName = "ContactDueDateTime";
            this.colContactDueDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactDueDateTime.Name = "colContactDueDateTime";
            this.colContactDueDateTime.OptionsColumn.AllowEdit = false;
            this.colContactDueDateTime.OptionsColumn.AllowFocus = false;
            this.colContactDueDateTime.OptionsColumn.ReadOnly = true;
            this.colContactDueDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactDueDateTime.Visible = true;
            this.colContactDueDateTime.VisibleIndex = 2;
            this.colContactDueDateTime.Width = 108;
            // 
            // repositoryItemTextDateTime
            // 
            this.repositoryItemTextDateTime.AutoHeight = false;
            this.repositoryItemTextDateTime.Mask.EditMask = "g";
            this.repositoryItemTextDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextDateTime.Name = "repositoryItemTextDateTime";
            // 
            // colContactMadeDateTime
            // 
            this.colContactMadeDateTime.Caption = "Contact Made";
            this.colContactMadeDateTime.ColumnEdit = this.repositoryItemTextDateTime;
            this.colContactMadeDateTime.FieldName = "ContactMadeDateTime";
            this.colContactMadeDateTime.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateRange;
            this.colContactMadeDateTime.Name = "colContactMadeDateTime";
            this.colContactMadeDateTime.OptionsColumn.AllowEdit = false;
            this.colContactMadeDateTime.OptionsColumn.AllowFocus = false;
            this.colContactMadeDateTime.OptionsColumn.ReadOnly = true;
            this.colContactMadeDateTime.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.DateSmart;
            this.colContactMadeDateTime.Visible = true;
            this.colContactMadeDateTime.VisibleIndex = 3;
            this.colContactMadeDateTime.Width = 102;
            // 
            // colContactMethodID
            // 
            this.colContactMethodID.Caption = "Contact Method ID";
            this.colContactMethodID.FieldName = "ContactMethodID";
            this.colContactMethodID.Name = "colContactMethodID";
            this.colContactMethodID.OptionsColumn.AllowEdit = false;
            this.colContactMethodID.OptionsColumn.AllowFocus = false;
            this.colContactMethodID.OptionsColumn.ReadOnly = true;
            this.colContactMethodID.Width = 109;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Contact Type ID";
            this.gridColumn3.FieldName = "ContactTypeID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Width = 97;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Description";
            this.gridColumn4.FieldName = "Description";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 6;
            this.gridColumn4.Width = 260;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Remarks";
            this.gridColumn5.ColumnEdit = this.repositoryItemMemoExEdit7;
            this.gridColumn5.FieldName = "Remarks";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 13;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // colStatusID
            // 
            this.colStatusID.Caption = "Status ID";
            this.colStatusID.FieldName = "StatusID";
            this.colStatusID.Name = "colStatusID";
            this.colStatusID.OptionsColumn.AllowEdit = false;
            this.colStatusID.OptionsColumn.AllowFocus = false;
            this.colStatusID.OptionsColumn.ReadOnly = true;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 113;
            // 
            // colContactedByStaffID
            // 
            this.colContactedByStaffID.Caption = "Contacted By";
            this.colContactedByStaffID.FieldName = "ContactedByStaffID";
            this.colContactedByStaffID.Name = "colContactedByStaffID";
            this.colContactedByStaffID.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffID.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffID.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffID.Width = 83;
            // 
            // colContactDirectionID
            // 
            this.colContactDirectionID.Caption = "Contact Direction ID";
            this.colContactDirectionID.FieldName = "ContactDirectionID";
            this.colContactDirectionID.Name = "colContactDirectionID";
            this.colContactDirectionID.OptionsColumn.AllowEdit = false;
            this.colContactDirectionID.OptionsColumn.AllowFocus = false;
            this.colContactDirectionID.OptionsColumn.ReadOnly = true;
            this.colContactDirectionID.Width = 101;
            // 
            // colLinkedRecordDescription1
            // 
            this.colLinkedRecordDescription1.Caption = "Linked Record";
            this.colLinkedRecordDescription1.ColumnEdit = this.repositoryItemHyperLinkEdit3;
            this.colLinkedRecordDescription1.FieldName = "LinkedRecordDescription";
            this.colLinkedRecordDescription1.Name = "colLinkedRecordDescription1";
            this.colLinkedRecordDescription1.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordDescription1.Visible = true;
            this.colLinkedRecordDescription1.VisibleIndex = 8;
            this.colLinkedRecordDescription1.Width = 191;
            // 
            // repositoryItemHyperLinkEdit3
            // 
            this.repositoryItemHyperLinkEdit3.AutoHeight = false;
            this.repositoryItemHyperLinkEdit3.Name = "repositoryItemHyperLinkEdit3";
            this.repositoryItemHyperLinkEdit3.SingleClick = true;
            this.repositoryItemHyperLinkEdit3.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit3_OpenLink);
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffName.Visible = true;
            this.colCreatedByStaffName.VisibleIndex = 12;
            this.colCreatedByStaffName.Width = 121;
            // 
            // colContactedByStaffName
            // 
            this.colContactedByStaffName.Caption = "GC Contact Person";
            this.colContactedByStaffName.FieldName = "ContactedByStaffName";
            this.colContactedByStaffName.Name = "colContactedByStaffName";
            this.colContactedByStaffName.OptionsColumn.AllowEdit = false;
            this.colContactedByStaffName.OptionsColumn.AllowFocus = false;
            this.colContactedByStaffName.OptionsColumn.ReadOnly = true;
            this.colContactedByStaffName.Visible = true;
            this.colContactedByStaffName.VisibleIndex = 4;
            this.colContactedByStaffName.Width = 131;
            // 
            // colContactMethod
            // 
            this.colContactMethod.Caption = "Contact Method";
            this.colContactMethod.FieldName = "ContactMethod";
            this.colContactMethod.Name = "colContactMethod";
            this.colContactMethod.OptionsColumn.AllowEdit = false;
            this.colContactMethod.OptionsColumn.AllowFocus = false;
            this.colContactMethod.OptionsColumn.ReadOnly = true;
            this.colContactMethod.Visible = true;
            this.colContactMethod.VisibleIndex = 9;
            this.colContactMethod.Width = 104;
            // 
            // colContactType
            // 
            this.colContactType.Caption = "Contact Type";
            this.colContactType.FieldName = "ContactType";
            this.colContactType.Name = "colContactType";
            this.colContactType.OptionsColumn.AllowEdit = false;
            this.colContactType.OptionsColumn.AllowFocus = false;
            this.colContactType.OptionsColumn.ReadOnly = true;
            this.colContactType.Visible = true;
            this.colContactType.VisibleIndex = 10;
            this.colContactType.Width = 102;
            // 
            // colStatusDescription
            // 
            this.colStatusDescription.Caption = "Status";
            this.colStatusDescription.FieldName = "StatusDescription";
            this.colStatusDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStatusDescription.Name = "colStatusDescription";
            this.colStatusDescription.OptionsColumn.AllowEdit = false;
            this.colStatusDescription.OptionsColumn.AllowFocus = false;
            this.colStatusDescription.OptionsColumn.ReadOnly = true;
            this.colStatusDescription.Visible = true;
            this.colStatusDescription.VisibleIndex = 1;
            // 
            // colContactDirectionDescription
            // 
            this.colContactDirectionDescription.Caption = "Contact Direction";
            this.colContactDirectionDescription.FieldName = "ContactDirectionDescription";
            this.colContactDirectionDescription.Name = "colContactDirectionDescription";
            this.colContactDirectionDescription.OptionsColumn.AllowEdit = false;
            this.colContactDirectionDescription.OptionsColumn.AllowFocus = false;
            this.colContactDirectionDescription.OptionsColumn.ReadOnly = true;
            this.colContactDirectionDescription.Visible = true;
            this.colContactDirectionDescription.VisibleIndex = 11;
            this.colContactDirectionDescription.Width = 101;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Client";
            this.gridColumn6.FieldName = "ClientName";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.Width = 229;
            // 
            // colLinkedRecordTypeDescription
            // 
            this.colLinkedRecordTypeDescription.Caption = "Linked Record Type";
            this.colLinkedRecordTypeDescription.FieldName = "LinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.Name = "colLinkedRecordTypeDescription";
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowEdit = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.AllowFocus = false;
            this.colLinkedRecordTypeDescription.OptionsColumn.ReadOnly = true;
            this.colLinkedRecordTypeDescription.Visible = true;
            this.colLinkedRecordTypeDescription.VisibleIndex = 7;
            this.colLinkedRecordTypeDescription.Width = 112;
            // 
            // colDateCreated
            // 
            this.colDateCreated.Caption = "Date Created";
            this.colDateCreated.ColumnEdit = this.repositoryItemTextDateTime;
            this.colDateCreated.FieldName = "DateCreated";
            this.colDateCreated.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colDateCreated.Name = "colDateCreated";
            this.colDateCreated.OptionsColumn.AllowEdit = false;
            this.colDateCreated.OptionsColumn.AllowFocus = false;
            this.colDateCreated.OptionsColumn.ReadOnly = true;
            this.colDateCreated.Visible = true;
            this.colDateCreated.VisibleIndex = 0;
            this.colDateCreated.Width = 120;
            // 
            // colClientContact
            // 
            this.colClientContact.Caption = "Client Contact Person";
            this.colClientContact.FieldName = "ClientContact";
            this.colClientContact.Name = "colClientContact";
            this.colClientContact.OptionsColumn.AllowEdit = false;
            this.colClientContact.OptionsColumn.AllowFocus = false;
            this.colClientContact.OptionsColumn.ReadOnly = true;
            this.colClientContact.Visible = true;
            this.colClientContact.VisibleIndex = 5;
            this.colClientContact.Width = 122;
            // 
            // sp00220_Linked_Documents_ListTableAdapter
            // 
            this.sp00220_Linked_Documents_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp04009_GC_Contacts_Linked_To_ClientTableAdapter
            // 
            this.sp04009_GC_Contacts_Linked_To_ClientTableAdapter.ClearBeforeFill = true;
            // 
            // sp04095_GC_POs_Linked_To_ClientTableAdapter
            // 
            this.sp04095_GC_POs_Linked_To_ClientTableAdapter.ClearBeforeFill = true;
            // 
            // sp04098_GC_Sites_Linked_To_Client_POTableAdapter
            // 
            this.sp04098_GC_Sites_Linked_To_Client_POTableAdapter.ClearBeforeFill = true;
            // 
            // sp04246_GC_Sent_Reports_Linked_To_ClientTableAdapter
            // 
            this.sp04246_GC_Sent_Reports_Linked_To_ClientTableAdapter.ClearBeforeFill = true;
            // 
            // sp04247_GC_Client_Invoice_GroupsTableAdapter
            // 
            this.sp04247_GC_Client_Invoice_GroupsTableAdapter.ClearBeforeFill = true;
            // 
            // sp04248_GC_Client_Invoice_Group_MembersTableAdapter
            // 
            this.sp04248_GC_Client_Invoice_Group_MembersTableAdapter.ClearBeforeFill = true;
            // 
            // pmInvoicing
            // 
            this.pmInvoicing.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiAdd),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiEdit),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiDelete),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiAddOutstandingSitesToInvoiceGroup, true)});
            this.pmInvoicing.Manager = this.barManager1;
            this.pmInvoicing.MenuCaption = "Invoice Groups Data Entry Menu";
            this.pmInvoicing.Name = "pmInvoicing";
            this.pmInvoicing.ShowCaption = true;
            // 
            // bbiAddOutstandingSitesToInvoiceGroup
            // 
            this.bbiAddOutstandingSitesToInvoiceGroup.Caption = "Add Unassigned Sites to Invoice Group";
            this.bbiAddOutstandingSitesToInvoiceGroup.Id = 27;
            this.bbiAddOutstandingSitesToInvoiceGroup.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiAddOutstandingSitesToInvoiceGroup.ImageOptions.Image")));
            this.bbiAddOutstandingSitesToInvoiceGroup.Name = "bbiAddOutstandingSitesToInvoiceGroup";
            this.bbiAddOutstandingSitesToInvoiceGroup.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddOutstandingSitesToInvoiceGroup_ItemClick);
            // 
            // sp05080_CRM_Contact_ListTableAdapter
            // 
            this.sp05080_CRM_Contact_ListTableAdapter.ClearBeforeFill = true;
            // 
            // sp05091_GC_Contact_People_Linked_To_ClientTableAdapter
            // 
            this.sp05091_GC_Contact_People_Linked_To_ClientTableAdapter.ClearBeforeFill = true;
            // 
            // sp04368_GC_Client_Invoice_Group_Client_ContactsTableAdapter
            // 
            this.sp04368_GC_Client_Invoice_Group_Client_ContactsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Client_Manager
            // 
            this.ClientSize = new System.Drawing.Size(937, 537);
            this.Controls.Add(this.splitContainerControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Client_Manager";
            this.Text = "Client Manager";
            this.Activated += new System.EventHandler(this.frm_Core_Client_Manager_Activated);
            this.Load += new System.EventHandler(this.frm_Core_Client_Manager_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.splitContainerControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp03001EPClientManagerListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_EP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditTimeMask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageLinkedDocuments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer2)).EndInit();
            this.gridSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00220LinkedDocumentsListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer4)).EndInit();
            this.gridSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05091GCContactPeopleLinkedToClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer3)).EndInit();
            this.gridSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04009GCContactsLinkedToClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer5)).EndInit();
            this.gridSplitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04095GCPOsLinkedToClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer6)).EndInit();
            this.gridSplitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04098GCSitesLinkedToClientPOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer7)).EndInit();
            this.gridSplitContainer7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04246GCSentReportsLinkedToClientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_GC_Reports)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEditLinkedReportFile)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer8)).EndInit();
            this.gridSplitContainer8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04247GCClientInvoiceGroupsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer9)).EndInit();
            this.gridSplitContainer9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04248GCClientInvoiceGroupMembersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer11)).EndInit();
            this.gridSplitContainer11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp04368GCClientInvoiceGroupClientContactsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit7)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditToDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer10)).EndInit();
            this.gridSplitContainer10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp05080CRMContactListBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmInvoicing)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_EP dataSet_EP;
        private System.Windows.Forms.BindingSource sp03001EPClientManagerListBindingSource;
        private WoodPlan5.DataSet_EPTableAdapters.sp03001_EP_Client_Manager_ListTableAdapter sp03001_EP_Client_Manager_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedSiteCount;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit1;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLinkedDocuments;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private System.Windows.Forms.BindingSource sp00220LinkedDocumentsListBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private WoodPlan5.DataSet_ATTableAdapters.sp00220_Linked_Documents_ListTableAdapter sp00220_Linked_Documents_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDailyGrittingReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientOrder;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource sp04009GCContactsLinkedToClientBindingSource;
        private DataSet_GC_Core dataSet_GC_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDetails;
        private DevExpress.XtraGrid.Columns.GridColumn colContactTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncludeForGrittingEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colContactTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode1;
        private DataSet_GC_CoreTableAdapters.sp04009_GC_Contacts_Linked_To_ClientTableAdapter sp04009_GC_Contacts_Linked_To_ClientTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDailyGrittingEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colDailyGrittingEmailShowUnGritted;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private System.Windows.Forms.BindingSource sp04095GCPOsLinkedToClientBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colPONumber;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateTimeReceived;
        private DevExpress.XtraGrid.Columns.GridColumn colGivenByPerson;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalAmount;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountSpent;
        private DevExpress.XtraGrid.Columns.GridColumn colAmountRemaining;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks2;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode2;
        private DataSet_GC_CoreTableAdapters.sp04095_GC_POs_Linked_To_ClientTableAdapter sp04095_GC_POs_Linked_To_ClientTableAdapter;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private System.Windows.Forms.BindingSource sp04098GCSitesLinkedToClientPOBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOSiteLinkID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientPOID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingAllowed;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceAllowed;
        private DevExpress.XtraGrid.Columns.GridColumn colPONumber1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName3;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode3;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DataSet_GC_CoreTableAdapters.sp04098_GC_Sites_Linked_To_Client_POTableAdapter sp04098_GC_Sites_Linked_To_Client_POTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingClientPONumberRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClientPONumberRequired;
        private DevExpress.XtraGrid.Columns.GridColumn colWinterMaintLastInvoiceDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWinterMaintInvoiceFrequency;
        private DevExpress.XtraGrid.Columns.GridColumn colWinterMaintInvoiceFrequencyDescriptor;
        private DevExpress.XtraGrid.Columns.GridColumn colWinterMaintInvoiceIgnoreSSColumns;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingEveningStartTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditTimeMask;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingEveningEndTime;
        private DevExpress.XtraGrid.Columns.GridColumn colWinterMaintInvoiceFrequencyDescriptorID;
        private DevExpress.XtraGrid.Columns.GridColumn colIncludeForReports;
        private DevExpress.XtraGrid.Columns.GridColumn colEmailPassword;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private System.Windows.Forms.BindingSource sp04246GCSentReportsLinkedToClientBindingSource;
        private DataSet_GC_Reports dataSet_GC_Reports;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colSentReportID;
        private DevExpress.XtraGrid.Columns.GridColumn colSentReportTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDateSent;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedReportFilename;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEditLinkedReportFile;
        private DevExpress.XtraGrid.Columns.GridColumn colReportTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName4;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode4;
        private DataSet_GC_ReportsTableAdapters.sp04246_GC_Sent_Reports_Linked_To_ClientTableAdapter sp04246_GC_Sent_Reports_Linked_To_ClientTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSentByStaffName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.GridControl gridControl8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private System.Windows.Forms.BindingSource sp04247GCClientInvoiceGroupsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceGroupID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID4;
        private DevExpress.XtraGrid.Columns.GridColumn colActive1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colIncludeGritting;
        private DevExpress.XtraGrid.Columns.GridColumn colIncludeSnowClearance;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName5;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode5;
        private System.Windows.Forms.BindingSource sp04248GCClientInvoiceGroupMembersBindingSource;
        private DataSet_GC_CoreTableAdapters.sp04247_GC_Client_Invoice_GroupsTableAdapter sp04247_GC_Client_Invoice_GroupsTableAdapter;
        private DataSet_GC_CoreTableAdapters.sp04248_GC_Client_Invoice_Group_MembersTableAdapter sp04248_GC_Client_Invoice_Group_MembersTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceGroupMember;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceGroupID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID5;
        private DevExpress.XtraGrid.Columns.GridColumn colActive2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colIncludeGritting1;
        private DevExpress.XtraGrid.Columns.GridColumn colIncludeSnowClearance1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName6;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode6;
        private DevExpress.XtraBars.BarButtonItem bbiAddOutstandingSitesToInvoiceGroup;
        private DevExpress.XtraBars.PopupMenu pmInvoicing;
        private DevExpress.XtraGrid.Columns.GridColumn colGrittingDefault;
        private DevExpress.XtraGrid.Columns.GridColumn colSnowClearanceDefault;
        private DevExpress.XtraGrid.Columns.GridColumn colIsActiveClient;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerRelationshipDepth;
        private DevExpress.XtraGrid.Columns.GridColumn colCustomerRelationshipDepthID;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraEditors.DateEdit dateEditFromDate;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.DateEdit dateEditToDate;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnReloadCRM;
        private DevExpress.XtraGrid.GridControl gridControl9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn colCRMID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDueDateTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMadeDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethodID;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordDescription1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit3;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colContactMethod;
        private DevExpress.XtraGrid.Columns.GridColumn colContactType;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDirectionDescription;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecordTypeDescription;
        private System.Windows.Forms.BindingSource sp05080CRMContactListBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlanDataSetTableAdapters.sp05080_CRM_Contact_ListTableAdapter sp05080_CRM_Contact_ListTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colDateCreated;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraGrid.GridControl gridControl10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private System.Windows.Forms.BindingSource sp05091GCContactPeopleLinkedToClientBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID6;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks5;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName7;
        private DevExpress.XtraGrid.Columns.GridColumn colClientCode7;
        private DataSet_EPTableAdapters.sp05091_GC_Contact_People_Linked_To_ClientTableAdapter sp05091_GC_Contact_People_Linked_To_ClientTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContact;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer2;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer3;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer4;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer6;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer7;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer8;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer10;
        private DevExpress.XtraGrid.Columns.GridColumn colWebSiteUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colWebSitePassword;
        private DevExpress.XtraGrid.Columns.GridColumn colIsUtilityArbClient;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlanWebActionDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlanWebFileManagerAdmin;
        private DevExpress.XtraGrid.Columns.GridColumn colWoodPlanWebWorkOrderDoneDate;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArbPermissionDocumentType;
        private DevExpress.XtraGrid.Columns.GridColumn colUtilityArbPermissionDocumentTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colImagesFolderOM;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartmentCode;
        private DevExpress.XtraGrid.Columns.GridColumn colFinanceCode;
        private DevExpress.XtraGrid.Columns.GridColumn colReceivesExternalGrittingEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colCareOfAddressLine1;
        private DevExpress.XtraGrid.Columns.GridColumn colCareOfAddressLine2;
        private DevExpress.XtraGrid.Columns.GridColumn colCareOfAddressLine3;
        private DevExpress.XtraGrid.Columns.GridColumn colCareOfAddressLine4;
        private DevExpress.XtraGrid.Columns.GridColumn colCareOfAddressLine5;
        private DevExpress.XtraGrid.Columns.GridColumn colCareOfPostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colCareOfEmail;
        private DevExpress.XtraGrid.Columns.GridColumn colAcceptsEmailInvoice;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer9;
        private DevExpress.XtraGrid.GridControl gridControl11;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private System.Windows.Forms.BindingSource sp04368GCClientInvoiceGroupClientContactsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colInvoiceGroupClientContactID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactID1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactDetails1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactType1;
        private DevExpress.XtraGrid.Columns.GridColumn colContactTypeID1;
        private DataSet_GC_CoreTableAdapters.sp04368_GC_Client_Invoice_Group_Client_ContactsTableAdapter sp04368_GC_Client_Invoice_Group_Client_ContactsTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer11;
        private DevExpress.XtraGrid.Columns.GridColumn colIsAmenityArbClient;
        private DevExpress.XtraGrid.Columns.GridColumn colIsSummerMaintenanceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colIsWinterMaintenanceClient;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTest;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactPersonID1;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonName1;
    }
}

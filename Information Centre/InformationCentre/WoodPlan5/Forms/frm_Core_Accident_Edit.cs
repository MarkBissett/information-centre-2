using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_Accident_Edit : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int intLinkedToRecordID = 0;
        public int intLinkedToRecordTypeID = 0;
        public string strLinkedToRecordDesc = "";

        public string strImagesFolderOM = "";

        #endregion

        public frm_Core_Accident_Edit()
        {
            InitializeComponent();
        }
        
        private void frm_Core_Accident_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 500207;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

           /* try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strSignaturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Client Signatures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Client Signature Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            } */

            try
            {
                sp_HR_00099_Business_Areas_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp_HR_00099_Business_Areas_With_BlankTableAdapter.Fill(dataSet_HR_Core.sp_HR_00099_Business_Areas_With_Blank);

                sp00305_Accident_Statuses_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp00305_Accident_Statuses_With_BlankTableAdapter.Fill(dataSet_Accident.sp00305_Accident_Statuses_With_Blank, 1);

                sp00306_Accident_Linked_Record_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp00306_Accident_Linked_Record_Types_With_BlankTableAdapter.Fill(dataSet_Accident.sp00306_Accident_Linked_Record_Types_With_Blank, 1);
            }
            catch (Exception) { }

            // Populate Main Dataset //         
            sp00303_Accident_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();

            switch (strFormMode)
            {
                case "add":
                case "blockadd":
                    try
                    {
                        DataRow drNewRow = dataSet_Accident.sp00303_Accident_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["AccidentID"] = 0;
                        drNewRow["LinkedToRecordID"] = intLinkedToRecordID;
                        drNewRow["LinkedToRecordFullDescription"] = intLinkedToRecordID;
                        drNewRow["LinkedToRecordTypeID"] = intLinkedToRecordTypeID;
                        drNewRow["BusinessAreaID"] = 0;
                        drNewRow["AccidentDate"] = DateTime.Now;
                        drNewRow["AccidentTypeID"] = 0;
                        drNewRow["AccidentType"] = "Accident";
                        drNewRow["AccidentSubTypeID"] = 0;
                        drNewRow["AccidentSubType"] = "";
                        drNewRow["AccidentStatusID"] = 1;
                        drNewRow["ReportedByPersonID"] = 0;
                        drNewRow["ReportedByPersonTypeID"] = 0;
                        drNewRow["ReportedByPersonOther"] = "";
                        drNewRow["ReportedByPerson"] = "";
                        drNewRow["ReportedToHSE"] = 0;
                        drNewRow["ResponsibleManagerID"] = 0;
                        drNewRow["HSQETeamLead"] = 0;
                        drNewRow["ClosedByStaffID"] = 0;
                        drNewRow["ReportedByPersonTypeIDParent"] = 0;
                        //drNewRow["LinkedToParent"] = (strFormMode == "add" ? strLinkedToRecordDesc : "N\\A when Block Adding");
                        dataSet_Accident.sp00303_Accident_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "blockedit":
                    try
                    {
                        //DataRow drNewRow;
                        DataRow drNewRow = dataSet_Accident.sp00303_Accident_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        dataSet_Accident.sp00303_Accident_Edit.Rows.Add(drNewRow);
                        drNewRow.AcceptChanges();
                    }
                    catch (Exception ex)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp00303_Accident_EditTableAdapter.Fill(dataSet_Accident.sp00303_Accident_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);

                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }
       
        private void frm_Core_Accident_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Accident_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }

 
        private void Attach_EditValueChanged_To_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_Accident.sp00303_Accident_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Accident", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        BusinessAreaIDGridLookUpEdit.Focus();

                        //AccidentSubTypeButtonEdit.Properties.ReadOnly = false;
                        //AccidentSubTypeButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        BusinessAreaIDGridLookUpEdit.Focus();

                        //AccidentSubTypeButtonEdit.Properties.ReadOnly = true;
                        //AccidentSubTypeButtonEdit.Properties.Buttons[0].Enabled = false;

                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        BusinessAreaIDGridLookUpEdit.Focus();

                        //AccidentSubTypeButtonEdit.Properties.ReadOnly = false;
                        //AccidentSubTypeButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        BusinessAreaIDGridLookUpEdit.Focus();

                        //AccidentSubTypeButtonEdit.Properties.ReadOnly = true;
                        //AccidentSubTypeButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
            ibool_ignoreValidation = true;
            ibool_FormStillLoading = false;
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = dataSet_Accident.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp00303AccidentEditBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["AccidentID"] == null ? 0 : Convert.ToInt32(currentRow["AccidentID"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of button //
            
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            this.sp00303AccidentEditBindingSource.EndEdit();
            try
            {
                sp00303_Accident_EditTableAdapter.Update(dataSet_Accident);  // Insert and Update queries defined in Table Adapter //
            }
            catch (Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                var currentRowView = (DataRowView)sp00303AccidentEditBindingSource.Current;
                var currentRow = (DataSet_Accident.sp00303_Accident_EditRow)currentRowView.Row;
                this.strFormMode = "edit";  // Switch mode to Edit so than any subsequent changes update this record //
                if (currentRow != null)
                {
                    currentRow.strMode = "edit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = currentRow.AccidentID + ";";
                }
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                var currentRowView = (DataRowView)sp00303AccidentEditBindingSource.Current;
                var currentRow = (DataSet_Accident.sp00303_Accident_EditRow)currentRowView.Row;
                this.strFormMode = "blockedit";  // Switch mode to BlockEdit so than any subsequent changes update these record //
                if (currentRow != null)
                {
                    currentRow.strMode = "blockedit";
                    currentRow.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Values returned from Update SP //
                }
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_Core_Accident_Manager")
                    {
                        var fParentForm = (frm_Core_Accident_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Accident, strNewIDs);
                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < dataSet_Accident.sp00303_Accident_Edit.Rows.Count; i++)
            {
                switch (dataSet_Accident.sp00303_Accident_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n";
                if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void AccidentDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit dt = (DateEdit)sender;
            if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;
            if (string.IsNullOrEmpty(dt.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(AccidentDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AccidentDateDateEdit, "");
            }
        }

        private void BusinessAreaIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(BusinessAreaIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(BusinessAreaIDGridLookUpEdit, "");
            }
        }

        private void AccidentStatusIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if ((this.strFormMode == "add" || this.strFormMode == "edit") && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(AccidentStatusIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AccidentStatusIDGridLookUpEdit, "");
            }
        }

        private void ResponsibleManagerButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp00303AccidentEditBindingSource.Current;
                if (currentRow == null) return;

                int CurrentID = 0;
                try
                {
                    CurrentID = (string.IsNullOrEmpty(currentRow["ResponsibleManagerID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ResponsibleManagerID"]));
                }
                catch (Exception) { };

                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = CurrentID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (CurrentID != fChildForm.intSelectedStaffID)
                    {
                        currentRow["ResponsibleManagerID"] = fChildForm.intSelectedStaffID;
                        currentRow["ResponsibleManager"] = fChildForm.strSelectedStaffName;
                        sp00303AccidentEditBindingSource.EndEdit();
                    }
                }
            }
        }

        private void HSQETeamLeadNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp00303AccidentEditBindingSource.Current;
                if (currentRow == null) return;

                int CurrentID = 0;
                try
                {
                    CurrentID = (string.IsNullOrEmpty(currentRow["HSQETeamLead"].ToString()) ? 0 : Convert.ToInt32(currentRow["HSQETeamLead"]));
                }
                catch (Exception) { };

                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = CurrentID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (CurrentID != fChildForm.intSelectedStaffID)
                    {
                        currentRow["HSQETeamLead"] = fChildForm.intSelectedStaffID;
                        currentRow["HSQETeamLeadName"] = fChildForm.strSelectedStaffName;
                        sp00303AccidentEditBindingSource.EndEdit();
                    }
                }
            }
        }

        private void ClosedByStaffButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp00303AccidentEditBindingSource.Current;
                if (currentRow == null) return;

                int CurrentID = 0;
                try
                {
                    CurrentID = (string.IsNullOrEmpty(currentRow["ClosedByStaffID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClosedByStaffID"]));
                }
                catch (Exception) { };

                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = CurrentID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (CurrentID != fChildForm.intSelectedStaffID)
                    {
                        currentRow["ClosedByStaffID"] = fChildForm.intSelectedStaffID;
                        currentRow["ClosedByStaff"] = fChildForm.strSelectedStaffName;
                        sp00303AccidentEditBindingSource.EndEdit();
                    }
                }
            }
        }

        private void LinkedToRecordTypeIDGridLookUpEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (strFormMode == "view") return;
            ibool_ignoreValidation = false;  // Toggles validation on and off //         
        }
        private void LinkedToRecordTypeIDGridLookUpEdit_Validated(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;
            if (ibool_ignoreValidation || ibool_FormStillLoading) return;
            ibool_ignoreValidation = true;

            DataRowView currentRow = (DataRowView)sp00303AccidentEditBindingSource.Current;
            if (currentRow == null) return;
            currentRow["LinkedToRecordID"] = 0;
            currentRow["LinkedToRecordFullDescription"] = "";
            currentRow["ClientName"] = "";
            currentRow["SiteName"] = "";
            sp00303AccidentEditBindingSource.EndEdit();
        }

        private void AccidentSubTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp00303AccidentEditBindingSource.Current;
                var currentRow = (DataSet_Accident.sp00303_Accident_EditRow)currentRowView.Row;
                if (currentRow == null) return;

                int intTypeID = 0;
                try
                {
                    intTypeID = (string.IsNullOrEmpty(currentRow["AccidentTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["AccidentTypeID"]));
                }
                catch (Exception) { };

                int intSubTypeID = 0;
                try
                {
                    intSubTypeID = (string.IsNullOrEmpty(currentRow["AccidentSubTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["AccidentSubTypeID"]));
                }
                catch (Exception) { };

                var fChildForm = new frm_Core_Accident_Type_Select();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intPassedInTypeID = intTypeID;
                fChildForm.intPassedInSubTypeID = intSubTypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.AccidentTypeID = fChildForm.intSelectedTypeID;
                    currentRow.AccidentSubTypeID = fChildForm.intSelectedSubTypeID;
                    currentRow.AccidentType = fChildForm.strSelectedTypeName;
                    currentRow.AccidentSubType = fChildForm.strSelectedSubTypeName;
                    sp00303AccidentEditBindingSource.EndEdit();
                }
            }
        }
        private void AccidentSubTypeButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && this.strFormMode != "view" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(AccidentSubTypeButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(AccidentSubTypeButtonEdit, "");
            }
        }

        private void ReportedByPersonButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var currentRowView = (DataRowView)sp00303AccidentEditBindingSource.Current;
                var currentRow = (DataSet_Accident.sp00303_Accident_EditRow)currentRowView.Row;
                if (currentRow == null) return;
                int intReportedByTypeID = 0;
                try
                {
                    intReportedByTypeID = (string.IsNullOrEmpty(currentRow.ReportedByPersonTypeID.ToString()) ? 0 : Convert.ToInt32(currentRow.ReportedByPersonTypeID));
                }
                catch (Exception) { };

                int intReportedByPersonID = 0;
                try
                {
                    intReportedByPersonID = (string.IsNullOrEmpty(currentRow.ReportedByPersonID.ToString()) ? 0 : Convert.ToInt32(currentRow.ReportedByPersonID));
                }
                catch (Exception) { };

                int intReportedByPersonTypeIDParent = 0;
                try
                {
                    intReportedByPersonTypeIDParent = (string.IsNullOrEmpty(currentRow.ReportedByPersonTypeIDParent.ToString()) ? 0 : Convert.ToInt32(currentRow.ReportedByPersonTypeIDParent));
                }
                catch (Exception) { };

                string strOriginalName = "";
                try
                {
                    strOriginalName = (string.IsNullOrEmpty(currentRow.ReportedByPerson.ToString()) ? "" : currentRow.ReportedByPerson.ToString());
                }
                catch (Exception) { };

                var fChildForm = new frm_Core_Accident_Reported_By_Select();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalTypeID = intReportedByTypeID;
                fChildForm.intOriginalID = intReportedByPersonID;
                fChildForm.intOriginalParentID = intReportedByPersonTypeIDParent;
                fChildForm.strOriginalName = strOriginalName;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow.ReportedByPersonTypeID = fChildForm.intSelectedTypeID;
                    currentRow.ReportedByPersonType = fChildForm.strSelectedType;
                    currentRow.ReportedByPersonID = fChildForm.intSelectedID;
                    currentRow.ReportedByPerson = fChildForm.strSelectedName;
                    currentRow.ReportedByPersonTypeIDParent = fChildForm.intSelectedParentID;
                    if (fChildForm.intSelectedTypeID == 4) currentRow.ReportedByPersonOther = fChildForm.strSelectedName;
                    sp00303AccidentEditBindingSource.EndEdit();
                }
            }
        }

        private void LinkedToRecordFullDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            DataRowView currentRow = (DataRowView)sp00303AccidentEditBindingSource.Current;
            if (currentRow == null) return;

            int RecordTypeID = 0;
            try
            {
                RecordTypeID = (string.IsNullOrEmpty(currentRow["LinkedToRecordTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordTypeID"]));
            }
            catch (Exception) { };

            if (RecordTypeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select the Linked To Record Type before proceeding.", "Linked To Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            int intOriginalID = (string.IsNullOrEmpty(currentRow["LinkedToRecordID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToRecordID"]));
            switch (RecordTypeID)
            {
                case 1:  // Visit // 
                    {
                        var fChildForm = new frm_OM_Select_Visit();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalClientContractID = 0;
                        fChildForm.intOriginalSiteContractID = 0;
                        fChildForm.intOriginalVisitID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedVisitID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedVisitID;
                            currentRow["LinkedToRecordFullDescription"] = fChildForm.strSelectedChildDescription;
                            currentRow["ClientName"] = fChildForm.strSelectedClientName;
                            currentRow["SiteName"] = fChildForm.strSelectedSiteName;
                            sp00303AccidentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                case 2:  // Job // 
                    {
                        var fChildForm = new frm_OM_Select_Job();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalJobID = intOriginalID;
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (intOriginalID == fChildForm.intSelectedJobID) return;
                            currentRow["LinkedToRecordID"] = fChildForm.intSelectedJobID;
                            currentRow["LinkedToRecordFullDescription"] = "Client: " + fChildForm.strSelectedClientName + ", Site: " + fChildForm.strSelectedSiteName + ", Visit #: " + fChildForm.intSelectedVisitNumber.ToString() + ", Job: " + fChildForm.strSelectedJobTypeDescription + " - " + fChildForm.strSelectedJobSubTypeDescription;
                            currentRow["ClientName"] = fChildForm.strSelectedClientName;
                            currentRow["SiteName"] = fChildForm.strSelectedSiteName;
                            sp00303AccidentEditBindingSource.EndEdit();
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp00303AccidentEditBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["AccidentID"].ToString()) ? 0 : Convert.ToInt32(currentRow["AccidentID"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 61;  // Accident //
            int intRecordSubType = 0;

            string strDate = "Unknown Date";
            try { strDate = currentRow["AccidentDate"].ToString(); }
            catch (Exception) { }

            string strRecordDescription = currentRow["AccidentType"].ToString() + " - " + strDate;
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);

        }



 






    }
}


namespace WoodPlan5
{
    partial class frm_Core_Accident_Injury_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Accident_Injury_Edit));
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling10 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling11 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling12 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling13 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.LinkedToPersonParentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp00310AccidentInjuryEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Accident = new WoodPlan5.DataSet_Accident();
            this.LinkedToPersonTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentDateTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NatureOfInjuryIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00313AccidentInjuryTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AccidentBodyAreaIDGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.sp00312AccidentBodyAreasWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LinkedToPersonOtherTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToLinkedToPersonTypeID = new DevExpress.XtraEditors.TextEdit();
            this.VisitNumberTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.SiteNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ClientNameContractDescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.AccidentIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToPersonButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.AccidentInjuryIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.LinkedToPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.LinkedToAccidentButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ItemForAccidentInjuryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonOther = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccidentDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonParentID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToAccident = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layGrpAddress = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForAccidentBodyAreaID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNatureOfInjuryID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPerson = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClientNameContractDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSiteName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForVisitNumber = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp00310_Accident_Injury_EditTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00310_Accident_Injury_EditTableAdapter();
            this.sp00312_Accident_Body_Areas_With_BlankTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00312_Accident_Body_Areas_With_BlankTableAdapter();
            this.sp00313_Accident_Injury_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_AccidentTableAdapters.sp00313_Accident_Injury_Types_With_BlankTableAdapter();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonParentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00310AccidentInjuryEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDateTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NatureOfInjuryIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00313AccidentInjuryTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentBodyAreaIDGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00312AccidentBodyAreasWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonOtherTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToLinkedToPersonTypeID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentInjuryIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToAccidentButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentInjuryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonOther)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonParentID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToAccident)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentBodyAreaID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNatureOfInjuryID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPerson)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 616);
            this.barDockControlBottom.Size = new System.Drawing.Size(686, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 590);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(686, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 590);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "ID";
            this.gridColumn1.FieldName = "ID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            this.gridColumn1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn1.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments});
            this.barManager2.MaxItemId = 16;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = global::WoodPlan5.Properties.Resources.SaveAndClose_16x16;
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip4.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem4.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem4.Text = "Form Mode - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.barStaticItemFormMode.SuperTip = superToolTip4;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(686, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 616);
            this.barDockControl2.Size = new System.Drawing.Size(686, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 590);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(686, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 590);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonParentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentDateTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NatureOfInjuryIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentBodyAreaIDGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonOtherTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToLinkedToPersonTypeID);
            this.dataLayoutControl1.Controls.Add(this.VisitNumberTextEdit);
            this.dataLayoutControl1.Controls.Add(this.SiteNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ClientNameContractDescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.AccidentInjuryIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.LinkedToAccidentButtonEdit);
            this.dataLayoutControl1.DataSource = this.sp00310AccidentInjuryEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForAccidentInjuryID,
            this.ItemForAccidentID,
            this.ItemForLinkedToPersonID,
            this.ItemForLinkedToPersonTypeID,
            this.ItemForLinkedToPersonOther,
            this.ItemForAccidentType,
            this.ItemForAccidentDate,
            this.ItemForLinkedToPersonParentID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1234, 162, 301, 422);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(686, 590);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // LinkedToPersonParentIDTextEdit
            // 
            this.LinkedToPersonParentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "LinkedToPersonParentID", true));
            this.LinkedToPersonParentIDTextEdit.Location = new System.Drawing.Point(149, 375);
            this.LinkedToPersonParentIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonParentIDTextEdit.Name = "LinkedToPersonParentIDTextEdit";
            this.LinkedToPersonParentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonParentIDTextEdit, true);
            this.LinkedToPersonParentIDTextEdit.Size = new System.Drawing.Size(525, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonParentIDTextEdit, optionsSpelling1);
            this.LinkedToPersonParentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonParentIDTextEdit.TabIndex = 75;
            // 
            // sp00310AccidentInjuryEditBindingSource
            // 
            this.sp00310AccidentInjuryEditBindingSource.DataMember = "sp00310_Accident_Injury_Edit";
            this.sp00310AccidentInjuryEditBindingSource.DataSource = this.dataSet_Accident;
            // 
            // dataSet_Accident
            // 
            this.dataSet_Accident.DataSetName = "DataSet_Accident";
            this.dataSet_Accident.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LinkedToPersonTypeTextEdit
            // 
            this.LinkedToPersonTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "LinkedToPersonType", true));
            this.LinkedToPersonTypeTextEdit.Location = new System.Drawing.Point(141, 212);
            this.LinkedToPersonTypeTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeTextEdit.Name = "LinkedToPersonTypeTextEdit";
            this.LinkedToPersonTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonTypeTextEdit, true);
            this.LinkedToPersonTypeTextEdit.Size = new System.Drawing.Size(509, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonTypeTextEdit, optionsSpelling2);
            this.LinkedToPersonTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonTypeTextEdit.TabIndex = 74;
            // 
            // AccidentDateTextEdit
            // 
            this.AccidentDateTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "AccidentDate", true));
            this.AccidentDateTextEdit.Location = new System.Drawing.Point(141, 341);
            this.AccidentDateTextEdit.MenuManager = this.barManager1;
            this.AccidentDateTextEdit.Name = "AccidentDateTextEdit";
            this.AccidentDateTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentDateTextEdit, true);
            this.AccidentDateTextEdit.Size = new System.Drawing.Size(509, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentDateTextEdit, optionsSpelling3);
            this.AccidentDateTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentDateTextEdit.TabIndex = 73;
            // 
            // AccidentTypeTextEdit
            // 
            this.AccidentTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "AccidentType", true));
            this.AccidentTypeTextEdit.Location = new System.Drawing.Point(129, 353);
            this.AccidentTypeTextEdit.MenuManager = this.barManager1;
            this.AccidentTypeTextEdit.Name = "AccidentTypeTextEdit";
            this.AccidentTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentTypeTextEdit, true);
            this.AccidentTypeTextEdit.Size = new System.Drawing.Size(533, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentTypeTextEdit, optionsSpelling4);
            this.AccidentTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentTypeTextEdit.TabIndex = 72;
            // 
            // NatureOfInjuryIDGridLookUpEdit
            // 
            this.NatureOfInjuryIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "NatureOfInjuryID", true));
            this.NatureOfInjuryIDGridLookUpEdit.Location = new System.Drawing.Point(141, 294);
            this.NatureOfInjuryIDGridLookUpEdit.MenuManager = this.barManager1;
            this.NatureOfInjuryIDGridLookUpEdit.Name = "NatureOfInjuryIDGridLookUpEdit";
            this.NatureOfInjuryIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NatureOfInjuryIDGridLookUpEdit.Properties.DataSource = this.sp00313AccidentInjuryTypesWithBlankBindingSource;
            this.NatureOfInjuryIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.NatureOfInjuryIDGridLookUpEdit.Properties.NullText = "";
            this.NatureOfInjuryIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.NatureOfInjuryIDGridLookUpEdit.Properties.View = this.gridView1;
            this.NatureOfInjuryIDGridLookUpEdit.Size = new System.Drawing.Size(509, 20);
            this.NatureOfInjuryIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.NatureOfInjuryIDGridLookUpEdit.TabIndex = 14;
            // 
            // sp00313AccidentInjuryTypesWithBlankBindingSource
            // 
            this.sp00313AccidentInjuryTypesWithBlankBindingSource.DataMember = "sp00313_Accident_Injury_Types_With_Blank";
            this.sp00313AccidentInjuryTypesWithBlankBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition1.Appearance.Options.UseForeColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.gridColumn4;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition1.Value1 = 0;
            this.gridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Injury Type";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // AccidentBodyAreaIDGridLookUpEdit
            // 
            this.AccidentBodyAreaIDGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "AccidentBodyAreaID", true));
            this.AccidentBodyAreaIDGridLookUpEdit.Location = new System.Drawing.Point(141, 270);
            this.AccidentBodyAreaIDGridLookUpEdit.MenuManager = this.barManager1;
            this.AccidentBodyAreaIDGridLookUpEdit.Name = "AccidentBodyAreaIDGridLookUpEdit";
            this.AccidentBodyAreaIDGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AccidentBodyAreaIDGridLookUpEdit.Properties.DataSource = this.sp00312AccidentBodyAreasWithBlankBindingSource;
            this.AccidentBodyAreaIDGridLookUpEdit.Properties.DisplayMember = "Description";
            this.AccidentBodyAreaIDGridLookUpEdit.Properties.NullText = "";
            this.AccidentBodyAreaIDGridLookUpEdit.Properties.ValueMember = "ID";
            this.AccidentBodyAreaIDGridLookUpEdit.Properties.View = this.gridView3;
            this.AccidentBodyAreaIDGridLookUpEdit.Size = new System.Drawing.Size(509, 20);
            this.AccidentBodyAreaIDGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AccidentBodyAreaIDGridLookUpEdit.TabIndex = 13;
            // 
            // sp00312AccidentBodyAreasWithBlankBindingSource
            // 
            this.sp00312AccidentBodyAreasWithBlankBindingSource.DataMember = "sp00312_Accident_Body_Areas_With_Blank";
            this.sp00312AccidentBodyAreasWithBlankBindingSource.DataSource = this.dataSet_Accident;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition2.Appearance.ForeColor = System.Drawing.Color.Red;
            styleFormatCondition2.Appearance.Options.UseForeColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.gridColumn1;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
            styleFormatCondition2.Value1 = 0;
            this.gridView3.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition2});
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn3, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Body Area";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.AllowFocus = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 220;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Order";
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // LinkedToPersonOtherTextEdit
            // 
            this.LinkedToPersonOtherTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "LinkedToPersonOther", true));
            this.LinkedToPersonOtherTextEdit.Location = new System.Drawing.Point(143, 353);
            this.LinkedToPersonOtherTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonOtherTextEdit.Name = "LinkedToPersonOtherTextEdit";
            this.LinkedToPersonOtherTextEdit.Properties.MaxLength = 100;
            this.LinkedToPersonOtherTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonOtherTextEdit, true);
            this.LinkedToPersonOtherTextEdit.Size = new System.Drawing.Size(519, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonOtherTextEdit, optionsSpelling5);
            this.LinkedToPersonOtherTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonOtherTextEdit.TabIndex = 71;
            // 
            // LinkedToLinkedToPersonTypeID
            // 
            this.LinkedToLinkedToPersonTypeID.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "LinkedToPersonTypeID", true));
            this.LinkedToLinkedToPersonTypeID.Location = new System.Drawing.Point(153, 353);
            this.LinkedToLinkedToPersonTypeID.MenuManager = this.barManager1;
            this.LinkedToLinkedToPersonTypeID.Name = "LinkedToLinkedToPersonTypeID";
            this.LinkedToLinkedToPersonTypeID.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToLinkedToPersonTypeID, true);
            this.LinkedToLinkedToPersonTypeID.Size = new System.Drawing.Size(509, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToLinkedToPersonTypeID, optionsSpelling6);
            this.LinkedToLinkedToPersonTypeID.StyleController = this.dataLayoutControl1;
            this.LinkedToLinkedToPersonTypeID.TabIndex = 70;
            // 
            // VisitNumberTextEdit
            // 
            this.VisitNumberTextEdit.Location = new System.Drawing.Point(117, 83);
            this.VisitNumberTextEdit.MenuManager = this.barManager1;
            this.VisitNumberTextEdit.Name = "VisitNumberTextEdit";
            this.VisitNumberTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.VisitNumberTextEdit, true);
            this.VisitNumberTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.VisitNumberTextEdit, optionsSpelling7);
            this.VisitNumberTextEdit.StyleController = this.dataLayoutControl1;
            this.VisitNumberTextEdit.TabIndex = 56;
            this.VisitNumberTextEdit.TabStop = false;
            // 
            // SiteNameTextEdit
            // 
            this.SiteNameTextEdit.Location = new System.Drawing.Point(117, 59);
            this.SiteNameTextEdit.MenuManager = this.barManager1;
            this.SiteNameTextEdit.Name = "SiteNameTextEdit";
            this.SiteNameTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.SiteNameTextEdit, true);
            this.SiteNameTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.SiteNameTextEdit, optionsSpelling8);
            this.SiteNameTextEdit.StyleController = this.dataLayoutControl1;
            this.SiteNameTextEdit.TabIndex = 55;
            this.SiteNameTextEdit.TabStop = false;
            // 
            // ClientNameContractDescriptionTextEdit
            // 
            this.ClientNameContractDescriptionTextEdit.Location = new System.Drawing.Point(117, 35);
            this.ClientNameContractDescriptionTextEdit.MenuManager = this.barManager1;
            this.ClientNameContractDescriptionTextEdit.Name = "ClientNameContractDescriptionTextEdit";
            this.ClientNameContractDescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.ClientNameContractDescriptionTextEdit, true);
            this.ClientNameContractDescriptionTextEdit.Size = new System.Drawing.Size(557, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.ClientNameContractDescriptionTextEdit, optionsSpelling9);
            this.ClientNameContractDescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.ClientNameContractDescriptionTextEdit.TabIndex = 54;
            this.ClientNameContractDescriptionTextEdit.TabStop = false;
            // 
            // AccidentIDTextEdit
            // 
            this.AccidentIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "AccidentID", true));
            this.AccidentIDTextEdit.Location = new System.Drawing.Point(154, 534);
            this.AccidentIDTextEdit.MenuManager = this.barManager1;
            this.AccidentIDTextEdit.Name = "AccidentIDTextEdit";
            this.AccidentIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentIDTextEdit, true);
            this.AccidentIDTextEdit.Size = new System.Drawing.Size(491, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentIDTextEdit, optionsSpelling10);
            this.AccidentIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentIDTextEdit.TabIndex = 53;
            // 
            // LinkedToPersonButtonEdit
            // 
            this.LinkedToPersonButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "LinkedToPerson", true));
            this.LinkedToPersonButtonEdit.EditValue = "";
            this.LinkedToPersonButtonEdit.Location = new System.Drawing.Point(141, 236);
            this.LinkedToPersonButtonEdit.MenuManager = this.barManager1;
            this.LinkedToPersonButtonEdit.Name = "LinkedToPersonButtonEdit";
            this.LinkedToPersonButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Waste Type screen", "choose", null, true)});
            this.LinkedToPersonButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToPersonButtonEdit.Size = new System.Drawing.Size(509, 20);
            this.LinkedToPersonButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonButtonEdit.TabIndex = 13;
            this.LinkedToPersonButtonEdit.TabStop = false;
            this.LinkedToPersonButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToPersonButtonEdit_ButtonClick);
            this.LinkedToPersonButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedToPersonButtonEdit_Validating);
            // 
            // AccidentInjuryIDTextEdit
            // 
            this.AccidentInjuryIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "AccidentInjuryID", true));
            this.AccidentInjuryIDTextEdit.Location = new System.Drawing.Point(154, 534);
            this.AccidentInjuryIDTextEdit.MenuManager = this.barManager1;
            this.AccidentInjuryIDTextEdit.Name = "AccidentInjuryIDTextEdit";
            this.AccidentInjuryIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.AccidentInjuryIDTextEdit, true);
            this.AccidentInjuryIDTextEdit.Size = new System.Drawing.Size(491, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.AccidentInjuryIDTextEdit, optionsSpelling11);
            this.AccidentInjuryIDTextEdit.StyleController = this.dataLayoutControl1;
            this.AccidentInjuryIDTextEdit.TabIndex = 42;
            // 
            // LinkedToPersonIDTextEdit
            // 
            this.LinkedToPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "LinkedToPersonID", true));
            this.LinkedToPersonIDTextEdit.Location = new System.Drawing.Point(154, 329);
            this.LinkedToPersonIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonIDTextEdit.Name = "LinkedToPersonIDTextEdit";
            this.LinkedToPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonIDTextEdit, true);
            this.LinkedToPersonIDTextEdit.Size = new System.Drawing.Size(508, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonIDTextEdit, optionsSpelling12);
            this.LinkedToPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonIDTextEdit.TabIndex = 44;
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(36, 212);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(614, 237);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling13);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 35;
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp00310AccidentInjuryEditBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(117, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(179, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 24;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // LinkedToAccidentButtonEdit
            // 
            this.LinkedToAccidentButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp00310AccidentInjuryEditBindingSource, "LinkedToAccident", true));
            this.LinkedToAccidentButtonEdit.EditValue = "";
            this.LinkedToAccidentButtonEdit.Location = new System.Drawing.Point(117, 107);
            this.LinkedToAccidentButtonEdit.MenuManager = this.barManager1;
            this.LinkedToAccidentButtonEdit.Name = "LinkedToAccidentButtonEdit";
            this.LinkedToAccidentButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to open the Select Job screen", "choose", null, true)});
            this.LinkedToAccidentButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.LinkedToAccidentButtonEdit.Size = new System.Drawing.Size(557, 20);
            this.LinkedToAccidentButtonEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToAccidentButtonEdit.TabIndex = 6;
            this.LinkedToAccidentButtonEdit.TabStop = false;
            this.LinkedToAccidentButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.LinkedToAccidentButtonEdit_ButtonClick);
            this.LinkedToAccidentButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.LinkedToAccidentButtonEdit_Validating);
            // 
            // ItemForAccidentInjuryID
            // 
            this.ItemForAccidentInjuryID.Control = this.AccidentInjuryIDTextEdit;
            this.ItemForAccidentInjuryID.CustomizationFormText = "Accident Injury ID";
            this.ItemForAccidentInjuryID.Location = new System.Drawing.Point(0, 418);
            this.ItemForAccidentInjuryID.Name = "ItemForAccidentInjuryID";
            this.ItemForAccidentInjuryID.Size = new System.Drawing.Size(625, 24);
            this.ItemForAccidentInjuryID.Text = "Accident Injury ID:";
            this.ItemForAccidentInjuryID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentID
            // 
            this.ItemForAccidentID.Control = this.AccidentIDTextEdit;
            this.ItemForAccidentID.CustomizationFormText = "Accident ID:";
            this.ItemForAccidentID.Location = new System.Drawing.Point(0, 418);
            this.ItemForAccidentID.Name = "ItemForAccidentID";
            this.ItemForAccidentID.Size = new System.Drawing.Size(625, 24);
            this.ItemForAccidentID.Text = "Accident ID:";
            this.ItemForAccidentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPersonID
            // 
            this.ItemForLinkedToPersonID.Control = this.LinkedToPersonIDTextEdit;
            this.ItemForLinkedToPersonID.CustomizationFormText = "Linked To Person ID:";
            this.ItemForLinkedToPersonID.Location = new System.Drawing.Point(0, 130);
            this.ItemForLinkedToPersonID.Name = "ItemForLinkedToPersonID";
            this.ItemForLinkedToPersonID.Size = new System.Drawing.Size(642, 24);
            this.ItemForLinkedToPersonID.Text = "Linked To Person ID:";
            this.ItemForLinkedToPersonID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPersonTypeID
            // 
            this.ItemForLinkedToPersonTypeID.Control = this.LinkedToLinkedToPersonTypeID;
            this.ItemForLinkedToPersonTypeID.CustomizationFormText = "Linked To Person Type ID:";
            this.ItemForLinkedToPersonTypeID.Location = new System.Drawing.Point(0, 154);
            this.ItemForLinkedToPersonTypeID.Name = "ItemForLinkedToPersonTypeID";
            this.ItemForLinkedToPersonTypeID.Size = new System.Drawing.Size(642, 24);
            this.ItemForLinkedToPersonTypeID.Text = "Linked To Person Type ID:";
            this.ItemForLinkedToPersonTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPersonOther
            // 
            this.ItemForLinkedToPersonOther.Control = this.LinkedToPersonOtherTextEdit;
            this.ItemForLinkedToPersonOther.CustomizationFormText = "Linked To Person Other:";
            this.ItemForLinkedToPersonOther.Location = new System.Drawing.Point(0, 154);
            this.ItemForLinkedToPersonOther.Name = "ItemForLinkedToPersonOther";
            this.ItemForLinkedToPersonOther.Size = new System.Drawing.Size(642, 24);
            this.ItemForLinkedToPersonOther.Text = "Linked To Person Other:";
            this.ItemForLinkedToPersonOther.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentType
            // 
            this.ItemForAccidentType.Control = this.AccidentTypeTextEdit;
            this.ItemForAccidentType.CustomizationFormText = "Accident Type:";
            this.ItemForAccidentType.Location = new System.Drawing.Point(0, 154);
            this.ItemForAccidentType.Name = "ItemForAccidentType";
            this.ItemForAccidentType.Size = new System.Drawing.Size(642, 24);
            this.ItemForAccidentType.Text = "Accident Type:";
            this.ItemForAccidentType.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForAccidentDate
            // 
            this.ItemForAccidentDate.Control = this.AccidentDateTextEdit;
            this.ItemForAccidentDate.CustomizationFormText = "Accident Date:";
            this.ItemForAccidentDate.Location = new System.Drawing.Point(0, 106);
            this.ItemForAccidentDate.Name = "ItemForAccidentDate";
            this.ItemForAccidentDate.Size = new System.Drawing.Size(618, 24);
            this.ItemForAccidentDate.Text = "Accident Date:";
            this.ItemForAccidentDate.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPersonParentID
            // 
            this.ItemForLinkedToPersonParentID.Control = this.LinkedToPersonParentIDTextEdit;
            this.ItemForLinkedToPersonParentID.CustomizationFormText = "Linked To Person Parent ID:";
            this.ItemForLinkedToPersonParentID.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedToPersonParentID.Name = "ItemForLinkedToPersonParentID";
            this.ItemForLinkedToPersonParentID.Size = new System.Drawing.Size(666, 24);
            this.ItemForLinkedToPersonParentID.Text = "Linked To Person Parent ID:";
            this.ItemForLinkedToPersonParentID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(686, 590);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToAccident,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlGroup6,
            this.ItemForClientNameContractDescription,
            this.ItemForSiteName,
            this.ItemForVisitNumber,
            this.emptySpaceItem6});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(666, 465);
            // 
            // ItemForLinkedToAccident
            // 
            this.ItemForLinkedToAccident.AllowHide = false;
            this.ItemForLinkedToAccident.Control = this.LinkedToAccidentButtonEdit;
            this.ItemForLinkedToAccident.CustomizationFormText = "Linked To Accident:";
            this.ItemForLinkedToAccident.Location = new System.Drawing.Point(0, 95);
            this.ItemForLinkedToAccident.Name = "ItemForLinkedToAccident";
            this.ItemForLinkedToAccident.Size = new System.Drawing.Size(666, 24);
            this.ItemForLinkedToAccident.Text = "Linked To Accident:";
            this.ItemForLinkedToAccident.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(105, 0);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(105, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(105, 23);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(288, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(378, 23);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(105, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(183, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Details";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 130);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(666, 335);
            this.layoutControlGroup6.Text = "Details";
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layGrpAddress;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(642, 289);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layGrpAddress,
            this.layoutControlGroup4});
            // 
            // layGrpAddress
            // 
            this.layGrpAddress.CustomizationFormText = "Address";
            this.layGrpAddress.ExpandButtonVisible = true;
            this.layGrpAddress.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layGrpAddress.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem4,
            this.ItemForAccidentBodyAreaID,
            this.ItemForNatureOfInjuryID,
            this.ItemForLinkedToPerson,
            this.ItemForLinkedToPersonType,
            this.emptySpaceItem5});
            this.layGrpAddress.Location = new System.Drawing.Point(0, 0);
            this.layGrpAddress.Name = "layGrpAddress";
            this.layGrpAddress.Size = new System.Drawing.Size(618, 241);
            this.layGrpAddress.Text = "Details";
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 106);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(618, 135);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForAccidentBodyAreaID
            // 
            this.ItemForAccidentBodyAreaID.Control = this.AccidentBodyAreaIDGridLookUpEdit;
            this.ItemForAccidentBodyAreaID.CustomizationFormText = "Accident Body Area:";
            this.ItemForAccidentBodyAreaID.Location = new System.Drawing.Point(0, 58);
            this.ItemForAccidentBodyAreaID.Name = "ItemForAccidentBodyAreaID";
            this.ItemForAccidentBodyAreaID.Size = new System.Drawing.Size(618, 24);
            this.ItemForAccidentBodyAreaID.Text = "Accident Body Area:";
            this.ItemForAccidentBodyAreaID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForNatureOfInjuryID
            // 
            this.ItemForNatureOfInjuryID.Control = this.NatureOfInjuryIDGridLookUpEdit;
            this.ItemForNatureOfInjuryID.CustomizationFormText = "Nature Of Injury:";
            this.ItemForNatureOfInjuryID.Location = new System.Drawing.Point(0, 82);
            this.ItemForNatureOfInjuryID.Name = "ItemForNatureOfInjuryID";
            this.ItemForNatureOfInjuryID.Size = new System.Drawing.Size(618, 24);
            this.ItemForNatureOfInjuryID.Text = "Nature Of Injury:";
            this.ItemForNatureOfInjuryID.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForLinkedToPerson
            // 
            this.ItemForLinkedToPerson.Control = this.LinkedToPersonButtonEdit;
            this.ItemForLinkedToPerson.CustomizationFormText = "Person Injured:";
            this.ItemForLinkedToPerson.Location = new System.Drawing.Point(0, 24);
            this.ItemForLinkedToPerson.Name = "ItemForLinkedToPerson";
            this.ItemForLinkedToPerson.Size = new System.Drawing.Size(618, 24);
            this.ItemForLinkedToPerson.Text = "Person Injured:";
            this.ItemForLinkedToPerson.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForLinkedToPersonType
            // 
            this.ItemForLinkedToPersonType.Control = this.LinkedToPersonTypeTextEdit;
            this.ItemForLinkedToPersonType.CustomizationFormText = "Person Type Injured:";
            this.ItemForLinkedToPersonType.Location = new System.Drawing.Point(0, 0);
            this.ItemForLinkedToPersonType.Name = "ItemForLinkedToPersonType";
            this.ItemForLinkedToPersonType.Size = new System.Drawing.Size(618, 24);
            this.ItemForLinkedToPersonType.Text = "Person Type Injured:";
            this.ItemForLinkedToPersonType.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem5.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(618, 10);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CaptionImage = ((System.Drawing.Image)(resources.GetObject("layoutControlGroup4.CaptionImage")));
            this.layoutControlGroup4.CustomizationFormText = "Remarks";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(618, 241);
            this.layoutControlGroup4.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Comments:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(618, 241);
            this.ItemForRemarks.Text = "Comments:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // ItemForClientNameContractDescription
            // 
            this.ItemForClientNameContractDescription.Control = this.ClientNameContractDescriptionTextEdit;
            this.ItemForClientNameContractDescription.CustomizationFormText = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.Location = new System.Drawing.Point(0, 23);
            this.ItemForClientNameContractDescription.Name = "ItemForClientNameContractDescription";
            this.ItemForClientNameContractDescription.Size = new System.Drawing.Size(666, 24);
            this.ItemForClientNameContractDescription.Text = "Client \\ Contract:";
            this.ItemForClientNameContractDescription.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForSiteName
            // 
            this.ItemForSiteName.Control = this.SiteNameTextEdit;
            this.ItemForSiteName.CustomizationFormText = "Site Name:";
            this.ItemForSiteName.Location = new System.Drawing.Point(0, 47);
            this.ItemForSiteName.Name = "ItemForSiteName";
            this.ItemForSiteName.Size = new System.Drawing.Size(666, 24);
            this.ItemForSiteName.Text = "Site Name:";
            this.ItemForSiteName.TextSize = new System.Drawing.Size(102, 13);
            // 
            // ItemForVisitNumber
            // 
            this.ItemForVisitNumber.Control = this.VisitNumberTextEdit;
            this.ItemForVisitNumber.CustomizationFormText = "Visit Number:";
            this.ItemForVisitNumber.Location = new System.Drawing.Point(0, 71);
            this.ItemForVisitNumber.Name = "ItemForVisitNumber";
            this.ItemForVisitNumber.Size = new System.Drawing.Size(666, 24);
            this.ItemForVisitNumber.Text = "Visit Number:";
            this.ItemForVisitNumber.TextSize = new System.Drawing.Size(102, 13);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.CustomizationFormText = "emptySpaceItem6";
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 119);
            this.emptySpaceItem6.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem6.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(666, 11);
            this.emptySpaceItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem3});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 465);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(666, 105);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(666, 105);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp00310_Accident_Injury_EditTableAdapter
            // 
            this.sp00310_Accident_Injury_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp00312_Accident_Body_Areas_With_BlankTableAdapter
            // 
            this.sp00312_Accident_Body_Areas_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp00313_Accident_Injury_Types_With_BlankTableAdapter
            // 
            this.sp00313_Accident_Injury_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16");
            // 
            // frm_Core_Accident_Injury_Edit
            // 
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(686, 644);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_Core_Accident_Injury_Edit";
            this.Text = "Edit Accident Injury";
            this.Activated += new System.EventHandler(this.frm_Core_Accident_Injury_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_Core_Accident_Injury_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_Core_Accident_Injury_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonParentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00310AccidentInjuryEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Accident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentDateTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NatureOfInjuryIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00313AccidentInjuryTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentBodyAreaIDGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00312AccidentBodyAreasWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonOtherTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToLinkedToPersonTypeID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.VisitNumberTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SiteNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClientNameContractDescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccidentInjuryIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToAccidentButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentInjuryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonOther)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonParentID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToAccident)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layGrpAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccidentBodyAreaID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNatureOfInjuryID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPerson)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClientNameContractDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSiteName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForVisitNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToAccident;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.ButtonEdit LinkedToAccidentButtonEdit;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraLayout.LayoutControlGroup layGrpAddress;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonID;
        private DevExpress.XtraEditors.TextEdit AccidentInjuryIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentInjuryID;
        private DevExpress.XtraEditors.ButtonEdit LinkedToPersonButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPerson;
        private DevExpress.XtraEditors.TextEdit AccidentIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentID;
        private DevExpress.XtraEditors.TextEdit ClientNameContractDescriptionTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClientNameContractDescription;
        private DevExpress.XtraEditors.TextEdit SiteNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSiteName;
        private DevExpress.XtraEditors.TextEdit VisitNumberTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForVisitNumber;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private DevExpress.XtraEditors.TextEdit LinkedToLinkedToPersonTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonTypeID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private System.Windows.Forms.BindingSource sp00310AccidentInjuryEditBindingSource;
        private DataSet_Accident dataSet_Accident;
        private DataSet_AccidentTableAdapters.sp00310_Accident_Injury_EditTableAdapter sp00310_Accident_Injury_EditTableAdapter;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonOtherTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonOther;
        private DevExpress.XtraEditors.GridLookUpEdit AccidentBodyAreaIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentBodyAreaID;
        private DevExpress.XtraEditors.GridLookUpEdit NatureOfInjuryIDGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNatureOfInjuryID;
        private DevExpress.XtraEditors.TextEdit AccidentTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentType;
        private DevExpress.XtraEditors.TextEdit AccidentDateTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccidentDate;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonTypeTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonType;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private System.Windows.Forms.BindingSource sp00313AccidentInjuryTypesWithBlankBindingSource;
        private System.Windows.Forms.BindingSource sp00312AccidentBodyAreasWithBlankBindingSource;
        private DataSet_AccidentTableAdapters.sp00312_Accident_Body_Areas_With_BlankTableAdapter sp00312_Accident_Body_Areas_With_BlankTableAdapter;
        private DataSet_AccidentTableAdapters.sp00313_Accident_Injury_Types_With_BlankTableAdapter sp00313_Accident_Injury_Types_With_BlankTableAdapter;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonParentIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonParentID;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

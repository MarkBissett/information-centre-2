namespace WoodPlan5
{
    partial class frm_Core_Add_Client_Or_Site_Check_Exists
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Core_Add_Client_Or_Site_Check_Exists));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.sp01047CoreSearchforClientNameBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_Common_Functionality = new WoodPlan5.DataSet_Common_Functionality();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Job = new WoodPlan5.DataSet_OM_Job();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnAcceptClientSelection = new DevExpress.XtraEditors.SimpleButton();
            this.teClientCriteria = new DevExpress.XtraEditors.TextEdit();
            this.cbeClientMatchType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnFindClient = new DevExpress.XtraEditors.SimpleButton();
            this.sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersTableAdapter = new WoodPlan5.DataSet_OM_JobTableAdapters.sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersTableAdapter();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageClient = new DevExpress.XtraTab.XtraTabPage();
            this.btnCancelClient = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddClient = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPageSite = new DevExpress.XtraTab.XtraTabPage();
            this.labelClientFilter = new DevExpress.XtraEditors.LabelControl();
            this.memoExEditClientFilter = new DevExpress.XtraEditors.MemoExEdit();
            this.btnCancelSite = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddSite = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.btnAcceptSiteSelection = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp01048CoreSearchforSiteNameBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteAddress = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colSitePostcode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationX = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteLocationY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teSiteCriteria = new DevExpress.XtraEditors.TextEdit();
            this.cbeSiteMatchType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnFindSite = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPageClientContactPerson = new DevExpress.XtraTab.XtraTabPage();
            this.labelClientFilter2 = new DevExpress.XtraEditors.LabelControl();
            this.memoExEditClientFilter2 = new DevExpress.XtraEditors.MemoExEdit();
            this.btnCancelClientContact = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddClientContact = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.btnAcceptClientContactSelection = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp01051CoreSearchForClientContactPersonBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPersonName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactPersonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactTelephone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colClientContactMobile = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContactEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teClientContactCriteria = new DevExpress.XtraEditors.TextEdit();
            this.cbeClientContactMatchType = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnFindClientContact = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.sp01047_Core_Search_for_Client_NameTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01047_Core_Search_for_Client_NameTableAdapter();
            this.sp01048_Core_Search_for_Site_NameTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01048_Core_Search_for_Site_NameTableAdapter();
            this.sp01051_Core_Search_For_Client_Contact_PersonTableAdapter = new WoodPlan5.DataSet_Common_FunctionalityTableAdapters.sp01051_Core_Search_For_Client_Contact_PersonTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01047CoreSearchforClientNameBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teClientCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeClientMatchType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.xtraTabPageSite.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditClientFilter.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01048CoreSearchforSiteNameBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSiteCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeSiteMatchType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            this.xtraTabPageClientContactPerson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditClientFilter2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01051CoreSearchForClientContactPersonBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teClientContactCriteria.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeClientContactMatchType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(615, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 565);
            this.barDockControlBottom.Size = new System.Drawing.Size(615, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 539);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(615, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 539);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.DataSource = this.sp01047CoreSearchforClientNameBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.Location = new System.Drawing.Point(3, 23);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(540, 295);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // sp01047CoreSearchforClientNameBindingSource
            // 
            this.sp01047CoreSearchforClientNameBindingSource.DataMember = "sp01047_Core_Search_for_Client_Name";
            this.sp01047CoreSearchforClientNameBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // dataSet_Common_Functionality
            // 
            this.dataSet_Common_Functionality.DataSetName = "DataSet_Common_Functionality";
            this.dataSet_Common_Functionality.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colClientID,
            this.colClientName});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.FindDelay = 2000;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView1.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView1.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView1.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView1.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Visible = true;
            this.colClientName.VisibleIndex = 0;
            this.colClientName.Width = 406;
            // 
            // sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource
            // 
            this.sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource.DataMember = "sp06399_OM_Block_Add_Labour_On_Site_Select_Team_Members";
            this.sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource.DataSource = this.dataSet_OM_Job;
            // 
            // dataSet_OM_Job
            // 
            this.dataSet_OM_Job.DataSetName = "DataSet_OM_Job";
            this.dataSet_OM_Job.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.groupControl2);
            this.groupControl1.Controls.Add(this.teClientCriteria);
            this.groupControl1.Controls.Add(this.cbeClientMatchType);
            this.groupControl1.Controls.Add(this.btnFindClient);
            this.groupControl1.Location = new System.Drawing.Point(12, 69);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(564, 416);
            this.groupControl1.TabIndex = 9;
            this.groupControl1.Text = "<b>Yes</b>, I want to check if the Client already exists...    Fill in the detail" +
    "s below and click the <b>Find</b> button";
            // 
            // groupControl2
            // 
            this.groupControl2.AllowHtmlText = true;
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.btnAcceptClientSelection);
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Location = new System.Drawing.Point(9, 57);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(546, 350);
            this.groupControl2.TabIndex = 15;
            this.groupControl2.Text = "Matching <b>Clients</b>...     Click on any found match to select it then click t" +
    "he <b>Accept Selection</b> button";
            // 
            // btnAcceptClientSelection
            // 
            this.btnAcceptClientSelection.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnAcceptClientSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAcceptClientSelection.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAcceptClientSelection.Appearance.Options.UseFont = true;
            this.btnAcceptClientSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAcceptClientSelection.ImageOptions.Image")));
            this.btnAcceptClientSelection.Location = new System.Drawing.Point(4, 323);
            this.btnAcceptClientSelection.Name = "btnAcceptClientSelection";
            this.btnAcceptClientSelection.Size = new System.Drawing.Size(219, 23);
            this.btnAcceptClientSelection.TabIndex = 12;
            this.btnAcceptClientSelection.Text = "<b>Accept Selection</b> and return to Caller";
            this.btnAcceptClientSelection.Click += new System.EventHandler(this.btnAcceptClientSelection_Click);
            // 
            // teClientCriteria
            // 
            this.teClientCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teClientCriteria.Location = new System.Drawing.Point(158, 29);
            this.teClientCriteria.MenuManager = this.barManager1;
            this.teClientCriteria.Name = "teClientCriteria";
            this.teClientCriteria.Properties.MaxLength = 50;
            this.teClientCriteria.Properties.NullValuePrompt = "Enter a value to search for";
            this.teClientCriteria.Properties.NullValuePromptShowForEmptyValue = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.teClientCriteria, true);
            this.teClientCriteria.Size = new System.Drawing.Size(303, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.teClientCriteria, optionsSpelling1);
            this.teClientCriteria.TabIndex = 13;
            this.teClientCriteria.EditValueChanged += new System.EventHandler(this.teClientCriteria_EditValueChanged);
            // 
            // cbeClientMatchType
            // 
            this.cbeClientMatchType.EditValue = "Client Name Starts With";
            this.cbeClientMatchType.Location = new System.Drawing.Point(9, 29);
            this.cbeClientMatchType.MenuManager = this.barManager1;
            this.cbeClientMatchType.Name = "cbeClientMatchType";
            this.cbeClientMatchType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeClientMatchType.Properties.Items.AddRange(new object[] {
            "Client Name Starts With",
            "Client Name Contains",
            "Client Name Ends With"});
            this.cbeClientMatchType.Size = new System.Drawing.Size(143, 20);
            this.cbeClientMatchType.TabIndex = 12;
            // 
            // btnFindClient
            // 
            this.btnFindClient.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnFindClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindClient.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Find_16x16;
            this.btnFindClient.Location = new System.Drawing.Point(467, 29);
            this.btnFindClient.Name = "btnFindClient";
            this.btnFindClient.Size = new System.Drawing.Size(88, 20);
            this.btnFindClient.TabIndex = 11;
            this.btnFindClient.Text = "<b>Find</b> Client";
            this.btnFindClient.Click += new System.EventHandler(this.btnFindClient_Click);
            // 
            // sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersTableAdapter
            // 
            this.sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersTableAdapter.ClearBeforeFill = true;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 26);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageClient;
            this.xtraTabControl1.Size = new System.Drawing.Size(615, 539);
            this.xtraTabControl1.TabIndex = 10;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageClient,
            this.xtraTabPageSite,
            this.xtraTabPageClientContactPerson});
            // 
            // xtraTabPageClient
            // 
            this.xtraTabPageClient.Controls.Add(this.btnCancelClient);
            this.xtraTabPageClient.Controls.Add(this.btnAddClient);
            this.xtraTabPageClient.Controls.Add(this.groupControl1);
            this.xtraTabPageClient.Controls.Add(this.labelControl2);
            this.xtraTabPageClient.Controls.Add(this.labelControl1);
            this.xtraTabPageClient.Controls.Add(this.pictureEdit1);
            this.xtraTabPageClient.Name = "xtraTabPageClient";
            this.xtraTabPageClient.Size = new System.Drawing.Size(588, 534);
            this.xtraTabPageClient.Text = "Client";
            // 
            // btnCancelClient
            // 
            this.btnCancelClient.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnCancelClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelClient.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelClient.ImageOptions.Image")));
            this.btnCancelClient.Location = new System.Drawing.Point(478, 499);
            this.btnCancelClient.Name = "btnCancelClient";
            this.btnCancelClient.Size = new System.Drawing.Size(98, 23);
            this.btnCancelClient.TabIndex = 10;
            this.btnCancelClient.Text = "Cancel";
            this.btnCancelClient.Click += new System.EventHandler(this.btnCancelClient_Click);
            // 
            // btnAddClient
            // 
            this.btnAddClient.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnAddClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddClient.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.btnAddClient.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnAddClient.Location = new System.Drawing.Point(12, 499);
            this.btnAddClient.Name = "btnAddClient";
            this.btnAddClient.Size = new System.Drawing.Size(207, 23);
            this.btnAddClient.TabIndex = 6;
            this.btnAddClient.Text = "<b>No</b>, I want to <b> Add a New Client</b>...";
            this.btnAddClient.Click += new System.EventHandler(this.btnAddClient_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Location = new System.Drawing.Point(54, 31);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(236, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Do you wish to Check if the Client already Exists?";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(54, 13);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(57, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Add Client";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(12, 13);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(32, 32);
            this.pictureEdit1.TabIndex = 2;
            // 
            // xtraTabPageSite
            // 
            this.xtraTabPageSite.Controls.Add(this.labelClientFilter);
            this.xtraTabPageSite.Controls.Add(this.memoExEditClientFilter);
            this.xtraTabPageSite.Controls.Add(this.btnCancelSite);
            this.xtraTabPageSite.Controls.Add(this.btnAddSite);
            this.xtraTabPageSite.Controls.Add(this.groupControl3);
            this.xtraTabPageSite.Controls.Add(this.labelControl3);
            this.xtraTabPageSite.Controls.Add(this.labelControl4);
            this.xtraTabPageSite.Controls.Add(this.pictureEdit2);
            this.xtraTabPageSite.Name = "xtraTabPageSite";
            this.xtraTabPageSite.Size = new System.Drawing.Size(588, 534);
            this.xtraTabPageSite.Text = "Site";
            // 
            // labelClientFilter
            // 
            this.labelClientFilter.AllowHtmlString = true;
            this.labelClientFilter.Location = new System.Drawing.Point(317, 12);
            this.labelClientFilter.Name = "labelClientFilter";
            this.labelClientFilter.Size = new System.Drawing.Size(58, 13);
            this.labelClientFilter.TabIndex = 14;
            this.labelClientFilter.Text = "Client Filter:";
            // 
            // memoExEditClientFilter
            // 
            this.memoExEditClientFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memoExEditClientFilter.Location = new System.Drawing.Point(336, 27);
            this.memoExEditClientFilter.MenuManager = this.barManager1;
            this.memoExEditClientFilter.Name = "memoExEditClientFilter";
            this.memoExEditClientFilter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEditClientFilter.Properties.ReadOnly = true;
            this.memoExEditClientFilter.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.memoExEditClientFilter.Properties.ShowIcon = false;
            this.memoExEditClientFilter.Size = new System.Drawing.Size(241, 20);
            this.memoExEditClientFilter.TabIndex = 13;
            // 
            // btnCancelSite
            // 
            this.btnCancelSite.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnCancelSite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelSite.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelSite.ImageOptions.Image")));
            this.btnCancelSite.Location = new System.Drawing.Point(478, 499);
            this.btnCancelSite.Name = "btnCancelSite";
            this.btnCancelSite.Size = new System.Drawing.Size(98, 23);
            this.btnCancelSite.TabIndex = 12;
            this.btnCancelSite.Text = "Cancel";
            this.btnCancelSite.Click += new System.EventHandler(this.btnCancelSite_Click);
            // 
            // btnAddSite
            // 
            this.btnAddSite.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnAddSite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddSite.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.btnAddSite.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnAddSite.Location = new System.Drawing.Point(12, 499);
            this.btnAddSite.Name = "btnAddSite";
            this.btnAddSite.Size = new System.Drawing.Size(207, 23);
            this.btnAddSite.TabIndex = 11;
            this.btnAddSite.Text = "<b>No</b>, I want to <b> Add a New Site</b>...";
            this.btnAddSite.Click += new System.EventHandler(this.btnAddSite_Click);
            // 
            // groupControl3
            // 
            this.groupControl3.AllowHtmlText = true;
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.groupControl4);
            this.groupControl3.Controls.Add(this.teSiteCriteria);
            this.groupControl3.Controls.Add(this.cbeSiteMatchType);
            this.groupControl3.Controls.Add(this.btnFindSite);
            this.groupControl3.Location = new System.Drawing.Point(12, 69);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(564, 416);
            this.groupControl3.TabIndex = 10;
            this.groupControl3.Text = "<b>Yes</b>, I want to check if the Site already exists...    Fill in the details " +
    "below and click the <b>Find</b> button";
            // 
            // groupControl4
            // 
            this.groupControl4.AllowHtmlText = true;
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.btnAcceptSiteSelection);
            this.groupControl4.Controls.Add(this.gridControl2);
            this.groupControl4.Location = new System.Drawing.Point(9, 57);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(546, 350);
            this.groupControl4.TabIndex = 15;
            this.groupControl4.Text = "Matching <b>Sites</b>...     Click on any found match to select it then click the" +
    " <b>Accept Selection</b> button";
            // 
            // btnAcceptSiteSelection
            // 
            this.btnAcceptSiteSelection.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnAcceptSiteSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAcceptSiteSelection.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAcceptSiteSelection.Appearance.Options.UseFont = true;
            this.btnAcceptSiteSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAcceptSiteSelection.ImageOptions.Image")));
            this.btnAcceptSiteSelection.Location = new System.Drawing.Point(4, 323);
            this.btnAcceptSiteSelection.Name = "btnAcceptSiteSelection";
            this.btnAcceptSiteSelection.Size = new System.Drawing.Size(219, 23);
            this.btnAcceptSiteSelection.TabIndex = 12;
            this.btnAcceptSiteSelection.Text = "<b>Accept Selection</b> and return to Caller";
            this.btnAcceptSiteSelection.Click += new System.EventHandler(this.btnAcceptSiteSelection_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp01048CoreSearchforSiteNameBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.Location = new System.Drawing.Point(3, 23);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl2.Size = new System.Drawing.Size(540, 295);
            this.gridControl2.TabIndex = 4;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp01048CoreSearchforSiteNameBindingSource
            // 
            this.sp01048CoreSearchforSiteNameBindingSource.DataMember = "sp01048_Core_Search_for_Site_Name";
            this.sp01048CoreSearchforSiteNameBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteName,
            this.colSiteID,
            this.colClientID1,
            this.colClientName1,
            this.colSiteAddress,
            this.colSitePostcode,
            this.colSiteLocationX,
            this.colSiteLocationY,
            this.colSiteCode});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsFind.FindDelay = 2000;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 0;
            this.colSiteName.Width = 237;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientName1
            // 
            this.colClientName1.Caption = "Client Name";
            this.colClientName1.FieldName = "ClientName";
            this.colClientName1.Name = "colClientName1";
            this.colClientName1.OptionsColumn.AllowEdit = false;
            this.colClientName1.OptionsColumn.AllowFocus = false;
            this.colClientName1.OptionsColumn.ReadOnly = true;
            this.colClientName1.Visible = true;
            this.colClientName1.VisibleIndex = 4;
            this.colClientName1.Width = 230;
            // 
            // colSiteAddress
            // 
            this.colSiteAddress.Caption = "Address";
            this.colSiteAddress.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colSiteAddress.FieldName = "SiteAddress";
            this.colSiteAddress.Name = "colSiteAddress";
            this.colSiteAddress.OptionsColumn.ReadOnly = true;
            this.colSiteAddress.Visible = true;
            this.colSiteAddress.VisibleIndex = 2;
            this.colSiteAddress.Width = 186;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colSitePostcode
            // 
            this.colSitePostcode.Caption = "Postcode";
            this.colSitePostcode.FieldName = "SitePostcode";
            this.colSitePostcode.Name = "colSitePostcode";
            this.colSitePostcode.OptionsColumn.AllowEdit = false;
            this.colSitePostcode.OptionsColumn.AllowFocus = false;
            this.colSitePostcode.OptionsColumn.ReadOnly = true;
            this.colSitePostcode.Visible = true;
            this.colSitePostcode.VisibleIndex = 3;
            // 
            // colSiteLocationX
            // 
            this.colSiteLocationX.Caption = "Site Location X";
            this.colSiteLocationX.FieldName = "SiteLocationX";
            this.colSiteLocationX.Name = "colSiteLocationX";
            this.colSiteLocationX.OptionsColumn.AllowEdit = false;
            this.colSiteLocationX.OptionsColumn.AllowFocus = false;
            this.colSiteLocationX.OptionsColumn.ReadOnly = true;
            this.colSiteLocationX.Width = 89;
            // 
            // colSiteLocationY
            // 
            this.colSiteLocationY.Caption = "Site Location Y";
            this.colSiteLocationY.FieldName = "SiteLocationY";
            this.colSiteLocationY.Name = "colSiteLocationY";
            this.colSiteLocationY.OptionsColumn.AllowEdit = false;
            this.colSiteLocationY.OptionsColumn.AllowFocus = false;
            this.colSiteLocationY.OptionsColumn.ReadOnly = true;
            this.colSiteLocationY.Width = 89;
            // 
            // colSiteCode
            // 
            this.colSiteCode.Caption = "Site Code";
            this.colSiteCode.FieldName = "SiteCode";
            this.colSiteCode.Name = "colSiteCode";
            this.colSiteCode.OptionsColumn.AllowEdit = false;
            this.colSiteCode.OptionsColumn.AllowFocus = false;
            this.colSiteCode.OptionsColumn.ReadOnly = true;
            this.colSiteCode.Visible = true;
            this.colSiteCode.VisibleIndex = 1;
            this.colSiteCode.Width = 91;
            // 
            // teSiteCriteria
            // 
            this.teSiteCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teSiteCriteria.Location = new System.Drawing.Point(158, 29);
            this.teSiteCriteria.MenuManager = this.barManager1;
            this.teSiteCriteria.Name = "teSiteCriteria";
            this.teSiteCriteria.Properties.MaxLength = 50;
            this.teSiteCriteria.Properties.NullValuePrompt = "Enter a value to search for";
            this.teSiteCriteria.Properties.NullValuePromptShowForEmptyValue = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.teSiteCriteria, true);
            this.teSiteCriteria.Size = new System.Drawing.Size(303, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.teSiteCriteria, optionsSpelling2);
            this.teSiteCriteria.TabIndex = 13;
            this.teSiteCriteria.EditValueChanged += new System.EventHandler(this.teSiteCriteria_EditValueChanged);
            // 
            // cbeSiteMatchType
            // 
            this.cbeSiteMatchType.EditValue = "Site Name Starts With";
            this.cbeSiteMatchType.Location = new System.Drawing.Point(9, 29);
            this.cbeSiteMatchType.MenuManager = this.barManager1;
            this.cbeSiteMatchType.Name = "cbeSiteMatchType";
            this.cbeSiteMatchType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeSiteMatchType.Properties.Items.AddRange(new object[] {
            "Site Name Starts With",
            "Site Name Contains",
            "Site Name Ends With"});
            this.cbeSiteMatchType.Size = new System.Drawing.Size(143, 20);
            this.cbeSiteMatchType.TabIndex = 12;
            // 
            // btnFindSite
            // 
            this.btnFindSite.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnFindSite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindSite.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Find_16x16;
            this.btnFindSite.Location = new System.Drawing.Point(467, 29);
            this.btnFindSite.Name = "btnFindSite";
            this.btnFindSite.Size = new System.Drawing.Size(88, 20);
            this.btnFindSite.TabIndex = 11;
            this.btnFindSite.Text = "<b>Find</b> Site";
            this.btnFindSite.Click += new System.EventHandler(this.btnFindSite_Click);
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Location = new System.Drawing.Point(54, 31);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(227, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Do you wish to Check if the Site already Exists?";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(54, 13);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(47, 13);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "Add Site";
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.Home_32x32;
            this.pictureEdit2.Location = new System.Drawing.Point(12, 13);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Size = new System.Drawing.Size(32, 32);
            this.pictureEdit2.TabIndex = 1;
            // 
            // xtraTabPageClientContactPerson
            // 
            this.xtraTabPageClientContactPerson.Controls.Add(this.labelClientFilter2);
            this.xtraTabPageClientContactPerson.Controls.Add(this.memoExEditClientFilter2);
            this.xtraTabPageClientContactPerson.Controls.Add(this.btnCancelClientContact);
            this.xtraTabPageClientContactPerson.Controls.Add(this.btnAddClientContact);
            this.xtraTabPageClientContactPerson.Controls.Add(this.groupControl5);
            this.xtraTabPageClientContactPerson.Controls.Add(this.labelControl6);
            this.xtraTabPageClientContactPerson.Controls.Add(this.labelControl7);
            this.xtraTabPageClientContactPerson.Controls.Add(this.pictureEdit3);
            this.xtraTabPageClientContactPerson.Name = "xtraTabPageClientContactPerson";
            this.xtraTabPageClientContactPerson.Size = new System.Drawing.Size(588, 534);
            this.xtraTabPageClientContactPerson.Text = "Client Contact Person";
            // 
            // labelClientFilter2
            // 
            this.labelClientFilter2.AllowHtmlString = true;
            this.labelClientFilter2.Location = new System.Drawing.Point(342, 12);
            this.labelClientFilter2.Name = "labelClientFilter2";
            this.labelClientFilter2.Size = new System.Drawing.Size(58, 13);
            this.labelClientFilter2.TabIndex = 22;
            this.labelClientFilter2.Text = "Client Filter:";
            // 
            // memoExEditClientFilter2
            // 
            this.memoExEditClientFilter2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.memoExEditClientFilter2.Location = new System.Drawing.Point(361, 27);
            this.memoExEditClientFilter2.MenuManager = this.barManager1;
            this.memoExEditClientFilter2.Name = "memoExEditClientFilter2";
            this.memoExEditClientFilter2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.memoExEditClientFilter2.Properties.ReadOnly = true;
            this.memoExEditClientFilter2.Properties.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.memoExEditClientFilter2.Properties.ShowIcon = false;
            this.memoExEditClientFilter2.Size = new System.Drawing.Size(216, 20);
            this.memoExEditClientFilter2.TabIndex = 21;
            // 
            // btnCancelClientContact
            // 
            this.btnCancelClientContact.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnCancelClientContact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelClientContact.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelClientContact.ImageOptions.Image")));
            this.btnCancelClientContact.Location = new System.Drawing.Point(478, 499);
            this.btnCancelClientContact.Name = "btnCancelClientContact";
            this.btnCancelClientContact.Size = new System.Drawing.Size(98, 23);
            this.btnCancelClientContact.TabIndex = 20;
            this.btnCancelClientContact.Text = "Cancel";
            this.btnCancelClientContact.Click += new System.EventHandler(this.btnCancelClientContact_Click);
            // 
            // btnAddClientContact
            // 
            this.btnAddClientContact.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnAddClientContact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddClientContact.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Add_16x16;
            this.btnAddClientContact.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnAddClientContact.Location = new System.Drawing.Point(12, 499);
            this.btnAddClientContact.Name = "btnAddClientContact";
            this.btnAddClientContact.Size = new System.Drawing.Size(261, 23);
            this.btnAddClientContact.TabIndex = 19;
            this.btnAddClientContact.Text = "<b>No</b>, I want to <b> Add a New Contact Person</b>...";
            this.btnAddClientContact.Click += new System.EventHandler(this.btnAddClientContact_Click);
            // 
            // groupControl5
            // 
            this.groupControl5.AllowHtmlText = true;
            this.groupControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl5.Controls.Add(this.groupControl6);
            this.groupControl5.Controls.Add(this.teClientContactCriteria);
            this.groupControl5.Controls.Add(this.cbeClientContactMatchType);
            this.groupControl5.Controls.Add(this.btnFindClientContact);
            this.groupControl5.Location = new System.Drawing.Point(12, 69);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(564, 416);
            this.groupControl5.TabIndex = 18;
            this.groupControl5.Text = "<b>Yes</b>, I want to check if the Contact Person already exists...    Fill in th" +
    "e details below and click the <b>Find</b> button";
            // 
            // groupControl6
            // 
            this.groupControl6.AllowHtmlText = true;
            this.groupControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl6.Controls.Add(this.btnAcceptClientContactSelection);
            this.groupControl6.Controls.Add(this.gridControl3);
            this.groupControl6.Location = new System.Drawing.Point(9, 57);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(546, 350);
            this.groupControl6.TabIndex = 15;
            this.groupControl6.Text = "Matching <b>Contacts</b>...     Click on any found match to select it then click " +
    "the <b>Accept Selection</b> button";
            // 
            // btnAcceptClientContactSelection
            // 
            this.btnAcceptClientContactSelection.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnAcceptClientContactSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAcceptClientContactSelection.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnAcceptClientContactSelection.Appearance.Options.UseFont = true;
            this.btnAcceptClientContactSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnAcceptClientContactSelection.ImageOptions.Image")));
            this.btnAcceptClientContactSelection.Location = new System.Drawing.Point(4, 323);
            this.btnAcceptClientContactSelection.Name = "btnAcceptClientContactSelection";
            this.btnAcceptClientContactSelection.Size = new System.Drawing.Size(219, 23);
            this.btnAcceptClientContactSelection.TabIndex = 12;
            this.btnAcceptClientContactSelection.Text = "<b>Accept Selection</b> and return to Caller";
            this.btnAcceptClientContactSelection.Click += new System.EventHandler(this.btnAcceptClientContactSelection_Click);
            // 
            // gridControl3
            // 
            this.gridControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl3.DataSource = this.sp01051CoreSearchForClientContactPersonBindingSource;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Location = new System.Drawing.Point(3, 23);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit2});
            this.gridControl3.Size = new System.Drawing.Size(540, 295);
            this.gridControl3.TabIndex = 4;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp01051CoreSearchForClientContactPersonBindingSource
            // 
            this.sp01051CoreSearchForClientContactPersonBindingSource.DataMember = "sp01051_Core_Search_For_Client_Contact_Person";
            this.sp01051CoreSearchForClientContactPersonBindingSource.DataSource = this.dataSet_Common_Functionality;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPersonName,
            this.colClientContactPersonID,
            this.colClientID2,
            this.colTitle,
            this.colPosition,
            this.colClientName2,
            this.colClientContactTelephone,
            this.colClientContactMobile,
            this.colClientContactEmail});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.OptionsFind.FindDelay = 2000;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsLayout.StoreFormatRules = true;
            this.gridView3.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colClientName2, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colPersonName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.GridView_FocusedRowChanged_NoGroupSelection);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_NoGroupSelection);
            // 
            // colPersonName
            // 
            this.colPersonName.Caption = "Contact Person Name";
            this.colPersonName.FieldName = "PersonName";
            this.colPersonName.Name = "colPersonName";
            this.colPersonName.OptionsColumn.AllowEdit = false;
            this.colPersonName.OptionsColumn.AllowFocus = false;
            this.colPersonName.OptionsColumn.ReadOnly = true;
            this.colPersonName.Visible = true;
            this.colPersonName.VisibleIndex = 0;
            this.colPersonName.Width = 224;
            // 
            // colClientContactPersonID
            // 
            this.colClientContactPersonID.Caption = "Client Contact Person ID";
            this.colClientContactPersonID.FieldName = "ClientContactPersonID";
            this.colClientContactPersonID.Name = "colClientContactPersonID";
            this.colClientContactPersonID.OptionsColumn.AllowEdit = false;
            this.colClientContactPersonID.OptionsColumn.AllowFocus = false;
            this.colClientContactPersonID.OptionsColumn.ReadOnly = true;
            this.colClientContactPersonID.Width = 137;
            // 
            // colClientID2
            // 
            this.colClientID2.Caption = "Client ID";
            this.colClientID2.FieldName = "ClientID";
            this.colClientID2.Name = "colClientID2";
            this.colClientID2.OptionsColumn.AllowEdit = false;
            this.colClientID2.OptionsColumn.AllowFocus = false;
            this.colClientID2.OptionsColumn.ReadOnly = true;
            // 
            // colTitle
            // 
            this.colTitle.Caption = "Title";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.AllowFocus = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 1;
            this.colTitle.Width = 153;
            // 
            // colPosition
            // 
            this.colPosition.Caption = "Position";
            this.colPosition.FieldName = "Position";
            this.colPosition.Name = "colPosition";
            this.colPosition.OptionsColumn.AllowEdit = false;
            this.colPosition.OptionsColumn.AllowFocus = false;
            this.colPosition.OptionsColumn.ReadOnly = true;
            this.colPosition.Visible = true;
            this.colPosition.VisibleIndex = 2;
            this.colPosition.Width = 158;
            // 
            // colClientName2
            // 
            this.colClientName2.Caption = "Client Name";
            this.colClientName2.FieldName = "ClientName";
            this.colClientName2.Name = "colClientName2";
            this.colClientName2.OptionsColumn.AllowEdit = false;
            this.colClientName2.OptionsColumn.AllowFocus = false;
            this.colClientName2.OptionsColumn.ReadOnly = true;
            this.colClientName2.Visible = true;
            this.colClientName2.VisibleIndex = 3;
            this.colClientName2.Width = 326;
            // 
            // colClientContactTelephone
            // 
            this.colClientContactTelephone.Caption = "Telephone";
            this.colClientContactTelephone.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colClientContactTelephone.FieldName = "ClientContactTelephone";
            this.colClientContactTelephone.Name = "colClientContactTelephone";
            this.colClientContactTelephone.OptionsColumn.ReadOnly = true;
            this.colClientContactTelephone.Visible = true;
            this.colClientContactTelephone.VisibleIndex = 4;
            this.colClientContactTelephone.Width = 100;
            // 
            // repositoryItemMemoExEdit2
            // 
            this.repositoryItemMemoExEdit2.AutoHeight = false;
            this.repositoryItemMemoExEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit2.Name = "repositoryItemMemoExEdit2";
            this.repositoryItemMemoExEdit2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit2.ShowIcon = false;
            // 
            // colClientContactMobile
            // 
            this.colClientContactMobile.Caption = "Mobile";
            this.colClientContactMobile.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colClientContactMobile.FieldName = "ClientContactMobile";
            this.colClientContactMobile.Name = "colClientContactMobile";
            this.colClientContactMobile.OptionsColumn.ReadOnly = true;
            this.colClientContactMobile.Visible = true;
            this.colClientContactMobile.VisibleIndex = 5;
            this.colClientContactMobile.Width = 100;
            // 
            // colClientContactEmail
            // 
            this.colClientContactEmail.Caption = "Email";
            this.colClientContactEmail.ColumnEdit = this.repositoryItemMemoExEdit2;
            this.colClientContactEmail.FieldName = "ClientContactEmail";
            this.colClientContactEmail.Name = "colClientContactEmail";
            this.colClientContactEmail.OptionsColumn.ReadOnly = true;
            this.colClientContactEmail.Visible = true;
            this.colClientContactEmail.VisibleIndex = 6;
            this.colClientContactEmail.Width = 150;
            // 
            // teClientContactCriteria
            // 
            this.teClientContactCriteria.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.teClientContactCriteria.Location = new System.Drawing.Point(168, 29);
            this.teClientContactCriteria.MenuManager = this.barManager1;
            this.teClientContactCriteria.Name = "teClientContactCriteria";
            this.teClientContactCriteria.Properties.MaxLength = 50;
            this.teClientContactCriteria.Properties.NullValuePrompt = "Enter a value to search for";
            this.teClientContactCriteria.Properties.NullValuePromptShowForEmptyValue = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.teClientContactCriteria, true);
            this.teClientContactCriteria.Size = new System.Drawing.Size(281, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.teClientContactCriteria, optionsSpelling3);
            this.teClientContactCriteria.TabIndex = 13;
            this.teClientContactCriteria.EditValueChanged += new System.EventHandler(this.teClientContactCriteria_EditValueChanged);
            // 
            // cbeClientContactMatchType
            // 
            this.cbeClientContactMatchType.EditValue = "Contact Name Starts With";
            this.cbeClientContactMatchType.Location = new System.Drawing.Point(9, 29);
            this.cbeClientContactMatchType.MenuManager = this.barManager1;
            this.cbeClientContactMatchType.Name = "cbeClientContactMatchType";
            this.cbeClientContactMatchType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbeClientContactMatchType.Properties.Items.AddRange(new object[] {
            "Contact Name Starts With",
            "Contact Name Contains",
            "Contact Name Ends With"});
            this.cbeClientContactMatchType.Size = new System.Drawing.Size(153, 20);
            this.cbeClientContactMatchType.TabIndex = 12;
            // 
            // btnFindClientContact
            // 
            this.btnFindClientContact.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnFindClientContact.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFindClientContact.ImageOptions.Image = global::WoodPlan5.Properties.Resources.Find_16x16;
            this.btnFindClientContact.Location = new System.Drawing.Point(455, 29);
            this.btnFindClientContact.Name = "btnFindClientContact";
            this.btnFindClientContact.Size = new System.Drawing.Size(98, 20);
            this.btnFindClientContact.TabIndex = 11;
            this.btnFindClientContact.Text = "<b>Find</b> Person";
            this.btnFindClientContact.Click += new System.EventHandler(this.btnFindClientContact_Click);
            // 
            // labelControl6
            // 
            this.labelControl6.AllowHtmlString = true;
            this.labelControl6.Location = new System.Drawing.Point(54, 31);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(283, 13);
            this.labelControl6.TabIndex = 17;
            this.labelControl6.Text = "Do you wish to Check if the Contact Person already Exists?";
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(54, 13);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(146, 13);
            this.labelControl7.TabIndex = 16;
            this.labelControl7.Text = "Add Client Contact Person";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.EditValue = ((object)(resources.GetObject("pictureEdit3.EditValue")));
            this.pictureEdit3.Location = new System.Drawing.Point(12, 13);
            this.pictureEdit3.MenuManager = this.barManager1;
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit3.Properties.ShowMenu = false;
            this.pictureEdit3.Size = new System.Drawing.Size(32, 32);
            this.pictureEdit3.TabIndex = 15;
            // 
            // sp01047_Core_Search_for_Client_NameTableAdapter
            // 
            this.sp01047_Core_Search_for_Client_NameTableAdapter.ClearBeforeFill = true;
            // 
            // sp01048_Core_Search_for_Site_NameTableAdapter
            // 
            this.sp01048_Core_Search_for_Site_NameTableAdapter.ClearBeforeFill = true;
            // 
            // sp01051_Core_Search_For_Client_Contact_PersonTableAdapter
            // 
            this.sp01051_Core_Search_For_Client_Contact_PersonTableAdapter.ClearBeforeFill = true;
            // 
            // frm_Core_Add_Client_Or_Site_Check_Exists
            // 
            this.ClientSize = new System.Drawing.Size(615, 565);
            this.ControlBox = false;
            this.Controls.Add(this.xtraTabControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_Core_Add_Client_Or_Site_Check_Exists";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Record - Check If Exists";
            this.Load += new System.EventHandler(this.frm_Core_Add_Client_Or_Site_Check_Exists_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01047CoreSearchforClientNameBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_Common_Functionality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Job)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.teClientCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeClientMatchType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageClient.ResumeLayout(false);
            this.xtraTabPageClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.xtraTabPageSite.ResumeLayout(false);
            this.xtraTabPageSite.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditClientFilter.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01048CoreSearchforSiteNameBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSiteCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeSiteMatchType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            this.xtraTabPageClientContactPerson.ResumeLayout(false);
            this.xtraTabPageClientContactPerson.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.memoExEditClientFilter2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp01051CoreSearchForClientContactPersonBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teClientContactCriteria.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbeClientContactMatchType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnFindClient;
        private System.Windows.Forms.BindingSource sp06399OMBlockAddLabourOnSiteSelectTeamMembersBindingSource;
        private DataSet_OM_Job dataSet_OM_Job;
        private DataSet_OM_JobTableAdapters.sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersTableAdapter sp06399_OM_Block_Add_Labour_On_Site_Select_Team_MembersTableAdapter;
        public DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageClient;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSite;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnAddClient;
        private DevExpress.XtraEditors.TextEdit teClientCriteria;
        private DevExpress.XtraEditors.ComboBoxEdit cbeClientMatchType;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.SimpleButton btnAcceptClientSelection;
        private System.Windows.Forms.BindingSource sp01047CoreSearchforClientNameBindingSource;
        private DataSet_Common_Functionality dataSet_Common_Functionality;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DataSet_Common_FunctionalityTableAdapters.sp01047_Core_Search_for_Client_NameTableAdapter sp01047_Core_Search_for_Client_NameTableAdapter;
        private DevExpress.XtraEditors.SimpleButton btnCancelClient;
        private DevExpress.XtraEditors.SimpleButton btnCancelSite;
        private DevExpress.XtraEditors.SimpleButton btnAddSite;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton btnAcceptSiteSelection;
        public DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.TextEdit teSiteCriteria;
        private DevExpress.XtraEditors.ComboBoxEdit cbeSiteMatchType;
        private DevExpress.XtraEditors.SimpleButton btnFindSite;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.BindingSource sp01048CoreSearchforSiteNameBindingSource;
        private DataSet_Common_FunctionalityTableAdapters.sp01048_Core_Search_for_Site_NameTableAdapter sp01048_Core_Search_for_Site_NameTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteAddress;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colSitePostcode;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationX;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteLocationY;
        private DevExpress.XtraEditors.LabelControl labelClientFilter;
        private DevExpress.XtraEditors.MemoExEdit memoExEditClientFilter;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteCode;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageClientContactPerson;
        private DevExpress.XtraEditors.LabelControl labelClientFilter2;
        private DevExpress.XtraEditors.MemoExEdit memoExEditClientFilter2;
        private DevExpress.XtraEditors.SimpleButton btnCancelClientContact;
        private DevExpress.XtraEditors.SimpleButton btnAddClientContact;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private DevExpress.XtraEditors.SimpleButton btnAcceptClientContactSelection;
        public DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit2;
        private DevExpress.XtraEditors.TextEdit teClientContactCriteria;
        private DevExpress.XtraEditors.ComboBoxEdit cbeClientContactMatchType;
        private DevExpress.XtraEditors.SimpleButton btnFindClientContact;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private System.Windows.Forms.BindingSource sp01051CoreSearchForClientContactPersonBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPersonName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactPersonID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID2;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName2;
        private DataSet_Common_FunctionalityTableAdapters.sp01051_Core_Search_For_Client_Contact_PersonTableAdapter sp01051_Core_Search_For_Client_Contact_PersonTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactTelephone;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactMobile;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContactEmail;
    }
}

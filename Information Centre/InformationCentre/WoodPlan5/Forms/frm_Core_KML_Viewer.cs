﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraMap;

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_KML_Viewer : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private string bingkey = "";
        string i_str_MapType = "Bing Satellite";
        private VectorItemsLayer _vectorItemsLayer;
        private ImageTilesLayer _BackgroundLayer;
        public string strPassedInKMLFileName = "";
        private string strKMLFileViewerOpenFileDefaultLocation = "";

        double latitude;
        double longitude;
        private bool BestFitZoomInProcess = false;
        int intPointsToZoomToCount = 0;
        int intRecordIDToZoomTo = 0;


        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        private bool boolFirstLoadOfMap = true;

        #endregion

        public frm_Core_KML_Viewer()
        {
            InitializeComponent();
        }

        private void frm_Core_KML_Viewer_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 500292;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            //this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager();

            DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            string strMapStartType = "Bing Satellite";
            try
            {
                strMapStartType = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "KMLFileViewerStartMapType").ToString();
            }
            catch (Exception) { }

            try
            {
                bingkey = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "BingMappingKey").ToString();
            }
            catch (Exception) 
            {
                // Unable to get Bing Map key so switch to OpenStreetMapping //
                strMapStartType = "OpenStreetMap";
            }

            try
            {
                strKMLFileViewerOpenFileDefaultLocation = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "KMLFileViewerOpenFileDefaultLocation").ToString();
            }
            catch (Exception) 
            {
                // Unable to get Bing Map key so switch to OpenStreetMapping //
                strKMLFileViewerOpenFileDefaultLocation = "C:";
            }



            

            mapControl1.EnableAnimation = false;  // Speed up laoding - turn off animation //

            _BackgroundLayer = new ImageTilesLayer();
            mapControl1.Layers.Add(_BackgroundLayer);
            ImageTilesLayer layer = null;
            if (strMapStartType == "Bing Satellite" && !string.IsNullOrWhiteSpace(bingkey))
            {
                BingMapDataProvider bingProvider = new BingMapDataProvider();
                bingProvider.Kind = BingMapKind.Area;
                layer = (ImageTilesLayer)mapControl1.Layers[0];
                layer.DataProvider = bingProvider;
                bingProvider.BingKey = bingkey;
            }
            else  // Unable to get Bing so use Open Street Mapping instead //
            {
                OpenStreetMapDataProvider openStreetMapDataProvider = new OpenStreetMapDataProvider();
                layer = (ImageTilesLayer)mapControl1.Layers[0];
                layer.DataProvider = openStreetMapDataProvider;
            }

            _vectorItemsLayer = new VectorItemsLayer();
            mapControl1.Layers.Add(_vectorItemsLayer);

            _vectorItemsLayer.DataLoaded += VectorItemsLayer_DataLoaded;  // Add listener event for Vector Layer Finished Loading Map Objects //
            mapControl1.RightClickDrag = false;  // Prevent dragging with the right mouse button //

            #region MiniMap

            // Create a mini map and data for it.         
            MiniMap miniMap = new MiniMap()
            {
                Alignment = MiniMapAlignment.BottomRight
            };
            miniMap.Behavior = new DynamicMiniMapBehavior();

            miniMap.Layers.Add(new MiniMapImageTilesLayer()
            {
                DataProvider = new BingMapDataProvider() { BingKey = bingkey }
            });
            mapControl1.MiniMap = miniMap;
            
            #endregion MiniMap

            if (!string.IsNullOrWhiteSpace(strPassedInKMLFileName)) LoadKmlFile(strPassedInKMLFileName);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            
            LoadLastSavedUserScreenSettings();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();

            Application.DoEvents();
            if (!string.IsNullOrWhiteSpace(strPassedInKMLFileName)) Zoom_Map_To_Objects();
        }
        
        private void frm_Core_KML_Viewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
            }
            _vectorItemsLayer.DataLoaded -= VectorItemsLayer_DataLoaded;  // Remove Event //
        }


        void pointLayer_ViewportChanged(object sender, ViewportChangedEventArgs e)
        {
            /*VectorItemsLayer pointLayer = (VectorItemsLayer)sender;
            if (BestFitZoomInProcess)
            {
                int p_count = 0;
                for (int i = 0; i < pointLayer.Items.Count; i++)
                {
                    GeoPoint geoPoint = ((MapCustomElement)pointLayer.Items[i]).Location;
                    if (Convert.ToInt32(pointLayer.Items[i].Attributes["ContractorID"].Value) == intRecordIDToZoomTo)
                    {
                        if (geoPoint.Latitude < e.TopLeft.Latitude && geoPoint.Latitude > e.BottomRight.Latitude && geoPoint.Longitude > e.TopLeft.Longitude && geoPoint.Longitude < e.BottomRight.Longitude) p_count++;
                    }
                }
                if (p_count == intPointsToZoomToCount) BestFitZoomInProcess = false;
            }*/
        }

        public void LoadLastSavedUserScreenSettings()
        {
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }



        #region Map Type

        private void btnOKMapType_Click(object sender, EventArgs e)
        {
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditMapType_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            string strTempType = "";
            if (checkEdit1.Checked)
            {
                strTempType = "Bing Hybrid";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = bingkey;
                    bingProvider.Kind = BingMapKind.Hybrid;
                    i_str_MapType = strTempType;
                    
                    mapControl1.MiniMap.Layers.Clear();
                    mapControl1.MiniMap.Layers.Add(new MiniMapImageTilesLayer()
                    {
                        DataProvider = new BingMapDataProvider() { BingKey = bingkey, Kind = BingMapKind.Hybrid }
                    });
                }
            }
            else if (checkEdit2.Checked)
            {
                strTempType = "Bing Satellite";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = bingkey;
                    bingProvider.Kind = BingMapKind.Area;
                    i_str_MapType = strTempType;

                    mapControl1.MiniMap.Layers.Clear();
                    mapControl1.MiniMap.Layers.Add(new  MiniMapImageTilesLayer()
                    {
                        DataProvider = new BingMapDataProvider() { BingKey = bingkey, Kind = BingMapKind.Area }                        
                    });
                }
            }
            else if (checkEdit3.Checked)
            {
                strTempType = "Bing Road";
                if (strTempType != i_str_MapType)
                {
                    BingMapDataProvider bingProvider = new BingMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = bingProvider;
                    bingProvider.BingKey = bingkey;
                    bingProvider.Kind = BingMapKind.Road;
                    i_str_MapType = strTempType;

                    mapControl1.MiniMap.Layers.Clear();
                    mapControl1.MiniMap.Layers.Add(new MiniMapImageTilesLayer()
                    {
                        DataProvider = new BingMapDataProvider() { BingKey = bingkey, Kind = BingMapKind.Road }
                    });
                }
            }
            else
            {
                strTempType = "Open Street Map";
                if (strTempType != i_str_MapType)
                {
                    OpenStreetMapDataProvider provider = new OpenStreetMapDataProvider();
                    ImageTilesLayer layer = (ImageTilesLayer)mapControl1.Layers[0];
                    layer.DataProvider = provider;
                    i_str_MapType = strTempType;

                    mapControl1.MiniMap.Layers.Clear();
                    mapControl1.MiniMap.Layers.Add(new MiniMapImageTilesLayer()
                    {
                        DataProvider = new OpenStreetMapDataProvider() { Kind = OpenStreetMapKind.Basic }
                    });
                }
            }
            return i_str_MapType;
        }

        #endregion


        #region Map Control

        private void mapControl1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            MapControl map = sender as MapControl;
            MapHitInfo hitInfo = map.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left)
            {
                return;
            }
            if (e.Button == MouseButtons.Right)
            {
                pmMapControl.ShowPopup(new System.Drawing.Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void bbiMapPrintPreview_ItemClick(object sender, ItemClickEventArgs e)
        {
            WaitDialogForm loadingForm = new WaitDialogForm("Loading Preview...", "Print Preview");
            loadingForm.Show();

            frmPrintPreview fPrintPreview = new frmPrintPreview();
            fPrintPreview.loadingForm = loadingForm;
            fPrintPreview.objObject = this;
            fPrintPreview.MdiParent = this.MdiParent;
            fPrintPreview.GlobalSettings = this.GlobalSettings;
            fPrintPreview.StaffID = GlobalSettings.UserID;
            fPrintPreview.Show();
            
            // Invoke Post Open event on Base form //
            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");
            if (method != null) method.Invoke(fPrintPreview, new object[] { null });
        }

        void VectorItemsLayer_DataLoaded(object sender, DataLoadedEventArgs e)
        {
            if (this.splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager.SetWaitFormDescription("Loading...");  // Reset caption //
                this.splashScreenManager.CloseWaitForm();
            }
            Set_Map_Object_Count();
        }

        #endregion


        #region Execute load file into map control
        
        private void LoadKmlFile(string fileName)
        {
            CleanMapContent();

            Uri uri;
            if (!GetFileUri(fileName, out uri))
            {
                return;
            }

            var dataAdapter = new KmlFileDataAdapter
            {
                FileUri = uri
            };

            _vectorItemsLayer.Data = dataAdapter;
        }

        private void LoadShpFile(string fileName)
        {
            CleanMapContent();

            Uri uri;
            if (!GetFileUri(fileName, out uri))
            {
                return;
            }

            var dataAdapter = new ShapefileDataAdapter
            {
                FileUri = uri
            };

            _vectorItemsLayer.Data = dataAdapter;
        }

        private void CleanMapContent()
        {
            _vectorItemsLayer.Data = null;

//            mapControl1.CenterPoint = new GeoPoint(20, -20);
//            mapControl1.ZoomLevel = 2;
        }

        private static bool GetFileUri(string fileName, out Uri uri)
        {
            if (!File.Exists(fileName))
            {
                //MessageBox.Show("File not found: {path} {basePath}");
                uri = null;
                return false;
            }

            uri = new Uri(fileName, UriKind.RelativeOrAbsolute);
            return true;
        }
        
        #endregion

       
        private void Set_Map_Object_Count()
        {
            bsiMapObjectCount.Caption = "Map Object Count: " + _vectorItemsLayer.Data.Count.ToString();
        }

        private void bciMiniMap_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            mapControl1.MiniMap.Visible = bciMiniMap.Checked;
        }

        private void bciNavigation_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            mapControl1.NavigationPanelOptions.Visible = bciNavigation.Checked;
        }

        public bool Save_Map_As_Image(string strFileName, System.Drawing.Imaging.ImageFormat imageFormat)
        {
            try
            {
                mapControl1.ExportToImage(strFileName, imageFormat);
            }
            catch (Exception) { return false; }
            return true;
        }

        public bool Load_KML_File(string strFileName)
        {
            try
            {
                LoadKmlFile(strFileName);
            }
            catch (Exception) { return false; }
            return true;

        }

        public bool Zoom_Map_To_Objects()
        {
            try
            {
                mapControl1.ZoomToFit(_vectorItemsLayer.Data.Items);
                mapControl1.ZoomLevel = Math.Min(mapControl1.ZoomLevel, 19.4);  // Bing Maps tiles are not available at a certain zoom level (ZoomLevel = 20) and the DataLoaded event is never fired so prevent it. //

                mapControl1.Invalidate();
            }
            catch (Exception) { return false; }
            return true;

        }


        private void bbiOpenKMLFile_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.DefaultExt = "kml";
                dlg.Filter = "KML Files (*.kml)|*.kml|KMZ Files (*.kmz)|*.kmz";
                dlg.InitialDirectory = strKMLFileViewerOpenFileDefaultLocation;
                dlg.Multiselect = false;
                dlg.ShowDialog();
                if (dlg.FileNames.Length > 0)
                {
                    foreach (string filename in dlg.FileNames)
                    {
                        LoadKmlFile(filename);
                    }
                }
                Application.DoEvents();
                if (!string.IsNullOrWhiteSpace(strPassedInKMLFileName)) Zoom_Map_To_Objects();

            }
        }

        private void bbiZoomToFile_ItemClick(object sender, ItemClickEventArgs e)
        {
            Zoom_Map_To_Objects();
        }

        private void bbiClearKML_ItemClick(object sender, ItemClickEventArgs e)
        {
            CleanMapContent();
        }

        private void bbiPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            mapControl1.ShowPrintPreview();
        }

        private void bbiSaveImage_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif|Png Image|*.png|Tiff Image|*.tiff|WMF Image|*.wmp";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            string strFileName = saveFileDialog1.FileName;
            if (!string.IsNullOrWhiteSpace(strFileName))
            {

                System.Drawing.Imaging.ImageFormat imageFormat = null;
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        {
                            imageFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
                        }
                        break;
                    case 2:
                        {
                            imageFormat = System.Drawing.Imaging.ImageFormat.Bmp;
                        }
                        break;
                    case 3:
                        {
                            imageFormat = System.Drawing.Imaging.ImageFormat.Gif;
                        }
                        break;
                    case 4:
                        {
                            imageFormat = System.Drawing.Imaging.ImageFormat.Png;
                        }
                        break;
                    case 5:
                        {
                            imageFormat = System.Drawing.Imaging.ImageFormat.Tiff;
                        }
                        break;
                    case 6:
                        {
                            imageFormat = System.Drawing.Imaging.ImageFormat.Wmf;
                        }
                        break;
                }
                mapControl1.ExportToImage(strFileName, imageFormat);
            }
        }

 


    }
}

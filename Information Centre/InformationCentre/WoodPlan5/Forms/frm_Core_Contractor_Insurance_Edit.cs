using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_Core_Contractor_Insurance_Edit : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //


        #endregion

        public frm_Core_Contractor_Insurance_Edit()
        {
            InitializeComponent();
        }

        private void frm_Core_Contractor_Insurance_Edit_Load(object sender, EventArgs e)
        {
            this.FormID = 401;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter GetSetting = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp01015_Get_Linked_Document_Folder_Path(102).ToString();
            }
            catch (Exception) { }
            if (!strDefaultPath.EndsWith("\\")) strDefaultPath += "\\";

            // Populate Main Dataset //
            
            this.sp00247_Core_Team_Insurance_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.NewRow();
                        drNewRow["strMode"] = "add";
                        drNewRow["InsuranceID"] = 0;
                        drNewRow["TeamID"] = intLinkedToRecordID;
                        drNewRow["SubContractorName"] = strLinkedToRecordDesc;
                        drNewRow["GrittingPublicValue"] = 0.00;
                        drNewRow["GrittingEmployersValue"] = 0.00;
                        drNewRow["SnowClearancePublicValue"] = 0.00;
                        drNewRow["SnowClearanceEmployersValue"] = 0.00;
                        drNewRow["MaintenancePublicValue"] = 0.00;
                        drNewRow["MaintenanceEmployersValue"] = 0.00;
                        drNewRow["ConstructionPublicValue"] = 0.00;
                        drNewRow["ConstructionEmployersValue"] = 0.00;
                        drNewRow["AmenityArbPublicValue"] = 0.00;
                        drNewRow["AmenityArbEmployersValue"] = 0.00;
                        drNewRow["UtilityArbPublicValue"] = 0.00;
                        drNewRow["UtilityArbEmployersValue"] = 0.00;
                        drNewRow["RailArbPublicValue"] = 0.00;
                        drNewRow["RailArbEmployersValue"] = 0.00;
                        drNewRow["PestControlPublicValue"] = 0.00;
                        drNewRow["PestControlEmployersValue"] = 0.00;
                        drNewRow["FencingPublicValue"] = 0.00;
                        drNewRow["FencingEmployersValue"] = 0.00;
                        drNewRow["RoofingPublicValue"] = 0.00;
                        drNewRow["RoofingEmployersValue"] = 0.00;
                        drNewRow["WindowCleaningPublicValue"] = 0.00;
                        drNewRow["WindowCleaningEmployersValue"] = 0.00;
                        drNewRow["PotholePublicValue"] = 0.00;
                        drNewRow["potholeEmployersValue"] = 0.00;
                        this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["InsuranceID"] = 0;
                        drNewRow["TeamID"] = 0;
                        drNewRow["SubContractorName"] = strLinkedToRecordDesc;
                        drNewRow["GrittingPublicValue"] = 0.00;
                        drNewRow["GrittingEmployersValue"] = 0.00;
                        drNewRow["SnowClearancePublicValue"] = 0.00;
                        drNewRow["SnowClearanceEmployersValue"] = 0.00;
                        drNewRow["MaintenancePublicValue"] = 0.00;
                        drNewRow["MaintenanceEmployersValue"] = 0.00;
                        drNewRow["ConstructionPublicValue"] = 0.00;
                        drNewRow["ConstructionEmployersValue"] = 0.00;
                        drNewRow["AmenityArbPublicValue"] = 0.00;
                        drNewRow["AmenityArbEmployersValue"] = 0.00;
                        drNewRow["UtilityArbPublicValue"] = 0.00;
                        drNewRow["UtilityArbEmployersValue"] = 0.00;
                        drNewRow["RailArbPublicValue"] = 0.00;
                        drNewRow["RailArbEmployersValue"] = 0.00;
                        drNewRow["PestControlPublicValue"] = 0.00;
                        drNewRow["PestControlEmployersValue"] = 0.00;
                        drNewRow["FencingPublicValue"] = 0.00;
                        drNewRow["FencingEmployersValue"] = 0.00;
                        drNewRow["RoofingPublicValue"] = 0.00;
                        drNewRow["RoofingEmployersValue"] = 0.00;
                        drNewRow["WindowCleaningPublicValue"] = 0.00;
                        drNewRow["WindowCleaningEmployersValue"] = 0.00;
                        drNewRow["PotholePublicValue"] = 0.00;
                        drNewRow["potholeEmployersValue"] = 0.00;
                        this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["InsuranceID"] = 0;
                        drNewRow["TeamID"] = 0;
                        this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.Rows.Add(drNewRow);
                        this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp00247_Core_Team_Insurance_EditTableAdapter.Fill(this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Team Insurance", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        SubContractorNameButtonEdit.Focus();

                        SubContractorNameButtonEdit.Properties.ReadOnly = false;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        SubContractorNameButtonEdit.Focus();

                        SubContractorNameButtonEdit.Properties.ReadOnly = false;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        //DescriptionTextEdit.Focus();

                        SubContractorNameButtonEdit.Properties.ReadOnly = false;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        //DescriptionTextEdit.Focus();

                        SubContractorNameButtonEdit.Properties.ReadOnly = true;
                        SubContractorNameButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            
            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.woodPlanDataSet.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp00247CoreTeamInsuranceEditBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["InsuranceID"] == null ? 0 : Convert.ToInt32(currentRow["InsuranceID"]));
            }
            
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_Core_Contractor_Insurance_Edit_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_Core_Contractor_Insurance_Edit_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp00247CoreTeamInsuranceEditBindingSource.EndEdit();
            try
            {
                this.sp00247_Core_Team_Insurance_EditTableAdapter.Update(woodPlanDataSet);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp00247CoreTeamInsuranceEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["InsuranceID"]) + ";";
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp00247CoreTeamInsuranceEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                if (Application.OpenForms[strCaller] != null)
                {
                    (Application.OpenForms[strCaller] as frm_Core_Contractor_Manager).UpdateFormRefreshStatus(2, 17, strNewIDs);
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.Rows.Count; i++)
            {
                switch (this.woodPlanDataSet.sp00247_Core_Team_Insurance_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void SubContractorNameButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;

            DataRowView currentRow = (DataRowView)sp00247CoreTeamInsuranceEditBindingSource.Current;
            if (currentRow == null) return;
            var fChildForm1 = new frm_HR_Select_Contractor();
            fChildForm1.GlobalSettings = this.GlobalSettings;
            fChildForm1._Mode = (strFormMode == "blockadd" ? "multiple" : "single");
            if (strFormMode != "blockadd")
            {
                fChildForm1.intOriginalContractorID = Convert.ToInt32(currentRow["TeamID"]);
            }
            else  // blockadd //
            {
                fChildForm1.strOriginalContractorIDs = currentRow["strRecordIDs"].ToString();
            }
            if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
            {
                if (strFormMode != "blockadd")
                {
                    currentRow["TeamID"] = fChildForm1.intSelectedContractorID;
                    currentRow["SubContractorName"] = fChildForm1.strSelectedContractorName;
                }
                else  // blockadd //
                {
                    currentRow["strRecordIDs"] = fChildForm1.strSelectedContractorIDs;
                    currentRow["SubContractorName"] = "Block Adding To: " + fChildForm1.strSelectedContractorName;
                }
            }
        }
        private void SubContractorNameButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(SubContractorNameButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SubContractorNameButtonEdit, "");
            }
        }
 
        private void StartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;

            bool boolStartDateValid = false;
            bool boolEndDateValid = false;
            ValidateDates(de.DateTime, EndDateDateEdit.DateTime, ref boolStartDateValid, ref boolEndDateValid);
        }

        private void EndDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode == "blockedit" || this.strFormMode == "view") return;

            bool boolStartDateValid = false;
            bool boolEndDateValid = false;
            ValidateDates(StartDateDateEdit.DateTime, de.DateTime, ref boolStartDateValid, ref boolEndDateValid);
        }

        private void ValidateDates(DateTime? dtStart, DateTime? dtEnd, ref bool boolStartValid, ref bool boolEndValid)
        {
            if (dtStart > dtEnd)
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "End Date should be greater than Start Date.");
                dxErrorProvider1.SetError(EndDateDateEdit,  "End Date should be greater than Start Date.");
                boolStartValid = false;
                boolEndValid = false;
                return;
            }
            if ((dtStart == null || string.IsNullOrEmpty(dtStart.ToString())) || dtStart == DateTime.MinValue)
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "Select\\enter a value.");
                boolStartValid = false;
            }
            else
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "");
                boolStartValid = true;
            }
            if ((dtEnd == null || string.IsNullOrEmpty(dtEnd.ToString())) || dtEnd == DateTime.MinValue)
            {
                dxErrorProvider1.SetError(EndDateDateEdit, "Select\\enter a value.");
                boolEndValid = false;
            }
            else
            {
                dxErrorProvider1.SetError(EndDateDateEdit, "");
                boolEndValid = true;
            }
            return;
        }

        private void LinkedCertificateButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DataRowView currentRow = (DataRowView)sp00247CoreTeamInsuranceEditBindingSource.Current;
            if (currentRow == null) return;
            string strLinkedDocumentRecordTypePath = strDefaultPath;
            if (string.IsNullOrWhiteSpace(strLinkedDocumentRecordTypePath)) strLinkedDocumentRecordTypePath = strLinkedDocumentRecordTypePath.ToLower();
            switch (e.Button.Tag.ToString())
            {
                case "select":
                    {
                        using (OpenFileDialog dlg = new OpenFileDialog())
                        {
                            dlg.DefaultExt = "pdf";
                            dlg.Filter = "";
                            if (strLinkedDocumentRecordTypePath != "") dlg.InitialDirectory = strLinkedDocumentRecordTypePath;
                            dlg.Multiselect = false;
                            dlg.ShowDialog();
                            if (dlg.FileNames.Length > 0)
                            {
                                string strTempFileName = "";
                                string strTempResult = "";
                                string strExtension = "";


                                foreach (string filename in dlg.FileNames)
                                {
                                    if (strLinkedDocumentRecordTypePath != "")
                                    {
                                        if (!filename.ToLower().StartsWith(strLinkedDocumentRecordTypePath.ToLower()))
                                        {
                                            DevExpress.XtraEditors.XtraMessageBox.Show("You can only select files from under the default folder [as specified in the Linked Document Record Type table] or one of it's sub-folders.\n\nFile Selection Aborted!", "Select File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                                            break;
                                        }
                                    }
                                    strTempFileName = filename.Substring(strLinkedDocumentRecordTypePath.Length);
                                    if (strTempResult == "")
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += strTempFileName;
                                    }
                                    else
                                    {
                                        if (!strTempResult.Contains(strTempFileName)) strTempResult += "\r\n" + strTempFileName;
                                    }
                                }
                                LinkedCertificateButtonEdit.Text = strTempResult;
                            }
                            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter so linked file can be viewed if required //
                        }
                    }
                    break;
                case "view":
                    {
                        if (currentRow == null) return;
                        string strFile = currentRow["LinkedCertificate"].ToString();

                        if (string.IsNullOrEmpty(strFile))
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Link the File to be Viewed before proceeding.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        try
                        {
                            if (!strFile.ToLower().EndsWith("pdf"))
                            {
                                System.Diagnostics.Process.Start(Path.Combine(strLinkedDocumentRecordTypePath + strFile));
                            }
                            else
                            {
                                if (!BaseObjects.Keyboard.IsKeyDown(Keys.A))  // Only open in DevExpress PDF viewer if user is not holding down A key (CTRL Key doesn't work when fired from a grid as it's used for row selection) //
                                {
                                    frm_Core_PDF_Viewer fChildForm = new frm_Core_PDF_Viewer();
                                    fChildForm.strPDFFile = Path.Combine(strLinkedDocumentRecordTypePath + strFile);
                                    fChildForm.MdiParent = this.MdiParent;
                                    fChildForm.GlobalSettings = this.GlobalSettings;
                                    fChildForm.Show();
                                }
                                else
                                {
                                    System.Diagnostics.Process.Start(Path.Combine(strLinkedDocumentRecordTypePath + strFile));
                                }
                            }
                        }
                        catch
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view file: " + strLinkedDocumentRecordTypePath + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked File", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    break;
            }
        }

        #endregion






    }
}


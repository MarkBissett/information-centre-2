using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using System.Collections.Specialized;
using DevExpress.XtraSplashScreen;
using System.Linq;

namespace WoodPlan5
{
    public partial class frm_Core_Package_Configuration_Manager : BaseObjects.frmBase
    {

        private void frm_Core_Package_Configuration_Manager_Load(object sender, EventArgs e)
        {
            if (fProgress != null)
                fProgress.UpdateProgress(20); // Update Progress Bar //

            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 9227;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            LoadConnectionStrings(this.GlobalSettings.ConnectionString);
            LoadAdapters();
            // Get Form Permissions //            
            ProcessPermissionsForForm();
            if (fProgress != null)
                fProgress.UpdateProgress(20); // Update Progress Bar //

            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //

            RefreshGridViewStatePackageConfiguration = new RefreshGridState(packageConfigurationGridView, "JobPackageID");

            RefreshGridViewStateRecipientList = new RefreshGridState(recipientListGridView, "ServiceEmailListID");

         

            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //


            if (strPassedInRecordDs != "")  // Opened in drill-down mode //
            {
                Load_Data();  // Load records //
            }

            if (fProgress != null)
                fProgress.UpdateProgress(10); // Update Progress Bar //
            emptyEditor = new RepositoryItem();

        }
        
        #region Instance Variables...

        private Boolean iboolDistinctMulitpleChildSelected = false;
        private int iDistinctSelectedType = 0;
        private Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private bool isRunning = false;
        GridHitInfo downHitInfo = null;
        public string strPassedInRecordDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //
        public string strPackageConfigurationRecordIDs = "";
        public string strRecipientListRecordIDs = "";

        public string strRecordsToLoad = "";
        public int numOfSelectedRows = 0;
        public int mainViewRowCount = 0;
        private string strMessage1, strMessage2, strMessage3;

        private bool iBool_AllowDelete = false;
        private bool iBool_AllowAdd = false;
        private bool iBool_AllowEdit = false;

        private bool iBool_AllowAddRecipientList = false;
        private bool iBool_AllowEditRecipientList = false;
        private bool iBool_AllowDeleteRecipientList = false;
                     
        int i_int_FocusedGrid = 0;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //
        
        
        // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStatePackageConfiguration; 
        public RefreshGridState RefreshGridViewStateRecipientList;


        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        private string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs2 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        private string i_str_AddedRecordIDs3 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        
        private enum FormType { child, parent };
        private enum GridType {child, parent };
        private enum FormMode { add, edit, view, delete, blockadd, blockedit };
        private enum PackageConfigurationType { PackageConfiguration, RecipientList = 1}

        #endregion

        #region Constructor and compulsory implementation

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            getCurrentView(out view);

            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            toggleMenuButtons(alItems,intRowHandles);

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of Package ConfigurationView navigator custom buttons //
            getCRUDButtonPermissions(packageConfigurationGridControl, iBool_AllowAdd, iBool_AllowEdit, iBool_AllowDelete);

            // Set enabled status of Package Recipient Details GridControl navigator custom buttons //
            getCRUDButtonPermissions(recipientListGridControl, iBool_AllowAddRecipientList, iBool_AllowEditRecipientList, iBool_AllowDeleteRecipientList);
                      
        }

        private void toggleMenuButtons(ArrayList alItems, int[] intRowHandles)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Billing_Centre_Code
                                   
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    bbiBlockAdd.Enabled = false;
                }
                
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    bbiBlockEdit.Enabled = false;                  
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
                break;

                case 1://Package Recipient Details
                getChildMouseMenuPermissions(alItems,intRowHandles, iBool_AllowAddRecipientList, iBool_AllowEditRecipientList, iBool_AllowDeleteRecipientList);
                break;
            }
        }

        private void getCRUDButtonPermissions(GridControl GridControlX,bool iBool_AllowAddX, bool iBool_AllowEditX, bool iBool_AllowDeleteX)
        {
            GridControl xGridControl = null;
            xGridControl = GridControlX;
            GridView view = null;
            view = (GridView)xGridControl.MainView;
            int[] intRowHandles = view.GetSelectedRows();

            if (iBool_AllowAddX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
            }
            if (iBool_AllowEditX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
            }
            if (iBool_AllowDeleteX)
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0 ? true : false);
            }
            else
            {
                xGridControl.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = false;
            }
        }

        private void getChildMouseMenuPermissions(ArrayList alItems, int[] intRowHandles, bool iBool_AllowAddChild, bool iBool_AllowEditChild, bool iBool_AllowDeleteChild)
        {
            if (iBool_AllowAddChild)
            {
                alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                bsiAdd.Enabled = true;
                bbiSingleAdd.Enabled = true;
                bbiBlockAdd.Enabled = false;
              
            }
            if (iBool_AllowEditChild && intRowHandles.Length >= 1)
            {
                alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                bsiEdit.Enabled = true;
                bbiSingleEdit.Enabled = true;
                bbiBlockEdit.Enabled = false;
            }
            if (iBool_AllowDeleteChild && intRowHandles.Length >= 1)
            {
                alItems.Add("iDelete");
                bbiDelete.Enabled = true;
            }
        }

        private void getCurrentView(out GridView view)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://packageConfiguration
                    view = (GridView)packageConfigurationGridControl.MainView;
                    break;
                case 1://Package Recipient Details
                    view = (GridView)recipientListGridControl.MainView;
                    break;
              
                default://packageConfiguration
                    view = (GridView)packageConfigurationGridControl.MainView;
                    break;
            }
     
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // RecipientList //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddRecipientList = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditRecipientList = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteRecipientList = true;
                        }
                        break;
                }
            }
        }

        public frm_Core_Package_Configuration_Manager()
        {
            InitializeComponent();
        }

        #endregion


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "packageConfigurationGridView":
                    message = "No Job Packages Information Available";
                    break;
                case "recipientListGridView":
                    message = "No Recipient List Information Available";
                    break;          
                default:
                    message = "No Details Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        #endregion


        #region Form_Events

        private void packageConfigurationView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning) return;
            isRunning = true;
            GridView View = sender as GridView;
            if (e.Action == CollectionChangeAction.Add && View.IsGroupRow(e.ControllerRow))
            {
                View.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && View.SelectedRowsCount > 0)
            {
                View.BeginSelection();
                foreach (int Row in View.GetSelectedRows())
                {
                    if (View.IsGroupRow(Row)) 
                        View.UnselectRow(Row);
                }
                View.EndSelection();
            }
            isRunning = false;
            
            updateSelectedJobPackageIDs();
            LoadLinkedRecords();
            expandChildGridView();
            SetMenuStatus();  
        }

        private void childGridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (isRunning)
                return;
            isRunning = true;
            GridView view = sender as GridView;
            childGridViewSelectionChanged(view, e);  
        }

        private void packageConfigurationView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void commonGridView_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                iBoolDontFireGridGotFocusOnDoubleClick = true;
                Edit_Record();
            }
        }

        private void commonGridView_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            string GridName = ((GridView)sender).Name;
            switch (GridName)
            {
                case "packageConfigurationGridView":
                    i_int_FocusedGrid = 0;
                    break;

                case "recipientListGridView":
                    i_int_FocusedGrid = 1;
                    break;
            }

            SetMenuStatus();
        }

        private void commonGridControl_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("blockadd".Equals(e.Button.Tag))
                    {
                        Block_Add();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void commonGridView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void LoadAdapters()
        {
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            sp_PC_922701_Package_Configuration_ItemTableAdapter.Fill(dataSet_PC_Core.sp_PC_922701_Package_Configuration_Item, "", "view");
        }

        private void LoadConnectionStrings(string ConnString)
        {
            strConnectionString = ConnString;
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_PC_922701_Package_Configuration_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_PC_922703_Package_Email_List_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_PC_922709_Return_ServiceEmailListIDTableAdapter.Connection.ConnectionString = strConnectionString;
        }



        private void frm_Core_Package_Configuration_Manager_Activated(object sender, EventArgs e)
        {
            frmActivated();
        }
        #endregion

        #region Form_Functions

        public void frmActivated()
        {
            if (UpdateRefreshStatus > 0 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
            {
                Load_Data();
            }
            SetMenuStatus(); 
        }

        private void updateSelectedJobPackageIDs()
        {

            GridControl gridControl = null;
            switch(i_int_FocusedGrid)
            {
                case 0:
                    gridControl = packageConfigurationGridControl;
                    break;
                case 1:
                    gridControl = recipientListGridControl;
                    break;
                default:
                    gridControl = packageConfigurationGridControl;
                    break;
            }
            
            GridView view = (GridView)gridControl.MainView;
            numOfSelectedRows = view.SelectedRowsCount;

            if (i_int_FocusedGrid == 0)
            {
                mainViewRowCount = view.SelectedRowsCount;
            }
            else
            {
                mainViewRowCount = 0;
            }

            int tempNumOfSelectedRows;
            if (numOfSelectedRows > 0)
            {
                tempNumOfSelectedRows = numOfSelectedRows;
            }
            else
            {
                tempNumOfSelectedRows = 1;
            }
            int[] JobPackageIDs = new int[tempNumOfSelectedRows];
            int[] ServiceRecipientListIDs = new int[tempNumOfSelectedRows];
            
            int[] intRowHandles = view.GetSelectedRows();
            if (view.SelectedRowsCount > 0)
            {
                int countRows = 0;
                string strServiceEmailListIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    DataRow dr = view.GetDataRow(intRowHandle);
                    switch (i_int_FocusedGrid)
                    { 
                        case 0://Package Configuration
                            JobPackageIDs[countRows] = (((DataSet_PC_Core.sp_PC_922701_Package_Configuration_ItemRow)(dr)).JobPackageID);
                            string strJobPackageIDs = string.Join(",", JobPackageIDs);
                            strServiceEmailListIDs = returnServices(strJobPackageIDs);
                            countRows += 1;
                            strPackageConfigurationRecordIDs = stringRecords(JobPackageIDs);
                            strRecipientListRecordIDs = strServiceEmailListIDs;
                            break;
                        case 1://Recipient List
                            ServiceRecipientListIDs[countRows] = (((DataSet_PC_Core.sp_PC_922703_Package_Email_List_ItemRow)(dr)).ServiceEmailListID);
                            strRecipientListRecordIDs = stringRecords(ServiceRecipientListIDs);
                            countRows += 1;
                            break;
                    }
                    
                }
               
            }
        }

        

        public void LoadLinkedRecords(string strJobPackageID)
        {
            //recipientListTabPage
            recipientListGridControl.MainView.BeginUpdate();
            this.RefreshGridViewStateRecipientList.SaveViewInfo();  // Store expanded groups and selected rows //
            this.sp_PC_922703_Package_Email_List_ItemTableAdapter.Fill(this.dataSet_PC_Core.sp_PC_922703_Package_Email_List_Item, strJobPackageID, "jobid");
            this.RefreshGridViewStateRecipientList.LoadViewInfo();  // Reload any expanded groups and selected rows //

            recipientListGridControl.MainView.EndUpdate();
        }

        public void LoadLinkedRecords()
        {
            //recipientListTabPage
            recipientListGridControl.MainView.BeginUpdate();
            this.RefreshGridViewStateRecipientList.SaveViewInfo();  // Store expanded groups and selected rows //
            this.sp_PC_922703_Package_Email_List_ItemTableAdapter.Fill(this.dataSet_PC_Core.sp_PC_922703_Package_Email_List_Item, (string.IsNullOrEmpty(strRecipientListRecordIDs) ? "" : strRecipientListRecordIDs), "");
            this.sp_PC_922709_Return_ServiceEmailListIDTableAdapter.Fill(dataSet_PC_Core.sp_PC_922709_Return_ServiceEmailListID, "");
            this.RefreshGridViewStateRecipientList.LoadViewInfo();  // Reload any expanded groups and selected rows //

            recipientListGridControl.MainView.EndUpdate();                  
        }

        private void childGridViewSelectionChanged(GridView view, DevExpress.Data.SelectionChangedEventArgs e)
        {

            if (e.Action == CollectionChangeAction.Add && view.IsGroupRow(e.ControllerRow))
            {
                view.UnselectRow(e.ControllerRow);
            }
            if (e.Action == CollectionChangeAction.Refresh && view.SelectedRowsCount > 0)
            {
                view.BeginSelection();
                foreach (int Row in view.GetSelectedRows())
                {
                    if (view.IsGroupRow(Row)) view.UnselectRow(Row);
                }
                view.EndSelection();
            }
            isRunning = false;
            SetMenuStatus();
            updateSelectedJobPackageIDs();
            
        }

        private void OpenEditForm(FormMode mode, string frmCaller, GridControl gridControl,PackageConfigurationType packageConfigurationType)
        {

            if (mode == FormMode.blockadd || mode == FormMode.blockedit)
            {
                XtraMessageBox.Show("Block edit/add is not available.", "Block Edit/Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView view = null;
            //frmProgress fProgress = null;
            strRecordsToLoad = "";
            int[] intRowHandles;
            int intCount = 0;
            saveGridViewState();
            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (mode == FormMode.edit)
            {
                if (intCount <= 0)
                {
                    XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            updateSelectedJobPackageIDs();
                    
            switch (i_int_FocusedGrid)
            {
                case 0:     // Package Configuration
                    frm_Core_Package_Configuration_Edit fChildForm = new frm_Core_Package_Configuration_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strPackageConfigurationRecordIDs;
                    fChildForm.strPassedServiceEmailListIDs = strRecipientListRecordIDs;
                    fChildForm.formMode = (frm_Core_Package_Configuration_Edit.FormMode)mode;
                    fChildForm.strFormMode = (mode.ToString()).ToLower();
                    fChildForm.strCaller = frmCaller;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;                    
                    SplashScreenManager splashScreenManager1 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();
                    break;
                case 1:     // Recipient List
                    frm_Core_Package_Recipient_Details_Edit cChildForm = new frm_Core_Package_Recipient_Details_Edit();
                    cChildForm.GlobalSettings = this.GlobalSettings;
                    cChildForm.strPackageConfigurationRecordIDs = strPackageConfigurationRecordIDs;
                    cChildForm.strRecordIDs = strRecipientListRecordIDs;
                    cChildForm.strFormMode = (mode.ToString()).ToLower();
                    cChildForm.formMode = (frm_Core_Package_Recipient_Details_Edit.FormMode)mode;
                    cChildForm.strCaller = frmCaller;
                    cChildForm.intRecordCount = intCount;
                    cChildForm.FormPermissions = this.FormPermissions;                    
                    SplashScreenManager splashScreenManager2 = new SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    cChildForm.splashScreenManager = splashScreenManager2;
                    cChildForm.splashScreenManager.ShowWaitForm();
                    cChildForm.ShowDialog();
                    break;
                default:
                    MessageBox.Show("Failed to load edit form", "Error Loading Edit form", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void saveGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStatePackageConfiguration.SaveViewInfo();  
            this.RefreshGridViewStateRecipientList.SaveViewInfo(); 
        }

        private void loadGridViewState()
        {
            // Store Grid View State //
            this.RefreshGridViewStatePackageConfiguration.LoadViewInfo();
            this.RefreshGridViewStateRecipientList.LoadViewInfo();
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1, string strNewIDs2, string strNewIDs3)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
            if (strNewIDs2 != "") i_str_AddedRecordIDs2 = strNewIDs2;
            if (strNewIDs3 != "") i_str_AddedRecordIDs3 = strNewIDs3;
        }
     
        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                //LoadLastSavedUserScreenSettings();
            }
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0)
                UpdateRefreshStatus = 0;
            packageConfigurationGridControl.BeginUpdate();
            sp_PC_922701_Package_Configuration_ItemTableAdapter.Fill(dataSet_PC_Core.sp_PC_922701_Package_Configuration_Item, "", "view");
            loadGridViewState();
            packageConfigurationGridControl.EndUpdate();

            // Highlight any recently added rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                SetGridViewSelection((GridView)packageConfigurationGridControl.MainView, "JobPackageID", i_str_AddedRecordIDs1);
                //if (i_str_AddedRecordIDs2 != "")
                //{
                //    LoadLinkedRecords(i_str_AddedRecordIDs1);
                //}
                i_str_AddedRecordIDs1 = "";
            }
            if (i_str_AddedRecordIDs2 != "")
            {
                SetGridViewSelection((GridView)recipientListGridControl.MainView, "ServiceEmailListID", i_str_AddedRecordIDs2);
                i_str_AddedRecordIDs2 = "";
                //highlightChildrow();
            }
        }
        private void SetGridViewSelection(GridView view, string columnName, string recordIDs)
        {
            string[] strArray = splitStrRecords(recordIDs);
            int intID = 0;
            int intRowHandle = 0;
            view.ClearSelection(); // Clear any current selection so just the required rows are selected //
            //Begin/End Selectoin Block improves performance.
            
            if (columnName == "JobPackageID")
            {
                LoadLinkedRecords(recordIDs);
                
            }
            else
            {
                LoadLinkedRecords(returnJobs(recordIDs));                
             }
            view.BeginSelection();
            foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns[columnName], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.MakeRowVisible(intRowHandle, false);
                        //view.SelectRow(intRowHandle);
                    }
                }
                      
            view.EndSelection();
        }
        private string returnServices(string strJobPackageIDs)
        {
            sp_PC_922709_Return_ServiceEmailListIDTableAdapter.Fill(dataSet_PC_Core.sp_PC_922709_Return_ServiceEmailListID, strJobPackageIDs);
            int[] result = new int[dataSet_PC_Core.sp_PC_922709_Return_ServiceEmailListID.Rows.Count];
            result = dataSet_PC_Core.sp_PC_922709_Return_ServiceEmailListID.AsEnumerable().Select(r => r.Field<int>("ServiceEmailListID")).ToArray();
            return string.Join(",", result);
        }
        private string returnJobs(string recordIDs)
        {
            sp_PC_922710_Return_JobPackageIDTableAdapter.Fill(dataSet_PC_Core.sp_PC_922710_Return_JobPackageID, recordIDs);
            int[] result = new int[dataSet_PC_Core.sp_PC_922710_Return_JobPackageID.Rows.Count];
            result = dataSet_PC_Core.sp_PC_922710_Return_JobPackageID.AsEnumerable().Select(r => r.Field<int>("JobPackageID")).ToArray();
            return string.Join(",", result);
        }

        private string[] splitStrRecords(string newIDs)
        {
            string[] parts;
            char[] delimiters = new char[] { ';', ',' };
            if (newIDs == "")
            {
                parts = strPackageConfigurationRecordIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                parts = newIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            }

            return parts;
        }

        private string stringRecords(int[] IDs)
        {
            string strIDs = "";
            foreach (int rec in IDs)
            {
                strIDs += Convert.ToString(rec) + ',';
            }
            return strIDs;
        }

        private void expandChildGridView()
        {
            GridView view = (GridView)recipientListGridControl.MainView;
            view.ExpandAllGroups();            
        }

        #endregion
        
        
        #region CRUD

        #region CRUD Methods
           
        private void Add_Record()
        {
           
            switch (i_int_FocusedGrid)
            {
                case 0:     // Package Configuration //
                    if (!iBool_AllowAdd)
                        return;
                    OpenEditForm(FormMode.add, "frm_Core_Package_Configuration_Manager", packageConfigurationGridControl,PackageConfigurationType.PackageConfiguration);
                    break;
                case 1:     // Package Recipient Details //
                    if (!iBool_AllowAddRecipientList) 
                        return;
                    OpenEditForm(FormMode.add, "frm_Core_Package_Configuration_Manager", recipientListGridControl,PackageConfigurationType.RecipientList);                  
                    break;
            }
        }

        private void Block_Add()
        {
            XtraMessageBox.Show("Block adding is currently restricted the function is under construction.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void Block_Edit()
        {
            XtraMessageBox.Show("Block adding is restricted.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        }

        private void Edit_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Package Configuration //
                    if (!iBool_AllowEdit)
                        return;
                    OpenEditForm(FormMode.edit, "frm_Core_Package_Configuration_Manager", packageConfigurationGridControl, PackageConfigurationType.PackageConfiguration);
                    break;
                case 1:     // Package Recipient Details //
                    if (!iBool_AllowEditRecipientList)
                        return;
                    OpenEditForm(FormMode.edit, "frm_Core_Package_Configuration_Manager", recipientListGridControl, PackageConfigurationType.RecipientList);
                    break;
                default:
                    break;
            }
            
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridControl gridControl = null;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 0:     // Package Configuration
                    if (!iBool_AllowDelete)
                        return;
                    break;
                case 1:     // RecipientList
                    if (!iBool_AllowDeleteRecipientList)
                        return;
                    break;
                default:
                    if (!iBool_AllowDelete)
                        return;
                    break;
            }

            getCurrentGridControl(out gridControl, out strMessage1, out strMessage2, out strMessage3);
            view = (GridView)gridControl.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
                                        
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more records to delete.", "No Records To Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? strMessage1 : Convert.ToString(intRowHandles.Length) + strMessage2) + 
            " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this record" : "these records") + 
            " will no longer be available for selection and any related records will also be deleted!";
            if (XtraMessageBox.Show(strMessage, "Permanently Delete Record(s)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                frmProgress fProgress = new frmProgress(20);
                fProgress.UpdateCaption("Deleting...");
                fProgress.Show();
                Application.DoEvents();

                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

                this.RefreshGridViewStatePackageConfiguration.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                string jobsWithChildrenToBeDeleted = returnJobs(strRecipientListRecordIDs);
                this.RefreshGridViewStatePackageConfiguration.SaveViewInfo();  // Store Grid View State //
                try
                {
                    switch (i_int_FocusedGrid)
                    {
                        case 0:     // Package Configuration 
                            sp_PC_922701_Package_Configuration_ItemTableAdapter.Delete("delete", strPackageConfigurationRecordIDs);
                            break;
                        case 1:     // Recipient List
                            sp_PC_922703_Package_Email_List_ItemTableAdapter.Delete("delete", strRecipientListRecordIDs);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message, "Error deleting", MessageBoxButtons.OK, MessageBoxIcon.Error);           
                }
                if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //
                Load_Data();

                if (fProgress != null)
                {
                    fProgress.UpdateProgress(20); // Update Progress Bar //
                    fProgress.Close();
                    fProgress = null;
                }
                if (this.GlobalSettings.ShowConfirmations == 1)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                switch (i_int_FocusedGrid)
                {
                    case 0:     // Package Configuration 
                        packageConfigurationGridView.SelectRow(0);
                        break;
                    case 1:     // Recipient List
                        SetGridViewSelection(recipientListGridView, "ServiceEmailListID",returnServices(jobsWithChildrenToBeDeleted));                        
                        break;
                }              
            }
        }
       
        private void getCurrentGridControl(out GridControl gridControl, out string strMessage1, out string strMessage2, out string strMessage3)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Package Configuration
                    gridControl = packageConfigurationGridControl;                    
                    strMessage1 = "1 Package Configuration record";
                    strMessage2 = " Package Configuration records";
                    strMessage3 = "";
                    break;
                case 1://Package Recipient Details
                    gridControl = recipientListGridControl;                  
                    strMessage1 = "1 Recipient record";
                    strMessage2 = " Recipient records";
                    strMessage3 = "";
                    break;
                default:
                    gridControl = packageConfigurationGridControl;
                    strMessage1 = "1 Package Configuration record";
                    strMessage2 = " Package Configuration records";
                    strMessage3 = "";
                    break;
            }
        }

        private void getCurrentGridControl(out GridControl gridControl)
        {
            switch (i_int_FocusedGrid)
            {
                case 0://Package Configuration
                    gridControl = packageConfigurationGridControl;
                    break;
                case 1://Package Recipient Details
                    gridControl = recipientListGridControl;
                    break;
                default:
                    gridControl = packageConfigurationGridControl;
                    break;
            }
        }

        private void View_Record()
        {
            switch (i_int_FocusedGrid)
            {
                case 0:     // Package Configuration //
                    OpenEditForm(FormMode.view, "frm_Core_Package_Configuration_Manager", packageConfigurationGridControl, PackageConfigurationType.PackageConfiguration);
                    break;
                case 1:     // Package Recipient Details //
                    if (!iBool_AllowEditRecipientList)
                        return;
                    OpenEditForm(FormMode.edit, "frm_Core_Package_Configuration_Manager", recipientListGridControl, PackageConfigurationType.RecipientList);
                    break;
            }
            
        }

        #endregion

        #region CRUD Events


        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        #endregion

        #endregion




    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.Utils;

namespace WoodPlan5
{
    public partial class frmGroupManagerEditGroup : BaseObjects.frmBase
    {
        #region Instance Variables
      
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public WaitDialogForm loadingForm;
        public bool boolAllowEdit = false;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;

        #endregion

        public frmGroupManagerEditGroup()
        {
            InitializeComponent();
        }

        private void frmGroupManagerEditGroup_Load(object sender, EventArgs e)
        {
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp00078_permission_group_editTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00079_permission_group_type_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00079_permission_group_type_listTableAdapter.Fill(this.dataSet_Groups.sp00079_permission_group_type_list);
            String strTopFormCaption = "";
            DataRow drNewRow;
            switch (strFormMode)
            {
                case "add":
                    dataLayoutControl1.HideItem(layoutControlBlockEditWarningIcon);
                    dataLayoutControl1.HideItem(layoutControlBlockEditWarningLabel);
                    strTopFormCaption = "Add Permission Group";
                    drNewRow = this.dataSet_Groups.sp00078_permission_group_edit.NewRow();
                    this.dataSet_Groups.sp00078_permission_group_edit.Rows.Add(drNewRow);
                    break;
                case "edit":
                    dataLayoutControl1.HideItem(layoutControlBlockEditWarningIcon);
                    dataLayoutControl1.HideItem(layoutControlBlockEditWarningLabel);
                    strTopFormCaption = "Edit " + Convert.ToString(intRecordCount);
                    if (intRecordCount == 1)
                    {
                        strTopFormCaption += " Permission Group";
                    }
                    else
                    {
                        strTopFormCaption += " Permission Groups";
                    }
                    sp00078_permission_group_editTableAdapter.Fill(this.dataSet_Groups.sp00078_permission_group_edit, strRecordIDs);
                    break;

                case "blockedit":
                    strTopFormCaption = "Block Edit " + Convert.ToString(intRecordCount) + " Permission Groups";
                    drNewRow = this.dataSet_Groups.sp00078_permission_group_edit.NewRow();
                    this.dataSet_Groups.sp00078_permission_group_edit.Rows.Add(drNewRow);
                    GroupDescriptionTextEdit.Enabled = false;
                    lcEditCount.ForeColor = Color.Red;
                    break;
                default:
                    break;
            }

            lcEditCount.Text = strTopFormCaption;
            if (loadingForm != null) loadingForm.Close();
        }

        private void frmGroupManagerEditGroup_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frmGroupManagerEditGroup_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        this.dataSet_Groups.sp00078_permission_group_edit.Rows.Clear();
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        //if (SaveChanges(false) != 1)
                        if (SaveChanges(true) != 1)
                        {
                            e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            if (boolAllowEdit)
            {
                alItems.Add("iSave");
                bbiSave.Enabled = true;
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.CancelEdit)
            {
                e.Handled = true;
                this.dataSet_Groups.sp00078_permission_group_edit.Rows.Clear();  // Abort any changes //
                this.Close();
            }
            else if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.EndEdit)
            {
                e.Handled = true;
                if (SaveChanges(false) == 1) this.Close();
            }
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Commit any outstanding changes to underlying dataset //
            //this.shippersTableAdapter.Update(this.nwindDataSet.Shippers);

            for (int i = 0; i < this.dataSet_Groups.sp00078_permission_group_edit.Rows.Count; i++)
            {
                switch (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (strFormMode == "blockedit")
                {
                    strMessage = "You are block editing " + Convert.ToString(intRecordCount) + " records on the current screen but you have not saved the block edit changes!\n";
                }
                else
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
            }
            return strMessage;
        }

        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private int SaveChanges(Boolean ib_SuccessMessage)
        {
            // Paramters - ib_SuccessMessage - determins if the success message box should be shown at the end of the event //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            // Check to ensure all required fields have values //
            int intErrorFound = 0;
            string strErrorMessage = "";
            int intDuplicateDescriptors = 0;
          
            for (int i = this.dataSet_Groups.sp00078_permission_group_edit.Rows.Count - 1; i >= 0; i--)
            {
                if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i].RowState == DataRowState.Added || this.dataSet_Groups.sp00078_permission_group_edit.Rows[i].RowState == DataRowState.Modified)
                {
                    if (strFormMode != "blockedit")
                    {
                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupTypeID"] == DBNull.Value)
                        {
                            intErrorFound = 1;
                            strErrorMessage += "Missing Group Type\n";
                        }
                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupDescription"] == DBNull.Value)
                        {
                            intErrorFound = 2;
                            strErrorMessage += "Missing Group Description\n";
                        }

                        // Check the Group Description is unique //
                        intDuplicateDescriptors = Convert.ToInt32(sp00078_permission_group_editTableAdapter.sp00081_permission_group_check_desc_unique(Convert.ToString(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupDescription"]), Convert.ToInt32(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupTypeID"]), Convert.ToInt32(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupID"])));
                        if (intDuplicateDescriptors > 0)
                        {
                            intErrorFound = 3;
                            strErrorMessage += "Duplicate Group Description - " + this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupDescription"].ToString() + "\n";
                        }

                    }
                    if (intErrorFound > 0)
                    {
                        break;
                    }
                }
            }
            if (intErrorFound > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The following errors are present...\n\n" + strErrorMessage + "\nPlease fix then try re-saving.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return 0;
            }

            // Checks passed successfully so OK to proceed with save //
            if (ib_SuccessMessage)
            {
                loadingForm = new WaitDialogForm("Saving Changes...", "Edit");
                loadingForm.Show();
            }

            Boolean boolSaveSuccessfull = false;
            int intSaveFailCount = 0;

            int? intGroupID = null;
            int? intGroupTypeID = null;
            string strGroupDescription = null;
            string strGroupRemarks = null;

            for (int i = this.dataSet_Groups.sp00078_permission_group_edit.Rows.Count - 1; i >= 0; i--)
            {
                switch (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i].RowState)
                {
                    case DataRowState.Added:  // Handles Add and Block Edit via the strFormMode passed in the parameter string to the SP //
                        boolSaveSuccessfull = true;

                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupTypeID"] == DBNull.Value) intGroupTypeID = null;
                        else intGroupTypeID = Convert.ToInt32(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupTypeID"]);

                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupTypeID"] == DBNull.Value) intGroupTypeID = null;
                        else intGroupTypeID = Convert.ToInt32(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupTypeID"]);

                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupDescription"] == DBNull.Value) strGroupDescription = null;
                        else strGroupDescription = Convert.ToString(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupDescription"]);

                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupRemarks"] == DBNull.Value) strGroupRemarks = null;
                        else strGroupRemarks = Convert.ToString(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupRemarks"]);

                        try
                        {
                            intGroupID = Convert.ToInt32(sp00078_permission_group_editTableAdapter.sp00080_permission_group_save_changes(strFormMode, 0, strRecordIDs, intGroupTypeID, strGroupDescription, strGroupRemarks, this.GlobalSettings.UserID));
                        }
                        catch (Exception Ex)
                        {
                            boolSaveSuccessfull = false;
                            intSaveFailCount++;
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while saving. Please try again.\n\nException:" + Ex.Message, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        if (boolSaveSuccessfull && strFormMode == "add")  // Update Data on front end with new ID [adding only , not block editing] //
                        {
                            this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupID"] = intGroupID;
                            strFormMode = "edit";  // Switch form mode so that record is not added on any subsequent saves //
                        }
                        break;

                    case DataRowState.Modified:
                        boolSaveSuccessfull = true;
                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupID"] == DBNull.Value) intGroupID = null;
                        else intGroupID = Convert.ToInt32(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupID"]);

                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupTypeID"] == DBNull.Value) intGroupTypeID = null;
                        else intGroupTypeID = Convert.ToInt32(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupTypeID"]);

                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupDescription"] == DBNull.Value) strGroupDescription = null;
                        else strGroupDescription = Convert.ToString(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupDescription"]);

                        if (this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupRemarks"] == DBNull.Value) strGroupRemarks = null;
                        else strGroupRemarks = Convert.ToString(this.dataSet_Groups.sp00078_permission_group_edit.Rows[i]["GroupRemarks"]);

                        try
                        {
                            sp00078_permission_group_editTableAdapter.sp00080_permission_group_save_changes(strFormMode, intGroupID, "", intGroupTypeID, strGroupDescription, strGroupRemarks, this.GlobalSettings.UserID);
                        }
                        catch (Exception Ex)
                        {
                            boolSaveSuccessfull = false;
                            intSaveFailCount++;
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while saving. Please try again.\n\nException:" + Ex.Message, "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                        break;
                }
                if (boolSaveSuccessfull)
                {
                    this.dataSet_Groups.sp00078_permission_group_edit.Rows[i].AcceptChanges();  // Set row status to Unchanged so that update is not fired again unless further changes are made //
                }
            }
            if (boolSaveSuccessfull) // Set flag on parent form to update itself on activate //
            {
                if (this.ParentForm != null)
                {
                    foreach (Form frmChild in this.ParentForm.MdiChildren)
                    {
                        if (frmChild.Name == "frmGroupsManager")
                        {
                            var fGroupsManager = (frmGroupsManager)frmChild;
                            fGroupsManager.UpdateFormRefreshStatus(true);
                            break;
                        }
                    }
                }
            }
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                if (intSaveFailCount == 0)
                {
                    loadingForm.Caption = "Changes Saved";
                }
                else
                {
                    loadingForm.Caption = "Save failed on " + Convert.ToString(intSaveFailCount) + " record(s). Please try again.";
                }
                loadingForm.Close();  // Don't use the SetTimer Method on the form becuase it can interfer with the messagebox when a save is triggered just prior to running that process //
            }
            return 1;
        }



    }
}


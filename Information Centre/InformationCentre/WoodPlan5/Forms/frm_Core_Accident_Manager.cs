using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //
using System.Xml;
using System.Linq;
using System.Xml.Linq;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraBars.Docking;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;
using WoodPlan5.Classes.Operations;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_Core_Accident_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        bool iBool_AllowDeleteMasterSubTypes = false;
        bool iBool_AllowAddMasterSubTypes = false;
        bool iBool_AllowEditMasterSubTypes = false;

        bool iBool_AllowDeleteMasterBodyAreas = false;
        bool iBool_AllowAddMasterBodyAreas = false;
        bool iBool_AllowEditMasterBodyAreas = false;

        bool iBool_AllowDeleteMasterInjuryTypes = false;
        bool iBool_AllowAddMasterInjuryTypes = false;
        bool iBool_AllowEditMasterInjuryTypes = false;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateAccident;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateInjury;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateComment;
        public RefreshGridState RefreshGridViewStatePicture;
        public RefreshGridState RefreshGridViewStateSubType;
        public RefreshGridState RefreshGridViewStateBodyArea;
        public RefreshGridState RefreshGridViewStateInjuryType;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDsAccident = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsInjury = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsComment = "";
        string i_str_AddedRecordIDsPicture = "";
        string i_str_AddedRecordIDsSubType = "";
        string i_str_AddedRecordIDsBodyArea = "";
        string i_str_AddedRecordIDsInjuryType = "";

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        public string strPassedInDrillDownIDs = "";

        Bitmap bmpBlank = null;
        Bitmap bmpAlert = null;
        
        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;
        private string i_str_selected_business_area_ids = "";  // Reset any prior values first //
        private string i_str_selected_business_areas = "";  // Reset any prior values first //

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        BaseObjects.GridCheckMarksSelection selection1;

        SuperToolTip superToolTipBusinessAreaFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsBusinessAreaFilter = null;

        private string _PicturePath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        #endregion

        public frm_Core_Accident_Manager()
        {
            InitializeComponent();
        }

        private void frm_Core_Accident_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 8003;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();

            bmpBlank = new Bitmap(16, 16);
            bmpAlert = new Bitmap(imageCollection1.Images[6], 16, 16);

            sp00302_Accident_Business_AreasTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00302_Accident_Business_AreasTableAdapter.Fill(dataSet_Accident.sp00302_Accident_Business_Areas);
            gridControl2.ForceInitialize();
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _PicturePath = GetSetting.sp00043_RetrieveSingleSystemSetting(0, "Core_Accident_Picture_Path").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Accident Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Accident Picture Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            
         
            sp00301_Accident_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateAccident = new RefreshGridState(gridViewAccident, "AccidentID");
            gridControlAccident.ForceInitialize();

            i_dtStart = DateTime.Today.AddMonths(-12);
            i_dtEnd = DateTime.Today.AddDays(1).AddMilliseconds(-1);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            barEditItemDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            sp00309_Injuries_Linked_To_AccidentsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateInjury = new RefreshGridState(gridViewInjury, "AccidentInjuryID");

            sp00315_Comments_Linked_To_AccidentsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateComment = new RefreshGridState(gridViewComments, "AccidentCommentID");

            sp00318_Pictures_Linked_To_AccidentsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStatePicture = new RefreshGridState(gridViewPicture, "AccidentPictureID");

            sp00322_Accident_Sub_Types_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateSubType = new RefreshGridState(gridViewSubType, "AccidentSubTypeID");

            sp00325_Accident_Body_Areas_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateBodyArea = new RefreshGridState(gridViewBodyArea, "AccidentBodyAreaID");

            sp00328_Accident_Injury_Types_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateInjuryType = new RefreshGridState(gridViewInjuryType, "AccidentInjuryNatureID");

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                barEditItemDateRange.EditValue = "Custom Filter";
                barEditItemBusinessArea.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }
            Load_Data_SubType();
            Load_Data_BodyArea();
            Load_Data_InjuryType();

            popupContainerControlDateRange.Size = new System.Drawing.Size(200, 107);

            // Create a SuperToolTip //
            superToolTipBusinessAreaFilter = new SuperToolTip();
            superToolTipSetupArgsBusinessAreaFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsBusinessAreaFilter.Title.Text = "Business Area Filter - Information";
            superToolTipSetupArgsBusinessAreaFilter.Title.Image = imageCollectionTooltips.Images[0];
            superToolTipSetupArgsBusinessAreaFilter.Contents.Text = "I store the currently applied Business Area Filter.\n\nNo Filter Set.";
            superToolTipSetupArgsBusinessAreaFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsBusinessAreaFilter.Footer.Text = "";
            superToolTipBusinessAreaFilter.Setup(superToolTipSetupArgsBusinessAreaFilter);
            superToolTipBusinessAreaFilter.AllowHtmlText = DefaultBoolean.True;
            barEditItemBusinessArea.SuperTip = superToolTipBusinessAreaFilter;


            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_Core_Accident_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsAccident))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDsInjury) || !string.IsNullOrEmpty(i_str_AddedRecordIDsComment) || !string.IsNullOrEmpty(i_str_AddedRecordIDsPicture))
                {
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedRecordIDsSubType))
                {
                    Load_Data_SubType();
                }
                if (UpdateRefreshStatus == 4 || !string.IsNullOrEmpty(i_str_AddedRecordIDsBodyArea))
                {
                    Load_Data_BodyArea();
                }
                if (UpdateRefreshStatus == 5 || !string.IsNullOrEmpty(i_str_AddedRecordIDsInjuryType))
                {
                    Load_Data_InjuryType();
                }
            }
            SetMenuStatus();
        }

        private void frm_Core_Accident_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "BusinessAreaFilter", i_str_selected_business_area_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                string strItemFilter = "";

                // Business Area Filter //
                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("BusinessAreaFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayFilter = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl2.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayFilter)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["BusinessAreaID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    barEditItemBusinessArea.EditValue = PopupContainerEdit1_Get_Selected();
                }
                
                Load_Data();

                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                // Get BarButtons Location //
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 0, 0);
                foreach (BarItemLink link in bar1.ItemLinks)
                {
                    if (link.Item.Name == "barEditItemDateRange")  // Find the Correct button then call function to gets it position using Reflection (as this info is not publicly available //
                    {
                        rect = GetLinksScreenRect(link);
                        break;
                    }
                }
                if (rect.X > 0 && rect.Y > 0)
                {
                    // Draw Locus Effect on object so user can see it //
                    System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, rect.Y + rect.Height - 10);
                    locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                }
            }
        }
        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Accidents...");
            }

            if (string.IsNullOrWhiteSpace(i_str_selected_business_area_ids)) i_str_selected_business_area_ids = "";
            RefreshGridViewStateAccident.SaveViewInfo();
            GridView view = (GridView)gridControlAccident.MainView;

            view.BeginUpdate();
            try
            {
                if (barEditItemBusinessArea.EditValue.ToString() == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Callouts //
                {
                    sp00301_Accident_ManagerTableAdapter.Fill(this.dataSet_Accident.sp00301_Accident_Manager, strPassedInDrillDownIDs, i_dtStart, i_dtEnd, "");
                    RefreshGridViewStateAccident.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                else // Load users selection //
                {
                    sp00301_Accident_ManagerTableAdapter.Fill(this.dataSet_Accident.sp00301_Accident_Manager, "", i_dtStart, i_dtEnd, i_str_selected_business_area_ids);
                    RefreshGridViewStateAccident.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
            }
            catch (Exception ex)
            {
                 DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Accident Data.\n\nMessage = [" + ex.Message + "].\n\nIf the error is a result of a query timeout, try adjusting the parameters at the top of the screen to reduce the amount of data returned then try again. If the problem persists, contact Technical Support.", "Load Accidents", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsAccident != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsAccident.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["AccidentID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                        view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);  // Tick the new record so it is picked up by the calendar if it is active //
                    }
                }
                i_str_AddedRecordIDsAccident = "";
                view.EndSelection();
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlAccident.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["AccidentID"])) + ',';
            }
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            gridControlInjury.MainView.BeginUpdate();
            RefreshGridViewStateInjury.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_Accident.sp00309_Injuries_Linked_To_Accidents.Clear();
            }
            else
            {
                sp00309_Injuries_Linked_To_AccidentsTableAdapter.Fill(dataSet_Accident.sp00309_Injuries_Linked_To_Accidents, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateInjury.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlInjury.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsInjury != "")
            {
                strArray = i_str_AddedRecordIDsInjury.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlInjury.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["AccidentInjuryID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsInjury = "";
                view.EndSelection();
            }

            if (xtraTabPageComments.PageVisible)
            {
                gridControlComments.MainView.BeginUpdate();
                this.RefreshGridViewStateComment.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_Accident.sp00315_Comments_Linked_To_Accidents.Clear();
                }
                else
                {
                    try
                    {
                        sp00315_Comments_Linked_To_AccidentsTableAdapter.Fill(dataSet_Accident.sp00315_Comments_Linked_To_Accidents, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                    }
                    catch (Exception ex) { }
                    RefreshGridViewStateComment.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlComments.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsComment != "")
                {
                    strArray = i_str_AddedRecordIDsComment.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlComments.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["AccidentCommentID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsComment = "";
                    view.EndSelection();
                }
            }


            if (xtraTabPagePictures.PageVisible)
            {
                gridControlPicture.MainView.BeginUpdate();
                this.RefreshGridViewStatePicture.SaveViewInfo();  // Store expanded groups and selected rows //
                if (intCount == 0)
                {
                    dataSet_Accident.sp00318_Pictures_Linked_To_Accidents.Clear();
                }
                else
                {
                    try
                    {
                        sp00318_Pictures_Linked_To_AccidentsTableAdapter.Fill(dataSet_Accident.sp00318_Pictures_Linked_To_Accidents, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), _PicturePath);
                    }
                    catch (Exception ex) { }
                    RefreshGridViewStatePicture.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
                gridControlPicture.MainView.EndUpdate();

                // Highlight any recently added new rows //
                if (i_str_AddedRecordIDsPicture != "")
                {
                    strArray = i_str_AddedRecordIDsPicture.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    intID = 0;
                    int intRowHandle = 0;
                    view = (GridView)gridControlPicture.MainView;
                    view.BeginSelection();
                    view.ClearSelection(); // Clear any current selection so just the new record is selected //
                    foreach (string strElement in strArray)
                    {
                        intID = Convert.ToInt32(strElement);
                        intRowHandle = view.LocateByValue(0, view.Columns["AccidentPictureID"], intID);
                        if (intRowHandle != GridControl.InvalidRowHandle)
                        {
                            view.SelectRow(intRowHandle);
                            view.MakeRowVisible(intRowHandle, false);
                        }
                    }
                    i_str_AddedRecordIDsPicture = "";
                    view.EndSelection();
                }
            }

        }

        private void Load_Data_SubType()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            RefreshGridViewStateSubType.SaveViewInfo();
            GridView view = (GridView)gridControlSubType.MainView;

            view.BeginUpdate();
            try
            {
                sp00322_Accident_Sub_Types_ManagerTableAdapter.Fill(dataSet_Accident.sp00322_Accident_Sub_Types_Manager);
                RefreshGridViewStateSubType.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Accident Sub-Type Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Accident Sub-Types", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsSubType != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsSubType.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["AccidentSubTypeID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                        view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);  // Tick the new record so it is picked up by the calendar if it is active //
                    }
                }
                i_str_AddedRecordIDsSubType = "";
                view.EndSelection();
            }
        }

        private void Load_Data_BodyArea()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            RefreshGridViewStateBodyArea.SaveViewInfo();
            GridView view = (GridView)gridControlBodyArea.MainView;

            view.BeginUpdate();
            try
            {
                sp00325_Accident_Body_Areas_ManagerTableAdapter.Fill(dataSet_Accident.sp00325_Accident_Body_Areas_Manager);
                RefreshGridViewStateBodyArea.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Accident Body Area Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Accident Body Areas", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsBodyArea != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsBodyArea.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["AccidentBodyAreaID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                        view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);  // Tick the new record so it is picked up by the calendar if it is active //
                    }
                }
                i_str_AddedRecordIDsBodyArea = "";
                view.EndSelection();
            }
        }

        private void Load_Data_InjuryType()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            RefreshGridViewStateInjuryType.SaveViewInfo();
            GridView view = (GridView)gridControlInjuryType.MainView;

            view.BeginUpdate();
            try
            {
                sp00328_Accident_Injury_Types_ManagerTableAdapter.Fill(dataSet_Accident.sp00328_Accident_Injury_Types_Manager);
                RefreshGridViewStateBodyArea.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Accident Injury Type Data.\n\nMessage = [" + ex.Message + "].\n\nIf the problem persists, contact Technical Support.", "Load Accident Injury Types", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsInjuryType != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsInjuryType.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["AccidentInjuryNatureID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                        view.SetRowCellValue(intRowHandle, "CheckMarkSelection", 1);  // Tick the new record so it is picked up by the calendar if it is active //
                    }
                }
                i_str_AddedRecordIDsInjuryType = "";
                view.EndSelection();
            }
        }


        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data();
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmFocusedGrid.Accident:
                    i_str_AddedRecordIDsAccident = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsAccident : newIds);
                    break;
                case Utils.enmFocusedGrid.AccidentInjury:
                    i_str_AddedRecordIDsInjury = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsInjury : newIds);
                    break;
                case Utils.enmFocusedGrid.AccidentComment:
                    i_str_AddedRecordIDsComment = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsComment : newIds);
                    break;
                case Utils.enmFocusedGrid.AccidentPicture:
                    i_str_AddedRecordIDsPicture = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsPicture : newIds);
                    break;
                case Utils.enmFocusedGrid.AccidentSubType:
                    i_str_AddedRecordIDsSubType = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsSubType : newIds);
                    break;
                case Utils.enmFocusedGrid.AccidentBodyArea:
                    i_str_AddedRecordIDsBodyArea = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsBodyArea : newIds);
                    break;
                case Utils.enmFocusedGrid.AccidentInjuryType:
                    i_str_AddedRecordIDsInjuryType = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsInjuryType : newIds);
                    break;
            }
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Master Sub Types //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddMasterSubTypes = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditMasterSubTypes = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteMasterSubTypes = true;
                        }
                        break;
                    case 2:  // Master Body Areas //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddMasterBodyAreas = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditMasterBodyAreas = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteMasterBodyAreas = true;
                        }
                        break;
                    case 3:  // Master Injury Types //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAddMasterInjuryTypes = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEditMasterInjuryTypes = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDeleteMasterInjuryTypes = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControlAccident.MainView;
            GridView ParentView = (GridView)gridControlAccident.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                    view = (GridView)gridControlAccident.MainView;
                    break;
                case Utils.enmFocusedGrid.AccidentInjury:
                    view = (GridView)gridControlInjury.MainView;
                    break;
                case Utils.enmFocusedGrid.AccidentComment:
                    view = (GridView)gridControlComments.MainView;
                    break;
                case Utils.enmFocusedGrid.AccidentPicture:
                    view = (GridView)gridControlPicture.MainView;
                    break;
                case Utils.enmFocusedGrid.AccidentSubType:
                    view = (GridView)gridControlSubType.MainView;
                    break;
                case Utils.enmFocusedGrid.AccidentBodyArea:
                    view = (GridView)gridControlBodyArea.MainView;
                    break;
                case Utils.enmFocusedGrid.AccidentInjuryType:
                    view = (GridView)gridControlInjuryType.MainView;
                    break;
                default: 
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();
            int[] intParentRowHandles = ParentView.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                case Utils.enmFocusedGrid.AccidentInjury:
                case Utils.enmFocusedGrid.AccidentComment:
                case Utils.enmFocusedGrid.AccidentPicture:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentSubType:
                    if (iBool_AllowAddMasterSubTypes)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    if (iBool_AllowEditMasterSubTypes && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDeleteMasterSubTypes && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentBodyArea:
                    if (iBool_AllowAddMasterBodyAreas)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    if (iBool_AllowEditMasterBodyAreas && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDeleteMasterBodyAreas && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjuryType:
                    if (iBool_AllowAddMasterInjuryTypes)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    if (iBool_AllowEditMasterInjuryTypes && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDeleteMasterInjuryTypes && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                    {
                        bbiBlockAdd.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.Jobs:
                    {
                        bbiBlockAdd.Enabled = (iBool_AllowAdd && intParentRowHandles.Length > 1);
                    }
                    break;
            }

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            gridControlAccident.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlAccident.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intParentRowHandles.Length > 0);
            gridControlAccident.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intParentRowHandles.Length > 0);
            gridControlAccident.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intParentRowHandles.Length > 0);
            gridControlAccident.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intParentRowHandles.Length == 1);

            view = (GridView)gridControlInjury.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlInjury.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlInjury.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlInjury.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlInjury.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlInjury.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);
            
            view = (GridView)gridControlComments.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlComments.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlComments.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlComments.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlComments.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlPicture.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlPicture.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlSubType.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlSubType.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAddMasterSubTypes;
            gridControlSubType.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEditMasterSubTypes && intRowHandles.Length > 0);
            gridControlSubType.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDeleteMasterSubTypes && intRowHandles.Length > 0);
            gridControlSubType.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlBodyArea.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlBodyArea.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAddMasterSubTypes;
            gridControlBodyArea.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEditMasterSubTypes && intRowHandles.Length > 0);
            gridControlBodyArea.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDeleteMasterSubTypes && intRowHandles.Length > 0);
            gridControlBodyArea.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlInjuryType.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlInjuryType.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAddMasterInjuryTypes;
            gridControlInjuryType.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEditMasterInjuryTypes && intRowHandles.Length > 0);
            gridControlInjuryType.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDeleteMasterInjuryTypes && intRowHandles.Length > 0);
            gridControlInjuryType.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Accident_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)view.GetRow(intRowHandles[0]);
                            var row = (DataSet_Accident.sp00301_Accident_ManagerRow)rowView.Row;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjury:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Accident_Injury_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)view.GetRow(intRowHandles[0]);
                            var row = (DataSet_Accident.sp00301_Accident_ManagerRow)rowView.Row;
                            fChildForm._LinkedToAccidentID = row.AccidentID;
                            
                            string strDate = "Unknown Date";
                            try { strDate = Convert.ToDateTime(row.AccidentDate).ToString("dd/MM/yyyy HH:mm"); }
                            catch (Exception) { }
                            fChildForm._LinkedToAccident = (string.IsNullOrEmpty(row.AccidentType.ToString()) ? "" : row.AccidentType) + " - " + strDate;
                            fChildForm._LinkedToPersonTypeID = 1;
                            fChildForm._LinkedToPersonID = 0;
                        }
                        fChildForm.Show();
                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentComment:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Accident_Comment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlAccident.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)view.GetRow(intRowHandles[0]);
                            var row = (DataSet_Accident.sp00301_Accident_ManagerRow)rowView.Row;
                            fChildForm._LinkedToAccidentID = row.AccidentID;
                            
                            string strDate = "Unknown Date";
                            try { strDate = Convert.ToDateTime(row.AccidentDate).ToString("dd/MM/yyyy HH:mm"); }
                            catch (Exception) { }
                            fChildForm._LinkedToAccident = (string.IsNullOrEmpty(row.AccidentType.ToString()) ? "" : row.AccidentType) + " - " + strDate;
                            fChildForm._RecordedByPersonTypeID = 1;
                            fChildForm._RecordedByPersonID = 0;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentPicture:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Accident_Picture_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlAccident.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)view.GetRow(intRowHandles[0]);
                            var row = (DataSet_Accident.sp00301_Accident_ManagerRow)rowView.Row;
                            fChildForm._LinkedToAccidentID = row.AccidentID;
                            
                            string strDate = "Unknown Date";
                            try { strDate = Convert.ToDateTime(row.AccidentDate).ToString("dd/MM/yyyy HH:mm"); }
                            catch (Exception) { }
                            fChildForm._LinkedToAccident = (string.IsNullOrEmpty(row.AccidentType.ToString()) ? "" : row.AccidentType) + " - " + strDate;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentSubType:
                    {
                        if (!iBool_AllowAddMasterSubTypes) return;

                        view = (GridView)gridControlSubType.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Accident_Master_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentBodyArea:
                    {
                        if (!iBool_AllowAddMasterBodyAreas) return;

                        view = (GridView)gridControlBodyArea.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Accident_Master_Body_Area_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjuryType:
                    {
                        if (!iBool_AllowAddMasterInjuryTypes) return;

                        view = (GridView)gridControlInjuryType.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Accident_Master_Injury_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();

                        // Get Site Contracts to add visits to //
                        var fChildForm = new frm_OM_Select_Site_Contract_Multiple();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;

                        string strSelectedSiteContractIDs = fChildForm.strSelectedChildIDs;
                        if (string.IsNullOrWhiteSpace(strSelectedSiteContractIDs)) return;

                        // Open Edit Visit screen in BlockAdd mode //
                        var fChildForm1 = new frm_Core_Accident_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strSelectedSiteContractIDs;
                        fChildForm1.intLinkedToRecordID = 0;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjury:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Accident record to block add Injuries to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentID")) + ',';
                        }

                        var fChildForm1 = new frm_Core_Accident_Injury_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentComment:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentID")) + ',';
                        }

                        var fChildForm1 = new frm_Core_Accident_Comment_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        fChildForm1._RecordedByPersonTypeID = 0;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentPicture:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentID")) + ',';
                        }

                        var fChildForm1 = new frm_Core_Accident_Picture_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjury:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlInjury.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentInjuryID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Injury_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentComment:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlComments.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentCommentID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Comment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._RecordedByPersonTypeID = 0;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentPicture:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPicture.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentPictureID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Picture_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentSubType:
                    {
                        if (!iBool_AllowEditMasterSubTypes) return;
                        view = (GridView)gridControlSubType.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentSubTypeID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Master_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentBodyArea:
                    {
                        if (!iBool_AllowEditMasterBodyAreas) return;
                        view = (GridView)gridControlBodyArea.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentBodyAreaID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Master_Body_Area_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjuryType:
                    {
                        if (!iBool_AllowEditMasterInjuryTypes) return;
                        view = (GridView)gridControlInjuryType.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentInjuryNatureID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Master_Injury_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjury:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlInjury.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentInjuryID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Injury_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentComment:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlComments.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentCommentID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Comment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._RecordedByPersonTypeID = 0;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentPicture:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlPicture.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentPictureID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Picture_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentSubType:
                    {
                        if (!iBool_AllowEditMasterSubTypes) return;
                        view = (GridView)gridControlSubType.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentSubTypeID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Master_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentBodyArea:
                    {
                        if (!iBool_AllowEditMasterBodyAreas) return;
                        view = (GridView)gridControlBodyArea.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentBodyAreaID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Master_Body_Area_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjuryType:
                    {
                        if (!iBool_AllowEditMasterInjuryTypes) return;
                        view = (GridView)gridControlInjuryType.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentInjuryNatureID")) + ',';
                        }

                        var fChildForm = new frm_Core_Accident_Master_Injury_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            string strRecordIDs = "";
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlAccident.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Accidents to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Accident" : Convert.ToString(intRowHandles.Length) + " Accidents") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Accident" : "these Accidents") + " will no longer be available for selection and any related records will also be deleted!";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "AccidentID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_AccidentTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00300_Accident_Delete("accident", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjury:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlInjury.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Accident Injuries to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Accident Injury" : Convert.ToString(intRowHandles.Length) + " Accident Injuries") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Accident Injury" : "these Accident Injuries") + " will no longer be available for selection and any related records will also be deleted!";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentInjuryID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_AccidentTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00300_Accident_Delete("accident_injury", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentPicture:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlComments.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Accident Pictures to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Accident Picture" : Convert.ToString(intRowHandles.Length) + " Accident Picture") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Accident Picture" : "these Accident Pictures") + " will no longer be available for selection!";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentPictureID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_AccidentTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00300_Accident_Delete("accident_picture", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentComment:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlPicture.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Accident Comments to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Accident Comment" : Convert.ToString(intRowHandles.Length) + " Accident Comments") + " selected for delete!\n\nProceed?\n\nWarning: If you proceed " + (intCount == 1 ? "this Accident Comment" : "these Accident Comments") + " will no longer be available for selection!";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentCommentID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_AccidentTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00300_Accident_Delete("accident_comment", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentSubType:
                    {
                        if (!iBool_AllowDeleteMasterSubTypes) return;
                        view = (GridView)gridControlSubType.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Master Accident Sub-Types to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Master Accident Sub-Type" : Convert.ToString(intRowHandles.Length) + " Master Accident Sub-Types") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Master Accident Sub-Type" : "these Master Accident Sub-Types") + " will no longer be available for selection!";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "AccidentSubTypeID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_AccidentTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00300_Accident_Delete("accident_sub_type", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data_SubType();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentBodyArea:
                    {
                        if (!iBool_AllowDeleteMasterBodyAreas) return;
                        view = (GridView)gridControlBodyArea.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Master Accident Body Areas to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Master Accident Body Area" : Convert.ToString(intRowHandles.Length) + " Master Accident Body Areas") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Master Accident Body Area" : "these Master Accident Body Areas") + " will no longer be available for selection!";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "AccidentBodyAreaID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_AccidentTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00300_Accident_Delete("accident_body_area", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data_BodyArea();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjuryType:
                    {
                        if (!iBool_AllowDeleteMasterInjuryTypes) return;
                        view = (GridView)gridControlInjuryType.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Master Accident Injury Types to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Master Accident Injury Type" : Convert.ToString(intRowHandles.Length) + " Master Accident Injury Types") + " selected for delete!\n\nProceed?\n\nWARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Master Accident Injury Type" : "these Master Accident Injury Types") + " will no longer be available for selection!";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "AccidentInjuryNatureID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_AccidentTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp00300_Accident_Delete("accident_injury_type", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data_InjuryType();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Accident:
                    {
                        view = (GridView)gridControlAccident.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentID")) + ',';
                        }
                        var fChildForm = new frm_Core_Accident_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjury:
                    {
                        view = (GridView)gridControlInjury.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentInjuryID")) + ',';
                        }
                        var fChildForm = new frm_Core_Accident_Injury_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentComment:
                    {
                        view = (GridView)gridControlComments.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentCommentID")) + ',';
                        }
                        var fChildForm = new frm_Core_Accident_Comment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        fChildForm._RecordedByPersonTypeID = 0;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentPicture:
                    {
                        view = (GridView)gridControlPicture.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentPictureID")) + ',';
                        }
                        var fChildForm = new frm_Core_Accident_Picture_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentSubType:
                    {
                        view = (GridView)gridControlSubType.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentSubTypeID")) + ',';
                        }
                        var fChildForm = new frm_Core_Accident_Master_Sub_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentBodyArea:
                    {
                        view = (GridView)gridControlBodyArea.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentBodyAreaID")) + ',';
                        }
                        var fChildForm = new frm_Core_Accident_Master_Body_Area_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.AccidentInjuryType:
                    {
                        view = (GridView)gridControlInjuryType.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "AccidentInjuryNatureID")) + ',';
                        }
                        var fChildForm = new frm_Core_Accident_Master_Injury_Type_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewAccident":
                    message = "No Accidents Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewInjury":
                    message = "No Linked Injuries Available - Select one or more Accidents to view Linked Injuries";
                    break;
                case "gridViewPicture":
                    message = "No Pictures Available - Select one or more Accidents to view Linked Pictures";
                    break;
                case "gridViewComments":
                    message = "No Comments Available - Select one or more Accidents to view Linked Comments";
                    break;
                case "gridViewSubType":
                    message = "No Master Accident Sub-Types Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewBodyArea":
                    message = "No Master Accident Body Areas Available - Adjust any filters and click Refresh button";
                    break;
                case "gridViewInjuryType":
                    message = "No Master Accident Injury Types Available - Adjust any filters and click Refresh button";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewAccident":
                    LoadLinkedRecords();
                    view = (GridView)gridControlInjury.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlComments.MainView;
                    view.ExpandAllGroups();
                    view = (GridView)gridControlPicture.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }


        #endregion


        #region GridView - Accident

        private void gridControlAccident_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewAccident;
                        int intRecordType = 61;  // Accident //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "AccidentID"));

                        string strDate = "Unknown Date";
                        try { strDate = Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "AccidentDate")).ToString("dd/MM/yyyy HH:mm"); }
                        catch (Exception) { }
                        string strRecordDescription = (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AccidentType").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AccidentType").ToString()) + " - " + strDate;
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewAccident_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            /*GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "ClientName")
            {
                if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "DoNotInvoiceClient")) == 1)
                {
                    //e.Appearance.BackColor = Color.LightCoral;
                    //e.Appearance.BackColor2 = Color.Red;
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }*/
        }

        private void gridViewAccident_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "LinkedToRecordID":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedToRecordID")) <= 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewAccident_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewAccident_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Accident;
            SetMenuStatus();
        }

        private void gridViewAccident_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Accident;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewAccident_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedToRecordID":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedToRecordID")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedRecord_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Record //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intLinkedToRecordID = 0;
            try { intLinkedToRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LinkedToRecordID")); }
            catch (Exception) { }
           
            int intLinkedToRecordTypeID = 0;
            try { intLinkedToRecordTypeID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "LinkedToRecordTypeID")); }
            catch (Exception) { }

            if (intLinkedToRecordID <= 0 || intLinkedToRecordTypeID <= 0) return;

            string strRecordIDs = intLinkedToRecordID.ToString() + ",";
            switch (intLinkedToRecordTypeID)
            {
                case 1:  // Visit //
                    Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_visit");
                    break;
                case 2:  // Job //
                    Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_job");
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region GridView - Injuries

        private void gridControlInjury_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewAccident;
                        GridView view2 = gridViewInjury;
                        int intRecordType = 62;  // Accident Incident //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view2.GetRowCellValue(view2.FocusedRowHandle, "AccidentInjuryID"));

                        string strDate = "Unknown Date";
                        try { strDate = Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "AccidentDate")).ToString("dd/MM/yyyy HH:mm"); }
                        catch (Exception) { }
                        string strRecordDescription = (string.IsNullOrEmpty(view.GetRowCellValue(view.FocusedRowHandle, "AccidentType").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "AccidentType").ToString()) + " - " + strDate + ", Body Area: " + view2.GetRowCellValue(view2.FocusedRowHandle, "AccidentBodyArea").ToString() + ", Injury Type: " + view2.GetRowCellValue(view2.FocusedRowHandle, "NatureOfInjury").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinkedRecords();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewInjury_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {

        }

        private void gridViewInjury_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewInjury_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentInjury;
            SetMenuStatus();
        }

        private void gridViewInjury_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentInjury;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Pictures

        private void gridControlPicture_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlPicture.MainView;
                        int intRecordType = 30;  // Spraying //
                        int intRecordSubType = 0;  // 0 = Visit, 1 = Job //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "AccidentPictureID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Chemical: " + view.GetRowCellValue(view.FocusedRowHandle, "ChemicalName").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewPicture_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "PicturePath":
                    if (string.IsNullOrEmpty(view.GetRowCellValue(e.RowHandle, "PicturePath").ToString())) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }
        }

        private void gridViewPicture_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewPicture_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentPicture;
            SetMenuStatus();
        }

        private void gridViewPicture_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentPicture;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewPicture_ShowingEditor(object sender, CancelEventArgs e)
        {

        }
        
        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            string strFile = view.GetRowCellValue(view.FocusedRowHandle, "PicturePath").ToString();
            if (string.IsNullOrEmpty(strFile))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Picture Linked - unable to proceed.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(strFile);
            }
            catch
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view picture: " + strFile + ".\n\nThe file may no longer exist or you may not have a viewer installed on your computer capable of loading the file.", "View Linked Picture", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
        
        #endregion


        #region GridView - Comments

        private void gridControlComments_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlComments.MainView;
                        int intRecordType = 29;  // Waste //
                        int intRecordSubType = 0;  // 0 = Visit, 1 = Job //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "AccidentCommentID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "FullDescription").ToString() + ", Waste: " + view.GetRowCellValue(view.FocusedRowHandle, "WasteType").ToString();
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewComments_CustomColumnDisplayText(object sender, CustomColumnDisplayTextEventArgs e)
        {
        }

        private void gridViewComments_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewComments_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentComment;
            SetMenuStatus();
        }

        private void gridViewComments_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentComment;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region Callout Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        #region Business Area Filter Panel

        private void btnBusinessAreaFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditBusinessAreaFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            string strTooltipText = "";
            superToolTipSetupArgsBusinessAreaFilter.Contents.Text = "I store the currently applied Business Area Filter.\n\nNo Filter Set";
            i_str_selected_business_area_ids = "";  // Reset any prior values first //
            i_str_selected_business_areas = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_business_area_ids = "";
                return "No Business Area Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_business_area_ids = "";
                return "No Business Area Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_business_area_ids += Convert.ToString(view.GetRowCellValue(i, "BusinessAreaID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_business_areas = Convert.ToString(view.GetRowCellValue(i, "BusinessAreaName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_business_areas += ", " + Convert.ToString(view.GetRowCellValue(i, "BusinessAreaName"));
                        }
                        intCount++;
                    }
                }
                // Update Filter control's tooltip //
                strTooltipText = i_str_selected_business_areas.Replace(", ", "\n");
                superToolTipSetupArgsBusinessAreaFilter.Contents.Text = "I store the currently applied Business Area Filter.\n\n" + strTooltipText;
            }
            return i_str_selected_business_areas;
        }

        #endregion


        #region GridView - Master Sub Types

        private void gridControlSubType_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_SubType();                
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewSubType_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewSubType_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentSubType;
            SetMenuStatus();
        }

        private void gridViewSubType_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentSubType;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Master Body Areas

        private void gridControlBodyArea_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_BodyArea();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewBodyArea_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewBodyArea_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentBodyArea;
            SetMenuStatus();
        }

        private void gridViewBodyArea_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentBodyArea;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Master Injury Types

        private void gridControlInjuryType_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data_InjuryType();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewInjuryType_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewInjuryType_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentInjuryType;
            SetMenuStatus();
        }

        private void gridViewInjuryType_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.AccidentInjuryType;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }

        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo {Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip};

            toolTipController1.ShowHint(Info);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }










    }
}



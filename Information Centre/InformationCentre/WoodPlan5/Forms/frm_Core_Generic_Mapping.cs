﻿using System;
using System.Collections;
using System.Collections.Generic;  // Required for List call //
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Linq;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;

using BaseObjects;
using WoodPlan5.Properties;

using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.WindowsForms.ToolTips;
using GMap.NET.MapProviders;
using GoogleDirections;

namespace WoodPlan5
{
    public partial class frm_Core_Generic_Mapping : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;

        int _NextObjectID = 90000000;  // Holds unique ID stored in Tag of Markers, Polygons and Routes //
        int _SelectedObjectID = 0;
        public List<Mapping_Objects> ListPassedInObjects;

        GMapOverlay vertices;
        GMapOverlay auxiliar;
        GMapMarker center;
        GMapMarker selectedVertice;
        GMapMarker selectedIntermediatePoint;
        bool isMouseDown;
        bool polygonIsComplete = false;
        bool isDraggingVertice = false;
        bool isDraggingIntermediatePoint = false;
        private bool _allowDrawPolygon;
        private GMapPolygon _polygon;
        private GMapRoute _route;
        private List<GMapMarker> _secondaryMarkers;
        private List<GMapPolygon> _secondaryPolygons;

        private ArrayList arraylistFloatingPanels = new ArrayList();  // Holds which panels were floating when form was deactivated so the panels can be hidden then re-shown on form activate //

        GMapMarker selectedMarker;

        // layers //
        GMapOverlay top;
        internal GMapOverlay points;
        internal GMapOverlay lines;
        internal GMapOverlay polygons;

        string strMapMode = "None";
        bool boolMovingMap = false;
        #endregion

        #region Public properties

        public GMapPolygon Polygon
        {
            get { return _polygon; }
            set  { _polygon = value; }
        }

        public List<GMapMarker> SecondaryMarkers
        {
            get { return _secondaryMarkers; }
            set { _secondaryMarkers = value; }
        }

        public List<GMapPolygon> SecondaryPolygons
        {
            get { return _secondaryPolygons; }
            set { _secondaryPolygons = value; }
        }

        public bool AllowDrawPolygon
        {
            get { return _allowDrawPolygon; }
            set { _allowDrawPolygon = value; }
        }

        #endregion     

        public frm_Core_Generic_Mapping()
        {
            InitializeComponent();
        }

        private void frm_Core_Generic_Mapping_Load(object sender, EventArgs e)
        {
            this.FormID = 400056;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            //contruct fields
            _secondaryMarkers = new List<GMapMarker>();
            _secondaryPolygons = new List<GMapPolygon>();
 
            repositoryItemMarqueeProgressBar1.Stopped = false;
            beiProgress.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            //myMap.SetCurrentPositionByKeywords("UK");
            myMap.MapProvider = GMapProviders.BingMap;
            myMap.MinZoom = 3;
            myMap.MaxZoom = 17;
            myMap.Zoom = 10;
            myMap.Manager.Mode = AccessMode.ServerAndCache;

            repositoryItemComboBoxMapType.Items.AddRange(GMapProviders.List);
            beiMapType.EditValue = myMap.MapProvider;
            
            bsiPositionXY.Caption = "Position - Lat: " + myMap.Position.Lat.ToString(CultureInfo.InvariantCulture) + ", Long: " + myMap.Position.Lng.ToString(CultureInfo.InvariantCulture);

            // map events
            myMap.OnPositionChanged += new PositionChanged(myMap_OnPositionChanged);
            myMap.OnTileLoadStart += new TileLoadStart(myMap_OnTileLoadStart);
            myMap.OnTileLoadComplete += new TileLoadComplete(myMap_OnTileLoadComplete);
            myMap.OnMapZoomChanged += new MapZoomChanged(myMap_OnMapZoomChanged);
            myMap.OnMapTypeChanged += new MapTypeChanged(myMap_OnMapTypeChanged);
            myMap.MouseMove += new MouseEventHandler(myMap_MouseMove);
            myMap.MouseDown += new MouseEventHandler(myMap_MouseDown);
            myMap.MouseUp += new MouseEventHandler(myMap_MouseUp);
            myMap.MouseDoubleClick += new MouseEventHandler(myMap_MouseDoubleClick);
            myMap.OnMarkerClick += new MarkerClick(myMap_OnMarkerClick);
            myMap.OnMarkerEnter += new MarkerEnter(myMap_OnMarkerEnter);
            myMap.OnMarkerLeave += new MarkerLeave(myMap_OnMarkerLeave);
            myMap.OnPolygonClick += new PolygonClick(myMap_OnPolygonClick);
            myMap.OnPolygonEnter += new PolygonEnter(myMap_OnPolygonEnter);
            myMap.OnPolygonLeave += new PolygonLeave(myMap_OnPolygonLeave);
            myMap.OnRouteClick += new RouteClick(myMap_OnRouteClick);
            myMap.OnRouteEnter += new RouteEnter(myMap_OnRouteEnter);
            myMap.OnRouteLeave += new RouteLeave(myMap_OnRouteLeave);

            // add custom layers  
            {
                lines = new GMapOverlay("lines");
                myMap.Overlays.Add(lines);

                polygons = new GMapOverlay("polygons");
                myMap.Overlays.Add(polygons);
 
                points = new GMapOverlay("points");
                myMap.Overlays.Add(points);

                top = new GMapOverlay("top");
                myMap.Overlays.Add(top);

                auxiliar = new GMapOverlay("auxiliar");
                myMap.Overlays.Add(auxiliar);
                vertices = new GMapOverlay("vertices");
                myMap.Overlays.Add(vertices);

                lines.Routes.CollectionChanged += new GMap.NET.ObjectModel.NotifyCollectionChangedEventHandler(Lines_CollectionChanged);
                points.Markers.CollectionChanged += new GMap.NET.ObjectModel.NotifyCollectionChangedEventHandler(Points_CollectionChanged);
                polygons.Polygons.CollectionChanged += new GMap.NET.ObjectModel.NotifyCollectionChangedEventHandler(Polygons_CollectionChanged);
                top.Markers.CollectionChanged += new GMap.NET.ObjectModel.NotifyCollectionChangedEventHandler(Top_CollectionChanged);
            }
            // Load markers //
            GMapMarker currentMarker;
            double dStartLat = (double)0.00;
            double dStartLong = (double)0.00;
            if (ListPassedInObjects.Count == 0)  // No objects passed in so default to HO centre coordinates //
            {
                dStartLat = (double)51.626546;
                dStartLong = (double)0.392531;
                myMap.Position = new PointLatLng(dStartLat, dStartLong);

                //currentMarker = new GMapMarkerGoogleRed(myMap.Position);
                currentMarker = new GMarkerGoogle(myMap.Position, GMarkerGoogleType.red);
                currentMarker.IsHitTestVisible = true;
                currentMarker.ToolTipText = "Ground Control Head Office";
                currentMarker.ToolTipMode = MarkerTooltipMode.OnMouseOver;
                currentMarker.ToolTip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular);

                GMapBaloonToolTip tooltip = new GMapBaloonToolTip(currentMarker);
                tooltip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular);
                top.Markers.Add(currentMarker);
            }
            else
            {
                foreach (Mapping_Objects mapObject in ListPassedInObjects)
                {
                    // Need to work out what type of point it is so we can pick the correct colour //
                    char[] delimiters = new char[] { '|' };
                    string[] items = mapObject.stringID.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                    string strType = "";
                    if (items.Length > 0)
                    {
                        strType = items[0];
                    }
                    switch (strType)
                    {
                        case "Job Start":
                            //currentMarker = new GMapMarkerGoogleGreen(new PointLatLng(mapObject.doubleLat, mapObject.doubleLong));
                            currentMarker = new GMarkerGoogle(new PointLatLng(mapObject.doubleLat, mapObject.doubleLong), GMarkerGoogleType.green);
                           break;
                        case "Job End":
                            //currentMarker = new GMapMarkerGoogleRed(new PointLatLng(mapObject.doubleLat, mapObject.doubleLong));
                            currentMarker = new GMarkerGoogle(new PointLatLng(mapObject.doubleLat, mapObject.doubleLong), GMarkerGoogleType.red);
                           break;
                        default:
                            //currentMarker = new GMapMarkerGoogleRed(new PointLatLng(mapObject.doubleLat, mapObject.doubleLong));
                            currentMarker = new GMarkerGoogle(new PointLatLng(mapObject.doubleLat, mapObject.doubleLong), GMarkerGoogleType.red);
                          break;
                    }
                    currentMarker.Tag = mapObject.stringID;
                    currentMarker.IsHitTestVisible = true;
                    currentMarker.ToolTipText = mapObject.stringToolTip;
                    currentMarker.ToolTipMode = MarkerTooltipMode.OnMouseOver;
                    currentMarker.ToolTip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular);
                    
                    GMapBaloonToolTip tooltip = new GMapBaloonToolTip(currentMarker);
                    tooltip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular);
                    top.Markers.Add(currentMarker);
                }
                //top.Markers.Add(new GMap.NET.WindowsForms.Markers.GMapMarkerGoogleGreen(new PointLatLng(LatitudeValue, LongitudeValue)));
            }
            //myMap.MapScaleInfoEnabled = true;

            /*
            GMapTextMarker TextMarker;
            dStartLat = (double)0.392531;
            dStartLong = (double)51.626546;
            TextMarker = new GMapTextMarker(myMap.Position);
            TextMarker.IsHitTestVisible = true;
            TextMarker.ToolTipText = "Ground Control Head Office";
            TextMarker.ToolTipMode = MarkerTooltipMode.OnMouseOver;
            TextMarker.ToolTip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular);
            TextMarker.Text = "Test";
            TextMarker.TextBrush = Brushes.Blue; 
            top.Markers.Add(TextMarker);
            */

            myMap.ZoomAndCenterMarkers(null); 
            trackBar1.Properties.Minimum = myMap.MinZoom;
            trackBar1.Properties.Maximum = myMap.MaxZoom;
            trackBar1.Value = (int)myMap.Zoom;
 
            bsiZoom.Caption = "Zoom: " + myMap.Zoom.ToString();

            // Note that post open Zooms and Centres map as the screen is locked at this point //
        }

        private void Lines_CollectionChanged(object sender, GMap.NET.ObjectModel.NotifyCollectionChangedEventArgs e)
        {
            //textBoxrouteCount.Text = routes.Routes.Count.ToString();
            bsiMarkerCount.Caption = "Objects: " + Get_Map_Object_Count("all").ToString();
        }
        private void Points_CollectionChanged(object sender, GMap.NET.ObjectModel.NotifyCollectionChangedEventArgs e)
        {
            bsiMarkerCount.Caption = "Objects: " + Get_Map_Object_Count("all").ToString();
        }
        private void Polygons_CollectionChanged(object sender, GMap.NET.ObjectModel.NotifyCollectionChangedEventArgs e)
        {
            bsiMarkerCount.Caption = "Objects: " + Get_Map_Object_Count("all").ToString();
        }
        private void Top_CollectionChanged(object sender, GMap.NET.ObjectModel.NotifyCollectionChangedEventArgs e)
        {
            bsiMarkerCount.Caption = "Objects: " + Get_Map_Object_Count("all").ToString();
        }


        #region Map Events


        private void myMap_Paint(object sender, PaintEventArgs e)
        {
            // Rendering stuff //
            base.OnPaint(e);

            if (!bciScaleBar.Checked) return;

            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            double resolution = myMap.MapProvider.Projection.GetGroundResolution((int)myMap.Zoom, myMap.Position.Lat);

            int px10 = (int)(10.0 / resolution);            // 10 meters
            int px100 = (int)(100.0 / resolution);          // 100 meters
            int px1000 = (int)(1000.0 / resolution);        // 1km   
            int px10000 = (int)(10000.0 / resolution);      // 10km  
            int px100000 = (int)(100000.0 / resolution);    // 100km  
            int px1000000 = (int)(1000000.0 / resolution);  // 1000km
            int px5000000 = (int)(5000000.0 / resolution);  // 5000km

            //Check how much width we have and set the scale accordingly
            int availableWidth = (intScaleRectWidth - 2 * intScaleLeftPadding);

            //5000 kilometers:
            if (availableWidth >= px5000000)
                DrawScale(e.Graphics, px5000000, availableWidth, 5000, "km");
            //1000 kilometers:
            else if (availableWidth >= px1000000)
                DrawScale(e.Graphics, px1000000, availableWidth, 1000, "km");
            //100 kilometers:
            else if (availableWidth >= px100000)
                DrawScale(e.Graphics, px100000, availableWidth, 100, "km");
            //10 kilometers:
            else if (availableWidth >= px10000)
                DrawScale(e.Graphics, px10000, availableWidth, 10, "km");
            //1 kilometers:
            else if (availableWidth >= px1000)
                DrawScale(e.Graphics, px1000, availableWidth, 1, "km");
            //100 meters:
            else if (availableWidth >= px100)
                DrawScale(e.Graphics, px100, availableWidth, 100, "m");
            //10 meters:
            else if (availableWidth >= px10)
                DrawScale(e.Graphics, px10, availableWidth, 10, "m");
        }

        private void myMap_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = true;

                //if (myMap.PolygonsEnabled && _allowDrawPolygon)
                if (myMap.PolygonsEnabled && strMapMode == "AddPolygon") 
                {
                    if (_polygon == null) _polygon = new GMapPolygon(new List<PointLatLng>(), "MyPolygon");

                    // If the polygon is incomplete, click will create new vertices //
                    if (!polygonIsComplete)
                    {
                        if (selectedVertice == null)
                        {
                            GMapMarkerWithSquare marker = new GMapMarkerWithSquare(myMap.FromLocalToLatLng(e.X, e.Y));
                            vertices.Markers.Add(marker);
                            this.selectedVertice = marker;
                        }
                    }
                }
                else if (strMapMode == "AddPolyline")
                {
                    if (_route == null) _route = new GMapRoute(new List<PointLatLng>(), "MyPolyLine");

                    // If the polyline is incomplete, click will create new vertices //
                    if (!polygonIsComplete)
                    {
                        if (selectedVertice == null)
                        {
                            GMapMarkerWithSquare marker = new GMapMarkerWithSquare(myMap.FromLocalToLatLng(e.X, e.Y));
                            vertices.Markers.Add(marker);
                            this.selectedVertice = marker;
                        }
                    }
                    
                }
            }
        }
       
        private void myMap_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isMouseDown = false;
                // if (selectedMarker == null) 

                if (_allowDrawPolygon && (strMapMode == "AddPolygon" || strMapMode == "EditPolygon" || strMapMode == "AddPolyline" || strMapMode == "EditPolyline"))
                {
                    // OnDrop //
                    if (selectedVertice != null)
                    {
                        // Until the polygon\polyline is complete, make auxiliary lines joining the vertices //
                        if (strMapMode == "AddPolygon" || strMapMode == "AddPolyline")
                        {
                            if (!polygonIsComplete && vertices.Markers.Count > 1)
                            {
                                int verticeIndex = vertices.Markers.IndexOf(selectedVertice);
                                Pen pen = new Pen(Brushes.Red, 3);
                                GMapMarkerLine auxLine = new GMapMarkerLine(selectedVertice.Position, vertices.Markers[verticeIndex - 1].Position, pen);
                                auxiliar.Markers.Add(auxLine);
                            }
                        }
                        else if (strMapMode == "EditPolygon")
                        {
                            if (isDraggingVertice)
                            {
                                isDraggingVertice = false;

                                //if (polygonIsComplete)
                                //{
                                    _polygon.Points.Clear();
                                    _polygon.Points.AddRange(vertices.Markers.Select(m => m.Position));
                                    myMap.UpdatePolygonLocalPosition(_polygon);

                                    // Rearrangement of intermediate points //
                                    int selectedIndex = vertices.Markers.IndexOf(selectedVertice);
                                    // Previous point //
                                    if (selectedIndex == 0)
                                    {
                                        GMapMarker intermediatePoint = auxiliar.Markers.Last();
                                        intermediatePoint.Position = CalculateMiddlePoint(vertices.Markers[vertices.Markers.Count - 2], vertices.Markers[selectedIndex]);
                                    }
                                    else
                                    {
                                        auxiliar.Markers[selectedIndex - 1].Position = CalculateMiddlePoint(vertices.Markers[selectedIndex - 1], vertices.Markers[selectedIndex]);
                                    }
                                    // Next point //
                                    if (selectedIndex <= (auxiliar.Markers.Count - 1))
                                    {
                                        auxiliar.Markers[selectedIndex].Position = CalculateMiddlePoint(vertices.Markers[selectedIndex], vertices.Markers[selectedIndex + 1]);
                                    }
                                //}
                            }
                        }
                        selectedVertice = null;
                    }

                    if (strMapMode == "EditPolygon")
                    {
                        // if dragging intermediate point dragging is finish //
                        if (selectedIntermediatePoint != null)
                        {
                            if (isDraggingIntermediatePoint)
                            {
                                isDraggingIntermediatePoint = false;
                                // Make a new vertice //
                                GMapMarkerWithSquare newVertice = new GMapMarkerWithSquare(selectedIntermediatePoint.Position);
                                // Remove the old intermediate point //
                                int selectedIndex = auxiliar.Markers.IndexOf(selectedIntermediatePoint);
                                auxiliar.Markers.Remove(selectedIntermediatePoint);

                                // Add the new vertice, in the correct position of the vertices collection //
                                int newVerticeIndex = selectedIndex + 1;

                                vertices.Markers.Insert(newVerticeIndex, newVertice);

                                // Update polygon //
                                _polygon.Points.Clear();
                                _polygon.Points.AddRange(vertices.Markers.Select(m => m.Position));
                                myMap.UpdatePolygonLocalPosition(_polygon);

                                // Make and add new intermediate points //
                                PointLatLng intermediatePosition1 = CalculateMiddlePoint(vertices.Markers[newVerticeIndex - 1], vertices.Markers[newVerticeIndex]);
                                GMapMarkerGraySquare intermediatePoint1 = new GMapMarkerGraySquare(intermediatePosition1);

                                PointLatLng intermediatePosition2 = CalculateMiddlePoint(vertices.Markers[newVerticeIndex], vertices.Markers[newVerticeIndex + 1]);
                                GMapMarkerGraySquare intermediatePoint2 = new GMapMarkerGraySquare(intermediatePosition2);

                                auxiliar.Markers.Insert(selectedIndex, intermediatePoint1);
                                auxiliar.Markers.Insert(selectedIndex + 1, intermediatePoint2);

                            }
                            selectedIntermediatePoint = null;
                        }
                    }
                }

            }
            else if (e.Button == MouseButtons.Right)
            {
                if (boolMovingMap == true)
                {
                    boolMovingMap = false;
                }
                else
                {
                    bbiDeleteMarker.Enabled = (selectedMarker == null ? false : true);
                    bbiAddPolygon.Enabled = (myMap.PolygonsEnabled && _allowDrawPolygon && strMapMode == "AddPolygon" && !polygonIsComplete && vertices.Markers.Count > 2 ? true : false);
                    bbiCreatePolyline.Enabled = (myMap.PolygonsEnabled && _allowDrawPolygon && strMapMode == "AddPolyline" && !polygonIsComplete && vertices.Markers.Count > 1 ? true : false);
                    pmMapMarker.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
                }
            }
        } 

        private void myMap_MouseMove(object sender, MouseEventArgs e)
        {
            PointLatLng pnew = myMap.FromLocalToLatLng(e.X, e.Y);
            bsiPositionXY.Caption = "Position - Lat: " + pnew.Lat.ToString(CultureInfo.InvariantCulture) + ", Long: " + pnew.Lng.ToString(CultureInfo.InvariantCulture);

            if (e.Button == MouseButtons.Left && isMouseDown)
            {
                if (myMap.PolygonsEnabled && _allowDrawPolygon && strMapMode == "EditPolygon")
                {
                    // If there is a selected vertice //
                    if (this.selectedVertice != null)
                    {
                        // Drag the selected vertice //
                        this.selectedVertice.Position = myMap.FromLocalToLatLng(e.X, e.Y);
                        isDraggingVertice = true;
                    }
                    else
                    {
                        // If there is a selected intermediate point, we are dragging //
                        if (this.selectedIntermediatePoint != null)
                        {
                            this.selectedIntermediatePoint.Position = myMap.FromLocalToLatLng(e.X, e.Y);
                            isDraggingIntermediatePoint = true;
                        }
                    }
                }
            }
        }

        private void myMap_MouseEnter(object sender, EventArgs e)
        {
            myMap.Focus();
        }

        private void myMap_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (myMap.PolygonsEnabled)
            {
                PointLatLng point = myMap.FromLocalToLatLng(e.X, e.Y);
                foreach (GMapPolygon polygon in myMap.Overlays[0].Polygons)
                {
                    if (GMap_Functions.PointInPolygon(point, polygon.Points.ToArray()))
                    {
                        if (polygon.Name != null)
                        {
                            ToolTip tip = new ToolTip();
                            tip.SetToolTip(this, polygon.Name.ToString());
                            tip.Show("", this.FindForm(), 5000);
                        }
                    }
                }
            }
        }

        private void myMap_DoubleClick(object sender, EventArgs e)
        {
        }

        private void myMap_MouseClick(object sender, MouseEventArgs e)
        {
            switch (strMapMode)
            {
                case "AddMarker":
                    {
                        if (e.Button == MouseButtons.Left && Control.ModifierKeys != Keys.Shift) AddMarker(sender, e);
                    }
                    break;
                case "AddLine":
                    {
                        if (e.Button == MouseButtons.Left && Control.ModifierKeys != Keys.Shift) AddLine(sender, e);
                    }
                    break;
                default:
                    break;
            } 
        }
        private void AddMarker(object sender, MouseEventArgs e)
        {
            GMapMarker currentMarker;
            currentMarker = new GMarkerGoogle(myMap.FromLocalToLatLng(e.X, e.Y), GMarkerGoogleType.red);
            currentMarker.ToolTipText = "Marker: ";
            currentMarker.IsHitTestVisible = true;
            currentMarker.ToolTipMode = MarkerTooltipMode.OnMouseOver;
            currentMarker.ToolTip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular);

            GMapBaloonToolTip tooltip = new GMapBaloonToolTip(currentMarker);
            tooltip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular);

            top.Markers.Add(currentMarker);
            selectedMarker = currentMarker;
            //bsiMarkerCount.Caption = "Object: " + Get_Map_Object_Count("all").ToString();
        }
        private void AddLine(object sender, MouseEventArgs e)
        {
        }

        private void myMap_OnPositionChanged(PointLatLng point)
        {
            if (center != null) center.Position = point;
            boolMovingMap = true;
            //textBoxLatCurrent.Text = point.Lat.ToString(CultureInfo.InvariantCulture);
            //textBoxLngCurrent.Text = point.Lng.ToString(CultureInfo.InvariantCulture);
        }

        private void myMap_OnTileLoadStart()
        {
            MethodInvoker m = delegate()
            {
                // Show Progress Bar //
                repositoryItemMarqueeProgressBar1.Stopped = false;
                beiProgress.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            };
            try
            {
                BeginInvoke(m);
            }
            catch
            {
            }
        }

        private void myMap_OnTileLoadComplete(long ElapsedMilliseconds)
        {
            MethodInvoker m = delegate()
            {
                // Hide Progress Bar //
                repositoryItemMarqueeProgressBar1.Stopped = true;
                beiProgress.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            };
            try
            {
                BeginInvoke(m);
            }
            catch
            {
            }
        }

        private void myMap_OnMapTypeChanged(GMapProvider type)
        {
            trackBar1.Properties.Minimum = myMap.MinZoom;
            trackBar1.Properties.Maximum = myMap.MaxZoom;
        }

        private void myMap_OnMapZoomChanged()
        {
            trackBar1.Value = (int)(myMap.Zoom);
            bsiZoom.Caption = "Zoom: " + myMap.Zoom.ToString();
            if (center != null) center.Position = myMap.Position;
        }


        private void myMap_OnMarkerClick(GMapMarker item, MouseEventArgs e)
        {
           /* if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (item is GMapMarkerRect)
                {
                    GeoCoderStatusCode status;
                    var pos = GMapProviders.GoogleMap.GetPlacemark(item.Position, out status);
                    if (status == GeoCoderStatusCode.G_GEO_SUCCESS && pos != null)
                    {
                        GMapMarkerRect v = item as GMapMarkerRect;
                        {
                            v.ToolTipText = pos.Address;
                        }
                        myMap.Invalidate(false);
                    }
                }
            }*/
            if (item is GMapMarker) selectedMarker = item;
            
            if (myMap.PolygonsEnabled && _allowDrawPolygon && strMapMode == "AddPolygon")
            {
                // Can only create the polygon if the polygon is incomplete //
                if (!polygonIsComplete)
                {
                    // The polygon can only be closed if the click is on the first vertice, and it is not the first time //
                    if (vertices.Markers.First() == this.selectedVertice && vertices.Markers.Count > 1)
                    {
                        this.vertices.Markers.Add(this.selectedVertice);

                        // Add the vertices to polygon (make polygon) //
                        _polygon.Points.AddRange(vertices.Markers.Select(m => m.Position));
                        /*List<PointLatLng> polygonPoints = new List<PointLatLng>();
                        foreach (GMapMarker m in vertices.Markers)
                        {
                            if (m is GMapMarkerWithSquare)
                            {
                                m.Tag = polygonPoints.Count;
                                polygonPoints.Add(m.Position);
                            }
                        }
                        _polygon.Points.AddRange(polygonPoints);*/

 
                        myMap.UpdatePolygonLocalPosition(_polygon);
                        _polygon.IsHitTestVisible = true;
                        _polygon.Tag = _NextObjectID;
                        _NextObjectID++;

                        _polygon.Fill = new SolidBrush(Color.FromArgb(50, 0, 255, 18));  // Set background colour //
                       
                        polygons.Polygons.Add(_polygon);
 
                        polygonIsComplete = true;
                        auxiliar.Markers.Clear();
                        // Add new intermediate points between the vertices //
                        for (int i = 0; i < vertices.Markers.Count; i++)
                        {
                            if (i != vertices.Markers.Count - 1)
                            {
                                PointLatLng middle = CalculateMiddlePoint(vertices.Markers[i], vertices.Markers[i + 1]);
                                GMapMarkerGraySquare intermediatePoint = new GMapMarkerGraySquare(middle);
                                auxiliar.Markers.Add(intermediatePoint);
                            }
                        }
                        ClearVerticeSquares();
                    }
                }
            }
        }

        private void myMap_OnMarkerEnter(GMapMarker item)
        {
            /*if (item is GMapMarkerRect)
            {
                GMapMarkerRect rc = item as GMapMarkerRect;
                rc.Pen.Color = Color.Red;
                myMap.Invalidate(false);

                CurentRectMarker = rc;

                //Debug.WriteLine("OnMarkerEnter: " + item.Position);
            }*/
            if (myMap.PolygonsEnabled && (_allowDrawPolygon || strMapMode == "EditPolygon"))
            {
                // Select the marker that was clicked //
                if (vertices.Markers.Contains(item)) this.selectedVertice = item;

                if (auxiliar.Markers.Contains(item)) this.selectedIntermediatePoint = item;
            }
        }
        
        private void myMap_OnMarkerLeave(GMapMarker item)
        {
            /*if (item is GMapMarkerRect)
            {
                CurentRectMarker = null;

                GMapMarkerRect rc = item as GMapMarkerRect;
                rc.Pen.Color = Color.Blue;
                myMap.Invalidate(false);

               // Debug.WriteLine("OnMarkerLeave: " + item.Position);
            }*/

            // If the marker is being dragged, then it is not deselected //
            if (!isDraggingVertice)
            {
                selectedVertice = null;
            }
            if (!isDraggingIntermediatePoint)
            {
                selectedIntermediatePoint = null;
            }
        }


        private void myMap_OnPolygonClick(GMapPolygon item, MouseEventArgs e)
        {
            if (strMapMode != "EditPolygon") return;
            if (isDraggingVertice || isDraggingIntermediatePoint) return;

            if (item is GMapPolygon)
            {
                ClearVerticeSquares();
                _polygon = item;
                _SelectedObjectID = Convert.ToInt32(item.Tag);
                //List<PointLatLng> polygonPoints = new List<PointLatLng>();
                //polygonPoints = item.Points;
                //_polygon = new GMapPolygon(new List<PointLatLng>(), "Selected Polygon");
                //_polygon.Points.AddRange(polygonPoints);
            }
            else
            {
                return;
            }

            if (myMap.PolygonsEnabled && strMapMode == "EditPolygon")
            {
                // Add vertices for polygon //
                foreach (PointLatLng point in _polygon.Points)
                {
                    GMapMarkerWithSquare marker = new GMapMarkerWithSquare(point);
                    vertices.Markers.Add(marker);
                }
                // Add intermediate points between the vertices //
                for (int i = 0; i < vertices.Markers.Count; i++)
                {
                    if (i != vertices.Markers.Count - 1)
                    {
                        PointLatLng middle = CalculateMiddlePoint(vertices.Markers[i], vertices.Markers[i + 1]);
                        GMapMarkerGraySquare intermediatePoint = new GMapMarkerGraySquare(middle);
                        auxiliar.Markers.Add(intermediatePoint);
                    }
                }
            }
                /*
                // Can only create the polygon if the polygon is incomplete //
                if (!polygonIsComplete)
                {
                    // The polygon can only be closed if the click is on the first vertice, and it is not the first time //
                    if (vertices.Markers.First() == this.selectedVertice && vertices.Markers.Count > 1)
                    {
                        this.vertices.Markers.Add(this.selectedVertice);

                        // Add the vertices to polygon (make polygon) //
                        _polygon.Points.AddRange(vertices.Markers.Select(m => m.Position));

                        myMap.UpdatePolygonLocalPosition(_polygon);
                        polygons.Polygons.Add(_polygon);

                        polygonIsComplete = true;
                        auxiliar.Markers.Clear();
                        // Add new intermediate points between the vertices //
                        for (int i = 0; i < vertices.Markers.Count; i++)
                        {
                            if (i != vertices.Markers.Count - 1)
                            {
                                PointLatLng middle = CalculateMiddlePoint(vertices.Markers[i], vertices.Markers[i + 1]);
                                GMapMarkerGraySquare intermediatePoint = new GMapMarkerGraySquare(middle);
                                auxiliar.Markers.Add(intermediatePoint);
                            }
                        }
                        ClearVerticeSquares();
                    }
                 }*/
        }

        private void myMap_OnPolygonEnter(GMapPolygon item)
        {
            //selectedPolygon = item;    
        }

        private void myMap_OnPolygonLeave(GMapPolygon item)
        {
            //selectedPolygon = item;    
        }


        private void myMap_OnRouteClick(GMapRoute item, MouseEventArgs e)
        {
        }

        private void myMap_OnRouteEnter(GMapRoute item)
        {
        }

        private void myMap_OnRouteLeave(GMapRoute item)
        {
        }
        

        private void myMap_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.A)
            {
                myMap.Bearing--;
            }
            else if (e.KeyCode == Keys.Z)
            {
                myMap.Bearing++;
            }
        }

        private void myMap_KeyPress(object sender, KeyPressEventArgs e)
        {
            int offset = 22;

            if (myMap.Focused)
            {
                if (e.KeyChar == (char)Keys.Left)
                {
                    myMap.Offset(-offset, 0);
                }
                else if (e.KeyChar == (char)Keys.Right)
                {
                    myMap.Offset(offset, 0);
                }
                else if (e.KeyChar == (char)Keys.Up)
                {
                    myMap.Offset(0, -offset);
                }
                else if (e.KeyChar == (char)Keys.Down)
                {
                    myMap.Offset(0, offset);
                }
                else if (e.KeyChar == (char)Keys.Add)
                {
                    czuZoomUp_Click(null, null);
                }
                else if (e.KeyChar == (char)Keys.Subtract)
                {
                    czuZoomDown_Click(null, null);
                }
            }
        }

        #endregion


        #region Map Mode

        private void bbiNone_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            strMapMode = "None";
        }

        private void bciAddMarker_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            strMapMode = "AddMarker";
            ClearVerticeSquares();
        }

        private void bciAddPolygon_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            strMapMode = "AddPolygon";
            _allowDrawPolygon = true;
            ClearVerticeSquares();
        }

        private void bciAddPolyline_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            strMapMode = "AddPolyline";
            _allowDrawPolygon = true;
            ClearVerticeSquares();
        }

        private void bciMeasureArea_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            strMapMode = "MeasureArea";
        }

        private void bciMeasureLength_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            strMapMode = "MeasureLength";

            /*
            ClearRoutes();
            List<PointLatLng> routePoints = new List<PointLatLng>();
            routePoints.Add(new PointLatLng(51.62047, 0.3071749));
            routePoints.Add(new PointLatLng(51.576084, 0.488736));
            routePoints.Add(new PointLatLng(51.627903, 0.418397));
            routePoints.Add(new PointLatLng(51.611309, 0.52068));
            routePoints.Add(new PointLatLng(51.656489, -0.39032));
            routePoints.Add(new PointLatLng(51.580559, -0.341995));
            routePoints.Add(new PointLatLng(51.586385, 0.604871));

            RoutingProvider rp = myMap.MapProvider as RoutingProvider;
            if (rp == null)
            {
                rp = GMapProviders.GoogleMap; // use google if provider does not implement routing
            }
            MapRoute route = new MapRoute(routePoints, "route");
            //MapRoute route = rp.GetRoute((PointLatLng)start, (PointLatLng)end, false, false, (int)myMap.Zoom);
            if (route != null)
            {
                // add route
                GMapRoute r = new GMapRoute(route.Points, "Route");
                r.Stroke = new Pen(Color.FromArgb(144, Color.Blue));
                r.Stroke.Width = 5;
                r.Stroke.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                r.IsHitTestVisible = true; // Make it hitable with the mouse //

                lines.Routes.Add(r);

                myMap.ZoomAndCenterRoute(r);
            }
           */
        }

        private void bciEditPolygon_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            strMapMode = "EditPolygon";
        }

        private void bciEditPolyline_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            strMapMode = "EditPolyline";
        }

        private void bciScaleBar_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            myMap.Invalidate();
        }

       #endregion;


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            bbiScaleToFit.PerformClick();  // Zoom and Centre Map //
            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void frm_Core_Generic_Mapping_Activated(object sender, EventArgs e)
        {
            //SetMenuStatus();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in arraylistFloatingPanels)
            {
                dp.Show();
            }
        }

        private void frm_Core_Generic_Mapping_Deactivate(object sender, EventArgs e)
        {
            arraylistFloatingPanels = new ArrayList();
            foreach (DevExpress.XtraBars.Docking.DockPanel dp in dockManager1.Panels)
            {
                if (dp.Dock == DevExpress.XtraBars.Docking.DockingStyle.Float && dp.Visibility == DevExpress.XtraBars.Docking.DockVisibility.Visible)
                {
                    arraylistFloatingPanels.Add(dp);
                    dp.Hide();
                }
            }
        }


        private void ConfigureFormAccordingToMode()
        {
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            myMap.Zoom = (trackBar1.Value);
        }


        private void btnGetRoute_Click(object sender, EventArgs e)
        {
            /*ClearRoutes();           
            Route theRoute = RouteDirections.GetRoute(true,
                new Location("Cromdale, UK"),
                new Location("Grantown-on-Spey, UK"),
                new Location("Aviemore, UK"),
                new Location("Forres, UK"),
                new Location("Elgin, UK"),
                new Location("Inverness, UK"),
                new Location("Aberdeen, UK"),
                new Location("Inverurie, UK"),
                new Location("Ellon, UK")
            );

            List<PointLatLng> Stops = new List<PointLatLng>();  // gMap type //
            int intCount = 0;
            GMapMarker m1 = null;
            GMapMarker m2 = null;
            foreach (RouteLeg leg in theRoute.Legs)
	        {
                intCount++;
                Stops.Add(new PointLatLng(leg.EndLocation.Latitude, leg.EndLocation.Longitude));
                if (intCount == 1)
                {
                    // add route start marks
                    m1 = new GMarkerGoogle(new PointLatLng(leg.EndLocation.Latitude, leg.EndLocation.Longitude), GMarkerGoogleType.green_big_go);
                    m1.ToolTipText = "Route Start";
                    m1.ToolTipMode = MarkerTooltipMode.Always;
                    m1.Tag = "RouteStart";  // store value in tag so we can find it for deleting //
                }
                if (intCount >= theRoute.Legs.Length)
                {
                    // add route end marks
                    m2 = new GMarkerGoogle(new PointLatLng(leg.EndLocation.Latitude, leg.EndLocation.Longitude), GMarkerGoogleType.red_big_stop);
                    m2.ToolTipText = "Route End";
                    m2.ToolTipMode = MarkerTooltipMode.Always;
                    m2.Tag = "RouteEnd";  // store value in tag so we can find it for deleting //
                }
	        }
            GMapRoute _route = new GMapRoute(Stops, "MyRoute");

            //GMapRoute r = _route; // new GMapRoute(_route.Points, "Route");
            _route.Stroke = new Pen(Color.FromArgb(144, Color.Blue));
            _route.Stroke.Width = 5;
            _route.Stroke.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            _route.IsHitTestVisible = true; // Make it hitable with the mouse //
            lines.Routes.Add(_route);

            points.Markers.Add(m1);
            points.Markers.Add(m2);

            myMap.ZoomAndCenterRoute(_route);*/
            
            
            ClearRoutes();
            string strStartLocationName = textEditStartLocation.Text;
            string strEndLocationName = textEditEndLocation.Text;
            if (string.IsNullOrEmpty(strStartLocationName) || string.IsNullOrEmpty(strEndLocationName))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter a Start and End Location before proceeding.", "Get Route", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            for (int i = points.Markers.Count - 1; i >= 0; i--)
            {
                if (points.Markers[i].Tag.ToString().StartsWith("Route")) points.Markers.Remove(points.Markers[i]);
            }
            GeoCoderStatusCode status = GeoCoderStatusCode.Unknow;
            PointLatLng? start = GMapProviders.GoogleMap.GetPoint(textEditStartLocation.Text, out status);

            if (start == null || status != GeoCoderStatusCode.G_GEO_SUCCESS)
            {
                MessageBox.Show("Mapping Geocoder can't find: '" + textEditStartLocation.Text + "', reason: " + status.ToString(), "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            status = GeoCoderStatusCode.Unknow;
            PointLatLng? end = GMapProviders.GoogleMap.GetPoint(textEditEndLocation.Text, out status);
            if (end == null || status != GeoCoderStatusCode.G_GEO_SUCCESS)
            {
                MessageBox.Show("Mapping Geocoder can't find: '" + textEditEndLocation.Text + "', reason: " + status.ToString(), "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            RoutingProvider rp = myMap.MapProvider as RoutingProvider;
            if (rp == null)
            {
                rp = GMapProviders.GoogleMap; // use google if provider does not implement routing
            }
            MapRoute route = rp.GetRoute((PointLatLng)start, (PointLatLng)end, false, false, (int)myMap.Zoom);
            if (route != null)
            {
                // add route
                GMapRoute r = new GMapRoute(route.Points, "Route");
                r.Stroke = new Pen(Color.FromArgb(144, Color.Blue));
                r.Stroke.Width = 5;
                r.Stroke.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
                r.IsHitTestVisible = true; // Make it hitable with the mouse //
                lines.Routes.Add(r);
         
                // add route start/end marks
                GMapMarker m1 = new GMarkerGoogle((PointLatLng)start, GMarkerGoogleType.green_big_go);
                m1.ToolTipText = "Start: " + route.Name;
                m1.ToolTipMode = MarkerTooltipMode.Always;
                m1.Tag = "RouteStart";  // store value in tag so we can find it for deleting //

                GMapMarker m2 = new GMarkerGoogle((PointLatLng)end, GMarkerGoogleType.red_big_stop);
                m2.ToolTipText = "End: " + end.ToString();
                m2.ToolTipMode = MarkerTooltipMode.Always;
                m2.Tag = "RouteEnd";  // store value in tag so we can find it for deleting //

                points.Markers.Add(m1);
                points.Markers.Add(m2);

                myMap.ZoomAndCenterRoute(r);
            }
            //bsiMarkerCount.Caption = "Object: " + Get_Map_Object_Count("all").ToString();

        }

        private void btnClearRoute_Click(object sender, EventArgs e)
        {
            ClearRoutes();
        }
        private void ClearRoutes()
        {
            for (int i = points.Markers.Count - 1; i >= 0; i--)
            {
                if (points.Markers[i].Tag.ToString().StartsWith("Route")) points.Markers.Remove(points.Markers[i]);
            }
            lines.Routes.Clear();
            myMap.Invalidate();
            //bsiMarkerCount.Caption = "Object: " + Get_Map_Object_Count("all").ToString();
        }

        private void czuZoomUp_Click(object sender, EventArgs e)
        {
            myMap.Zoom = ((int)myMap.Zoom) + 1;   // zoom up //

        }

        private void czuZoomDown_Click(object sender, EventArgs e)
        {
            myMap.Zoom = ((int)(myMap.Zoom + 0.99)) - 1;   // zoom down //
        }

        private void repositoryItemComboBoxMapType_SelectedValueChanged(object sender, EventArgs e)
        {
            ComboBoxEdit cbe = (ComboBoxEdit)sender;
            myMap.MapProvider = (GMapProvider)cbe.SelectedItem;
        }


        private void bbiPanLeft_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int offset = 50;
            myMap.Offset(-offset, 0);
        }

        private void bbiRight_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int offset = 50;
            myMap.Offset(offset, 0);
        }

        private void bbiUp_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int offset = 50;
            myMap.Offset(0, -offset);
        }

        private void bbiDown_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int offset = 50;
            myMap.Offset(0, offset);
        }


        #region Panels

        private void bciZoom_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciZoom.Checked)
            {
                dockPanelMapZoom.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelMapZoom.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }

        private void dockPanelMapZoom_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciZoom.Checked) bciZoom.Checked = false;
        }

        private void bciRouting_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciRouting.Checked)
            {
                dockPanelRouting.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelRouting.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }
        
        private void dockPanelRouting_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciRouting.Checked) bciRouting.Checked = false;
        }

        private void bciFindPlace_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (bciFindPlace.Checked)
            {
                dockPanelFindPlace.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
            }
            else
            {
                dockPanelFindPlace.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            }
        }
        
        private void dockPanelFindPlace_ClosingPanel(object sender, DevExpress.XtraBars.Docking.DockPanelCancelEventArgs e)
        {
            if (bciFindPlace.Checked) bciFindPlace.Checked = false;

        }


        #endregion


        private void bbiDeleteMarker_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (selectedMarker != null)
            {
                try
                {
                    GMapOverlay mOverlay = selectedMarker.Overlay;
                    mOverlay.Markers.Remove(selectedMarker);
                    if (auxiliar.Markers.Count != null) auxiliar.Markers.RemoveAt(auxiliar.Markers.Count - 1);
                    selectedMarker = null;
                    myMap.Invalidate();
                    //bsiMarkerCount.Caption = "Objects: " + Get_Map_Object_Count("all").ToString();
                }
                catch (Exception)
                {
                }
            }
            else
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a marker to delete by clicking it.", "Delete Marker", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
        }

        private void buttonEditFindPlace_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string strStartLocationName = buttonEditFindPlace.Text;
            if (string.IsNullOrEmpty(strStartLocationName))
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter a place to find before proceeding.", "Find Place", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GeoCoderStatusCode status = GeoCoderStatusCode.Unknow;
            //PointLatLng? start = GMaps.Instance.GetLatLngFromGeocoder(buttonEditFindPlace.Text, out status);
            PointLatLng? start = GMapProviders.GoogleMap.GetPoint(buttonEditFindPlace.Text, out status);
            if (start == null || status != GeoCoderStatusCode.G_GEO_SUCCESS)
            {
                MessageBox.Show("Mapping Geocoder can't find: '" + textEditStartLocation.Text + "', reason: " + status.ToString(), "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            // add route start/end marks
            //GMapMarker m1 = new GMapMarkerGoogleRed((PointLatLng)start);
            GMapMarker m1 = new GMarkerGoogle((PointLatLng)start, GMarkerGoogleType.red);
            m1.ToolTipText = "Place: " + strStartLocationName;
            m1.ToolTipMode = MarkerTooltipMode.Always;
            m1.Tag = "RouteStart";  // store value in tag so we can find it for deleting //

            points.Markers.Add(m1);
            myMap.Position = (PointLatLng)start;
            selectedMarker = m1;

            /*  -- Alternative method without creating a point --
            GeoCoderStatusCode status = myMap.SetCurrentPositionByKeywords(textBoxGeo.Text);
            if (status != GeoCoderStatusCode.G_GEO_SUCCESS)
            {
                MessageBox.Show("Google Maps Geocoder can't find: '" + textBoxGeo.Text + "', reason: " + status.ToString(), "Mapping", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            */
        }


        #region Pageframe - Sites Page

        private void btnLoadSites_Click(object sender, EventArgs e)
        {
            // stops immediate marker/route/polygon invalidations;
            // call Refresh to perform single refresh and reset invalidation state
            myMap.HoldInvalidation = true;



            myMap.Refresh();
        }

        private void btnClearSites_Click(object sender, EventArgs e)
        {

        }

        #endregion


        private int Get_Map_Object_Count(string strLayer)
        {
            int intCount = 0;
            if (strLayer == "top")
            {
                intCount = top.Markers.Count;
                return intCount;
            }
            else if (strLayer == "points")
            {
                intCount = points.Markers.Count;
                return intCount;
            }
            else if (strLayer == "lines")
            {
                intCount = lines.Markers.Count;
                return intCount;
            }
            else if (strLayer == "polygons")
            {
                intCount = polygons.Polygons.Count;
                return intCount;
            }
            else if (strLayer == "all")
            {
                intCount = points.Markers.Count + lines.Markers.Count + polygons.Polygons.Count + top.Markers.Count;
                return intCount;
            }
            else return 0;
        }

        private void bbiScaleToFit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            myMap.ZoomAndCenterMarkers(null);
        }


        private void Search_For_Object()
        {
            //myMarker.Position = new PointLatLng(54.6961334816182, 25.2985095977783);
            //if(!myMap.CurrentViewArea.Contains(myMarker.Position) // center map if object is out of view, you can disable it, but then map will be centered on each position update
            //{
            //  myMap.Position = myMarker.Position;
            //}
        }

        private void Spider_Graph()
        {
            /*List<PointLatLng> points = new List<PointLatLng>();
            //Add points to list:
            points.Add(...)

            //Create path and add it to Overlay
            GMapRoute path = new GMapRoute(points , "testPath");
            routes.Routes.Add(path);
            */
        }

        private void DrawPolyline(List<PointLatLng> coords, string name)
        {
            // Create path and add it to Overlay //
            GMapRoute path = new GMapRoute(coords, name);
            lines.Routes.Add(path);
        }

        /// <summary>
        /// Draw the scale
        /// </summary>
        /// <param name="g"></param>
        /// <param name="resLength"></param>
        /// <param name="availableWidth"></param>
        /// <param name="totalDimenson"></param>
        /// <param name="unit"></param>
        private void DrawScale(System.Drawing.Graphics g, int resLength, int availableWidth, int totalDimenson, String unit)
        {
            //Point p = new System.Drawing.Point(this.Width - (intScaleRectWidth + 10), this.Height - (intScaleRectHeight + 10));
            Point p = new System.Drawing.Point((myMap.Width + myMap.Location.X - 310), (myMap.Height + myMap.Location.Y - 60));

            Rectangle rect = new Rectangle(p, new Size(intScaleRectWidth, intScaleRectHeight));
            g.FillRectangle(brushCustomScaleBackColor, rect);
            Pen pen = new Pen(colorCustomScaleText, 1);
            g.DrawRectangle(pen, rect);
            SizeF stringSize = new SizeF();
            Point pos = new Point();

            //Header:
            String scaleString = "Scale";
            stringSize = g.MeasureString(scaleString, fontCustomScaleBold);
            pos = new Point(p.X + (rect.Width - (int)stringSize.Width) / 2, p.Y + 3);
            g.DrawString(scaleString, fontCustomScaleBold, pen.Brush, pos);

            pos = new Point(p.X + intScaleLeftPadding, pos.Y + 30);

            //How many rectangles fit?
            int numRects = availableWidth / resLength;
            Size rectSize = new Size(resLength, intScaleBarHeight);
            //Center rectangle
            pos.X += (availableWidth - resLength * numRects) / 2;
            //Draw rectangles:
            for (int i = 0; i < numRects; i++)
            {
                Rectangle r = new Rectangle(pos, rectSize);
                if (i % 2 == 0)
                    g.FillRectangle(pen.Brush, r);
                else
                    g.DrawRectangle(pen, r);
                //Draw little vertical lines
                g.DrawLine(pen, pos, new Point(pos.X, pos.Y - 5));
                //Draw labels:
                int dist = i * totalDimenson;
                stringSize = g.MeasureString(dist + " " + unit, fontCustomScale);
                g.DrawString(dist + " " + unit, fontCustomScale, pen.Brush, new Point(pos.X - (int)stringSize.Width / 2, pos.Y - (7 + (int)stringSize.Height)));
                //Finally set new point
                pos = new Point(pos.X + resLength, pos.Y);
            }
            //Draw last line:
            g.DrawLine(pen, pos, new Point(pos.X, pos.Y - 5));
            //Draw last label
            int m = numRects * totalDimenson;
            stringSize = g.MeasureString(m + " " + unit, fontCustomScale);
            g.DrawString(m + " " + unit, fontCustomScale, pen.Brush, new Point(pos.X - (int)stringSize.Width / 2, pos.Y - (7 + (int)stringSize.Height)));
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GDirections s; 
            var x = GMapProviders.GoogleMap.GetDirections(out s, "Gatwick Airport", "Billericay", false, false, false, false, false);
            if (x == DirectionsStatusCode.OK)
            {
                System.Diagnostics.Debug.WriteLine(s.Summary + ", " + s.Copyrights);
                System.Diagnostics.Debug.WriteLine(s.StartAddress + " -> " + s.EndAddress);
                System.Diagnostics.Debug.WriteLine(s.Distance);
                System.Diagnostics.Debug.WriteLine(s.Duration);
                foreach (var step in s.Steps)
                {
                    System.Diagnostics.Debug.WriteLine(step);
                }
            }

            /*List<PointLatLng> polygonPoints = new List<PointLatLng>();
            foreach (GMapMarker m in top.Markers)
            {
                if (m is GMapMarker)
                {
                    m.Tag = polygonPoints.Count;
                    polygonPoints.Add(m.Position);
                }
            }
            GMapPolygon polygon = new GMapPolygon(polygonPoints, "polygon tester");
            top.Polygons.Add(polygon);
            */
        }

        private void bbiAddPolygon_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (myMap.PolygonsEnabled && _allowDrawPolygon && strMapMode == "AddPolygon")
            {
                // Can only create the polygon if the polygon is incomplete //
                if (!polygonIsComplete)
                {
                    // The polygon can only be closed if the click is on the first vertice, and it is not the first time //
                    if (vertices.Markers.Count > 2)
                    {
                        this.vertices.Markers.Add(this.selectedVertice);

                        // Add the vertices to polygon (make polygon) //
                        //_polygon.Points.AddRange(vertices.Markers.Select(m => m.Position));
                        List<PointLatLng> polygonPoints = new List<PointLatLng>();
                        foreach (GMapMarker m in vertices.Markers)
                        {
                            if (m is GMapMarkerWithSquare)
                            {
                                m.Tag = polygonPoints.Count;
                                polygonPoints.Add(m.Position);
                            }
                        }
                        _polygon.Points.AddRange(polygonPoints);


                        myMap.UpdatePolygonLocalPosition(_polygon);
                        _polygon.IsHitTestVisible = true;
                        _polygon.Tag = _NextObjectID;
                        _NextObjectID++;

                        _polygon.Fill = new SolidBrush(Color.FromArgb(50, 0, 255, 18));  // Set background colour //

                        polygons.Polygons.Add(_polygon);

                        _polygon = null;
                        polygonIsComplete = true;
                        auxiliar.Markers.Clear();
                        ClearVerticeSquares();
                    }
                }
            }
        }

        private void bbiCreatePolyline_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (myMap.PolygonsEnabled && _allowDrawPolygon && strMapMode == "AddPolyline")
            {
                // Can only create the polygon if the polygon is incomplete //
                if (!polygonIsComplete)
                {
                    // The polygon can only be closed if the click is on the first vertice, and it is not the first time //
                    if (vertices.Markers.Count > 1)
                    {
                        this.vertices.Markers.Add(this.selectedVertice);

                        // Add the vertices to polyline (make polyline) //
                        //_route.Points.AddRange(vertices.Markers.Select(m => m.Position));
                        List<PointLatLng> polygonPoints = new List<PointLatLng>();
                        foreach (GMapMarker m in vertices.Markers)
                        {
                            if (m is GMapMarkerWithSquare)
                            {
                                m.Tag = polygonPoints.Count;
                                polygonPoints.Add(m.Position);
                            }
                        }
                        _route.Points.AddRange(polygonPoints);


                        myMap.UpdateRouteLocalPosition(_route);
                        _route.IsHitTestVisible = true;
                        _route.Tag = _NextObjectID;
                        _NextObjectID++;

                        _route.Stroke = new Pen(Color.FromArgb(50, 0, 255, 18));
                        _route.Stroke.Width = 5;
                        _route.Stroke.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;

                        lines.Routes.Add(_route);
                        _route = null;
                        polygonIsComplete = true;
                        auxiliar.Markers.Clear();
                        /*// Add new intermediate points between the vertices //
                        for (int i = 0; i < vertices.Markers.Count; i++)
                        {
                            if (i != vertices.Markers.Count - 1)
                            {
                                PointLatLng middle = CalculateMiddlePoint(vertices.Markers[i], vertices.Markers[i + 1]);
                                GMapMarkerGraySquare intermediatePoint = new GMapMarkerGraySquare(middle);
                                auxiliar.Markers.Add(intermediatePoint);
                            }
                        }*/
                        ClearVerticeSquares();
                    }
                }
            }

        }



        #region Auxiliar functions

        private PointLatLng CalculateMiddlePoint(params GMapMarker[] marks)
        {
            return CalculateMiddlePoint(marks.ToList());
        }

        private PointLatLng CalculateMiddlePoint(List<GMapMarker> marks)
        {
            List<PointLatLng> points = marks.Select(m => m.Position).ToList();

            PointLatLng point = GMap_Functions.CalculateMiddlePoint(points.ToList());

            return point;

        }

        private PointLatLng CalculateMiddlePoint(List<PointLatLng> marks)
        {

            PointLatLng point = GMap_Functions.CalculateMiddlePoint(marks);

            return point;
        }

        private RectLatLng CalculateRectangle(IList<PointLatLng> points)
        {
            RectLatLng rect = new RectLatLng();

            if (points.Count > 1)
            {
                double maxLat = points.Max(p => p.Lat);
                double minLat = points.Min(p => p.Lat);

                double maxLng = points.Max(p => p.Lng);
                double minLng = points.Min(p => p.Lng);

                double widthLat = maxLat - minLat;
                double heightLng = maxLng - minLng;

                rect = new RectLatLng(maxLat, minLng, heightLng, widthLat);

            }
            else
            {
                if (points.Count > 0)
                {
                    SizeLatLng size = new SizeLatLng(0.005, 0.009);
                    PointLatLng point = new PointLatLng(points[0].Lat + 0.0025, points[0].Lng - 0.0045);
                    rect = new RectLatLng(point, size);

                }
            }
            return rect;
        }

        private RectLatLng AddMargin(RectLatLng rect)
        {
            rect.LocationTopLeft = new PointLatLng(rect.LocationTopLeft.Lat + 0.0009, rect.LocationTopLeft.Lng - 0.002);
            rect.HeightLat = rect.HeightLat + 0.0018;
            rect.WidthLng = rect.WidthLng + 0.004;

            return rect;
        }

        private void ClearVerticeSquares()
        {
            polygonIsComplete = false;
            _polygon = null;
            vertices.Markers.Clear();
            vertices.Polygons.Clear();
            vertices.Routes.Clear();
            auxiliar.Markers.Clear();
            auxiliar.Polygons.Clear();
            auxiliar.Routes.Clear();
            this.selectedVertice = null;
        }

        #endregion


        #region Scale variables

        /// <summary>
        /// The font for the m/km markers
        /// </summary>
        private Font fontCustomScale = new Font("Arial", 6);

        /// <summary>
        /// The font for the scale header 
        /// </summary>
        private Font fontCustomScaleBold = new Font("Arial", 8, FontStyle.Bold);

        /// <summary>
        /// The brush for the scale's background
        /// </summary>
        private Brush brushCustomScaleBackColor = new SolidBrush(Color.FromArgb(175, 185, 215, 255));

        /// <summary>
        /// The Textcolor for the scale's fonts
        /// </summary>
        private Color colorCustomScaleText = Color.FromArgb(20, 65, 140);

        /// <summary>
        /// The width of the scale-rectangle
        /// </summary>
        private int intScaleRectWidth = 300;

        /// <summary>
        /// The height of the scale-rectangle
        /// </summary>
        private int intScaleRectHeight = 50;

        /// <summary>
        /// The height of the scale bar
        /// </summary>
        private int intScaleBarHeight = 10;

        /// <summary>
        /// The padding of the scale
        /// </summary>
        private int intScaleLeftPadding = 10;

        #endregion

 


    }
}

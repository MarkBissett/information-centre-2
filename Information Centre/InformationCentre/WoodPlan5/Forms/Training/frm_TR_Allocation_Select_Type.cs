﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_TR_Allocation_Select_Type : WoodPlan5.frmBase_Modal
    {
        #region Instance Variabled
        public int _SelectedType = -1;
        public string _Mode = "add";
        #endregion

        public frm_TR_Allocation_Select_Type()
        {
            InitializeComponent();
        }

        private void frm_TR_Allocation_Select_Type_Load(object sender, EventArgs e)
        {
            this.FormID = 700014;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            labelControlInformation.Text = "What do you want to " + _Mode + " the Allocation to?";
        }

        private void btnQualification_Click(object sender, EventArgs e)
        {
            _SelectedType = 1;
            this.Close();
        }

        private void btnAllocation_Click(object sender, EventArgs e)
        {
            _SelectedType = 2;
            this.Close();
        }


    }
}

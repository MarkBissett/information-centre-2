using System;
using System.Drawing;
using System.Windows.Forms;
using WoodPlan5.Classes.TR;
using WoodPlan5.Properties;
using BaseObjects;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;

namespace WoodPlan5
{
    public partial class frm_TR_Select_Allocation_Type : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        
        public bool boolShowLinkToIDs = true;

        public int intIncludeBlankSubType = 1;
        public int intPassedInAllocationTypeID = 0;
        public int intPassedInAllocationlinkToID = 0;
        
        public int intSelectedAllocationTypeID = 0;
        public int intSelectedAllocationLinkToID = 0;

        public string strSelectedAllocationType = "";
        public string strSelectedAllocationLinkTo = "";

        GridHitInfo downHitInfo = null;

        private int selectID = 0;
        private string selectName = "";

        #endregion

        public frm_TR_Select_Allocation_Type()
        {
            InitializeComponent();
        }

        private void frm_TR_Select_Allocation_Type_Load(object sender, EventArgs e)
        {
            this.FormID = 700015;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            sp_TR_00042_Allocation_SelectTableAdapter.Connection.ConnectionString = strConnectionString;

            try
            {
                sp_TR_00006_Training_AllocationTableAdapter.Connection.ConnectionString = strConnectionString;
                sp_TR_00006_Training_AllocationTableAdapter.FillValidTypes(dataSet_TR_Core.sp_TR_00006_Training_Allocation);
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while loading the Allocation Types list. This screen will now close.\n\nPlease try again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            gridControl1.ForceInitialize();
            GridView view = (GridView)gridControl1.MainView;
            if (intPassedInAllocationTypeID != 0)  // Record selected so try to find and highlight //
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["AllocationTypeID"], intPassedInAllocationTypeID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            if (boolShowLinkToIDs)
            {
                gridControl2.ForceInitialize();
                view = (GridView)gridControl2.MainView;
                if (intPassedInAllocationlinkToID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["ID"], intPassedInAllocationlinkToID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            else
            {
                splitContainerControl1.Collapsed = true;
            }
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        bool internalRowFocusing;


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Allocation Types Available";
                    break;
                case "gridView2":
                    message = "No Allocation Link To items Available For Selection\r\nSelect a Type, and Filter when required to see Related items";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intSelectedAllocationTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, colAllocationTypeID));
                    }
                    buttonEdit1.Enabled = (intSelectedAllocationTypeID == AllocationType.Site);
                    if (boolShowLinkToIDs) LoadLinkedData1();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            foreach (int intRowHandle in intRowHandles)
            {
                intSelectedAllocationTypeID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, colAllocationTypeID));
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0 )          
            {
                dataSet_TR_Core.sp_TR_00042_Allocation_Select.Clear();
            }
            else
            {
                if (intSelectedAllocationTypeID == AllocationType.Site && selectID == 0)
                {
                    dataSet_TR_Core.sp_TR_00044_Allocation_Type_Select.Clear();

                    buttonEdit1.DoValidate();
                }
                else
                {
                    try
                    {
                        sp_TR_00044_Allocation_Type_SelectTableAdapter.Fill(dataSet_TR_Core.sp_TR_00044_Allocation_Type_Select, intSelectedAllocationTypeID, "", selectID);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Link IDs.\n\nTry selecting an Allocation Type again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedAllocationTypeID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedAllocationTypeID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "AllocationTypeID"));
                strSelectedAllocationType = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description"))) ? "Unknown Allocation Type" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description")));
            }
            if (boolShowLinkToIDs)
            {
                view = (GridView)gridControl2.MainView;
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    intSelectedAllocationLinkToID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ID"));
                    strSelectedAllocationLinkTo = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description"))) ? "No Allocation Link" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "Description")));
                }
            }
        }

        private void gridControlVettingSubTypes_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {

        }

        private void buttonEdit2_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {

        }

        private void allocationGridControl_Click(object sender, EventArgs e)
        {

        }


        private void buttonEdit1_EnabledChanged(object sender, EventArgs e)
        {
            if (!buttonEdit1.Enabled)
            {
                selectID = 0;
                selectName = "";
                buttonEdit1.EditValue = "";
            }

        }

        private void buttonEdit1_Click(object sender, EventArgs e)
        {
            if (strFormMode == "view") return;

            var fChildForm1 = new frm_TR_Select_Client();
            fChildForm1.GlobalSettings = this.GlobalSettings;
            fChildForm1._Mode = (strFormMode == "blockadd" ? "multiple" : "single");
            if (strFormMode != "blockadd")
            {
                fChildForm1.intOriginalClientID = selectID;
            }
            else  // blockadd //
            {
                //fChildForm1.strOriginalClientIDs = strSelectIDs;
            }

            if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
            {
                if (strFormMode != "blockadd")
                {
                    selectID = fChildForm1.intSelectedClientID;
                    selectName = fChildForm1.strSelectedClientName;
                }
                else  // blockadd //
                {
                    //strSelectIDs = fChildForm1.strSelectedClientIDs;
                    selectName = fChildForm1.strSelectedClientName;
                }

                buttonEdit1.EditValue = selectName;               

                LoadLinkedData1();
            }

        }
    }
}


﻿namespace WoodPlan5
{
    partial class MaintainQualificationAllocation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintainQualificationAllocation));
            this.gridControlAllocation = new DevExpress.XtraGrid.GridControl();
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAllocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllocationLinkToID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkToName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeLimitDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.colTeamHoldingType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTeamHoldingMinimum = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAllocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00020QualificationSubTypeAllocationSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlAllocation
            // 
            this.gridControlAllocation.DataSource = this.spTR00020QualificationSubTypeAllocationSelectBindingSource;
            this.gridControlAllocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlAllocation.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlAllocation.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlAllocation.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAllocation_EmbeddedNavigator_ButtonClick);
            this.gridControlAllocation.Location = new System.Drawing.Point(0, 0);
            this.gridControlAllocation.MainView = this.gridView1;
            this.gridControlAllocation.Name = "gridControlAllocation";
            this.gridControlAllocation.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlAllocation.Size = new System.Drawing.Size(469, 241);
            this.gridControlAllocation.TabIndex = 1;
            this.gridControlAllocation.UseEmbeddedNavigator = true;
            this.gridControlAllocation.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spTR00020QualificationSubTypeAllocationSelectBindingSource
            // 
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource.DataMember = "sp_TR_00020_Qualification_Sub_Type_Allocation_Select";
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAllocationID,
            this.colQualificationSubTypeID,
            this.colAllocationType,
            this.colAllocationLinkToID,
            this.colLinkToName,
            this.colRequirementType,
            this.colTeamHoldingType,
            this.colTeamHoldingMinimum,
            this.colTimeLimitDescription,
            this.colStartDate,
            this.colEndDate});
            this.gridView1.GridControl = this.gridControlAllocation;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colAllocationID
            // 
            this.colAllocationID.FieldName = "AllocationID";
            this.colAllocationID.Name = "colAllocationID";
            this.colAllocationID.OptionsColumn.AllowEdit = false;
            this.colAllocationID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colAllocationType
            // 
            this.colAllocationType.Caption = "Type";
            this.colAllocationType.FieldName = "AllocationType";
            this.colAllocationType.Name = "colAllocationType";
            this.colAllocationType.OptionsColumn.AllowEdit = false;
            this.colAllocationType.OptionsColumn.ReadOnly = true;
            this.colAllocationType.Visible = true;
            this.colAllocationType.VisibleIndex = 0;
            this.colAllocationType.Width = 96;
            // 
            // colAllocationLinkToID
            // 
            this.colAllocationLinkToID.FieldName = "AllocationLinkToID";
            this.colAllocationLinkToID.Name = "colAllocationLinkToID";
            this.colAllocationLinkToID.OptionsColumn.AllowEdit = false;
            this.colAllocationLinkToID.OptionsColumn.ReadOnly = true;
            // 
            // colLinkToName
            // 
            this.colLinkToName.Caption = "Name / Description";
            this.colLinkToName.FieldName = "LinkToName";
            this.colLinkToName.Name = "colLinkToName";
            this.colLinkToName.OptionsColumn.AllowEdit = false;
            this.colLinkToName.OptionsColumn.ReadOnly = true;
            this.colLinkToName.Visible = true;
            this.colLinkToName.VisibleIndex = 1;
            this.colLinkToName.Width = 282;
            // 
            // colRequirementType
            // 
            this.colRequirementType.Caption = "Requirement";
            this.colRequirementType.FieldName = "RequirementType";
            this.colRequirementType.Name = "colRequirementType";
            this.colRequirementType.OptionsColumn.AllowEdit = false;
            this.colRequirementType.OptionsColumn.ReadOnly = true;
            this.colRequirementType.Visible = true;
            this.colRequirementType.VisibleIndex = 2;
            this.colRequirementType.Width = 98;
            // 
            // colTimeLimitDescription
            // 
            this.colTimeLimitDescription.Caption = "Time Limit";
            this.colTimeLimitDescription.FieldName = "TimeLimitDescription";
            this.colTimeLimitDescription.Name = "colTimeLimitDescription";
            this.colTimeLimitDescription.OptionsColumn.AllowEdit = false;
            this.colTimeLimitDescription.OptionsColumn.ReadOnly = true;
            this.colTimeLimitDescription.Visible = true;
            this.colTimeLimitDescription.VisibleIndex = 5;
            this.colTimeLimitDescription.Width = 107;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start date";
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 6;
            this.colStartDate.Width = 96;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 7;
            this.colEndDate.Width = 110;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter
            // 
            this.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlAllocation;
            // 
            // colTeamHoldingType
            // 
            this.colTeamHoldingType.Caption = "Team Requirement";
            this.colTeamHoldingType.FieldName = "TeamHoldingType";
            this.colTeamHoldingType.Name = "colTeamHoldingType";
            this.colTeamHoldingType.OptionsColumn.AllowEdit = false;
            this.colTeamHoldingType.OptionsColumn.AllowFocus = false;
            this.colTeamHoldingType.OptionsColumn.ReadOnly = true;
            this.colTeamHoldingType.ToolTip = "Specifies any Team holding requirement";
            this.colTeamHoldingType.Visible = true;
            this.colTeamHoldingType.VisibleIndex = 3;
            // 
            // colTeamHoldingMinimum
            // 
            this.colTeamHoldingMinimum.Caption = "Team Minimum";
            this.colTeamHoldingMinimum.FieldName = "TeamHoldingMinimum";
            this.colTeamHoldingMinimum.Name = "colTeamHoldingMinimum";
            this.colTeamHoldingMinimum.OptionsColumn.AllowEdit = false;
            this.colTeamHoldingMinimum.OptionsColumn.AllowFocus = false;
            this.colTeamHoldingMinimum.OptionsColumn.ReadOnly = true;
            this.colTeamHoldingMinimum.ToolTip = "Specifies the minimum  number of Team members that should hold the qualification " +
    "when required";
            this.colTeamHoldingMinimum.Visible = true;
            this.colTeamHoldingMinimum.VisibleIndex = 4;
            // 
            // MaintainQualificationAllocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlAllocation);
            this.Name = "MaintainQualificationAllocation";
            this.Size = new System.Drawing.Size(469, 241);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAllocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00020QualificationSubTypeAllocationSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlAllocation;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_TR_Core dataSet_TR_Core;
        private System.Windows.Forms.BindingSource spTR00020QualificationSubTypeAllocationSelectBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationType;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationLinkToID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkToName;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementType;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeLimitDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamHoldingType;
        private DevExpress.XtraGrid.Columns.GridColumn colTeamHoldingMinimum;
    }
}

﻿namespace WoodPlan5
{
    partial class MaintainQualificationAwardingBody
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintainQualificationAwardingBody));
            this.gridControlAwardingBody = new DevExpress.XtraGrid.GridControl();
            this.spTR00010QualificationSubTypeAwardingBodySelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAwardingBodyID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp_TR_00010_Qualification_Sub_Type_Awarding_Body_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00010_Qualification_Sub_Type_Awarding_Body_SelectTableAdapter();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAwardingBody)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00010QualificationSubTypeAwardingBodySelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlAwardingBody
            // 
            this.gridControlAwardingBody.DataSource = this.spTR00010QualificationSubTypeAwardingBodySelectBindingSource;
            this.gridControlAwardingBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlAwardingBody.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlAwardingBody.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlAwardingBody.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAwardingBody_EmbeddedNavigator_ButtonClick);
            this.gridControlAwardingBody.Location = new System.Drawing.Point(0, 0);
            this.gridControlAwardingBody.MainView = this.gridView1;
            this.gridControlAwardingBody.Name = "gridControlAwardingBody";
            this.gridControlAwardingBody.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlAwardingBody.Size = new System.Drawing.Size(469, 241);
            this.gridControlAwardingBody.TabIndex = 1;
            this.gridControlAwardingBody.UseEmbeddedNavigator = true;
            this.gridControlAwardingBody.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spTR00010QualificationSubTypeAwardingBodySelectBindingSource
            // 
            this.spTR00010QualificationSubTypeAwardingBodySelectBindingSource.DataMember = "sp_TR_00010_Qualification_Sub_Type_Awarding_Body_Select";
            this.spTR00010QualificationSubTypeAwardingBodySelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colQualificationSubTypeID,
            this.colDescription,
            this.colAwardingBodyID});
            this.gridView1.GridControl = this.gridControlAwardingBody;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colID
            // 
            this.colID.Caption = "LinkID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.Caption = "Qualification Sub-Type ID";
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeID.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.colQualificationSubTypeID.Width = 143;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Awarding Body";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 225;
            // 
            // colAwardingBodyID
            // 
            this.colAwardingBodyID.Caption = "Awarding Body ID";
            this.colAwardingBodyID.FieldName = "AwardingBodyID";
            this.colAwardingBodyID.Name = "colAwardingBodyID";
            this.colAwardingBodyID.OptionsColumn.AllowEdit = false;
            this.colAwardingBodyID.OptionsColumn.AllowFocus = false;
            this.colAwardingBodyID.OptionsColumn.ReadOnly = true;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // sp_TR_00010_Qualification_Sub_Type_Awarding_Body_SelectTableAdapter
            // 
            this.sp_TR_00010_Qualification_Sub_Type_Awarding_Body_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "footer_16.png");
            this.imageList2.Images.SetKeyName(4, "expand_and_select_16.png");
            this.imageList2.Images.SetKeyName(5, "colour_expression_16.png");
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlAwardingBody;
            // 
            // MaintainQualificationAwardingBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlAwardingBody);
            this.Name = "MaintainQualificationAwardingBody";
            this.Size = new System.Drawing.Size(469, 241);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAwardingBody)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00010QualificationSubTypeAwardingBodySelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlAwardingBody;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private System.Windows.Forms.BindingSource spTR00010QualificationSubTypeAwardingBodySelectBindingSource;
        private DataSet_TR_Core dataSet_TR_Core;
        private DataSet_TR_CoreTableAdapters.sp_TR_00010_Qualification_Sub_Type_Awarding_Body_SelectTableAdapter sp_TR_00010_Qualification_Sub_Type_Awarding_Body_SelectTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAwardingBodyID;
        private System.Windows.Forms.ImageList imageList2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class MaintainPersonQualificationHistory : DevExpress.XtraEditors.XtraUserControl
    {
        #region Instance Variables...
        private frmBase parent;
        
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private int parQualificationID = 0;

        private bool SetUpDone = false;

        bool iBool_AllowAdd = false;
        bool iBool_AllowDelete = false;
        bool iBool_AllowEdit = false;

        bool isRunning = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        public RefreshGridState RefreshGridViewQualificationRefreshedBy;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        //string i_str_AddedQualificationSubTypeAwardingBodyIDs = "";

        #endregion

        public MaintainPersonQualificationHistory()
        {
            InitializeComponent();
        }

        public void SetUp(
            string ConnectionString,
            int QualificationID,
            string FormMode)
        {
            parent = (frmBase)this.ParentForm;
            
            if (FormMode != "view")
            {
                iBool_AllowAdd = true;

                iBool_AllowEdit = true;

                iBool_AllowDelete = true;
            }

            if (iBool_AllowDelete)
            {
                gridView1.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
                gridView1.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DefaultBoolean.False;
            }

            strConnectionString = ConnectionString;
            parQualificationID = QualificationID;
            
            sp_TR_00029_Person_Qualification_History_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewQualificationRefreshedBy = new RefreshGridState(gridView1, "ID");

            Load_History();

            emptyEditor = new RepositoryItem();

            SetMenuStatus();

            SetUpDone = true;
        }

        public void Reload()
        {
            if (!SetUpDone)
            { return; }

            Load_History();
        }


        private void Load_History()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            RefreshGridViewQualificationRefreshedBy.SaveViewInfo();
            gridControlHistory.BeginUpdate();

            sp_TR_00029_Person_Qualification_History_SelectTableAdapter.Fill(dataSet_TR_Core.sp_TR_00029_Person_Qualification_History_Select,parQualificationID);
            this.RefreshGridViewQualificationRefreshedBy.LoadViewInfo(); // Reload any expanded groups and selected rows //

            gridControlHistory.EndUpdate();

            /**
            // Highlight any recently added new rows //
            if (i_str_AddedQualificationSubTypeAwardingBodyIDs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedQualificationSubTypeAwardingBodyIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControlRefreshes.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedQualificationSubTypeAwardingBodyIDs = "";
            }
            **/
        }

        public void SetMenuStatus()
        {

            GridView view = (GridView)gridControlHistory.MainView;

            int[] intRowHandles;
            
            // Set enabled status of navigator custom buttons //
            intRowHandles = view.GetSelectedRows();
            gridControlHistory.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlHistory.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControlHistory.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControlHistory.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

        }

        private void gridControlAwardingBody_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    GridView view = (GridView)gridControlHistory.MainView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }


        private void Add_Record()
        {
           if (!iBool_AllowAdd) return;
           
            GridView view = (GridView)gridControlHistory.MainView; ;
            MethodInfo method = null;

            view.PostEditor();
           
            /***
           RefreshGridViewStateAwardingBodye.SaveViewInfo();  // Store Grid View State //
           ***/

           int intMaxOrder = 0;
           for (int i = 0; i < view.DataRowCount; i++)
           {
               if (Convert.ToInt32(view.GetRowCellValue(i, "RecordOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "RecordOrder"));
           }

            var fChildForm = new frm_HR_Qualification_Add();

           fChildForm.MdiParent = parent.MdiParent;
           fChildForm.GlobalSettings = parent.GlobalSettings;
           fChildForm.strRecordIDs = parQualificationID.ToString() + ",";
           fChildForm.strFormMode = "add";
           fChildForm.strCaller = this.Name;
           fChildForm.intRecordCount = 0;
           fChildForm.FormPermissions = parent.FormPermissions;
           DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this.ParentForm, typeof(global::WoodPlan5.WaitForm1), true, true);
           fChildForm.splashScreenManager = splashScreenManager1;
           fChildForm.splashScreenManager.ShowWaitForm();
           fChildForm.Show();
           
           method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
           if (method != null) method.Invoke(fChildForm, new object[] { null });
                      
        }

        public bool ChangesPending()
        {
            GridView view = (GridView)gridControlHistory.MainView;
            return view.SelectedRowsCount > 0;
        }

        private void Delete_Record()
        {
            if (!iBool_AllowDelete) return;

            int[] intRowHandles;
            int intCount = 0;
            GridView view  = (GridView)gridControlHistory.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Qualification Entries to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            string strMessage = "You have " + (intCount == 1 ? "1 Qualification Entry" : Convert.ToString(intRowHandles.Length) + " Qualification Entries") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Qualification Entry" : "these Qualification Entries") + " will no longer be available for selection and any related records will also be deleted!</color>";
            if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this.ParentForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                StringBuilder RecordIDs = new StringBuilder();
                foreach (int intRowHandle in intRowHandles)
                {
                    RecordIDs.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "HistoryID")) + ",");
                }

                this.RefreshGridViewQualificationRefreshedBy.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                using (var RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
                {
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        RemoveRecords.sp09000_HR_Delete("qualification_history",RecordIDs.ToString()); 
                    }
                    catch (Exception) { }
                }

                Load_History();
                
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (parent.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }


        private void Edit_Record()
        {
            if (!iBool_AllowEdit) return;

            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControlHistory.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Qualification History Entries to edit by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so edit selected record(s) //
            StringBuilder RecordIDs = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                RecordIDs.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "HistoryID")) + ",");
            }
            var fChildForm = new frm_HR_Qualification_History_Edit();

            fChildForm.MdiParent = parent.MdiParent;
            fChildForm.GlobalSettings = parent.GlobalSettings;
            fChildForm.strRecordIDs = RecordIDs.ToString();
            fChildForm.strFormMode = "edit";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = parent.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this.ParentForm, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });

        }

        private void View_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControlHistory.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Qualification History Entries to view  by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so edit selected record(s) //
            StringBuilder RecordIDs = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                RecordIDs.Append(Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "HistoryID")) + ",");
            }
            var fChildForm = new frm_HR_Qualification_History_Edit();

            fChildForm.MdiParent = parent.MdiParent;
            fChildForm.GlobalSettings = parent.GlobalSettings;
            fChildForm.strRecordIDs = RecordIDs.ToString();
            fChildForm.strFormMode = "view";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.FormPermissions = parent.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this.ParentForm, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        public int HoldCount
        {
            get
            {
                if (dataSet_TR_Core.sp_TR_00029_Person_Qualification_History_Select == null)
                    return 0;

                return dataSet_TR_Core.sp_TR_00029_Person_Qualification_History_Select.Count;
            }
        } 
    }
}

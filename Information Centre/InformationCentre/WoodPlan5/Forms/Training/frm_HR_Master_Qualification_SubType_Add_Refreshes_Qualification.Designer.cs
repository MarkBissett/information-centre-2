namespace WoodPlan5
{
    partial class frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification));
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spTR00013QualificationSubTypeRefreshesAvailableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSetTRCoreBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryPeriodDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshIntervalDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshQualificationStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshQualificationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.QualificationTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.spHR00232MasterQualificationTypesEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.DescriptionTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.spHR00234MasterQualificationSubTypeEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ItemForQualificationTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSubTypeDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.sp00156picklistitemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.woodPlanDataSet = new WoodPlan5.WoodPlanDataSet();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp_HR_00232_Master_Qualification_Types_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00232_Master_Qualification_Types_EditTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter();
            this.sp00156_picklist_itemsTableAdapter = new WoodPlan5.WoodPlanDataSetTableAdapters.sp00156_picklist_itemsTableAdapter();
            this.sp_TR_00013_Qualification_Sub_Type_Refreshes_AvailableTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00013_Qualification_Sub_Type_Refreshes_AvailableTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00013QualificationSubTypeRefreshesAvailableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetTRCoreBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00232MasterQualificationTypesEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00234MasterQualificationSubTypeEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubTypeDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00156picklistitemsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 478);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(628, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection1;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(628, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 478);
            this.barDockControl2.Size = new System.Drawing.Size(628, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(628, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 452);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.QualificationTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DescriptionTextEdit);
            this.dataLayoutControl1.Controls.Add(this.textEdit1);
            this.dataLayoutControl1.DataSource = this.spHR00232MasterQualificationTypesEditBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForQualificationTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(738, 183, 254, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(628, 452);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spTR00013QualificationSubTypeRefreshesAvailableBindingSource;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVettingSubTypes_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(12, 70);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.gridControl1.Size = new System.Drawing.Size(604, 370);
            this.gridControl1.TabIndex = 36;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spTR00013QualificationSubTypeRefreshesAvailableBindingSource
            // 
            this.spTR00013QualificationSubTypeRefreshesAvailableBindingSource.DataMember = "sp_TR_00013_Qualification_Sub_Type_Refreshes_Available";
            this.spTR00013QualificationSubTypeRefreshesAvailableBindingSource.DataSource = this.dataSetTRCoreBindingSource;
            // 
            // dataSetTRCoreBindingSource
            // 
            this.dataSetTRCoreBindingSource.DataSource = this.dataSet_TR_Core;
            this.dataSetTRCoreBindingSource.Position = 0;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOrderID,
            this.colQualificationSubTypeID,
            this.colQualificationTypeDescription,
            this.colDescription,
            this.colStatus,
            this.colValidity,
            this.colExpiryPeriodDescription,
            this.colRefreshDescription,
            this.colRefreshIntervalDescription,
            this.colRefreshQualificationStartDate,
            this.colRefreshQualificationEndDate});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.gridView1.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            // 
            // colOrderID
            // 
            this.colOrderID.FieldName = "OrderID";
            this.colOrderID.Name = "colOrderID";
            this.colOrderID.OptionsColumn.AllowEdit = false;
            this.colOrderID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationTypeDescription
            // 
            this.colQualificationTypeDescription.Caption = "Master Type";
            this.colQualificationTypeDescription.FieldName = "QualificationTypeDescription";
            this.colQualificationTypeDescription.Name = "colQualificationTypeDescription";
            this.colQualificationTypeDescription.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeDescription.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeDescription.Visible = true;
            this.colQualificationTypeDescription.VisibleIndex = 1;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 2;
            this.colDescription.Width = 183;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 3;
            this.colStatus.Width = 65;
            // 
            // colValidity
            // 
            this.colValidity.FieldName = "Validity";
            this.colValidity.Name = "colValidity";
            this.colValidity.OptionsColumn.AllowEdit = false;
            this.colValidity.OptionsColumn.ReadOnly = true;
            this.colValidity.Visible = true;
            this.colValidity.VisibleIndex = 4;
            this.colValidity.Width = 65;
            // 
            // colExpiryPeriodDescription
            // 
            this.colExpiryPeriodDescription.Caption = "Expiry Period";
            this.colExpiryPeriodDescription.FieldName = "ExpiryPeriodDescription";
            this.colExpiryPeriodDescription.Name = "colExpiryPeriodDescription";
            this.colExpiryPeriodDescription.OptionsColumn.AllowEdit = false;
            this.colExpiryPeriodDescription.OptionsColumn.ReadOnly = true;
            this.colExpiryPeriodDescription.Visible = true;
            this.colExpiryPeriodDescription.VisibleIndex = 5;
            this.colExpiryPeriodDescription.Width = 65;
            // 
            // colRefreshDescription
            // 
            this.colRefreshDescription.Caption = "Refresh Type";
            this.colRefreshDescription.FieldName = "RefreshDescription";
            this.colRefreshDescription.Name = "colRefreshDescription";
            this.colRefreshDescription.Visible = true;
            this.colRefreshDescription.VisibleIndex = 6;
            this.colRefreshDescription.Width = 68;
            // 
            // colRefreshIntervalDescription
            // 
            this.colRefreshIntervalDescription.Caption = "Refresh Interval";
            this.colRefreshIntervalDescription.FieldName = "RefreshIntervalDescription";
            this.colRefreshIntervalDescription.Name = "colRefreshIntervalDescription";
            this.colRefreshIntervalDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshIntervalDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshIntervalDescription.Visible = true;
            this.colRefreshIntervalDescription.VisibleIndex = 7;
            this.colRefreshIntervalDescription.Width = 80;
            // 
            // colRefreshQualificationStartDate
            // 
            this.colRefreshQualificationStartDate.Caption = "Start Date";
            this.colRefreshQualificationStartDate.FieldName = "RefreshQualificationStartDate";
            this.colRefreshQualificationStartDate.Name = "colRefreshQualificationStartDate";
            this.colRefreshQualificationStartDate.OptionsColumn.AllowEdit = false;
            this.colRefreshQualificationStartDate.OptionsColumn.ReadOnly = true;
            this.colRefreshQualificationStartDate.Visible = true;
            this.colRefreshQualificationStartDate.VisibleIndex = 8;
            this.colRefreshQualificationStartDate.Width = 53;
            // 
            // colRefreshQualificationEndDate
            // 
            this.colRefreshQualificationEndDate.Caption = "End Date";
            this.colRefreshQualificationEndDate.FieldName = "RefreshQualificationEndDate";
            this.colRefreshQualificationEndDate.Name = "colRefreshQualificationEndDate";
            this.colRefreshQualificationEndDate.OptionsColumn.AllowEdit = false;
            this.colRefreshQualificationEndDate.OptionsColumn.ReadOnly = true;
            this.colRefreshQualificationEndDate.Visible = true;
            this.colRefreshQualificationEndDate.VisibleIndex = 9;
            this.colRefreshQualificationEndDate.Width = 84;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // QualificationTypeIDTextEdit
            // 
            this.QualificationTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spHR00232MasterQualificationTypesEditBindingSource, "QualificationTypeID", true));
            this.QualificationTypeIDTextEdit.Location = new System.Drawing.Point(119, 83);
            this.QualificationTypeIDTextEdit.MenuManager = this.barManager1;
            this.QualificationTypeIDTextEdit.Name = "QualificationTypeIDTextEdit";
            this.QualificationTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QualificationTypeIDTextEdit, true);
            this.QualificationTypeIDTextEdit.Size = new System.Drawing.Size(497, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QualificationTypeIDTextEdit, optionsSpelling1);
            this.QualificationTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.QualificationTypeIDTextEdit.TabIndex = 4;
            // 
            // spHR00232MasterQualificationTypesEditBindingSource
            // 
            this.spHR00232MasterQualificationTypesEditBindingSource.DataMember = "sp_HR_00232_Master_Qualification_Types_Edit";
            this.spHR00232MasterQualificationTypesEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // DescriptionTextEdit
            // 
            this.DescriptionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spHR00232MasterQualificationTypesEditBindingSource, "Description", true));
            this.DescriptionTextEdit.Location = new System.Drawing.Point(120, 36);
            this.DescriptionTextEdit.MenuManager = this.barManager1;
            this.DescriptionTextEdit.Name = "DescriptionTextEdit";
            this.DescriptionTextEdit.Properties.MaxLength = 100;
            this.DescriptionTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.DescriptionTextEdit, true);
            this.DescriptionTextEdit.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.DescriptionTextEdit, optionsSpelling2);
            this.DescriptionTextEdit.StyleController = this.dataLayoutControl1;
            this.DescriptionTextEdit.TabIndex = 35;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spHR00234MasterQualificationSubTypeEditBindingSource, "Description", true));
            this.textEdit1.Location = new System.Drawing.Point(120, 12);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.MaxLength = 100;
            this.textEdit1.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit1, true);
            this.textEdit1.Size = new System.Drawing.Size(496, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit1, optionsSpelling3);
            this.textEdit1.StyleController = this.dataLayoutControl1;
            this.textEdit1.TabIndex = 35;
            // 
            // spHR00234MasterQualificationSubTypeEditBindingSource
            // 
            this.spHR00234MasterQualificationSubTypeEditBindingSource.DataMember = "sp_HR_00234_Master_Qualification_SubType_Edit";
            this.spHR00234MasterQualificationSubTypeEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // ItemForQualificationTypeID
            // 
            this.ItemForQualificationTypeID.Control = this.QualificationTypeIDTextEdit;
            this.ItemForQualificationTypeID.CustomizationFormText = "Qualification Type ID:";
            this.ItemForQualificationTypeID.Location = new System.Drawing.Point(0, 48);
            this.ItemForQualificationTypeID.Name = "ItemForQualificationTypeID";
            this.ItemForQualificationTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForQualificationTypeID.Text = "Qualification Type ID:";
            this.ItemForQualificationTypeID.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(628, 452);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTypeDescription,
            this.ItemForSubTypeDescription});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(608, 48);
            // 
            // ItemForTypeDescription
            // 
            this.ItemForTypeDescription.AllowHide = false;
            this.ItemForTypeDescription.Control = this.DescriptionTextEdit;
            this.ItemForTypeDescription.CustomizationFormText = "Type Description:";
            this.ItemForTypeDescription.Location = new System.Drawing.Point(0, 24);
            this.ItemForTypeDescription.Name = "ItemForTypeDescription";
            this.ItemForTypeDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForTypeDescription.Text = "Type Description:";
            this.ItemForTypeDescription.TextSize = new System.Drawing.Size(105, 13);
            // 
            // ItemForSubTypeDescription
            // 
            this.ItemForSubTypeDescription.Control = this.textEdit1;
            this.ItemForSubTypeDescription.ControlAlignment = System.Drawing.ContentAlignment.BottomLeft;
            this.ItemForSubTypeDescription.CustomizationFormText = "Sub Type Description:";
            this.ItemForSubTypeDescription.Location = new System.Drawing.Point(0, 0);
            this.ItemForSubTypeDescription.Name = "ItemForSubTypeDescription";
            this.ItemForSubTypeDescription.Size = new System.Drawing.Size(608, 24);
            this.ItemForSubTypeDescription.Text = "Sub Type Description:";
            this.ItemForSubTypeDescription.TextSize = new System.Drawing.Size(105, 13);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem2,
            this.layoutControlItem2});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 48);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(608, 384);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(608, 10);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 10);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(608, 374);
            this.layoutControlItem2.Text = "Awarding Bodies";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // sp00156picklistitemsBindingSource
            // 
            this.sp00156picklistitemsBindingSource.DataMember = "sp00156_picklist_items";
            this.sp00156picklistitemsBindingSource.DataSource = this.woodPlanDataSet;
            // 
            // woodPlanDataSet
            // 
            this.woodPlanDataSet.DataSetName = "WoodPlanDataSet";
            this.woodPlanDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp_HR_00232_Master_Qualification_Types_EditTableAdapter
            // 
            this.sp_HR_00232_Master_Qualification_Types_EditTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(0, "add_16.png");
            this.imageCollection2.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection2.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection2.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection2.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // sp_HR_00234_Master_Qualification_SubType_EditTableAdapter
            // 
            this.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp00156_picklist_itemsTableAdapter
            // 
            this.sp00156_picklist_itemsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00013_Qualification_Sub_Type_Refreshes_AvailableTableAdapter
            // 
            this.sp_TR_00013_Qualification_Sub_Type_Refreshes_AvailableTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Preview_16x16");
            // 
            // frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(628, 508);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification";
            this.Text = "Master Qualification Sub Type Add Refreshes Qualification";
            this.Activated += new System.EventHandler(this.frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00013QualificationSubTypeRefreshesAvailableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSetTRCoreBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00232MasterQualificationTypesEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescriptionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00234MasterQualificationSubTypeEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSubTypeDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00156picklistitemsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.woodPlanDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit QualificationTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTypeDescription;
        private DevExpress.XtraEditors.TextEdit DescriptionTextEdit;
        private System.Windows.Forms.BindingSource spHR00232MasterQualificationTypesEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00232_Master_Qualification_Types_EditTableAdapter sp_HR_00232_Master_Qualification_Types_EditTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSubTypeDescription;
        private System.Windows.Forms.BindingSource spHR00234MasterQualificationSubTypeEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter sp_HR_00234_Master_Qualification_SubType_EditTableAdapter;
        private System.Windows.Forms.BindingSource sp00156picklistitemsBindingSource;
        private WoodPlanDataSet woodPlanDataSet;
        private WoodPlanDataSetTableAdapters.sp00156_picklist_itemsTableAdapter sp00156_picklist_itemsTableAdapter;
        private DataSet_TR_Core dataSet_TR_Core;
        private System.Windows.Forms.BindingSource dataSetTRCoreBindingSource;
        private System.Windows.Forms.BindingSource spTR00013QualificationSubTypeRefreshesAvailableBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colValidity;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryPeriodDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshIntervalDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshQualificationStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshQualificationEndDate;
        private DataSet_TR_CoreTableAdapters.sp_TR_00013_Qualification_Sub_Type_Refreshes_AvailableTableAdapter sp_TR_00013_Qualification_Sub_Type_Refreshes_AvailableTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeDescription;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.Utils.ImageCollection imageCollection1;
    }
}

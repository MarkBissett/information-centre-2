using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Classes.TR;
using WoodPlan5.Properties;
using LocusEffects;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_TR_Team_Training_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string strPassedInRecordIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;

        private int i_int_FocusedGrid = 1;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewState1;  // Used by Grid View State Facilities //
        private string i_str_selected_staff_ids = "";
        private string i_str_selected_staff_names = "";
        private string i_str_selected_contractor_ids = "";
        private string i_str_selected_contractor_names = "";
        private string i_str_selected_contractor_team_member_ids = "";
        private string i_str_selected_contractor_team_member_names = "";
        private string i_str_selected_certificate_type_ids = "";
        private string i_str_selected_certificate_type_names = "";
        private DateTime? i_dt_FromDate = null;
        private DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";
        private int i_int_show_archived = 0;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDs1 = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;
        BaseObjects.GridCheckMarksSelection selection3;
        BaseObjects.GridCheckMarksSelection selection4;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;
        private bool boolDateFilter = false;

        private DataSet_Selection DS_Selection1;

        #endregion

        public frm_TR_Team_Training_Manager()
        {
            InitializeComponent();
        }

        private void frm_TR_Team_Training_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 9120;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            DS_Selection1 = new Utilities.DataSet_Selection((GridView)gridView1, strConnectionString);  // Used by DataSets //

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(9, "HR_QualificationCertificatePath").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Qualification Certificate Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Qualification Certificate Files Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp09101_HR_Staff_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp09101_HR_Staff_FilterTableAdapter.Fill(dataSet_HR_Core.sp09101_HR_Staff_Filter);
            gridControl2.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;

            sp09102_HR_Contractor_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp09102_HR_Contractor_FilterTableAdapter.Fill(dataSet_HR_Core.sp09102_HR_Contractor_Filter);
            gridControl3.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection2.CheckMarkColumn.VisibleIndex = 0;
            selection2.CheckMarkColumn.Width = 30;

            sp09103_HR_Contractor_Team_Member_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp09103_HR_Contractor_Team_Member_FilterTableAdapter.Fill(dataSet_HR_Core.sp09103_HR_Contractor_Team_Member_Filter);
            gridControl4.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl4.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;

            sp09104_HR_Certificate_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp09104_HR_Certificate_TypesTableAdapter.Fill(dataSet_HR_Core.sp09104_HR_Certificate_Types);
            gridControl5.ForceInitialize();
            // Add record selection checkboxes to popup grid control //
            selection4 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection4.CheckMarkColumn.VisibleIndex = 0;
            selection4.CheckMarkColumn.Width = 30;

            popupContainerControlStaff.Size = new System.Drawing.Size(325, 480);
            popupContainerControlContractors.Size = new System.Drawing.Size(325, 480);
            popupContainerControlContractorTeamMembers.Size = new System.Drawing.Size(325, 480);
            popupContainerControlCertificateType.Size = new System.Drawing.Size(325, 480);
            
            //sp09100_HR_Qualification_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00033_Training_Matrix_Team_SelectTableAdapter.Connection.ConnectionString = strConnectionString;

            RefreshGridViewState1 = new RefreshGridState(gridView1, "QualificationID");

            emptyEditor = new RepositoryItem();

            if (strPassedInRecordIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEdit1.EditValue = "Custom Filter";
                popupContainerEdit2.EditValue = "Custom Filter";
                popupContainerEdit3.EditValue = "Custom Filter";
                Load_Data();  // Load records //
            }

        }
        private System.Drawing.Rectangle GetLinksScreenRect(BarItemLink link)
        {
            System.Reflection.PropertyInfo info = typeof(BarItemLink).GetProperty("BarControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            Control c = (Control)info.GetValue(link, null);
            return c.RectangleToScreen(link.Bounds);
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            if (strPassedInRecordIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_TR_Team_Training_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDs1))
                {
                    Load_Data();
                }
            }
            SetMenuStatus();
        }

        private void frm_TR_Team_Training_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInRecordIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "StaffFilter", i_str_selected_staff_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ContractorFilter", i_str_selected_contractor_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ContractorTeamMemberFilter", i_str_selected_contractor_team_member_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CertificateTypeFilter", i_str_selected_certificate_type_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "FromDate", i_dt_FromDate.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ToDate", i_dt_ToDate.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ShowArchived", i_int_show_archived.ToString());
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Client Filter //
                int intFoundRow = 0;
                string strFilter = default_screen_settings.RetrieveSetting("StaffFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayRecords = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl2.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayRecords)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["intStaffID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    popupContainerEdit1.EditValue = PopupContainerEdit1_Get_Selected();
                }

                // Contractor Filter //
                strFilter = default_screen_settings.RetrieveSetting("ContractorFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayRecords = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl3.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayRecords)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["intContractorID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    popupContainerEdit2.EditValue = PopupContainerEdit2_Get_Selected();
                }

                // Contractor Team Member Filter //
                strFilter = default_screen_settings.RetrieveSetting("ContractorTeamMemberFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayRecords = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl4.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayRecords)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["TeamMemberID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    popupContainerEdit3.EditValue = PopupContainerEdit3_Get_Selected();
                }

                // Certificate Type Filter //
                strFilter = default_screen_settings.RetrieveSetting("CertificateTypeFilter");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    Array arrayRecords = strFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView view = (GridView)gridControl5.MainView;
                    view.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayRecords)
                    {
                        if (strElement == "") break;
                        intFoundRow = view.LocateByValue(0, view.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            view.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    view.EndUpdate();
                    popupContainerEdit4.EditValue = PopupContainerEdit5_Get_Selected();
                }
                strFilter = default_screen_settings.RetrieveSetting("FromDate");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    if (Convert.ToDateTime(strFilter) != DateTime.MinValue)
                    {
                        dateEditFromDate.DateTime = Convert.ToDateTime(strFilter);
                    }
                }
                strFilter = default_screen_settings.RetrieveSetting("ToDate");
                if (!string.IsNullOrEmpty(strFilter))
                {
                    if (Convert.ToDateTime(strFilter) != DateTime.MinValue)
                    {
                        dateEditToDate.DateTime = Convert.ToDateTime(strFilter);
                    }
                }
                i_str_date_range = PopupContainerEditDateRange_Get_Selected();
                popupContainerDateRange.EditValue = i_str_date_range;

                strFilter = default_screen_settings.RetrieveSetting("ShowArchived");
                i_int_show_archived = (!string.IsNullOrEmpty(strFilter) ? Convert.ToInt32(strFilter) : 0);
                beiShowArchived.EditValue = i_int_show_archived;

                bbiRefresh.PerformClick();  // Load Data //
                if (boolDateFilter)  // Show Locus Effect if a date filter is in place //
                {
                    // Prepare LocusEffects and add custom effect //
                    locusEffectsProvider1 = new LocusEffectsProvider();
                    locusEffectsProvider1.Initialize();
                    locusEffectsProvider1.FramesPerSecond = 30;
                    m_customArrowLocusEffect1 = new ArrowLocusEffect();
                    m_customArrowLocusEffect1.Name = "CustomeArrow1";
                    m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                    m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                    m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                    m_customArrowLocusEffect1.MovementCycles = 20;
                    m_customArrowLocusEffect1.MovementAmplitude = 200;
                    m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                    m_customArrowLocusEffect1.LeadInTime = 0; //msec
                    m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                    m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                    locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                    // Get BarButtons Location //
                    System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, 0, 0);
                    foreach (BarItemLink link in bar1.ItemLinks)
                    {
                        if (link.Item.Name == "popupContainerDateRange")  // Find the Correct button then call function to gets it position using Reflection (as this info is not publicly available //
                        {
                            rect = GetLinksScreenRect(link);
                            break;
                        }
                    }
                    if (rect.X > 0 && rect.Y > 0)
                    {
                        // Draw Locus Effect on object so user can see it //
                        System.Drawing.Point screenPoint = new System.Drawing.Point(rect.X + rect.Width - 10, rect.Y + rect.Height - 10);
                        locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
                    }
                }

            }
        }

        public void UpdateFormRefreshStatus(int status, string strNewIDs1)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            if (strNewIDs1 != "") i_str_AddedRecordIDs1 = strNewIDs1;
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                         //   iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                         //   iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                         //   iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            if (i_int_FocusedGrid == 1)
            {
                view = (GridView)gridControl1.MainView;
            }
            int[] intRowHandles = view.GetSelectedRows();

            if (i_int_FocusedGrid == 1)
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                    bbiBlockAdd.Enabled = true;
                }
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length == 1);

        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;

            GridView view = (GridView)gridControl1.MainView;
            RefreshGridViewState1.SaveViewInfo();
            if (popupContainerEdit1.EditValue.ToString() == "Custom Filter" && strPassedInRecordIDs != "")  // Load passed in Drilldown //
            {
                view.BeginUpdate();
                try
                {
                    //sp09100_HR_Qualification_ManagerTableAdapter.Fill(this.dataSet_HR_Core.sp09100_HR_Qualification_Manager, "", "", "", "", null, null, 0, strPassedInRecordIDs);
                    sp_TR_00033_Training_Matrix_Team_SelectTableAdapter.Fill(this.dataSet_TR_Core.sp_TR_00033_Training_Matrix_Team_Select);
                }
                catch (Exception) { }
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.ExpandAllGroups();
                view.EndUpdate();
            }
            else // Load users selection //
            {
                if (i_str_selected_staff_ids == null) i_str_selected_staff_ids = "";
                if (i_str_selected_contractor_ids == null) i_str_selected_contractor_ids = "";
                if (i_str_selected_contractor_team_member_ids == null) i_str_selected_contractor_team_member_ids = "";
                DateTime? dtFrom = new DateTime(1900, 1, 1);
                DateTime? dtTo = new DateTime(2900, 12, 31);
                if (i_dt_FromDate != Convert.ToDateTime("01/01/0001")) dtFrom = i_dt_FromDate;
                if (i_dt_ToDate != Convert.ToDateTime("01/01/0001")) dtTo = i_dt_ToDate;
                i_int_show_archived = Convert.ToInt32(beiShowArchived.EditValue);

                if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
                view.BeginUpdate();
                try
                {
                    //sp09100_HR_Qualification_ManagerTableAdapter.Fill(this.dataSet_HR_Core.sp09100_HR_Qualification_Manager, i_str_selected_staff_ids, i_str_selected_contractor_ids, i_str_selected_contractor_team_member_ids, i_str_selected_certificate_type_ids, dtFrom, dtTo, i_int_show_archived, "");
                    sp_TR_00033_Training_Matrix_Team_SelectTableAdapter.Fill(this.dataSet_TR_Core.sp_TR_00033_Training_Matrix_Team_Select);
                }
                catch (Exception) { }
                this.RefreshGridViewState1.LoadViewInfo();  // Reload any expanded groups and selected rows //
                view.EndUpdate();
            }
            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDs1 != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDs1.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["QualificationID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDs1 = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            /***

            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    if (!iBool_AllowAdd) return;

                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    int intPersonType = 0;
                    frm_HR_Qualification_Add_Type frm_Child = new frm_HR_Qualification_Add_Type();
                    frm_Child.ShowDialog();
                    switch (frm_Child._SelectedType)
	                {
                        case -1:
                            return;
                        default:
                            intPersonType = frm_Child._SelectedType;
                            break;
	                }           

                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    //frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                    frm_HR_Qualification_Add fChildForm = new frm_HR_Qualification_Add();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.intRecordTypeID = intPersonType;

                    ParentView = (GridView)gridControl1.MainView;
                    intRowHandles = ParentView.GetSelectedRows();
                    if (intRowHandles.Length == 1)
                    {
                        //fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                        //fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" :ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString());

                        //fChildForm.intLinkedToRecordID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0],  colpersonID).ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], colpersonID))); ;
                        //fChildForm.intRecordTypeID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], colPersonTypeID).ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], colPersonTypeID))); ;
                        //fChildForm.strLinkedToRecordDesc = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], colPersonType).ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], colPersonType).ToString()); ;

                    }

                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                default:
                    break;
            }
            ***/
        }

        private void Block_Add()
        {
            /***

            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    if (!iBool_AllowAdd) return;

                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    int intPersonType = 0;
                    frm_HR_Qualification_Add_Type frm_Child = new frm_HR_Qualification_Add_Type();
                    frm_Child.ShowDialog();
                    switch (frm_Child._SelectedType)
                    {
                        case -1:
                            return;
                        default:
                            intPersonType = frm_Child._SelectedType;
                            break;
                    }
                    
                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    //frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                    frm_HR_Qualification_Add fChildForm = new frm_HR_Qualification_Add();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "blockadd";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    fChildForm.intRecordTypeID = intPersonType;

                    ParentView = (GridView)gridControl1.MainView;
                    intRowHandles = ParentView.GetSelectedRows();
                    if (intRowHandles.Length == 1)
                    {
                        //fChildForm.intLinkedToClientID = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientID")));
                        //fChildForm.strLinkedToClientName = (string.IsNullOrEmpty(ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()) ? "" :ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString());
                    }

                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                default:
                    break;
            }

            ***/
        }

        private void Block_Edit()
        {
            /***

            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }

            ***/
        }

        private void Edit_Record()
        {
            /***

            GridView view = null;
            System.Reflection.MethodInfo method = null;
            StringBuilder strRecordIDs = new StringBuilder();
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Surveys //
                    if (!iBool_AllowEdit) return;
                    view = (GridView)gridControl1.MainView;
                    view.PostEditor();
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    intCount = 0;
                    int qID = 0;
                    foreach (int intRowHandle in intRowHandles)
                    {
                        qID = 0;
                        int.TryParse(view.GetRowCellValue(intRowHandle, "QualificationID").ToString(), out qID);
                        if (qID != 0)
                        {
                            intCount++;
                            strRecordIDs.Append(Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',');
                        }
                    }
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("No held Qualifications have been selected.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                    frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = strRecordIDs.ToString();
                    fChildForm.strFormMode = "edit";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = intCount;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                    break;
                default:
                    break;
            }

            ***/
        }

        private void Delete_Record()
        {
            /***

            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    if (!iBool_AllowDelete) return;
                    view = (GridView)gridControl1.MainView;
                    intRowHandles = view.GetSelectedRows();
                    intCount = intRowHandles.Length;
                    if (intCount <= 0)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Qualifications to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    // Checks passed so delete selected record(s) //
                    strMessage = "You have " + (intCount == 1 ? "1 Qualification" : Convert.ToString(intRowHandles.Length) + " Qualifications") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this OQualification" : "these Qualifications") + " will no longer be available for selection!</color>";
                    if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                    {
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        splashScreenManager1.ShowWaitForm();
                        splashScreenManager1.SetWaitFormDescription("Deleting...");

                        string strRecordIDs = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "QualificationID")) + ",";
                        }

                        DataSet_HR_CoreTableAdapters.QueriesTableAdapter RemoveRecords = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
                        RemoveRecords.ChangeConnectionString(strConnectionString);
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        try
                        {
                            RemoveRecords.sp09000_HR_Delete("qualification", strRecordIDs);  // Remove the records from the DB in one go //
                        }
                        catch (Exception)
                        {
                        }
                        Load_Data();

                        if (splashScreenManager1.IsSplashFormVisible)
                        {
                            splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                            splashScreenManager1.CloseWaitForm();
                        }
                        if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;
            }

            ***/
        }

        private void View_Record()
        {
            /***

            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (i_int_FocusedGrid)
            {
                case 1:     // Surveys //
                    {
                        view = (GridView)gridControl1.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "QualificationID")) + ',';
                        }
                        this.RefreshGridViewState1.SaveViewInfo();  // Store Grid View State //
                        frm_HR_Qualification_Edit fChildForm = new frm_HR_Qualification_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }

            ***/
        }


        #region Staff Filter Panel

        private void btnStaffFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView2_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit1_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit1_Get_Selected();
        }

        private string PopupContainerEdit1_Get_Selected()
        {
            i_str_selected_staff_ids = "";    // Reset any prior values first //
            i_str_selected_staff_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl2.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_staff_ids = "";
                return "No Staff Filter";

            }
            else if (selection1.SelectedCount <= 0)
            {
                i_str_selected_staff_ids = "";
                return "No Staff Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_staff_ids += Convert.ToString(view.GetRowCellValue(i, "intStaffID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_staff_names = Convert.ToString(view.GetRowCellValue(i, "SurnameForename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_staff_names += ", " + Convert.ToString(view.GetRowCellValue(i, "SurnameForename"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_staff_names) ? "No Staff Filter" : i_str_selected_staff_names);
        }

        #endregion


        #region Contractor Filter Panel

        private void btnContractorFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView3_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit2_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit2_Get_Selected();
        }

        private string PopupContainerEdit2_Get_Selected()
        {
            i_str_selected_contractor_ids = "";    // Reset any prior values first //
            i_str_selected_contractor_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_contractor_ids = "";
                return "No Team Filter";

            }
            else if (selection2.SelectedCount <= 0)
            {
                i_str_selected_contractor_ids = "";
                return "No Team Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_contractor_ids += Convert.ToString(view.GetRowCellValue(i, "intContractorID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_contractor_names = Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_contractor_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Name"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_contractor_names) ? "No Team Filter" : i_str_selected_contractor_names);
        }

        #endregion


        #region Contractor Team Member Filter Panel

        private void btnContractorTeamMemberOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView4_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit3_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit3_Get_Selected();
        }

        private string PopupContainerEdit3_Get_Selected()
        {
            i_str_selected_contractor_team_member_ids = "";    // Reset any prior values first //
            i_str_selected_contractor_team_member_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl4.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_contractor_team_member_ids = "";
                return "No Team Members Filter";

            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_contractor_team_member_ids = "";
                return "No Team Members Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_contractor_team_member_ids += Convert.ToString(view.GetRowCellValue(i, "TeamMemberID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_contractor_team_member_names = Convert.ToString(view.GetRowCellValue(i, "TeamMemberName"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_contractor_team_member_names += ", " + Convert.ToString(view.GetRowCellValue(i, "TeamMemberName"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_contractor_team_member_names) ? "No Team Members Filter" : i_str_selected_contractor_team_member_names);
        }

        #endregion


        #region Certificate Type Filter Panel

        private void btnCertificateTypeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
        }

        private void repositoryItemPopupContainerEdit5_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEdit5_Get_Selected();
        }

        private string PopupContainerEdit5_Get_Selected()
        {
            i_str_selected_certificate_type_ids = "";    // Reset any prior values first //
            i_str_selected_certificate_type_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_certificate_type_ids = "";
                return "No Certificate Type Filter";

            }
            else if (selection4.SelectedCount <= 0)
            {
                i_str_selected_certificate_type_ids = "";
                return "No Certificate Type Filter";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_certificate_type_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_certificate_type_names = Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_certificate_type_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Description"));
                        }
                        intCount++;
                    }
                }
            }
            return (string.IsNullOrEmpty(i_str_selected_certificate_type_names) ? "No Certificate Type Filter" : i_str_selected_certificate_type_names);
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";
            GridView view = (GridView)gridControl2.MainView;

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));

            boolDateFilter = ((i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001")) && (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001")) ? false : true);

            return i_str_date_range;
        }

        #endregion


        #region GridView1
        
        private void gridView1_CustomDrawEmptyForeground(object sender, DevExpress.XtraGrid.Views.Base.CustomDrawEventArgs e)
        {           
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Qualifications - Click Load Data button");
        }

        private void gridView1_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;

            if (e.Column == colFulfilled)
            {
                string fulfilled = view.GetRowCellValue(e.RowHandle, colFulfilled).ToString();

                if (fulfilled == "N")
                {
                    gridview_Cell_Error(e);
                }
                if (fulfilled == "Y")
                {
                    gridview_Cell_Highlight(e);
                }
            }


            
        }

        private void gridview1_Check_DaysToDo(RowCellCustomDrawEventArgs e,int daysToDo)
        {
            if (daysToDo < 0)
            {
                gridview_Cell_Error(e);
            }
            else if (daysToDo < 30)
            {
                gridview_Cell_Warning(e);
            }
        }


        private void gridview_Cell_Highlight(RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = Color.White;
            e.Appearance.BackColor2 = Color.LightGreen;
            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
        }
        private void gridview_Cell_Error(RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
        }
        private void gridview_Cell_Warning(RowCellCustomDrawEventArgs e)
        {
            e.Appearance.BackColor = Color.Khaki;
            e.Appearance.BackColor2 = Color.DarkOrange;
            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
        }


        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
        
        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            i_int_FocusedGrid = 1;
            SetMenuStatus();
        }

        private void gridView1_MouseUp(object sender, MouseEventArgs e)
        {
            i_int_FocusedGrid = 1;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                bbiDatasetCreate.Enabled = (view.SelectedRowsCount > 0 ? true : false);
                if (view.RowCount > 0)
                {
                    bbiDatasetSelection.Enabled = true;
                }
                else
                {
                    bbiDatasetSelection.Enabled = false;
                }
                bsiDataset.Enabled = true;
                bbiDatasetSelectionInverted.Enabled = true;
                bbiDatasetManager.Enabled = true;

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;

            GridView view = (GridView)sender;
            switch (e.Column.FieldName)
            {
                case "DaysUntilExpiry":
                case "DaysUntilRefreshDue":
                case "DaysUntilObtainBy": 
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle,e.Column)) == 99999) e.RepositoryItem = emptyEditor;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) == 0) e.RepositoryItem = emptyEditor;
                    break;
                default:
                    break;
            }

            int idValue = 0;

            if (e.Column == colQualificationSubTypeDescription)
            {
                Int32.TryParse(view.GetRowCellValue(e.RowHandle, colQualificationSubTypeID).ToString(), out idValue);
                if (idValue == 0) e.RepositoryItem = emptyEditor;
            }


        }

        private void gridView1_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) == 0) e.Cancel = true;
                    break;
                default:
                    break;
            }

            if (view.FocusedColumn == colQualificationSubTypeDescription)
            {
                int qID = 0;
                int.TryParse(view.GetFocusedRowCellValue(colQualificationSubTypeID).ToString(), out qID);
                if (qID == 0) e.Cancel = true;
            }



        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControl1.MainView;
                        int intRecordType = 4;  // Training //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsTraining_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 4;  // Training //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "QualificationID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToPersonName").ToString() + " - " + view.GetRowCellValue(view.FocusedRowHandle, "CourseName").ToString() + " [" + view.GetRowCellValue(view.FocusedRowHandle, "CourseNumber").ToString() + "]";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;

            string strRecordIDs = "";



            if (view.FocusedColumn == colQualificationSubTypeDescription)
            {

                int qID = 0;
                int.TryParse(view.GetFocusedRowCellValue(colQualificationSubTypeID).ToString(), out qID);
                if (qID == 0) return;

                strRecordIDs = qID.ToString() + ",";

                frm_HR_Master_Qualification_SubType_Edit frmChild = new frm_HR_Master_Qualification_SubType_Edit();

                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                frmChild.splashScreenManager = splashScreenManager1;
                frmChild.splashScreenManager.ShowWaitForm();
                frmChild.MdiParent = this.MdiParent;
                frmChild.GlobalSettings = GlobalSettings;
                frmChild.strRecordIDs = strRecordIDs;

                Drill_Down(frmChild);
            }


        }

        private void Drill_Down(BaseObjects.frmBase frmChild)
        {
            System.Reflection.MethodInfo method = null;

            if (iBool_AllowEdit) { frmChild.strFormMode = "edit"; }
            else { frmChild.strFormMode = "view"; }
            
            frmChild.Show();

            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(frmChild, new object[] { null });
        }

        #endregion

        #region DataSet

        public override void OnDatasetCreateEvent(object sender, EventArgs e)
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strCurrentID = "";
            string strSelectedIDs = ",";
            string strColumnName = "";
            switch (i_int_FocusedGrid)
            {
                case 1:
                    view = (GridView)gridControl1.MainView;
                    strColumnName = "LinkedToPersonID";
                    break;
                default:
                    return;
            }
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records to add to the dataset before proceeding!", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            intCount = 0;
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, view.Columns["LinkedToPersonTypeID"])) != 0) continue;
                strCurrentID = view.GetRowCellValue(intRowHandle, view.Columns[strColumnName]).ToString() + ',';
                if (!strSelectedIDs.Contains("," + strCurrentID))  // Put leading comma in temporarily for checking purposes //
                {
                    strSelectedIDs += strCurrentID;
                    intCount++;
                }
            }
            strSelectedIDs = strSelectedIDs.Remove(0, 1);  // Remove leading ',' //
            CreateDataset("Employee", intCount, strSelectedIDs);
        }

        private void CreateDataset(string strType, int intRecordCount, string strSelectedRecordIDs)
        {
            frmDatasetCreate fChildForm = new frmDatasetCreate();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_dataset_type = strType;
            fChildForm.i_int_selected_employee_count = intRecordCount;

            if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                int intAction = fChildForm.i_int_returned_action;
                string strName = fChildForm.i_str_dataset_name;
                strType = fChildForm.i_str_dataset_type;
                int intDatasetID = fChildForm.i_int_selected_dataset;
                string strDatasetType = fChildForm.i_str_dataset_type;
                int intReturnValue = 0;
                switch (intAction)
                {
                    case 1:  // Create New dataset and dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AddDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AddDataset.ChangeConnectionString(strConnectionString);

                        intReturnValue = Convert.ToInt32(AddDataset.sp01224_AT_Dataset_create(strDatasetType, strName, this.GlobalSettings.UserID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to create the dataset!\n\nNo Dataset Created.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 2:  // Append to dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter AppendDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        AppendDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(AppendDataset.sp01226_AT_Dataset_append_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to append the selected records to the dataset!\n\nNo Records Append to the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    case 3: // Replace dataset members //
                        Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter ReplaceDataset = new Utilities.DataSet_Utilities_DatasetTableAdapters.QueriesTableAdapter();
                        ReplaceDataset.ChangeConnectionString(strConnectionString);
                        intReturnValue = Convert.ToInt32(ReplaceDataset.sp01227_AT_Dataset_replace_dataset_members(intDatasetID, strSelectedRecordIDs));
                        if (intReturnValue <= 0)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("A problem occurred while attempting to replace the records in the selected dataset!\n\nNo Records Replaced in the Dataset.", "Create Dataset", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        return;
                    default:
                        return;
                }
            }
        }

        public override void OnDatasetSelectionEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Employee,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDataset(intSelectedDataset, "LinkedToPersonID");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetSelectionInvertedEvent(object sender, EventArgs e)
        {
            frmDatasetSelection fChildForm = new frmDatasetSelection();
            Form frmMain = this.MdiParent;
            int intSelectedDataset;
            switch (i_int_FocusedGrid)
            {
                case 1:
                    frmMain.AddOwnedForm(fChildForm);
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.i_str_dataset_types = "Employee,"; // Comma seperated list needs to be passed //

                    if (fChildForm.ShowDialog() != DialogResult.Yes)  // User aborted //
                    {
                        return;
                    }
                    intSelectedDataset = fChildForm.i_int_selected_dataset;
                    DS_Selection1.SelectRecordsFromDatasetInverted(intSelectedDataset, "LinkedToPersonID");
                    break;
                default:
                    break;
            }
        }

        public override void OnDatasetManagerEvent(object sender, EventArgs e)
        {
            var fChildForm = new frm_DatasetManager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.FormPermissions = this.FormPermissions;
            fChildForm._i_PassedInFilterTypes = "Employee,";
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        #endregion


        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void bciFilterSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (bciFilterSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more records to filter by before proceeding.", "Filter Selected Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControl1.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
            }
            else  // Clear Filter //
            {
                try
                {
                    gridControl1.BeginUpdate();
                    view.ActiveFilter.Clear();
                    //foreach (DataRow dr in dataSet_HR_Core.sp09100_HR_Qualification_Manager.Rows)
                    foreach (DataRow dr in dataSet_TR_Core.sp_TR_00033_Training_Matrix_Team_Select.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControl1.EndUpdate();
        }

    }
}


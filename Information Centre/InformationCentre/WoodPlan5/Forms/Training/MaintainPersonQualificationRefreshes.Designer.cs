﻿namespace WoodPlan5
{
    partial class MaintainPersonQualificationRefreshes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintainPersonQualificationRefreshes));
            this.gridControlRefreshes = new DevExpress.XtraGrid.GridControl();
            this.spTR00031PersonQualificationRefreshesSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRefreshID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshDueDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp_TR_00031_Person_Qualification_Refreshes_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00031_Person_Qualification_Refreshes_SelectTableAdapter();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRefreshes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00031PersonQualificationRefreshesSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlRefreshes
            // 
            this.gridControlRefreshes.DataSource = this.spTR00031PersonQualificationRefreshesSelectBindingSource;
            this.gridControlRefreshes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlRefreshes.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAwardingBody_EmbeddedNavigator_ButtonClick);
            this.gridControlRefreshes.Location = new System.Drawing.Point(0, 0);
            this.gridControlRefreshes.MainView = this.gridView;
            this.gridControlRefreshes.Name = "gridControlRefreshes";
            this.gridControlRefreshes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlRefreshes.Size = new System.Drawing.Size(469, 241);
            this.gridControlRefreshes.TabIndex = 1;
            this.gridControlRefreshes.UseEmbeddedNavigator = true;
            this.gridControlRefreshes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // spTR00031PersonQualificationRefreshesSelectBindingSource
            // 
            this.spTR00031PersonQualificationRefreshesSelectBindingSource.DataMember = "sp_TR_00031_Person_Qualification_Refreshes_Select";
            this.spTR00031PersonQualificationRefreshesSelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridView
            // 
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRefreshID,
            this.colRefreshTypeDescription,
            this.colStartDate,
            this.colRefreshDueDate,
            this.colSubTypeDescription,
            this.colTypeDescription});
            this.gridView.GridControl = this.gridControlRefreshes;
            this.gridView.Name = "gridView";
            this.gridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView.OptionsLayout.StoreAppearance = true;
            this.gridView.OptionsLayout.StoreFormatRules = true;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSubTypeDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView_PopupMenuShowing);
            this.gridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colRefreshID
            // 
            this.colRefreshID.FieldName = "RefreshID";
            this.colRefreshID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRefreshID.Name = "colRefreshID";
            this.colRefreshID.OptionsColumn.AllowEdit = false;
            this.colRefreshID.OptionsColumn.AllowFocus = false;
            this.colRefreshID.OptionsColumn.ReadOnly = true;
            // 
            // colRefreshTypeDescription
            // 
            this.colRefreshTypeDescription.Caption = "Refresh Type";
            this.colRefreshTypeDescription.FieldName = "RefreshTypeDescription";
            this.colRefreshTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRefreshTypeDescription.Name = "colRefreshTypeDescription";
            this.colRefreshTypeDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshTypeDescription.OptionsColumn.AllowFocus = false;
            this.colRefreshTypeDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshTypeDescription.Visible = true;
            this.colRefreshTypeDescription.VisibleIndex = 0;
            this.colRefreshTypeDescription.Width = 89;
            // 
            // colStartDate
            // 
            this.colStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.colStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 1;
            this.colStartDate.Width = 113;
            // 
            // colRefreshDueDate
            // 
            this.colRefreshDueDate.AppearanceCell.Options.UseTextOptions = true;
            this.colRefreshDueDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefreshDueDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colRefreshDueDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRefreshDueDate.Caption = "Expiry / Lapse Date";
            this.colRefreshDueDate.FieldName = "ExpiryDate";
            this.colRefreshDueDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRefreshDueDate.Name = "colRefreshDueDate";
            this.colRefreshDueDate.OptionsColumn.AllowEdit = false;
            this.colRefreshDueDate.OptionsColumn.AllowFocus = false;
            this.colRefreshDueDate.OptionsColumn.ReadOnly = true;
            this.colRefreshDueDate.Visible = true;
            this.colRefreshDueDate.VisibleIndex = 2;
            this.colRefreshDueDate.Width = 103;
            // 
            // colSubTypeDescription
            // 
            this.colSubTypeDescription.Caption = "Refreshes Qualification ";
            this.colSubTypeDescription.FieldName = "SubTypeDescription";
            this.colSubTypeDescription.Name = "colSubTypeDescription";
            this.colSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSubTypeDescription.Visible = true;
            this.colSubTypeDescription.VisibleIndex = 3;
            this.colSubTypeDescription.Width = 348;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Qualification Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 4;
            this.colTypeDescription.Width = 134;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // sp_TR_00031_Person_Qualification_Refreshes_SelectTableAdapter
            // 
            this.sp_TR_00031_Person_Qualification_Refreshes_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "footer_16.png");
            this.imageList2.Images.SetKeyName(4, "expand_and_select_16.png");
            this.imageList2.Images.SetKeyName(5, "colour_expression_16.png");
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlRefreshes;
            // 
            // MaintainPersonQualificationRefreshes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlRefreshes);
            this.Name = "MaintainPersonQualificationRefreshes";
            this.Size = new System.Drawing.Size(469, 241);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRefreshes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00031PersonQualificationRefreshesSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlRefreshes;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DataSet_TR_Core dataSet_TR_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshID;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshDueDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private System.Windows.Forms.BindingSource spTR00031PersonQualificationRefreshesSelectBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00031_Person_Qualification_Refreshes_SelectTableAdapter sp_TR_00031_Person_Qualification_Refreshes_SelectTableAdapter;
        private System.Windows.Forms.ImageList imageList2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

﻿namespace WoodPlan5
{
    partial class MaintainPersonQualificationRefreshedBy
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintainPersonQualificationRefreshedBy));
            this.gridControlRefreshedBy = new DevExpress.XtraGrid.GridControl();
            this.spTR00030PersonQualificationRefreshedBySelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colRefreshID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp_TR_00030_Person_Qualification_Refreshed_By_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00030_Person_Qualification_Refreshed_By_SelectTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRefreshedBy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00030PersonQualificationRefreshedBySelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlRefreshedBy
            // 
            this.gridControlRefreshedBy.DataSource = this.spTR00030PersonQualificationRefreshedBySelectBindingSource;
            this.gridControlRefreshedBy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlRefreshedBy.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlRefreshedBy.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAwardingBody_EmbeddedNavigator_ButtonClick);
            this.gridControlRefreshedBy.Location = new System.Drawing.Point(0, 0);
            this.gridControlRefreshedBy.MainView = this.gridView1;
            this.gridControlRefreshedBy.Name = "gridControlRefreshedBy";
            this.gridControlRefreshedBy.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlRefreshedBy.Size = new System.Drawing.Size(555, 243);
            this.gridControlRefreshedBy.TabIndex = 1;
            this.gridControlRefreshedBy.UseEmbeddedNavigator = true;
            this.gridControlRefreshedBy.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spTR00030PersonQualificationRefreshedBySelectBindingSource
            // 
            this.spTR00030PersonQualificationRefreshedBySelectBindingSource.DataMember = "sp_TR_00030_Person_Qualification_Refreshed_By_Select";
            this.spTR00030PersonQualificationRefreshedBySelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRefreshID,
            this.colRefreshTypeDescription,
            this.colStartDate,
            this.colExpiryDate,
            this.colSubTypeDescription,
            this.colTypeDescription,
            this.colJobTitle,
            this.colRequirement});
            this.gridView1.GridControl = this.gridControlRefreshedBy;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colRefreshID
            // 
            this.colRefreshID.FieldName = "RefreshID";
            this.colRefreshID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRefreshID.Name = "colRefreshID";
            this.colRefreshID.OptionsColumn.AllowEdit = false;
            this.colRefreshID.OptionsColumn.AllowFocus = false;
            this.colRefreshID.OptionsColumn.ReadOnly = true;
            // 
            // colRefreshTypeDescription
            // 
            this.colRefreshTypeDescription.Caption = "Refreshed Type";
            this.colRefreshTypeDescription.FieldName = "RefreshTypeDescription";
            this.colRefreshTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRefreshTypeDescription.Name = "colRefreshTypeDescription";
            this.colRefreshTypeDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshTypeDescription.OptionsColumn.AllowFocus = false;
            this.colRefreshTypeDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshTypeDescription.Visible = true;
            this.colRefreshTypeDescription.VisibleIndex = 0;
            this.colRefreshTypeDescription.Width = 89;
            // 
            // colStartDate
            // 
            this.colStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.colStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 1;
            this.colStartDate.Width = 113;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.AppearanceCell.Options.UseTextOptions = true;
            this.colExpiryDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpiryDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpiryDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpiryDate.Caption = "Expiry / Lapse Date";
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 2;
            this.colExpiryDate.Width = 103;
            // 
            // colSubTypeDescription
            // 
            this.colSubTypeDescription.Caption = "Refreshed By Qualification ";
            this.colSubTypeDescription.FieldName = "SubTypeDescription";
            this.colSubTypeDescription.Name = "colSubTypeDescription";
            this.colSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colSubTypeDescription.Visible = true;
            this.colSubTypeDescription.VisibleIndex = 3;
            this.colSubTypeDescription.Width = 242;
            // 
            // colTypeDescription
            // 
            this.colTypeDescription.Caption = "Qualification Type";
            this.colTypeDescription.FieldName = "TypeDescription";
            this.colTypeDescription.Name = "colTypeDescription";
            this.colTypeDescription.OptionsColumn.AllowEdit = false;
            this.colTypeDescription.OptionsColumn.AllowFocus = false;
            this.colTypeDescription.OptionsColumn.ReadOnly = true;
            this.colTypeDescription.Visible = true;
            this.colTypeDescription.VisibleIndex = 4;
            this.colTypeDescription.Width = 159;
            // 
            // colJobTitle
            // 
            this.colJobTitle.Caption = "Job Title (When  Assessed)";
            this.colJobTitle.FieldName = "JobTitle";
            this.colJobTitle.Name = "colJobTitle";
            this.colJobTitle.OptionsColumn.AllowEdit = false;
            this.colJobTitle.OptionsColumn.AllowFocus = false;
            this.colJobTitle.OptionsColumn.ReadOnly = true;
            this.colJobTitle.Visible = true;
            this.colJobTitle.VisibleIndex = 5;
            this.colJobTitle.Width = 118;
            // 
            // colRequirement
            // 
            this.colRequirement.Caption = "Requirement (When Assessed)";
            this.colRequirement.FieldName = "Requirement";
            this.colRequirement.Name = "colRequirement";
            this.colRequirement.OptionsColumn.AllowEdit = false;
            this.colRequirement.OptionsColumn.AllowFocus = false;
            this.colRequirement.OptionsColumn.ReadOnly = true;
            this.colRequirement.Visible = true;
            this.colRequirement.VisibleIndex = 6;
            this.colRequirement.Width = 62;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // sp_TR_00030_Person_Qualification_Refreshed_By_SelectTableAdapter
            // 
            this.sp_TR_00030_Person_Qualification_Refreshed_By_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlRefreshedBy;
            // 
            // MaintainPersonQualificationRefreshedBy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlRefreshedBy);
            this.Name = "MaintainPersonQualificationRefreshedBy";
            this.Size = new System.Drawing.Size(555, 243);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRefreshedBy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00030PersonQualificationRefreshedBySelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlRefreshedBy;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_TR_Core dataSet_TR_Core;
        private System.Windows.Forms.BindingSource spTR00030PersonQualificationRefreshedBySelectBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00030_Person_Qualification_Refreshed_By_SelectTableAdapter sp_TR_00030_Person_Qualification_Refreshed_By_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshID;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private DevExpress.XtraGrid.Columns.GridColumn colSubTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeDescription;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirement;
    }
}

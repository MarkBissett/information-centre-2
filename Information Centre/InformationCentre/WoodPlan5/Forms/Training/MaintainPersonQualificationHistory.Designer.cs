﻿namespace WoodPlan5
{
    partial class MaintainPersonQualificationHistory
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintainPersonQualificationHistory));
            this.gridControlHistory = new DevExpress.XtraGrid.GridControl();
            this.spTR00029PersonQualificationHistorySelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQualificationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHistoryID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAwardingBody = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssessmentType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAssessmentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCourseName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCostToCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirement = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_TR_00029_Person_Qualification_History_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00029_Person_Qualification_History_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00029PersonQualificationHistorySelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlHistory
            // 
            this.gridControlHistory.DataSource = this.spTR00029PersonQualificationHistorySelectBindingSource;
            this.gridControlHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlHistory.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlHistory.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlHistory.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAwardingBody_EmbeddedNavigator_ButtonClick);
            this.gridControlHistory.Location = new System.Drawing.Point(0, 0);
            this.gridControlHistory.MainView = this.gridView1;
            this.gridControlHistory.Name = "gridControlHistory";
            this.gridControlHistory.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlHistory.Size = new System.Drawing.Size(555, 243);
            this.gridControlHistory.TabIndex = 1;
            this.gridControlHistory.UseEmbeddedNavigator = true;
            this.gridControlHistory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spTR00029PersonQualificationHistorySelectBindingSource
            // 
            this.spTR00029PersonQualificationHistorySelectBindingSource.DataMember = "sp_TR_00029_Person_Qualification_History_Select";
            this.spTR00029PersonQualificationHistorySelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQualificationID,
            this.colHistoryID,
            this.colAwardingBody,
            this.colRefreshTypeDescription,
            this.colStartDate,
            this.colExpiryDate,
            this.colAssessmentType,
            this.colAssessmentDate,
            this.colCourseNumber,
            this.colCourseName,
            this.colCostToCompany,
            this.colJobTitle,
            this.colRequirement});
            this.gridView1.GridControl = this.gridControlHistory;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colQualificationID
            // 
            this.colQualificationID.FieldName = "QualificationID";
            this.colQualificationID.Name = "colQualificationID";
            this.colQualificationID.OptionsColumn.AllowEdit = false;
            this.colQualificationID.OptionsColumn.AllowFocus = false;
            this.colQualificationID.OptionsColumn.ReadOnly = true;
            // 
            // colHistoryID
            // 
            this.colHistoryID.FieldName = "HistoryID";
            this.colHistoryID.Name = "colHistoryID";
            this.colHistoryID.OptionsColumn.AllowEdit = false;
            this.colHistoryID.OptionsColumn.AllowFocus = false;
            this.colHistoryID.OptionsColumn.ReadOnly = true;
            // 
            // colAwardingBody
            // 
            this.colAwardingBody.Caption = "Awarding Body";
            this.colAwardingBody.FieldName = "AwardingBody";
            this.colAwardingBody.Name = "colAwardingBody";
            this.colAwardingBody.OptionsColumn.AllowEdit = false;
            this.colAwardingBody.OptionsColumn.AllowFocus = false;
            this.colAwardingBody.OptionsColumn.ReadOnly = true;
            this.colAwardingBody.Visible = true;
            this.colAwardingBody.VisibleIndex = 3;
            this.colAwardingBody.Width = 70;
            // 
            // colRefreshTypeDescription
            // 
            this.colRefreshTypeDescription.Caption = "Type";
            this.colRefreshTypeDescription.FieldName = "RefreshTypeDescription";
            this.colRefreshTypeDescription.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colRefreshTypeDescription.Name = "colRefreshTypeDescription";
            this.colRefreshTypeDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshTypeDescription.OptionsColumn.AllowFocus = false;
            this.colRefreshTypeDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshTypeDescription.Visible = true;
            this.colRefreshTypeDescription.VisibleIndex = 0;
            this.colRefreshTypeDescription.Width = 73;
            // 
            // colStartDate
            // 
            this.colStartDate.AppearanceCell.Options.UseTextOptions = true;
            this.colStartDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colStartDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 1;
            this.colStartDate.Width = 70;
            // 
            // colExpiryDate
            // 
            this.colExpiryDate.AppearanceCell.Options.UseTextOptions = true;
            this.colExpiryDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpiryDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colExpiryDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colExpiryDate.Caption = "Expiry / Lapse Date";
            this.colExpiryDate.FieldName = "ExpiryDate";
            this.colExpiryDate.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colExpiryDate.Name = "colExpiryDate";
            this.colExpiryDate.OptionsColumn.AllowEdit = false;
            this.colExpiryDate.OptionsColumn.AllowFocus = false;
            this.colExpiryDate.OptionsColumn.ReadOnly = true;
            this.colExpiryDate.Visible = true;
            this.colExpiryDate.VisibleIndex = 2;
            this.colExpiryDate.Width = 103;
            // 
            // colAssessmentType
            // 
            this.colAssessmentType.Caption = "Assessment Type";
            this.colAssessmentType.FieldName = "AssessmentType";
            this.colAssessmentType.Name = "colAssessmentType";
            this.colAssessmentType.OptionsColumn.AllowEdit = false;
            this.colAssessmentType.OptionsColumn.AllowFocus = false;
            this.colAssessmentType.OptionsColumn.ReadOnly = true;
            this.colAssessmentType.Visible = true;
            this.colAssessmentType.VisibleIndex = 4;
            // 
            // colAssessmentDate
            // 
            this.colAssessmentDate.Caption = "Assessment Date";
            this.colAssessmentDate.FieldName = "AssessmentDate";
            this.colAssessmentDate.Name = "colAssessmentDate";
            this.colAssessmentDate.OptionsColumn.AllowEdit = false;
            this.colAssessmentDate.OptionsColumn.AllowFocus = false;
            this.colAssessmentDate.OptionsColumn.ReadOnly = true;
            this.colAssessmentDate.Visible = true;
            this.colAssessmentDate.VisibleIndex = 5;
            // 
            // colCourseNumber
            // 
            this.colCourseNumber.Caption = "Course Id.";
            this.colCourseNumber.FieldName = "CourseNumber";
            this.colCourseNumber.Name = "colCourseNumber";
            this.colCourseNumber.OptionsColumn.AllowEdit = false;
            this.colCourseNumber.OptionsColumn.AllowFocus = false;
            this.colCourseNumber.OptionsColumn.ReadOnly = true;
            this.colCourseNumber.Visible = true;
            this.colCourseNumber.VisibleIndex = 6;
            // 
            // colCourseName
            // 
            this.colCourseName.Caption = "Course ";
            this.colCourseName.FieldName = "CourseName";
            this.colCourseName.Name = "colCourseName";
            this.colCourseName.OptionsColumn.AllowEdit = false;
            this.colCourseName.OptionsColumn.AllowFocus = false;
            this.colCourseName.OptionsColumn.ReadOnly = true;
            this.colCourseName.Visible = true;
            this.colCourseName.VisibleIndex = 7;
            this.colCourseName.Width = 173;
            // 
            // colCostToCompany
            // 
            this.colCostToCompany.Caption = "Cost To Company";
            this.colCostToCompany.DisplayFormat.FormatString = "N2";
            this.colCostToCompany.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCostToCompany.FieldName = "CostToCompany";
            this.colCostToCompany.Name = "colCostToCompany";
            this.colCostToCompany.OptionsColumn.AllowEdit = false;
            this.colCostToCompany.OptionsColumn.AllowFocus = false;
            this.colCostToCompany.OptionsColumn.ReadOnly = true;
            this.colCostToCompany.Visible = true;
            this.colCostToCompany.VisibleIndex = 8;
            this.colCostToCompany.Width = 59;
            // 
            // colJobTitle
            // 
            this.colJobTitle.Caption = "Job Title (When  Assessed)";
            this.colJobTitle.FieldName = "JobTitle";
            this.colJobTitle.Name = "colJobTitle";
            this.colJobTitle.OptionsColumn.AllowEdit = false;
            this.colJobTitle.OptionsColumn.AllowFocus = false;
            this.colJobTitle.OptionsColumn.ReadOnly = true;
            this.colJobTitle.Visible = true;
            this.colJobTitle.VisibleIndex = 9;
            this.colJobTitle.Width = 95;
            // 
            // colRequirement
            // 
            this.colRequirement.Caption = "Requirement (When Assessed)";
            this.colRequirement.FieldName = "Requirement";
            this.colRequirement.Name = "colRequirement";
            this.colRequirement.OptionsColumn.AllowEdit = false;
            this.colRequirement.OptionsColumn.AllowFocus = false;
            this.colRequirement.OptionsColumn.ReadOnly = true;
            this.colRequirement.Visible = true;
            this.colRequirement.VisibleIndex = 10;
            this.colRequirement.Width = 67;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "footer_16.png");
            this.imageList2.Images.SetKeyName(4, "expand_and_select_16.png");
            this.imageList2.Images.SetKeyName(5, "colour_expression_16.png");
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlHistory;
            // 
            // sp_TR_00029_Person_Qualification_History_SelectTableAdapter
            // 
            this.sp_TR_00029_Person_Qualification_History_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // MaintainPersonQualificationHistory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlHistory);
            this.Name = "MaintainPersonQualificationHistory";
            this.Size = new System.Drawing.Size(555, 243);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00029PersonQualificationHistorySelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlHistory;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet_TR_Core dataSet_TR_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryDate;
        private System.Windows.Forms.ImageList imageList2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colJobTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirement;
        private System.Windows.Forms.BindingSource spTR00029PersonQualificationHistorySelectBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00029_Person_Qualification_History_SelectTableAdapter sp_TR_00029_Person_Qualification_History_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationID;
        private DevExpress.XtraGrid.Columns.GridColumn colHistoryID;
        private DevExpress.XtraGrid.Columns.GridColumn colAssessmentType;
        private DevExpress.XtraGrid.Columns.GridColumn colAssessmentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCourseName;
        private DevExpress.XtraGrid.Columns.GridColumn colCostToCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colAwardingBody;
    }
}

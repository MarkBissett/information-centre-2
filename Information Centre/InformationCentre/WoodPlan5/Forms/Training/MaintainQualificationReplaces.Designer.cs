﻿namespace WoodPlan5
{
    partial class MaintainQualificationReplaces
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintainQualificationReplaces));
            this.gridControlReplaces = new DevExpress.XtraGrid.GridControl();
            this.spTR00018QualificationSubTypeReplacesSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMemberQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryPeriodDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshIntervalDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReplacedQualificationStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReplacedQualificationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_TR_00018_Qualification_Sub_Type_Replaces_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00018_Qualification_Sub_Type_Replaces_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReplaces)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00018QualificationSubTypeReplacesSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlReplaces
            // 
            this.gridControlReplaces.DataSource = this.spTR00018QualificationSubTypeReplacesSelectBindingSource;
            this.gridControlReplaces.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlReplaces.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlReplaces.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlReplaces.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAwardingBody_EmbeddedNavigator_ButtonClick);
            this.gridControlReplaces.Location = new System.Drawing.Point(0, 0);
            this.gridControlReplaces.MainView = this.gridView1;
            this.gridControlReplaces.Name = "gridControlReplaces";
            this.gridControlReplaces.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlReplaces.Size = new System.Drawing.Size(469, 241);
            this.gridControlReplaces.TabIndex = 1;
            this.gridControlReplaces.UseEmbeddedNavigator = true;
            this.gridControlReplaces.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spTR00018QualificationSubTypeReplacesSelectBindingSource
            // 
            this.spTR00018QualificationSubTypeReplacesSelectBindingSource.DataMember = "sp_TR_00018_Qualification_Sub_Type_Replaces_Select";
            this.spTR00018QualificationSubTypeReplacesSelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colMemberQualificationSubTypeID,
            this.colOrderID,
            this.colQualificationTypeDescription,
            this.colDescription,
            this.colStatus,
            this.colValidity,
            this.colExpiryPeriodDescription,
            this.colRefreshDescription,
            this.colRefreshIntervalDescription,
            this.colReplacedQualificationStartDate,
            this.colReplacedQualificationEndDate});
            this.gridView1.GridControl = this.gridControlReplaces;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colMemberQualificationSubTypeID
            // 
            this.colMemberQualificationSubTypeID.FieldName = "MemberQualificationSubTypeID";
            this.colMemberQualificationSubTypeID.Name = "colMemberQualificationSubTypeID";
            this.colMemberQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colMemberQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colOrderID
            // 
            this.colOrderID.FieldName = "OrderID";
            this.colOrderID.Name = "colOrderID";
            this.colOrderID.OptionsColumn.AllowEdit = false;
            this.colOrderID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationTypeDescription
            // 
            this.colQualificationTypeDescription.Caption = "Master Type";
            this.colQualificationTypeDescription.FieldName = "QualificationTypeDescription";
            this.colQualificationTypeDescription.Name = "colQualificationTypeDescription";
            this.colQualificationTypeDescription.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeDescription.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeDescription.Visible = true;
            this.colQualificationTypeDescription.VisibleIndex = 0;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 175;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 6;
            this.colStatus.Width = 133;
            // 
            // colValidity
            // 
            this.colValidity.FieldName = "Validity";
            this.colValidity.Name = "colValidity";
            this.colValidity.OptionsColumn.AllowEdit = false;
            this.colValidity.OptionsColumn.ReadOnly = true;
            this.colValidity.Visible = true;
            this.colValidity.VisibleIndex = 2;
            this.colValidity.Width = 73;
            // 
            // colExpiryPeriodDescription
            // 
            this.colExpiryPeriodDescription.Caption = "Expiry Period";
            this.colExpiryPeriodDescription.FieldName = "ExpiryPeriodDescription";
            this.colExpiryPeriodDescription.Name = "colExpiryPeriodDescription";
            this.colExpiryPeriodDescription.OptionsColumn.AllowEdit = false;
            this.colExpiryPeriodDescription.OptionsColumn.ReadOnly = true;
            this.colExpiryPeriodDescription.Visible = true;
            this.colExpiryPeriodDescription.VisibleIndex = 3;
            this.colExpiryPeriodDescription.Width = 78;
            // 
            // colRefreshDescription
            // 
            this.colRefreshDescription.Caption = "Refresh Type";
            this.colRefreshDescription.FieldName = "RefreshDescription";
            this.colRefreshDescription.Name = "colRefreshDescription";
            this.colRefreshDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshDescription.Visible = true;
            this.colRefreshDescription.VisibleIndex = 4;
            this.colRefreshDescription.Width = 86;
            // 
            // colRefreshIntervalDescription
            // 
            this.colRefreshIntervalDescription.Caption = "Refresh Interval";
            this.colRefreshIntervalDescription.FieldName = "RefreshIntervalDescription";
            this.colRefreshIntervalDescription.Name = "colRefreshIntervalDescription";
            this.colRefreshIntervalDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshIntervalDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshIntervalDescription.Visible = true;
            this.colRefreshIntervalDescription.VisibleIndex = 5;
            this.colRefreshIntervalDescription.Width = 91;
            // 
            // colReplacedQualificationStartDate
            // 
            this.colReplacedQualificationStartDate.Caption = "Start Date";
            this.colReplacedQualificationStartDate.FieldName = "ReplacedQualificationStartDate";
            this.colReplacedQualificationStartDate.Name = "colReplacedQualificationStartDate";
            this.colReplacedQualificationStartDate.OptionsColumn.AllowEdit = false;
            this.colReplacedQualificationStartDate.OptionsColumn.ReadOnly = true;
            this.colReplacedQualificationStartDate.Visible = true;
            this.colReplacedQualificationStartDate.VisibleIndex = 7;
            this.colReplacedQualificationStartDate.Width = 71;
            // 
            // colReplacedQualificationEndDate
            // 
            this.colReplacedQualificationEndDate.Caption = "End Date";
            this.colReplacedQualificationEndDate.FieldName = "ReplacedQualificationEndDate";
            this.colReplacedQualificationEndDate.Name = "colReplacedQualificationEndDate";
            this.colReplacedQualificationEndDate.OptionsColumn.AllowEdit = false;
            this.colReplacedQualificationEndDate.OptionsColumn.ReadOnly = true;
            this.colReplacedQualificationEndDate.Visible = true;
            this.colReplacedQualificationEndDate.VisibleIndex = 8;
            this.colReplacedQualificationEndDate.Width = 82;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlReplaces;
            // 
            // sp_TR_00018_Qualification_Sub_Type_Replaces_SelectTableAdapter
            // 
            this.sp_TR_00018_Qualification_Sub_Type_Replaces_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // MaintainQualificationReplaces
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlReplaces);
            this.Name = "MaintainQualificationReplaces";
            this.Size = new System.Drawing.Size(469, 241);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlReplaces)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00018QualificationSubTypeReplacesSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlReplaces;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colMemberQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colValidity;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryPeriodDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshIntervalDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colReplacedQualificationStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colReplacedQualificationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshDescription;
        private DataSet_TR_Core dataSet_TR_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeDescription;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private System.Windows.Forms.BindingSource spTR00018QualificationSubTypeReplacesSelectBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00018_Qualification_Sub_Type_Replaces_SelectTableAdapter sp_TR_00018_Qualification_Sub_Type_Replaces_SelectTableAdapter;
    }
}

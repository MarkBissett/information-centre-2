﻿namespace WoodPlan5
{
    partial class frm_HR_Qualification_Add_Type
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Qualification_Add_Type));
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.btnStaff = new DevExpress.XtraEditors.SimpleButton();
            this.brnContractor = new DevExpress.XtraEditors.SimpleButton();
            this.btnContractorTeamMember = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControlInformation = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(395, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 105);
            this.barDockControlBottom.Size = new System.Drawing.Size(395, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 79);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(395, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 79);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItemInformation});
            this.barManager1.MaxItemId = 31;
            this.barManager1.StatusBar = this.bar1;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information: Click a button to proceed.\r\n";
            this.barStaticItemInformation.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItemInformation.Glyph")));
            this.barStaticItemInformation.Id = 30;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnStaff
            // 
            this.btnStaff.Location = new System.Drawing.Point(9, 71);
            this.btnStaff.Name = "btnStaff";
            this.btnStaff.Size = new System.Drawing.Size(120, 23);
            this.btnStaff.TabIndex = 4;
            this.btnStaff.Text = "Add to Staff";
            this.btnStaff.Click += new System.EventHandler(this.btnStaff_Click);
            // 
            // brnContractor
            // 
            this.brnContractor.Location = new System.Drawing.Point(138, 71);
            this.brnContractor.Name = "brnContractor";
            this.brnContractor.Size = new System.Drawing.Size(120, 23);
            this.brnContractor.TabIndex = 5;
            this.brnContractor.Text = "Add to Team";
            this.brnContractor.Click += new System.EventHandler(this.brnContractor_Click);
            // 
            // btnContractorTeamMember
            // 
            this.btnContractorTeamMember.Location = new System.Drawing.Point(266, 71);
            this.btnContractorTeamMember.Name = "btnContractorTeamMember";
            this.btnContractorTeamMember.Size = new System.Drawing.Size(120, 23);
            this.btnContractorTeamMember.TabIndex = 6;
            this.btnContractorTeamMember.Text = "Add to Team Member";
            this.btnContractorTeamMember.Click += new System.EventHandler(this.btnContractorTeamMember_Click);
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(6, 32);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Size = new System.Drawing.Size(31, 33);
            this.pictureEdit1.TabIndex = 7;
            // 
            // labelControlInformation
            // 
            this.labelControlInformation.Location = new System.Drawing.Point(44, 42);
            this.labelControlInformation.Name = "labelControlInformation";
            this.labelControlInformation.Size = new System.Drawing.Size(218, 13);
            this.labelControlInformation.TabIndex = 8;
            this.labelControlInformation.Text = "Who do you want to add the Qualification to?";
            // 
            // frm_HR_Qualification_Add_Type
            // 
            this.ClientSize = new System.Drawing.Size(395, 135);
            this.Controls.Add(this.labelControlInformation);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.btnContractorTeamMember);
            this.Controls.Add(this.brnContractor);
            this.Controls.Add(this.btnStaff);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_HR_Qualification_Add_Type";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Qualification - Select Add Type";
            this.Load += new System.EventHandler(this.frm_HR_Qualification_Add_Type_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnStaff, 0);
            this.Controls.SetChildIndex(this.brnContractor, 0);
            this.Controls.SetChildIndex(this.btnContractorTeamMember, 0);
            this.Controls.SetChildIndex(this.pictureEdit1, 0);
            this.Controls.SetChildIndex(this.labelControlInformation, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraEditors.SimpleButton btnStaff;
        private DevExpress.XtraEditors.SimpleButton brnContractor;
        private DevExpress.XtraEditors.SimpleButton btnContractorTeamMember;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControlInformation;
    }
}

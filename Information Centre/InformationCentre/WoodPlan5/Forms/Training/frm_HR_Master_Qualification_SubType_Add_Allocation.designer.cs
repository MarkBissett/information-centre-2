namespace WoodPlan5
{
    partial class frm_HR_Master_Qualification_SubType_Add_Allocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Master_Qualification_SubType_Add_Allocation));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule4 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue4 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colAllocationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeLimitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.allocationGridControl = new DevExpress.XtraGrid.GridControl();
            this.spTR00021QualificationSubTypeAllocationAvailableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.allocationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.QualificationSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.startDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.spTR00108QualificationAllocationEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_DataEntry = new WoodPlan5.DataSet_TR_DataEntry();
            this.endDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.timeLimitSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.AllocationTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00006TrainingAllocationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeLimitTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00008AllocationTimeLimitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teamMinimumSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.RequirementTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00007AllocationRequirementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.buttonEdit2 = new DevExpress.XtraEditors.ButtonEdit();
            this.spHR00232MasterQualificationTypesEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.spHR00234MasterQualificationSubTypeEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.teamHoldingTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00050TeamHoldingTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.ItemForQualificationSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQualificationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQualificationSubType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.filterLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRequirementTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemFortimeLimitdType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTimeLimitValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamMinimum = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForTeamHoldingType = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForBreachReporting = new DevExpress.XtraLayout.LayoutControlItem();
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spHR00236MasterQualificationTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.spTR00001QualificationSubTypeStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter();
            this.sp_TR_00001_Qualification_SubType_StatusTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00001_Qualification_SubType_StatusTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_TR_CoreTableAdapters.TableAdapterManager();
            this.sp_TR_00006_Training_AllocationTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00006_Training_AllocationTableAdapter();
            this.sp_TR_00007_Allocation_RequirementTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00007_Allocation_RequirementTableAdapter();
            this.sp_TR_00008_Allocation_Time_LimitTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00008_Allocation_Time_LimitTableAdapter();
            this.sp_TR_00021_Qualification_Sub_Type_Allocation_AvailableTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00021_Qualification_Sub_Type_Allocation_AvailableTableAdapter();
            this.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter();
            this.sp_TR_00108_Qualification_Allocation_EditTableAdapter = new WoodPlan5.DataSet_TR_DataEntryTableAdapters.sp_TR_00108_Qualification_Allocation_EditTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_HR_00232_Master_Qualification_Types_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00232_Master_Qualification_Types_EditTableAdapter();
            this.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter();
            this.sp_TR_00050_Team_Holding_TypeTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00050_Team_Holding_TypeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allocationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00021QualificationSubTypeAllocationAvailableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allocationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00108QualificationAllocationEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeLimitSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00006TrainingAllocationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeLimitTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00008AllocationTimeLimitBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamMinimumSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequirementTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00007AllocationRequirementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00232MasterQualificationTypesEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00234MasterQualificationSubTypeEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamHoldingTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00050TeamHoldingTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequirementTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFortimeLimitdType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeLimitValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMinimum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamHoldingType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBreachReporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00020QualificationSubTypeAllocationSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00236MasterQualificationTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00001QualificationSubTypeStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(656, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 517);
            this.barDockControlBottom.Size = new System.Drawing.Size(656, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 491);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(656, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 491);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection1;
            // 
            // colAllocationTypeID
            // 
            this.colAllocationTypeID.FieldName = "AllocationTypeID";
            this.colAllocationTypeID.Name = "colAllocationTypeID";
            this.colAllocationTypeID.OptionsColumn.AllowEdit = false;
            this.colAllocationTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colTimeLimitTypeID
            // 
            this.colTimeLimitTypeID.FieldName = "TimeLimitTypeID";
            this.colTimeLimitTypeID.Name = "colTimeLimitTypeID";
            this.colTimeLimitTypeID.OptionsColumn.AllowEdit = false;
            this.colTimeLimitTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colRequirementTypeID
            // 
            this.colRequirementTypeID.FieldName = "RequirementTypeID";
            this.colRequirementTypeID.Name = "colRequirementTypeID";
            this.colRequirementTypeID.OptionsColumn.AllowEdit = false;
            this.colRequirementTypeID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "AllocationTypeID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(656, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 517);
            this.barDockControl2.Size = new System.Drawing.Size(656, 28);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 491);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(656, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 491);
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.allocationGridControl);
            this.dataLayoutControl1.Controls.Add(this.QualificationSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.startDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.endDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.timeLimitSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.AllocationTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.TimeLimitTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.teamMinimumSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.RequirementTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.buttonEdit1);
            this.dataLayoutControl1.Controls.Add(this.buttonEdit2);
            this.dataLayoutControl1.Controls.Add(this.textEdit1);
            this.dataLayoutControl1.Controls.Add(this.teamHoldingTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.checkEdit1);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForQualificationSubTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(288, 280, 688, 396);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(656, 491);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // allocationGridControl
            // 
            this.allocationGridControl.DataSource = this.spTR00021QualificationSubTypeAllocationAvailableBindingSource;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 5, true, true, "View Selected Record(s)", "view")});
            this.allocationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVettingSubTypes_EmbeddedNavigator_ButtonClick);
            this.allocationGridControl.Location = new System.Drawing.Point(12, 204);
            this.allocationGridControl.MainView = this.allocationGridView;
            this.allocationGridControl.Name = "allocationGridControl";
            this.allocationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.allocationGridControl.Size = new System.Drawing.Size(632, 275);
            this.allocationGridControl.TabIndex = 40;
            this.allocationGridControl.UseEmbeddedNavigator = true;
            this.allocationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.allocationGridView});
            // 
            // spTR00021QualificationSubTypeAllocationAvailableBindingSource
            // 
            this.spTR00021QualificationSubTypeAllocationAvailableBindingSource.DataMember = "sp_TR_00021_Qualification_Sub_Type_Allocation_Available";
            this.spTR00021QualificationSubTypeAllocationAvailableBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 3);
            this.imageCollection1.Images.SetKeyName(3, "attention_16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 4);
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Preview_16x16, "Preview_16x16", typeof(global::WoodPlan5.Properties.Resources), 5);
            this.imageCollection1.Images.SetKeyName(5, "Preview_16x16");
            // 
            // allocationGridView
            // 
            this.allocationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colName});
            this.allocationGridView.GridControl = this.allocationGridControl;
            this.allocationGridView.Name = "allocationGridView";
            this.allocationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.allocationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.allocationGridView.OptionsLayout.StoreAppearance = true;
            this.allocationGridView.OptionsLayout.StoreFormatRules = true;
            this.allocationGridView.OptionsSelection.MultiSelect = true;
            this.allocationGridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.allocationGridView.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.False;
            this.allocationGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 734;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // QualificationSubTypeIDTextEdit
            // 
            this.QualificationSubTypeIDTextEdit.Location = new System.Drawing.Point(141, 107);
            this.QualificationSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.QualificationSubTypeIDTextEdit.Name = "QualificationSubTypeIDTextEdit";
            this.QualificationSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QualificationSubTypeIDTextEdit, true);
            this.QualificationSubTypeIDTextEdit.Size = new System.Drawing.Size(475, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QualificationSubTypeIDTextEdit, optionsSpelling1);
            this.QualificationSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.QualificationSubTypeIDTextEdit.TabIndex = 4;
            // 
            // startDateDateEdit
            // 
            this.startDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "StartDate", true));
            this.startDateDateEdit.EditValue = null;
            this.startDateDateEdit.Location = new System.Drawing.Point(154, 108);
            this.startDateDateEdit.Name = "startDateDateEdit";
            this.startDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.startDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.startDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.startDateDateEdit.Size = new System.Drawing.Size(125, 20);
            this.startDateDateEdit.StyleController = this.dataLayoutControl1;
            this.startDateDateEdit.TabIndex = 21;
            this.startDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.startDateDateEdit_Validating);
            // 
            // spTR00108QualificationAllocationEditBindingSource
            // 
            this.spTR00108QualificationAllocationEditBindingSource.DataMember = "sp_TR_00108_Qualification_Allocation_Edit";
            this.spTR00108QualificationAllocationEditBindingSource.DataSource = this.dataSet_TR_DataEntry;
            // 
            // dataSet_TR_DataEntry
            // 
            this.dataSet_TR_DataEntry.DataSetName = "DataSet_TR_DataEntry";
            this.dataSet_TR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // endDateDateEdit
            // 
            this.endDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "EndDate", true));
            this.endDateDateEdit.EditValue = null;
            this.endDateDateEdit.Location = new System.Drawing.Point(425, 108);
            this.endDateDateEdit.Name = "endDateDateEdit";
            this.endDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.endDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.endDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.endDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.endDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.endDateDateEdit.Size = new System.Drawing.Size(117, 20);
            this.endDateDateEdit.StyleController = this.dataLayoutControl1;
            this.endDateDateEdit.TabIndex = 22;
            this.endDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.endDateDateEdit_Validating);
            // 
            // timeLimitSpinEdit
            // 
            this.timeLimitSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "TimeLimitValue", true));
            this.timeLimitSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.timeLimitSpinEdit.Location = new System.Drawing.Point(425, 156);
            this.timeLimitSpinEdit.Name = "timeLimitSpinEdit";
            this.timeLimitSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeLimitSpinEdit.Properties.Mask.EditMask = "N0";
            this.timeLimitSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.timeLimitSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.timeLimitSpinEdit.Size = new System.Drawing.Size(117, 20);
            this.timeLimitSpinEdit.StyleController = this.dataLayoutControl1;
            this.timeLimitSpinEdit.TabIndex = 20;
            this.timeLimitSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.timeLimitSpinEdit_Validating);
            // 
            // AllocationTypeGridLookUpEdit
            // 
            this.AllocationTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "AllocationTypeID", true));
            this.AllocationTypeGridLookUpEdit.Location = new System.Drawing.Point(154, 60);
            this.AllocationTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.AllocationTypeGridLookUpEdit.Name = "AllocationTypeGridLookUpEdit";
            this.AllocationTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AllocationTypeGridLookUpEdit.Properties.DataSource = this.spTR00006TrainingAllocationBindingSource;
            this.AllocationTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.AllocationTypeGridLookUpEdit.Properties.NullText = "";
            this.AllocationTypeGridLookUpEdit.Properties.ValueMember = "AllocationTypeID";
            this.AllocationTypeGridLookUpEdit.Properties.View = this.gridView1;
            this.AllocationTypeGridLookUpEdit.Size = new System.Drawing.Size(219, 20);
            this.AllocationTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.AllocationTypeGridLookUpEdit.TabIndex = 38;
            this.AllocationTypeGridLookUpEdit.EditValueChanged += new System.EventHandler(this.AllocationTypeGridLookUpEdit_EditValueChanged_1);
            this.AllocationTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.AllocationTypeGridLookUpEdit_Validating);
            // 
            // spTR00006TrainingAllocationBindingSource
            // 
            this.spTR00006TrainingAllocationBindingSource.DataMember = "sp_TR_00006_Training_Allocation";
            this.spTR00006TrainingAllocationBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAllocationTypeID,
            this.colDescription,
            this.colOrder});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colAllocationTypeID;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView1.FormatRules.Add(gridFormatRule1);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Allocation Type";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 0;
            this.colDescription.Width = 281;
            // 
            // colOrder
            // 
            this.colOrder.FieldName = "Order";
            this.colOrder.Name = "colOrder";
            this.colOrder.OptionsColumn.AllowEdit = false;
            this.colOrder.OptionsColumn.ReadOnly = true;
            // 
            // TimeLimitTypeGridLookUpEdit
            // 
            this.TimeLimitTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "TimeLimitTypeID", true));
            this.TimeLimitTypeGridLookUpEdit.Location = new System.Drawing.Point(154, 156);
            this.TimeLimitTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.TimeLimitTypeGridLookUpEdit.Name = "TimeLimitTypeGridLookUpEdit";
            this.TimeLimitTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TimeLimitTypeGridLookUpEdit.Properties.DataSource = this.spTR00008AllocationTimeLimitBindingSource;
            this.TimeLimitTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.TimeLimitTypeGridLookUpEdit.Properties.NullText = "";
            this.TimeLimitTypeGridLookUpEdit.Properties.ValueMember = "TimeLimitTypeID";
            this.TimeLimitTypeGridLookUpEdit.Properties.View = this.gridView2;
            this.TimeLimitTypeGridLookUpEdit.Size = new System.Drawing.Size(125, 20);
            this.TimeLimitTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.TimeLimitTypeGridLookUpEdit.TabIndex = 38;
            this.TimeLimitTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TimeLimitTypeGridLookUpEdit_Validating);
            // 
            // spTR00008AllocationTimeLimitBindingSource
            // 
            this.spTR00008AllocationTimeLimitBindingSource.DataMember = "sp_TR_00008_Allocation_Time_Limit";
            this.spTR00008AllocationTimeLimitBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTimeLimitTypeID,
            this.colDescription2,
            this.colOrder2});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colTimeLimitTypeID;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView2.FormatRules.Add(gridFormatRule2);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Time limit Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 221;
            // 
            // colOrder2
            // 
            this.colOrder2.FieldName = "Order";
            this.colOrder2.Name = "colOrder2";
            this.colOrder2.OptionsColumn.AllowEdit = false;
            this.colOrder2.OptionsColumn.ReadOnly = true;
            // 
            // teamMinimumSpinEdit
            // 
            this.teamMinimumSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "TeamHoldingMinimum", true));
            this.teamMinimumSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.teamMinimumSpinEdit.Location = new System.Drawing.Point(425, 180);
            this.teamMinimumSpinEdit.Name = "teamMinimumSpinEdit";
            this.teamMinimumSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teamMinimumSpinEdit.Properties.Mask.EditMask = "N0";
            this.teamMinimumSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.teamMinimumSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.teamMinimumSpinEdit.Size = new System.Drawing.Size(115, 20);
            this.teamMinimumSpinEdit.StyleController = this.dataLayoutControl1;
            this.teamMinimumSpinEdit.TabIndex = 20;
            this.teamMinimumSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.teamMinimumSpinEdit_Validating);
            // 
            // RequirementTypeGridLookUpEdit
            // 
            this.RequirementTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "RequirementTypeID", true));
            this.RequirementTypeGridLookUpEdit.Location = new System.Drawing.Point(154, 132);
            this.RequirementTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.RequirementTypeGridLookUpEdit.Name = "RequirementTypeGridLookUpEdit";
            this.RequirementTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RequirementTypeGridLookUpEdit.Properties.DataSource = this.spTR00007AllocationRequirementBindingSource;
            this.RequirementTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.RequirementTypeGridLookUpEdit.Properties.NullText = "";
            this.RequirementTypeGridLookUpEdit.Properties.ValueMember = "RequirementTypeID";
            this.RequirementTypeGridLookUpEdit.Properties.View = this.gridView4;
            this.RequirementTypeGridLookUpEdit.Size = new System.Drawing.Size(219, 20);
            this.RequirementTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RequirementTypeGridLookUpEdit.TabIndex = 38;
            this.RequirementTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RequirementTypeGridLookUpEdit_Validating);
            // 
            // spTR00007AllocationRequirementBindingSource
            // 
            this.spTR00007AllocationRequirementBindingSource.DataMember = "sp_TR_00007_Allocation_Requirement";
            this.spTR00007AllocationRequirementBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRequirementTypeID,
            this.colDescription1,
            this.colOrder1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.colRequirementTypeID;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            formatConditionRuleValue3.Value2 = ((short)(0));
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView4.FormatRules.Add(gridFormatRule3);
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Requirement Type";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 247;
            // 
            // colOrder1
            // 
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.EditValue = "";
            this.buttonEdit1.Location = new System.Drawing.Point(154, 84);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Select From List", null, null, true)});
            this.buttonEdit1.Properties.MaxLength = 100;
            this.buttonEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEdit1.Size = new System.Drawing.Size(490, 20);
            this.buttonEdit1.StyleController = this.dataLayoutControl1;
            this.buttonEdit1.TabIndex = 14;
            this.buttonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            this.buttonEdit1.EnabledChanged += new System.EventHandler(this.buttonEdit1_EnabledChanged);
            this.buttonEdit1.Validating += new System.ComponentModel.CancelEventHandler(this.buttonEdit1_Validating);
            // 
            // buttonEdit2
            // 
            this.buttonEdit2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spHR00232MasterQualificationTypesEditBindingSource, "Description", true));
            this.buttonEdit2.Location = new System.Drawing.Point(154, 12);
            this.buttonEdit2.Name = "buttonEdit2";
            this.buttonEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Click me to open the Select Qualification Type \\ Sub-Type screen", "choose", null, true)});
            this.buttonEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEdit2.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit2_ButtonClick);
            this.buttonEdit2.Size = new System.Drawing.Size(490, 20);
            this.buttonEdit2.StyleController = this.dataLayoutControl1;
            this.buttonEdit2.TabIndex = 31;
            this.buttonEdit2.Validating += new System.ComponentModel.CancelEventHandler(this.buttonEdit2_Validating);
            // 
            // spHR00232MasterQualificationTypesEditBindingSource
            // 
            this.spHR00232MasterQualificationTypesEditBindingSource.DataMember = "sp_HR_00232_Master_Qualification_Types_Edit";
            this.spHR00232MasterQualificationTypesEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spHR00234MasterQualificationSubTypeEditBindingSource, "Description", true));
            this.textEdit1.Location = new System.Drawing.Point(154, 36);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit1, true);
            this.textEdit1.Size = new System.Drawing.Size(490, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit1, optionsSpelling2);
            this.textEdit1.StyleController = this.dataLayoutControl1;
            this.textEdit1.TabIndex = 32;
            // 
            // spHR00234MasterQualificationSubTypeEditBindingSource
            // 
            this.spHR00234MasterQualificationSubTypeEditBindingSource.DataMember = "sp_HR_00234_Master_Qualification_SubType_Edit";
            this.spHR00234MasterQualificationSubTypeEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // teamHoldingTypeGridLookUpEdit
            // 
            this.teamHoldingTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "TeamHoldingTypeID", true));
            this.teamHoldingTypeGridLookUpEdit.Location = new System.Drawing.Point(154, 180);
            this.teamHoldingTypeGridLookUpEdit.Name = "teamHoldingTypeGridLookUpEdit";
            this.teamHoldingTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teamHoldingTypeGridLookUpEdit.Properties.DataSource = this.spTR00050TeamHoldingTypeBindingSource;
            this.teamHoldingTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.teamHoldingTypeGridLookUpEdit.Properties.NullText = "";
            this.teamHoldingTypeGridLookUpEdit.Properties.ValueMember = "TeamHoldingTypeID";
            this.teamHoldingTypeGridLookUpEdit.Properties.View = this.gridView3;
            this.teamHoldingTypeGridLookUpEdit.Size = new System.Drawing.Size(125, 20);
            this.teamHoldingTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.teamHoldingTypeGridLookUpEdit.TabIndex = 38;
            this.teamHoldingTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.teamHoldingTypeGridLookUpEdit_Validating);
            // 
            // spTR00050TeamHoldingTypeBindingSource
            // 
            this.spTR00050TeamHoldingTypeBindingSource.DataMember = "sp_TR_00050_Team_Holding_Type";
            this.spTR00050TeamHoldingTypeBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule4.ApplyToRow = true;
            gridFormatRule4.Column = this.gridColumn1;
            gridFormatRule4.Name = "Format0";
            formatConditionRuleValue4.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue4.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue4.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue4.Value1 = 0;
            gridFormatRule4.Rule = formatConditionRuleValue4;
            this.gridView3.FormatRules.Add(gridFormatRule4);
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Allocation Type";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 281;
            // 
            // gridColumn3
            // 
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "BreachReportableActive", true));
            this.checkEdit1.Location = new System.Drawing.Point(519, 60);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "(Tick if Yes)";
            this.checkEdit1.Properties.ValueChecked = 1;
            this.checkEdit1.Properties.ValueUnchecked = 0;
            this.checkEdit1.Size = new System.Drawing.Size(125, 19);
            this.checkEdit1.StyleController = this.dataLayoutControl1;
            this.checkEdit1.TabIndex = 29;
            // 
            // ItemForQualificationSubTypeID
            // 
            this.ItemForQualificationSubTypeID.Control = this.QualificationSubTypeIDTextEdit;
            this.ItemForQualificationSubTypeID.CustomizationFormText = "Qualification Sub-Type ID:";
            this.ItemForQualificationSubTypeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForQualificationSubTypeID.Name = "ItemForQualificationSubTypeID";
            this.ItemForQualificationSubTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForQualificationSubTypeID.Text = "Qualification Sub-Type ID:";
            this.ItemForQualificationSubTypeID.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(656, 491);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup5});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(636, 471);
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.ItemForQualificationType,
            this.ItemForQualificationSubType,
            this.ItemForAllocationType,
            this.filterLayoutControlItem,
            this.ItemForStartDate,
            this.ItemForEndDate,
            this.ItemForRequirementTypeID,
            this.emptySpaceItem3,
            this.ItemFortimeLimitdType,
            this.ItemForTimeLimitValue,
            this.ItemForTeamMinimum,
            this.emptySpaceItem2,
            this.ItemForTeamHoldingType,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.ItemForBreachReporting});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(636, 471);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.allocationGridControl;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 192);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(636, 279);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForQualificationType
            // 
            this.ItemForQualificationType.Control = this.buttonEdit2;
            this.ItemForQualificationType.CustomizationFormText = "Qualification Type:";
            this.ItemForQualificationType.Location = new System.Drawing.Point(0, 0);
            this.ItemForQualificationType.Name = "ItemForQualificationType";
            this.ItemForQualificationType.Size = new System.Drawing.Size(636, 24);
            this.ItemForQualificationType.Text = "Qualification Type:";
            this.ItemForQualificationType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForQualificationSubType
            // 
            this.ItemForQualificationSubType.Control = this.textEdit1;
            this.ItemForQualificationSubType.CustomizationFormText = "Qualification Sub-Type:";
            this.ItemForQualificationSubType.Location = new System.Drawing.Point(0, 24);
            this.ItemForQualificationSubType.Name = "ItemForQualificationSubType";
            this.ItemForQualificationSubType.Size = new System.Drawing.Size(636, 24);
            this.ItemForQualificationSubType.Text = "Qualification Sub-Type:";
            this.ItemForQualificationSubType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForAllocationType
            // 
            this.ItemForAllocationType.Control = this.AllocationTypeGridLookUpEdit;
            this.ItemForAllocationType.CustomizationFormText = "Allocation Type :";
            this.ItemForAllocationType.Location = new System.Drawing.Point(0, 48);
            this.ItemForAllocationType.MaxSize = new System.Drawing.Size(365, 24);
            this.ItemForAllocationType.MinSize = new System.Drawing.Size(365, 24);
            this.ItemForAllocationType.Name = "ItemForAllocationType";
            this.ItemForAllocationType.Size = new System.Drawing.Size(365, 24);
            this.ItemForAllocationType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForAllocationType.Text = "Allocation Type:";
            this.ItemForAllocationType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // filterLayoutControlItem
            // 
            this.filterLayoutControlItem.Control = this.buttonEdit1;
            this.filterLayoutControlItem.CustomizationFormText = "Linked To Record:";
            this.filterLayoutControlItem.Location = new System.Drawing.Point(0, 72);
            this.filterLayoutControlItem.MaxSize = new System.Drawing.Size(636, 24);
            this.filterLayoutControlItem.MinSize = new System.Drawing.Size(636, 24);
            this.filterLayoutControlItem.Name = "filterLayoutControlItem";
            this.filterLayoutControlItem.Size = new System.Drawing.Size(636, 24);
            this.filterLayoutControlItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.filterLayoutControlItem.Text = "Filter By  :";
            this.filterLayoutControlItem.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.startDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Allocation Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 96);
            this.ItemForStartDate.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForStartDate.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(271, 24);
            this.ItemForStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.endDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "Allocation End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(271, 96);
            this.ItemForEndDate.MaxSize = new System.Drawing.Size(263, 24);
            this.ItemForEndDate.MinSize = new System.Drawing.Size(263, 24);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(263, 24);
            this.ItemForEndDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForRequirementTypeID
            // 
            this.ItemForRequirementTypeID.Control = this.RequirementTypeGridLookUpEdit;
            this.ItemForRequirementTypeID.CustomizationFormText = "Requirement Type:";
            this.ItemForRequirementTypeID.Location = new System.Drawing.Point(0, 120);
            this.ItemForRequirementTypeID.MaxSize = new System.Drawing.Size(365, 24);
            this.ItemForRequirementTypeID.MinSize = new System.Drawing.Size(365, 24);
            this.ItemForRequirementTypeID.Name = "ItemForRequirementTypeID";
            this.ItemForRequirementTypeID.Size = new System.Drawing.Size(365, 24);
            this.ItemForRequirementTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRequirementTypeID.Text = "Requirement Type:";
            this.ItemForRequirementTypeID.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(365, 120);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(271, 24);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemFortimeLimitdType
            // 
            this.ItemFortimeLimitdType.Control = this.TimeLimitTypeGridLookUpEdit;
            this.ItemFortimeLimitdType.CustomizationFormText = "Time Limit Type:";
            this.ItemFortimeLimitdType.Location = new System.Drawing.Point(0, 144);
            this.ItemFortimeLimitdType.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemFortimeLimitdType.MinSize = new System.Drawing.Size(271, 24);
            this.ItemFortimeLimitdType.Name = "ItemFortimeLimitdType";
            this.ItemFortimeLimitdType.Size = new System.Drawing.Size(271, 24);
            this.ItemFortimeLimitdType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemFortimeLimitdType.Text = "Time Limit Type:";
            this.ItemFortimeLimitdType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForTimeLimitValue
            // 
            this.ItemForTimeLimitValue.Control = this.timeLimitSpinEdit;
            this.ItemForTimeLimitValue.CustomizationFormText = "Time Limit Value:";
            this.ItemForTimeLimitValue.Location = new System.Drawing.Point(271, 144);
            this.ItemForTimeLimitValue.MaxSize = new System.Drawing.Size(263, 24);
            this.ItemForTimeLimitValue.MinSize = new System.Drawing.Size(263, 24);
            this.ItemForTimeLimitValue.Name = "ItemForTimeLimitValue";
            this.ItemForTimeLimitValue.Size = new System.Drawing.Size(263, 24);
            this.ItemForTimeLimitValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTimeLimitValue.Text = "Time Limit Value:";
            this.ItemForTimeLimitValue.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForTeamMinimum
            // 
            this.ItemForTeamMinimum.Control = this.teamMinimumSpinEdit;
            this.ItemForTeamMinimum.CustomizationFormText = "Minimum Required:";
            this.ItemForTeamMinimum.Location = new System.Drawing.Point(271, 168);
            this.ItemForTeamMinimum.MaxSize = new System.Drawing.Size(261, 24);
            this.ItemForTeamMinimum.MinSize = new System.Drawing.Size(261, 24);
            this.ItemForTeamMinimum.Name = "ItemForTeamMinimum";
            this.ItemForTeamMinimum.Size = new System.Drawing.Size(261, 24);
            this.ItemForTeamMinimum.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTeamMinimum.Text = "Minimum Required:";
            this.ItemForTeamMinimum.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(534, 144);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(102, 24);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForTeamHoldingType
            // 
            this.ItemForTeamHoldingType.Control = this.teamHoldingTypeGridLookUpEdit;
            this.ItemForTeamHoldingType.CustomizationFormText = "Team Holding Type:";
            this.ItemForTeamHoldingType.Location = new System.Drawing.Point(0, 168);
            this.ItemForTeamHoldingType.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForTeamHoldingType.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForTeamHoldingType.Name = "ItemForTeamHoldingType";
            this.ItemForTeamHoldingType.Size = new System.Drawing.Size(271, 24);
            this.ItemForTeamHoldingType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTeamHoldingType.Text = "Team Holding Type:";
            this.ItemForTeamHoldingType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(532, 168);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(104, 24);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(104, 24);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(534, 96);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(102, 24);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForBreachReporting
            // 
            this.ItemForBreachReporting.Control = this.checkEdit1;
            this.ItemForBreachReporting.CustomizationFormText = "Include in Breach Reporting";
            this.ItemForBreachReporting.Location = new System.Drawing.Point(365, 48);
            this.ItemForBreachReporting.Name = "ItemForBreachReporting";
            this.ItemForBreachReporting.Size = new System.Drawing.Size(271, 24);
            this.ItemForBreachReporting.Text = "Include in Breach Reporting :";
            this.ItemForBreachReporting.TextSize = new System.Drawing.Size(139, 13);
            // 
            // spTR00020QualificationSubTypeAllocationSelectBindingSource
            // 
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource.DataMember = "sp_TR_00020_Qualification_Sub_Type_Allocation_Select";
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // spHR00236MasterQualificationTypesWithBlankBindingSource
            // 
            this.spHR00236MasterQualificationTypesWithBlankBindingSource.DataMember = "sp_HR_00236_Master_Qualification_Types_With_Blank";
            this.spHR00236MasterQualificationTypesWithBlankBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spTR00001QualificationSubTypeStatusBindingSource
            // 
            this.spTR00001QualificationSubTypeStatusBindingSource.DataMember = "sp_TR_00001_Qualification_SubType_Status";
            this.spTR00001QualificationSubTypeStatusBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter
            // 
            this.sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00001_Qualification_SubType_StatusTableAdapter
            // 
            this.sp_TR_00001_Qualification_SubType_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_TR_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_TR_00006_Training_AllocationTableAdapter
            // 
            this.sp_TR_00006_Training_AllocationTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00007_Allocation_RequirementTableAdapter
            // 
            this.sp_TR_00007_Allocation_RequirementTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00008_Allocation_Time_LimitTableAdapter
            // 
            this.sp_TR_00008_Allocation_Time_LimitTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00021_Qualification_Sub_Type_Allocation_AvailableTableAdapter
            // 
            this.sp_TR_00021_Qualification_Sub_Type_Allocation_AvailableTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter
            // 
            this.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00108_Qualification_Allocation_EditTableAdapter
            // 
            this.sp_TR_00108_Qualification_Allocation_EditTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            // 
            // sp_HR_00232_Master_Qualification_Types_EditTableAdapter
            // 
            this.sp_HR_00232_Master_Qualification_Types_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00234_Master_Qualification_SubType_EditTableAdapter
            // 
            this.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00050_Team_Holding_TypeTableAdapter
            // 
            this.sp_TR_00050_Team_Holding_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Master_Qualification_SubType_Add_Allocation
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(656, 545);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Master_Qualification_SubType_Add_Allocation";
            this.Text = "Master Qualification Sub-Type Add Allocation";
            this.Activated += new System.EventHandler(this.frm_HR_Master_Qualification_SubType_Add_Allocation_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Master_Qualification_SubType_Add_Allocation_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Master_Qualification_SubType_Add_Allocation_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.allocationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00021QualificationSubTypeAllocationAvailableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allocationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00108QualificationAllocationEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeLimitSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00006TrainingAllocationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeLimitTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00008AllocationTimeLimitBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamMinimumSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequirementTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00007AllocationRequirementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00232MasterQualificationTypesEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00234MasterQualificationSubTypeEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamHoldingTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00050TeamHoldingTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequirementTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFortimeLimitdType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeLimitValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMinimum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamHoldingType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBreachReporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00020QualificationSubTypeAllocationSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00236MasterQualificationTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00001QualificationSubTypeStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit QualificationSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationSubTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource spHR00236MasterQualificationTypesWithBlankBindingSource;
        private DataSet_HR_CoreTableAdapters.sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter sp_HR_00236_Master_Qualification_Types_With_BlankTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.DateEdit startDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraEditors.DateEdit endDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraEditors.SpinEdit timeLimitSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTimeLimitValue;
        private DevExpress.XtraEditors.GridLookUpEdit AllocationTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationType;
        private DevExpress.XtraEditors.GridLookUpEdit TimeLimitTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemFortimeLimitdType;
        private DevExpress.XtraEditors.SpinEdit teamMinimumSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamMinimum;
        private DataSet_TR_Core dataSet_TR_Core;
        private System.Windows.Forms.BindingSource spTR00001QualificationSubTypeStatusBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00001_Qualification_SubType_StatusTableAdapter sp_TR_00001_Qualification_SubType_StatusTableAdapter;
        private DataSet_TR_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraEditors.GridLookUpEdit RequirementTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequirementTypeID;
        private System.Windows.Forms.BindingSource spTR00006TrainingAllocationBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00006_Training_AllocationTableAdapter sp_TR_00006_Training_AllocationTableAdapter;
        private System.Windows.Forms.BindingSource spTR00007AllocationRequirementBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00007_Allocation_RequirementTableAdapter sp_TR_00007_Allocation_RequirementTableAdapter;
        private System.Windows.Forms.BindingSource spTR00008AllocationTimeLimitBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00008_Allocation_Time_LimitTableAdapter sp_TR_00008_Allocation_Time_LimitTableAdapter;
        private System.Windows.Forms.BindingSource spTR00021QualificationSubTypeAllocationAvailableBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00021_Qualification_Sub_Type_Allocation_AvailableTableAdapter sp_TR_00021_Qualification_Sub_Type_Allocation_AvailableTableAdapter;
        private System.Windows.Forms.BindingSource spTR00020QualificationSubTypeAllocationSelectBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter;
        private DataSet_TR_DataEntry dataSet_TR_DataEntry;
        private System.Windows.Forms.BindingSource spTR00108QualificationAllocationEditBindingSource;
        private DataSet_TR_DataEntryTableAdapters.sp_TR_00108_Qualification_Allocation_EditTableAdapter sp_TR_00108_Qualification_Allocation_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeLimitTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder2;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.GridControl allocationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView allocationGridView;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private DevExpress.XtraLayout.LayoutControlItem filterLayoutControlItem;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationType;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationSubType;
        private System.Windows.Forms.BindingSource spHR00232MasterQualificationTypesEditBindingSource;
        private System.Windows.Forms.BindingSource spHR00234MasterQualificationSubTypeEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00232_Master_Qualification_Types_EditTableAdapter sp_HR_00232_Master_Qualification_Types_EditTableAdapter;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter sp_HR_00234_Master_Qualification_SubType_EditTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.GridLookUpEdit teamHoldingTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamHoldingType;
        private System.Windows.Forms.BindingSource spTR00050TeamHoldingTypeBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00050_Team_Holding_TypeTableAdapter sp_TR_00050_Team_Holding_TypeTableAdapter;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBreachReporting;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class MaintainPersonQualificationRefreshes : DevExpress.XtraEditors.XtraUserControl
    {
        #region Instance Variables...
        private frmBase parent;
        
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private int parQualificationID = 0;

        private bool SetUpDone = false;

        bool iBool_AllowAdd = false;
        bool iBool_AllowDelete = false;
        bool iBool_AllowEdit = false;

        bool isRunning = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        public RefreshGridState RefreshGridViewQualificationRefreshes;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        //string i_str_AddedQualificationSubTypeAwardingBodyIDs = "";

        #endregion

        public MaintainPersonQualificationRefreshes()
        {
            InitializeComponent();
        }

        public void SetUp(
            string ConnectionString,
            int QualificationID,
            string FormMode)
        {
            parent = (frmBase)this.ParentForm;
            
            if (FormMode != "view")
            {
                iBool_AllowAdd = true;

                //Not relevant on this screen
                //iBool_AllowEdit = true;

                iBool_AllowDelete = true;
            }

            if (iBool_AllowDelete)
            {
                gridView.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
                gridView.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DefaultBoolean.False;
            }

            strConnectionString = ConnectionString;
            parQualificationID = QualificationID;
            
            sp_TR_00031_Person_Qualification_Refreshes_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewQualificationRefreshes = new RefreshGridState(gridView, "ID");

            Load_Refreshes();

            emptyEditor = new RepositoryItem();

            SetMenuStatus();

            SetUpDone = true;
        }

        public void Reload()
        {
            if (!SetUpDone)
            { return; }

            Load_Refreshes();
        }


        private void Load_Refreshes()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            RefreshGridViewQualificationRefreshes.SaveViewInfo();
            gridControlRefreshes.BeginUpdate();

            sp_TR_00031_Person_Qualification_Refreshes_SelectTableAdapter.Fill(dataSet_TR_Core.sp_TR_00031_Person_Qualification_Refreshes_Select,parQualificationID);
            this.RefreshGridViewQualificationRefreshes.LoadViewInfo(); // Reload any expanded groups and selected rows //

            gridControlRefreshes.EndUpdate();

            /**
            // Highlight any recently added new rows //
            if (i_str_AddedQualificationSubTypeAwardingBodyIDs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedQualificationSubTypeAwardingBodyIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControlRefreshes.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedQualificationSubTypeAwardingBodyIDs = "";
            }
            **/
        }

        public void SetMenuStatus()
        {

            GridView view = (GridView)gridControlRefreshes.MainView;

            int[] intRowHandles;
            
            // Set enabled status of navigator custom buttons //
            intRowHandles = view.GetSelectedRows();
            gridControlRefreshes.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlRefreshes.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControlRefreshes.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControlRefreshes.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

        }

        private void gridControlAwardingBody_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    GridView view = (GridView)gridControlRefreshes.MainView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        //Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        //Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        //View_Record();
                    }
                    break;
                default:
                    break;
            }
        }


        private void Add_Record()
        {
           if (!iBool_AllowAdd) return;
           
            GridView view = (GridView)gridControlRefreshes.MainView; ;
            MethodInfo method = null;

            view.PostEditor();
           
            /***
           RefreshGridViewStateAwardingBodye.SaveViewInfo();  // Store Grid View State //
           ***/

           int intMaxOrder = 0;
           for (int i = 0; i < view.DataRowCount; i++)
           {
               if (Convert.ToInt32(view.GetRowCellValue(i, "RecordOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "RecordOrder"));
           }

            var fChildForm = new frm_HR_Master_Qualification_SubType_Add_Refreshed_By_Qualification();

           fChildForm.MdiParent = parent.MdiParent;
           fChildForm.GlobalSettings = parent.GlobalSettings;
           fChildForm.strRecordIDs = parQualificationID.ToString() + ",";
           fChildForm.strFormMode = "add";
           fChildForm.strCaller = this.Name;
           fChildForm.intRecordCount = 0;
           fChildForm.FormPermissions = parent.FormPermissions;
           fChildForm._LastOrder = intMaxOrder;
           DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this.ParentForm, typeof(global::WoodPlan5.WaitForm1), true, true);
           fChildForm.splashScreenManager = splashScreenManager1;
           fChildForm.splashScreenManager.ShowWaitForm();
           fChildForm.Show();

           method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
           if (method != null) method.Invoke(fChildForm, new object[] { null });
                      
        }

        public bool ChangesPending()
        {
            GridView view = (GridView)gridControlRefreshes.MainView;
            return view.SelectedRowsCount > 0;
        }

        private void Delete_Record()
        {
            if (!iBool_AllowDelete) return;

            int[] intRowHandles;
            int intCount = 0;
            GridView view  = (GridView)gridControlRefreshes.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Awarding Bodies to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            string strMessage = "You have " + (intCount == 1 ? "1 Awarding Body" : Convert.ToString(intRowHandles.Length) + " Awarding Bodies") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Awarding Body" : "these Awarding Bodies") + " will no longer be available for selection and any related records will also be deleted!</color>";
            if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this.ParentForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                string strRecordIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ID")) + ",";
                }

                this.RefreshGridViewQualificationRefreshes.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                using (var RemoveRecords = new DataSet_TR_DataEntryTableAdapters.QueriesTableAdapter())
                {
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        RemoveRecords.sp_TR_00103_Qualification_Refreshes_Delete(strRecordIDs, 0); //Deletion Mode 0 marks records as Historic. Mode 1 Deletes
                    }
                    catch (Exception) { }
                }

                Load_Refreshes();
                
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (parent.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }

            SetMenuStatus();
        }

        private void gridView_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }
    }
}

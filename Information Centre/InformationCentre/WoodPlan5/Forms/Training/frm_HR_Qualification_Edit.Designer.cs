namespace WoodPlan5
{
    partial class frm_HR_Qualification_Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Qualification_Edit));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip5 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem5 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling2 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling3 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling4 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling5 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling6 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling7 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling8 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling9 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocuments = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLinkedDocumentAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.maintainPersonQualificationHistory = new WoodPlan5.MaintainPersonQualificationHistory();
            this.maintainPersonQualificationRefreshes = new WoodPlan5.MaintainPersonQualificationRefreshes();
            this.maintainPersonQualificationRefreshedBy = new WoodPlan5.MaintainPersonQualificationRefreshedBy();
            this.LinkedDocumentCountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.sp09105HRQualificationItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.QualificationSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.QualificationTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.QualificationTypeButtonEdit = new DevExpress.XtraEditors.ButtonEdit();
            this.ArchivedCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WoodPlanCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.UtilityRailCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.UtilityArbCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.WinterMaintenanceCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.SummerMaintenanceCheckEdit = new DevExpress.XtraEditors.CheckEdit();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.RemarksMemoEdit = new DevExpress.XtraEditors.MemoEdit();
            this.LinkedToPersonTypeIDLookupEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.sp09107HRQualificationLinkedRecordTypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.LinkedToPersonIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.QualificationIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.QualificationSubTypeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.validityTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00002QualificationSubTypeValidityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.refreshTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00004QualificationSubTypeRefreshBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.ItemForQualificationID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedToPersonID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQualificationTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQualificationSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForLinkedToPersonTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForArchived = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForLinkedDocumentCount = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSummerMaintenance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWinterMaintenance = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUtilityArb = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUtilityRail = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForWoodPlan = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem15 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem17 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem18 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem19 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRemarks = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQualificationType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForQualificationSubType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValidityTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRefreshTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpiryDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForRefreshDescription = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sp09109HRQualificationSourcesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spTR00009AssessmentTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp09108HRCertificateTypesWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.dataSet_AT_DataEntry = new WoodPlan5.DataSet_AT_DataEntry();
            this.sp09105_HR_Qualification_ItemTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp09105_HR_Qualification_ItemTableAdapter();
            this.sp09107_HR_Qualification_Linked_Record_TypesTableAdapter = new WoodPlan5.DataSet_HR_CoreTableAdapters.sp09107_HR_Qualification_Linked_Record_TypesTableAdapter();
            this.sp09108_HR_Certificate_Types_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp09108_HR_Certificate_Types_With_BlankTableAdapter();
            this.sp09109_HR_Qualification_Sources_With_BlankTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp09109_HR_Qualification_Sources_With_BlankTableAdapter();
            this.sp_TR_00002_Qualification_SubType_ValidityTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00002_Qualification_SubType_ValidityTableAdapter();
            this.sp_TR_00004_Qualification_SubType_RefreshTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00004_Qualification_SubType_RefreshTableAdapter();
            this.sp_TR_00009_Assessment_TypeTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00009_Assessment_TypeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentCountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09105HRQualificationItemBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationTypeButtonEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArchivedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityRailCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintenanceCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummerMaintenanceCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeIDLookupEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09107HRQualificationLinkedRecordTypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.validityTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00002QualificationSubTypeValidityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00004QualificationSubTypeRefreshBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArchived)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentCount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSummerMaintenance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintenance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityRail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValidityTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshDescription)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09109HRQualificationSourcesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00009AssessmentTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09108HRCertificateTypesWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(753, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 502);
            this.barDockControlBottom.Size = new System.Drawing.Size(753, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(753, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 476);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "ID";
            this.gridColumn4.FieldName = "ValidityTypeID";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.AllowEdit = false;
            this.gridColumn4.OptionsColumn.AllowFocus = false;
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn4.Width = 53;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "ID";
            this.gridColumn7.FieldName = "RefreshTypeID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn7.Width = 53;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation,
            this.bbiLinkedDocuments,
            this.bbiLinkedDocumentAdd});
            this.barManager2.MaxItemId = 17;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocuments, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLinkedDocumentAdd)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bbiLinkedDocuments
            // 
            this.bbiLinkedDocuments.Caption = "Linked Documents";
            this.bbiLinkedDocuments.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocuments.Glyph")));
            this.bbiLinkedDocuments.Id = 15;
            this.bbiLinkedDocuments.Name = "bbiLinkedDocuments";
            this.bbiLinkedDocuments.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "Linked Documents - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "Click me to open the Linked Documents Manager, filtered on the current data entry" +
    " record.";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.bbiLinkedDocuments.SuperTip = superToolTip3;
            this.bbiLinkedDocuments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocuments_ItemClick);
            // 
            // bbiLinkedDocumentAdd
            // 
            this.bbiLinkedDocumentAdd.Caption = "Add Linked Document";
            this.bbiLinkedDocumentAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLinkedDocumentAdd.Glyph")));
            this.bbiLinkedDocumentAdd.Id = 16;
            this.bbiLinkedDocumentAdd.Name = "bbiLinkedDocumentAdd";
            this.bbiLinkedDocumentAdd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            toolTipTitleItem4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem4.Appearance.Options.UseImage = true;
            toolTipTitleItem4.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem4.Image")));
            toolTipTitleItem4.Text = "Add Linked Document - Information";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = resources.GetString("toolTipItem4.Text");
            superToolTip4.Items.Add(toolTipTitleItem4);
            superToolTip4.Items.Add(toolTipItem4);
            this.bbiLinkedDocumentAdd.SuperTip = superToolTip4;
            this.bbiLinkedDocumentAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLinkedDocumentAdd_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip5.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "Form Mode - Information";
            toolTipItem5.LeftIndent = 6;
            toolTipItem5.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip5.Items.Add(toolTipTitleItem5);
            superToolTip5.Items.Add(toolTipItem5);
            this.barStaticItemFormMode.SuperTip = superToolTip5;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(753, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 502);
            this.barDockControl2.Size = new System.Drawing.Size(753, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 476);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(753, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 476);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "info_16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Delete_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.maintainPersonQualificationHistory);
            this.dataLayoutControl1.Controls.Add(this.maintainPersonQualificationRefreshes);
            this.dataLayoutControl1.Controls.Add(this.maintainPersonQualificationRefreshedBy);
            this.dataLayoutControl1.Controls.Add(this.LinkedDocumentCountTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QualificationSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QualificationTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QualificationTypeButtonEdit);
            this.dataLayoutControl1.Controls.Add(this.ArchivedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WoodPlanCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.UtilityRailCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.UtilityArbCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.WinterMaintenanceCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.SummerMaintenanceCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.buttonEdit1);
            this.dataLayoutControl1.Controls.Add(this.dataNavigator1);
            this.dataLayoutControl1.Controls.Add(this.RemarksMemoEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonTypeIDLookupEdit);
            this.dataLayoutControl1.Controls.Add(this.LinkedToPersonIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QualificationIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.QualificationSubTypeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.validityTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.refreshTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.textEdit1);
            this.dataLayoutControl1.Controls.Add(this.textEdit2);
            this.dataLayoutControl1.DataSource = this.sp09105HRQualificationItemBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForQualificationID,
            this.ItemForLinkedToPersonID,
            this.ItemForQualificationTypeID,
            this.ItemForQualificationSubTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(611, 243, 250, 350);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(753, 476);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // maintainPersonQualificationHistory
            // 
            this.maintainPersonQualificationHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maintainPersonQualificationHistory.Location = new System.Drawing.Point(24, 261);
            this.maintainPersonQualificationHistory.Name = "maintainPersonQualificationHistory";
            this.maintainPersonQualificationHistory.Size = new System.Drawing.Size(705, 191);
            this.maintainPersonQualificationHistory.TabIndex = 39;
            // 
            // maintainPersonQualificationRefreshes
            // 
            this.maintainPersonQualificationRefreshes.Location = new System.Drawing.Point(24, 261);
            this.maintainPersonQualificationRefreshes.Name = "maintainPersonQualificationRefreshes";
            this.maintainPersonQualificationRefreshes.Size = new System.Drawing.Size(705, 191);
            this.maintainPersonQualificationRefreshes.TabIndex = 4;
            // 
            // maintainPersonQualificationRefreshedBy
            // 
            this.maintainPersonQualificationRefreshedBy.Location = new System.Drawing.Point(24, 261);
            this.maintainPersonQualificationRefreshedBy.Name = "maintainPersonQualificationRefreshedBy";
            this.maintainPersonQualificationRefreshedBy.Size = new System.Drawing.Size(705, 191);
            this.maintainPersonQualificationRefreshedBy.TabIndex = 4;
            // 
            // LinkedDocumentCountTextEdit
            // 
            this.LinkedDocumentCountTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "LinkedDocumentCount", true));
            this.LinkedDocumentCountTextEdit.Location = new System.Drawing.Point(486, 83);
            this.LinkedDocumentCountTextEdit.MenuManager = this.barManager1;
            this.LinkedDocumentCountTextEdit.Name = "LinkedDocumentCountTextEdit";
            this.LinkedDocumentCountTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedDocumentCountTextEdit, true);
            this.LinkedDocumentCountTextEdit.Size = new System.Drawing.Size(255, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedDocumentCountTextEdit, optionsSpelling1);
            this.LinkedDocumentCountTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedDocumentCountTextEdit.TabIndex = 34;
            // 
            // sp09105HRQualificationItemBindingSource
            // 
            this.sp09105HRQualificationItemBindingSource.DataMember = "sp09105_HR_Qualification_Item";
            this.sp09105HRQualificationItemBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // QualificationSubTypeIDTextEdit
            // 
            this.QualificationSubTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "QualificationSubTypeID", true));
            this.QualificationSubTypeIDTextEdit.Location = new System.Drawing.Point(140, 84);
            this.QualificationSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.QualificationSubTypeIDTextEdit.Name = "QualificationSubTypeIDTextEdit";
            this.QualificationSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QualificationSubTypeIDTextEdit, true);
            this.QualificationSubTypeIDTextEdit.Size = new System.Drawing.Size(459, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QualificationSubTypeIDTextEdit, optionsSpelling2);
            this.QualificationSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.QualificationSubTypeIDTextEdit.TabIndex = 33;
            // 
            // QualificationTypeIDTextEdit
            // 
            this.QualificationTypeIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "QualificationTypeID", true));
            this.QualificationTypeIDTextEdit.Location = new System.Drawing.Point(121, 84);
            this.QualificationTypeIDTextEdit.MenuManager = this.barManager1;
            this.QualificationTypeIDTextEdit.Name = "QualificationTypeIDTextEdit";
            this.QualificationTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QualificationTypeIDTextEdit, true);
            this.QualificationTypeIDTextEdit.Size = new System.Drawing.Size(495, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QualificationTypeIDTextEdit, optionsSpelling3);
            this.QualificationTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.QualificationTypeIDTextEdit.TabIndex = 30;
            // 
            // QualificationTypeButtonEdit
            // 
            this.QualificationTypeButtonEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "QualificationType", true));
            this.QualificationTypeButtonEdit.Location = new System.Drawing.Point(132, 119);
            this.QualificationTypeButtonEdit.MenuManager = this.barManager1;
            this.QualificationTypeButtonEdit.Name = "QualificationTypeButtonEdit";
            this.QualificationTypeButtonEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Qualification Type \\ Sub-Type screen", "choose", null, true)});
            this.QualificationTypeButtonEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.QualificationTypeButtonEdit.Size = new System.Drawing.Size(609, 20);
            this.QualificationTypeButtonEdit.StyleController = this.dataLayoutControl1;
            this.QualificationTypeButtonEdit.TabIndex = 31;
            this.QualificationTypeButtonEdit.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.QualificationTypeButtonEdit_ButtonClick);
            this.QualificationTypeButtonEdit.Validating += new System.ComponentModel.CancelEventHandler(this.QualificationTypeButtonEdit_Validating);
            // 
            // ArchivedCheckEdit
            // 
            this.ArchivedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "Archived", true));
            this.ArchivedCheckEdit.Location = new System.Drawing.Point(132, 83);
            this.ArchivedCheckEdit.MenuManager = this.barManager1;
            this.ArchivedCheckEdit.Name = "ArchivedCheckEdit";
            this.ArchivedCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.ArchivedCheckEdit.Properties.ValueChecked = 1;
            this.ArchivedCheckEdit.Properties.ValueUnchecked = 0;
            this.ArchivedCheckEdit.Size = new System.Drawing.Size(230, 19);
            this.ArchivedCheckEdit.StyleController = this.dataLayoutControl1;
            this.ArchivedCheckEdit.TabIndex = 29;
            // 
            // WoodPlanCheckEdit
            // 
            this.WoodPlanCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "WoodPlan", true));
            this.WoodPlanCheckEdit.Location = new System.Drawing.Point(144, 353);
            this.WoodPlanCheckEdit.MenuManager = this.barManager1;
            this.WoodPlanCheckEdit.Name = "WoodPlanCheckEdit";
            this.WoodPlanCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WoodPlanCheckEdit.Properties.ValueChecked = 1;
            this.WoodPlanCheckEdit.Properties.ValueUnchecked = 0;
            this.WoodPlanCheckEdit.Size = new System.Drawing.Size(223, 19);
            this.WoodPlanCheckEdit.StyleController = this.dataLayoutControl1;
            this.WoodPlanCheckEdit.TabIndex = 28;
            // 
            // UtilityRailCheckEdit
            // 
            this.UtilityRailCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "UtilityRail", true));
            this.UtilityRailCheckEdit.Location = new System.Drawing.Point(144, 330);
            this.UtilityRailCheckEdit.MenuManager = this.barManager1;
            this.UtilityRailCheckEdit.Name = "UtilityRailCheckEdit";
            this.UtilityRailCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.UtilityRailCheckEdit.Properties.ValueChecked = 1;
            this.UtilityRailCheckEdit.Properties.ValueUnchecked = 0;
            this.UtilityRailCheckEdit.Size = new System.Drawing.Size(223, 19);
            this.UtilityRailCheckEdit.StyleController = this.dataLayoutControl1;
            this.UtilityRailCheckEdit.TabIndex = 27;
            // 
            // UtilityArbCheckEdit
            // 
            this.UtilityArbCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "UtilityArb", true));
            this.UtilityArbCheckEdit.Location = new System.Drawing.Point(144, 307);
            this.UtilityArbCheckEdit.MenuManager = this.barManager1;
            this.UtilityArbCheckEdit.Name = "UtilityArbCheckEdit";
            this.UtilityArbCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.UtilityArbCheckEdit.Properties.ValueChecked = 1;
            this.UtilityArbCheckEdit.Properties.ValueUnchecked = 0;
            this.UtilityArbCheckEdit.Size = new System.Drawing.Size(223, 19);
            this.UtilityArbCheckEdit.StyleController = this.dataLayoutControl1;
            this.UtilityArbCheckEdit.TabIndex = 26;
            // 
            // WinterMaintenanceCheckEdit
            // 
            this.WinterMaintenanceCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "WinterMaintenance", true));
            this.WinterMaintenanceCheckEdit.Location = new System.Drawing.Point(144, 284);
            this.WinterMaintenanceCheckEdit.MenuManager = this.barManager1;
            this.WinterMaintenanceCheckEdit.Name = "WinterMaintenanceCheckEdit";
            this.WinterMaintenanceCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.WinterMaintenanceCheckEdit.Properties.ValueChecked = 1;
            this.WinterMaintenanceCheckEdit.Properties.ValueUnchecked = 0;
            this.WinterMaintenanceCheckEdit.Size = new System.Drawing.Size(223, 19);
            this.WinterMaintenanceCheckEdit.StyleController = this.dataLayoutControl1;
            this.WinterMaintenanceCheckEdit.TabIndex = 25;
            // 
            // SummerMaintenanceCheckEdit
            // 
            this.SummerMaintenanceCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "SummerMaintenance", true));
            this.SummerMaintenanceCheckEdit.Location = new System.Drawing.Point(144, 261);
            this.SummerMaintenanceCheckEdit.MenuManager = this.barManager1;
            this.SummerMaintenanceCheckEdit.Name = "SummerMaintenanceCheckEdit";
            this.SummerMaintenanceCheckEdit.Properties.Caption = "(Tick if Yes)";
            this.SummerMaintenanceCheckEdit.Properties.ValueChecked = 1;
            this.SummerMaintenanceCheckEdit.Properties.ValueUnchecked = 0;
            this.SummerMaintenanceCheckEdit.Size = new System.Drawing.Size(223, 19);
            this.SummerMaintenanceCheckEdit.StyleController = this.dataLayoutControl1;
            this.SummerMaintenanceCheckEdit.TabIndex = 24;
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "LinkedToPersonName", true));
            this.buttonEdit1.Location = new System.Drawing.Point(132, 35);
            this.buttonEdit1.MenuManager = this.barManager1;
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Select", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, "Select From List", null, null, true)});
            this.buttonEdit1.Properties.MaxLength = 100;
            this.buttonEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEdit1.Size = new System.Drawing.Size(609, 20);
            this.buttonEdit1.StyleController = this.dataLayoutControl1;
            this.buttonEdit1.TabIndex = 14;
            this.buttonEdit1.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_ButtonClick);
            this.buttonEdit1.Validating += new System.ComponentModel.CancelEventHandler(this.buttonEdit1_Validating);
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Buttons.Append.Enabled = false;
            this.dataNavigator1.Buttons.Append.Visible = false;
            this.dataNavigator1.Buttons.CancelEdit.Enabled = false;
            this.dataNavigator1.Buttons.CancelEdit.Visible = false;
            this.dataNavigator1.Buttons.EndEdit.Enabled = false;
            this.dataNavigator1.Buttons.EndEdit.Visible = false;
            this.dataNavigator1.Buttons.NextPage.Enabled = false;
            this.dataNavigator1.Buttons.NextPage.Visible = false;
            this.dataNavigator1.Buttons.PrevPage.Enabled = false;
            this.dataNavigator1.Buttons.PrevPage.Visible = false;
            this.dataNavigator1.Buttons.Remove.Enabled = false;
            this.dataNavigator1.Buttons.Remove.Visible = false;
            this.dataNavigator1.DataSource = this.sp09105HRQualificationItemBindingSource;
            this.dataNavigator1.Location = new System.Drawing.Point(137, 12);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(196, 19);
            this.dataNavigator1.StyleController = this.dataLayoutControl1;
            this.dataNavigator1.TabIndex = 13;
            this.dataNavigator1.Text = "dataNavigator1";
            this.dataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            this.dataNavigator1.PositionChanged += new System.EventHandler(this.dataNavigator1_PositionChanged);
            this.dataNavigator1.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.dataNavigator1_ButtonClick);
            // 
            // RemarksMemoEdit
            // 
            this.RemarksMemoEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "Remarks", true));
            this.RemarksMemoEdit.Location = new System.Drawing.Point(24, 261);
            this.RemarksMemoEdit.MenuManager = this.barManager1;
            this.RemarksMemoEdit.Name = "RemarksMemoEdit";
            this.scSpellChecker.SetShowSpellCheckMenu(this.RemarksMemoEdit, true);
            this.RemarksMemoEdit.Size = new System.Drawing.Size(705, 191);
            this.scSpellChecker.SetSpellCheckerOptions(this.RemarksMemoEdit, optionsSpelling4);
            this.RemarksMemoEdit.StyleController = this.dataLayoutControl1;
            this.RemarksMemoEdit.TabIndex = 12;
            // 
            // LinkedToPersonTypeIDLookupEdit
            // 
            this.LinkedToPersonTypeIDLookupEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "LinkedToPersonTypeID", true));
            this.LinkedToPersonTypeIDLookupEdit.Location = new System.Drawing.Point(132, 59);
            this.LinkedToPersonTypeIDLookupEdit.MenuManager = this.barManager1;
            this.LinkedToPersonTypeIDLookupEdit.Name = "LinkedToPersonTypeIDLookupEdit";
            this.LinkedToPersonTypeIDLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LinkedToPersonTypeIDLookupEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intID", "ID", 49, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("strDescription", "Description", 150, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("intOrder", "Order", 53, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.LinkedToPersonTypeIDLookupEdit.Properties.DataSource = this.sp09107HRQualificationLinkedRecordTypesBindingSource;
            this.LinkedToPersonTypeIDLookupEdit.Properties.DisplayMember = "strDescription";
            this.LinkedToPersonTypeIDLookupEdit.Properties.NullText = "";
            this.LinkedToPersonTypeIDLookupEdit.Properties.ReadOnly = true;
            this.LinkedToPersonTypeIDLookupEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.LinkedToPersonTypeIDLookupEdit.Properties.ValueMember = "intID";
            this.LinkedToPersonTypeIDLookupEdit.Size = new System.Drawing.Size(609, 20);
            this.LinkedToPersonTypeIDLookupEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonTypeIDLookupEdit.TabIndex = 6;
            this.LinkedToPersonTypeIDLookupEdit.TabStop = false;
            // 
            // sp09107HRQualificationLinkedRecordTypesBindingSource
            // 
            this.sp09107HRQualificationLinkedRecordTypesBindingSource.DataMember = "sp09107_HR_Qualification_Linked_Record_Types";
            this.sp09107HRQualificationLinkedRecordTypesBindingSource.DataSource = this.dataSet_HR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // LinkedToPersonIDTextEdit
            // 
            this.LinkedToPersonIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "LinkedToPersonID", true));
            this.LinkedToPersonIDTextEdit.EditValue = "";
            this.LinkedToPersonIDTextEdit.Location = new System.Drawing.Point(122, 134);
            this.LinkedToPersonIDTextEdit.MenuManager = this.barManager1;
            this.LinkedToPersonIDTextEdit.Name = "LinkedToPersonIDTextEdit";
            this.LinkedToPersonIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.LinkedToPersonIDTextEdit, true);
            this.LinkedToPersonIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.LinkedToPersonIDTextEdit, optionsSpelling5);
            this.LinkedToPersonIDTextEdit.StyleController = this.dataLayoutControl1;
            this.LinkedToPersonIDTextEdit.TabIndex = 5;
            this.LinkedToPersonIDTextEdit.TabStop = false;
            // 
            // QualificationIDTextEdit
            // 
            this.QualificationIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "QualificationID", true));
            this.QualificationIDTextEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.QualificationIDTextEdit.Location = new System.Drawing.Point(122, 134);
            this.QualificationIDTextEdit.MenuManager = this.barManager1;
            this.QualificationIDTextEdit.Name = "QualificationIDTextEdit";
            this.QualificationIDTextEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Buffered;
            this.QualificationIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.QualificationIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QualificationIDTextEdit, true);
            this.QualificationIDTextEdit.Size = new System.Drawing.Size(494, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QualificationIDTextEdit, optionsSpelling6);
            this.QualificationIDTextEdit.StyleController = this.dataLayoutControl1;
            this.QualificationIDTextEdit.TabIndex = 4;
            // 
            // QualificationSubTypeTextEdit
            // 
            this.QualificationSubTypeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "QualificationSubType", true));
            this.QualificationSubTypeTextEdit.Location = new System.Drawing.Point(132, 143);
            this.QualificationSubTypeTextEdit.MenuManager = this.barManager1;
            this.QualificationSubTypeTextEdit.Name = "QualificationSubTypeTextEdit";
            this.QualificationSubTypeTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QualificationSubTypeTextEdit, true);
            this.QualificationSubTypeTextEdit.Size = new System.Drawing.Size(609, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QualificationSubTypeTextEdit, optionsSpelling7);
            this.QualificationSubTypeTextEdit.StyleController = this.dataLayoutControl1;
            this.QualificationSubTypeTextEdit.TabIndex = 32;
            // 
            // validityTypeGridLookUpEdit
            // 
            this.validityTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "ValidityTypeID", true));
            this.validityTypeGridLookUpEdit.Location = new System.Drawing.Point(132, 167);
            this.validityTypeGridLookUpEdit.Name = "validityTypeGridLookUpEdit";
            this.validityTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.validityTypeGridLookUpEdit.Properties.DataSource = this.spTR00002QualificationSubTypeValidityBindingSource;
            this.validityTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.validityTypeGridLookUpEdit.Properties.NullText = "";
            this.validityTypeGridLookUpEdit.Properties.ReadOnly = true;
            this.validityTypeGridLookUpEdit.Properties.ValueMember = "ValidityTypeID";
            this.validityTypeGridLookUpEdit.Properties.View = this.gridView2;
            this.validityTypeGridLookUpEdit.Size = new System.Drawing.Size(163, 20);
            this.validityTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.validityTypeGridLookUpEdit.TabIndex = 38;
            this.validityTypeGridLookUpEdit.EditValueChanged += new System.EventHandler(this.validityTypeGridLookUpEdit_EditValueChanged);
            // 
            // spTR00002QualificationSubTypeValidityBindingSource
            // 
            this.spTR00002QualificationSubTypeValidityBindingSource.DataMember = "sp_TR_00002_Qualification_SubType_Validity";
            this.spTR00002QualificationSubTypeValidityBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn4;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            formatConditionRuleValue1.Value2 = ((short)(0));
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn6, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Validity Type";
            this.gridColumn5.FieldName = "Description";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.AllowEdit = false;
            this.gridColumn5.OptionsColumn.AllowFocus = false;
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 0;
            this.gridColumn5.Width = 220;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Order";
            this.gridColumn6.FieldName = "Order";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.AllowEdit = false;
            this.gridColumn6.OptionsColumn.AllowFocus = false;
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // refreshTypeGridLookUpEdit
            // 
            this.refreshTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "RefreshTypeID", true));
            this.refreshTypeGridLookUpEdit.Location = new System.Drawing.Point(132, 191);
            this.refreshTypeGridLookUpEdit.Name = "refreshTypeGridLookUpEdit";
            this.refreshTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.refreshTypeGridLookUpEdit.Properties.DataSource = this.spTR00004QualificationSubTypeRefreshBindingSource;
            this.refreshTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.refreshTypeGridLookUpEdit.Properties.NullText = "";
            this.refreshTypeGridLookUpEdit.Properties.ReadOnly = true;
            this.refreshTypeGridLookUpEdit.Properties.ValueMember = "RefreshTypeID";
            this.refreshTypeGridLookUpEdit.Properties.View = this.gridView3;
            this.refreshTypeGridLookUpEdit.Size = new System.Drawing.Size(163, 20);
            this.refreshTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.refreshTypeGridLookUpEdit.TabIndex = 38;
            this.refreshTypeGridLookUpEdit.EditValueChanged += new System.EventHandler(this.refreshTypeGridLookUpEdit_EditValueChanged);
            // 
            // spTR00004QualificationSubTypeRefreshBindingSource
            // 
            this.spTR00004QualificationSubTypeRefreshBindingSource.DataMember = "sp_TR_00004_Qualification_SubType_Refresh";
            this.spTR00004QualificationSubTypeRefreshBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn9});
            this.gridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.gridColumn7;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            formatConditionRuleValue2.Value2 = ((short)(0));
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView3.FormatRules.Add(gridFormatRule2);
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView3.OptionsView.ShowIndicator = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn9, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn8, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Validity Type";
            this.gridColumn8.FieldName = "Description";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.AllowEdit = false;
            this.gridColumn8.OptionsColumn.AllowFocus = false;
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            this.gridColumn8.Width = 220;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Order";
            this.gridColumn9.FieldName = "Order";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.AllowFocus = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.Default;
            // 
            // textEdit1
            // 
            this.textEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "ExpiryPeriodDescription", true));
            this.textEdit1.Location = new System.Drawing.Point(299, 167);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.MaxLength = 50;
            this.textEdit1.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit1, true);
            this.textEdit1.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit1, optionsSpelling8);
            this.textEdit1.StyleController = this.dataLayoutControl1;
            this.textEdit1.TabIndex = 19;
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp09105HRQualificationItemBindingSource, "RefreshIntervalDescription", true));
            this.textEdit2.Location = new System.Drawing.Point(299, 191);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.MaxLength = 50;
            this.textEdit2.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.textEdit2, true);
            this.textEdit2.Size = new System.Drawing.Size(442, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.textEdit2, optionsSpelling9);
            this.textEdit2.StyleController = this.dataLayoutControl1;
            this.textEdit2.TabIndex = 19;
            // 
            // ItemForQualificationID
            // 
            this.ItemForQualificationID.Control = this.QualificationIDTextEdit;
            this.ItemForQualificationID.CustomizationFormText = "Qualification ID:";
            this.ItemForQualificationID.Location = new System.Drawing.Point(0, 122);
            this.ItemForQualificationID.Name = "ItemForQualificationID";
            this.ItemForQualificationID.Size = new System.Drawing.Size(608, 24);
            this.ItemForQualificationID.Text = "Qualification ID:";
            this.ItemForQualificationID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // ItemForLinkedToPersonID
            // 
            this.ItemForLinkedToPersonID.Control = this.LinkedToPersonIDTextEdit;
            this.ItemForLinkedToPersonID.CustomizationFormText = "Linked To Record ID:";
            this.ItemForLinkedToPersonID.Location = new System.Drawing.Point(0, 122);
            this.ItemForLinkedToPersonID.Name = "ItemForLinkedToPersonID";
            this.ItemForLinkedToPersonID.Size = new System.Drawing.Size(608, 24);
            this.ItemForLinkedToPersonID.Text = "Linked To Record ID:";
            this.ItemForLinkedToPersonID.TextSize = new System.Drawing.Size(98, 13);
            // 
            // ItemForQualificationTypeID
            // 
            this.ItemForQualificationTypeID.Control = this.QualificationTypeIDTextEdit;
            this.ItemForQualificationTypeID.CustomizationFormText = "Qualification Type ID:";
            this.ItemForQualificationTypeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForQualificationTypeID.Name = "ItemForQualificationTypeID";
            this.ItemForQualificationTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForQualificationTypeID.Text = "Qualification Type ID:";
            this.ItemForQualificationTypeID.TextSize = new System.Drawing.Size(106, 13);
            // 
            // ItemForQualificationSubTypeID
            // 
            this.ItemForQualificationSubTypeID.Control = this.QualificationSubTypeIDTextEdit;
            this.ItemForQualificationSubTypeID.CustomizationFormText = "Qualification Sub Type ID:";
            this.ItemForQualificationSubTypeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForQualificationSubTypeID.Name = "ItemForQualificationSubTypeID";
            this.ItemForQualificationSubTypeID.Size = new System.Drawing.Size(591, 24);
            this.ItemForQualificationSubTypeID.Text = "Qualification Sub Type ID:";
            this.ItemForQualificationSubTypeID.TextSize = new System.Drawing.Size(50, 20);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(753, 476);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForLinkedToPersonTypeID,
            this.emptySpaceItem2,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.ItemForArchived,
            this.ItemForLinkedDocumentCount,
            this.tabbedControlGroup1,
            this.ItemForQualificationType,
            this.ItemForQualificationSubType,
            this.ItemForValidityTypeID,
            this.ItemForRefreshTypeID,
            this.ItemForExpiryDescription,
            this.ItemForRefreshDescription,
            this.emptySpaceItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(733, 456);
            // 
            // ItemForLinkedToPersonTypeID
            // 
            this.ItemForLinkedToPersonTypeID.Control = this.LinkedToPersonTypeIDLookupEdit;
            this.ItemForLinkedToPersonTypeID.CustomizationFormText = "Linked Record Type:";
            this.ItemForLinkedToPersonTypeID.Location = new System.Drawing.Point(0, 47);
            this.ItemForLinkedToPersonTypeID.Name = "ItemForLinkedToPersonTypeID";
            this.ItemForLinkedToPersonTypeID.Size = new System.Drawing.Size(733, 24);
            this.ItemForLinkedToPersonTypeID.Text = "Linked Record Type:";
            this.ItemForLinkedToPersonTypeID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 95);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(733, 12);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem4.MaxSize = new System.Drawing.Size(125, 0);
            this.emptySpaceItem4.MinSize = new System.Drawing.Size(125, 10);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(125, 23);
            this.emptySpaceItem4.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(325, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(408, 23);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dataNavigator1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(125, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(200, 23);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AllowHide = false;
            this.layoutControlItem2.Control = this.buttonEdit1;
            this.layoutControlItem2.CustomizationFormText = "Linked To Record:";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(733, 24);
            this.layoutControlItem2.Text = "Linked To Record:";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForArchived
            // 
            this.ItemForArchived.Control = this.ArchivedCheckEdit;
            this.ItemForArchived.CustomizationFormText = "Archived:";
            this.ItemForArchived.Location = new System.Drawing.Point(0, 71);
            this.ItemForArchived.Name = "ItemForArchived";
            this.ItemForArchived.Size = new System.Drawing.Size(354, 24);
            this.ItemForArchived.Text = "Archived:";
            this.ItemForArchived.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForLinkedDocumentCount
            // 
            this.ItemForLinkedDocumentCount.Control = this.LinkedDocumentCountTextEdit;
            this.ItemForLinkedDocumentCount.CustomizationFormText = "Linked Document Count:";
            this.ItemForLinkedDocumentCount.Location = new System.Drawing.Point(354, 71);
            this.ItemForLinkedDocumentCount.Name = "ItemForLinkedDocumentCount";
            this.ItemForLinkedDocumentCount.Size = new System.Drawing.Size(379, 24);
            this.ItemForLinkedDocumentCount.Text = "Linked Document Count:";
            this.ItemForLinkedDocumentCount.TextSize = new System.Drawing.Size(117, 13);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 213);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.SelectedTabPageIndex = 2;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(733, 243);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup6,
            this.layoutControlGroup3,
            this.layoutControlGroup4,
            this.layoutControlGroup7,
            this.layoutControlGroup8});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.ExpandButtonVisible = true;
            this.layoutControlGroup4.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(709, 195);
            this.layoutControlGroup4.Text = "Qualification History ";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.maintainPersonQualificationHistory;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(709, 195);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Applies To";
            this.layoutControlGroup6.ExpandButtonVisible = true;
            this.layoutControlGroup6.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSummerMaintenance,
            this.ItemForWinterMaintenance,
            this.ItemForUtilityArb,
            this.ItemForUtilityRail,
            this.ItemForWoodPlan,
            this.emptySpaceItem8,
            this.emptySpaceItem15,
            this.emptySpaceItem16,
            this.emptySpaceItem17,
            this.emptySpaceItem18,
            this.emptySpaceItem19});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(709, 195);
            this.layoutControlGroup6.Text = "Applies To";
            // 
            // ItemForSummerMaintenance
            // 
            this.ItemForSummerMaintenance.Control = this.SummerMaintenanceCheckEdit;
            this.ItemForSummerMaintenance.CustomizationFormText = "Summer Maintenance:";
            this.ItemForSummerMaintenance.Location = new System.Drawing.Point(0, 0);
            this.ItemForSummerMaintenance.Name = "ItemForSummerMaintenance";
            this.ItemForSummerMaintenance.Size = new System.Drawing.Size(347, 23);
            this.ItemForSummerMaintenance.Text = "Summer Maintenance:";
            this.ItemForSummerMaintenance.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForWinterMaintenance
            // 
            this.ItemForWinterMaintenance.Control = this.WinterMaintenanceCheckEdit;
            this.ItemForWinterMaintenance.CustomizationFormText = "Winter Maintenance:";
            this.ItemForWinterMaintenance.Location = new System.Drawing.Point(0, 23);
            this.ItemForWinterMaintenance.Name = "ItemForWinterMaintenance";
            this.ItemForWinterMaintenance.Size = new System.Drawing.Size(347, 23);
            this.ItemForWinterMaintenance.Text = "Winter Maintenance:";
            this.ItemForWinterMaintenance.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForUtilityArb
            // 
            this.ItemForUtilityArb.Control = this.UtilityArbCheckEdit;
            this.ItemForUtilityArb.CustomizationFormText = "Utility ARB:";
            this.ItemForUtilityArb.Location = new System.Drawing.Point(0, 46);
            this.ItemForUtilityArb.Name = "ItemForUtilityArb";
            this.ItemForUtilityArb.Size = new System.Drawing.Size(347, 23);
            this.ItemForUtilityArb.Text = "Utility ARB:";
            this.ItemForUtilityArb.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForUtilityRail
            // 
            this.ItemForUtilityRail.Control = this.UtilityRailCheckEdit;
            this.ItemForUtilityRail.CustomizationFormText = "Utility Rail:";
            this.ItemForUtilityRail.Location = new System.Drawing.Point(0, 69);
            this.ItemForUtilityRail.Name = "ItemForUtilityRail";
            this.ItemForUtilityRail.Size = new System.Drawing.Size(347, 23);
            this.ItemForUtilityRail.Text = "Utility Rail:";
            this.ItemForUtilityRail.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForWoodPlan
            // 
            this.ItemForWoodPlan.Control = this.WoodPlanCheckEdit;
            this.ItemForWoodPlan.CustomizationFormText = "WoodPlan:";
            this.ItemForWoodPlan.Location = new System.Drawing.Point(0, 92);
            this.ItemForWoodPlan.Name = "ItemForWoodPlan";
            this.ItemForWoodPlan.Size = new System.Drawing.Size(347, 23);
            this.ItemForWoodPlan.Text = "WoodPlan:";
            this.ItemForWoodPlan.TextSize = new System.Drawing.Size(117, 13);
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.CustomizationFormText = "emptySpaceItem8";
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 115);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(709, 80);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem15
            // 
            this.emptySpaceItem15.AllowHotTrack = false;
            this.emptySpaceItem15.Location = new System.Drawing.Point(347, 0);
            this.emptySpaceItem15.Name = "emptySpaceItem15";
            this.emptySpaceItem15.Size = new System.Drawing.Size(362, 23);
            this.emptySpaceItem15.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.AllowHotTrack = false;
            this.emptySpaceItem16.Location = new System.Drawing.Point(347, 23);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(362, 23);
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem17
            // 
            this.emptySpaceItem17.AllowHotTrack = false;
            this.emptySpaceItem17.Location = new System.Drawing.Point(347, 46);
            this.emptySpaceItem17.Name = "emptySpaceItem17";
            this.emptySpaceItem17.Size = new System.Drawing.Size(362, 23);
            this.emptySpaceItem17.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem18
            // 
            this.emptySpaceItem18.AllowHotTrack = false;
            this.emptySpaceItem18.Location = new System.Drawing.Point(347, 69);
            this.emptySpaceItem18.Name = "emptySpaceItem18";
            this.emptySpaceItem18.Size = new System.Drawing.Size(362, 23);
            this.emptySpaceItem18.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem19
            // 
            this.emptySpaceItem19.AllowHotTrack = false;
            this.emptySpaceItem19.Location = new System.Drawing.Point(347, 92);
            this.emptySpaceItem19.Name = "emptySpaceItem19";
            this.emptySpaceItem19.Size = new System.Drawing.Size(362, 23);
            this.emptySpaceItem19.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CaptionImage = global::WoodPlan5.Properties.Resources.Notes_16x16;
            this.layoutControlGroup3.CustomizationFormText = "Remarks";
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRemarks});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(709, 195);
            this.layoutControlGroup3.Text = "Remarks";
            // 
            // ItemForRemarks
            // 
            this.ItemForRemarks.Control = this.RemarksMemoEdit;
            this.ItemForRemarks.CustomizationFormText = "Remarks:";
            this.ItemForRemarks.Location = new System.Drawing.Point(0, 0);
            this.ItemForRemarks.Name = "ItemForRemarks";
            this.ItemForRemarks.Size = new System.Drawing.Size(709, 195);
            this.ItemForRemarks.Text = "Remarks:";
            this.ItemForRemarks.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRemarks.TextVisible = false;
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.ExpandButtonVisible = true;
            this.layoutControlGroup7.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(709, 195);
            this.layoutControlGroup7.Text = "Auto-Refreshed By";
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.maintainPersonQualificationRefreshedBy;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(709, 195);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.ExpandButtonVisible = true;
            this.layoutControlGroup8.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(709, 195);
            this.layoutControlGroup8.Text = "Auto-Refreshes ";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.maintainPersonQualificationRefreshes;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(709, 195);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // ItemForQualificationType
            // 
            this.ItemForQualificationType.Control = this.QualificationTypeButtonEdit;
            this.ItemForQualificationType.CustomizationFormText = "Qualification Type:";
            this.ItemForQualificationType.Location = new System.Drawing.Point(0, 107);
            this.ItemForQualificationType.Name = "ItemForQualificationType";
            this.ItemForQualificationType.Size = new System.Drawing.Size(733, 24);
            this.ItemForQualificationType.Text = "Qualification Type:";
            this.ItemForQualificationType.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForQualificationSubType
            // 
            this.ItemForQualificationSubType.Control = this.QualificationSubTypeTextEdit;
            this.ItemForQualificationSubType.CustomizationFormText = "Qualification Sub-Type:";
            this.ItemForQualificationSubType.Location = new System.Drawing.Point(0, 131);
            this.ItemForQualificationSubType.Name = "ItemForQualificationSubType";
            this.ItemForQualificationSubType.Size = new System.Drawing.Size(733, 24);
            this.ItemForQualificationSubType.Text = "Qualification Sub-Type:";
            this.ItemForQualificationSubType.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForValidityTypeID
            // 
            this.ItemForValidityTypeID.Control = this.validityTypeGridLookUpEdit;
            this.ItemForValidityTypeID.CustomizationFormText = "Validity Type:";
            this.ItemForValidityTypeID.Location = new System.Drawing.Point(0, 155);
            this.ItemForValidityTypeID.MaxSize = new System.Drawing.Size(287, 24);
            this.ItemForValidityTypeID.MinSize = new System.Drawing.Size(287, 24);
            this.ItemForValidityTypeID.Name = "ItemForValidityTypeID";
            this.ItemForValidityTypeID.Size = new System.Drawing.Size(287, 24);
            this.ItemForValidityTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForValidityTypeID.Text = "Validity Type:";
            this.ItemForValidityTypeID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForRefreshTypeID
            // 
            this.ItemForRefreshTypeID.Control = this.refreshTypeGridLookUpEdit;
            this.ItemForRefreshTypeID.CustomizationFormText = "Refresh Type:";
            this.ItemForRefreshTypeID.Location = new System.Drawing.Point(0, 179);
            this.ItemForRefreshTypeID.MaxSize = new System.Drawing.Size(287, 24);
            this.ItemForRefreshTypeID.MinSize = new System.Drawing.Size(287, 24);
            this.ItemForRefreshTypeID.Name = "ItemForRefreshTypeID";
            this.ItemForRefreshTypeID.Size = new System.Drawing.Size(287, 24);
            this.ItemForRefreshTypeID.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForRefreshTypeID.Text = "Refresh Type:";
            this.ItemForRefreshTypeID.TextSize = new System.Drawing.Size(117, 13);
            // 
            // ItemForExpiryDescription
            // 
            this.ItemForExpiryDescription.Control = this.textEdit1;
            this.ItemForExpiryDescription.CustomizationFormText = "Course Name:";
            this.ItemForExpiryDescription.Location = new System.Drawing.Point(287, 155);
            this.ItemForExpiryDescription.Name = "ItemForExpiryDescription";
            this.ItemForExpiryDescription.Size = new System.Drawing.Size(446, 24);
            this.ItemForExpiryDescription.Text = "Expiry Description:";
            this.ItemForExpiryDescription.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForExpiryDescription.TextVisible = false;
            // 
            // ItemForRefreshDescription
            // 
            this.ItemForRefreshDescription.Control = this.textEdit2;
            this.ItemForRefreshDescription.CustomizationFormText = "Course Name:";
            this.ItemForRefreshDescription.Location = new System.Drawing.Point(287, 179);
            this.ItemForRefreshDescription.Name = "ItemForRefreshDescription";
            this.ItemForRefreshDescription.Size = new System.Drawing.Size(446, 24);
            this.ItemForRefreshDescription.Text = "Refresh Description:";
            this.ItemForRefreshDescription.TextSize = new System.Drawing.Size(0, 0);
            this.ItemForRefreshDescription.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 203);
            this.emptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(733, 10);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // sp09109HRQualificationSourcesWithBlankBindingSource
            // 
            this.sp09109HRQualificationSourcesWithBlankBindingSource.DataMember = "sp09109_HR_Qualification_Sources_With_Blank";
            this.sp09109HRQualificationSourcesWithBlankBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // spTR00009AssessmentTypeBindingSource
            // 
            this.spTR00009AssessmentTypeBindingSource.DataMember = "sp_TR_00009_Assessment_Type";
            this.spTR00009AssessmentTypeBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // sp09108HRCertificateTypesWithBlankBindingSource
            // 
            this.sp09108HRCertificateTypesWithBlankBindingSource.DataMember = "sp09108_HR_Certificate_Types_With_Blank";
            this.sp09108HRCertificateTypesWithBlankBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataSet_AT_DataEntry
            // 
            this.dataSet_AT_DataEntry.DataSetName = "DataSet_AT_DataEntry";
            this.dataSet_AT_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp09105_HR_Qualification_ItemTableAdapter
            // 
            this.sp09105_HR_Qualification_ItemTableAdapter.ClearBeforeFill = true;
            // 
            // sp09107_HR_Qualification_Linked_Record_TypesTableAdapter
            // 
            this.sp09107_HR_Qualification_Linked_Record_TypesTableAdapter.ClearBeforeFill = true;
            // 
            // sp09108_HR_Certificate_Types_With_BlankTableAdapter
            // 
            this.sp09108_HR_Certificate_Types_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp09109_HR_Qualification_Sources_With_BlankTableAdapter
            // 
            this.sp09109_HR_Qualification_Sources_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00002_Qualification_SubType_ValidityTableAdapter
            // 
            this.sp_TR_00002_Qualification_SubType_ValidityTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00004_Qualification_SubType_RefreshTableAdapter
            // 
            this.sp_TR_00004_Qualification_SubType_RefreshTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00009_Assessment_TypeTableAdapter
            // 
            this.sp_TR_00009_Assessment_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Qualification_Edit
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(753, 532);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Qualification_Edit";
            this.Text = "Qualification Edit";
            this.Activated += new System.EventHandler(this.frm_HR_Qualification_Edit_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Qualification_Edit_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Qualification_Edit_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LinkedDocumentCountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09105HRQualificationItemBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationTypeButtonEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ArchivedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WoodPlanCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityRailCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UtilityArbCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WinterMaintenanceCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SummerMaintenanceCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RemarksMemoEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonTypeIDLookupEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09107HRQualificationLinkedRecordTypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkedToPersonIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.validityTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00002QualificationSubTypeValidityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.refreshTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00004QualificationSubTypeRefreshBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedToPersonTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForArchived)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLinkedDocumentCount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSummerMaintenance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWinterMaintenance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityArb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUtilityRail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForWoodPlan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRemarks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValidityTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpiryDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRefreshDescription)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09109HRQualificationSourcesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00009AssessmentTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp09108HRCertificateTypesWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT_DataEntry)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DataSet_AT dataSet_AT;
        private DevExpress.XtraEditors.MemoEdit RemarksMemoEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedToPersonTypeID;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRemarks;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.LookUpEdit LinkedToPersonTypeIDLookupEdit;
        private DataSet_AT_DataEntry dataSet_AT_DataEntry;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.TextEdit LinkedToPersonIDTextEdit;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private System.Windows.Forms.BindingSource sp09105HRQualificationItemBindingSource;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DataSet_HR_DataEntryTableAdapters.sp09105_HR_Qualification_ItemTableAdapter sp09105_HR_Qualification_ItemTableAdapter;
        private DevExpress.XtraEditors.TextEdit QualificationIDTextEdit;
        private DataSet_HR_Core dataSet_HR_Core;
        private System.Windows.Forms.BindingSource sp09107HRQualificationLinkedRecordTypesBindingSource;
        private DataSet_HR_CoreTableAdapters.sp09107_HR_Qualification_Linked_Record_TypesTableAdapter sp09107_HR_Qualification_Linked_Record_TypesTableAdapter;
        private System.Windows.Forms.BindingSource sp09108HRCertificateTypesWithBlankBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp09108_HR_Certificate_Types_With_BlankTableAdapter sp09108_HR_Certificate_Types_With_BlankTableAdapter;
        private System.Windows.Forms.BindingSource sp09109HRQualificationSourcesWithBlankBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp09109_HR_Qualification_Sources_With_BlankTableAdapter sp09109_HR_Qualification_Sources_With_BlankTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraEditors.CheckEdit SummerMaintenanceCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSummerMaintenance;
        private DevExpress.XtraEditors.CheckEdit WoodPlanCheckEdit;
        private DevExpress.XtraEditors.CheckEdit UtilityRailCheckEdit;
        private DevExpress.XtraEditors.CheckEdit UtilityArbCheckEdit;
        private DevExpress.XtraEditors.CheckEdit WinterMaintenanceCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWinterMaintenance;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUtilityArb;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUtilityRail;
        private DevExpress.XtraLayout.LayoutControlItem ItemForWoodPlan;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocuments;
        private DevExpress.XtraEditors.CheckEdit ArchivedCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForArchived;
        private DevExpress.XtraEditors.TextEdit QualificationTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationTypeID;
        private DevExpress.XtraEditors.TextEdit QualificationSubTypeIDTextEdit;
        private DevExpress.XtraEditors.ButtonEdit QualificationTypeButtonEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationSubTypeID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationSubType;
        private DevExpress.XtraEditors.TextEdit QualificationSubTypeTextEdit;
        private DevExpress.XtraEditors.TextEdit LinkedDocumentCountTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLinkedDocumentCount;
        private DevExpress.XtraBars.BarButtonItem bbiLinkedDocumentAdd;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem15;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem17;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem19;
        private DevExpress.XtraEditors.GridLookUpEdit validityTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValidityTypeID;
        private DevExpress.XtraEditors.GridLookUpEdit refreshTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRefreshTypeID;
        private DataSet_TR_Core dataSet_TR_Core;
        private System.Windows.Forms.BindingSource spTR00002QualificationSubTypeValidityBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00002_Qualification_SubType_ValidityTableAdapter sp_TR_00002_Qualification_SubType_ValidityTableAdapter;
        private System.Windows.Forms.BindingSource spTR00004QualificationSubTypeRefreshBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00004_Qualification_SubType_RefreshTableAdapter sp_TR_00004_Qualification_SubType_RefreshTableAdapter;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private MaintainPersonQualificationRefreshedBy maintainPersonQualificationRefreshedBy;
        private MaintainPersonQualificationRefreshes maintainPersonQualificationRefreshes;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpiryDescription;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRefreshDescription;
        private System.Windows.Forms.BindingSource spTR00009AssessmentTypeBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00009_Assessment_TypeTableAdapter sp_TR_00009_Assessment_TypeTableAdapter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private MaintainPersonQualificationHistory maintainPersonQualificationHistory;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
    }
}

﻿namespace WoodPlan5
{
    partial class MaintainGenericJobTitleQualification
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintainGenericJobTitleQualification));
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.spTR00022GenericJobTitleAllocationSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAllocationID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryPeriodDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshIntervalDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeLimitDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_TR_00022_Generic_Job_Title_Allocation_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00022_Generic_Job_Title_Allocation_SelectTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00022GenericJobTitleAllocationSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.spTR00022GenericJobTitleAllocationSelectBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl1.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl1.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControl1.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAwardingBody_EmbeddedNavigator_ButtonClick);
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControl1.Size = new System.Drawing.Size(469, 241);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spTR00022GenericJobTitleAllocationSelectBindingSource
            // 
            this.spTR00022GenericJobTitleAllocationSelectBindingSource.DataMember = "sp_TR_00022_Generic_Job_Title_Allocation_Select";
            this.spTR00022GenericJobTitleAllocationSelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAllocationID,
            this.colQualificationTypeID,
            this.colQualificationTypeDescription,
            this.colQualificationSubTypeID,
            this.colQualificationSubTypeDescription,
            this.colStatus,
            this.colRequirementType,
            this.colValidity,
            this.colExpiryPeriodDescription,
            this.colRefreshDescription,
            this.colRefreshIntervalDescription,
            this.colTimeLimitDescription,
            this.colStartDate,
            this.colEndDate});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colAllocationID
            // 
            this.colAllocationID.FieldName = "AllocationID";
            this.colAllocationID.Name = "colAllocationID";
            this.colAllocationID.OptionsColumn.AllowEdit = false;
            this.colAllocationID.OptionsColumn.AllowFocus = false;
            this.colAllocationID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationTypeID
            // 
            this.colQualificationTypeID.FieldName = "QualificationTypeID";
            this.colQualificationTypeID.Name = "colQualificationTypeID";
            this.colQualificationTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationTypeDescription
            // 
            this.colQualificationTypeDescription.Caption = "Master Type";
            this.colQualificationTypeDescription.FieldName = "QualificationTypeDescription";
            this.colQualificationTypeDescription.Name = "colQualificationTypeDescription";
            this.colQualificationTypeDescription.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeDescription.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeDescription.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeDescription.Visible = true;
            this.colQualificationTypeDescription.VisibleIndex = 0;
            this.colQualificationTypeDescription.Width = 79;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationSubTypeDescription
            // 
            this.colQualificationSubTypeDescription.FieldName = "QualificationSubTypeDescription";
            this.colQualificationSubTypeDescription.Name = "colQualificationSubTypeDescription";
            this.colQualificationSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeDescription.Visible = true;
            this.colQualificationSubTypeDescription.VisibleIndex = 1;
            this.colQualificationSubTypeDescription.Width = 180;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.AllowFocus = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 7;
            this.colStatus.Width = 78;
            // 
            // colRequirementType
            // 
            this.colRequirementType.Caption = "Requirement";
            this.colRequirementType.FieldName = "RequirementType";
            this.colRequirementType.Name = "colRequirementType";
            this.colRequirementType.OptionsColumn.AllowEdit = false;
            this.colRequirementType.OptionsColumn.AllowFocus = false;
            this.colRequirementType.OptionsColumn.ReadOnly = true;
            this.colRequirementType.Visible = true;
            this.colRequirementType.VisibleIndex = 2;
            this.colRequirementType.Width = 73;
            // 
            // colValidity
            // 
            this.colValidity.FieldName = "Validity";
            this.colValidity.Name = "colValidity";
            this.colValidity.OptionsColumn.AllowEdit = false;
            this.colValidity.OptionsColumn.AllowFocus = false;
            this.colValidity.OptionsColumn.ReadOnly = true;
            this.colValidity.Visible = true;
            this.colValidity.VisibleIndex = 3;
            this.colValidity.Width = 55;
            // 
            // colExpiryPeriodDescription
            // 
            this.colExpiryPeriodDescription.Caption = "Expiry Period";
            this.colExpiryPeriodDescription.FieldName = "ExpiryPeriodDescription";
            this.colExpiryPeriodDescription.Name = "colExpiryPeriodDescription";
            this.colExpiryPeriodDescription.OptionsColumn.AllowEdit = false;
            this.colExpiryPeriodDescription.OptionsColumn.AllowFocus = false;
            this.colExpiryPeriodDescription.OptionsColumn.ReadOnly = true;
            this.colExpiryPeriodDescription.Visible = true;
            this.colExpiryPeriodDescription.VisibleIndex = 4;
            this.colExpiryPeriodDescription.Width = 72;
            // 
            // colRefreshDescription
            // 
            this.colRefreshDescription.Caption = "Refresh Type";
            this.colRefreshDescription.FieldName = "RefreshDescription";
            this.colRefreshDescription.Name = "colRefreshDescription";
            this.colRefreshDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshDescription.OptionsColumn.AllowFocus = false;
            this.colRefreshDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshDescription.Visible = true;
            this.colRefreshDescription.VisibleIndex = 5;
            this.colRefreshDescription.Width = 64;
            // 
            // colRefreshIntervalDescription
            // 
            this.colRefreshIntervalDescription.Caption = "Refresh Interval";
            this.colRefreshIntervalDescription.FieldName = "RefreshIntervalDescription";
            this.colRefreshIntervalDescription.Name = "colRefreshIntervalDescription";
            this.colRefreshIntervalDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshIntervalDescription.OptionsColumn.AllowFocus = false;
            this.colRefreshIntervalDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshIntervalDescription.Visible = true;
            this.colRefreshIntervalDescription.VisibleIndex = 6;
            this.colRefreshIntervalDescription.Width = 84;
            // 
            // colTimeLimitDescription
            // 
            this.colTimeLimitDescription.Caption = "Time Limit";
            this.colTimeLimitDescription.FieldName = "TimeLimitDescription";
            this.colTimeLimitDescription.Name = "colTimeLimitDescription";
            this.colTimeLimitDescription.OptionsColumn.AllowEdit = false;
            this.colTimeLimitDescription.OptionsColumn.AllowFocus = false;
            this.colTimeLimitDescription.OptionsColumn.ReadOnly = true;
            this.colTimeLimitDescription.Visible = true;
            this.colTimeLimitDescription.VisibleIndex = 8;
            this.colTimeLimitDescription.Width = 56;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Start Date";
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Visible = true;
            this.colStartDate.VisibleIndex = 9;
            this.colStartDate.Width = 56;
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "End Date";
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Visible = true;
            this.colEndDate.VisibleIndex = 10;
            this.colEndDate.Width = 91;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControl1;
            // 
            // sp_TR_00022_Generic_Job_Title_Allocation_SelectTableAdapter
            // 
            this.sp_TR_00022_Generic_Job_Title_Allocation_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // MaintainGenericJobTitleQualification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Name = "MaintainGenericJobTitleQualification";
            this.Size = new System.Drawing.Size(469, 241);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00022GenericJobTitleAllocationSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colValidity;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryPeriodDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshIntervalDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshDescription;
        private DataSet_TR_Core dataSet_TR_Core;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeDescription;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private System.Windows.Forms.BindingSource spTR00022GenericJobTitleAllocationSelectBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAllocationID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeDescription;
        private DataSet_TR_CoreTableAdapters.sp_TR_00022_Generic_Job_Title_Allocation_SelectTableAdapter sp_TR_00022_Generic_Job_Title_Allocation_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementType;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeLimitDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
    }
}

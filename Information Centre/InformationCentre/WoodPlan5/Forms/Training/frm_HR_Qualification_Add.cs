using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;

namespace WoodPlan5
{
    public partial class frm_HR_Qualification_Add : BaseObjects.frmBase
    {
        #region Instance Variables
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;

        public int intLinkedToRecordID = 0;
        public string strLinkedToRecordDesc = "";
        public int intRecordTypeID = 0;  // 0 = Staff, 1 = Contractor, 2 = Contractor Team Member //

        public int intQualificationTypeID = 0;
        public int intQualificationSubTypeID = 0;
        public string strQualificationType = "";
        public string strQualificationSubType = "";

        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        private DataSet_HR_DataEntryTableAdapters.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter sp_HR_00234_Master_Qualificaton_SubTyp_EditTableAdapter;

        private DataRow subTypeDataRow;
        #endregion

        public frm_HR_Qualification_Add()
        {
            InitializeComponent();
        }

        private void frm_HR_Qualification_Add_Load(object sender, EventArgs e)
        {
            this.FormID = 700011;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(9, "HR_QualificationCertificatePath").ToString();
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the default Linked Files path (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Default Linked Files path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            sp09107_HR_Qualification_Linked_Record_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
            sp09107_HR_Qualification_Linked_Record_TypesTableAdapter.Fill(this.dataSet_HR_Core.sp09107_HR_Qualification_Linked_Record_Types);

            sp09108_HR_Certificate_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp09108_HR_Certificate_Types_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp09108_HR_Certificate_Types_With_Blank);

            sp09109_HR_Qualification_Sources_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp09109_HR_Qualification_Sources_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp09109_HR_Qualification_Sources_With_Blank,0);

            sp_HR_00234_Master_Qualificaton_SubTyp_EditTableAdapter = new DataSet_HR_DataEntryTableAdapters.sp_HR_00234_Master_Qualification_SubType_EditTableAdapter();
            sp_HR_00234_Master_Qualificaton_SubTyp_EditTableAdapter.Connection.ConnectionString = strConnectionString;

            sp_TR_00002_Qualification_SubType_ValidityTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00002_Qualification_SubType_ValidityTableAdapter.Fill(this.dataSet_TR_Core.sp_TR_00002_Qualification_SubType_Validity);

            sp_TR_00004_Qualification_SubType_RefreshTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00004_Qualification_SubType_RefreshTableAdapter.Fill(this.dataSet_TR_Core.sp_TR_00004_Qualification_SubType_Refresh);

            sp_TR_00009_Assessment_TypeTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00009_Assessment_TypeTableAdapter.Fill(this.dataSet_TR_Core.sp_TR_00009_Assessment_Type);

            // Populate Main Dataset //

            this.sp09105_HR_Qualification_ItemTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        if (strRecordIDs == "")
                        {
                            DataRow drNewRow;
                            drNewRow = this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["QualificationID"] = 0;
                            drNewRow["HistoryID"] = 0;
                            drNewRow["LinkedToPersonID"] = intLinkedToRecordID;
                            drNewRow["LinkedToPersonName"] = strLinkedToRecordDesc;
                            drNewRow["LinkedToPersonTypeID"] = intRecordTypeID;
                            drNewRow["CostToCompany"] = 0.00;
                            drNewRow["SummerMaintenance"] = 1;
                            drNewRow["WinterMaintenance"] = 1;
                            drNewRow["UtilityArb"] = 1;
                            drNewRow["UtilityRail"] = 1;
                            drNewRow["WoodPlan"] = 1;
                            drNewRow["Archived"] = 0;
                            drNewRow["QualificationTypeID"] = intQualificationTypeID;
                            drNewRow["QualificationType"] = strQualificationType;
                            drNewRow["QualificationSubTypeID"] = intQualificationSubTypeID;
                            drNewRow["QualificationSubType"] = strQualificationSubType;
                            drNewRow["LinkedDocumentCount"] = 0;
                            drNewRow["ValidityTypeID"] = 0;
                            drNewRow["RefreshTypeID"] = 0;
                            drNewRow["ExpiryPeriodTypeID"] = 0;
                            drNewRow["ExpiryPeriodValue"] = 0;
                            drNewRow["RefreshIntervalTypeID"] = 0;
                            drNewRow["RefreshIntervalValue"] = 0;
                            drNewRow["AssessmentTypeID"] = 0;


                            this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows.Add(drNewRow);
                        }
                        else
                        {
                            sp09105_HR_Qualification_ItemTableAdapter.Fill(this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item, strRecordIDs, strFormMode);

                            DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;

                            currentRow["StartDate"] = DBNull.Value;
                            currentRow["ExpiryDate"] = DBNull.Value;
                            currentRow["AssessmentTypeID"] = 0;
                            currentRow["AssessmentDate"] = DBNull.Value;
                            currentRow["QualificationFromID"] = 0;
                            currentRow["CourseNumber"] = DBNull.Value;
                            currentRow["CourseName"] = DBNull.Value;
                            currentRow["CourseStartDate"] = DBNull.Value;
                            currentRow["CourseEndDate"] = DBNull.Value;
                            currentRow["CostToCompany"] = 0;
                           
                            DataRow dr = dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows[0];
                            int qualificationSubTypeID = 0;
                            int.TryParse(dr["QualificationSubTypeID"].ToString(),out qualificationSubTypeID);

                            //Reload the Awarding body selection list based on the QualificationSubType selected
                            sp09109_HR_Qualification_Sources_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp09109_HR_Qualification_Sources_With_Blank, qualificationSubTypeID);

                        }
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.NewRow();
                        drNewRow["strMode"] = "blockadd";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["QualificationID"] = 0;
                        drNewRow["HistoryID"] = 0;
                        drNewRow["LinkedToPersonID"] = 0;
                        drNewRow["LinkedToPersonName"] = strLinkedToRecordDesc; //"Not Applicable - Block Adding";
                        drNewRow["LinkedToPersonTypeID"] = intRecordTypeID;
                        drNewRow["CostToCompany"] = 0.00;
                        drNewRow["SummerMaintenance"] = 1;
                        drNewRow["WinterMaintenance"] = 1;
                        drNewRow["UtilityArb"] = 1;
                        drNewRow["UtilityRail"] = 1;
                        drNewRow["WoodPlan"] = 1;
                        drNewRow["ValidityTypeID"] = 0;
                        drNewRow["RefreshTypeID"] = 0;
                        drNewRow["ExpiryPeriodTypeID"] = 0;
                        drNewRow["ExpiryPeriodValue"] = 0;
                        drNewRow["RefreshIntervalTypeID"] = 0;
                        drNewRow["RefreshIntervalValue"] = 0;
                        drNewRow["AssessmentTypeID"] = 0;

                        this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows.Add(drNewRow);
                    }
                    catch (Exception Ex)
                    {
                        DevExpress.XtraEditors.XtraMessageBox.Show("Error: " + Ex.Message);
                    }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.NewRow();
                        drNewRow["strMode"] = "blockedit";
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["QualificationID"] = 0;
                        drNewRow["HistoryID"] = 0;
                        drNewRow["LinkedToPersonID"] = 0;
                        drNewRow["LinkedToPersonTypeID"] = 0;
                        this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows.Add(drNewRow);
                        this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //

                    }
                    catch (Exception)
                    {
                    }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp09105_HR_Qualification_ItemTableAdapter.Fill(this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item, strRecordIDs, strFormMode);



                    }
                    catch (Exception)
                    {
                    }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Qualification", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        buttonEdit1.Focus();

                        if (strRecordIDs == "")
                        {
                            buttonEdit1.Properties.ReadOnly = false;
                            buttonEdit1.Properties.Buttons[0].Enabled = true;

                            QualificationTypeButtonEdit.Properties.ReadOnly = false;
                            QualificationTypeButtonEdit.Properties.Buttons[0].Enabled = true;
                        }
                        else
                        {
                            buttonEdit1.Properties.ReadOnly = true;
                            buttonEdit1.Properties.Buttons[0].Enabled = false;

                            QualificationTypeButtonEdit.Properties.ReadOnly = true;
                            QualificationTypeButtonEdit.Properties.Buttons[0].Enabled = false;
                        }
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Adding";
                        barStaticItemFormMode.ImageIndex = 0;  // Add //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block adding - a copy of the current record will be created for each record you are adding to.";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        buttonEdit1.Focus();

                        buttonEdit1.Properties.ReadOnly = false;
                        buttonEdit1.Properties.Buttons[0].Enabled = true;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        //DescriptionTextEdit.Focus();
                        /*
                        buttonEdit1.Properties.ReadOnly = false;
                        buttonEdit1.Properties.Buttons[0].Enabled = true;
                        */
                        buttonEdit1.Properties.ReadOnly = true;
                        buttonEdit1.Properties.Buttons[0].Enabled = false;

                        QualificationTypeButtonEdit.Properties.ReadOnly = true;
                        QualificationTypeButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        //DescriptionTextEdit.Focus();

                        buttonEdit1.Properties.ReadOnly = true;
                        buttonEdit1.Properties.Buttons[0].Enabled = false;

                        QualificationTypeButtonEdit.Properties.ReadOnly = true;
                        QualificationTypeButtonEdit.Properties.Buttons[0].Enabled = false;
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }
            SetChangesPendingLabel();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //
            
            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            int intID = 0;  // Check if the current record has been saved at some point //
            DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
            if (currentRow != null)
            {
                intID = (currentRow["QualificationID"] == null ? 0 : Convert.ToInt32(currentRow["QualificationID"]));
            }
            bbiLinkedDocuments.Enabled = intID > 0;  // Set status of Linked Documents button //
            bbiLinkedDocumentAdd.Enabled = !(strFormMode == "blockadd" || strFormMode == "blockedit");
            
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);
        }


        private void frm_HR_Qualification_Add_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Qualification_Add_FormClosing(object sender, FormClosingEventArgs e)
        {
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.sp09105HRQualificationItemBindingSource.EndEdit();
            try
            {
                this.sp09105_HR_Qualification_ItemTableAdapter.Update(dataSet_HR_DataEntry);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["QualificationID"]) + ";";
            }
            else if (this.strFormMode.ToLower() == "blockadd")
            {
                DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToString(currentRow["strRecordIDs"]);  // Field originally held district IDs, but now holds new record linked document ids (changed via Update SP) //
            }

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Qualification_Edit")
                    {
                        frm_HR_Qualification_Edit fParentForm = (frm_HR_Qualification_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1);
                    }

                    if (frmChild.Name == "frm_HR_Qualifications_Manager")
                    {
                        frm_HR_Qualifications_Manager fParentForm = (frm_HR_Qualifications_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                    }
                    if (frmChild.Name == "frm_HR_Training_Manager")
                    {
                        frm_HR_Training_Manager fParentForm = (frm_HR_Training_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, strNewIDs);
                    }
                    if (frmChild.Name == "frm_HR_Employee_Manager")
                    {
                        frm_HR_Employee_Manager fParentForm = (frm_HR_Employee_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, WoodPlan5.Classes.HR.Utils.enmMainGrids.Training, strNewIDs);
                    }
                    if (frmChild.Name == "frm_HR_Employee_Edit")
                    {
                        frm_HR_Employee_Edit fParentForm = (frm_HR_Employee_Edit)frmChild;
                        fParentForm.UpdateFormRefreshStatus(3, WoodPlan5.Classes.HR.Utils.enmMainGrids.Training, strNewIDs);
                    }
                    if (frmChild.Name == "frm_Core_Contractor_Manager")
                    {
                        frm_Core_Contractor_Manager fParentForm = (frm_Core_Contractor_Manager)frmChild;
                        if (intRecordTypeID == 1)  // Team //
                        {
                            fParentForm.UpdateFormRefreshStatus(2, "", "", "", "", "", "", "", "", "", strNewIDs, "", "");
                        }
                        else if (intRecordTypeID == 2)  // Team Member //
                        {
                            fParentForm.UpdateFormRefreshStatus(3, "", "", "", "", "", "", "", "", "", "", strNewIDs, "");
                        }
                    }
                }
            }

            SetMenuStatus();  // Disables Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();
            Application.DoEvents();
            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows.Count; i++)
            {
                switch (this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }
            string strMessage = "";
            if (intRecordNew > 0 || intRecordModified > 0 || intRecordDeleted > 0)
            {
                if (this.strFormMode != "blockedit")
                {
                    strMessage = "You have unsaved changes on the current screen...\n\n";
                    if (intRecordNew > 0) strMessage += Convert.ToString(intRecordNew) + " New record(s)\n";
                    if (intRecordModified > 0) strMessage += Convert.ToString(intRecordModified) + " Updated record(s)\n";
                    if (intRecordDeleted > 0) strMessage += Convert.ToString(intRecordDeleted) + " Deleted record(s)\n";
                }
                else  // Block Editing //
                {
                    strMessage = "You have unsaved block edit changes on the current screen...\n";
                }
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }


            //Set up tabbed detail screens
            if (this.strFormMode.ToLower() == "edit" || this.strFormMode.ToLower() == "view")
            {
                DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
                if (currentRow != null)
                {
                    int qualificationID = Convert.ToInt32(currentRow["QualificationID"]);

                  maintainPersonQualificationHistory.SetUp(strConnectionString, qualificationID, "View");

                }
            }

        }

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void buttonEdit1_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            
            DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
            if (currentRow == null) return;
            int intLinkedToPersonTypeID = (currentRow["LinkedToPersonTypeID"].ToString() == null ? 0 : Convert.ToInt32(currentRow["LinkedToPersonTypeID"]));

            switch (intLinkedToPersonTypeID)
            {
                case 0:  // Employee // 
                    {
                        var fChildForm1 = new frm_HR_Select_Employee();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1._Mode = (strFormMode == "blockadd" ? "multiple" : "single");
                        if (strFormMode != "blockadd")
                        {
                            fChildForm1.intOriginalEmployeeID = Convert.ToInt32(currentRow["LinkedToPersonID"]);
                        }
                        else  // blockadd //
                        {
                            fChildForm1.strOriginalEmployeeIDs = currentRow["strRecordIDs"].ToString();
                        }
                        if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (strFormMode != "blockadd")
                            {
                                currentRow["LinkedToPersonID"] = fChildForm1.intSelectedEmployeeID;
                                currentRow["LinkedToPersonName"] = fChildForm1.strSelectedEmployeeName;
                            }
                            else  // blockadd //
                            {
                                currentRow["strRecordIDs"] = fChildForm1.strSelectedEmployeeIDs;
                                string name = fChildForm1.strSelectedEmployeeName;
                                currentRow["LinkedToPersonName"] = name.Substring(0,100);
                            }
                        }
                    }
                    break;
                case 1:  // Contractor // 
                    {
                        var fChildForm1 = new frm_HR_Select_Contractor();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1._Mode = (strFormMode == "blockadd" ? "multiple" : "single");
                        if (strFormMode != "blockadd")
                        {
                            fChildForm1.intOriginalContractorID = Convert.ToInt32(currentRow["LinkedToPersonID"]);
                        }
                        else  // blockadd //
                        {
                            fChildForm1.strOriginalContractorIDs = currentRow["strRecordIDs"].ToString();
                        }
                        if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (strFormMode != "blockadd")
                            {
                                currentRow["LinkedToPersonID"] = fChildForm1.intSelectedContractorID;
                                currentRow["LinkedToPersonName"] = fChildForm1.strSelectedContractorName;
                            }
                            else  // blockadd //
                            {
                                currentRow["strRecordIDs"] = fChildForm1.strSelectedContractorIDs;
                                string name = fChildForm1.strSelectedContractorName;
                                currentRow["LinkedToPersonName"] = name.Substring(0,100);
                            }
                        }
                    }
                    break;
                case 2:  // Contractor Team Member // 
                    {
                        var fChildForm1 = new frm_HR_Select_Contractor_Team_Member();
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1._Mode = (strFormMode == "blockadd" ? "multiple" : "single");
                        if (strFormMode != "blockadd")
                        {
                            fChildForm1.intOriginalTeamMemberID = Convert.ToInt32(currentRow["LinkedToPersonID"]);
                        }
                        else  // blockadd //
                        {
                            fChildForm1.strOriginalTeamMemberIDs = currentRow["strRecordIDs"].ToString();
                        }
                        if (fChildForm1.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (strFormMode != "blockadd")
                            {
                                currentRow["LinkedToPersonID"] = fChildForm1.intSelectedTeamMemberID;
                                currentRow["LinkedToPersonName"] = fChildForm1.strSelectedTeamMemberName;
                            }
                            else  // blockadd //
                            {
                                currentRow["strRecordIDs"] = fChildForm1.strSelectedTeamMemberIDs;
                                string name = fChildForm1.strSelectedTeamMemberName;
                                currentRow["LinkedToPersonName"] = name.Substring(1, 100);
                            }
                        }
                    }
                    break;

                 default:
                    break;
            }
        }
        private void buttonEdit1_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(buttonEdit1, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(buttonEdit1, "");
            }
        }

        private void StartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit"
                && string.IsNullOrEmpty(de.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "");
            }
        }

        //End Date is actually the Expiry or Lapse date depending on the Validity Type 
        // and/or RefreshType
        private void EndDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit"
                && (
                    (!String.IsNullOrEmpty(validityTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(validityTypeGridLookUpEdit.EditValue) == 2)
                ||  (!String.IsNullOrEmpty(refreshTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(refreshTypeGridLookUpEdit.EditValue) == 1)
                    )
                && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(EndDateDateEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(EndDateDateEdit, "");
            }
        }

        private void QualificationFromIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(be.EditValue.ToString()) || be.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(QualificationFromIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(QualificationFromIDGridLookUpEdit, "");
            }
        }

        private void QualificationTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
                if (currentRow == null) return;
                int intQualificationTypeID = (string.IsNullOrEmpty(currentRow["QualificationTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["QualificationTypeID"]));
                int intQualificationSubTypeID = (string.IsNullOrEmpty(currentRow["QualificationSubTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["QualificationSubTypeID"]));
                var fChildForm = new frm_HR_Select_Qualification_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intPassedInQualificationTypeID = intQualificationTypeID;
                fChildForm.intPassedInQualificationSubTypeID = intQualificationSubTypeID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    currentRow["QualificationTypeID"] = fChildForm.intSelectedQualificiationTypeID;
                    currentRow["QualificationSubTypeID"] = fChildForm.intSelectedQualificationSubTypeID;
                    currentRow["QualificationType"] = fChildForm.strSelectedQualificationType;
                    currentRow["QualificationSubType"] = fChildForm.strSelectedQualificationSubType;

                    sp_HR_00234_Master_Qualificaton_SubTyp_EditTableAdapter.Fill(
                        dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit, "edit", currentRow["QualificationSubTypeID"].ToString());

                    //Reload the Awarding body selection list based on the QualificationSubType selected
                    sp09109_HR_Qualification_Sources_With_BlankTableAdapter.Fill(dataSet_HR_DataEntry.sp09109_HR_Qualification_Sources_With_Blank, fChildForm.intSelectedQualificationSubTypeID);

                    subTypeDataRow = dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows[0];

                    currentRow["ValidityTypeID"] = subTypeDataRow["ValidityTypeID"];
                    currentRow["RefreshTypeID"] = subTypeDataRow["RefreshTypeID"];

                    currentRow["ExpiryPeriodTypeID"] = subTypeDataRow["ExpiryPeriodTypeID"];
                    currentRow["ExpiryPeriodValue"] = subTypeDataRow["ExpiryPeriodValue"];
                    currentRow["ExpiryPeriodDescription"] = subTypeDataRow["ExpiryPeriodDescription"];

                    currentRow["RefreshIntervalTypeID"] = subTypeDataRow["RefreshIntervalTypeID"];
                    currentRow["RefreshIntervalValue"] = subTypeDataRow["RefreshIntervalValue"];
                    currentRow["RefreshIntervalDescription"] = subTypeDataRow["RefreshIntervalDescription"];

                    sp09105HRQualificationItemBindingSource.EndEdit();
                }
            }
        }
        private void QualificationTypeButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(QualificationTypeButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(QualificationTypeButtonEdit, "");
            }
        }

        #endregion

        private DateTime CalculateDate(DateTime startDate, int intervalType, int intervalValue)
        {
            DateTime newDate = startDate;

            switch (intervalType)
            {
                case 1: //Days
                    newDate = newDate.AddDays(intervalValue);
                    break;
                case 2: //Weeks
                    newDate = newDate.AddDays(7 * intervalValue);
                    break;
                case 3: //Months
                    newDate = newDate.AddMonths(intervalValue);
                    break;
                case 4: //Years
                    newDate = newDate.AddYears(intervalValue);
                    break;
                default:
                    break;
            }

            return newDate;
        }



        private void bbiLinkedDocuments_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (strFormMode == "blockedit" || strFormMode == "blockadd") return;
            DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
            if (currentRow == null) return;
            int intRecordID = (string.IsNullOrEmpty(currentRow["QualificationID"].ToString()) ? 0 : Convert.ToInt32(currentRow["QualificationID"]));
            if (intRecordID <= 0) return;

            // Drilldown to Linked Document Manager //
            int intRecordType = 4;  // Training //
            int intRecordSubType = 0;  // Not Used //
            string strRecordDescription = currentRow["LinkedToPersonName"].ToString() + " - " + currentRow["CourseName"].ToString() + " [" + currentRow["CourseNumber"].ToString() + "]";
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_HR_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp_HR_00191_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "hr_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void bbiLinkedDocumentAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool boolProceed = true;
            try
            {
                DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
                if (currentRow == null) return;
                int intQualificationID = 0;
                if (!string.IsNullOrEmpty(currentRow["QualificationID"].ToString())) intQualificationID = Convert.ToInt32(currentRow["QualificationID"]);

                string strMessage = CheckForPendingSave();

                if (intQualificationID <= 0 || !string.IsNullOrWhiteSpace(strMessage))
                {
                    if (DevExpress.XtraEditors.XtraMessageBox.Show("The screen contains one or more pending changes.\n\nYou must save these before attempting to add a linked document.\n\nProceed with Saving?", "Add Linked Document", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        if (!string.IsNullOrEmpty(SaveChanges(true))) boolProceed = false;  // An error occurred //
                    }
                    else
                    {
                        boolProceed = false;  // The user said no to save changes //
                    }
                }
                if (boolProceed)
                {
                    if (!string.IsNullOrEmpty(currentRow["QualificationID"].ToString())) intQualificationID = Convert.ToInt32(currentRow["QualificationID"]);
                    var fChildForm = new frm_HR_Linked_Document_Edit();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.Name;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager1;
                    fChildForm.splashScreenManager.ShowWaitForm();

                    fChildForm.intLinkedToRecordID = intQualificationID;
                    fChildForm.strLinkedToRecordDesc = currentRow["LinkedToPersonName"].ToString() + " - " + currentRow["CourseName"].ToString() + " [" + currentRow["CourseNumber"].ToString() + "]";
                    fChildForm.intRecordTypeID = 4;  // Training //
                    fChildForm.strRecordTypeDescription = "Training Record";
                    fChildForm.intRecordSubTypeID = 0;  // Not Used //
                    fChildForm.strRecordSubTypeDescription = "";

                    //Expects it to be an instance of HR_Qualification_Edit, not Add
                    //so leave as null and do the getlinkeddocumentcount here regardless
                    //fChildForm.frmCallingQualificationForm = this;  // Required so called screen can update this specific instance on Save //
                    Get_Linked_Document_Count();
                    //

                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });

                    //If saved and go to Add linked Document then close this screen to avoid
                    //the duplicate saving on an Add
                    this.Close();
                }
            }
            catch (Exception) { }
        }
        public void Get_Linked_Document_Count()
        {
            DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
            if (currentRow == null) return;
            int intQualificationID = 0;
            if (!string.IsNullOrEmpty(currentRow["QualificationID"].ToString())) intQualificationID = Convert.ToInt32(currentRow["QualificationID"]);
            if (intQualificationID <= 0) return;

            int intCount = 0;
            using (var GetRecordCount = new DataSet_HR_CoreTableAdapters.QueriesTableAdapter())
            {
                GetRecordCount.ChangeConnectionString(strConnectionString);
                try
                {
                    intCount = Convert.ToInt32(GetRecordCount.sp09115_Get_Linked_Doc_Count(intQualificationID, 4));
                }
                catch (Exception) { }
            }
            currentRow["LinkedDocumentCount"] = intCount;
            this.dataSet_HR_DataEntry.sp09105_HR_Qualification_Item.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
        }

        private void validityTypeGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            ItemForEndDate.Text = "End Date";

            if (validityTypeGridLookUpEdit.EditValue == null
                || String.IsNullOrEmpty(validityTypeGridLookUpEdit.EditValue.ToString())) { return; }
            
            int validityTypeID = Convert.ToInt32(validityTypeGridLookUpEdit.EditValue);

            if (validityTypeID == 1) ItemForEndDate.Text = "Lapse Date";
            if (validityTypeID == 2) ItemForEndDate.Text = "Expiry Date";

        }

        private void refreshTypeGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view") { return; }
            if (refreshTypeGridLookUpEdit.EditValue == null
                || String.IsNullOrEmpty(refreshTypeGridLookUpEdit.EditValue.ToString())) { return; }

            if (Convert.ToInt32(refreshTypeGridLookUpEdit.EditValue) == 2)
            {
                EndDateDateEdit.EditValue = null;
                EndDateDateEdit.Enabled = false;
            }
            else
            {
                EndDateDateEdit.Enabled = true;
            }
        }

        private void StartDateDateEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "blockedit") return;

            if (StartDateDateEdit.EditValue == null || String.IsNullOrEmpty(StartDateDateEdit.EditValue.ToString()))
            { return; }

            DataRowView currentRow = (DataRowView)sp09105HRQualificationItemBindingSource.Current;
            if (currentRow == null) return;


            if (!String.IsNullOrEmpty(validityTypeGridLookUpEdit.EditValue.ToString()))
            {
                if (Convert.ToInt32(validityTypeGridLookUpEdit.EditValue) == 2)
                {
                    EndDateDateEdit.EditValue =
                         CalculateDate((DateTime)StartDateDateEdit.EditValue,
                                        Convert.ToInt32(currentRow["ExpiryPeriodTypeID"]),
                                        Convert.ToInt32(currentRow["ExpiryPeriodValue"]));
                }
            
            if (Convert.ToInt32(refreshTypeGridLookUpEdit.EditValue) == 1)
                {
                    EndDateDateEdit.EditValue =
                         CalculateDate((DateTime)StartDateDateEdit.EditValue,
                                        Convert.ToInt32(currentRow["RefreshIntervalTypeID"]),
                                        Convert.ToInt32(currentRow["RefreshIntervalValue"]));
                }
            }
        

            /**
            if ( !String.IsNullOrEmpty(validityTypeGridLookUpEdit.EditValue.ToString())
                && Convert.ToInt32(validityTypeGridLookUpEdit.EditValue) == 2
                && ( EndDateDateEdit.EditValue == null 
                || String.IsNullOrEmpty(EndDateDateEdit.EditValue.ToString())))
            {
                EndDateDateEdit.EditValue =
                     CalculateDate((DateTime)StartDateDateEdit.EditValue,
                                    Convert.ToInt32(currentRow["ExpiryPeriodTypeID"]),
                                    Convert.ToInt32(currentRow["ExpiryPeriodValue"]));
            }

            if (  !String.IsNullOrEmpty(refreshTypeGridLookUpEdit.EditValue.ToString())
                && Convert.ToInt32(refreshTypeGridLookUpEdit.EditValue) == 1
                && ( EndDateDateEdit.EditValue == null
                || String.IsNullOrEmpty(EndDateDateEdit.EditValue.ToString())))
            {
                EndDateDateEdit.EditValue =
                     CalculateDate((DateTime)StartDateDateEdit.EditValue,
                                    Convert.ToInt32(currentRow["RefreshIntervalTypeID"]),
                                    Convert.ToInt32(currentRow["RefreshIntervalValue"]));
            }
            **/
        }

    }
}


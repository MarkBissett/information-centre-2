using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;

        private string subTypeID = "";

        public int _LastOrder = 0;
        #endregion

        public frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification()
        {
            InitializeComponent();
        }

        private void frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification_Load(object sender, EventArgs e)
        {
            this.FormID = 700004;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            // Populate Main Dataset //  

            sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit, strFormMode, strRecordIDs);

            subTypeID = this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows[0]["QualificationSubTypeID"].ToString();
            string typeID = this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows[0]["QualificationTypeID"].ToString();

            sp_HR_00232_Master_Qualification_Types_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00232_Master_Qualification_Types_EditTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00232_Master_Qualification_Types_Edit, strFormMode, typeID);
            
            dataLayoutControl1.BeginUpdate();
            try
            {
                sp_TR_00013_Qualification_Sub_Type_Refreshes_AvailableTableAdapter.Connection.ConnectionString = strConnectionString;
                sp_TR_00013_Qualification_Sub_Type_Refreshes_AvailableTableAdapter.Fill(this.dataSet_TR_Core.sp_TR_00013_Qualification_Sub_Type_Refreshes_Available, Convert.ToInt32(subTypeID));
            }
            catch
            (Exception)
            { }

            dataLayoutControl1.EndUpdate();
           
            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
            else
            {
                SetChangesPendingLabel();
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_HR_DataEntry.sp_HR_00232_Master_Qualification_Types_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Master Qualification Type", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;

            //Mode will only be "add"
            try
            {
             barStaticItemFormMode.Caption = "Form Mode: Adding";
             barStaticItemFormMode.ImageIndex = 0;  // Add //
             barStaticItemFormMode.Appearance.ForeColor = color;
             barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
             barStaticItemInformation.Caption = "Information:";
             barStaticItemInformation.ImageIndex = 2; // Information //
             barStaticItemInformation.Appearance.ForeColor = color;

             gridControl1.Focus();
            }
            catch (Exception)
            {}

            this.ValidateChildren();  // Force Validation Message on any controls containing them //

            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            /**
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_HR_DataEntry.GetChanges();
            **/

            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControl1.MainView;

            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            //if (dsChanges != null)
            if (intCount > 0)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[1].Visible = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[2].Visible = false;
            gridControl1.EmbeddedNavigator.Buttons.CustomButtons[3].Visible = false;
        }


        private void frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void frm_HR_Master_Qualification_SubType_Add_Refreshes_Qualification_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        //ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControl1.MainView;

            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Qualifications to add by clicking on them then try again.", "Add", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "None selected";
            }

            //this.spHR00232MasterQualificationTypesEditBindingSource.EndEdit();
            try
            {
                string strRecordIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, colQualificationSubTypeID)) + ",";
                }

                using (var Addrecords = new DataSet_TR_DataEntryTableAdapters.QueriesTableAdapter())
                {
                    Addrecords.ChangeConnectionString(strConnectionString);

                    try
                    {
                        Addrecords.sp_TR_00102_Qualification_Refreshes_Add(Convert.ToInt32(subTypeID), strRecordIDs);
                    }
                    catch (Exception) { } 
                }

            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }

            //
            gridView1.ClearSelection();

            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                frm_HR_Master_Qualification_SubType_Edit fParentForm;
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Master_Qualification_SubType_Edit")
                    {
                        fParentForm = (frm_HR_Master_Qualification_SubType_Edit)frmChild;
                        //fParentForm.UpdateRefreshStatus(1,strNewIDs);

                        fParentForm.UpdateFormRefreshStatus(2);

                    }
                }
            }

            SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

            fProgress.Close();
            fProgress.Dispose();           
            Application.DoEvents();

            if (GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) added.", "Add", MessageBoxButtons.OK, MessageBoxIcon.Information);

            return "";  // No problems //
        }

        private string CheckForPendingSave()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = (GridView)gridControl1.MainView;

            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;

            string strMessage = "";

            if (intCount > 0)
            {
                strMessage = "You have unsaved changes on the current screen...\n\n"
                 + Convert.ToString(intCount) + " New Link(s)\n";
            }
            return strMessage;
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator


        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors


        #endregion

        private void gridControlVettingSubTypes_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            Generic_EditChanged(sender, new EventArgs());
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }


    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using DevExpress.Data;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class MaintainQualificationRefreshedBy : DevExpress.XtraEditors.XtraUserControl
    {
        #region Instance Variables...
        private frmBase parent;
        
        private Settings set = Settings.Default;
        private string strConnectionString = "";
        private int parQualificationSubTypeID = 0;

        private bool SetUpDone = false;

        bool iBool_AllowAdd = false;
        bool iBool_AllowDelete = false;
        bool iBool_AllowEdit = false;

        bool isRunning = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        public RefreshGridState RefreshGridViewQualificationRefreshes;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        //string i_str_AddedQualificationSubTypeAwardingBodyIDs = "";

        #endregion

        public MaintainQualificationRefreshedBy()
        {
            InitializeComponent();
        }

        public void SetUp(
            string ConnectionString,
            int QualificationSubTypeID,
            string FormMode)
        {
            parent = (frmBase)this.ParentForm;
            
            if (FormMode != "view")
            {
                iBool_AllowAdd = true;

                //Not relevant on this screen
                //iBool_AllowEdit = true;

                iBool_AllowDelete = true;
            }

            if (iBool_AllowDelete)
            {
                gridView1.OptionsSelection.MultiSelectMode = GridMultiSelectMode.CheckBoxRowSelect;
                gridView1.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DefaultBoolean.False;
            }

            strConnectionString = ConnectionString;
            parQualificationSubTypeID = QualificationSubTypeID;
            
            sp_TR_00014_Qualification_Sub_Type_Refreshed_By_SelectTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewQualificationRefreshes = new RefreshGridState(gridView1, "ID");

            Load_Refreshed_By();

            emptyEditor = new RepositoryItem();

            SetMenuStatus();

            SetUpDone = true;
        }

        public void Reload()
        {
            if (!SetUpDone)
            { return; }

            Load_Refreshed_By();
        }


        private void Load_Refreshed_By()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            RefreshGridViewQualificationRefreshes.SaveViewInfo();
            gridControlRefreshedBy.BeginUpdate();

            sp_TR_00014_Qualification_Sub_Type_Refreshed_By_SelectTableAdapter.Fill(dataSet_TR_Core.sp_TR_00014_Qualification_Sub_Type_Refreshed_By_Select, parQualificationSubTypeID);
            this.RefreshGridViewQualificationRefreshes.LoadViewInfo(); // Reload any expanded groups and selected rows //

            gridControlRefreshedBy.EndUpdate();

            /**
            // Highlight any recently added new rows //
            if (i_str_AddedQualificationSubTypeAwardingBodyIDs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedQualificationSubTypeAwardingBodyIDs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                GridView view = (GridView)gridControlRefreshes.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedQualificationSubTypeAwardingBodyIDs = "";
            }
            **/
        }

        public void SetMenuStatus()
        {

            GridView view = (GridView)gridControlRefreshedBy.MainView;

            int[] intRowHandles;
            
            // Set enabled status of navigator custom buttons //
            intRowHandles = view.GetSelectedRows();
            gridControlRefreshedBy.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (iBool_AllowAdd ? true : false);
            gridControlRefreshedBy.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0 ? true : false);
            gridControlRefreshedBy.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0 ? true : false);
            gridControlRefreshedBy.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = intRowHandles.Length > 0;

        }

        private void gridControlAwardingBody_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    GridView view = (GridView)gridControlRefreshedBy.MainView;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        //Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        //View_Record();
                    }
                    break;
                default:
                    break;
            }
        }


        private void Add_Record()
        {
           if (!iBool_AllowAdd) return;
           
            GridView view = (GridView)gridControlRefreshedBy.MainView; ;
            MethodInfo method = null;

            view.PostEditor();
           
            /***
           RefreshGridViewStateAwardingBodye.SaveViewInfo();  // Store Grid View State //
           ***/

           int intMaxOrder = 0;
           for (int i = 0; i < view.DataRowCount; i++)
           {
               if (Convert.ToInt32(view.GetRowCellValue(i, "RecordOrder")) > intMaxOrder) intMaxOrder = Convert.ToInt32(view.GetRowCellValue(i, "RecordOrder"));
           }

            var fChildForm = new frm_HR_Master_Qualification_SubType_Add_Refreshed_By_Qualification();

           fChildForm.MdiParent = parent.MdiParent;
           fChildForm.GlobalSettings = parent.GlobalSettings;
           fChildForm.strRecordIDs = parQualificationSubTypeID.ToString() + ",";
           fChildForm.strFormMode = "add";
           fChildForm.strCaller = this.Name;
           fChildForm.intRecordCount = 0;
           fChildForm.FormPermissions = parent.FormPermissions;
           fChildForm._LastOrder = intMaxOrder;
           DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this.ParentForm, typeof(global::WoodPlan5.WaitForm1), true, true);
           fChildForm.splashScreenManager = splashScreenManager1;
           fChildForm.splashScreenManager.ShowWaitForm();
           fChildForm.Show();

           method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
           if (method != null) method.Invoke(fChildForm, new object[] { null });
                      
        }

        public bool ChangesPending()
        {
            GridView view = (GridView)gridControlRefreshedBy.MainView;
            return view.SelectedRowsCount > 0;
        }

        private void Delete_Record()
        {
            if (!iBool_AllowDelete) return;

            int[] intRowHandles;
            int intCount = 0;
            GridView view  = (GridView)gridControlRefreshedBy.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Awarding Bodies to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            string strMessage = "You have " + (intCount == 1 ? "1 Awarding Body" : Convert.ToString(intRowHandles.Length) + " Awarding Bodies") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Awarding Body" : "these Awarding Bodies") + " will no longer be available for selection and any related records will also be deleted!</color>";
            if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this.ParentForm, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                string strRecordIDs = "";
                foreach (int intRowHandle in intRowHandles)
                {
                    strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ID")) + ",";
                }

                this.RefreshGridViewQualificationRefreshes.SaveViewInfo();  // Store Grid View State so we can put it back once the grid is reloaded (preserve expanded items etc) //
                using (var RemoveRecords = new DataSet_TR_DataEntryTableAdapters.QueriesTableAdapter())
                {
                    RemoveRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        RemoveRecords.sp_TR_00103_Qualification_Refreshes_Delete(strRecordIDs, 1); //Deletion Mode 0 marks records as Historic. Mode 1 Deletes
                    }
                    catch (Exception) { }
                }

                Load_Refreshed_By();
                
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (parent.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void gridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            SetMenuStatus();
        }

        private void gridView1_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }

            SetMenuStatus();
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        public int RefreshedByCount
        {
            get
            {
                if (dataSet_TR_Core.sp_TR_00014_Qualification_Sub_Type_Refreshed_By_Select == null)
                    return 0;

                return dataSet_TR_Core.sp_TR_00014_Qualification_Sub_Type_Refreshed_By_Select.Count;
            }
        }
    }
}

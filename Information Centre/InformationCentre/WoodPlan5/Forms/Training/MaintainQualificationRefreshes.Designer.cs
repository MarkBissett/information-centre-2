﻿namespace WoodPlan5
{
    partial class MaintainQualificationRefreshes
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaintainQualificationRefreshes));
            this.gridControlRefreshes = new DevExpress.XtraGrid.GridControl();
            this.spTR00012QualificationSubTypeRefreshesSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMemberQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrderID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colValidity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpiryPeriodDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshIntervalDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshQualificationStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRefreshQualificationEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.sp_TR_00012_Qualification_Sub_Type_Refreshes_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00012_Qualification_Sub_Type_Refreshes_SelectTableAdapter();
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRefreshes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00012QualificationSubTypeRefreshesSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControlRefreshes
            // 
            this.gridControlRefreshes.DataSource = this.spTR00012QualificationSubTypeRefreshesSelectBindingSource;
            this.gridControlRefreshes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControlRefreshes.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControlRefreshes.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 3, true, true, "View Selected Record(s)", "view")});
            this.gridControlRefreshes.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlAwardingBody_EmbeddedNavigator_ButtonClick);
            this.gridControlRefreshes.Location = new System.Drawing.Point(0, 0);
            this.gridControlRefreshes.MainView = this.gridView1;
            this.gridControlRefreshes.Name = "gridControlRefreshes";
            this.gridControlRefreshes.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4});
            this.gridControlRefreshes.Size = new System.Drawing.Size(469, 241);
            this.gridControlRefreshes.TabIndex = 1;
            this.gridControlRefreshes.UseEmbeddedNavigator = true;
            this.gridControlRefreshes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // spTR00012QualificationSubTypeRefreshesSelectBindingSource
            // 
            this.spTR00012QualificationSubTypeRefreshesSelectBindingSource.DataMember = "sp_TR_00012_Qualification_Sub_Type_Refreshes_Select";
            this.spTR00012QualificationSubTypeRefreshesSelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "Preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("refresh_16x16.png", "images/actions/refresh_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/refresh_16x16.png"), 4);
            this.imageCollection1.Images.SetKeyName(4, "refresh_16x16.png");
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colMemberQualificationSubTypeID,
            this.colOrderID,
            this.colQualificationTypeDescription,
            this.colDescription,
            this.colStatus,
            this.colValidity,
            this.colExpiryPeriodDescription,
            this.colRefreshDescription,
            this.colRefreshIntervalDescription,
            this.colRefreshQualificationStartDate,
            this.colRefreshQualificationEndDate});
            this.gridView1.GridControl = this.gridControlRefreshes;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsLayout.StoreFormatRules = true;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.GotFocus += new System.EventHandler(this.gridView1_GotFocus);
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.ReadOnly = true;
            // 
            // colMemberQualificationSubTypeID
            // 
            this.colMemberQualificationSubTypeID.FieldName = "MemberQualificationSubTypeID";
            this.colMemberQualificationSubTypeID.Name = "colMemberQualificationSubTypeID";
            this.colMemberQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colMemberQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colOrderID
            // 
            this.colOrderID.FieldName = "OrderID";
            this.colOrderID.Name = "colOrderID";
            this.colOrderID.OptionsColumn.AllowEdit = false;
            this.colOrderID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationTypeDescription
            // 
            this.colQualificationTypeDescription.Caption = "Master Type";
            this.colQualificationTypeDescription.FieldName = "QualificationTypeDescription";
            this.colQualificationTypeDescription.Name = "colQualificationTypeDescription";
            this.colQualificationTypeDescription.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeDescription.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeDescription.Visible = true;
            this.colQualificationTypeDescription.VisibleIndex = 0;
            // 
            // colDescription
            // 
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 175;
            // 
            // colStatus
            // 
            this.colStatus.FieldName = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.OptionsColumn.AllowEdit = false;
            this.colStatus.OptionsColumn.ReadOnly = true;
            this.colStatus.Visible = true;
            this.colStatus.VisibleIndex = 6;
            this.colStatus.Width = 133;
            // 
            // colValidity
            // 
            this.colValidity.FieldName = "Validity";
            this.colValidity.Name = "colValidity";
            this.colValidity.OptionsColumn.AllowEdit = false;
            this.colValidity.OptionsColumn.ReadOnly = true;
            this.colValidity.Visible = true;
            this.colValidity.VisibleIndex = 2;
            this.colValidity.Width = 73;
            // 
            // colExpiryPeriodDescription
            // 
            this.colExpiryPeriodDescription.Caption = "Expiry Period";
            this.colExpiryPeriodDescription.FieldName = "ExpiryPeriodDescription";
            this.colExpiryPeriodDescription.Name = "colExpiryPeriodDescription";
            this.colExpiryPeriodDescription.OptionsColumn.AllowEdit = false;
            this.colExpiryPeriodDescription.OptionsColumn.ReadOnly = true;
            this.colExpiryPeriodDescription.Visible = true;
            this.colExpiryPeriodDescription.VisibleIndex = 3;
            this.colExpiryPeriodDescription.Width = 78;
            // 
            // colRefreshDescription
            // 
            this.colRefreshDescription.Caption = "Refresh Type";
            this.colRefreshDescription.FieldName = "RefreshDescription";
            this.colRefreshDescription.Name = "colRefreshDescription";
            this.colRefreshDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshDescription.Visible = true;
            this.colRefreshDescription.VisibleIndex = 4;
            this.colRefreshDescription.Width = 86;
            // 
            // colRefreshIntervalDescription
            // 
            this.colRefreshIntervalDescription.Caption = "Refresh Interval";
            this.colRefreshIntervalDescription.FieldName = "RefreshIntervalDescription";
            this.colRefreshIntervalDescription.Name = "colRefreshIntervalDescription";
            this.colRefreshIntervalDescription.OptionsColumn.AllowEdit = false;
            this.colRefreshIntervalDescription.OptionsColumn.ReadOnly = true;
            this.colRefreshIntervalDescription.Visible = true;
            this.colRefreshIntervalDescription.VisibleIndex = 5;
            this.colRefreshIntervalDescription.Width = 91;
            // 
            // colRefreshQualificationStartDate
            // 
            this.colRefreshQualificationStartDate.Caption = "Start Date";
            this.colRefreshQualificationStartDate.FieldName = "RefreshQualificationStartDate";
            this.colRefreshQualificationStartDate.Name = "colRefreshQualificationStartDate";
            this.colRefreshQualificationStartDate.OptionsColumn.AllowEdit = false;
            this.colRefreshQualificationStartDate.OptionsColumn.ReadOnly = true;
            this.colRefreshQualificationStartDate.Visible = true;
            this.colRefreshQualificationStartDate.VisibleIndex = 7;
            this.colRefreshQualificationStartDate.Width = 71;
            // 
            // colRefreshQualificationEndDate
            // 
            this.colRefreshQualificationEndDate.Caption = "End Date";
            this.colRefreshQualificationEndDate.FieldName = "RefreshQualificationEndDate";
            this.colRefreshQualificationEndDate.Name = "colRefreshQualificationEndDate";
            this.colRefreshQualificationEndDate.OptionsColumn.AllowEdit = false;
            this.colRefreshQualificationEndDate.OptionsColumn.ReadOnly = true;
            this.colRefreshQualificationEndDate.Visible = true;
            this.colRefreshQualificationEndDate.VisibleIndex = 8;
            this.colRefreshQualificationEndDate.Width = 82;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // sp_TR_00012_Qualification_Sub_Type_Refreshes_SelectTableAdapter
            // 
            this.sp_TR_00012_Qualification_Sub_Type_Refreshes_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList2.Images.SetKeyName(0, "");
            this.imageList2.Images.SetKeyName(1, "");
            this.imageList2.Images.SetKeyName(2, "");
            this.imageList2.Images.SetKeyName(3, "footer_16.png");
            this.imageList2.Images.SetKeyName(4, "expand_and_select_16.png");
            this.imageList2.Images.SetKeyName(5, "colour_expression_16.png");
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.gridControlRefreshes;
            // 
            // MaintainQualificationRefreshes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControlRefreshes);
            this.Name = "MaintainQualificationRefreshes";
            this.Size = new System.Drawing.Size(469, 241);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRefreshes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00012QualificationSubTypeRefreshesSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControlRefreshes;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colMemberQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colOrderID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colValidity;
        private DevExpress.XtraGrid.Columns.GridColumn colExpiryPeriodDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshIntervalDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshQualificationStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshQualificationEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colRefreshDescription;
        private System.Windows.Forms.BindingSource spTR00012QualificationSubTypeRefreshesSelectBindingSource;
        private DataSet_TR_Core dataSet_TR_Core;
        private DataSet_TR_CoreTableAdapters.sp_TR_00012_Qualification_Sub_Type_Refreshes_SelectTableAdapter sp_TR_00012_Qualification_Sub_Type_Refreshes_SelectTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeDescription;
        private System.Windows.Forms.ImageList imageList2;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
    }
}

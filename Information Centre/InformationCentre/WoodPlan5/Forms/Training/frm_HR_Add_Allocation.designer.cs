namespace WoodPlan5
{
    partial class frm_HR_Add_Allocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HR_Add_Allocation));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule2 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue2 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridFormatRule gridFormatRule3 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue3 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            this.colTimeLimitTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRequirementTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bbiFormSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiFormCancel = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItemFormMode = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemRecordsLoaded = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemChangesPending = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemInformation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dxErrorProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.dataLayoutControl1 = new BaseObjects.ExtendedDataLayoutControl();
            this.allocationGridControl = new DevExpress.XtraGrid.GridControl();
            this.spTR00043AllocationAvailableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_Core = new WoodPlan5.DataSet_TR_Core();
            this.allocationGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colQualificationTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationSubTypeDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colQualificationStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.QualificationSubTypeIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.startDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.spTR00108QualificationAllocationEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_TR_DataEntry = new WoodPlan5.DataSet_TR_DataEntry();
            this.endDateDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.timeLimitSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.TimeLimitTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00008AllocationTimeLimitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RequirementTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00007AllocationRequirementBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrder1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.spTR00044AllocationTypeSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.teamMinimumSpinEdit = new DevExpress.XtraEditors.SpinEdit();
            this.teamHoldingTypeGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spTR00050TeamHoldingTypeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.ItemForQualificationSubTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.availableLayoutControlGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.AllocationLayoutControlItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForRequirementTypeID = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForEndDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStartDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemFortimeLimitdType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTimeLimitValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamHoldingType = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTeamMinimum = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAllocationTypeLink = new DevExpress.XtraLayout.LayoutControlItem();
            this.spTR00006TrainingAllocationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ItemForBreachReporting = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataSet_HR_DataEntry = new WoodPlan5.DataSet_HR_DataEntry();
            this.spHR00232MasterQualificationTypesEditBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_HR_Core = new WoodPlan5.DataSet_HR_Core();
            this.spTR00001QualificationSubTypeStatusBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00235picklisteditpermissionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00235_picklist_edit_permissionsTableAdapter = new WoodPlan5.DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter();
            this.sp_TR_00001_Qualification_SubType_StatusTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00001_Qualification_SubType_StatusTableAdapter();
            this.tableAdapterManager = new WoodPlan5.DataSet_TR_CoreTableAdapters.TableAdapterManager();
            this.sp_TR_00006_Training_AllocationTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00006_Training_AllocationTableAdapter();
            this.sp_TR_00007_Allocation_RequirementTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00007_Allocation_RequirementTableAdapter();
            this.sp_TR_00008_Allocation_Time_LimitTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00008_Allocation_Time_LimitTableAdapter();
            this.sp_HR_00232_Master_Qualification_Types_EditTableAdapter = new WoodPlan5.DataSet_HR_DataEntryTableAdapters.sp_HR_00232_Master_Qualification_Types_EditTableAdapter();
            this.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter();
            this.sp_TR_00108_Qualification_Allocation_EditTableAdapter = new WoodPlan5.DataSet_TR_DataEntryTableAdapters.sp_TR_00108_Qualification_Allocation_EditTableAdapter();
            this.xtraGridBlending1 = new DevExpress.XtraGrid.Blending.XtraGridBlending();
            this.sp_TR_00044_Allocation_Type_SelectTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00044_Allocation_Type_SelectTableAdapter();
            this.sp_TR_00043_Allocation_AvailableTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00043_Allocation_AvailableTableAdapter();
            this.sp_TR_00050_Team_Holding_TypeTableAdapter = new WoodPlan5.DataSet_TR_CoreTableAdapters.sp_TR_00050_Team_Holding_TypeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.allocationGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00043AllocationAvailableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.allocationGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00108QualificationAllocationEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeLimitSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeLimitTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00008AllocationTimeLimitBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequirementTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00007AllocationRequirementBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00044AllocationTypeSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamMinimumSpinEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamHoldingTypeGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00050TeamHoldingTypeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableLayoutControlGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationLayoutControlItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequirementTypeID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFortimeLimitdType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeLimitValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamHoldingType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMinimum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationTypeLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00006TrainingAllocationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBreachReporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00232MasterQualificationTypesEditBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00020QualificationSubTypeAllocationSelectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00001QualificationSubTypeStatusBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Location = new System.Drawing.Point(0, 26);
            this.barDockControlTop.Size = new System.Drawing.Size(648, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 515);
            this.barDockControlBottom.Size = new System.Drawing.Size(648, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 489);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(648, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 489);
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // colTimeLimitTypeID
            // 
            this.colTimeLimitTypeID.FieldName = "TimeLimitTypeID";
            this.colTimeLimitTypeID.Name = "colTimeLimitTypeID";
            this.colTimeLimitTypeID.OptionsColumn.AllowEdit = false;
            this.colTimeLimitTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colRequirementTypeID
            // 
            this.colRequirementTypeID.FieldName = "RequirementTypeID";
            this.colRequirementTypeID.Name = "colRequirementTypeID";
            this.colRequirementTypeID.OptionsColumn.AllowEdit = false;
            this.colRequirementTypeID.OptionsColumn.ReadOnly = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "AllocationTypeID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // barManager2
            // 
            this.barManager2.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar3});
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.HideBarsWhenMerging = false;
            this.barManager2.Images = this.imageCollection1;
            this.barManager2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiFormSave,
            this.bbiFormCancel,
            this.barStaticItemFormMode,
            this.barStaticItemChangesPending,
            this.barStaticItemRecordsLoaded,
            this.barStaticItemInformation});
            this.barManager2.MaxItemId = 15;
            this.barManager2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit3});
            this.barManager2.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "bar1";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.FloatLocation = new System.Drawing.Point(489, 159);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiFormCancel)});
            this.bar1.Text = "Screen Functions";
            // 
            // bbiFormSave
            // 
            this.bbiFormSave.Caption = "Save";
            this.bbiFormSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiFormSave.Glyph")));
            this.bbiFormSave.Id = 0;
            this.bbiFormSave.Name = "bbiFormSave";
            superToolTip1.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "Save Button - Information";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "Click me to <b>save</b> all outstanding changes and close the screen.";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.bbiFormSave.SuperTip = superToolTip1;
            this.bbiFormSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormSave_ItemClick);
            // 
            // bbiFormCancel
            // 
            this.bbiFormCancel.Caption = "Cancel";
            this.bbiFormCancel.Glyph = global::WoodPlan5.Properties.Resources.close_16x16;
            this.bbiFormCancel.Id = 1;
            this.bbiFormCancel.Name = "bbiFormCancel";
            superToolTip2.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "Cancel Button - Information";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "Click me to <b>cancel</b> all outstanding changes and close the screen.";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.bbiFormCancel.SuperTip = superToolTip2;
            this.bbiFormCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiFormCancel_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemFormMode),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemRecordsLoaded),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemChangesPending),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItemInformation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItemFormMode
            // 
            this.barStaticItemFormMode.Caption = "Form Mode: Editing";
            this.barStaticItemFormMode.Id = 6;
            this.barStaticItemFormMode.ImageIndex = 1;
            this.barStaticItemFormMode.ItemAppearance.Normal.Options.UseImage = true;
            this.barStaticItemFormMode.Name = "barStaticItemFormMode";
            this.barStaticItemFormMode.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            superToolTip3.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            toolTipTitleItem3.Appearance.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            toolTipTitleItem3.Text = "Form Mode - Information";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "I hold the current form\'s data editing mode:\r\n\r\n=> Add\r\n=> Edit\r\n=> Block Add\r\n=>" +
    " Block Edit";
            superToolTip3.Items.Add(toolTipTitleItem3);
            superToolTip3.Items.Add(toolTipItem3);
            this.barStaticItemFormMode.SuperTip = superToolTip3;
            this.barStaticItemFormMode.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemRecordsLoaded
            // 
            this.barStaticItemRecordsLoaded.Caption = "Records Loaded: 1";
            this.barStaticItemRecordsLoaded.Id = 13;
            this.barStaticItemRecordsLoaded.Name = "barStaticItemRecordsLoaded";
            this.barStaticItemRecordsLoaded.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemChangesPending
            // 
            this.barStaticItemChangesPending.Caption = "Changes Pending";
            this.barStaticItemChangesPending.Id = 12;
            this.barStaticItemChangesPending.Name = "barStaticItemChangesPending";
            this.barStaticItemChangesPending.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barStaticItemInformation
            // 
            this.barStaticItemInformation.Caption = "Information:";
            this.barStaticItemInformation.Id = 14;
            this.barStaticItemInformation.ImageIndex = 2;
            this.barStaticItemInformation.Name = "barStaticItemInformation";
            this.barStaticItemInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItemInformation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(648, 26);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 515);
            this.barDockControl2.Size = new System.Drawing.Size(648, 30);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 26);
            this.barDockControl3.Size = new System.Drawing.Size(0, 489);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(648, 26);
            this.barDockControl4.Size = new System.Drawing.Size(0, 489);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "add_16.png");
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16.png");
            this.imageCollection1.Images.SetKeyName(2, "Info_16x16.png");
            this.imageCollection1.Images.SetKeyName(3, "attention_16.png");
            this.imageCollection1.Images.SetKeyName(4, "Preview_16x16.png");
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // dxErrorProvider1
            // 
            this.dxErrorProvider1.ContainerControl = this;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.allocationGridControl);
            this.dataLayoutControl1.Controls.Add(this.QualificationSubTypeIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.startDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.endDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.timeLimitSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.TimeLimitTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.RequirementTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.buttonEdit1);
            this.dataLayoutControl1.Controls.Add(this.teamMinimumSpinEdit);
            this.dataLayoutControl1.Controls.Add(this.teamHoldingTypeGridLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.checkEdit1);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForQualificationSubTypeID});
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 26);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(288, 280, 688, 396);
            this.dataLayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(648, 489);
            this.dataLayoutControl1.TabIndex = 8;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // allocationGridControl
            // 
            this.allocationGridControl.DataSource = this.spTR00043AllocationAvailableBindingSource;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.allocationGridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.allocationGridControl.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 0, true, true, "Add New Record", "add"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 1, true, true, "Edit Selected Record(s)", "edit"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 2, true, true, "Delete Selected Record(s)", "delete"),
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 4, true, true, "View Selected Record(s)", "view")});
            this.allocationGridControl.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControlVettingSubTypes_EmbeddedNavigator_ButtonClick);
            this.allocationGridControl.Location = new System.Drawing.Point(24, 165);
            this.allocationGridControl.MainView = this.allocationGridView;
            this.allocationGridControl.Name = "allocationGridControl";
            this.allocationGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1});
            this.allocationGridControl.Size = new System.Drawing.Size(600, 300);
            this.allocationGridControl.TabIndex = 39;
            this.allocationGridControl.UseEmbeddedNavigator = true;
            this.allocationGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.allocationGridView});
            // 
            // spTR00043AllocationAvailableBindingSource
            // 
            this.spTR00043AllocationAvailableBindingSource.DataMember = "sp_TR_00043_Allocation_Available";
            this.spTR00043AllocationAvailableBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_TR_Core
            // 
            this.dataSet_TR_Core.DataSetName = "DataSet_TR_Core";
            this.dataSet_TR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // allocationGridView
            // 
            this.allocationGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colQualificationTypeID,
            this.colQualificationTypeDescription,
            this.colQualificationSubTypeID,
            this.colQualificationSubTypeDescription,
            this.colQualificationStatus});
            this.allocationGridView.GridControl = this.allocationGridControl;
            this.allocationGridView.Name = "allocationGridView";
            this.allocationGridView.OptionsFilter.UseNewCustomFilterDialog = true;
            this.allocationGridView.OptionsLayout.Columns.StoreAllOptions = true;
            this.allocationGridView.OptionsLayout.StoreAppearance = true;
            this.allocationGridView.OptionsLayout.StoreFormatRules = true;
            this.allocationGridView.OptionsSelection.MultiSelect = true;
            this.allocationGridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.allocationGridView.OptionsSelection.ShowCheckBoxSelectorInColumnHeader = DevExpress.Utils.DefaultBoolean.False;
            this.allocationGridView.OptionsView.ShowGroupPanel = false;
            this.allocationGridView.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.allocationGridView_PopupMenuShowing);
            this.allocationGridView.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.allocationGridView_SelectionChanged);
            // 
            // colQualificationTypeID
            // 
            this.colQualificationTypeID.FieldName = "QualificationTypeID";
            this.colQualificationTypeID.Name = "colQualificationTypeID";
            this.colQualificationTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationTypeDescription
            // 
            this.colQualificationTypeDescription.Caption = "Qualification Type";
            this.colQualificationTypeDescription.FieldName = "QualificationTypeDescription";
            this.colQualificationTypeDescription.Name = "colQualificationTypeDescription";
            this.colQualificationTypeDescription.OptionsColumn.AllowEdit = false;
            this.colQualificationTypeDescription.OptionsColumn.AllowFocus = false;
            this.colQualificationTypeDescription.OptionsColumn.ReadOnly = true;
            this.colQualificationTypeDescription.Visible = true;
            this.colQualificationTypeDescription.VisibleIndex = 1;
            this.colQualificationTypeDescription.Width = 145;
            // 
            // colQualificationSubTypeID
            // 
            this.colQualificationSubTypeID.FieldName = "QualificationSubTypeID";
            this.colQualificationSubTypeID.Name = "colQualificationSubTypeID";
            this.colQualificationSubTypeID.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeID.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeID.OptionsColumn.ReadOnly = true;
            // 
            // colQualificationSubTypeDescription
            // 
            this.colQualificationSubTypeDescription.Caption = "Qualification Sub Type";
            this.colQualificationSubTypeDescription.FieldName = "QualificationSubTypeDescription";
            this.colQualificationSubTypeDescription.Name = "colQualificationSubTypeDescription";
            this.colQualificationSubTypeDescription.OptionsColumn.AllowEdit = false;
            this.colQualificationSubTypeDescription.OptionsColumn.AllowFocus = false;
            this.colQualificationSubTypeDescription.OptionsColumn.ReadOnly = true;
            this.colQualificationSubTypeDescription.Visible = true;
            this.colQualificationSubTypeDescription.VisibleIndex = 2;
            this.colQualificationSubTypeDescription.Width = 562;
            // 
            // colQualificationStatus
            // 
            this.colQualificationStatus.Caption = "Status";
            this.colQualificationStatus.FieldName = "QualificationStatus";
            this.colQualificationStatus.Name = "colQualificationStatus";
            this.colQualificationStatus.OptionsColumn.AllowEdit = false;
            this.colQualificationStatus.OptionsColumn.AllowFocus = false;
            this.colQualificationStatus.OptionsColumn.ReadOnly = true;
            this.colQualificationStatus.Visible = true;
            this.colQualificationStatus.VisibleIndex = 3;
            this.colQualificationStatus.Width = 106;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // QualificationSubTypeIDTextEdit
            // 
            this.QualificationSubTypeIDTextEdit.Location = new System.Drawing.Point(141, 107);
            this.QualificationSubTypeIDTextEdit.MenuManager = this.barManager1;
            this.QualificationSubTypeIDTextEdit.Name = "QualificationSubTypeIDTextEdit";
            this.QualificationSubTypeIDTextEdit.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.QualificationSubTypeIDTextEdit, true);
            this.QualificationSubTypeIDTextEdit.Size = new System.Drawing.Size(475, 20);
            this.scSpellChecker.SetSpellCheckerOptions(this.QualificationSubTypeIDTextEdit, optionsSpelling1);
            this.QualificationSubTypeIDTextEdit.StyleController = this.dataLayoutControl1;
            this.QualificationSubTypeIDTextEdit.TabIndex = 4;
            // 
            // startDateDateEdit
            // 
            this.startDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "StartDate", true));
            this.startDateDateEdit.EditValue = null;
            this.startDateDateEdit.Location = new System.Drawing.Point(154, 36);
            this.startDateDateEdit.Name = "startDateDateEdit";
            this.startDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.startDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.startDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.startDateDateEdit.Size = new System.Drawing.Size(125, 20);
            this.startDateDateEdit.StyleController = this.dataLayoutControl1;
            this.startDateDateEdit.TabIndex = 21;
            this.startDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.startDateDateEdit_Validating);
            // 
            // spTR00108QualificationAllocationEditBindingSource
            // 
            this.spTR00108QualificationAllocationEditBindingSource.DataMember = "sp_TR_00108_Qualification_Allocation_Edit";
            this.spTR00108QualificationAllocationEditBindingSource.DataSource = this.dataSet_TR_DataEntry;
            // 
            // dataSet_TR_DataEntry
            // 
            this.dataSet_TR_DataEntry.DataSetName = "DataSet_TR_DataEntry";
            this.dataSet_TR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // endDateDateEdit
            // 
            this.endDateDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "EndDate", true));
            this.endDateDateEdit.EditValue = null;
            this.endDateDateEdit.Location = new System.Drawing.Point(425, 36);
            this.endDateDateEdit.Name = "endDateDateEdit";
            this.endDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.endDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.endDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.endDateDateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.endDateDateEdit.Properties.MaxValue = new System.DateTime(2900, 12, 31, 0, 0, 0, 0);
            this.endDateDateEdit.Size = new System.Drawing.Size(125, 20);
            this.endDateDateEdit.StyleController = this.dataLayoutControl1;
            this.endDateDateEdit.TabIndex = 22;
            this.endDateDateEdit.Validating += new System.ComponentModel.CancelEventHandler(this.endDateDateEdit_Validating);
            // 
            // timeLimitSpinEdit
            // 
            this.timeLimitSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "TimeLimitValue", true));
            this.timeLimitSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.timeLimitSpinEdit.Location = new System.Drawing.Point(425, 84);
            this.timeLimitSpinEdit.Name = "timeLimitSpinEdit";
            this.timeLimitSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeLimitSpinEdit.Properties.Mask.EditMask = "N0";
            this.timeLimitSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.timeLimitSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.timeLimitSpinEdit.Size = new System.Drawing.Size(125, 20);
            this.timeLimitSpinEdit.StyleController = this.dataLayoutControl1;
            this.timeLimitSpinEdit.TabIndex = 20;
            this.timeLimitSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.timeLimitSpinEdit_Validating);
            // 
            // TimeLimitTypeGridLookUpEdit
            // 
            this.TimeLimitTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "TimeLimitTypeID", true));
            this.TimeLimitTypeGridLookUpEdit.Location = new System.Drawing.Point(154, 84);
            this.TimeLimitTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.TimeLimitTypeGridLookUpEdit.Name = "TimeLimitTypeGridLookUpEdit";
            this.TimeLimitTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TimeLimitTypeGridLookUpEdit.Properties.DataSource = this.spTR00008AllocationTimeLimitBindingSource;
            this.TimeLimitTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.TimeLimitTypeGridLookUpEdit.Properties.NullText = "";
            this.TimeLimitTypeGridLookUpEdit.Properties.ValueMember = "TimeLimitTypeID";
            this.TimeLimitTypeGridLookUpEdit.Properties.View = this.gridView2;
            this.TimeLimitTypeGridLookUpEdit.Size = new System.Drawing.Size(125, 20);
            this.TimeLimitTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.TimeLimitTypeGridLookUpEdit.TabIndex = 38;
            this.TimeLimitTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.TimeLimitTypeGridLookUpEdit_Validating);
            // 
            // spTR00008AllocationTimeLimitBindingSource
            // 
            this.spTR00008AllocationTimeLimitBindingSource.DataMember = "sp_TR_00008_Allocation_Time_Limit";
            this.spTR00008AllocationTimeLimitBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTimeLimitTypeID,
            this.colDescription2,
            this.colOrder2});
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.colTimeLimitTypeID;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.gridView2.FormatRules.Add(gridFormatRule1);
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView2.OptionsView.ShowIndicator = false;
            // 
            // colDescription2
            // 
            this.colDescription2.Caption = "Time limit Type";
            this.colDescription2.FieldName = "Description";
            this.colDescription2.Name = "colDescription2";
            this.colDescription2.OptionsColumn.AllowEdit = false;
            this.colDescription2.OptionsColumn.ReadOnly = true;
            this.colDescription2.Visible = true;
            this.colDescription2.VisibleIndex = 0;
            this.colDescription2.Width = 221;
            // 
            // colOrder2
            // 
            this.colOrder2.FieldName = "Order";
            this.colOrder2.Name = "colOrder2";
            this.colOrder2.OptionsColumn.AllowEdit = false;
            this.colOrder2.OptionsColumn.ReadOnly = true;
            // 
            // RequirementTypeGridLookUpEdit
            // 
            this.RequirementTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "RequirementTypeID", true));
            this.RequirementTypeGridLookUpEdit.Location = new System.Drawing.Point(154, 60);
            this.RequirementTypeGridLookUpEdit.MenuManager = this.barManager1;
            this.RequirementTypeGridLookUpEdit.Name = "RequirementTypeGridLookUpEdit";
            this.RequirementTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RequirementTypeGridLookUpEdit.Properties.DataSource = this.spTR00007AllocationRequirementBindingSource;
            this.RequirementTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.RequirementTypeGridLookUpEdit.Properties.NullText = "";
            this.RequirementTypeGridLookUpEdit.Properties.ValueMember = "RequirementTypeID";
            this.RequirementTypeGridLookUpEdit.Properties.View = this.gridView4;
            this.RequirementTypeGridLookUpEdit.Size = new System.Drawing.Size(151, 20);
            this.RequirementTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.RequirementTypeGridLookUpEdit.TabIndex = 38;
            this.RequirementTypeGridLookUpEdit.EditValueChanged += new System.EventHandler(this.RequirementTypeGridLookUpEdit_EditValueChanged);
            this.RequirementTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.RequirementTypeGridLookUpEdit_Validating);
            // 
            // spTR00007AllocationRequirementBindingSource
            // 
            this.spTR00007AllocationRequirementBindingSource.DataMember = "sp_TR_00007_Allocation_Requirement";
            this.spTR00007AllocationRequirementBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colRequirementTypeID,
            this.colDescription1,
            this.colOrder1});
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule2.ApplyToRow = true;
            gridFormatRule2.Column = this.colRequirementTypeID;
            gridFormatRule2.Name = "Format0";
            formatConditionRuleValue2.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue2.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue2.Value1 = 0;
            formatConditionRuleValue2.Value2 = ((short)(0));
            gridFormatRule2.Rule = formatConditionRuleValue2;
            this.gridView4.FormatRules.Add(gridFormatRule2);
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView4.OptionsView.ShowIndicator = false;
            // 
            // colDescription1
            // 
            this.colDescription1.Caption = "Requirement Type";
            this.colDescription1.FieldName = "Description";
            this.colDescription1.Name = "colDescription1";
            this.colDescription1.OptionsColumn.AllowEdit = false;
            this.colDescription1.OptionsColumn.ReadOnly = true;
            this.colDescription1.Visible = true;
            this.colDescription1.VisibleIndex = 0;
            this.colDescription1.Width = 247;
            // 
            // colOrder1
            // 
            this.colOrder1.FieldName = "Order";
            this.colOrder1.Name = "colOrder1";
            this.colOrder1.OptionsColumn.AllowEdit = false;
            this.colOrder1.OptionsColumn.ReadOnly = true;
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spTR00044AllocationTypeSelectBindingSource, "Description", true));
            this.buttonEdit1.Location = new System.Drawing.Point(154, 12);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Qualification Type \\ Sub-Type screen", "choose", null, true)});
            this.buttonEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.buttonEdit1.Properties.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.buttonEdit1_Properties_ButtonClick);
            this.buttonEdit1.Size = new System.Drawing.Size(220, 20);
            this.buttonEdit1.StyleController = this.dataLayoutControl1;
            this.buttonEdit1.TabIndex = 31;
            this.buttonEdit1.EditValueChanged += new System.EventHandler(this.buttonEdit1_EditValueChanged);
            this.buttonEdit1.Validating += new System.ComponentModel.CancelEventHandler(this.buttonEdit1_Validating);
            // 
            // spTR00044AllocationTypeSelectBindingSource
            // 
            this.spTR00044AllocationTypeSelectBindingSource.DataMember = "sp_TR_00044_Allocation_Type_Select";
            this.spTR00044AllocationTypeSelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // teamMinimumSpinEdit
            // 
            this.teamMinimumSpinEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "TeamHoldingMinimum", true));
            this.teamMinimumSpinEdit.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.teamMinimumSpinEdit.Location = new System.Drawing.Point(425, 108);
            this.teamMinimumSpinEdit.Name = "teamMinimumSpinEdit";
            this.teamMinimumSpinEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teamMinimumSpinEdit.Properties.Mask.EditMask = "N0";
            this.teamMinimumSpinEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.teamMinimumSpinEdit.Properties.MaxValue = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.teamMinimumSpinEdit.Size = new System.Drawing.Size(125, 20);
            this.teamMinimumSpinEdit.StyleController = this.dataLayoutControl1;
            this.teamMinimumSpinEdit.TabIndex = 20;
            this.teamMinimumSpinEdit.Validating += new System.ComponentModel.CancelEventHandler(this.teamMinimumSpinEdit_Validating);
            // 
            // teamHoldingTypeGridLookUpEdit
            // 
            this.teamHoldingTypeGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "TeamHoldingTypeID", true));
            this.teamHoldingTypeGridLookUpEdit.Location = new System.Drawing.Point(154, 108);
            this.teamHoldingTypeGridLookUpEdit.Name = "teamHoldingTypeGridLookUpEdit";
            this.teamHoldingTypeGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.teamHoldingTypeGridLookUpEdit.Properties.DataSource = this.spTR00050TeamHoldingTypeBindingSource;
            this.teamHoldingTypeGridLookUpEdit.Properties.DisplayMember = "Description";
            this.teamHoldingTypeGridLookUpEdit.Properties.NullText = "";
            this.teamHoldingTypeGridLookUpEdit.Properties.ValueMember = "TeamHoldingTypeID";
            this.teamHoldingTypeGridLookUpEdit.Properties.View = this.gridView1;
            this.teamHoldingTypeGridLookUpEdit.Size = new System.Drawing.Size(125, 20);
            this.teamHoldingTypeGridLookUpEdit.StyleController = this.dataLayoutControl1;
            this.teamHoldingTypeGridLookUpEdit.TabIndex = 38;
            this.teamHoldingTypeGridLookUpEdit.Validating += new System.ComponentModel.CancelEventHandler(this.teamHoldingTypeGridLookUpEdit_Validating);
            // 
            // spTR00050TeamHoldingTypeBindingSource
            // 
            this.spTR00050TeamHoldingTypeBindingSource.DataMember = "sp_TR_00050_Team_Holding_Type";
            this.spTR00050TeamHoldingTypeBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule3.ApplyToRow = true;
            gridFormatRule3.Column = this.gridColumn1;
            gridFormatRule3.Name = "Format0";
            formatConditionRuleValue3.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue3.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue3.Value1 = 0;
            gridFormatRule3.Rule = formatConditionRuleValue3;
            this.gridView1.FormatRules.Add(gridFormatRule3);
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView1.OptionsLayout.StoreAppearance = true;
            this.gridView1.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Allocation Type";
            this.gridColumn2.FieldName = "Description";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowEdit = false;
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 281;
            // 
            // gridColumn3
            // 
            this.gridColumn3.FieldName = "Order";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // checkEdit1
            // 
            this.checkEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.spTR00108QualificationAllocationEditBindingSource, "BreachReportableActive", true));
            this.checkEdit1.Location = new System.Drawing.Point(520, 12);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "(Tick if Yes)";
            this.checkEdit1.Properties.ValueChecked = 1;
            this.checkEdit1.Properties.ValueUnchecked = 0;
            this.checkEdit1.Size = new System.Drawing.Size(116, 19);
            this.checkEdit1.StyleController = this.dataLayoutControl1;
            this.checkEdit1.TabIndex = 29;
            // 
            // ItemForQualificationSubTypeID
            // 
            this.ItemForQualificationSubTypeID.Control = this.QualificationSubTypeIDTextEdit;
            this.ItemForQualificationSubTypeID.CustomizationFormText = "Qualification Sub-Type ID:";
            this.ItemForQualificationSubTypeID.Location = new System.Drawing.Point(0, 72);
            this.ItemForQualificationSubTypeID.Name = "ItemForQualificationSubTypeID";
            this.ItemForQualificationSubTypeID.Size = new System.Drawing.Size(608, 24);
            this.ItemForQualificationSubTypeID.Text = "Qualification Sub-Type ID:";
            this.ItemForQualificationSubTypeID.TextSize = new System.Drawing.Size(96, 13);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(648, 489);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup1";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1,
            this.layoutControlGroup5,
            this.ItemForAllocationTypeLink,
            this.ItemForBreachReporting});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "autoGeneratedGroup1";
            this.layoutControlGroup3.Size = new System.Drawing.Size(628, 469);
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 120);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.availableLayoutControlGroup;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(628, 349);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.availableLayoutControlGroup});
            // 
            // availableLayoutControlGroup
            // 
            this.availableLayoutControlGroup.ExpandButtonVisible = true;
            this.availableLayoutControlGroup.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.availableLayoutControlGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.AllocationLayoutControlItem});
            this.availableLayoutControlGroup.Location = new System.Drawing.Point(0, 0);
            this.availableLayoutControlGroup.Name = "availableLayoutControlGroup";
            this.availableLayoutControlGroup.Size = new System.Drawing.Size(604, 304);
            this.availableLayoutControlGroup.Text = "Qualifications";
            // 
            // AllocationLayoutControlItem
            // 
            this.AllocationLayoutControlItem.Control = this.allocationGridControl;
            this.AllocationLayoutControlItem.Location = new System.Drawing.Point(0, 0);
            this.AllocationLayoutControlItem.Name = "AllocationLayoutControlItem";
            this.AllocationLayoutControlItem.Size = new System.Drawing.Size(604, 304);
            this.AllocationLayoutControlItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.AllocationLayoutControlItem.TextSize = new System.Drawing.Size(0, 0);
            this.AllocationLayoutControlItem.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.HeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForRequirementTypeID,
            this.emptySpaceItem3,
            this.emptySpaceItem5,
            this.ItemForEndDate,
            this.ItemForStartDate,
            this.ItemFortimeLimitdType,
            this.ItemForTimeLimitValue,
            this.ItemForTeamHoldingType,
            this.ItemForTeamMinimum});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 24);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(628, 96);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // ItemForRequirementTypeID
            // 
            this.ItemForRequirementTypeID.Control = this.RequirementTypeGridLookUpEdit;
            this.ItemForRequirementTypeID.CustomizationFormText = "Requirement Type:";
            this.ItemForRequirementTypeID.Location = new System.Drawing.Point(0, 24);
            this.ItemForRequirementTypeID.Name = "ItemForRequirementTypeID";
            this.ItemForRequirementTypeID.Size = new System.Drawing.Size(297, 24);
            this.ItemForRequirementTypeID.Text = "Requirement Type:";
            this.ItemForRequirementTypeID.TextSize = new System.Drawing.Size(139, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(297, 24);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(245, 24);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem5.Location = new System.Drawing.Point(542, 0);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(86, 96);
            this.emptySpaceItem5.Text = "emptySpaceItem3";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForEndDate
            // 
            this.ItemForEndDate.Control = this.endDateDateEdit;
            this.ItemForEndDate.CustomizationFormText = "Allocation End Date:";
            this.ItemForEndDate.Location = new System.Drawing.Point(271, 0);
            this.ItemForEndDate.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForEndDate.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForEndDate.Name = "ItemForEndDate";
            this.ItemForEndDate.Size = new System.Drawing.Size(271, 24);
            this.ItemForEndDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForEndDate.Text = "End Date:";
            this.ItemForEndDate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForStartDate
            // 
            this.ItemForStartDate.Control = this.startDateDateEdit;
            this.ItemForStartDate.CustomizationFormText = "Allocation Start Date:";
            this.ItemForStartDate.Location = new System.Drawing.Point(0, 0);
            this.ItemForStartDate.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForStartDate.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForStartDate.Name = "ItemForStartDate";
            this.ItemForStartDate.Size = new System.Drawing.Size(271, 24);
            this.ItemForStartDate.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForStartDate.Text = "Start Date:";
            this.ItemForStartDate.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemFortimeLimitdType
            // 
            this.ItemFortimeLimitdType.Control = this.TimeLimitTypeGridLookUpEdit;
            this.ItemFortimeLimitdType.CustomizationFormText = "Time Limit Type:";
            this.ItemFortimeLimitdType.Location = new System.Drawing.Point(0, 48);
            this.ItemFortimeLimitdType.Name = "ItemFortimeLimitdType";
            this.ItemFortimeLimitdType.Size = new System.Drawing.Size(271, 24);
            this.ItemFortimeLimitdType.Text = "Time Limit Type:";
            this.ItemFortimeLimitdType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForTimeLimitValue
            // 
            this.ItemForTimeLimitValue.Control = this.timeLimitSpinEdit;
            this.ItemForTimeLimitValue.CustomizationFormText = "Time Limit Value:";
            this.ItemForTimeLimitValue.Location = new System.Drawing.Point(271, 48);
            this.ItemForTimeLimitValue.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForTimeLimitValue.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForTimeLimitValue.Name = "ItemForTimeLimitValue";
            this.ItemForTimeLimitValue.Size = new System.Drawing.Size(271, 24);
            this.ItemForTimeLimitValue.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTimeLimitValue.Text = "Time Limit Value:";
            this.ItemForTimeLimitValue.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForTeamHoldingType
            // 
            this.ItemForTeamHoldingType.Control = this.teamHoldingTypeGridLookUpEdit;
            this.ItemForTeamHoldingType.CustomizationFormText = "Team Holding Type:";
            this.ItemForTeamHoldingType.Location = new System.Drawing.Point(0, 72);
            this.ItemForTeamHoldingType.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForTeamHoldingType.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForTeamHoldingType.Name = "ItemForTeamHoldingType";
            this.ItemForTeamHoldingType.Size = new System.Drawing.Size(271, 24);
            this.ItemForTeamHoldingType.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTeamHoldingType.Text = "Team Holding Type:";
            this.ItemForTeamHoldingType.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForTeamMinimum
            // 
            this.ItemForTeamMinimum.Control = this.teamMinimumSpinEdit;
            this.ItemForTeamMinimum.CustomizationFormText = "Minimum Required:";
            this.ItemForTeamMinimum.Location = new System.Drawing.Point(271, 72);
            this.ItemForTeamMinimum.MaxSize = new System.Drawing.Size(271, 24);
            this.ItemForTeamMinimum.MinSize = new System.Drawing.Size(271, 24);
            this.ItemForTeamMinimum.Name = "ItemForTeamMinimum";
            this.ItemForTeamMinimum.Size = new System.Drawing.Size(271, 24);
            this.ItemForTeamMinimum.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.ItemForTeamMinimum.Text = "Minimum Required:";
            this.ItemForTeamMinimum.TextSize = new System.Drawing.Size(139, 13);
            // 
            // ItemForAllocationTypeLink
            // 
            this.ItemForAllocationTypeLink.Control = this.buttonEdit1;
            this.ItemForAllocationTypeLink.CustomizationFormText = "Allocation Type:";
            this.ItemForAllocationTypeLink.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.spTR00006TrainingAllocationBindingSource, "Description", true));
            this.ItemForAllocationTypeLink.Location = new System.Drawing.Point(0, 0);
            this.ItemForAllocationTypeLink.Name = "ItemForAllocationTypeLink";
            this.ItemForAllocationTypeLink.Size = new System.Drawing.Size(366, 24);
            this.ItemForAllocationTypeLink.Text = "Allocation Type:";
            this.ItemForAllocationTypeLink.TextSize = new System.Drawing.Size(139, 13);
            // 
            // spTR00006TrainingAllocationBindingSource
            // 
            this.spTR00006TrainingAllocationBindingSource.DataMember = "sp_TR_00006_Training_Allocation";
            this.spTR00006TrainingAllocationBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // ItemForBreachReporting
            // 
            this.ItemForBreachReporting.Control = this.checkEdit1;
            this.ItemForBreachReporting.CustomizationFormText = "Include in Breach Reporting";
            this.ItemForBreachReporting.Location = new System.Drawing.Point(366, 0);
            this.ItemForBreachReporting.Name = "ItemForBreachReporting";
            this.ItemForBreachReporting.Size = new System.Drawing.Size(262, 24);
            this.ItemForBreachReporting.Text = "Include in Breach Reporting :";
            this.ItemForBreachReporting.TextSize = new System.Drawing.Size(139, 13);
            // 
            // dataSet_HR_DataEntry
            // 
            this.dataSet_HR_DataEntry.DataSetName = "DataSet_HR_DataEntry";
            this.dataSet_HR_DataEntry.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spHR00232MasterQualificationTypesEditBindingSource
            // 
            this.spHR00232MasterQualificationTypesEditBindingSource.DataMember = "sp_HR_00232_Master_Qualification_Types_Edit";
            this.spHR00232MasterQualificationTypesEditBindingSource.DataSource = this.dataSet_HR_DataEntry;
            // 
            // spTR00020QualificationSubTypeAllocationSelectBindingSource
            // 
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource.DataMember = "sp_TR_00020_Qualification_Sub_Type_Allocation_Select";
            this.spTR00020QualificationSubTypeAllocationSelectBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_HR_Core
            // 
            this.dataSet_HR_Core.DataSetName = "DataSet_HR_Core";
            this.dataSet_HR_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spTR00001QualificationSubTypeStatusBindingSource
            // 
            this.spTR00001QualificationSubTypeStatusBindingSource.DataMember = "sp_TR_00001_Qualification_SubType_Status";
            this.spTR00001QualificationSubTypeStatusBindingSource.DataSource = this.dataSet_TR_Core;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00235_picklist_edit_permissionsTableAdapter
            // 
            this.sp00235_picklist_edit_permissionsTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00001_Qualification_SubType_StatusTableAdapter
            // 
            this.sp_TR_00001_Qualification_SubType_StatusTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = WoodPlan5.DataSet_TR_CoreTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_TR_00006_Training_AllocationTableAdapter
            // 
            this.sp_TR_00006_Training_AllocationTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00007_Allocation_RequirementTableAdapter
            // 
            this.sp_TR_00007_Allocation_RequirementTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00008_Allocation_Time_LimitTableAdapter
            // 
            this.sp_TR_00008_Allocation_Time_LimitTableAdapter.ClearBeforeFill = true;
            // 
            // sp_HR_00232_Master_Qualification_Types_EditTableAdapter
            // 
            this.sp_HR_00232_Master_Qualification_Types_EditTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter
            // 
            this.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00108_Qualification_Allocation_EditTableAdapter
            // 
            this.sp_TR_00108_Qualification_Allocation_EditTableAdapter.ClearBeforeFill = true;
            // 
            // xtraGridBlending1
            // 
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupPanel", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Empty", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("HeaderPanel", 250);
            this.xtraGridBlending1.AlphaStyles.AddReplace("EvenRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("FocusedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("RowSeparator", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("OddRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupFooter", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("SelectedRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("GroupRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("TopNewRow", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Row", 125);
            this.xtraGridBlending1.AlphaStyles.AddReplace("Preview", 125);
            this.xtraGridBlending1.GridControl = this.allocationGridControl;
            // 
            // sp_TR_00044_Allocation_Type_SelectTableAdapter
            // 
            this.sp_TR_00044_Allocation_Type_SelectTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00043_Allocation_AvailableTableAdapter
            // 
            this.sp_TR_00043_Allocation_AvailableTableAdapter.ClearBeforeFill = true;
            // 
            // sp_TR_00050_Team_Holding_TypeTableAdapter
            // 
            this.sp_TR_00050_Team_Holding_TypeTableAdapter.ClearBeforeFill = true;
            // 
            // frm_HR_Add_Allocation
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(648, 545);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_HR_Add_Allocation";
            this.Text = "Add Allocation";
            this.Activated += new System.EventHandler(this.frm_HR_Add_Allocation_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_HR_Add_Allocation_FormClosing);
            this.Load += new System.EventHandler(this.frm_HR_Add_Allocation_Load);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.dataLayoutControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.allocationGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00043AllocationAvailableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.allocationGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QualificationSubTypeIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00108QualificationAllocationEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_TR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeLimitSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeLimitTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00008AllocationTimeLimitBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RequirementTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00007AllocationRequirementBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00044AllocationTypeSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamMinimumSpinEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamHoldingTypeGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00050TeamHoldingTypeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForQualificationSubTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.availableLayoutControlGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AllocationLayoutControlItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForRequirementTypeID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemFortimeLimitdType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTimeLimitValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamHoldingType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTeamMinimum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAllocationTypeLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00006TrainingAllocationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForBreachReporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_DataEntry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spHR00232MasterQualificationTypesEditBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00020QualificationSubTypeAllocationSelectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_HR_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spTR00001QualificationSubTypeStatusBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00235picklisteditpermissionsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager2;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem bbiFormSave;
        private DevExpress.XtraBars.BarButtonItem bbiFormCancel;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItemFormMode;
        private DevExpress.XtraBars.BarStaticItem barStaticItemRecordsLoaded;
        private DevExpress.XtraBars.BarStaticItem barStaticItemChangesPending;
        private DevExpress.XtraBars.BarStaticItem barStaticItemInformation;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrorProvider1;
        private DevExpress.XtraEditors.TextEdit QualificationSubTypeIDTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForQualificationSubTypeID;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DataSet_AT dataSet_AT;
        private BaseObjects.ExtendedDataLayoutControl dataLayoutControl1;
        private System.Windows.Forms.BindingSource sp00235picklisteditpermissionsBindingSource;
        private DataSet_AT_DataEntryTableAdapters.sp00235_picklist_edit_permissionsTableAdapter sp00235_picklist_edit_permissionsTableAdapter;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_HR_DataEntry dataSet_HR_DataEntry;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DataSet_HR_Core dataSet_HR_Core;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.DateEdit startDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStartDate;
        private DevExpress.XtraEditors.DateEdit endDateDateEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndDate;
        private DevExpress.XtraEditors.SpinEdit timeLimitSpinEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTimeLimitValue;
        private DevExpress.XtraEditors.GridLookUpEdit TimeLimitTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlItem ItemFortimeLimitdType;
        private DataSet_TR_Core dataSet_TR_Core;
        private System.Windows.Forms.BindingSource spTR00001QualificationSubTypeStatusBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00001_Qualification_SubType_StatusTableAdapter sp_TR_00001_Qualification_SubType_StatusTableAdapter;
        private DataSet_TR_CoreTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraLayout.LayoutControlGroup availableLayoutControlGroup;
        private DevExpress.XtraEditors.GridLookUpEdit RequirementTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem ItemForRequirementTypeID;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private System.Windows.Forms.BindingSource spTR00006TrainingAllocationBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00006_Training_AllocationTableAdapter sp_TR_00006_Training_AllocationTableAdapter;
        private System.Windows.Forms.BindingSource spTR00007AllocationRequirementBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00007_Allocation_RequirementTableAdapter sp_TR_00007_Allocation_RequirementTableAdapter;
        private System.Windows.Forms.BindingSource spTR00008AllocationTimeLimitBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00008_Allocation_Time_LimitTableAdapter sp_TR_00008_Allocation_Time_LimitTableAdapter;
        private DevExpress.XtraGrid.GridControl allocationGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView allocationGridView;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraLayout.LayoutControlItem AllocationLayoutControlItem;
        private System.Windows.Forms.BindingSource spHR00232MasterQualificationTypesEditBindingSource;
        private DataSet_HR_DataEntryTableAdapters.sp_HR_00232_Master_Qualification_Types_EditTableAdapter sp_HR_00232_Master_Qualification_Types_EditTableAdapter;
        private System.Windows.Forms.BindingSource spTR00020QualificationSubTypeAllocationSelectBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter sp_TR_00020_Qualification_Sub_Type_Allocation_SelectTableAdapter;
        private DataSet_TR_DataEntry dataSet_TR_DataEntry;
        private System.Windows.Forms.BindingSource spTR00108QualificationAllocationEditBindingSource;
        private DataSet_TR_DataEntryTableAdapters.sp_TR_00108_Qualification_Allocation_EditTableAdapter sp_TR_00108_Qualification_Allocation_EditTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeLimitTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription2;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder2;
        private DevExpress.XtraGrid.Columns.GridColumn colRequirementTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colOrder1;
        private DevExpress.XtraGrid.Blending.XtraGridBlending xtraGridBlending1;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationTypeDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationSubTypeDescription;
        private System.Windows.Forms.BindingSource spTR00044AllocationTypeSelectBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00044_Allocation_Type_SelectTableAdapter sp_TR_00044_Allocation_Type_SelectTableAdapter;
        private System.Windows.Forms.BindingSource spTR00043AllocationAvailableBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00043_Allocation_AvailableTableAdapter sp_TR_00043_Allocation_AvailableTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colQualificationStatus;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAllocationTypeLink;
        private DevExpress.XtraEditors.SpinEdit teamMinimumSpinEdit;
        private DevExpress.XtraEditors.GridLookUpEdit teamHoldingTypeGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamHoldingType;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTeamMinimum;
        private System.Windows.Forms.BindingSource spTR00050TeamHoldingTypeBindingSource;
        private DataSet_TR_CoreTableAdapters.sp_TR_00050_Team_Holding_TypeTableAdapter sp_TR_00050_Team_Holding_TypeTableAdapter;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForBreachReporting;
    }
}

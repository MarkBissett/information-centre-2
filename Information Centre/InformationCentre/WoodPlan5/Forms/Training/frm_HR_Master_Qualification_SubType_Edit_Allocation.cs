using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.Skins;
using WoodPlan5.Classes.TR;
using WoodPlan5.Properties;
using BaseObjects;
using WoodPlan5.Classes.HR;

namespace WoodPlan5
{
    public partial class frm_HR_Master_Qualification_SubType_Edit_Allocation : BaseObjects.frmBase
    {
        #region Instance Variables
        
        private string strConnectionString = "";
        Settings set = Settings.Default;
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        private bool ibool_FormEditingCancelled = false;
        //private bool ibool_ignoreValidation = false;  
        // Toggles validation on and off for Next Inspection Date Calculation //

        
        private string subTypeID = "";

        public int UpdateRefreshStatus = 0;
        public int _LastOrder = 0;
        #endregion

        public frm_HR_Master_Qualification_SubType_Edit_Allocation()
        {
            InitializeComponent();
        }

        private void frm_HR_Master_Qualification_SubType_Edit_Allocation_Load(object sender, EventArgs e)
        {
            this.FormID = 700005;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //

            strConnectionString = this.GlobalSettings.ConnectionString;

            /**
            sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00234_Master_Qualification_SubType_EditTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit, strFormMode, subTypeID);

            string typeID = this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows[0]["QualificationTypeID"].ToString();

            sp_HR_00232_Master_Qualification_Types_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_HR_00232_Master_Qualification_Types_EditTableAdapter.Fill(this.dataSet_HR_DataEntry.sp_HR_00232_Master_Qualification_Types_Edit, strFormMode, typeID);
            **/

            sp_TR_00001_Qualification_SubType_StatusTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00001_Qualification_SubType_StatusTableAdapter.Fill(dataSet_TR_Core.sp_TR_00001_Qualification_SubType_Status);

            sp_TR_00006_Training_AllocationTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00006_Training_AllocationTableAdapter.Fill(dataSet_TR_Core.sp_TR_00006_Training_Allocation);

            sp_TR_00007_Allocation_RequirementTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00007_Allocation_RequirementTableAdapter.Fill(dataSet_TR_Core.sp_TR_00007_Allocation_Requirement);

            sp_TR_00008_Allocation_Time_LimitTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00008_Allocation_Time_LimitTableAdapter.Fill(dataSet_TR_Core.sp_TR_00008_Allocation_Time_Limit);

            sp_TR_00021_Qualification_Sub_Type_Allocation_AvailableTableAdapter.Connection.ConnectionString = strConnectionString;

            sp_TR_00050_Team_Holding_TypeTableAdapter.Connection.ConnectionString = strConnectionString;
            sp_TR_00050_Team_Holding_TypeTableAdapter.Fill(this.dataSet_TR_Core.sp_TR_00050_Team_Holding_Type);

            sp_TR_00108_Qualification_Allocation_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            
            // Populate Main Dataset //   

            dataLayoutControl1.BeginUpdate();

            BlockModePreset();

            switch (strFormMode)
            {
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = this.dataSet_TR_DataEntry.sp_TR_00108_Qualification_Allocation_Edit.NewRow();
                        drNewRow["Mode"] = strFormMode;
                        drNewRow["RecordIDS"] = "";
                        drNewRow["QualificationSubTypeID"] = Convert.ToInt32(subTypeID);

                        drNewRow["CreatedByUserID"] = 0;
                        drNewRow["CreatedTimeStamp"] = DateTime.Now;
                        drNewRow["CreatedSourceID"] = 00108;
                        drNewRow["LastUpdatedByUserID"] = 0;
                        drNewRow["LastUpdatedTimeStamp"] = DateTime.Now;
                        drNewRow["LastUpdatedSourceID"] = 00108;
                        this.dataSet_TR_DataEntry.sp_TR_00108_Qualification_Allocation_Edit.Rows.Add(drNewRow);
                        this.dataSet_TR_DataEntry.sp_TR_00108_Qualification_Allocation_Edit.Rows[0].AcceptChanges();
                    }
                    catch (Exception ex)
                    { }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp_TR_00108_Qualification_Allocation_EditTableAdapter.Fill(this.dataSet_TR_DataEntry.sp_TR_00108_Qualification_Allocation_Edit, strFormMode, strRecordIDs);
                    }
                    catch (Exception ex)
                    { }
                    break;
                default:
                    break;
            }


            dataLayoutControl1.EndUpdate();

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //
        }


        private void BlockModePreset()
        {
            TimeLimitTypeGridLookUpEdit.ReadOnly = true;
            timeLimitSpinEdit.ReadOnly = true;
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is DevExpress.XtraEditors.TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is DevExpress.XtraEditors.BaseEdit) ((DevExpress.XtraEditors.BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            {
                SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
            }
            else
            {
                SetChangesPendingLabel();
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            
            /**
            if (this.dataSet_HR_DataEntry.sp_HR_00234_Master_Qualification_SubType_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Qualification Sub-Type", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                this.strFormMode = "blockedit";
                dxErrorProvider1.ClearErrors();
                this.ValidateChildren();
                this.Close();
                return;
            }

            **/
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }


        private void ConfigureFormAccordingToMode()
        {
            Skin skin = RibbonSkins.GetSkin(this.LookAndFeel);
            SkinElement element = skin[RibbonSkins.SkinStatusBarButton];
            Color color = element.GetForeColorAppearance(new DevExpress.Utils.AppearanceObject(), DevExpress.Utils.Drawing.ObjectState.Normal).ForeColor;

            switch (strFormMode)
            {
                case "edit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information:";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                        startDateDateEdit.Focus();                       
                    }
                    catch (Exception)
                    {
                    }
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Block Editing";
                        barStaticItemFormMode.ImageIndex = 1;  // Edit //
                        barStaticItemFormMode.Appearance.ForeColor = Color.Red;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: You are block editing - any change made to any field will be applied to all selected records!";
                        barStaticItemInformation.ImageIndex = 3; // Exclamation //
                        barStaticItemInformation.Appearance.ForeColor = Color.Red;
                        startDateDateEdit.Focus();
                    }
                    catch (Exception)
                    {
                    }
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    try
                    {
                        barStaticItemFormMode.Caption = "Form Mode: Viewing";
                        barStaticItemFormMode.ImageIndex = 4;  // View //
                        barStaticItemFormMode.Appearance.ForeColor = color;
                        barStaticItemRecordsLoaded.Caption = "Records Loaded: " + intRecordCount.ToString();
                        barStaticItemInformation.Caption = "Information: Read Only";
                        barStaticItemInformation.ImageIndex = 2; // Information //
                        barStaticItemInformation.Appearance.ForeColor = color;
                    }
                    catch (Exception)
                    {
                    }
                    break;
                default:
                    break;
            }

            SetChangesPendingLabel();
            SetEditorButtons();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(this.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }
        }

        private void SetEditorButtons()
        {
            // Get Form Permissions //
        }
        
        private Boolean SetChangesPendingLabel()
        {
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //
            DataSet dsChanges = this.dataSet_TR_DataEntry.GetChanges();
            if (dsChanges != null)
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;  // Show Changes Pending on StatusBar //
                bbiSave.Enabled = (strFormMode == "blockadd" || strFormMode == "blockedit" ? false : true);
                bbiFormSave.Enabled = true;
                return true;
            }
            else
            {
                barStaticItemChangesPending.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;  // Hide Changes Pending on StatusBar //
                bbiSave.Enabled = false;
                bbiFormSave.Enabled = false;
                return false;
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            if (SetChangesPendingLabel() && (strFormMode != "blockadd" && strFormMode != "blockedit")) alItems.Add("iSave");

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

        }

        
        private void frm_HR_Master_Qualification_SubType_Edit_Allocation_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }
        

        private void frm_HR_Master_Qualification_SubType_Edit_Allocation_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;

            if (strFormMode == "view") return;
            string strMessage = CheckForPendingSave();
            if (strMessage != "")
            {
                strMessage += "\nWould you like to save the change(s) before closing the screen?";
                switch (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Close Screen - Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        // Abort screen close //
                        e.Cancel = true;
                        break;
                    case DialogResult.No:
                        // Proceed with screen close and don't save //
                        ibool_FormEditingCancelled = true; // Ensure that dataNavigator1_PositionChanged does not fire validation checks //
                        dxErrorProvider1.ClearErrors();
                        Detach_EditValuechanged_From_Children(this.Controls); // Attach Validating Event to all Editors to track changes...  Detached on Form Closing Event... //
                        e.Cancel = false;
                        break;
                    case DialogResult.Yes:
                        // Fire Save //
                        if (!string.IsNullOrEmpty(SaveChanges(true))) e.Cancel = true;  // Save Failed so abort form closing to allow user to correct //
                        break;
                }
            }
        }


        public override void OnSaveEvent(object sender, EventArgs e)
        {
            SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations));
        }

        private string SaveChanges(Boolean ib_SuccessMessage)
        {
            // Parameters - ib_SuccessMessage - determins if the success message should be shown at the end of the event //

            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //

            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            frmProgress fProgress = new frmProgress(0);
            fProgress.UpdateCaption("Saving...");
            fProgress.Show();
            Application.DoEvents();

            this.spTR00108QualificationAllocationEditBindingSource.EndEdit();

            try
            {
                this.sp_TR_00108_Qualification_Allocation_EditTableAdapter.Update(dataSet_TR_DataEntry);
            }
            catch (System.Exception ex)
            {
                fProgress.Close();
                fProgress.Dispose();
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return "Error";
            }

            fProgress.SetProgressValue(100);
            if (ib_SuccessMessage)  // If show confirmations is switched on in user preferences, show success message //
            {
                fProgress.UpdateCaption("Changes Saved");
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);  // Pause for 0.5 seconds //
            }
            /***

                        // If adding, pick up the new ID so it can be passed back to the manager so the new record can be highlghted //
            string strNewIDs = "";
            if (this.strFormMode.ToLower() == "add")
            {
                DataRowView currentRow = (DataRowView)spHR00234MasterQualificationSubTypeEditBindingSource.Current;
                if (currentRow != null) strNewIDs = Convert.ToInt32(currentRow["QualificationSubTypeID"]) + ";";

                // Switch mode to Edit so than any subsequent changes update this record //
                this.strFormMode = "edit";
                if (currentRow != null)
                {
                    currentRow["Mode"] = "edit";
                    currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                }
            }
            ***/


            // Notify any open instances of parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {

                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_HR_Master_Qualification_SubType_Edit")
                    {
                        frm_HR_Master_Qualification_SubType_Edit fParentForm;
                        fParentForm = (frm_HR_Master_Qualification_SubType_Edit)frmChild;
                        //fParentForm.UpdateFormRefreshStatus(2, Utils.enmMasterVettingGrids.VettingSubType, strNewIDs);
                        fParentForm.UpdateFormRefreshStatus(4);
                    }
                    {
                        if (frmChild.Name == "frm_TR_Allocation_Manager")
                        {
                            frm_TR_Allocation_Manager fParentForm;
                            fParentForm = (frm_TR_Allocation_Manager)frmChild;
                            //fParentForm.UpdateFormRefreshStatus(2, Utils.enmMasterVettingGrids.VettingSubType, strNewIDs);
                            fParentForm.UpdateFormRefreshStatus(1, "");
                        }

                    }
                }

                SetMenuStatus();  // Disabled Save and sets visibility of ChangesPending to Never //

                fProgress.Close();
                fProgress.Dispose();
                Application.DoEvents();

                if (GlobalSettings.ShowConfirmations == 1)
                {
                    int intCount = dataSet_TR_DataEntry.sp_TR_00108_Qualification_Allocation_Edit.Rows.Count;
                    XtraMessageBox.Show(intCount + " record(s) updated.", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
                return "";  // No problems //
            
        }

        private string CheckForPendingSave()
        {
            int intRecordNew = 0;
            int intRecordModified = 0;
            int intRecordDeleted = 0;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DatatLayout to underlying table adapter //

            for (int i = 0; i < this.dataSet_TR_DataEntry.sp_TR_00108_Qualification_Allocation_Edit.Rows.Count; i++)
            {
                switch (this.dataSet_TR_DataEntry.sp_TR_00108_Qualification_Allocation_Edit.Rows[i].RowState)
                {
                    case DataRowState.Added:
                        intRecordNew++;
                        break;
                    case DataRowState.Modified:
                        intRecordModified++;
                        break;
                    case DataRowState.Deleted:
                        intRecordDeleted++;
                        break;
                }
            }

            StringBuilder sbMessage = new StringBuilder();

            if (intRecordNew > 0) sbMessage.Append(Convert.ToString(intRecordNew) + " New record(s)\n");
            if (intRecordModified > 0) sbMessage.Append(Convert.ToString(intRecordModified) + " Updated record(s)\n");
            if (intRecordDeleted > 0) sbMessage.Append(Convert.ToString(intRecordDeleted) + " Deleted record(s)\n");

            //to do this probably need a seperate pending changes check for the tabs, so
            //can go back and complete rather than auto-save.
            //if (maintainQualificationAwardingBody.ChangesPending()) { sbMessage.Append("Awarding Body amendments\n\n"); }              

            if (string.IsNullOrEmpty(sbMessage.ToString()))
            { return ""; }
            else
            {
                return "You have unsaved changes on the current screen...\n\n" +
                    sbMessage.ToString();
            }
        }

        private void bbiFormSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (string.IsNullOrEmpty(SaveChanges(Convert.ToBoolean(this.GlobalSettings.ShowConfirmations)))) this.Close();
        }

        private void bbiFormCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }


        #region Data Navigator

        private void dataNavigator1_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                e.Handled = true;
            }
        }

        private void dataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            if (!ibool_FormEditingCancelled)
            {
                if (dxErrorProvider1.HasErrors)
                {
                    string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                    DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
        }


        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Editors

        private void AllocationTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {

        }

        private void startDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;

            dxErrorProvider1.SetError(de,"");
            if (this.strFormMode != "blockedit")
            {
                int allocationType = 0;
                if (AllocationTypeGridLookUpEdit.EditValue != null)
                {
                    int.TryParse(AllocationTypeGridLookUpEdit.EditValue.ToString(), out allocationType);
                }

                if (allocationType == AllocationType.Client
                    || allocationType == AllocationType.Site)
                {
                    if (de.EditValue == null
                    || string.IsNullOrEmpty(de.EditValue.ToString()) )
                    {
                        dxErrorProvider1.SetError(de, "Start Date is Mandatory for this AllocationType.");
                        e.Cancel = true;  // Show stop icon as field is invalid //
                        return;
                    }
                }

                if (de.EditValue == null)
                { return; }

                if (string.IsNullOrEmpty(de.EditValue.ToString()))
                { return; }

                if (string.IsNullOrEmpty(endDateDateEdit.EditValue.ToString()))
                { return; }

                if (de.DateTime > endDateDateEdit.DateTime)
                {
                    dxErrorProvider1.SetError(de, "Start Date cannot be greater than End Date.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }

        }

        private void endDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;

            dxErrorProvider1.SetError(de, "");
            if (this.strFormMode != "blockedit")
            {
                if (de.EditValue == null)
                { return; }

                if (string.IsNullOrEmpty(de.EditValue.ToString()))
                    { return;}

                if (string.IsNullOrEmpty(startDateDateEdit.EditValue.ToString()))
                    { return;}

                if (de.DateTime < startDateDateEdit.DateTime)
                {
                    dxErrorProvider1.SetError(de, "End Date cannot be less than Start Date.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }

        }

        private void RequirementTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            int glueValue = 0;

            if (glue.EditValue != null && !string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                glueValue = Convert.ToInt32(glue.EditValue);
            }

            if (this.strFormMode != "blockedit" && glueValue == 0)
            {
                //dxErrorProvider1.SetError(QualificationSubTypeStatusTypeGridLookUpEdit, "Select a value.");
                dxErrorProvider1.SetError(glue, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }

            dxErrorProvider1.SetError(glue, "");

            SetUpTimeLimitFields();
        }

        private void SetUpTimeLimitFields()
        {
            int allocationTypeValue = 0;
            int requirementTypeValue = 0;

            if (AllocationTypeGridLookUpEdit.EditValue != null && !string.IsNullOrEmpty(AllocationTypeGridLookUpEdit.ToString()))
            {
                allocationTypeValue = Convert.ToInt32(AllocationTypeGridLookUpEdit.EditValue);
            }

            if (RequirementTypeGridLookUpEdit.EditValue != null && !string.IsNullOrEmpty(RequirementTypeGridLookUpEdit.ToString()))
            {
                requirementTypeValue = Convert.ToInt32(RequirementTypeGridLookUpEdit.EditValue);
            }

            if (AllocationTypeGridLookUpEdit.EditValue == null
               && this.strFormMode != "blockedit")
            {
                TimeLimitTypeGridLookUpEdit.EditValue = null;
                timeLimitSpinEdit.EditValue = null;

                return;
            }

            if (allocationTypeValue == AllocationType.GenericJobTitle
               && requirementTypeValue == RequirementType.Mandatory)
            {
                TimeLimitTypeGridLookUpEdit.ReadOnly = false;
                timeLimitSpinEdit.ReadOnly = false;
            }
            else
            {
                TimeLimitTypeGridLookUpEdit.EditValue = 0;
                timeLimitSpinEdit.EditValue = 0;

                TimeLimitTypeGridLookUpEdit.ReadOnly = true;
                timeLimitSpinEdit.ReadOnly = true;
            }
        }

        private void TimeLimitTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            int allocationTypeValue = 0;
            int requirementTypeValue = 0;

            if (AllocationTypeGridLookUpEdit.EditValue != null && !string.IsNullOrEmpty(AllocationTypeGridLookUpEdit.ToString()))
            {
                allocationTypeValue = Convert.ToInt32(AllocationTypeGridLookUpEdit.EditValue);
            }

            if (RequirementTypeGridLookUpEdit.EditValue != null && !string.IsNullOrEmpty(RequirementTypeGridLookUpEdit.ToString()))
            {
                requirementTypeValue = Convert.ToInt32(RequirementTypeGridLookUpEdit.EditValue);
            }

            GridLookUpEdit glue = (GridLookUpEdit)sender;

            dxErrorProvider1.SetError(glue, "");

            if (TimeLimitTypeGridLookUpEdit.ReadOnly) { return; }

            int timeLimitTypeValue = 0;
            if (TimeLimitTypeGridLookUpEdit.EditValue != null && !string.IsNullOrEmpty(TimeLimitTypeGridLookUpEdit.ToString()))
            {
                timeLimitTypeValue = Convert.ToInt32(TimeLimitTypeGridLookUpEdit.EditValue);
            }

            //Requirement Type is set to Mandatory
            if (allocationTypeValue == AllocationType.GenericJobTitle
                && requirementTypeValue == RequirementType.Mandatory
                && timeLimitTypeValue == 0
                )
            {
                dxErrorProvider1.SetError(glue, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }

            dxErrorProvider1.SetError(glue, "");
        }

        private void timeLimitSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            if ( ( (this.strFormMode == "blockedit"
                    && TimeLimitTypeGridLookUpEdit.EditValue != null
                    && !string.IsNullOrEmpty(TimeLimitTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(TimeLimitTypeGridLookUpEdit.EditValue) > 0 )
                || (this.strFormMode != "blockedit" ) ) 
                && (se.EditValue == null || string.IsNullOrEmpty(se.EditValue.ToString()) || Convert.ToInt32(se.EditValue) == 0))
            {
                if (TimeLimitTypeGridLookUpEdit.EditValue != null 
                    && !string.IsNullOrEmpty(TimeLimitTypeGridLookUpEdit.EditValue.ToString())
                    && Convert.ToInt32(TimeLimitTypeGridLookUpEdit.EditValue) > 0)
                {
                    dxErrorProvider1.SetError(se, "Select a value.");
                    e.Cancel = true;  // Show stop icon as field is invalid //
                    return;
                }
            }
           
            dxErrorProvider1.SetError(se, "");
           
        }

 

        #endregion

        private void gridControlVettingSubTypes_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {

        }

        private void allocationGridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            Generic_EditChanged(sender, new EventArgs());
        }

        private void teamHoldingTypeGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;

            if (glue.ReadOnly) { return; }

            int teamHoldingTypeValue = 0;
            if (glue.EditValue != null && !string.IsNullOrEmpty(glue.EditValue.ToString()))
            {
                teamHoldingTypeValue = Convert.ToInt32(glue.EditValue);
            }


            if (teamHoldingTypeValue == TeamHolderType.SubjectToMinimum)
            {
                teamMinimumSpinEdit.ReadOnly = false;
            }
            else
            {
                teamMinimumSpinEdit.EditValue = 0;
                teamMinimumSpinEdit.ReadOnly = true;
            }
        }

        private void teamMinimumSpinEdit_Validating(object sender, CancelEventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;

            if (se.ReadOnly) { return; }

            int teamHoldingTypeValue = 0;
            if (teamHoldingTypeGridLookUpEdit.EditValue != null && !string.IsNullOrEmpty(teamHoldingTypeGridLookUpEdit.EditValue.ToString()))
            {
                teamHoldingTypeValue = Convert.ToInt32(teamHoldingTypeGridLookUpEdit.EditValue);
            }

            int seValue = 0;

            if (se.EditValue != null && !string.IsNullOrEmpty(se.EditValue.ToString()))
            {
                seValue = Convert.ToInt32(se.EditValue);
            }

            if (teamHoldingTypeValue == TeamHolderType.SubjectToMinimum && seValue == 0)
            {
                dxErrorProvider1.SetError(se, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(se, "");

            }
        }

        private void AllocationTypeGridLookUpEdit_EditValueChanged(object sender, EventArgs e)
        {
            if (strFormMode == "view")
            {
                checkEdit1.Enabled = false;
                return;
            }

            int allocationType = 0;
            if (AllocationTypeGridLookUpEdit.EditValue != null)
            {
                int.TryParse(AllocationTypeGridLookUpEdit.EditValue.ToString(), out allocationType);
            }

            if (allocationType == AllocationType.Client
                || allocationType == AllocationType.Site )
            {
                checkEdit1.Enabled = true;
            }
            else
            {
                checkEdit1.Enabled = false;
            }
        }
    }
}


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Linq;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_Core_Select_Site_For_Single_Client : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;

        public string _Mode = "single";  // single or multiple //
        public int intOriginalSiteID = 0;
        public string strPassedInClientIDsFilter = "";
        public string strPassedInClientNameFilter = "";
        public string strOriginalSiteIDs = "";

        public int intSelectedSiteID = 0;
        public string strSelectedSiteIDs = "";
        public string strSelectedSiteName = "";
        public string strSelectedSiteCode = "";
        public string strSelectedSiteAddress = "";
        public string strSelectedSitePostcode = "";
        public double dblSelectedSiteLocationX = (double)0.00;
        public double dblSelectedSiteLocationY = (double)0.00;
        public int _SelectedCount = 0;
        BaseObjects.GridCheckMarksSelection selection1;
       
        #endregion

        public frm_Core_Select_Site_For_Single_Client()
        {
            InitializeComponent();
        }

        private void frm_Core_Select_Site_For_Single_Client_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500279;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp01045_Core_Sites_For_ClientIDTableAdapter.Connection.ConnectionString = strConnectionString;
            GridView view = (GridView)gridControl1.MainView;

            LoadData();
            gridControl1.ForceInitialize();

            this.Text = "Select Site For Client: " + strPassedInClientNameFilter;
            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.Width = 30;
                selection1.CheckMarkColumn.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
                selection1.CheckMarkColumn.VisibleIndex = 0;

                Array arrayRecords = strOriginalSiteIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["SiteID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalSiteID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["SiteID"], intOriginalSiteID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp01045_Core_Sites_For_ClientIDTableAdapter.Fill(dataSet_Common_Functionality.sp01045_Core_Sites_For_ClientID, strPassedInClientIDsFilter);            
            view.EndUpdate();
        }


        #region GridView1

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No Sites Available");
        }

        bool internalRowFocusing;
        private void gridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void gridView1_FilterEditorCreated(object sender, DevExpress.XtraGrid.Views.Base.FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void gridView1_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void gridView1_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (_Mode != "single")
            {
                if (string.IsNullOrEmpty(strSelectedSiteIDs))
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more records by ticking them before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            else  // Single selection mode //
            {
                if (intSelectedSiteID == 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a record before proceeding.", "Select Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }


        private void GetSelectedDetails()
        {
            GridView view = (GridView)gridControl1.MainView;
            if (_Mode != "single")
            {
                strSelectedSiteIDs = "";    // Reset any prior values first //
                strSelectedSiteName = "";  // Reset any prior values first //
                string strDescriptor1 = "";
                if (selection1.SelectedCount <= 0) return;

                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedSiteIDs += Convert.ToString(view.GetRowCellValue(i, "SiteID")) + ",";
                        strDescriptor1 = Convert.ToString(view.GetRowCellValue(i, "SiteName"));
                        
                        if (intCount == 0)
                        {
                            strSelectedSiteName = strDescriptor1;
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedSiteName += ", " + strDescriptor1;
                        }
                        intCount++;
                    }
                }
                _SelectedCount = intCount;
            }
            else  // Single record selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    var currentRowView = (DataRowView)sp01045CoreSitesForClientIDBindingSource.Current;
                    var currentRow = (DataSet_Common_Functionality.sp01045_Core_Sites_For_ClientIDRow)currentRowView.Row;
                    if (currentRow == null) return;
                    intSelectedSiteID = (string.IsNullOrEmpty(currentRow.SiteID.ToString()) ? 0 : currentRow.SiteID);
                    strSelectedSiteName = (string.IsNullOrEmpty(currentRow.SiteName.ToString()) ? "" : currentRow.SiteName);
                    strSelectedSiteCode = (string.IsNullOrEmpty(currentRow.SiteCode.ToString()) ? "" : currentRow.SiteCode);
                    strSelectedSiteAddress = (string.IsNullOrEmpty(currentRow.SiteAddress.ToString()) ? "" : currentRow.SiteAddress);
                    strSelectedSitePostcode = (string.IsNullOrEmpty(currentRow.SitePostcode.ToString()) ? "" : currentRow.SitePostcode);
                    dblSelectedSiteLocationX = (string.IsNullOrEmpty(currentRow.SiteLocationX.ToString()) ? (double)0.00 : currentRow.SiteLocationX);
                    dblSelectedSiteLocationY = (string.IsNullOrEmpty(currentRow.SiteLocationY.ToString()) ? (double)0.00 : currentRow.SiteLocationY);
                }
            }
        }




    }
}


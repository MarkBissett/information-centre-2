using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;  // for StreamReader //
using BaseObjects;
using WoodPlan5.Properties;
using DevExpress.XtraEditors.DXErrorProvider;  // For Validation Rules //
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;  // Mouse down Args for GridView Mouse Down //

namespace WoodPlan5
{
    public partial class frmUserScreenSettingsSave : BaseObjects.frmBase
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;
        GridHitInfo downHitInfo = null;
        BaseObjects.GridCheckMarksSelection selection1;
        private frmBase frmToProcess;
        public int intLayoutID = 0;
         public int intLayoutCreatedByID = 0;

        #endregion
        
        public frmUserScreenSettingsSave(frmBase activeForm)
        {
            InitializeComponent();
            frmToProcess = activeForm;

            InitValidationRules();
        }

        private void InitValidationRules()
        {
            // Create Rule // 
            ConditionValidationRule notEmptyValidationRule = new ConditionValidationRule();
            notEmptyValidationRule.ConditionOperator = ConditionOperator.IsNotBlank;
            notEmptyValidationRule.ErrorText = "Please enter a value [100 characters maximum]";

            // Link Rule to control //
            dxValidationProvider1.SetValidationRule(textEdit1, notEmptyValidationRule);
        }

        private void frmUserScreenSettingsSave_Load(object sender, EventArgs e)
        {
            this.Text += "  -  " + frmToProcess.Text;
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            Set_Grid_Highlighter_Transparent(this.Controls);

            textEdit1.Text = "";
            
            dxValidationProvider1.ValidationMode = ValidationMode.Auto;
            dxValidationProvider1.Validate();

            // Add record selection checkboxes to grid control //
            selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
            selection1.CheckMarkColumn.VisibleIndex = 0;
            selection1.CheckMarkColumn.Width = 30;
            
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            sp00082_save_user_screen_settings_group_listTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00082_save_user_screen_settings_group_listTableAdapter.Fill(this.dataSet_Groups.sp00082_save_user_screen_settings_group_list, this.GlobalSettings.UserID, this.GlobalSettings.PersonType);
            view.EndUpdate();
            
            InitValidationRules();

            if (this.GlobalSettings.PersonType == 0)  // Super User //
            {
                checkEdit4.Enabled = true;
            }
            if (dataSet_Groups.sp00082_save_user_screen_settings_group_list.Rows.Count == 0)
            {
                checkEdit5.Enabled = false;  // No groups available to share with so disable option //
            }           
        }

        private void gridView1_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, "No groups available to share with");
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            int intScreenID = frmToProcess.FormID;
            string strLayoutDescription = textEdit1.Text;
            if (strLayoutDescription.Trim() == "")
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("No Layout Description has been entered!\n\nPlease enter a description before proceeding.", "Save Screen Layout As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            string strShareWithGroupIDs = "";
    
            // Check the Layout Descriptor is Unique //
            int intMatchingDescriptors = 0;
            DataSet_GroupsTableAdapters.QueriesTableAdapter GetCount = new DataSet_GroupsTableAdapters.QueriesTableAdapter();
            GetCount.ChangeConnectionString(strConnectionString);
            intMatchingDescriptors = Convert.ToInt32(GetCount.sp00085_save_user_screen_Check_Descriptor_Unique(strLayoutDescription, intScreenID, -1));
            if (intMatchingDescriptors > 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("The Layout Description entered [" + strLayoutDescription + "] has already been linked to another layout for the current screen!\n\nPlease enter a different description before proceeding.", "Save Screen Layout As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (checkEdit5.Checked)
            {
                GridView view = (GridView)gridControl1.MainView;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(i, "CheckMarkSelection")) == 1)
                    {
                        strShareWithGroupIDs += Convert.ToString(view.GetRowCellValue(i, "GroupID")) + ",";
                    }
                }
                if (strShareWithGroupIDs == "")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("You have set the Sharing option to 'Only staff in the following groups...', but you have not chosen any groups to share with!\n\nTip: To specify groups, tick the tick box next to their name.\n\nIf you cannot see any groups in the list then you do not have access to any groups. Contact your system administrator for advice.", "Save Screen Layout As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("Are you sure you wish to save the current screen layout?", "Save Screen Layout As", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
           
            string strChangeType = "insert";
            intLayoutID = 0;
            intLayoutCreatedByID = this.GlobalSettings.UserID;
            string strLayoutRemarks = memoEdit1.Text;
            int intX = frmToProcess.Location.X;
            int intY = frmToProcess.Location.Y;
            int intWidth = frmToProcess.Width;
            int intHeight = frmToProcess.Height;
            int intState = 0;
            switch (frmToProcess.WindowState.ToString())
	        {
                case "Maximized":
                    intState = 0;
                    break;
                case "Minimized":
                    intState = 1;
                    break;
                case "Normal":
                    intState = 2;
                    break;
	            default:
                    intState = 2;
                    break;
            }
            int intDefaultLayout = Convert.ToInt32(checkEdit1.Checked);
            int intShareType = 0;
            if (checkEdit4.Checked)
            {
                intShareType = 1;
            }
            else if (checkEdit5.Checked)
            {
                intShareType = 2;
            }

            if (this.ValidateChildren() == true)
            {
                DataSet_GroupsTableAdapters.QueriesTableAdapter SaveScreenLayout = new DataSet_GroupsTableAdapters.QueriesTableAdapter();
                SaveScreenLayout.ChangeConnectionString(strConnectionString);
                intLayoutID = Convert.ToInt32(SaveScreenLayout.sp00083_Save_User_Screen_Layout(intLayoutID,
                                                                                               intScreenID,
                                                                                               intLayoutCreatedByID,
                                                                                               strLayoutDescription,
                                                                                               strLayoutRemarks,
                                                                                               intX,
                                                                                               intY,
                                                                                               intWidth,
                                                                                               intHeight,
                                                                                               intState,
                                                                                               intDefaultLayout,
                                                                                               intShareType,
                                                                                               strChangeType,
                                                                                               strShareWithGroupIDs));
                if (intLayoutID <= 0)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Unable to save layout [" + strLayoutDescription + "] - an error occurred while trying to save the form settings!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Screen Layout As", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
                frmToProcess.LoadedViewID = intLayoutID;
                frmToProcess.LoadedViewCreatedByID = intLayoutCreatedByID;

                Load_Saved_Layout SaveLayout = new Load_Saved_Layout();
                SaveLayout.Store_Screen_Object_Settings(strConnectionString, intLayoutID, frmToProcess, null);

                this.DialogResult = DialogResult.OK;  // Put here, not in properties of OK button otherwise form will shut even if it fails validation test earlier in this process //
                this.Close();
            }
        }

        private void checkEdit3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit3.Checked)
            {
                gridControl1.Enabled = false;
            }
        }

        private void checkEdit4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit4.Checked)
            {
                gridControl1.Enabled = false;
            }
        }

        private void checkEdit5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit5.Checked)
            {
                gridControl1.Enabled = true;
            }
        }


 
    }
}


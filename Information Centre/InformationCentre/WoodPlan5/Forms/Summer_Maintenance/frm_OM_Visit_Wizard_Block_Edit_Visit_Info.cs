﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Wizard_Block_Edit_Visit_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strMaterialDescription = null;

        public int? intVisitNumber = null;
        public DateTime? dtVisitStartDate = null;
        public DateTime? dtVisitEndDate = null;
        public int? intCostCalculationLevel = null;
        public int? intSellCalculationLevel = null;
        public int? intVisitCategoryID = null;
        public int? intVisitTypeID = null;
        public string strWorkNumber = null;
        public int intCascadeDates = 0;
        public int? intClientPOID = null;
        public string strClientPONumber = null;
        public int? intDaysSeparation = null;

        #endregion

        public frm_OM_Visit_Wizard_Block_Edit_Visit_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Wizard_Block_Edit_Visit_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500202;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            try
            {
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06250_OM_Visit_Categories_With_BlankTableAdapter.Fill(dataSet_OM_Visit.sp06250_OM_Visit_Categories_With_Blank, 0);

                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06134_OM_Job_Calculation_Level_DescriptorsTableAdapter.Fill(dataSet_OM_Visit.sp06134_OM_Job_Calculation_Level_Descriptors, 0);

                sp06308_OM_Visit_TypesTableAdapter.Connection.ConnectionString = strConnectionString;
                sp06308_OM_Visit_TypesTableAdapter.Fill(dataSet_OM_Visit.sp06308_OM_Visit_Types, 0);
            }
            catch (Exception) { }

            spinEditVisitNumber.EditValue = null;
            dateEditVisitStartDate.EditValue = null;
            dateEditVisitEndDate.EditValue = null;
            gridLookUpEditApplyCostCalculationLevel.EditValue = null;
            gridLookUpEditApplySellCalculationLevel.EditValue = null;
            gridLookUpEditApplyVisitCategoryID.EditValue = null;
            gridLookUpEditApplyVisitTypeID.EditValue = null;
            textEditApplyWorkNumber.EditValue = null;
            ClientPONumberButtonEdit.EditValue = null;
            ClientPOIDTextEdit.EditValue = null;
            DaysSeparationSpinEdit.EditValue = null;

            checkEditCascadeDates.Checked = false;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (spinEditVisitNumber.EditValue != null) intVisitNumber = Convert.ToInt32(spinEditVisitNumber.EditValue);
            if (dateEditVisitStartDate.EditValue != null) dtVisitStartDate = Convert.ToDateTime(dateEditVisitStartDate.EditValue);
            if (dateEditVisitEndDate.EditValue != null) dtVisitEndDate = Convert.ToDateTime(dateEditVisitEndDate.EditValue);
            if (gridLookUpEditApplyCostCalculationLevel.EditValue != null) intCostCalculationLevel = Convert.ToInt32(gridLookUpEditApplyCostCalculationLevel.EditValue);
            if (gridLookUpEditApplySellCalculationLevel.EditValue != null) intSellCalculationLevel = Convert.ToInt32(gridLookUpEditApplySellCalculationLevel.EditValue);
            if (gridLookUpEditApplyVisitCategoryID.EditValue != null) intVisitCategoryID = Convert.ToInt32(gridLookUpEditApplyVisitCategoryID.EditValue);
            if (gridLookUpEditApplyVisitTypeID.EditValue != null) intVisitTypeID = Convert.ToInt32(gridLookUpEditApplyVisitTypeID.EditValue);
            if (textEditApplyWorkNumber.EditValue != null) strWorkNumber = textEditApplyWorkNumber.EditValue.ToString();
            if (ClientPONumberButtonEdit.EditValue != null) strClientPONumber = ClientPONumberButtonEdit.EditValue.ToString();
            if (ClientPOIDTextEdit.EditValue != null) intClientPOID = Convert.ToInt32(ClientPOIDTextEdit.EditValue);
            if (DaysSeparationSpinEdit.EditValue != null) intDaysSeparation = Convert.ToInt32(DaysSeparationSpinEdit.EditValue);


            intCascadeDates = (checkEditCascadeDates.Checked ? 1 : 0);

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ClientPONumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_OM_Select_Client_PO();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInParentIDs = "";
                fChildForm.intPassedInChildID = 0;
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    ClientPOIDTextEdit.EditValue = fChildForm.intSelectedChildID;
                    ClientPONumberButtonEdit.EditValue = fChildForm.strSelectedChildDescriptions;
                }
            }
            else
            {
                ClientPOIDTextEdit.EditValue = 0;
                ClientPONumberButtonEdit.EditValue = "";
            }
        }





    }
}

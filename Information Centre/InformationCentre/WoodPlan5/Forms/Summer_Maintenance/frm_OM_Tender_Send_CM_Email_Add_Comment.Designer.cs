﻿namespace WoodPlan5
{
    partial class frm_OM_Tender_Send_CM_Email_Add_Comment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Tender_Send_CM_Email_Add_Comment));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            this.btnOK = new DevExpress.XtraEditors.SimpleButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bsiInformation = new DevExpress.XtraBars.BarStaticItem();
            this.memoEditComments = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Core = new WoodPlan5.DataSet_OM_Core();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLinkedDocumentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToRecordTypeID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentPath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemHyperLinkEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.colDocumentExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateAdded = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colLinkedRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAddedByStaff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumentRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colLinkedToRecordParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.checkEditYes = new DevExpress.XtraEditors.CheckEdit();
            this.checkEditNo = new DevExpress.XtraEditors.CheckEdit();
            this.sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter = new WoodPlan5.DataSet_OM_CoreTableAdapters.sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditComments.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditYes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(697, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 522);
            this.barDockControlBottom.Size = new System.Drawing.Size(697, 30);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 496);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(697, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 496);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.Images = this.imageCollection1;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiInformation});
            this.barManager1.MaxItemId = 32;
            this.barManager1.StatusBar = this.bar1;
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // btnOK
            // 
            this.btnOK.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.ImageOptions.ImageIndex = 1;
            this.btnOK.ImageOptions.ImageList = this.imageCollection1;
            this.btnOK.Location = new System.Drawing.Point(520, 494);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(80, 22);
            this.btnOK.TabIndex = 13;
            this.btnOK.Text = "<b>OK</b>";
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Info_16x16.png");
            this.imageCollection1.InsertGalleryImage("apply_16x16.png", "images/actions/apply_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/apply_16x16.png"), 1);
            this.imageCollection1.Images.SetKeyName(1, "apply_16x16.png");
            this.imageCollection1.InsertGalleryImage("cancel_16x16.png", "images/actions/cancel_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/actions/cancel_16x16.png"), 2);
            this.imageCollection1.Images.SetKeyName(2, "cancel_16x16.png");
            // 
            // btnCancel
            // 
            this.btnCancel.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.ImageOptions.ImageIndex = 2;
            this.btnCancel.ImageOptions.ImageList = this.imageCollection1;
            this.btnCancel.Location = new System.Drawing.Point(608, 494);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 22);
            this.btnCancel.TabIndex = 14;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiInformation)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 3";
            // 
            // bsiInformation
            // 
            this.bsiInformation.Caption = "Select the action to take. Enter a comment if appropriate.";
            this.bsiInformation.Id = 30;
            this.bsiInformation.ImageOptions.ImageIndex = 0;
            this.bsiInformation.Name = "bsiInformation";
            this.bsiInformation.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // memoEditComments
            // 
            this.memoEditComments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memoEditComments.Location = new System.Drawing.Point(0, 0);
            this.memoEditComments.MenuManager = this.barManager1;
            this.memoEditComments.Name = "memoEditComments";
            this.memoEditComments.Properties.ReadOnly = true;
            this.scSpellChecker.SetShowSpellCheckMenu(this.memoEditComments, true);
            this.memoEditComments.Size = new System.Drawing.Size(636, 112);
            this.scSpellChecker.SetSpellCheckerOptions(this.memoEditComments, optionsSpelling1);
            this.memoEditComments.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.AllowHtmlText = true;
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.splitContainerControl1);
            this.groupControl1.Controls.Add(this.checkEditYes);
            this.groupControl1.Controls.Add(this.checkEditNo);
            this.groupControl1.Location = new System.Drawing.Point(9, 35);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(679, 453);
            this.groupControl1.TabIndex = 23;
            this.groupControl1.Text = "Do you wish to <b>add</b> any specific comments or linked documents to the <b>CM " +
    "email</b> ?";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(29, 77);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel1.Controls.Add(this.memoEditComments);
            this.splitContainerControl1.Panel1.ShowCaption = true;
            this.splitContainerControl1.Panel1.Text = "Comment";
            this.splitContainerControl1.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl1.Panel2.Controls.Add(this.gridControl3);
            this.splitContainerControl1.Panel2.ShowCaption = true;
            this.splitContainerControl1.Panel2.Text = "Linked Documents - Tick those to add to Email";
            this.splitContainerControl1.Size = new System.Drawing.Size(640, 366);
            this.splitContainerControl1.SplitterPosition = 136;
            this.splitContainerControl1.TabIndex = 27;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl3.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl3.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl3.Enabled = false;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView3;
            this.gridControl3.MenuManager = this.barManager1;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit4,
            this.repositoryItemHyperLinkEdit2,
            this.repositoryItemTextEditDateTime4});
            this.gridControl3.Size = new System.Drawing.Size(636, 200);
            this.gridControl3.TabIndex = 25;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // sp06514OMLinkedDocumentsLinkedToRecordBindingSource
            // 
            this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource.DataMember = "sp06514_OM_Linked_Documents_Linked_To_Record";
            this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource.DataSource = this.dataSet_OM_Core;
            // 
            // dataSet_OM_Core
            // 
            this.dataSet_OM_Core.DataSetName = "DataSet_OM_Core";
            this.dataSet_OM_Core.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLinkedDocumentID,
            this.colLinkedToRecordID,
            this.colLinkedToRecordTypeID,
            this.colDocumentPath,
            this.colDocumentExtension,
            this.colDescription,
            this.colAddedByStaffID,
            this.colDateAdded,
            this.colLinkedRecord,
            this.colAddedByStaff,
            this.colDocumentRemarks,
            this.colLinkedToRecordParent});
            this.gridView3.GridControl = this.gridControl3;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView3.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView3.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView3.OptionsLayout.StoreAppearance = true;
            this.gridView3.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView3.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView3.OptionsView.ColumnAutoWidth = false;
            this.gridView3.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDateAdded, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDescription, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView3.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView3_PopupMenuShowing);
            this.gridView3.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.gridView3_CustomDrawEmptyForeground);
            this.gridView3.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView3_FocusedRowChanged);
            this.gridView3.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.gridView3_CustomFilterDialog);
            this.gridView3.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.gridView3_FilterEditorCreated);
            this.gridView3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView3_MouseDown);
            // 
            // colLinkedDocumentID
            // 
            this.colLinkedDocumentID.Caption = "Linked Document ID";
            this.colLinkedDocumentID.FieldName = "LinkedDocumentID";
            this.colLinkedDocumentID.Name = "colLinkedDocumentID";
            this.colLinkedDocumentID.OptionsColumn.AllowEdit = false;
            this.colLinkedDocumentID.OptionsColumn.AllowFocus = false;
            this.colLinkedDocumentID.OptionsColumn.ReadOnly = true;
            this.colLinkedDocumentID.Width = 116;
            // 
            // colLinkedToRecordID
            // 
            this.colLinkedToRecordID.Caption = "Linked Record ID";
            this.colLinkedToRecordID.FieldName = "LinkedToRecordID";
            this.colLinkedToRecordID.Name = "colLinkedToRecordID";
            this.colLinkedToRecordID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordID.Width = 102;
            // 
            // colLinkedToRecordTypeID
            // 
            this.colLinkedToRecordTypeID.Caption = "Linked Record Type ID";
            this.colLinkedToRecordTypeID.FieldName = "LinkedDocumentTypeID";
            this.colLinkedToRecordTypeID.Name = "colLinkedToRecordTypeID";
            this.colLinkedToRecordTypeID.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordTypeID.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordTypeID.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordTypeID.Width = 129;
            // 
            // colDocumentPath
            // 
            this.colDocumentPath.Caption = "Document";
            this.colDocumentPath.ColumnEdit = this.repositoryItemHyperLinkEdit2;
            this.colDocumentPath.FieldName = "DocumentPath";
            this.colDocumentPath.Name = "colDocumentPath";
            this.colDocumentPath.OptionsColumn.ReadOnly = true;
            this.colDocumentPath.Visible = true;
            this.colDocumentPath.VisibleIndex = 2;
            this.colDocumentPath.Width = 360;
            // 
            // repositoryItemHyperLinkEdit2
            // 
            this.repositoryItemHyperLinkEdit2.AutoHeight = false;
            this.repositoryItemHyperLinkEdit2.Name = "repositoryItemHyperLinkEdit2";
            this.repositoryItemHyperLinkEdit2.SingleClick = true;
            this.repositoryItemHyperLinkEdit2.OpenLink += new DevExpress.XtraEditors.Controls.OpenLinkEventHandler(this.repositoryItemHyperLinkEdit2_OpenLink);
            // 
            // colDocumentExtension
            // 
            this.colDocumentExtension.Caption = "Type";
            this.colDocumentExtension.FieldName = "DocumentExtension";
            this.colDocumentExtension.Name = "colDocumentExtension";
            this.colDocumentExtension.OptionsColumn.AllowEdit = false;
            this.colDocumentExtension.OptionsColumn.AllowFocus = false;
            this.colDocumentExtension.OptionsColumn.ReadOnly = true;
            this.colDocumentExtension.Visible = true;
            this.colDocumentExtension.VisibleIndex = 3;
            this.colDocumentExtension.Width = 84;
            // 
            // colDescription
            // 
            this.colDescription.Caption = "Description";
            this.colDescription.FieldName = "Description";
            this.colDescription.Name = "colDescription";
            this.colDescription.OptionsColumn.AllowEdit = false;
            this.colDescription.OptionsColumn.AllowFocus = false;
            this.colDescription.OptionsColumn.ReadOnly = true;
            this.colDescription.Visible = true;
            this.colDescription.VisibleIndex = 1;
            this.colDescription.Width = 319;
            // 
            // colAddedByStaffID
            // 
            this.colAddedByStaffID.Caption = "Added By Staff ID";
            this.colAddedByStaffID.FieldName = "AddedByStaffID";
            this.colAddedByStaffID.Name = "colAddedByStaffID";
            this.colAddedByStaffID.OptionsColumn.AllowEdit = false;
            this.colAddedByStaffID.OptionsColumn.AllowFocus = false;
            this.colAddedByStaffID.OptionsColumn.ReadOnly = true;
            this.colAddedByStaffID.Width = 108;
            // 
            // colDateAdded
            // 
            this.colDateAdded.Caption = "Date Added";
            this.colDateAdded.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colDateAdded.FieldName = "DateAdded";
            this.colDateAdded.Name = "colDateAdded";
            this.colDateAdded.OptionsColumn.AllowEdit = false;
            this.colDateAdded.OptionsColumn.AllowFocus = false;
            this.colDateAdded.OptionsColumn.ReadOnly = true;
            this.colDateAdded.Visible = true;
            this.colDateAdded.VisibleIndex = 0;
            this.colDateAdded.Width = 98;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colLinkedRecord
            // 
            this.colLinkedRecord.Caption = "Linked To";
            this.colLinkedRecord.FieldName = "LinkedToRecord";
            this.colLinkedRecord.Name = "colLinkedRecord";
            this.colLinkedRecord.OptionsColumn.AllowEdit = false;
            this.colLinkedRecord.OptionsColumn.AllowFocus = false;
            this.colLinkedRecord.OptionsColumn.ReadOnly = true;
            this.colLinkedRecord.Width = 131;
            // 
            // colAddedByStaff
            // 
            this.colAddedByStaff.Caption = "Added By";
            this.colAddedByStaff.FieldName = "AddedByStaff";
            this.colAddedByStaff.Name = "colAddedByStaff";
            this.colAddedByStaff.OptionsColumn.AllowEdit = false;
            this.colAddedByStaff.OptionsColumn.AllowFocus = false;
            this.colAddedByStaff.OptionsColumn.ReadOnly = true;
            this.colAddedByStaff.Visible = true;
            this.colAddedByStaff.VisibleIndex = 4;
            this.colAddedByStaff.Width = 138;
            // 
            // colDocumentRemarks
            // 
            this.colDocumentRemarks.Caption = "Remarks";
            this.colDocumentRemarks.ColumnEdit = this.repositoryItemMemoExEdit4;
            this.colDocumentRemarks.FieldName = "Remarks";
            this.colDocumentRemarks.Name = "colDocumentRemarks";
            this.colDocumentRemarks.OptionsColumn.ReadOnly = true;
            this.colDocumentRemarks.Visible = true;
            this.colDocumentRemarks.VisibleIndex = 5;
            this.colDocumentRemarks.Width = 134;
            // 
            // repositoryItemMemoExEdit4
            // 
            this.repositoryItemMemoExEdit4.AutoHeight = false;
            this.repositoryItemMemoExEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit4.Name = "repositoryItemMemoExEdit4";
            this.repositoryItemMemoExEdit4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit4.ShowIcon = false;
            // 
            // colLinkedToRecordParent
            // 
            this.colLinkedToRecordParent.Caption = "Linked To Tender";
            this.colLinkedToRecordParent.FieldName = "LinkedToRecordParent";
            this.colLinkedToRecordParent.Name = "colLinkedToRecordParent";
            this.colLinkedToRecordParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToRecordParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToRecordParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToRecordParent.Width = 375;
            // 
            // checkEditYes
            // 
            this.checkEditYes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditYes.EditValue = 0;
            this.checkEditYes.Location = new System.Drawing.Point(8, 52);
            this.checkEditYes.MenuManager = this.barManager1;
            this.checkEditYes.Name = "checkEditYes";
            this.checkEditYes.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditYes.Properties.Caption = "<b>Yes</b>, enter the comments and \\ or tick the linked documents to send then pr" +
    "oceed with Sending";
            this.checkEditYes.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditYes.Properties.RadioGroupIndex = 1;
            this.checkEditYes.Properties.ValueChecked = 1;
            this.checkEditYes.Properties.ValueUnchecked = 0;
            this.checkEditYes.Size = new System.Drawing.Size(663, 19);
            this.checkEditYes.TabIndex = 24;
            this.checkEditYes.TabStop = false;
            this.checkEditYes.CheckedChanged += new System.EventHandler(this.checkEditYes_CheckedChanged);
            // 
            // checkEditNo
            // 
            this.checkEditNo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEditNo.EditValue = 1;
            this.checkEditNo.Location = new System.Drawing.Point(8, 26);
            this.checkEditNo.MenuManager = this.barManager1;
            this.checkEditNo.Name = "checkEditNo";
            this.checkEditNo.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEditNo.Properties.Caption = "<b>No</b>, proceed with Sending";
            this.checkEditNo.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEditNo.Properties.RadioGroupIndex = 1;
            this.checkEditNo.Properties.ValueChecked = 1;
            this.checkEditNo.Properties.ValueUnchecked = 0;
            this.checkEditNo.Size = new System.Drawing.Size(662, 19);
            this.checkEditNo.TabIndex = 0;
            this.checkEditNo.CheckedChanged += new System.EventHandler(this.checkEditNo_CheckedChanged);
            // 
            // sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter
            // 
            this.sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Tender_Send_CM_Email_Add_Comment
            // 
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(697, 552);
            this.ControlBox = false;
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.LookAndFeel.SkinName = "Blue";
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_OM_Tender_Send_CM_Email_Add_Comment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Send CM Tender Quote";
            this.Load += new System.EventHandler(this.frm_OM_Tender_Send_CM_Email_Add_Comment_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.btnCancel, 0);
            this.Controls.SetChildIndex(this.btnOK, 0);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.memoEditComments.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06514OMLinkedDocumentsLinkedToRecordBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Core)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemHyperLinkEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditYes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditNo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnOK;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiInformation;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEditYes;
        private DevExpress.XtraEditors.CheckEdit checkEditNo;
        private DevExpress.XtraEditors.MemoEdit memoEditComments;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedDocumentID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordID;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordTypeID;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentPath;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit repositoryItemHyperLinkEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateAdded;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedRecord;
        private DevExpress.XtraGrid.Columns.GridColumn colAddedByStaff;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumentRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToRecordParent;
        private System.Windows.Forms.BindingSource sp06514OMLinkedDocumentsLinkedToRecordBindingSource;
        private DataSet_OM_Core dataSet_OM_Core;
        private DataSet_OM_CoreTableAdapters.sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter sp06514_OM_Linked_Documents_Linked_To_RecordTableAdapter;
        public DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Wizard_Block_Edit_Equipment_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public string strEquipmentType = null;
        public decimal? decCostUnitsUsed = null;
        public int? intCostUnitDescriptorID = null;
        public decimal? decCostPerUnitExVat = null;
        public decimal? decCostPerUnitVatRate = null;

        public decimal? decSellUnitsUsed = null;
        public int? intSellUnitDescriptorID = null;
        public decimal? decSellPerUnitExVat = null;
        public decimal? decSellPerUnitVatRate = null;
       
        public string strRemarks = null;
        public string strOwnerName = null;
        public string strOwnerType = null;

        public int? intEquipmentID = null;
        public double? dbTeamLocationX = null;
        public double? dbTeamLocationY = null;
        public string strTeamPostcode = null;
        #endregion

        public frm_OM_Job_Wizard_Block_Edit_Equipment_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Job_Wizard_Block_Edit_Equipment_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500169;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;
            
            sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06101_OM_Work_Unit_Types_PicklistTableAdapter.Fill(dataSet_OM_Contract.sp06101_OM_Work_Unit_Types_Picklist, 1);


            EquipmentIDTextEdit.EditValue = null;
            EquipmentTypeButtonEdit.EditValue = null;
            CostUnitsUsedSpinEdit.EditValue = null;
            CostUnitDescriptorIDGridLookUpEdit.EditValue = null;
            CostPerUnitExVatSpinEdit.EditValue = null;
            CostPerUnitVatRateSpinEdit.EditValue = null;

            SellUnitsUsedSpinEdit.EditValue = null;
            SellUnitDescriptorIDGridLookUpEdit.EditValue = null;
            SellPerUnitExVatSpinEdit.EditValue = null;
            SellPerUnitVatRateSpinEdit.EditValue = null;
            
            RemarksMemoEdit.EditValue = null;
            OwnerNameTextEdit.EditValue = null;
            OwnerTypeTextEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void EquipmentTypeButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_OM_Select_Equipment();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = 0;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    EquipmentIDTextEdit.EditValue = fChildForm.intSelectedID;
                    EquipmentTypeButtonEdit.EditValue = fChildForm.strSelectedDescription1;
                    OwnerNameTextEdit.EditValue = fChildForm.strSelectedDescription2;
                    OwnerTypeTextEdit.EditValue = fChildForm.strSelectedDescription3;
                }
            }
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (EquipmentIDTextEdit.EditValue != null) intEquipmentID = Convert.ToInt32(EquipmentIDTextEdit.EditValue);
            if (EquipmentTypeButtonEdit.EditValue != null) strEquipmentType = EquipmentTypeButtonEdit.EditValue.ToString();
            if (CostUnitsUsedSpinEdit.EditValue != null) decCostUnitsUsed = Convert.ToDecimal(CostUnitsUsedSpinEdit.EditValue);
            if (CostUnitDescriptorIDGridLookUpEdit.EditValue != null) intCostUnitDescriptorID = Convert.ToInt32(CostUnitDescriptorIDGridLookUpEdit.EditValue);
            if (CostPerUnitExVatSpinEdit.EditValue != null) decCostPerUnitExVat = Convert.ToDecimal(CostPerUnitExVatSpinEdit.EditValue);           
            if (CostPerUnitVatRateSpinEdit.EditValue != null) decCostPerUnitVatRate = Convert.ToDecimal(CostPerUnitVatRateSpinEdit.EditValue);

            if (SellUnitsUsedSpinEdit.EditValue != null) decSellUnitsUsed = Convert.ToDecimal(SellUnitsUsedSpinEdit.EditValue);
            if (SellUnitDescriptorIDGridLookUpEdit.EditValue != null) intSellUnitDescriptorID = Convert.ToInt32(SellUnitDescriptorIDGridLookUpEdit.EditValue);
            if (SellPerUnitExVatSpinEdit.EditValue != null) decSellPerUnitExVat = Convert.ToDecimal(SellPerUnitExVatSpinEdit.EditValue);
            if (SellPerUnitVatRateSpinEdit.EditValue != null) decSellPerUnitVatRate = Convert.ToDecimal(SellPerUnitVatRateSpinEdit.EditValue);
            
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
            if (OwnerNameTextEdit.EditValue != null) strOwnerName = OwnerNameTextEdit.EditValue.ToString();
            if (OwnerTypeTextEdit.EditValue != null) strOwnerType = OwnerTypeTextEdit.EditValue.ToString();
              
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }


 





    }
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Visit_Chemical : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        public int intOriginalClientContractID = 0;
        public int intOriginalSiteContractID = 0;
        public int intOriginalVisitID = 0;
        public int intOriginalSprayingID = 0;

        public int intSelectedClientID = 0;
        public int intSelectedClientContractID = 0;
        public int intSelectedSiteID = 0;
        public int intSelectedSiteContractID = 0;
        public int intSelectedVisitID = 0;
        public int intSelectedVisitNumber = 0;
        public int intSelectedSprayingID = 0;
        public int intSelectedChemicalID = 0;

        public string strSelectedClientName = "";
        public string strSelectedContractDescription = "";
        public string strSelectedSiteName = "";
        public string strSelectedChemicalName = "";

        public string _strExcludeIDs = "";

        public DateTime? i_dt_FromDate = null;
        public DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";

        int intLinkedToRecordTypeID = 0;

        #endregion

        public frm_OM_Select_Visit_Chemical()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Visit_Chemical_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 50020500;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06066_OM_Select_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06147_OM_Visits_For_Site_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06233_OM_Chemicals_For_JobTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (i_dt_FromDate != null) dateEditFromDate.DateTime = Convert.ToDateTime(i_dt_FromDate);
            if (i_dt_ToDate != null) dateEditToDate.DateTime = Convert.ToDateTime(i_dt_ToDate);
            beiDateFilter.EditValue = PopupContainerEditDateRange_Get_Selected();

            GridView view = (GridView)gridControl1.MainView;
            LoadData();
            gridControl1.ForceInitialize();
            if (intOriginalClientContractID != 0)
            {
                int intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], intOriginalClientContractID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl2.ForceInitialize();
            if (intOriginalSiteContractID != 0)
            {
                view = (GridView)gridControl2.MainView; 
                int intFoundRow = view.LocateByValue(0, view.Columns["SiteContractID"], intOriginalSiteContractID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            gridControl3.ForceInitialize();
            if (intOriginalVisitID != 0)
            {
                view = (GridView)gridControl3.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["VisitID"], intOriginalVisitID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }
            
            gridControl5.ForceInitialize();
            if (intOriginalSprayingID != 0)
            {
                view = (GridView)gridControl5.MainView;
                int intFoundRow = view.LocateByValue(0, view.Columns["SprayingID"], intOriginalSprayingID);
                if (intFoundRow != GridControl.InvalidRowHandle)
                {
                    view.FocusedRowHandle = intFoundRow;
                    view.MakeRowVisible(intFoundRow, false);
                }
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }

        public override void PostLoadView(object objParameter)
        {
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;

            gridControl2.MainView.BeginUpdate();
            dataSet_OM_Contract.sp06066_OM_Select_Client_Contract.Clear();
            gridControl2.MainView.EndUpdate();

            gridControl3.MainView.BeginUpdate();
            dataSet_OM_Visit.sp06147_OM_Visits_For_Site_Contract.Clear();
            gridControl3.MainView.EndUpdate();

            gridControl5.MainView.BeginUpdate();
            dataSet_OM_Job.sp06233_OM_Chemicals_For_Job.Clear();
            gridControl5.MainView.EndUpdate();

            view.BeginUpdate();
            DateTime dtFromDate = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2001, 01, 01) : Convert.ToDateTime(i_dt_FromDate));
            DateTime dtToDate = (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2900, 12, 31) : Convert.ToDateTime(i_dt_ToDate));

            sp06066_OM_Select_Client_ContractTableAdapter.Fill(dataSet_OM_Contract.sp06066_OM_Select_Client_Contract, _strExcludeIDs, dtFromDate, dtToDate);
            view.EndUpdate();
        }

        private void LoadLinkedData1()
        {
            GridView view = (GridView)gridControl1.MainView;

            gridControl3.MainView.BeginUpdate();
            dataSet_OM_Visit.sp06147_OM_Visits_For_Site_Contract.Clear();
            gridControl3.MainView.EndUpdate();

            gridControl5.MainView.BeginUpdate();
            dataSet_OM_Job.sp06233_OM_Chemicals_For_Job.Clear();
            gridControl5.MainView.EndUpdate();

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientContractID"])) + ',';
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06066_OM_Select_Client_Contract.Clear();
            }
            else
            {
                try
                {
                    sp06117_OM_Site_Contracts_For_Client_ContractTableAdapter.Fill(dataSet_OM_Contract.sp06117_OM_Site_Contracts_For_Client_Contract, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 0);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Site Contracts.\n\nTry selecting a Client Contract again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
        }

        private void LoadLinkedData2()
        {
            GridView view = (GridView)gridControl2.MainView;

            gridControl5.MainView.BeginUpdate();
            dataSet_OM_Job.sp06233_OM_Chemicals_For_Job.Clear();
            gridControl5.MainView.EndUpdate();

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["SiteContractID"])) + ',';
            }

            gridControl3.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Visit.sp06147_OM_Visits_For_Site_Contract.Clear();
            }
            else
            {
                try
                {
                    sp06147_OM_Visits_For_Site_ContractTableAdapter.Fill(dataSet_OM_Visit.sp06147_OM_Visits_For_Site_Contract, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Visits.\n\nTry selecting a Site Contract again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl3.MainView.EndUpdate();
        }

        private void LoadLinkedData3()
        {
            GridView view = (GridView)gridControl3.MainView;

            gridControl5.MainView.BeginUpdate();
            dataSet_OM_Job.sp06233_OM_Chemicals_For_Job.Clear();
            gridControl5.MainView.EndUpdate();

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["VisitID"])) + ',';
            }

            gridControl5.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06233_OM_Chemicals_For_Job.Clear();
            }
            else
            {
                try
                {
                    sp06233_OM_Chemicals_For_JobTableAdapter.Fill(dataSet_OM_Job.sp06233_OM_Chemicals_For_Job, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intLinkedToRecordTypeID);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Chemicals.\n\nTry selecting a Job again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl5.MainView.EndUpdate();
        }

        private void LoadLinkedData4()
        {
            GridView view = (GridView)gridControl3.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["VisitID"])) + ',';
            }

            gridControl5.MainView.BeginUpdate();
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06233_OM_Chemicals_For_Job.Clear();
            }
            else
            {
                try
                {
                    sp06233_OM_Chemicals_For_JobTableAdapter.Fill(dataSet_OM_Job.sp06233_OM_Chemicals_For_Job, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intLinkedToRecordTypeID);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Chemicals.\n\nTry selecting a Job again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl5.MainView.EndUpdate();
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Crontacts Available - Try adjusting any Date Filters and click Refresh Data button";
                    break;
                case "gridView2":
                    message = "No Site Contracts Available - Select a Client Contract to view linked Site Contracts";
                    break;
                case "gridView3":
                    message = "No Visits Available - Select a Site Contract to view linked Visits";
                    break;
                case "gridView5":
                    message = "No Chemicals Available - Select a Visit to view linked Chemicals";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    LoadLinkedData1();
                    break;
                case "gridView2":
                    LoadLinkedData2();
                    break;
                case "gridView3":
                    LoadLinkedData3();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";
            GridView view = (GridView)gridControl1.MainView;

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (intSelectedSprayingID <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select a visit spraying record before proceeding.", "Select Visit Spraying", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            GridView view1 = (GridView)gridControl1.MainView;
            GridView view2 = (GridView)gridControl2.MainView;
            GridView view3 = (GridView)gridControl3.MainView;
            GridView view5 = (GridView)gridControl5.MainView;
            if (view5.FocusedRowHandle != GridControl.InvalidRowHandle)
            {
                intSelectedClientID = Convert.ToInt32(view3.GetRowCellValue(view1.FocusedRowHandle, "ClientID"));
                intSelectedClientContractID = Convert.ToInt32(view3.GetRowCellValue(view1.FocusedRowHandle, "ClientContractID"));
                intSelectedSiteID = Convert.ToInt32(view3.GetRowCellValue(view2.FocusedRowHandle, "SiteID"));
                intSelectedSiteContractID = Convert.ToInt32(view3.GetRowCellValue(view2.FocusedRowHandle, "SiteContractID"));
                intSelectedVisitID = Convert.ToInt32(view3.GetRowCellValue(view3.FocusedRowHandle, "VisitID"));
                intSelectedSprayingID = Convert.ToInt32(view5.GetRowCellValue(view5.FocusedRowHandle, "SprayingID"));
                intSelectedChemicalID = Convert.ToInt32(view5.GetRowCellValue(view5.FocusedRowHandle, "ChemicalID"));

                intSelectedVisitNumber = (String.IsNullOrEmpty(Convert.ToString(view5.GetRowCellValue(view5.FocusedRowHandle, "VisitNumber"))) ? 0 : Convert.ToInt32(view5.GetRowCellValue(view5.FocusedRowHandle, "VisitNumber")));
                strSelectedClientName = (String.IsNullOrEmpty(Convert.ToString(view1.GetRowCellValue(view1.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view1.GetRowCellValue(view1.FocusedRowHandle, "ClientName")));
                strSelectedSiteName = (String.IsNullOrEmpty(Convert.ToString(view2.GetRowCellValue(view2.FocusedRowHandle, "SiteName"))) ? "Unknown Site" : Convert.ToString(view2.GetRowCellValue(view2.FocusedRowHandle, "SiteName")));
                strSelectedChemicalName = (String.IsNullOrEmpty(Convert.ToString(view5.GetRowCellValue(view5.FocusedRowHandle, "ChemicalName"))) ? "" : Convert.ToString(view5.GetRowCellValue(view5.FocusedRowHandle, "ChemicalName")));
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }



    }
}


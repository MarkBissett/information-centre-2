using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Labour_Reassign : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;

        public string _PassedInJobIDs = "";  // Used to hold IDs when form opened from another form in drill-down mode //
        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;

        bool iBool_AllowDelete = true;
        bool iBool_AllowAdd = true;
        bool iBool_AllowEdit = true;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateJobs;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateLabour;  // Used by Grid View State Facilities //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDsJobs = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsLabour = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        #endregion

        public frm_OM_Job_Labour_Reassign()
        {
            InitializeComponent();
        }

        private void frm_OM_Job_Labour_Reassign_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 500205;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(this.dataSet_AT.sp00039GetFormPermissionsForUser, this.FormID, this.GlobalSettings.UserID, 0, this.GlobalSettings.ViewedPeriodID);  // 1 = Staff //
            ProcessPermissionsForForm();

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06269_OM_Job_Labour_Reassign_Available_JobsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateJobs = new RefreshGridState(gridViewJob, "JobID");
            
            sp06270_OM_Job_Labour_Reassign_Linked_LabourTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateLabour = new RefreshGridState(gridViewLabour, "LabourUsedID");

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            
            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
            Application.DoEvents();  // Allow Form time to repaint itself //

            if (splitContainerControl1.Horizontal)
            {
                int intPosition = (splitContainerControl1.Width / 2) - 5;
                splitContainerControl1.SplitterPosition = intPosition;
            }
            else
            {
                int intPosition = (splitContainerControl1.Height / 2) - 5;
                splitContainerControl1.SplitterPosition = intPosition;
            }
            
            LoadLastSavedUserScreenSettings();
            Load_Data();  // Load records //

        }

        private void frm_OM_Job_Labour_Reassign_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsJobs))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDsLabour))
                {
                    LoadLinkedRecords();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Job_Labour_Reassign_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_PassedInJobIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ActivePatterns", beiShowActiveOnly.EditValue.ToString());
                    //default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                // Show Active Patterns //
                /*string strFilter = default_screen_settings.RetrieveSetting("ActivePatterns");
                if (!(string.IsNullOrEmpty(strFilter)))
                {
                    beiShowActiveOnly.EditValue = (strFilter == "0" ? 0 : 1);
                }*/

                Load_Data();
            }
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    i_str_AddedRecordIDsJobs = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsJobs : newIds);
                    break;
                case Utils.enmFocusedGrid.Labour:
                    i_str_AddedRecordIDsLabour = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsLabour : newIds);
                    break;
                default:
                    break;
            }
        }

        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:    // Whole Form //
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    view = (GridView)gridControlJob.MainView;
                    break;
                case Utils.enmFocusedGrid.Labour:
                    view = (GridView)gridControlLabour.MainView;
                    break;
            }
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();

            if (_enmFocusedGrid == Utils.enmFocusedGrid.Labour)
            {
                if (iBool_AllowAdd)
                {
                    alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                    bsiAdd.Enabled = true;
                    bbiSingleAdd.Enabled = true;
                }
                bbiBlockAdd.Enabled = false;
                if (iBool_AllowEdit && intRowHandles.Length >= 1)
                {
                    alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                    bsiEdit.Enabled = true;
                    bbiSingleEdit.Enabled = true;
                    if (intRowHandles.Length >= 2)
                    {
                        alItems.Add("iBlockEdit");
                        bbiBlockEdit.Enabled = true;
                    }
                }
                if (iBool_AllowDelete && intRowHandles.Length >= 1)
                {
                    alItems.Add("iDelete");
                    bbiDelete.Enabled = true;
                }
            }
            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of GridView1 navigator custom buttons //
            view = (GridView)gridControlJob.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlJob.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (intRowHandles.Length > 0);

            // Set enabled status of GridView8 navigator custom buttons //
            view = (GridView)gridControlLabour.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            bbiChangeTeam.Enabled = (intRowHandles.Length > 0);

        }


        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();

            GridView view = (GridView)gridControlJob.MainView;
            RefreshGridViewStateJobs.SaveViewInfo();
            view.BeginUpdate();
            sp06269_OM_Job_Labour_Reassign_Available_JobsTableAdapter.Fill(dataSet_OM_Job.sp06269_OM_Job_Labour_Reassign_Available_Jobs, _PassedInJobIDs);
            RefreshGridViewStateJobs.LoadViewInfo();  // Reload any expanded groups and selected rows //
            view.ExpandAllGroups();
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsJobs != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsJobs.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["JobID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsJobs = "";
            }

            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlJob.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["JobID"])) + ',';
            }

            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            // Populate Linked Records //
            gridControlLabour.MainView.BeginUpdate();
            this.RefreshGridViewStateLabour.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Job.sp06270_OM_Job_Labour_Reassign_Linked_Labour.Clear();
            }
            else // Load users selection //
            {
                sp06270_OM_Job_Labour_Reassign_Linked_LabourTableAdapter.Fill(dataSet_OM_Job.sp06270_OM_Job_Labour_Reassign_Linked_Labour, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                this.RefreshGridViewStateLabour.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlLabour.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsLabour != "")
            {
                strArray = i_str_AddedRecordIDsLabour.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlLabour.MainView;
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["LabourUsedID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsLabour = "";
            }
        }


        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            if (!iBool_AllowAdd) return;
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlJob.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Job.sp06269_OM_Job_Labour_Reassign_Available_JobsRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._ClientContractID = row.ClientContractID;
                            fChildForm._SiteID = row.SiteID;
                            fChildForm._SiteContractID = row.SiteContractID;
                            fChildForm._VisitID = row.VisitID;
                            fChildForm._VisitNumber = row.VisitNumber;
                            fChildForm._JobID = row.JobID;
                            fChildForm._JobTypeID = row.JobTypeID;
                            fChildForm._JobSubTypeID = row.JobSubTypeID;
                            fChildForm._ClientName = row.ClientName;
                            fChildForm._ContractDescription = row.ContractDescription;
                            fChildForm._SiteName = row.SiteName;
                            fChildForm._JobTypeDescription = row.JobTypeDescription;
                            fChildForm._JobSubTypeDescription = row.JobSubTypeDescription;
                            fChildForm._SiteLatitude = row.LocationX;
                            fChildForm._SiteLongitude = row.LocationY;
                            fChildForm._SitePostcode = row.SitePostcode;
                        }
                        else if (intRowHandles.Length > 1)  // Check if the same site or same job sub-type and if yes, pass through to edit form to help with the select Contractor screen for use with Job Competencies and Contractor Location //
                        {
                            int intSelectedJobSubTypeID = 0;
                            int intTempJobSubTypeID = 0;
                            bool boolOneJobSubTypeSelected = true;

                            int intSelectedSiteID = 0;
                            int intTempJobSiteID = 0;
                            bool boolOneSiteSelected = true;

                            foreach (int intRowHandle in intRowHandles)
                            {
                                intTempJobSubTypeID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                                if (intSelectedJobSubTypeID == 0)
                                {
                                    intSelectedJobSubTypeID = intTempJobSubTypeID;
                                }
                                else if (intSelectedJobSubTypeID != intTempJobSubTypeID)
                                {
                                    boolOneJobSubTypeSelected = false;
                                }
                                intTempJobSiteID = Convert.ToInt32(ParentView.GetRowCellValue(intRowHandle, "SiteID"));
                                if (intSelectedSiteID == 0)
                                {
                                    intSelectedSiteID = intTempJobSiteID;
                                }
                                else if (intSelectedSiteID != intTempJobSiteID)
                                {
                                    boolOneSiteSelected = false;
                                }
                            }
                            if (boolOneJobSubTypeSelected) fChildForm._JobSubTypeID = intSelectedJobSubTypeID;
                            if (boolOneSiteSelected) fChildForm._SiteID = intSelectedSiteID;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
             GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Job_Labour_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();

                        // Check if we have just one Job Sub-Type selected as we can pass this through to the select Contractor screen for use with Job Competencies //
                        int intSelectedJobSubTypeID = 0;
                        int intTempValue = 0;
                        bool boolOneJobSubTypeSelected = true;
                        string strSelectedSiteIDs = "";
                        string strTempValue2 = "";
                        bool boolOneSiteSelected = true;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            intTempValue = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobSubTypeID"));
                            strTempValue2 = view.GetRowCellValue(intRowHandle, "SiteID").ToString();
                            if (intSelectedJobSubTypeID <= 0)
                            {
                                intSelectedJobSubTypeID = intTempValue;
                            }
                            else if (intSelectedJobSubTypeID != intTempValue)
                            {
                                boolOneJobSubTypeSelected = false;
                            }

                            if (string.IsNullOrWhiteSpace(strSelectedSiteIDs))
                            {
                                strSelectedSiteIDs = strTempValue2;
                            }
                            else if (strSelectedSiteIDs != strTempValue2)
                            {
                                boolOneSiteSelected = false;
                            }
                        }
                        if (boolOneJobSubTypeSelected) fChildForm1._JobSubTypeID = intSelectedJobSubTypeID;

                        if (boolOneSiteSelected)
                        {
                            fChildForm1._SitePostcode = view.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString();
                            fChildForm1._SiteLatitude = Convert.ToDouble(view.GetRowCellValue(intRowHandles[0], "SiteLocationX"));
                            fChildForm1._SiteLongitude = Convert.ToDouble(view.GetRowCellValue(intRowHandles[0], "SiteLocationY"));
                        }

                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
            }
       }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }

                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            string strRecordIDs = "";
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlLabour.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Labour to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Labour" : Convert.ToString(intRowHandles.Length) + " Linked Labour") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Labour" : "these Linked Labour") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("job_labour_used", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Jobs:
                    {
                        view = (GridView)gridControlJob.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "JobID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "LabourUsedID")) + ',';
                        }
                        var fChildForm = new frm_OM_Job_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridViewJob":
                    message = "No Jobs - Adjust any filters";
                    break;
                case "gridViewLabour":
                    message = "No Linked Labour Available - Select one or more Jobs Records to view Linked Labour";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewJob":
                    LoadLinkedRecords();
                    view = (GridView)gridControlLabour.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView Job

        private void gridControlJob_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridViewJob;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Data();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewJob_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "LinkedLabourCount")
            {
                int intLinkedLabour = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedLabourCount"));
                if (intLinkedLabour <= 0)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewJob_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridViewJob_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewJob_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            SetMenuStatus();
        }

        private void gridViewJob_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }
            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }

        }

        private void gridViewJob_ShowingEditor(object sender, CancelEventArgs e)
        {
        }


        #endregion


        #region GridView Labour

        private void gridControlLabour_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case DevExpress.XtraEditors.NavigatorButtonType.Custom:
                    GridView view = gridViewJob;
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("reload".Equals(e.Button.Tag))
                    {
                        LoadLinkedRecords();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewLabour_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
        }

        private void gridViewLabour_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
        }

        private void gridViewLabour_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewLabour_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            SetMenuStatus();
        }

        private void gridViewLabour_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Load_Data();
        }

        private void bbiPivotLayout_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gridControlJob.BeginUpdate();
            gridControlLabour.BeginUpdate();
            if (splitContainerControl1.Horizontal)  // Make Verically split //
            {
                splitContainerControl1.Horizontal = false;
                int intPosition = (splitContainerControl1.Height / 2) - 5;
                splitContainerControl1.SplitterPosition = intPosition;
            }
            else  // Make Horizontally split //
            {
                splitContainerControl1.Horizontal = true;
                int intPosition = (splitContainerControl1.Width / 2) - 5;
                splitContainerControl1.SplitterPosition = intPosition;
            }
            gridControlJob.EndUpdate();
            gridControlLabour.EndUpdate();
        }

        private void bbiChangeTeam_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            int intRecordCount = 0;
            GridView view = (GridView)gridControlLabour.MainView;
            view.PostEditor();
            int[] intRowHandles;
            intRowHandles = view.GetSelectedRows();
            intRecordCount = intRowHandles.Length;
            if (intRowHandles.Length <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Job Labour records to Reassign then try again.", "Reassign Job Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Job_Wizard_Block_Edit_Labour_Info();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._FormMode = 1;  // Job Reassign //
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Reassigning...");

                char[] delimiters = new char[] { ',' };
                int intLabourUsedID = 0;
                int intContractorID = 0;
                int intJobID = 0;
                int intVisitID = 0;
                int intLinkedToPersonTypeID = 0;

                decimal decCostUnitsUsed = (decimal)0.00;
                int intCostUnitDescriptorID = 0;
                decimal decSellUnitsUsed = (decimal)0.00;
                int intSellUnitDescriptorID = 0;
                decimal decCostPerUnitExVat = (decimal)0.00;
                decimal decCostPerUnitVatRate = (decimal)0.00;
                decimal decSellPerUnitExVat = (decimal)0.00;
                decimal decSellPerUnitVatRate = (decimal)0.00;
                decimal decCISPercentage = (decimal)0.00;
                decimal decCISValue = (decimal)0.00;
                string strRemarks = "";

                decimal decCostTotalValueExVat = (decimal)0.00;
                decimal decCostTotalValueVat = (decimal)0.00;
                decimal decCostTotalValue = (decimal)0.00;
                decimal decSellTotalValueExVat = (decimal)0.00;
                decimal decSellTotalValueVat = (decimal)0.00;
                decimal decSellTotalValue = (decimal)0.00;

                intRowHandles = view.GetSelectedRows();
                intRecordCount = intRowHandles.Length;
                if (intRowHandles.Length <= 0) return;
                view.BeginUpdate();
                view.BeginSort();

                int intCount = 0;
                List<int> ContractorIDs = new List<int>();
                GoogleCloudMessaging GCM = new GoogleCloudMessaging();

                // Get list of Contractors //
                foreach (int intRowHandle in intRowHandles)
                {
                    if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LinkedToPersonTypeID")) == 1)  // Ignore any Staff Labour - just process contractor Labour //
                    {
                        intContractorID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ContractorID"));
                        if (!ContractorIDs.Contains(intContractorID)) ContractorIDs.Add(intContractorID);
                    }
                }

                List<int> VisitIDsToRecalculate = new List<int>();
                foreach (int ContractorID in ContractorIDs)
                {
                    // Get phones for Team //
                    string[] strPhoneIDs = GCM.GetPhoneIDs(strConnectionString, ContractorID.ToString() + ',');
                    if (strPhoneIDs.Length <= 0) continue;  // Abort and move to next team //

                    // Get list Of Visits for Team //
                    List<int> VisitIDs = new List<int>();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ContractorID")) == ContractorID)
                        {
                            intVisitID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitID"));
                            if (!VisitIDs.Contains(intVisitID)) VisitIDs.Add(intVisitID);
                            if (!VisitIDsToRecalculate.Contains(intVisitID)) VisitIDsToRecalculate.Add(intVisitID);
                        }
                    }
                    /*foreach (int VisitID in VisitIDs)
                    {

                        // Get comma separated list of Jobs for visit //
                        string strJobsID = "";
                        string strPDA_JobID = "";
                        foreach (int intRowHandle in intRowHandles)
                        {
                            if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitID")) == VisitID)
                            {
                                strJobsID += view.GetRowCellValue(intRowHandle, "JobID").ToString() + ",";
                                strPDA_JobID += view.GetRowCellValue(intRowHandle, "PDA_JobID").ToString() + ",";
                            }
                        }

                        // Now have a list of Jobs for the Visit for the Contractor - check if there are any jobs left on the visit for the contractor //
                        int intRemainingCount = 999;  // Best to be safe here and do a Job Clear instead of a Visit Clear if the subsequent call to GetRemainingRecords fails //
                        using (var GetRemainingRecords = new DataSet_OM_JobTableAdapters.QueriesTableAdapter())
                        {
                            GetRemainingRecords.ChangeConnectionString(strConnectionString);
                            try
                            {
                                intRemainingCount = Convert.ToInt32(GetRemainingRecords.sp06287_OM_Reassign_Jobs_Get_Job_Count_Left(ContractorID, VisitID, strJobsID));
                            }
                            catch (Exception) { }
                        }
                        string strMessage = "";
                        using (var GetMessage = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                        {
                            GetMessage.ChangeConnectionString(strConnectionString);
                            try
                            {
                                strMessage = GetMessage.sp06289_OM_Get_Description_For_Messaging(1, VisitID).ToString();
                            }
                            catch (Exception) { }
                        }
                        if (intRecordCount > 0)
                        {

                            GCM.SendUsingGoogleCloudMessaging(strPhoneIDs, "Job(s) Reallocated", strMessage, 3001, 1015, strPDA_JobID, strConnectionString);  // Job Clear //                   
                        }
                        else
                        {
                            GCM.SendUsingGoogleCloudMessaging(strPhoneIDs, "Visit Reallocated", strMessage, 3001, 1005, intVisitID.ToString() + ",", strConnectionString);  // Visit Clear //
                        }
                    }  */               
                }

                using (var UpdateLabour = new DataSet_OM_JobTableAdapters.QueriesTableAdapter())
                {
                    UpdateLabour.ChangeConnectionString(strConnectionString);                      
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intCount++;
                        intLabourUsedID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LabourUsedID"));
                        intContractorID = Convert.ToInt32(fChildForm.intContractorID);
                        intJobID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "JobID"));
                        intVisitID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "VisitID"));

                        decCostUnitsUsed = (fChildForm.decCostUnitsUsed != null ? (decimal)fChildForm.decCostUnitsUsed : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "CostUnitsUsed")));
                        intCostUnitDescriptorID = (fChildForm.intCostUnitDescriptorID != null ? (int)fChildForm.intCostUnitDescriptorID : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "CostUnitDescriptorID")));
                        decSellUnitsUsed = (fChildForm.decSellUnitsUsed != null ? (decimal)fChildForm.decSellUnitsUsed : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "SellUnitsUsed")));
                        intSellUnitDescriptorID = (fChildForm.intSellUnitDescriptorID != null ? (int)fChildForm.intSellUnitDescriptorID : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SellUnitDescriptorID")));

                        decCostPerUnitExVat = (fChildForm.decCostPerUnitExVat != null ? (decimal)fChildForm.decCostPerUnitExVat : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "CostPerUnitExVat")));
                        decCostPerUnitVatRate = (fChildForm.decCostPerUnitVatRate != null ? (decimal)fChildForm.decCostPerUnitVatRate : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "CostPerUnitVatRate")));
                        decSellPerUnitExVat = (fChildForm.decSellPerUnitExVat != null ? (decimal)fChildForm.decSellPerUnitExVat : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "SellPerUnitExVat")));
                        decSellPerUnitVatRate = (fChildForm.decSellPerUnitVatRate != null ? (decimal)fChildForm.decSellPerUnitVatRate : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "SellPerUnitVatRate")));
                        decCISPercentage = (fChildForm.decCISPercentage != null ? (decimal)fChildForm.decCISPercentage : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "CISPercentage")));
                        decCISValue = (fChildForm.decCISValue != null ? (decimal)fChildForm.decCISValue : Convert.ToDecimal(view.GetRowCellValue(intRowHandle, "CISValue")));
                        strRemarks = (fChildForm.strRemarks != null ? fChildForm.strRemarks : Convert.ToString(view.GetRowCellValue(intRowHandle, "Remarks")));
                        intLinkedToPersonTypeID = (fChildForm.intLinkedToPersonTypeID != null ? Convert.ToInt32(fChildForm.intLinkedToPersonTypeID) : Convert.ToInt32(view.GetRowCellValue(intRowHandle, "LinkedToPersonTypeID")));

                        decCostTotalValueExVat = decCostUnitsUsed * decCostPerUnitExVat;
                        decCostTotalValueVat = (decCostTotalValueExVat * decCostPerUnitVatRate) / 100;
                        decCostTotalValue = decCostTotalValueExVat + decCostTotalValueVat;

                        decSellTotalValueExVat = decSellUnitsUsed * decSellPerUnitExVat;
                        decSellTotalValueVat = (decSellTotalValueExVat * decSellPerUnitVatRate) / 100;
                        decSellTotalValue = decSellTotalValueExVat + decSellTotalValueVat;
                        

                        try
                        {
                            UpdateLabour.sp06271_OM_Job_Labour_Update(intLabourUsedID,
                                                                        intContractorID, 
                                                                        intJobID, 
                                                                        decCostUnitsUsed, 
                                                                        intCostUnitDescriptorID, 
                                                                        decSellUnitsUsed, 
                                                                        intSellUnitDescriptorID, 
                                                                        decCostPerUnitExVat, 
                                                                        decCostPerUnitVatRate, 
                                                                        decSellPerUnitExVat, 
                                                                        decSellPerUnitVatRate, 
                                                                        decCostTotalValueExVat, 
                                                                        decCostTotalValueVat, 
                                                                        decCostTotalValue, 
                                                                        decSellTotalValueExVat, 
                                                                        decSellTotalValueVat, 
                                                                        decSellTotalValue, 
                                                                        decCISPercentage, 
                                                                        decCISValue,
                                                                        strRemarks,
                                                                        intLinkedToPersonTypeID);
                        }
                        catch (Exception) { }
                    }
                }

                // Recalculate Visit Labour and Operative Timings //
                foreach (int VisitID in VisitIDsToRecalculate)
                {
                    using (var UpdateRecords = new DataSet_OM_VisitTableAdapters.QueriesTableAdapter())
                    {
                        UpdateRecords.ChangeConnectionString(strConnectionString);
                        try
                        {
                            UpdateRecords.sp06902_OM_UTILITY_Recreate_Visit_Labour(VisitID);
                            UpdateRecords.sp06903_OM_UTILITY_Recreate_Visit_Operative_Timing(VisitID);
                        }
                        catch (Exception) { }
                    }
                }

                LoadLinkedRecords();
                view.EndSort();
                view.EndUpdate();
                if (this.ParentForm != null)
                {
                    foreach (Form frmChild in this.ParentForm.MdiChildren)
                    {
                        if (frmChild.Name == "frm_OM_Job_Manager")
                        {
                            var fParentForm = (frm_OM_Job_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(2, Utils.enmFocusedGrid.Labour, "");
                        }
                        if (frmChild.Name == "frm_OM_Visit_Manager")
                        {
                            var fParentForm = (frm_OM_Visit_Manager)frmChild;
                            fParentForm.UpdateFormRefreshStatus(50, Utils.enmFocusedGrid.Labour, "");
                        }
                    }
                }
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " Job Labour Record(s) Reassigned.", "Reassign Job Labour", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
         


 

    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Visit_Reschedule_Subsequent_Visits : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public DateTime? dtVisitStartDate = null;
        public DateTime? dtVisitEndDate = null;
   
        #endregion

        public frm_OM_Visit_Reschedule_Subsequent_Visits()
        {
            InitializeComponent();
        }

        private void frm_OM_Visit_Reschedule_Subsequent_Visits_Load(object sender, EventArgs e)
        {
            this.FormID = 500203;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            dateEditVisitStartDate.EditValue = null;
            dateEditVisitEndDate.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }
       
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dateEditVisitStartDate.EditValue == null || dateEditVisitEndDate.EditValue == null)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Enter a date\\time in both date boxes them before proceeding.", "Reschedule Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (dateEditVisitStartDate.EditValue != null) dtVisitStartDate = Convert.ToDateTime(dateEditVisitStartDate.EditValue);
            if (dateEditVisitEndDate.EditValue != null) dtVisitEndDate = Convert.ToDateTime(dateEditVisitEndDate.EditValue);
            
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }





    }
}

﻿namespace WoodPlan5
{
    partial class frm_OM_Site_Contract_Off_Hold_Wizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_OM_Site_Contract_Off_Hold_Wizard));
            DevExpress.XtraGrid.GridFormatRule gridFormatRule1 = new DevExpress.XtraGrid.GridFormatRule();
            DevExpress.XtraEditors.FormatConditionRuleValue formatConditionRuleValue1 = new DevExpress.XtraEditors.FormatConditionRuleValue();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions1 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.Animation.Transition transition1 = new DevExpress.Utils.Animation.Transition();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageWelcome = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.btnWelcomeNext = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.xtraTabPageStep1 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet_OM_Contract = new WoodPlan5.DataSet_OM_Contract();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colSiteContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colContractValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditCurrency = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colYearlyPercentageIncrease = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditPercentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colClientID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colOnHold = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReasonID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitIDs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnHoldReason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClearOnHoldData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.repositoryItemGridLookUpEditOnHoldReasonID = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl4 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.btnStep1Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep1Previous = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPageStep2 = new DevExpress.XtraTab.XtraTabPage();
            this.labelControlSelectedVisitCount = new DevExpress.XtraEditors.LabelControl();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.sp06411OMSiteContractWizardOffHoldVisitsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisitID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpectedStartDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditDateTime4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colExpectedEndDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRemarks1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemMemoExEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.colCostCalculationLevelDescription = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreatedByStaffName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientContractID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSiteName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContractDescription1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLinkedToParent1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatusID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisitStatus = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colJobCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEditInteger4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.btnStep2Next = new DevExpress.XtraEditors.SimpleButton();
            this.btnStep2Previous = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabPageFinish = new DevExpress.XtraTab.XtraTabPage();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnFinish = new DevExpress.XtraEditors.SimpleButton();
            this.btnFinishPrevious = new DevExpress.XtraEditors.SimpleButton();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.repositoryItemMemoExEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemGridLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemMemoExEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.repositoryItemButtonEditChooseStaff = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.dataSet_AT = new WoodPlan5.DataSet_AT();
            this.sp00039GetFormPermissionsForUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sp00039GetFormPermissionsForUserTableAdapter = new WoodPlan5.DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.repositoryItemDuration1 = new DevExpress.XtraScheduler.UI.RepositoryItemDuration();
            this.bsiRecordTicking = new DevExpress.XtraBars.BarSubItem();
            this.bbiTick = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUntick = new DevExpress.XtraBars.BarButtonItem();
            this.transitionManager1 = new DevExpress.Utils.Animation.TransitionManager(this.components);
            this.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter();
            this.sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter = new WoodPlan5.DataSet_OM_ContractTableAdapters.sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPageWelcome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.xtraTabPageStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditOnHoldReasonID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).BeginInit();
            this.panelControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            this.xtraTabPageStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06411OMSiteContractWizardOffHoldVisitsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            this.xtraTabPageFinish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            this.SuspendLayout();
            // 
            // pmDataContextMenu
            // 
            this.pmDataContextMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiRecordTicking, true)});
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.BackColor2 = System.Drawing.Color.White;
            this.pmDataContextMenu.MenuAppearance.SideStrip.Options.UseBackColor = true;
            // 
            // bsiAdd
            // 
            this.bsiAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAdd.ImageOptions.Image")));
            // 
            // bbiSingleAdd
            // 
            this.bbiSingleAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleAdd.ImageOptions.Image")));
            // 
            // bbiBlockAdd
            // 
            this.bbiBlockAdd.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockAdd.ImageOptions.Image")));
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Size = new System.Drawing.Size(1178, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Size = new System.Drawing.Size(1178, 0);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Location = new System.Drawing.Point(1178, 0);
            // 
            // bsiEdit
            // 
            this.bsiEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiEdit.ImageOptions.Image")));
            // 
            // bbiSingleEdit
            // 
            this.bbiSingleEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSingleEdit.ImageOptions.Image")));
            // 
            // bbiBlockEdit
            // 
            this.bbiBlockEdit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiBlockEdit.ImageOptions.Image")));
            // 
            // bbiDelete
            // 
            this.bbiDelete.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDelete.ImageOptions.Image")));
            // 
            // bbiSave
            // 
            this.bbiSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSave.ImageOptions.Image")));
            // 
            // bbiUndo
            // 
            this.bbiUndo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiUndo.ImageOptions.Image")));
            // 
            // bbiRedo
            // 
            this.bbiRedo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiRedo.ImageOptions.Image")));
            // 
            // bbiCut
            // 
            this.bbiCut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCut.ImageOptions.Image")));
            // 
            // bbiCopy
            // 
            this.bbiCopy.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopy.ImageOptions.Image")));
            // 
            // bbiPaste
            // 
            this.bbiPaste.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiPaste.ImageOptions.Image")));
            // 
            // bbiSelectAll
            // 
            this.bbiSelectAll.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSelectAll.ImageOptions.Image")));
            // 
            // bbiClear
            // 
            this.bbiClear.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiClear.ImageOptions.Image")));
            // 
            // bbiSpellChecker
            // 
            this.bbiSpellChecker.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiSpellChecker.ImageOptions.Image")));
            // 
            // scSpellChecker
            // 
            this.scSpellChecker.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.scSpellChecker.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            // 
            // bbiGrammarCheck
            // 
            this.bbiGrammarCheck.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiGrammarCheck.ImageOptions.Image")));
            // 
            // bsiDataset
            // 
            this.bsiDataset.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.Image")));
            this.bsiDataset.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bsiDataset.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelection
            // 
            this.bbiDatasetSelection.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.Image")));
            this.bbiDatasetSelection.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelection.ImageOptions.LargeImage")));
            // 
            // bbiDatasetCreate
            // 
            this.bbiDatasetCreate.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.Image")));
            this.bbiDatasetCreate.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetCreate.ImageOptions.LargeImage")));
            // 
            // bbiDatasetManager
            // 
            this.bbiDatasetManager.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.Image")));
            this.bbiDatasetManager.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiDatasetManager.ImageOptions.LargeImage")));
            // 
            // bbiDatasetSelectionInverted
            // 
            this.bbiDatasetSelectionInverted.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiDatasetSelectionInverted.ImageOptions.Image")));
            // 
            // bbiShowMap
            // 
            this.bbiShowMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowMap.ImageOptions.Image")));
            // 
            // bbiShowGoogleMap
            // 
            this.bbiShowGoogleMap.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiShowGoogleMap.ImageOptions.Image")));
            // 
            // barManager1
            // 
            this.barManager1.Images = this.imageCollection2;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bsiRecordTicking,
            this.bbiTick,
            this.bbiUntick});
            this.barManager1.MaxItemId = 96;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit4,
            this.repositoryItemSpinEdit1,
            this.repositoryItemDuration1});
            // 
            // bsiAuditTrail
            // 
            this.bsiAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bsiAuditTrail.ImageOptions.Image")));
            // 
            // bbiViewAuditTrail
            // 
            this.bbiViewAuditTrail.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiViewAuditTrail.ImageOptions.Image")));
            // 
            // bbiCopyToClipboard
            // 
            this.bbiCopyToClipboard.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("bbiCopyToClipboard.ImageOptions.Image")));
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "ID";
            this.gridColumn10.FieldName = "ID";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.AllowEdit = false;
            this.gridColumn10.OptionsColumn.AllowFocus = false;
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.Width = 53;
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.xtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, -1);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            this.xtraTabControl1.Size = new System.Drawing.Size(1182, 542);
            this.xtraTabControl1.TabIndex = 6;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageWelcome,
            this.xtraTabPageStep1,
            this.xtraTabPageStep2,
            this.xtraTabPageFinish});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControl1_SelectedPageChanging);
            // 
            // xtraTabPageWelcome
            // 
            this.xtraTabPageWelcome.Controls.Add(this.panelControl1);
            this.xtraTabPageWelcome.Controls.Add(this.btnWelcomeNext);
            this.xtraTabPageWelcome.Controls.Add(this.labelControl2);
            this.xtraTabPageWelcome.Controls.Add(this.pictureEdit1);
            this.xtraTabPageWelcome.Name = "xtraTabPageWelcome";
            this.xtraTabPageWelcome.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageWelcome.Tag = "0";
            this.xtraTabPageWelcome.Text = "Welcome";
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pictureEdit4);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl9);
            this.panelControl1.Location = new System.Drawing.Point(211, 6);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(938, 48);
            this.panelControl1.TabIndex = 17;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit4.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit4.Location = new System.Drawing.Point(894, 4);
            this.pictureEdit4.MenuManager = this.barManager1;
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit4.Properties.ReadOnly = true;
            this.pictureEdit4.Properties.ShowMenu = false;
            this.pictureEdit4.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit4.TabIndex = 16;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl1.Appearance.Options.UseBackColor = true;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(5, 1);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(531, 29);
            this.labelControl1.TabIndex = 5;
            this.labelControl1.Text = "Welcome to the <b>Site Contract Off-Hold Wizard</b>";
            // 
            // labelControl9
            // 
            this.labelControl9.AllowHtmlString = true;
            this.labelControl9.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl9.Appearance.Options.UseBackColor = true;
            this.labelControl9.Location = new System.Drawing.Point(9, 31);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(524, 13);
            this.labelControl9.TabIndex = 15;
            this.labelControl9.Text = "This wizard will allow you to set one or more Site Contracts (and optionally link" +
    "ed Visits and Jobs) as Off-Hold.";
            // 
            // btnWelcomeNext
            // 
            this.btnWelcomeNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWelcomeNext.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnWelcomeNext.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnWelcomeNext.Location = new System.Drawing.Point(1061, 501);
            this.btnWelcomeNext.Name = "btnWelcomeNext";
            this.btnWelcomeNext.Size = new System.Drawing.Size(88, 30);
            this.btnWelcomeNext.TabIndex = 7;
            this.btnWelcomeNext.Text = "Next";
            this.btnWelcomeNext.Click += new System.EventHandler(this.btnWelcomeNext_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(220, 104);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(220, 25);
            this.labelControl2.TabIndex = 6;
            this.labelControl2.Text = "Click <b>Next</b> To Continue";
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit1.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit1.EditValue = global::WoodPlan5.Properties.Resources.wizard_image;
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.MenuManager = this.barManager1;
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.ShowMenu = false;
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(205, 537);
            this.pictureEdit1.TabIndex = 4;
            // 
            // xtraTabPageStep1
            // 
            this.xtraTabPageStep1.Controls.Add(this.gridControl2);
            this.xtraTabPageStep1.Controls.Add(this.panelControl4);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Next);
            this.xtraTabPageStep1.Controls.Add(this.btnStep1Previous);
            this.xtraTabPageStep1.Name = "xtraTabPageStep1";
            this.xtraTabPageStep1.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageStep1.Tag = "1";
            this.xtraTabPageStep1.Text = "Step 1";
            // 
            // gridControl2
            // 
            this.gridControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl2.DataSource = this.sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl2.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl2.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl2.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton(-1, 7, true, true, "Block Edit selected Record(s)", "block edit")});
            this.gridControl2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridControl2_EmbeddedNavigator_ButtonClick);
            this.gridControl2.Location = new System.Drawing.Point(7, 62);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.MenuManager = this.barManager1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit1,
            this.repositoryItemCheckEdit1,
            this.repositoryItemDateEdit1,
            this.repositoryItemGridLookUpEditOnHoldReasonID,
            this.repositoryItemTextEditDateTime,
            this.repositoryItemTextEditCurrency,
            this.repositoryItemTextEditPercentage});
            this.gridControl2.Size = new System.Drawing.Size(1142, 433);
            this.gridControl2.TabIndex = 16;
            this.gridControl2.UseEmbeddedNavigator = true;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource
            // 
            this.sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource.DataMember = "sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_Contracts";
            this.sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // dataSet_OM_Contract
            // 
            this.dataSet_OM_Contract.DataSetName = "DataSet_OM_Contract";
            this.dataSet_OM_Contract.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Add_16x16, "Add_16x16", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection1.Images.SetKeyName(0, "Add_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Edit_16x16, "Edit_16x16", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection1.Images.SetKeyName(1, "Edit_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Delete_16x16, "Delete_16x16", typeof(global::WoodPlan5.Properties.Resources), 2);
            this.imageCollection1.Images.SetKeyName(2, "Delete_16x16");
            this.imageCollection1.Images.SetKeyName(3, "arrow_up_blue_round.png");
            this.imageCollection1.Images.SetKeyName(4, "arrow_down_blue_round.png");
            this.imageCollection1.Images.SetKeyName(5, "Sort_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.refresh_16x16, "refresh_16x16", typeof(global::WoodPlan5.Properties.Resources), 6);
            this.imageCollection1.Images.SetKeyName(6, "refresh_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.BlockEdit_16x16, "BlockEdit_16x16", typeof(global::WoodPlan5.Properties.Resources), 7);
            this.imageCollection1.Images.SetKeyName(7, "BlockEdit_16x16");
            this.imageCollection1.InsertGalleryImage("preview_16x16.png", "images/print/preview_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/print/preview_16x16.png"), 8);
            this.imageCollection1.Images.SetKeyName(8, "preview_16x16.png");
            this.imageCollection1.InsertGalleryImage("groupheader_16x16.png", "images/reports/groupheader_16x16.png", DevExpress.Images.ImageResourceCache.Default.GetImage("images/reports/groupheader_16x16.png"), 9);
            this.imageCollection1.Images.SetKeyName(9, "groupheader_16x16.png");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.Info_16x16, "Info_16x16", typeof(global::WoodPlan5.Properties.Resources), 10);
            this.imageCollection1.Images.SetKeyName(10, "Info_16x16");
            this.imageCollection1.InsertImage(global::WoodPlan5.Properties.Resources.attention_16, "attention_16", typeof(global::WoodPlan5.Properties.Resources), 11);
            this.imageCollection1.Images.SetKeyName(11, "attention_16");
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colSiteContractID,
            this.colClientContractID,
            this.colSiteID,
            this.colStartDate,
            this.colEndDate,
            this.colActive,
            this.colContractValue,
            this.colYearlyPercentageIncrease,
            this.colClientID,
            this.colSiteName,
            this.colLinkedToParent,
            this.colContractDescription,
            this.colVisitCount,
            this.colRemarks,
            this.colOnHold,
            this.colOnHoldReasonID,
            this.colOnHoldStartDate,
            this.colOnHoldEndDate,
            this.colVisitIDs,
            this.colOnHoldReason,
            this.colClearOnHoldData});
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.GroupCount = 1;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsFind.AlwaysVisible = true;
            this.gridView2.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView2.OptionsLayout.StoreAppearance = true;
            this.gridView2.OptionsLayout.StoreFormatRules = true;
            this.gridView2.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView2.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView2.OptionsSelection.MultiSelect = true;
            this.gridView2.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView2.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView2.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView2.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView2.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView2_MouseUp);
            this.gridView2.GotFocus += new System.EventHandler(this.gridView2_GotFocus);
            // 
            // colSiteContractID
            // 
            this.colSiteContractID.Caption = "Site Contract ID";
            this.colSiteContractID.FieldName = "SiteContractID";
            this.colSiteContractID.Name = "colSiteContractID";
            this.colSiteContractID.OptionsColumn.AllowEdit = false;
            this.colSiteContractID.OptionsColumn.AllowFocus = false;
            this.colSiteContractID.OptionsColumn.ReadOnly = true;
            this.colSiteContractID.Width = 96;
            // 
            // colClientContractID
            // 
            this.colClientContractID.Caption = "Client Contract ID";
            this.colClientContractID.FieldName = "ClientContractID";
            this.colClientContractID.Name = "colClientContractID";
            this.colClientContractID.OptionsColumn.AllowEdit = false;
            this.colClientContractID.OptionsColumn.AllowFocus = false;
            this.colClientContractID.OptionsColumn.ReadOnly = true;
            this.colClientContractID.Width = 105;
            // 
            // colSiteID
            // 
            this.colSiteID.Caption = "Site ID";
            this.colSiteID.FieldName = "SiteID";
            this.colSiteID.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteID.Name = "colSiteID";
            this.colSiteID.OptionsColumn.AllowEdit = false;
            this.colSiteID.OptionsColumn.AllowFocus = false;
            this.colSiteID.OptionsColumn.ReadOnly = true;
            this.colSiteID.Visible = true;
            this.colSiteID.VisibleIndex = 0;
            this.colSiteID.Width = 55;
            // 
            // colStartDate
            // 
            this.colStartDate.Caption = "Contract Start Date";
            this.colStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colStartDate.FieldName = "StartDate";
            this.colStartDate.Name = "colStartDate";
            this.colStartDate.OptionsColumn.AllowEdit = false;
            this.colStartDate.OptionsColumn.AllowFocus = false;
            this.colStartDate.OptionsColumn.ReadOnly = true;
            this.colStartDate.Width = 114;
            // 
            // repositoryItemTextEditDateTime
            // 
            this.repositoryItemTextEditDateTime.AutoHeight = false;
            this.repositoryItemTextEditDateTime.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime.Name = "repositoryItemTextEditDateTime";
            // 
            // colEndDate
            // 
            this.colEndDate.Caption = "Contract End Date";
            this.colEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colEndDate.FieldName = "EndDate";
            this.colEndDate.Name = "colEndDate";
            this.colEndDate.OptionsColumn.AllowEdit = false;
            this.colEndDate.OptionsColumn.AllowFocus = false;
            this.colEndDate.OptionsColumn.ReadOnly = true;
            this.colEndDate.Width = 108;
            // 
            // colActive
            // 
            this.colActive.Caption = "Active";
            this.colActive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colActive.FieldName = "Active";
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.AllowEdit = false;
            this.colActive.OptionsColumn.AllowFocus = false;
            this.colActive.OptionsColumn.ReadOnly = true;
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 7;
            this.colActive.Width = 49;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.ValueChecked = 1;
            this.repositoryItemCheckEdit1.ValueUnchecked = 0;
            // 
            // colContractValue
            // 
            this.colContractValue.Caption = "Contract Value";
            this.colContractValue.ColumnEdit = this.repositoryItemTextEditCurrency;
            this.colContractValue.FieldName = "ContractValue";
            this.colContractValue.Name = "colContractValue";
            this.colContractValue.OptionsColumn.AllowEdit = false;
            this.colContractValue.OptionsColumn.AllowFocus = false;
            this.colContractValue.OptionsColumn.ReadOnly = true;
            this.colContractValue.Width = 90;
            // 
            // repositoryItemTextEditCurrency
            // 
            this.repositoryItemTextEditCurrency.AutoHeight = false;
            this.repositoryItemTextEditCurrency.Mask.EditMask = "c";
            this.repositoryItemTextEditCurrency.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditCurrency.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditCurrency.Name = "repositoryItemTextEditCurrency";
            // 
            // colYearlyPercentageIncrease
            // 
            this.colYearlyPercentageIncrease.Caption = "Yearly Percentage Increase";
            this.colYearlyPercentageIncrease.ColumnEdit = this.repositoryItemTextEditPercentage;
            this.colYearlyPercentageIncrease.FieldName = "YearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.Name = "colYearlyPercentageIncrease";
            this.colYearlyPercentageIncrease.OptionsColumn.AllowEdit = false;
            this.colYearlyPercentageIncrease.OptionsColumn.AllowFocus = false;
            this.colYearlyPercentageIncrease.OptionsColumn.ReadOnly = true;
            this.colYearlyPercentageIncrease.Width = 152;
            // 
            // repositoryItemTextEditPercentage
            // 
            this.repositoryItemTextEditPercentage.AutoHeight = false;
            this.repositoryItemTextEditPercentage.Mask.EditMask = "P";
            this.repositoryItemTextEditPercentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditPercentage.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditPercentage.Name = "repositoryItemTextEditPercentage";
            // 
            // colClientID
            // 
            this.colClientID.Caption = "Client ID";
            this.colClientID.FieldName = "ClientID";
            this.colClientID.Name = "colClientID";
            this.colClientID.OptionsColumn.AllowEdit = false;
            this.colClientID.OptionsColumn.AllowFocus = false;
            this.colClientID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName
            // 
            this.colSiteName.Caption = "Site Name";
            this.colSiteName.FieldName = "SiteName";
            this.colSiteName.Fixed = DevExpress.XtraGrid.Columns.FixedStyle.Left;
            this.colSiteName.Name = "colSiteName";
            this.colSiteName.OptionsColumn.AllowEdit = false;
            this.colSiteName.OptionsColumn.AllowFocus = false;
            this.colSiteName.OptionsColumn.ReadOnly = true;
            this.colSiteName.Visible = true;
            this.colSiteName.VisibleIndex = 1;
            this.colSiteName.Width = 213;
            // 
            // colLinkedToParent
            // 
            this.colLinkedToParent.Caption = "Client Contract";
            this.colLinkedToParent.FieldName = "LinkedToParent";
            this.colLinkedToParent.Name = "colLinkedToParent";
            this.colLinkedToParent.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent.Visible = true;
            this.colLinkedToParent.VisibleIndex = 8;
            this.colLinkedToParent.Width = 355;
            // 
            // colContractDescription
            // 
            this.colContractDescription.Caption = "Contract Description";
            this.colContractDescription.FieldName = "ContractDescription";
            this.colContractDescription.Name = "colContractDescription";
            this.colContractDescription.OptionsColumn.AllowEdit = false;
            this.colContractDescription.OptionsColumn.AllowFocus = false;
            this.colContractDescription.OptionsColumn.ReadOnly = true;
            this.colContractDescription.Width = 252;
            // 
            // colVisitCount
            // 
            this.colVisitCount.Caption = "Linked Visits";
            this.colVisitCount.FieldName = "VisitCount";
            this.colVisitCount.Name = "colVisitCount";
            this.colVisitCount.OptionsColumn.AllowEdit = false;
            this.colVisitCount.OptionsColumn.AllowFocus = false;
            this.colVisitCount.OptionsColumn.ReadOnly = true;
            this.colVisitCount.Visible = true;
            this.colVisitCount.VisibleIndex = 8;
            this.colVisitCount.Width = 76;
            // 
            // colRemarks
            // 
            this.colRemarks.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colRemarks.AppearanceCell.Options.UseBackColor = true;
            this.colRemarks.Caption = "Remarks";
            this.colRemarks.ColumnEdit = this.repositoryItemMemoExEdit1;
            this.colRemarks.FieldName = "Remarks";
            this.colRemarks.Name = "colRemarks";
            this.colRemarks.Visible = true;
            this.colRemarks.VisibleIndex = 6;
            this.colRemarks.Width = 132;
            // 
            // repositoryItemMemoExEdit1
            // 
            this.repositoryItemMemoExEdit1.AutoHeight = false;
            this.repositoryItemMemoExEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit1.Name = "repositoryItemMemoExEdit1";
            this.repositoryItemMemoExEdit1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit1.ShowIcon = false;
            // 
            // colOnHold
            // 
            this.colOnHold.Caption = "On-Hold";
            this.colOnHold.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colOnHold.FieldName = "OnHold";
            this.colOnHold.Name = "colOnHold";
            this.colOnHold.OptionsColumn.AllowEdit = false;
            this.colOnHold.OptionsColumn.AllowFocus = false;
            this.colOnHold.OptionsColumn.ReadOnly = true;
            this.colOnHold.Width = 58;
            // 
            // colOnHoldReasonID
            // 
            this.colOnHoldReasonID.Caption = "On-Hold Reason ID";
            this.colOnHoldReasonID.FieldName = "OnHoldReasonID";
            this.colOnHoldReasonID.Name = "colOnHoldReasonID";
            this.colOnHoldReasonID.OptionsColumn.AllowEdit = false;
            this.colOnHoldReasonID.OptionsColumn.AllowFocus = false;
            this.colOnHoldReasonID.OptionsColumn.ReadOnly = true;
            this.colOnHoldReasonID.Width = 182;
            // 
            // colOnHoldStartDate
            // 
            this.colOnHoldStartDate.Caption = "On-Hold Start";
            this.colOnHoldStartDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colOnHoldStartDate.FieldName = "OnHoldStartDate";
            this.colOnHoldStartDate.Name = "colOnHoldStartDate";
            this.colOnHoldStartDate.OptionsColumn.AllowEdit = false;
            this.colOnHoldStartDate.OptionsColumn.AllowFocus = false;
            this.colOnHoldStartDate.OptionsColumn.ReadOnly = true;
            this.colOnHoldStartDate.Visible = true;
            this.colOnHoldStartDate.VisibleIndex = 3;
            this.colOnHoldStartDate.Width = 110;
            // 
            // colOnHoldEndDate
            // 
            this.colOnHoldEndDate.Caption = "On-Hold End";
            this.colOnHoldEndDate.ColumnEdit = this.repositoryItemTextEditDateTime;
            this.colOnHoldEndDate.FieldName = "OnHoldEndDate";
            this.colOnHoldEndDate.Name = "colOnHoldEndDate";
            this.colOnHoldEndDate.OptionsColumn.AllowEdit = false;
            this.colOnHoldEndDate.OptionsColumn.AllowFocus = false;
            this.colOnHoldEndDate.OptionsColumn.ReadOnly = true;
            this.colOnHoldEndDate.Visible = true;
            this.colOnHoldEndDate.VisibleIndex = 4;
            this.colOnHoldEndDate.Width = 110;
            // 
            // colVisitIDs
            // 
            this.colVisitIDs.Caption = "Visit IDs";
            this.colVisitIDs.FieldName = "VisitIDs";
            this.colVisitIDs.Name = "colVisitIDs";
            this.colVisitIDs.OptionsColumn.AllowEdit = false;
            this.colVisitIDs.OptionsColumn.AllowFocus = false;
            this.colVisitIDs.OptionsColumn.ReadOnly = true;
            this.colVisitIDs.Width = 181;
            // 
            // colOnHoldReason
            // 
            this.colOnHoldReason.FieldName = "OnHoldReason";
            this.colOnHoldReason.Name = "colOnHoldReason";
            this.colOnHoldReason.OptionsColumn.AllowEdit = false;
            this.colOnHoldReason.OptionsColumn.AllowFocus = false;
            this.colOnHoldReason.OptionsColumn.ReadOnly = true;
            this.colOnHoldReason.Visible = true;
            this.colOnHoldReason.VisibleIndex = 2;
            this.colOnHoldReason.Width = 152;
            // 
            // colClearOnHoldData
            // 
            this.colClearOnHoldData.AppearanceCell.BackColor = System.Drawing.Color.OldLace;
            this.colClearOnHoldData.AppearanceCell.Options.UseBackColor = true;
            this.colClearOnHoldData.Caption = "Clear On-Hold Data";
            this.colClearOnHoldData.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colClearOnHoldData.FieldName = "ClearOnHoldData";
            this.colClearOnHoldData.Name = "colClearOnHoldData";
            this.colClearOnHoldData.Visible = true;
            this.colClearOnHoldData.VisibleIndex = 5;
            this.colClearOnHoldData.Width = 112;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Mask.EditMask = "g";
            this.repositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // repositoryItemGridLookUpEditOnHoldReasonID
            // 
            this.repositoryItemGridLookUpEditOnHoldReasonID.AutoHeight = false;
            this.repositoryItemGridLookUpEditOnHoldReasonID.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEditOnHoldReasonID.DataSource = this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource;
            this.repositoryItemGridLookUpEditOnHoldReasonID.DisplayMember = "Description";
            this.repositoryItemGridLookUpEditOnHoldReasonID.Name = "repositoryItemGridLookUpEditOnHoldReasonID";
            this.repositoryItemGridLookUpEditOnHoldReasonID.PopupView = this.repositoryItemGridLookUpEdit2View;
            this.repositoryItemGridLookUpEditOnHoldReasonID.ValueMember = "ID";
            // 
            // sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource
            // 
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource.DataMember = "sp06406_OM_Site_Contract_On_Hold_Reasons_With_Blank";
            this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // repositoryItemGridLookUpEdit2View
            // 
            this.repositoryItemGridLookUpEdit2View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12});
            this.repositoryItemGridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            gridFormatRule1.ApplyToRow = true;
            gridFormatRule1.Column = this.gridColumn10;
            gridFormatRule1.Name = "Format0";
            formatConditionRuleValue1.Appearance.ForeColor = System.Drawing.Color.Red;
            formatConditionRuleValue1.Appearance.Options.UseForeColor = true;
            formatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal;
            formatConditionRuleValue1.Value1 = 0;
            gridFormatRule1.Rule = formatConditionRuleValue1;
            this.repositoryItemGridLookUpEdit2View.FormatRules.Add(gridFormatRule1);
            this.repositoryItemGridLookUpEdit2View.Name = "repositoryItemGridLookUpEdit2View";
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.Columns.StoreAllOptions = true;
            this.repositoryItemGridLookUpEdit2View.OptionsLayout.StoreAppearance = true;
            this.repositoryItemGridLookUpEdit2View.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.repositoryItemGridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.AllowHtmlDrawHeaders = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ColumnAutoWidth = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.EnableAppearanceEvenRow = true;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemGridLookUpEdit2View.OptionsView.ShowIndicator = false;
            this.repositoryItemGridLookUpEdit2View.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn12, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "On-Hold Reason ";
            this.gridColumn11.FieldName = "Description";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.AllowFocus = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 0;
            this.gridColumn11.Width = 220;
            // 
            // gridColumn12
            // 
            this.gridColumn12.Caption = "Order";
            this.gridColumn12.FieldName = "RecordOrder";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsColumn.AllowEdit = false;
            this.gridColumn12.OptionsColumn.AllowFocus = false;
            this.gridColumn12.OptionsColumn.ReadOnly = true;
            // 
            // panelControl4
            // 
            this.panelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl4.Controls.Add(this.pictureEdit5);
            this.panelControl4.Controls.Add(this.labelControl7);
            this.panelControl4.Controls.Add(this.labelControl8);
            this.panelControl4.Location = new System.Drawing.Point(7, 6);
            this.panelControl4.Name = "panelControl4";
            this.panelControl4.Size = new System.Drawing.Size(1142, 48);
            this.panelControl4.TabIndex = 15;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit5.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit5.Location = new System.Drawing.Point(1098, 4);
            this.pictureEdit5.MenuManager = this.barManager1;
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit5.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit5.Properties.ReadOnly = true;
            this.pictureEdit5.Properties.ShowMenu = false;
            this.pictureEdit5.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit5.TabIndex = 9;
            // 
            // labelControl7
            // 
            this.labelControl7.AllowHtmlString = true;
            this.labelControl7.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl7.Appearance.Options.UseBackColor = true;
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(5, 5);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(370, 16);
            this.labelControl7.TabIndex = 6;
            this.labelControl7.Text = "<b>Step 1:</b> Set the Off-Hold values for the selected Site Contract(s)";
            // 
            // labelControl8
            // 
            this.labelControl8.AllowHtmlString = true;
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl8.Appearance.Options.UseBackColor = true;
            this.labelControl8.Location = new System.Drawing.Point(57, 29);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(659, 13);
            this.labelControl8.TabIndex = 7;
            this.labelControl8.Text = "Choose if you wish to clear the existing On-Hold values (Start, End Date and Reas" +
    "on) and optionally any Remarks. Click Next when done.";
            // 
            // btnStep1Next
            // 
            this.btnStep1Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep1Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep1Next.Location = new System.Drawing.Point(1061, 501);
            this.btnStep1Next.Name = "btnStep1Next";
            this.btnStep1Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Next.TabIndex = 0;
            this.btnStep1Next.Text = "Next";
            this.btnStep1Next.Click += new System.EventHandler(this.btnStep1Next_Click);
            // 
            // btnStep1Previous
            // 
            this.btnStep1Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep1Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep1Previous.Location = new System.Drawing.Point(967, 501);
            this.btnStep1Previous.Name = "btnStep1Previous";
            this.btnStep1Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep1Previous.TabIndex = 10;
            this.btnStep1Previous.Text = "Previous";
            this.btnStep1Previous.Click += new System.EventHandler(this.btnStep1Previous_Click);
            // 
            // xtraTabPageStep2
            // 
            this.xtraTabPageStep2.Controls.Add(this.labelControlSelectedVisitCount);
            this.xtraTabPageStep2.Controls.Add(this.gridControl4);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Next);
            this.xtraTabPageStep2.Controls.Add(this.btnStep2Previous);
            this.xtraTabPageStep2.Controls.Add(this.panelControl5);
            this.xtraTabPageStep2.Name = "xtraTabPageStep2";
            this.xtraTabPageStep2.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageStep2.Tag = "2";
            this.xtraTabPageStep2.Text = "Step 2";
            // 
            // labelControlSelectedVisitCount
            // 
            this.labelControlSelectedVisitCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControlSelectedVisitCount.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControlSelectedVisitCount.Appearance.ImageIndex = 10;
            this.labelControlSelectedVisitCount.Appearance.ImageList = this.imageCollection1;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageAlign = true;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageIndex = true;
            this.labelControlSelectedVisitCount.Appearance.Options.UseImageList = true;
            this.labelControlSelectedVisitCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControlSelectedVisitCount.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControlSelectedVisitCount.Location = new System.Drawing.Point(6, 497);
            this.labelControlSelectedVisitCount.Name = "labelControlSelectedVisitCount";
            this.labelControlSelectedVisitCount.Size = new System.Drawing.Size(159, 17);
            this.labelControlSelectedVisitCount.TabIndex = 26;
            this.labelControlSelectedVisitCount.Text = "0 Selected Visits";
            // 
            // gridControl4
            // 
            this.gridControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl4.DataSource = this.sp06411OMSiteContractWizardOffHoldVisitsBindingSource;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl4.EmbeddedNavigator.Buttons.ImageList = this.imageCollection1;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl4.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl4.Location = new System.Drawing.Point(7, 62);
            this.gridControl4.MainView = this.gridView4;
            this.gridControl4.MenuManager = this.barManager1;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoExEdit8,
            this.repositoryItemTextEditDateTime4,
            this.repositoryItemTextEditInteger4});
            this.gridControl4.Size = new System.Drawing.Size(1142, 433);
            this.gridControl4.TabIndex = 25;
            this.gridControl4.UseEmbeddedNavigator = true;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // sp06411OMSiteContractWizardOffHoldVisitsBindingSource
            // 
            this.sp06411OMSiteContractWizardOffHoldVisitsBindingSource.DataMember = "sp06411_OM_Site_Contract_Wizard_Off_Hold_Visits";
            this.sp06411OMSiteContractWizardOffHoldVisitsBindingSource.DataSource = this.dataSet_OM_Contract;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisitID,
            this.colSiteContractID1,
            this.colVisitNumber,
            this.colCreatedByStaffID,
            this.colExpectedStartDate,
            this.colExpectedEndDate,
            this.colStartDate1,
            this.colEndDate1,
            this.colRemarks1,
            this.colCostCalculationLevelDescription,
            this.colCreatedByStaffName,
            this.colClientContractID1,
            this.colSiteID1,
            this.colClientID1,
            this.colSiteName1,
            this.colClientName,
            this.colContractDescription1,
            this.colLinkedToParent1,
            this.colVisitStatusID,
            this.colVisitStatus,
            this.colJobCount});
            this.gridView4.GridControl = this.gridControl4;
            this.gridView4.GroupCount = 2;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsFilter.UseNewCustomFilterDialog = true;
            this.gridView4.OptionsFind.AlwaysVisible = true;
            this.gridView4.OptionsFind.FindDelay = 2000;
            this.gridView4.OptionsLayout.Columns.StoreAllOptions = true;
            this.gridView4.OptionsLayout.StoreAppearance = true;
            this.gridView4.OptionsLayout.StoreFormatRules = true;
            this.gridView4.OptionsMenu.ShowConditionalFormattingItem = true;
            this.gridView4.OptionsMenu.ShowGroupSummaryEditorItem = true;
            this.gridView4.OptionsSelection.MultiSelect = true;
            this.gridView4.OptionsView.AllowHtmlDrawHeaders = true;
            this.gridView4.OptionsView.ColumnAutoWidth = false;
            this.gridView4.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            this.gridView4.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colLinkedToParent1, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colSiteName1, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView4.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.GridView_PopupMenuShowing);
            this.gridView4.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.GridView_SelectionChanged);
            this.gridView4.CustomDrawEmptyForeground += new DevExpress.XtraGrid.Views.Base.CustomDrawEventHandler(this.GridView_CustomDrawEmptyForeground);
            this.gridView4.CustomFilterDialog += new DevExpress.XtraGrid.Views.Grid.CustomFilterDialogEventHandler(this.GridView_CustomFilterDialog);
            this.gridView4.FilterEditorCreated += new DevExpress.XtraGrid.Views.Base.FilterControlEventHandler(this.GridView_FilterEditorCreated);
            this.gridView4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GridView_MouseDown_Standard);
            this.gridView4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridView4_MouseUp);
            this.gridView4.GotFocus += new System.EventHandler(this.gridView4_GotFocus);
            // 
            // colVisitID
            // 
            this.colVisitID.Caption = "Visit ID";
            this.colVisitID.FieldName = "VisitID";
            this.colVisitID.Name = "colVisitID";
            this.colVisitID.OptionsColumn.AllowEdit = false;
            this.colVisitID.OptionsColumn.AllowFocus = false;
            this.colVisitID.OptionsColumn.ReadOnly = true;
            // 
            // colSiteContractID1
            // 
            this.colSiteContractID1.Caption = "Site Contract ID";
            this.colSiteContractID1.FieldName = "SiteContractID";
            this.colSiteContractID1.Name = "colSiteContractID1";
            this.colSiteContractID1.OptionsColumn.AllowEdit = false;
            this.colSiteContractID1.OptionsColumn.AllowFocus = false;
            this.colSiteContractID1.OptionsColumn.ReadOnly = true;
            this.colSiteContractID1.Width = 96;
            // 
            // colVisitNumber
            // 
            this.colVisitNumber.Caption = "Visit #";
            this.colVisitNumber.FieldName = "VisitNumber";
            this.colVisitNumber.Name = "colVisitNumber";
            this.colVisitNumber.OptionsColumn.AllowEdit = false;
            this.colVisitNumber.OptionsColumn.AllowFocus = false;
            this.colVisitNumber.OptionsColumn.ReadOnly = true;
            this.colVisitNumber.Visible = true;
            this.colVisitNumber.VisibleIndex = 0;
            this.colVisitNumber.Width = 81;
            // 
            // colCreatedByStaffID
            // 
            this.colCreatedByStaffID.Caption = "Created By Staff ID";
            this.colCreatedByStaffID.FieldName = "CreatedByStaffID";
            this.colCreatedByStaffID.Name = "colCreatedByStaffID";
            this.colCreatedByStaffID.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffID.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffID.OptionsColumn.ReadOnly = true;
            this.colCreatedByStaffID.Width = 114;
            // 
            // colExpectedStartDate
            // 
            this.colExpectedStartDate.Caption = "Expected Start";
            this.colExpectedStartDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colExpectedStartDate.FieldName = "ExpectedStartDate";
            this.colExpectedStartDate.Name = "colExpectedStartDate";
            this.colExpectedStartDate.OptionsColumn.AllowEdit = false;
            this.colExpectedStartDate.OptionsColumn.AllowFocus = false;
            this.colExpectedStartDate.OptionsColumn.ReadOnly = true;
            this.colExpectedStartDate.Visible = true;
            this.colExpectedStartDate.VisibleIndex = 1;
            this.colExpectedStartDate.Width = 100;
            // 
            // repositoryItemTextEditDateTime4
            // 
            this.repositoryItemTextEditDateTime4.AutoHeight = false;
            this.repositoryItemTextEditDateTime4.Mask.EditMask = "g";
            this.repositoryItemTextEditDateTime4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.repositoryItemTextEditDateTime4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditDateTime4.Name = "repositoryItemTextEditDateTime4";
            // 
            // colExpectedEndDate
            // 
            this.colExpectedEndDate.Caption = "Expected End";
            this.colExpectedEndDate.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colExpectedEndDate.FieldName = "ExpectedEndDate";
            this.colExpectedEndDate.Name = "colExpectedEndDate";
            this.colExpectedEndDate.OptionsColumn.AllowEdit = false;
            this.colExpectedEndDate.OptionsColumn.AllowFocus = false;
            this.colExpectedEndDate.OptionsColumn.ReadOnly = true;
            this.colExpectedEndDate.Visible = true;
            this.colExpectedEndDate.VisibleIndex = 2;
            this.colExpectedEndDate.Width = 100;
            // 
            // colStartDate1
            // 
            this.colStartDate1.Caption = "Start Date";
            this.colStartDate1.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colStartDate1.FieldName = "StartDate";
            this.colStartDate1.Name = "colStartDate1";
            this.colStartDate1.OptionsColumn.AllowEdit = false;
            this.colStartDate1.OptionsColumn.AllowFocus = false;
            this.colStartDate1.OptionsColumn.ReadOnly = true;
            this.colStartDate1.Visible = true;
            this.colStartDate1.VisibleIndex = 3;
            this.colStartDate1.Width = 100;
            // 
            // colEndDate1
            // 
            this.colEndDate1.Caption = "End Date";
            this.colEndDate1.ColumnEdit = this.repositoryItemTextEditDateTime4;
            this.colEndDate1.FieldName = "EndDate";
            this.colEndDate1.Name = "colEndDate1";
            this.colEndDate1.OptionsColumn.AllowEdit = false;
            this.colEndDate1.OptionsColumn.AllowFocus = false;
            this.colEndDate1.OptionsColumn.ReadOnly = true;
            this.colEndDate1.Visible = true;
            this.colEndDate1.VisibleIndex = 4;
            this.colEndDate1.Width = 100;
            // 
            // colRemarks1
            // 
            this.colRemarks1.Caption = "Remarks";
            this.colRemarks1.ColumnEdit = this.repositoryItemMemoExEdit8;
            this.colRemarks1.FieldName = "Remarks";
            this.colRemarks1.Name = "colRemarks1";
            this.colRemarks1.OptionsColumn.ReadOnly = true;
            this.colRemarks1.Visible = true;
            this.colRemarks1.VisibleIndex = 7;
            this.colRemarks1.Width = 182;
            // 
            // repositoryItemMemoExEdit8
            // 
            this.repositoryItemMemoExEdit8.AutoHeight = false;
            this.repositoryItemMemoExEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit8.Name = "repositoryItemMemoExEdit8";
            this.repositoryItemMemoExEdit8.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit8.ShowIcon = false;
            // 
            // colCostCalculationLevelDescription
            // 
            this.colCostCalculationLevelDescription.Caption = "Cost Calculation";
            this.colCostCalculationLevelDescription.FieldName = "CostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.Name = "colCostCalculationLevelDescription";
            this.colCostCalculationLevelDescription.OptionsColumn.AllowEdit = false;
            this.colCostCalculationLevelDescription.OptionsColumn.AllowFocus = false;
            this.colCostCalculationLevelDescription.OptionsColumn.ReadOnly = true;
            this.colCostCalculationLevelDescription.Width = 96;
            // 
            // colCreatedByStaffName
            // 
            this.colCreatedByStaffName.Caption = "Created By";
            this.colCreatedByStaffName.FieldName = "CreatedByStaffName";
            this.colCreatedByStaffName.Name = "colCreatedByStaffName";
            this.colCreatedByStaffName.OptionsColumn.AllowEdit = false;
            this.colCreatedByStaffName.OptionsColumn.AllowFocus = false;
            this.colCreatedByStaffName.OptionsColumn.ReadOnly = true;
            // 
            // colClientContractID1
            // 
            this.colClientContractID1.Caption = "Contract";
            this.colClientContractID1.FieldName = "ClientContractID";
            this.colClientContractID1.Name = "colClientContractID1";
            this.colClientContractID1.OptionsColumn.AllowEdit = false;
            this.colClientContractID1.OptionsColumn.AllowFocus = false;
            this.colClientContractID1.OptionsColumn.ReadOnly = true;
            this.colClientContractID1.Width = 198;
            // 
            // colSiteID1
            // 
            this.colSiteID1.Caption = "Site ID";
            this.colSiteID1.FieldName = "SiteID";
            this.colSiteID1.Name = "colSiteID1";
            this.colSiteID1.OptionsColumn.AllowEdit = false;
            this.colSiteID1.OptionsColumn.AllowFocus = false;
            this.colSiteID1.OptionsColumn.ReadOnly = true;
            // 
            // colClientID1
            // 
            this.colClientID1.Caption = "Client ID";
            this.colClientID1.FieldName = "ClientID";
            this.colClientID1.Name = "colClientID1";
            this.colClientID1.OptionsColumn.AllowEdit = false;
            this.colClientID1.OptionsColumn.AllowFocus = false;
            this.colClientID1.OptionsColumn.ReadOnly = true;
            // 
            // colSiteName1
            // 
            this.colSiteName1.Caption = "Site Name";
            this.colSiteName1.FieldName = "SiteName";
            this.colSiteName1.Name = "colSiteName1";
            this.colSiteName1.OptionsColumn.AllowEdit = false;
            this.colSiteName1.OptionsColumn.AllowFocus = false;
            this.colSiteName1.OptionsColumn.ReadOnly = true;
            this.colSiteName1.Visible = true;
            this.colSiteName1.VisibleIndex = 8;
            this.colSiteName1.Width = 260;
            // 
            // colClientName
            // 
            this.colClientName.Caption = "Client Name";
            this.colClientName.FieldName = "ClientName";
            this.colClientName.Name = "colClientName";
            this.colClientName.OptionsColumn.AllowEdit = false;
            this.colClientName.OptionsColumn.AllowFocus = false;
            this.colClientName.OptionsColumn.ReadOnly = true;
            this.colClientName.Width = 193;
            // 
            // colContractDescription1
            // 
            this.colContractDescription1.Caption = "Contract";
            this.colContractDescription1.FieldName = "ContractDescription";
            this.colContractDescription1.Name = "colContractDescription1";
            this.colContractDescription1.OptionsColumn.AllowEdit = false;
            this.colContractDescription1.OptionsColumn.AllowFocus = false;
            this.colContractDescription1.OptionsColumn.ReadOnly = true;
            this.colContractDescription1.Width = 133;
            // 
            // colLinkedToParent1
            // 
            this.colLinkedToParent1.Caption = "Client Contract";
            this.colLinkedToParent1.FieldName = "LinkedToParent";
            this.colLinkedToParent1.Name = "colLinkedToParent1";
            this.colLinkedToParent1.OptionsColumn.AllowEdit = false;
            this.colLinkedToParent1.OptionsColumn.AllowFocus = false;
            this.colLinkedToParent1.OptionsColumn.ReadOnly = true;
            this.colLinkedToParent1.Visible = true;
            this.colLinkedToParent1.VisibleIndex = 9;
            this.colLinkedToParent1.Width = 288;
            // 
            // colVisitStatusID
            // 
            this.colVisitStatusID.Caption = "Visit Status ID";
            this.colVisitStatusID.FieldName = "VisitStatusID";
            this.colVisitStatusID.Name = "colVisitStatusID";
            this.colVisitStatusID.OptionsColumn.AllowEdit = false;
            this.colVisitStatusID.OptionsColumn.AllowFocus = false;
            this.colVisitStatusID.OptionsColumn.ReadOnly = true;
            this.colVisitStatusID.Width = 86;
            // 
            // colVisitStatus
            // 
            this.colVisitStatus.Caption = "Visit Status";
            this.colVisitStatus.FieldName = "VisitStatus";
            this.colVisitStatus.Name = "colVisitStatus";
            this.colVisitStatus.OptionsColumn.AllowEdit = false;
            this.colVisitStatus.OptionsColumn.AllowFocus = false;
            this.colVisitStatus.OptionsColumn.ReadOnly = true;
            this.colVisitStatus.Visible = true;
            this.colVisitStatus.VisibleIndex = 5;
            this.colVisitStatus.Width = 136;
            // 
            // colJobCount
            // 
            this.colJobCount.Caption = "Job Count";
            this.colJobCount.ColumnEdit = this.repositoryItemTextEditInteger4;
            this.colJobCount.FieldName = "JobCount";
            this.colJobCount.Name = "colJobCount";
            this.colJobCount.OptionsColumn.AllowEdit = false;
            this.colJobCount.OptionsColumn.AllowFocus = false;
            this.colJobCount.OptionsColumn.ReadOnly = true;
            this.colJobCount.Visible = true;
            this.colJobCount.VisibleIndex = 6;
            // 
            // repositoryItemTextEditInteger4
            // 
            this.repositoryItemTextEditInteger4.AutoHeight = false;
            this.repositoryItemTextEditInteger4.Mask.EditMask = "n0";
            this.repositoryItemTextEditInteger4.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEditInteger4.Mask.UseMaskAsDisplayFormat = true;
            this.repositoryItemTextEditInteger4.Name = "repositoryItemTextEditInteger4";
            // 
            // btnStep2Next
            // 
            this.btnStep2Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Next.ImageOptions.Image = global::WoodPlan5.Properties.Resources.forward_32x32;
            this.btnStep2Next.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnStep2Next.Location = new System.Drawing.Point(1061, 501);
            this.btnStep2Next.Name = "btnStep2Next";
            this.btnStep2Next.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Next.TabIndex = 17;
            this.btnStep2Next.Text = "Next";
            this.btnStep2Next.Click += new System.EventHandler(this.btnStep2Next_Click);
            // 
            // btnStep2Previous
            // 
            this.btnStep2Previous.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStep2Previous.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnStep2Previous.Location = new System.Drawing.Point(967, 501);
            this.btnStep2Previous.Name = "btnStep2Previous";
            this.btnStep2Previous.Size = new System.Drawing.Size(88, 30);
            this.btnStep2Previous.TabIndex = 18;
            this.btnStep2Previous.Text = "Previous";
            this.btnStep2Previous.Click += new System.EventHandler(this.btnStep2Previous_Click);
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Controls.Add(this.labelControl3);
            this.panelControl5.Controls.Add(this.pictureEdit7);
            this.panelControl5.Controls.Add(this.labelControl4);
            this.panelControl5.Controls.Add(this.labelControl6);
            this.panelControl5.Location = new System.Drawing.Point(7, 6);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(1142, 48);
            this.panelControl5.TabIndex = 16;
            // 
            // labelControl3
            // 
            this.labelControl3.AllowHtmlString = true;
            this.labelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl3.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelControl3.Appearance.ImageIndex = 10;
            this.labelControl3.Appearance.ImageList = this.imageCollection1;
            this.labelControl3.Appearance.Options.UseBackColor = true;
            this.labelControl3.Appearance.Options.UseImageAlign = true;
            this.labelControl3.Appearance.Options.UseImageIndex = true;
            this.labelControl3.Appearance.Options.UseImageList = true;
            this.labelControl3.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl3.Location = new System.Drawing.Point(315, 26);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(280, 20);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "<b>Note:</b> Only Visits with a Status of On-Hold are shown.";
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit7.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit7.Location = new System.Drawing.Point(1098, 4);
            this.pictureEdit7.MenuManager = this.barManager1;
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit7.Properties.ReadOnly = true;
            this.pictureEdit7.Properties.ShowMenu = false;
            this.pictureEdit7.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit7.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.AllowHtmlString = true;
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl4.Appearance.Options.UseBackColor = true;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(5, 5);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(215, 16);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "<b>Step 2:</b> Choose Visits to put Off-Hold";
            // 
            // labelControl6
            // 
            this.labelControl6.AllowHtmlString = true;
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl6.Appearance.Options.UseBackColor = true;
            this.labelControl6.Location = new System.Drawing.Point(57, 29);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(254, 13);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "Tick the Visits to put Off-Hold. Click Next when done.";
            // 
            // xtraTabPageFinish
            // 
            this.xtraTabPageFinish.Controls.Add(this.panelControl2);
            this.xtraTabPageFinish.Controls.Add(this.groupControl1);
            this.xtraTabPageFinish.Controls.Add(this.labelControl10);
            this.xtraTabPageFinish.Controls.Add(this.btnFinish);
            this.xtraTabPageFinish.Controls.Add(this.btnFinishPrevious);
            this.xtraTabPageFinish.Controls.Add(this.pictureEdit2);
            this.xtraTabPageFinish.Name = "xtraTabPageFinish";
            this.xtraTabPageFinish.Size = new System.Drawing.Size(1155, 537);
            this.xtraTabPageFinish.Tag = "99";
            this.xtraTabPageFinish.Text = "Finished";
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.pictureEdit6);
            this.panelControl2.Controls.Add(this.labelControl11);
            this.panelControl2.Controls.Add(this.labelControl12);
            this.panelControl2.Location = new System.Drawing.Point(211, 6);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(938, 48);
            this.panelControl2.TabIndex = 18;
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureEdit6.EditValue = global::WoodPlan5.Properties.Resources.wizard_32_32;
            this.pictureEdit6.Location = new System.Drawing.Point(894, 4);
            this.pictureEdit6.MenuManager = this.barManager1;
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit6.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit6.Properties.ReadOnly = true;
            this.pictureEdit6.Properties.ShowMenu = false;
            this.pictureEdit6.Size = new System.Drawing.Size(40, 40);
            this.pictureEdit6.TabIndex = 19;
            // 
            // labelControl11
            // 
            this.labelControl11.AllowHtmlString = true;
            this.labelControl11.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 18F);
            this.labelControl11.Appearance.Options.UseBackColor = true;
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(5, 1);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(240, 29);
            this.labelControl11.TabIndex = 5;
            this.labelControl11.Text = "Completing the Wizard";
            // 
            // labelControl12
            // 
            this.labelControl12.AllowHtmlString = true;
            this.labelControl12.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.labelControl12.Appearance.Options.UseBackColor = true;
            this.labelControl12.Location = new System.Drawing.Point(9, 31);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(567, 13);
            this.labelControl12.TabIndex = 15;
            this.labelControl12.Text = "You have successfully completed the Wizard. On clicking <b>Finish</b>, your selec" +
    "ted Site Contract(s) will be set as Off-Hold.";
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.checkEdit1);
            this.groupControl1.Location = new System.Drawing.Point(211, 155);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(938, 55);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Available Choices:";
            // 
            // checkEdit1
            // 
            this.checkEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit1.EditValue = true;
            this.checkEdit1.Location = new System.Drawing.Point(6, 26);
            this.checkEdit1.MenuManager = this.barManager1;
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.AllowHtmlDraw = DevExpress.Utils.DefaultBoolean.True;
            this.checkEdit1.Properties.Caption = "Set the Site Contract(s) Off-Hold then <b>return</b> to the parent<b> Manager</b>" +
    ".";
            this.checkEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit1.Properties.RadioGroupIndex = 1;
            this.checkEdit1.Size = new System.Drawing.Size(926, 19);
            this.checkEdit1.TabIndex = 0;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(213, 132);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(154, 13);
            this.labelControl10.TabIndex = 15;
            this.labelControl10.Text = "What would you like to do next?";
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnFinish.Appearance.Options.UseFont = true;
            this.btnFinish.ImageOptions.Image = global::WoodPlan5.Properties.Resources.apply_32x32;
            this.btnFinish.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnFinish.Location = new System.Drawing.Point(1061, 501);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(88, 30);
            this.btnFinish.TabIndex = 12;
            this.btnFinish.Text = "Finish";
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnFinishPrevious
            // 
            this.btnFinishPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinishPrevious.ImageOptions.Image = global::WoodPlan5.Properties.Resources.backward_32x32;
            this.btnFinishPrevious.Location = new System.Drawing.Point(967, 501);
            this.btnFinishPrevious.Name = "btnFinishPrevious";
            this.btnFinishPrevious.Size = new System.Drawing.Size(88, 30);
            this.btnFinishPrevious.TabIndex = 11;
            this.btnFinishPrevious.Text = "Previous";
            this.btnFinishPrevious.Click += new System.EventHandler(this.btnFinishPrevious_Click);
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureEdit2.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureEdit2.EditValue = global::WoodPlan5.Properties.Resources.wizard_finish1;
            this.pictureEdit2.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit2.MenuManager = this.barManager1;
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Properties.ShowMenu = false;
            this.pictureEdit2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit2.Size = new System.Drawing.Size(205, 537);
            this.pictureEdit2.TabIndex = 5;
            // 
            // repositoryItemMemoExEdit7
            // 
            this.repositoryItemMemoExEdit7.AutoHeight = false;
            this.repositoryItemMemoExEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit7.LookAndFeel.SkinName = "Blue";
            this.repositoryItemMemoExEdit7.Name = "repositoryItemMemoExEdit7";
            this.repositoryItemMemoExEdit7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit7.ShowIcon = false;
            // 
            // repositoryItemGridLookUpEdit1
            // 
            this.repositoryItemGridLookUpEdit1.AutoHeight = false;
            this.repositoryItemGridLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemGridLookUpEdit1.DisplayMember = "Description";
            this.repositoryItemGridLookUpEdit1.LookAndFeel.SkinName = "Blue";
            this.repositoryItemGridLookUpEdit1.Name = "repositoryItemGridLookUpEdit1";
            this.repositoryItemGridLookUpEdit1.NullText = "";
            this.repositoryItemGridLookUpEdit1.ValueMember = "ID";
            // 
            // repositoryItemMemoExEdit5
            // 
            this.repositoryItemMemoExEdit5.AutoHeight = false;
            this.repositoryItemMemoExEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemMemoExEdit5.Name = "repositoryItemMemoExEdit5";
            this.repositoryItemMemoExEdit5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.repositoryItemMemoExEdit5.ShowIcon = false;
            // 
            // repositoryItemButtonEditChooseStaff
            // 
            this.repositoryItemButtonEditChooseStaff.AutoHeight = false;
            this.repositoryItemButtonEditChooseStaff.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "Choose", -1, true, true, true, editorButtonImageOptions1, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, "Click me to open the Select Staff screen", "choose", null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEditChooseStaff.Name = "repositoryItemButtonEditChooseStaff";
            this.repositoryItemButtonEditChooseStaff.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // dataSet_AT
            // 
            this.dataSet_AT.DataSetName = "DataSet_AT";
            this.dataSet_AT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // sp00039GetFormPermissionsForUserTableAdapter
            // 
            this.sp00039GetFormPermissionsForUserTableAdapter.ClearBeforeFill = true;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.Appearance.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceDisabled.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceFocused.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.repositoryItemCheckEdit4.AppearanceReadOnly.Options.UseBackColor = true;
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Caption = "";
            this.repositoryItemCheckEdit4.GlyphAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.ValueChecked = 1;
            this.repositoryItemCheckEdit4.ValueUnchecked = 0;
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.MinValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // repositoryItemDuration1
            // 
            this.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemDuration1.AutoHeight = false;
            this.repositoryItemDuration1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDuration1.DisabledStateText = null;
            this.repositoryItemDuration1.Name = "repositoryItemDuration1";
            this.repositoryItemDuration1.NullValuePromptShowForEmptyValue = true;
            this.repositoryItemDuration1.ShowEmptyItem = true;
            this.repositoryItemDuration1.ValidateOnEnterKey = true;
            // 
            // bsiRecordTicking
            // 
            this.bsiRecordTicking.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bsiRecordTicking.Caption = "Record <b>Ticking</b>";
            this.bsiRecordTicking.Id = 91;
            this.bsiRecordTicking.ImageOptions.ImageIndex = 0;
            this.bsiRecordTicking.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTick),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiUntick)});
            this.bsiRecordTicking.Name = "bsiRecordTicking";
            // 
            // bbiTick
            // 
            this.bbiTick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiTick.Caption = "<b>Tick</b> Selected Records";
            this.bbiTick.Id = 94;
            this.bbiTick.ImageOptions.ImageIndex = 0;
            this.bbiTick.Name = "bbiTick";
            this.bbiTick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTick_ItemClick);
            // 
            // bbiUntick
            // 
            this.bbiUntick.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.bbiUntick.Caption = "<b>Untick</b> Selected Records";
            this.bbiUntick.Id = 95;
            this.bbiUntick.ImageOptions.ImageIndex = 1;
            this.bbiUntick.Name = "bbiUntick";
            this.bbiUntick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUntick_ItemClick);
            // 
            // transitionManager1
            // 
            this.transitionManager1.FrameInterval = 5000;
            transition1.Control = this.xtraTabControl1;
            transition1.EasingMode = DevExpress.Data.Utils.EasingMode.EaseInOut;
            transition1.WaitingIndicatorProperties.Caption = "Loading...";
            this.transitionManager1.Transitions.Add(transition1);
            // 
            // sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter
            // 
            this.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter.ClearBeforeFill = true;
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.checked_32, "checked_32", typeof(global::WoodPlan5.Properties.Resources), 0);
            this.imageCollection2.Images.SetKeyName(0, "checked_32");
            this.imageCollection2.InsertImage(global::WoodPlan5.Properties.Resources.unchecked_32, "unchecked_32", typeof(global::WoodPlan5.Properties.Resources), 1);
            this.imageCollection2.Images.SetKeyName(1, "unchecked_32");
            // 
            // sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter
            // 
            this.sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter.ClearBeforeFill = true;
            // 
            // sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter
            // 
            this.sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter.ClearBeforeFill = true;
            // 
            // frm_OM_Site_Contract_Off_Hold_Wizard
            // 
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(1178, 537);
            this.Controls.Add(this.xtraTabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.LookAndFeel.SkinName = "Blue";
            this.Name = "frm_OM_Site_Contract_Off_Hold_Wizard";
            this.Text = "Site Contract Off-Hold Wizard";
            this.Activated += new System.EventHandler(this.frm_OM_Site_Contract_Off_Hold_Wizard_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_OM_Site_Contract_Off_Hold_Wizard_FormClosing);
            this.Load += new System.EventHandler(this.frm_OM_Site_Contract_Off_Hold_Wizard_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.xtraTabControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pmDataContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pmEditContextMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPageWelcome.ResumeLayout(false);
            this.xtraTabPageWelcome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.xtraTabPageStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_OM_Contract)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCurrency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditPercentage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEditOnHoldReasonID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl4)).EndInit();
            this.panelControl4.ResumeLayout(false);
            this.panelControl4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            this.xtraTabPageStep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp06411OMSiteContractWizardOffHoldVisitsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditDateTime4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditInteger4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            this.panelControl5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            this.xtraTabPageFinish.ResumeLayout(false);
            this.xtraTabPageFinish.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoExEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEditChooseStaff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet_AT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp00039GetFormPermissionsForUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDuration1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageWelcome;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnWelcomeNext;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageFinish;
        private DevExpress.XtraEditors.SimpleButton btnStep1Next;
        private DevExpress.XtraEditors.SimpleButton btnStep1Previous;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.SimpleButton btnFinish;
        private DevExpress.XtraEditors.SimpleButton btnFinishPrevious;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.PanelControl panelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DataSet_OM_Contract dataSet_OM_Contract;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageStep2;
        private DevExpress.XtraEditors.SimpleButton btnStep2Next;
        private DevExpress.XtraEditors.SimpleButton btnStep2Previous;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DataSet_AT dataSet_AT;
        private System.Windows.Forms.BindingSource sp00039GetFormPermissionsForUserBindingSource;
        private DataSet_ATTableAdapters.sp00039GetFormPermissionsForUserTableAdapter sp00039GetFormPermissionsForUserTableAdapter;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEditChooseStaff;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraScheduler.UI.RepositoryItemDuration repositoryItemDuration1;
        private DevExpress.XtraBars.BarSubItem bsiRecordTicking;
        private DevExpress.Utils.Animation.TransitionManager transitionManager1;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit7;
        private System.Windows.Forms.BindingSource sp06406OMSiteContractOnHoldReasonsWithBlankBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter sp06406_OM_Site_Contract_On_Hold_Reasons_With_BlankTableAdapter;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colContractValue;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCurrency;
        private DevExpress.XtraGrid.Columns.GridColumn colYearlyPercentageIncrease;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditPercentage;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitCount;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHold;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReasonID;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit repositoryItemGridLookUpEditOnHoldReasonID;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit2View;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldStartDate;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldEndDate;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditDateTime4;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit repositoryItemMemoExEdit8;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditInteger4;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitID;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitNumber;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffID;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedStartDate;
        private DevExpress.XtraGrid.Columns.GridColumn colExpectedEndDate;
        private DevExpress.XtraGrid.Columns.GridColumn colStartDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colEndDate1;
        private DevExpress.XtraGrid.Columns.GridColumn colRemarks1;
        private DevExpress.XtraGrid.Columns.GridColumn colCostCalculationLevelDescription;
        private DevExpress.XtraGrid.Columns.GridColumn colCreatedByStaffName;
        private DevExpress.XtraGrid.Columns.GridColumn colClientContractID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteID1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientID1;
        private DevExpress.XtraGrid.Columns.GridColumn colSiteName1;
        private DevExpress.XtraGrid.Columns.GridColumn colClientName;
        private DevExpress.XtraGrid.Columns.GridColumn colContractDescription1;
        private DevExpress.XtraGrid.Columns.GridColumn colLinkedToParent1;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatusID;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitStatus;
        private DevExpress.XtraGrid.Columns.GridColumn colJobCount;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControlSelectedVisitCount;
        private DevExpress.XtraBars.BarButtonItem bbiTick;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.XtraBars.BarButtonItem bbiUntick;
        private DevExpress.XtraGrid.Columns.GridColumn colVisitIDs;
        private System.Windows.Forms.BindingSource sp06410OMSiteContractWizardOffHoldSiteContractsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colOnHoldReason;
        private DataSet_OM_ContractTableAdapters.sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter sp06410_OM_Site_Contract_Wizard_Off_Hold_Site_ContractsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colClearOnHoldData;
        private System.Windows.Forms.BindingSource sp06411OMSiteContractWizardOffHoldVisitsBindingSource;
        private DataSet_OM_ContractTableAdapters.sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter sp06411_OM_Site_Contract_Wizard_Off_Hold_VisitsTableAdapter;
    }
}

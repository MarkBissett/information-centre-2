﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Job_Wizard_Block_Edit_Job_Info : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables

        private string strConnectionString = "";
        Settings set = Settings.Default;

        public int? intJobSubTypeID = null;
        public int? intJobTypeID = null;
        public string strJobTypeJobSubTypeDescription = null;
        public string strJobSubTypeDescription = null;
        public string strJobTypeDescription = null;
        public DateTime? dtExpectedStartDate = null;
        public DateTime? dtExpectedEndDate = null;
        public decimal? decExpectedDurationUnits = null;
        public int? intExpectedDurationUnitsDescriptorID = null;
        public int? intMinimumDaysFromLastVisit = null;
        public int? intMaximumDaysFromLastVisit = null;
        public int? intDaysLeeway = null;
        public int? intReactive = null;
        public int? intRequiresAccessPermit = null;
        public string strRemarks = null;
        public int? intClientPOID = null;
        public string strClientPONumber = null;

        #endregion

        public frm_OM_Job_Wizard_Block_Edit_Job_Info()
        {
            InitializeComponent();
        }

        private void frm_OM_Job_Wizard_Block_Edit_Job_Info_Load(object sender, EventArgs e)
        {
            this.FormID = 500166;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            strConnectionString = this.GlobalSettings.ConnectionString;
            Application.DoEvents();  // Allow Form time to repaint itself //
            
            strConnectionString = this.GlobalSettings.ConnectionString;

            sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06177_OM_Job_Duration_Descriptors_With_BlankTableAdapter.Fill(dataSet_OM_Job.sp06177_OM_Job_Duration_Descriptors_With_Blank, 1);

            JobSubTypeIDTextEdit.EditValue = null;
            JobTypeIDTextEdit.EditValue = null;
            JobTypeJobSubTypeDescriptionButtonEdit.EditValue = null;
            JobSubTypeDescriptionTextEdit.EditValue = null;
            JobTypeDescriptionTextEdit.EditValue = null;
            ExpectedStartDateDateEdit.EditValue = null;
            ExpectedEndDateDateEdit.EditValue = null;
            ExpectedDurationUnitsSpinEdit.EditValue = null;
            ExpectedDurationUnitsDescriptorIDGridLookUpEdit.EditValue = null;
            MinimumDaysFromLastVisitSpinEdit.EditValue = null;
            MaximumDaysFromLastVisitSpinEdit.EditValue = null;
            DaysLeewaySpinEdit.EditValue = null;
            ReactiveCheckEdit.EditValue = null;
            RequiresAccessPermitCheckEdit.EditValue = null;
            RemarksMemoEdit.EditValue = null;
            ClientPONumberButtonEdit.EditValue = null;
            ClientPOIDTextEdit.EditValue = null;
            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //
            ConfigureFormAccordingToMode();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
        }

        private void JobTypeJobSubTypeDescriptionButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_OM_Select_Job_Sub_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm._Mode = "single";
                fChildForm.intOperationManagerJobsOnly = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    JobSubTypeIDTextEdit.EditValue = fChildForm.intSelectedChildID;
                    JobTypeIDTextEdit.EditValue = fChildForm.intSelectedParentID;
                    JobSubTypeDescriptionTextEdit.EditValue = fChildForm.strSelectedChildDescriptions;
                    JobTypeDescriptionTextEdit.EditValue = fChildForm.strSelectedParentDescriptions;
                    JobTypeJobSubTypeDescriptionButtonEdit.EditValue = fChildForm.strSelectedParentDescriptions + " - " + fChildForm.strSelectedChildDescriptions;
                }
            }
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (JobSubTypeIDTextEdit.EditValue != null) intJobSubTypeID = Convert.ToInt32(JobSubTypeIDTextEdit.EditValue);
            if (JobTypeIDTextEdit.EditValue != null) intJobTypeID = Convert.ToInt32(JobTypeIDTextEdit.EditValue);
            if (JobTypeJobSubTypeDescriptionButtonEdit.EditValue != null) strJobTypeJobSubTypeDescription = JobTypeJobSubTypeDescriptionButtonEdit.EditValue.ToString();
            if (JobSubTypeDescriptionTextEdit.EditValue != null) strJobSubTypeDescription = JobSubTypeDescriptionTextEdit.EditValue.ToString();
            if (JobTypeDescriptionTextEdit.EditValue != null) strJobTypeDescription = JobTypeDescriptionTextEdit.EditValue.ToString();
            if (ExpectedStartDateDateEdit.EditValue != null) dtExpectedStartDate = Convert.ToDateTime(ExpectedStartDateDateEdit.DateTime);
            if (ExpectedEndDateDateEdit.EditValue != null) dtExpectedEndDate = Convert.ToDateTime(ExpectedEndDateDateEdit.DateTime);
            if (ExpectedDurationUnitsSpinEdit.EditValue != null) decExpectedDurationUnits = Convert.ToDecimal(ExpectedDurationUnitsSpinEdit.EditValue);
            if (ExpectedDurationUnitsDescriptorIDGridLookUpEdit.EditValue != null) intExpectedDurationUnitsDescriptorID = Convert.ToInt32(ExpectedDurationUnitsDescriptorIDGridLookUpEdit.EditValue);
            if (MinimumDaysFromLastVisitSpinEdit.EditValue != null) intMinimumDaysFromLastVisit = Convert.ToInt32(MinimumDaysFromLastVisitSpinEdit.EditValue);
            if (MaximumDaysFromLastVisitSpinEdit.EditValue != null) intMaximumDaysFromLastVisit = Convert.ToInt32(MaximumDaysFromLastVisitSpinEdit.EditValue);
            if (DaysLeewaySpinEdit.EditValue != null) intDaysLeeway = Convert.ToInt32(DaysLeewaySpinEdit.EditValue);
            if (ReactiveCheckEdit.EditValue != null) intReactive = Convert.ToInt32(ReactiveCheckEdit.EditValue);
            if (RequiresAccessPermitCheckEdit.EditValue != null) intRequiresAccessPermit = Convert.ToInt32(RequiresAccessPermitCheckEdit.EditValue);
            if (RemarksMemoEdit.EditValue != null) strRemarks = RemarksMemoEdit.EditValue.ToString();
            if (ClientPONumberButtonEdit.EditValue != null) strClientPONumber = ClientPONumberButtonEdit.EditValue.ToString();
            if (ClientPOIDTextEdit.EditValue != null) intClientPOID = Convert.ToInt32(ClientPOIDTextEdit.EditValue);

            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ClientPONumberButtonEdit_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                var fChildForm = new frm_OM_Select_Client_PO();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.strPassedInParentIDs = "";
                fChildForm.intPassedInChildID = 0;
                fChildForm.intFilterSummerMaintenanceClient = 1;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    ClientPOIDTextEdit.EditValue = fChildForm.intSelectedChildID;
                    ClientPONumberButtonEdit.EditValue = fChildForm.strSelectedChildDescriptions;
                }
            }
            else
            {
                ClientPOIDTextEdit.EditValue = 0;
                ClientPONumberButtonEdit.EditValue = "";
            }
        }



 





    }
}

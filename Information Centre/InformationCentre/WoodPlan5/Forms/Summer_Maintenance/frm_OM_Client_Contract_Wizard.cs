﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraDataLayout;
using DevExpress.XtraEditors.Controls;
using DevExpress.Skins;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using WoodPlan5.Classes.Operations;

namespace WoodPlan5
{
    public partial class frm_OM_Client_Contract_Wizard : BaseObjects.frmBase
    {

        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        public string strCaller = "";
        public string strRecordIDs = "";
        public int intRecordCount = 0;
        
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;

        bool iBool_ContractChangesMade = false;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //

        private bool ibool_ignoreValidation = false;  // Toggles validation on and off for Next Inspection Date Calculation //
        private bool ibool_FormStillLoading = true;

        public int i_intClientID = 0;

        #endregion

        public frm_OM_Client_Contract_Wizard()
        {
            InitializeComponent();
        }

        private void frm_OM_Client_Contract_Wizard_Load(object sender, EventArgs e)
        {
            this.FormID = 500127;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** //
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            strConnectionString = this.GlobalSettings.ConnectionString;

            // Get Form Permissions (For Edit Templates Button) //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            ProcessPermissionsForForm();

            Set_Grid_Highlighter_Transparent(this.Controls);

            xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;  // Hide Table Page headers so it doesn't look like a pageframe //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;

            sp06060_OM_GC_Companies_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06060_OM_GC_Companies_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06060_OM_GC_Companies_With_Blank);

            sp06061_OM_Sectors_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06061_OM_Sectors_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06061_OM_Sectors_With_Blank);

            sp06062_OM_Contract_Statuses_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06062_OM_Contract_Statuses_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06062_OM_Contract_Statuses_With_Blank);

            sp06063_OM_Contract_Types_With_BlankTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06063_OM_Contract_Types_With_BlankTableAdapter.Fill(dataSet_OM_Contract.sp06063_OM_Contract_Types_With_Blank);

            sp06058_OM_Client_Contract_Wizard_Client_ListTableAdapter.Connection.ConnectionString = strConnectionString;
            Load_Clients();

            sp06067_OM_Client_Contract_Year_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06069_OM_Client_Contract_Year_Billing_Profile_EditTableAdapter.Connection.ConnectionString = strConnectionString;

            sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter.Connection.ConnectionString = strConnectionString;

            // Populate Main Dataset //
            sp06059_OM_Client_Contract_EditTableAdapter.Connection.ConnectionString = strConnectionString;
            dataLayoutControl1.BeginUpdate();
            switch (strFormMode)
            {
                case "add":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["ClientContractID"] = 0;
                        drNewRow["ClientID"] = 0;
                        drNewRow["ContractDescription"] = "";
                        drNewRow["Reactive"] = 0;
                        drNewRow["GCCompanyID"] = 0;
                        drNewRow["SectorTypeID"] = 0;
                        drNewRow["ContractStatusID"] = 1;
                        drNewRow["ContractDirectorID"] = 0;
                        drNewRow["ContractDirector"] = "";
                        drNewRow["ContractTypeID"] = 0;
                        drNewRow["Active"] = 1;
                        drNewRow["ContractValue"] = (decimal)0.00;
                        drNewRow["YearlyPercentageIncrease"] = (decimal)0.00;
                        drNewRow["BillingCentreCodeID"] = 0;
                        drNewRow["LinkedToPreviousContractID"] = 0;
                        drNewRow["LinkedToPreviousContract"] = "";
                        drNewRow["CostCentreID"] = 0;
                        drNewRow["CostCentreCode"] = "";
                        drNewRow["CostCentre"] = "";
                        drNewRow["BillingCentreCode"] = "";
                        drNewRow["StartDate"] = DateTime.Today;
                        dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception) { }
                    break;
                case "blockadd":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        drNewRow["ClientContractID"] = 0;
                        drNewRow["ClientID"] = 0;
                        drNewRow["GCCompanyID"] = 0;
                        drNewRow["SectorTypeID"] = 0;
                        drNewRow["ContractStatusID"] = 0;
                        drNewRow["ContractDirectorID"] = 0;
                        drNewRow["ContractDirector"] = "";
                        drNewRow["ContractTypeID"] = 0;
                        drNewRow["Active"] = 1;
                        drNewRow["ContractValue"] = (decimal)0.00;
                        drNewRow["YearlyPercentageIncrease"] = (decimal)0.00;
                        drNewRow["BillingCentreCodeID"] = 0;
                        drNewRow["LinkedToPreviousContractID"] = 0;
                        drNewRow["LinkedToPreviousContract"] = "";
                        drNewRow["CostCentreID"] = 0;
                        drNewRow["CostCentreCode"] = "";
                        drNewRow["CostCentre"] = "";
                        drNewRow["BillingCentreCode"] = "";
                        drNewRow["StartDate"] = DateTime.Today;
                        dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows.Add(drNewRow);
                    }
                    catch (Exception) { }
                    break;
                case "blockedit":
                    try
                    {
                        DataRow drNewRow;
                        drNewRow = dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.NewRow();
                        drNewRow["strMode"] = strFormMode;
                        drNewRow["strRecordIDs"] = strRecordIDs;
                        /*drNewRow["ClientContractID"] = 0;
                        drNewRow["ClientID"] = 0;
                        drNewRow["GCCompanyID"] = 0;
                        drNewRow["SectorTypeID"] = 0;
                        drNewRow["ContractStatusID"] = 0;
                        drNewRow["ContractDirectorID"] = 0;
                        drNewRow["ContractDirector"] = "";
                        drNewRow["ContractTypeID"] = 0;
                        drNewRow["Active"] = 1;
                        drNewRow["ContractValue"] = (decimal)0.00;
                        drNewRow["YearlyPercentageIncrease"] = (decimal)0.00;
                        drNewRow["BillingCentreCodeID"] = 0;
                        drNewRow["LinkedToPreviousContractID"] = 0;
                        drNewRow["LinkedToPreviousContract"] = "";
                        drNewRow["CostCentreID"] = 0;
                        drNewRow["CostCentreCode"] = "";
                        drNewRow["CostCentre"] = "";
                        drNewRow["BillingCentreCode"] = "";*/
                        dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows.Add(drNewRow);
                        dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows[0].AcceptChanges();  // Set row status to Unchanged so Pending Changes does not show until further changes are made //
                    }
                    catch (Exception) { }
                    break;
                case "edit":
                case "view":
                    try
                    {
                        sp06059_OM_Client_Contract_EditTableAdapter.Fill(dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit, strRecordIDs, strFormMode);
                    }
                    catch (Exception) { }
                    break;
            }
            dataLayoutControl1.EndUpdate();

            spinEditContractYears.EditValue = 0;

            Attach_EditValueChanged_To_Children(this.Controls); // Attach EditValueChanged Event to all Editors to track changes...  Detached on Form Closing Event... //

            emptyEditor = new RepositoryItem();
        }

        private void Attach_EditValueChanged_To_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged += new EventHandler(Generic_EditChanged);
                if (item2 is ContainerControl) this.Attach_EditValueChanged_To_Children(item.Controls);
            }
        }

        private void Detach_EditValuechanged_From_Children(System.Collections.IEnumerable controls)
        {
            foreach (Control item in controls)
            {
                Control item2 = item;
                if (item2 is TextBoxMaskBox) item2 = item2.Parent;
                if (item2 is BaseEdit) ((BaseEdit)item2).EditValueChanged -= new EventHandler(Generic_EditChanged);
                this.Detach_EditValuechanged_From_Children(item.Controls);
            }
        }

        private void Generic_EditChanged(object sender, EventArgs e)
        {
            //if (barStaticItemChangesPending.Visibility == DevExpress.XtraBars.BarItemVisibility.Never)
            if (!iBool_ContractChangesMade)
            {
                //SetMenuStatus();  // Enable Save Buttons and sets visibility of ChangesPending to Always //
                iBool_ContractChangesMade = true;
            }
        }


        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (this.dataSet_OM_Contract.sp06059_OM_Client_Contract_Edit.Rows.Count == 0)
            {
                _KeepWaitFormOpen = false;
                splashScreenManager.CloseWaitForm();
                DevExpress.XtraEditors.XtraMessageBox.Show("Unable to " + this.strFormMode + " record - record not found.\n\nAnother user may have already deleted the record.", System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(this.strFormMode) + " Client Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                dxErrorProvider1.ClearErrors();
                ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                this.ValidateChildren();
                this.Close();
                return;
            }
            ConfigureFormAccordingToMode();

            _KeepWaitFormOpen = false;
            splashScreenManager.CloseWaitForm();
        }

        public override void PostLoadView(object objParameter)
        {
            ConfigureFormAccordingToMode();
        }

        private void ConfigureFormAccordingToMode()
        {
            switch (strFormMode)
            {
                case "add":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockadd":
                    // No InitValidationRules() Required //
                    break;
                case "edit":
                    ibool_ignoreValidation = true;  // Toggles validation on and off for Next Inspection Date Calculation //
                    this.ValidateChildren();  // Force Validation Message on any controls containing them //
                    break;
                case "blockedit":
                    // No InitValidationRules() Required //
                    break;
                case "view":
                    break;
                default:
                    break;
            }
            ibool_FormStillLoading = false;

            // Following need to get validation to fire on these controls - Activate_Pages doesn't seem to do it for some weird reason! //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
            ContractDescriptionTextEdit.Focus();
            StartDateDateEdit.Focus();
            ContractTypeIDGridLookUpEdit.Focus();
            ContractDirectorButtonEdit.Focus();
            ContractStatusIDGridLookUpEdit.Focus();
            SectorTypeIDGridLookUpEdit.Focus();
            GCCompanyIDGridLookUpEdit.Focus();
            ContractValueSpinEdit.Focus();

            Activate_All_TabPages Activate_Pages = new Activate_All_TabPages();
            Activate_Pages.Activate(xtraTabPageStep2.Controls);  // Force All controls hosted on any tabpages to initialise their databindings //

            if (strFormMode == "view")  // Disable all controls //
            {
                dataLayoutControl1.OptionsView.IsReadOnly = DevExpress.Utils.DefaultBoolean.True;
            }

            // Put focus back after cludge above //
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
            btnWelcomeNext.Focus();
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = true;
            bbiCopy.Enabled = true;
            bbiPaste.Enabled = true;
            bbiClear.Enabled = true;
            bbiSpellChecker.Enabled = true;

            ArrayList alItems = new ArrayList();
            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });

            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            GridView view = null;
            GridView parentView = null;
            int[] intParentRowHandles = null;
            int[] intRowHandles = null;

            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl5.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);

            parentView = (GridView)gridControl6.MainView;
            intParentRowHandles = parentView.GetSelectedRows();
            view = (GridView)gridControl7.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            gridControl7.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);
            bbiAddProfile.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);
            bbiAddProfileFromTemplate.Enabled = (strFormMode != "view" && intParentRowHandles.Length > 0);

            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = (strFormMode != "view");
            gridControl8.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (strFormMode != "view" && intRowHandles.Length > 0);

            bbiEditTemplates.Enabled = iBool_TemplateManager;
        }

        private void frm_OM_Client_Contract_Wizard_Activated(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

            }
        }

        private void frm_OM_Client_Contract_Wizard_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
            {
                // Store last used screen settings for current user //
                //default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CompanyFilter", i_str_selected_Company_ids);
                //default_screen_settings.SaveDefaultScreenSettings();
            }
        }


        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            switch (xtraTabControl1.SelectedTabPage.Name)
            {
                case "xtraTabPageStep4":
                    {
                        Clear_Filter_Billing_Profile();

                        // Check LinkedToParent Values in case they have changed //
                        GridView parentView = (GridView)gridControl6.MainView;
                        int intParentMax = parentView.DataRowCount;
                        if (intParentMax <= 0) return;

                        GridView childView = (GridView)gridControl7.MainView;
                        int intChildMax = childView.DataRowCount;
                        if (intChildMax <= 0) return;

                        string strDescription = "";
                        int intParentClientContractYearID = 0;
                        for (int i = 0; i < intParentMax; i++)
                        {
                            intParentClientContractYearID = Convert.ToInt32(parentView.GetRowCellValue(i, "ClientContractYearID"));
                            strDescription = (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate")).ToString("dd/MM/yyyy")) + " - " + (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(parentView.GetRowCellValue(i, "EndDate")).ToString("dd/MM/yyyy"));
                            for (int j = 0; j < intChildMax; j++)
                            {
                                if (Convert.ToInt32(childView.GetRowCellValue(j, "ClientContractYearID")) == intParentClientContractYearID)
                                {
                                    if (childView.GetRowCellValue(j, "LinkedToParent").ToString() != strDescription) childView.SetRowCellValue(j, "LinkedToParent", strDescription);
                                }
                            }
                        }
                        Filter_Billing_Profile_On_Year();
                    }
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Clients Available - click the Refresh button";
                    break;
                case "gridView5":
                    message = "No Contract Years Available - click the Add button or enter the number of Contract Years to add and click the Add Years To Grid button";
                    break;
                case "gridView6":
                    message = "No Contract Years Available - Create Contract Years in Step 3 of the Wizard";
                    break;
                case "gridView7":
                    message = "No Billing Profiles Available - select the Contract Year to see linked Billing Profiles. Click the Add button once a Year is selected to add one or more Profiles";
                    break;
                case "gridView8":
                    message = "No Responsible People Available - click the Add button to add Responsible Peoplee";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView6":
                    Filter_Billing_Profile_On_Year();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

       #endregion



        #region Welcome Page

        private void btnWelcomeNext_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }

        #endregion


        #region Step 1 Page

        private void btnStep1Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageWelcome;
        }
        private void bntStep1Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl1.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //         

            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select just one Client to create the Contract for before proceeding.", "Create Client Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Pick up selected client and set in next page if not already equal to this value //
            i_intClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandles[0], "ClientID"));
            DataRowView currentRow = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
            if (currentRow == null) return;
            int intBillingCentreCodeID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));
            if (intBillingCentreCodeID != i_intClientID)
            {
                currentRow["ClientID"] = i_intClientID;
                currentRow["ClientName"] = view.GetRowCellValue(intRowHandles[0], "ClientName").ToString();
            }

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            this.ValidateChildren();
        }

        private void Load_Clients()
        {
            if (!splashScreenManager.IsSplashFormVisible) splashScreenManager.ShowWaitForm();
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();

            try
            {
                sp06058_OM_Client_Contract_Wizard_Client_ListTableAdapter.Fill(dataSet_OM_Contract.sp06058_OM_Client_Contract_Wizard_Client_List);
            }
            catch (Exception ex)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Clients.\n\nMessage = [" + ex.Message + "].\n\nPlease close the screen then try again. If the problem persists, contact Technical Support.", "Load Clients", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }
            view.EndUpdate();
            if (splashScreenManager.IsSplashFormVisible && !_KeepWaitFormOpen) splashScreenManager.CloseWaitForm();
        }

        #region GridView1

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("reload".Equals(e.Button.Tag))
                    {
                        Load_Clients();
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion

        #endregion


        #region Step 2 Page

        private void btnStep2Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep1;
        }
        private void btnStep2Next_Click(object sender, EventArgs e)
        {

            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            this.ValidateChildren();

            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Client Contract Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;

            // Set Number of Years if no Year Records Present //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)// && Convert.ToInt32(spinEditContractYears.EditValue) <= 0)
            {
                if ((String.IsNullOrEmpty(StartDateDateEdit.DateTime.ToString()) || StartDateDateEdit.DateTime == DateTime.MinValue) || (String.IsNullOrEmpty(EndDateDateEdit.DateTime.ToString()) || EndDateDateEdit.DateTime == DateTime.MinValue))
                {

                }
                TimeSpan TS = EndDateDateEdit.DateTime - StartDateDateEdit.DateTime;
                double dYears = TS.TotalDays / 365.25;
                int intYears = Convert.ToInt32(Math.Ceiling(dYears));
                spinEditContractYears.EditValue = (intYears > 0 ? intYears : 0);
            }
        }

        #region Editors

        private void ContractDescriptionTextEdit_Validating(object sender, CancelEventArgs e)
        {
            TextEdit be = (TextEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ContractDescriptionTextEdit, "Enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContractDescriptionTextEdit, "");
            }
        }

        private void GCCompanyIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(GCCompanyIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(GCCompanyIDGridLookUpEdit, "");
            }
        }

        private void SectorTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(SectorTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(SectorTypeIDGridLookUpEdit, "");
            }
        }

        private void ContractStatusIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(ContractStatusIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContractStatusIDGridLookUpEdit, "");
            }
        }

        private void ContractDirectorButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
                if (currentRow == null) return;
                int ContractDirectorID = (string.IsNullOrEmpty(currentRow["ContractDirectorID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ContractDirectorID"]));

                var fChildForm = new frm_HR_Select_Staff();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalStaffID = ContractDirectorID;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (ContractDirectorID != fChildForm.intSelectedStaffID)
                    {
                        currentRow["ContractDirectorID"] = fChildForm.intSelectedStaffID;
                        currentRow["ContractDirector"] = fChildForm.strSelectedStaffName;
                        sp06059OMClientContractEditBindingSource.EndEdit();
                    }
                }
            }
        }
        private void ContractDirectorButtonEdit_Validating(object sender, CancelEventArgs e)
        {
            ButtonEdit be = (ButtonEdit)sender;
            if (this.strFormMode != "blockedit" && string.IsNullOrEmpty(be.EditValue.ToString()))
            {
                dxErrorProvider1.SetError(ContractDirectorButtonEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContractDirectorButtonEdit, "");
            }
        }

        private void ContractTypeIDGridLookUpEdit_Validating(object sender, CancelEventArgs e)
        {
            GridLookUpEdit glue = (GridLookUpEdit)sender;
            if (this.strFormMode != "blockedit" && (string.IsNullOrEmpty(glue.EditValue.ToString()) || glue.EditValue.ToString() == "0"))
            {
                dxErrorProvider1.SetError(ContractTypeIDGridLookUpEdit, "Select a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(ContractTypeIDGridLookUpEdit, "");
            }
        }

        private void StartDateDateEdit_Validating(object sender, CancelEventArgs e)
        {
            DateEdit de = (DateEdit)sender;
            if (this.strFormMode != "blockedit" && (de.EditValue == null || string.IsNullOrEmpty(de.EditValue.ToString())))
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "Select\\enter a value.");
                e.Cancel = true;  // Show stop icon as field is invalid //
                return;
            }
            else
            {
                dxErrorProvider1.SetError(StartDateDateEdit, "");
            }
        }

        private void BillingCentreCodeButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
                if (currentRow == null) return;
                int intBillingCentreCodeID = (string.IsNullOrEmpty(currentRow["BillingCentreCodeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["BillingCentreCodeID"]));
                int intClientID = (string.IsNullOrEmpty(currentRow["ClientID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientID"]));

                var fChildForm = new frm_OM_Select_Cost_Centre();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalBillingCodeID = intBillingCentreCodeID;
                fChildForm.strOriginalDepartmentIDs = (intClientID == 0 ? "" : intClientID.ToString() + ","); ;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intBillingCentreCodeID != fChildForm.intSelectedBillingCentreCodeID)
                    {
                        currentRow["BillingCentreCodeID"] = fChildForm.intSelectedBillingCentreCodeID;
                        currentRow["CostCentreID"] = fChildForm.intSelectedCostCentreID;
                        currentRow["CostCentreCode"] = fChildForm.strSelectedCostCentreCodes;
                        currentRow["CostCentre"] = fChildForm.strSelectedCostCentres;
                        currentRow["BillingCentreCode"] = fChildForm.strSelectedBillingCentreCode;
                        sp06059OMClientContractEditBindingSource.EndEdit();
                    }
                }
            }
        }

        private void LinkedToPreviousContractButtonEdit_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
                if (currentRow == null) return;
                int intOriginalID = (string.IsNullOrEmpty(currentRow["LinkedToPreviousContractID"].ToString()) ? 0 : Convert.ToInt32(currentRow["LinkedToPreviousContractID"]));
                int intExcludeID = (string.IsNullOrEmpty(currentRow["ClientContractID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ClientContractID"]));

                var fChildForm = new frm_OM_Select_Client_Contract();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalSelectedID = intOriginalID;
                fChildForm._strExcludeIDs = intExcludeID.ToString() + ",";
                fChildForm.i_dt_FromDate = DateTime.Today.AddMonths(-6);
                fChildForm.i_dt_ToDate = DateTime.Today.AddMonths(6);
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (intOriginalID != fChildForm.intSelectedID)
                    {
                        currentRow["LinkedToPreviousContractID"] = fChildForm.intSelectedID;
                        currentRow["LinkedToPreviousContract"] = fChildForm.strSelectedDescription;
                        sp06059OMClientContractEditBindingSource.EndEdit();
                    }
                }
            }
        }

        #endregion

        private string GetInvalidDataEntryValues(DXErrorProvider dxErrorProvider, DataLayoutControl dataLayoutcontrol)
        {
            string strErrorMessages = "";
            bool boolFocusMoved = false;
            foreach (Control c in dxErrorProvider.GetControlsWithError())
            {
                dataLayoutcontrol.GetItemByControl(c);
                DevExpress.XtraLayout.LayoutControlItem item = dataLayoutcontrol.GetItemByControl(c);
                if (item != null)
                {
                    if (!boolFocusMoved)
                    {
                        dataLayoutcontrol.FocusHelper.PlaceItemIntoView(item);
                        dataLayoutcontrol.FocusHelper.FocusElement(item, true);
                        boolFocusMoved = true;
                    }
                    strErrorMessages += "--> " + item.Text.ToString() + "  " + dxErrorProvider.GetError(c) + "\n";
                }
            }
            if (strErrorMessages != "") strErrorMessages = "The following errors are present...\n\n" + strErrorMessages + "\n";
            return strErrorMessages;
        }

        #endregion


        #region Step 3 Page

        private void btnStep3Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep2;
        }
        private void btnStep3Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl5.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep4;
        }

        private void spinEditContractYears_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit se = (SpinEdit)sender;
            btnAddYearsBtn.Enabled = (Convert.ToInt32(se.EditValue) > 0);
        }

        private void btnAddYearsBtn_Click(object sender, EventArgs e)
        {
            int intYears = Convert.ToInt32(spinEditContractYears.EditValue);
            if (intYears <= 0)
            {
                XtraMessageBox.Show("Please enter a value greater than 0 for the number of Years to add to the grid before proceeding.", "Add Years to Grid", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            GridView view = (GridView)gridControl5.MainView;

            DateTime dtStartDate = StartDateDateEdit.DateTime;
            if (view.DataRowCount > 0)
            {
                DateTime dtMaxStartDate = DateTime.MinValue;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToDateTime(view.GetRowCellValue(i, "StartDate")) > dtMaxStartDate) dtMaxStartDate = Convert.ToDateTime(view.GetRowCellValue(i, "StartDate"));
                }
                dtStartDate = (dtMaxStartDate == DateTime.MinValue ? DateTime.Today : dtMaxStartDate.AddYears(1));
            }
            DateTime dtEndDate = dtStartDate.AddYears(1).AddDays(-1);
            
            decimal decValue = Convert.ToDecimal(ContractValueSpinEdit.EditValue);
            if (decValue > (decimal)0.00) decValue = decValue / (decimal)intYears;

            decimal decPercentageIncrease = Convert.ToDecimal(YearlyPercentageIncreaseSpinEdit.EditValue);
            decimal tempLastValue = decValue;

            view.BeginDataUpdate();
            for (int i = 0; i < intYears; i++)
            {
                if (i > 0 && decPercentageIncrease != (decimal)0.00) tempLastValue += (tempLastValue * (decPercentageIncrease / (decimal)100.00));
                try
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06067_OM_Client_Contract_Year_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["ClientContractID"] = 0;
                    drNewRow["YearDescription"] = "Year: " + dtStartDate.Year.ToString();
                    drNewRow["StartDate"] = dtStartDate;
                    drNewRow["EndDate"] = dtEndDate;
                    drNewRow["Active"] = (DateTime.Today.Year == dtStartDate.Year ? 1 : 0);
                    drNewRow["ClientValue"] = tempLastValue;
                    drNewRow["YearlyPercentageIncrease"] = decPercentageIncrease;
                    dataSet_OM_Contract.sp06067_OM_Client_Contract_Year_Edit.Rows.Add(drNewRow);
                }
                catch (Exception Ex) { }
                dtStartDate = dtEndDate.AddDays(1);
                dtEndDate = (i + 1 < intYears ? dtStartDate.AddYears(1).AddDays(-1) : dtEndDate);
            }
            view.EndDataUpdate();
        }

        #region GridView5

        private void gridControl5_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Contract_Year();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Contract_Year();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView5_GotFocus(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void gridView5_MouseUp(object sender, MouseEventArgs e)
        {
            SetMenuStatus();
        }

        #endregion

        private void Add_Contract_Year()
        {
            GridView view = (GridView)gridControl5.MainView;

            DateTime dtStartDate = StartDateDateEdit.DateTime;
            if (view.DataRowCount > 0)
            {
                DateTime dtMaxStartDate = DateTime.MinValue;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToDateTime(view.GetRowCellValue(i, "StartDate")) > dtMaxStartDate) dtMaxStartDate = Convert.ToDateTime(view.GetRowCellValue(i, "StartDate"));
                }
                dtStartDate = (dtMaxStartDate == DateTime.MinValue ? DateTime.Today : dtMaxStartDate.AddYears(1));
            }
            DateTime dtEndDate = dtStartDate.AddYears(1).AddDays(-1);

            decimal decValue = (decimal)0.00;

            view.BeginUpdate();
            try
            {
                DataRow drNewRow;
                drNewRow = dataSet_OM_Contract.sp06067_OM_Client_Contract_Year_Edit.NewRow();
                drNewRow["strMode"] = "add";
                drNewRow["strRecordIDs"] = "";
                drNewRow["ClientContractID"] = 0;
                drNewRow["YearDescription"] = "Year: " + dtStartDate.Year.ToString();
                drNewRow["StartDate"] = dtStartDate;
                drNewRow["EndDate"] = dtEndDate;
                drNewRow["Active"] = (DateTime.Today.Year == dtStartDate.Year ? 1 : 0);
                drNewRow["ClientValue"] = decValue;
                drNewRow["YearlyPercentageIncrease"] = decValue;
                drNewRow["DummyClientContractYearID"] = 0;
                dataSet_OM_Contract.sp06067_OM_Client_Contract_Year_Edit.Rows.Add(drNewRow);
            }
            catch (Exception Ex) { }
            view.EndUpdate();
        }

        private void Delete_Contract_Year()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl5.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Contract Years to remove from the Contract by clicking on them then try again.", "Remove Linked Contract Years from Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Contract Year" : Convert.ToString(intRowHandles.Length) + " Linked Contract Years") + " selected for removing from the current Contract!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Contract Year" : "these Linked Contract Years") + " will be removed from the Contract. Any Linked Billing Profiles will also be removed.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Contract Year from Client Contract", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");
               
                int intChildMax = 0;
                GridView childView = (GridView)gridControl7.MainView;
                int intParentClientContractYearID = 0;
                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    intParentClientContractYearID = Convert.ToInt32(view.GetRowCellValue(i, "ClientContractYearID"));
                    
                    // Take out any linked Billing Profile first //
                    Clear_Filter_Billing_Profile();
                    intChildMax = childView.DataRowCount;
                    for (int j = intChildMax - 1; j >= 0; j--)
                    {
                        if (Convert.ToInt32(childView.GetRowCellValue(j, "ClientContractYearID")) == intParentClientContractYearID)
                        {
                            childView.DeleteRow(j);  // Delete child row //
                        }                       
                    }

                    view.DeleteRow(intRowHandles[i]);  // Delete parent row //
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Contract.", "Remove Linked Contract Years from Contract", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }


        #endregion


        #region Step 4 Page

        private void btnStep4Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep3;
        }
        private void btnStep4Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            
            view = (GridView)gridControl7.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep5;
        }


        #region GridView6

        private void gridView6_GotFocus(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void gridView6_MouseUp(object sender, MouseEventArgs e)
        {
            SetMenuStatus();
        }

        #endregion


        #region GridView7

        private void gridControl7_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Billing_Profile();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Billing_Profile();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView7_GotFocus(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void gridView7_MouseUp(object sender, MouseEventArgs e)
        {
            SetMenuStatus();
        }

        #endregion


        private void bbiAddProfile_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Add_Billing_Profile();
        }

        private void bbiAddProfileFromTemplate_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridView parentView = (GridView)gridControl6.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Contract Years to add the Billing Profile Template to by clicking on them then try again.", "Add Billing Profile From Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Contract Years selected. If you proceed a new Billing Profile will be created for each of these records from the chosen Template.\n\nProceed?", "Add Billing Profile From Template", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();

            var fChildForm = new frm_OM_Select_Template_Billing_Profile_Header();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                int intTemplateID = fChildForm.intSelectedTemplateHeaderID;
                if (intTemplateID <= 0) return;

                GridControl gridControlTemp = fChildForm.gridControl2;
                GridView viewTemp = (GridView)gridControlTemp.MainView;
                int intRowCount = viewTemp.DataRowCount;
                if (intCount <= 0) return;

                view.PostEditor();
                view.BeginUpdate();
                try
                {
                    decimal decValuePercentage = (decimal)0.00;
                    DateTime dtCalculatedDate;
                    int intUnits = 0;
                    int intUnitDescriptor = 0;
                    foreach (int i in intRowHandles)  // Selected Years //
                    {
                        for (int j = 0; j < intRowCount; j++)  // Template Items //
			            {
                            DataRow drNewRow;
                            drNewRow = dataSet_OM_Contract.sp06069_OM_Client_Contract_Year_Billing_Profile_Edit.NewRow();
                            drNewRow["strMode"] = "add";
                            drNewRow["strRecordIDs"] = "";
                            drNewRow["ClientContractYearID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "ClientContractYearID"));

                            if (!string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()))
                            {
                                intUnits = Convert.ToInt32(viewTemp.GetRowCellValue(j, "UnitsFromStart"));
                                intUnitDescriptor = Convert.ToInt32(viewTemp.GetRowCellValue(j, "UnitsFromStartDescriptorID"));
                                if (intUnits > 0 && intUnitDescriptor > 0)
                                {
                                    dtCalculatedDate = Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate"));
                                    switch (intUnitDescriptor)
                                    {
                                        case 1:  // Days //
                                            {
                                                dtCalculatedDate = dtCalculatedDate.AddDays(intUnits);
                                            }
                                            break;
                                        case 2:  // Weeks //
                                            {
                                                dtCalculatedDate = dtCalculatedDate.AddDays(intUnits * 7);
                                            }
                                            break;
                                        case 3:  // Month //
                                            {
                                                dtCalculatedDate = dtCalculatedDate.AddMonths(intUnits);
                                            }
                                            break;
                                        case 4:  // Years //
                                            {
                                                dtCalculatedDate = dtCalculatedDate.AddYears(intUnits);
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                    drNewRow["InvoiceDateDue"] = dtCalculatedDate;
                                }
                            }
                            decValuePercentage = Convert.ToDecimal(viewTemp.GetRowCellValue(j, "ValuePercentage"));
                            drNewRow["EstimatedBillAmount"] = (decValuePercentage > (decimal)0.00 ? Convert.ToDecimal(parentView.GetRowCellValue(i, "ClientValue")) * (decValuePercentage / 100) : (decimal)0.00);
                           
                            drNewRow["ActualBillAmount"] = (decimal)0.00;
                            drNewRow["BilledByPersonID"] = 0;
                            drNewRow["LinkedToParent"] = (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate")).ToString("dd/MM/yyyy")) + " - " + (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(parentView.GetRowCellValue(i, "EndDate")).ToString("dd/MM/yyyy"));
                            dataSet_OM_Contract.sp06069_OM_Client_Contract_Year_Billing_Profile_Edit.Rows.Add(drNewRow);
			            }
                    }
                }
                catch (Exception Ex) { }
                view.ExpandAllGroups();
                view.EndUpdate();
            }
        }

        private void bbiEditTemplates_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var fChildForm = new frm_OM_Template_Billing_Profile_Manager();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();
            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void Add_Billing_Profile()
        {
            GridView parentView = (GridView)gridControl6.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Contract Years to add the Billing Profile to by clicking on them then try again.", "Add Billing Profile", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Contract Years selected. If you proceed a new Billing Profile will be created for each of these records.\n\nProceed?", "Add Billing Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();
            parentView.BeginDataUpdate();
            view.BeginDataUpdate();
            try
            {
                foreach (int i in intRowHandles)
                {
                    DataRow drNewRow;
                    drNewRow = dataSet_OM_Contract.sp06069_OM_Client_Contract_Year_Billing_Profile_Edit.NewRow();
                    drNewRow["strMode"] = "add";
                    drNewRow["strRecordIDs"] = "";
                    drNewRow["ClientContractYearID"] = Convert.ToInt32(parentView.GetRowCellValue(i, "ClientContractYearID"));
                    drNewRow["EstimatedBillAmount"] = (decimal)0.00;
                    drNewRow["ActualBillAmount"] = (decimal)0.00;
                    drNewRow["BilledByPersonID"] = 0;
                    drNewRow["LinkedToParent"] = (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()) ? "Unknown Start" : Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate")).ToString("dd/MM/yyyy")) + " - " + (string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "EndDate").ToString()) ? "Unknown End" : Convert.ToDateTime(parentView.GetRowCellValue(i, "EndDate")).ToString("dd/MM/yyyy"));
                    dataSet_OM_Contract.sp06069_OM_Client_Contract_Year_Billing_Profile_Edit.Rows.Add(drNewRow);
               }
            }
            catch (Exception Ex) { }
            parentView.EndDataUpdate();
            view.EndDataUpdate();
            view.ExpandAllGroups();
        }

        private void Delete_Billing_Profile()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl7.MainView;
            view.PostEditor();
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Billing Profiles to remove from the Contract Year by clicking on them then try again.", "Remove Linked Billing Profile from Contract Year", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Billing Profile" : Convert.ToString(intRowHandles.Length) + " Linked Billing Profiles") + " selected for removing from the current Contract Year!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Billing Profile" : "these Linked Billing Profiles") + " will be removed from the Contract Year.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Billing Profile from Contract Year", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    view.DeleteRow(intRowHandles[i]);
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Contract Year.", "Remove Linked Billing Profile from Contract Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SetMenuStatus();
            }
        }

        private void Filter_Billing_Profile_On_Year()
        {
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();

            string strFilterCondition = "";
            GridView parentView = (GridView)gridControl6.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                strFilterCondition = "[ClientContractYearID] = 9999999999";
            }
            else
            {
                foreach (int i in intRowHandles)
                {
                    strFilterCondition += (string.IsNullOrWhiteSpace(strFilterCondition) ? "" : " or ");
                    strFilterCondition += "[ClientContractYearID] = " + parentView.GetRowCellValue(i, "ClientContractYearID").ToString();
                }
            }
            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.ActiveFilter.NonColumnFilter = strFilterCondition;
            view.MakeRowVisible(-1, true);
            view.ExpandAllGroups();
            view.EndUpdate();
            SetMenuStatus();
        }

        private void Clear_Filter_Billing_Profile()
        {
            GridView view = (GridView)gridControl7.MainView;
            view.PostEditor();

            view.BeginUpdate();
            view.ActiveFilter.Clear();
            view.MakeRowVisible(-1, true);
            view.EndUpdate();
            SetMenuStatus();
        }

        #endregion


        #region Step 5 Page

        private void btnStep5Previous_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep4;
        }
        private void btnStep5Next_Click(object sender, EventArgs e)
        {
            GridView view = (GridView)gridControl8.MainView;
            view.PostEditor();
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  

            xtraTabControl1.SelectedTabPage = this.xtraTabPageFinish;
        }

        private void bbiAddPersonResponsibility_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Add_Responsible_Person();
        }

        #region GridView8

        private void gridControl8_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Responsible_Person();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Responsible_Person();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridView8_GotFocus(object sender, EventArgs e)
        {
            SetMenuStatus();
        }

        private void gridView8_MouseUp(object sender, MouseEventArgs e)
        {
            SetMenuStatus();
        }

        private void repositoryItemButtonEdit1_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.Current;
                if (currentRow == null) return;
                int StaffID = 0;
                try
                {
                    StaffID = (string.IsNullOrEmpty(currentRow["StaffID"].ToString()) ? 0 : Convert.ToInt32(currentRow["StaffID"]));
                }
                catch (Exception) { }

                var fChildForm1 = new frm_OM_Client_Contract_Person_Responsibility_Select_Person_Type();
                fChildForm1.GlobalSettings = this.GlobalSettings;
                fChildForm1.ShowDialog();
                int intPersonTypeID = fChildForm1._SelectedTypeID;
                if (intPersonTypeID == -1) return;
                string strPersonType = fChildForm1._SelectedTypeDescription;

                if (intPersonTypeID == 0)  // Staff //
                {
                    var fChildForm = new frm_HR_Select_Staff();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalStaffID = StaffID;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        if (StaffID != fChildForm.intSelectedStaffID)
                        {
                            currentRow["StaffID"] = fChildForm.intSelectedStaffID;
                            currentRow["StaffName"] = fChildForm.strSelectedStaffName;
                            currentRow["PersonTypeID"] = intPersonTypeID;
                            currentRow["PersonTypeDescription"] = strPersonType;
                            currentRow["ResponsibilityTypeID"] = 0;  // Clear and force user to re-select it since the Person Type may have changed //
                            currentRow["ResponsibilityType"] = "";  // Clear and force user to re-select it since the Person Type may have changed //
                            sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();
                            GridView view = (GridView)gridControl8.MainView;
                            view.FocusedColumn = colResponsibilityType;  // Move focus to next column so new value is shown in original column //
                        }
                    }
                }
                else  // Client Contact //
                {
                    var fChildForm = new frm_Core_Select_Client_Contact_Person();
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.intOriginalChildSelectedID = StaffID;
                    fChildForm.intFilterSummerMaintenanceClient = 1;
                    if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                    {
                        if (StaffID != fChildForm.intSelectedChildID)
                        {
                            currentRow["StaffID"] = fChildForm.intSelectedChildID;
                            currentRow["StaffName"] = fChildForm.strSelectedChildDescription;
                            currentRow["PersonTypeID"] = intPersonTypeID;
                            currentRow["PersonTypeDescription"] = strPersonType;
                            currentRow["ResponsibilityTypeID"] = 0;  // Clear and force user to re-select it since the Person Type may have changed //
                            currentRow["ResponsibilityType"] = "";  // Clear and force user to re-select it since the Person Type may have changed //
                            sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();
                            GridView view = (GridView)gridControl8.MainView;
                            view.FocusedColumn = colResponsibilityType;  // Move focus to next column so new value is shown in original column //
                        }
                    }
                }
            }
        }

        private void repositoryItemButtonEditResponsibilityType_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            if (strFormMode == "view") return;
            if (e.Button.Tag.ToString().ToLower() == "choose")  // Choose Button //
            {
                DataRowView currentRow = (DataRowView)sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.Current;
                if (currentRow == null) return;
                int ResponsibilityTypeID = 0;
                int PersonTypeID = 0;
                try
                {
                    ResponsibilityTypeID = (string.IsNullOrEmpty(currentRow["ResponsibilityTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["ResponsibilityTypeID"]));
                    PersonTypeID = (string.IsNullOrEmpty(currentRow["PersonTypeID"].ToString()) ? 0 : Convert.ToInt32(currentRow["PersonTypeID"]));
                }
                catch (Exception) { }

                var fChildForm = new frm_OM_Select_Person_Responsibility_Type();
                fChildForm.GlobalSettings = this.GlobalSettings;
                fChildForm.intOriginalID = ResponsibilityTypeID;
                fChildForm.strPassedInResponsibilityTypeIDs = "11,12,31,32,41,42,901";
                fChildForm.intPassedInPersonTypeID = PersonTypeID;
                fChildForm.intIncludeBlankRow = 0;
                if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
                {
                    if (ResponsibilityTypeID != fChildForm.intSelectedID)
                    {
                        currentRow["ResponsibilityTypeID"] = fChildForm.intSelectedID;
                        currentRow["ResponsibilityType"] = fChildForm.strSelectedDescription;
                        sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();
                        GridView view = (GridView)gridControl8.MainView;
                        view.FocusedColumn = colRemarks3;  // Move focus to next column so new value is shown in original column //
                    }
                }
            }
        }

        #endregion

        private void Add_Responsible_Person()
        {
            GridView view = (GridView)gridControl8.MainView;
            view.BeginDataUpdate();
            try
            {
                DataRow drNewRow;
                drNewRow = dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.NewRow();
                drNewRow["strMode"] = "add";
                drNewRow["strRecordIDs"] = "";
                drNewRow["StaffID"] = 0;
                drNewRow["ResponsibleForRecordID"] = 0;  // ClientContractID //
                drNewRow["ResponsibleForRecordTypeID"] = 2;  // 2 = OM Client Contract.  See OM_Master_Person_Responsibility_Record_Type Table //
                drNewRow["ResponsibilityTypeID"] = 11;  // 11 = Contract Director.  See OM_Master_Person_Responsibility_Type Table //
                drNewRow["StaffName"] = "Select a Value";
                drNewRow["PersonTypeID"] = 0;  // 0 = Staff, 1 = Client //
                drNewRow["PersonTypeDescription"] = "Staff";
                dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows.Add(drNewRow);
            }
            catch (Exception Ex) { }
            view.EndDataUpdate();
        }

        private void Delete_Responsible_Person()
        {
            if (strFormMode == "view") return;
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";

            view = (GridView)gridControl8.MainView;
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Person Responsibilities to remove from the Contract by clicking on them then try again.", "Remove Linked Person Responsibilities from Contract", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Checks passed so delete selected record(s) //
            strMessage = "You have " + (intCount == 1 ? "1 Linked Person Responsibility" : Convert.ToString(intRowHandles.Length) + " Linked Person Responsibilities") + " selected for removing from the current Contract!\n\nProceed?\n\nIf you proceed " + (intCount == 1 ? "this Linked Person Responsibility" : "these Linked Person Responsibilities") + " will be removed from the Contract.";
            if (DevExpress.XtraEditors.XtraMessageBox.Show(strMessage, "Remove Linked Contract Year from Client Contract", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager1.ShowWaitForm();
                splashScreenManager1.SetWaitFormDescription("Deleting...");

                int intChildMax = 0;
                GridView childView = (GridView)gridControl7.MainView;
                int intParentClientContractYearID = 0;
                for (int i = intRowHandles.Length - 1; i >= 0; i--)
                {
                    view.DeleteRow(intRowHandles[i]);  // Delete parent row //
                }

                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                if (this.GlobalSettings.ShowConfirmations == 1) DevExpress.XtraEditors.XtraMessageBox.Show(intCount.ToString() + " record(s) marked for unlinking from Contract.", "Remove Linked Person Responsibilities from Contract", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion


        #region Finish Page

        private void btnFinishPrevious_Click(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage = this.xtraTabPageStep5;
        }
        private void btnFinish_Click(object sender, EventArgs e)
        {
            // force changes back to dataset //
            this.BindingContext[dataLayoutControl1.DataSource, dataLayoutControl1.DataMember].EndCurrentEdit();  // Force any pending save from DataLayout to underlying table adapter //
            this.ValidateChildren();
            if (dxErrorProvider1.HasErrors)
            {
                string strErrors = GetInvalidDataEntryValues(dxErrorProvider1, dataLayoutControl1);
                DevExpress.XtraEditors.XtraMessageBox.Show("One or more missing\\incorrect values are contained within the current record!\n\n" + strErrors + "Tip: Errors have a warning\\stop icon next to them.\n\nPlease correct before proceeding.\n\n", "Record Navigation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            GridView view = (GridView)gridControl5.MainView;  // Linked Contract Years //
            view.PostEditor();
            view = (GridView)gridControl7.MainView;  // Linked Billing Profile //
            view.PostEditor();
            view = (GridView)gridControl8.MainView;  // Linked Person Responsibilities //
            view.PostEditor();

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Saving Changes...");

            // Save Client Contract //
            sp06059OMClientContractEditBindingSource.EndEdit();
            try
            {
                sp06059_OM_Client_Contract_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
            }
            catch (System.Exception ex)
            {
                if (splashScreenManager1.IsSplashFormVisible)
                {
                    splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                    splashScreenManager1.CloseWaitForm();
                }
                XtraMessageBox.Show("An error occurred while saving the record changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }


            // Pick up the new ID so it can be passed back to the manager so the new record can be highlighted //
            string strNewIDs = "";
            int intClientContractID = 0;
            DataRowView currentRow = (DataRowView)sp06059OMClientContractEditBindingSource.Current;
            if (currentRow != null)
            {
                strNewIDs = currentRow["ClientContractID"].ToString() + ";";
                intClientContractID = Convert.ToInt32(currentRow["ClientContractID"]);
                if (this.strFormMode.ToLower() == "add")
                {
                    // Switch mode to Edit so than any subsequent changes update this record //
                    this.strFormMode = "edit";
                    if (currentRow != null)
                    {
                        currentRow["strMode"] = "edit";
                        currentRow.Row.AcceptChanges();  // Commit the strMode column change value so save button is not re-enabled until any further user initiated data change is made //
                    }
                }
                strRecordIDs = intClientContractID.ToString() + ",";


                // Pass new ID to any records in the linked Person Responsibilities grid //
                view = (GridView)gridControl8.MainView;
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows)
                {
                    dr["ResponsibleForRecordID"] = intClientContractID;
                }
                sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
                view.EndUpdate();
                // Save the linked Person Responsibilities Records //
                try
                {
                    sp06081_OM_Client_Contract_Linked_Responsibilities_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                    foreach (DataRow dr in dataSet_OM_Contract.sp06081_OM_Client_Contract_Linked_Responsibilities_Edit.Rows)
                    {
                        dr["strMode"] = "edit";
                    }
                    sp06081OMClientContractLinkedResponsibilitiesEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
                }
                catch (System.Exception ex)
                {
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    XtraMessageBox.Show("An error occurred while saving the linked person responsibility changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }


                // Process Linked Contract Years - Copy VS generated IDs to dummy column so we can find related children later as the save will overwrite the VS generated auto-incrementing keys //
                view = (GridView)gridControl5.MainView;
                view.BeginUpdate();
                foreach (DataRow dr in dataSet_OM_Contract.sp06067_OM_Client_Contract_Year_Edit.Rows)
                {
                    dr["DummyClientContractYearID"] = dr["ClientContractYearID"];
                    dr["ClientContractID"] = intClientContractID;
                }
                sp06067OMClientContractYearEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();
                // Attempt to save any changes... //
                try
                {
                    sp06067_OM_Client_Contract_Year_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                    foreach (DataRow dr in dataSet_OM_Contract.sp06067_OM_Client_Contract_Year_Edit.Rows)
                    {
                        dr["strMode"] = "edit";
                    }
                    sp06067OMClientContractYearEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
                }
                catch (System.Exception ex)
                {
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    XtraMessageBox.Show("An error occurred while saving the linked Contract Year changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }


                // Process Linked Billing Profiles //
                view = (GridView)gridControl7.MainView;
                view.BeginUpdate();
                int intClientContractYearID = 0;
                int intDummyClientContractYearID = 0;
                foreach (DataRow dr in dataSet_OM_Contract.sp06067_OM_Client_Contract_Year_Edit.Rows)
                {
                    intClientContractYearID = Convert.ToInt32(dr["ClientContractYearID"]);
                    intDummyClientContractYearID = Convert.ToInt32(dr["DummyClientContractYearID"]);
                    foreach (DataRow dr2 in dataSet_OM_Contract.sp06069_OM_Client_Contract_Year_Billing_Profile_Edit.Rows)
                    {
                        if (Convert.ToInt32(dr2["ClientContractYearID"]) == intDummyClientContractYearID) dr2["ClientContractYearID"] = intClientContractYearID;
                    }
                }
                sp06069OMClientContractYearBillingProfileEditBindingSource.EndEdit();  // Commit any changs from grid to underlying datasource //
                view.EndUpdate();
                // Attempt to save any changes... //
                try
                {
                    sp06069_OM_Client_Contract_Year_Billing_Profile_EditTableAdapter.Update(dataSet_OM_Contract);  // Insert, Update and Delete queries defined in Table Adapter //
                    foreach (DataRow dr in dataSet_OM_Contract.sp06069_OM_Client_Contract_Year_Billing_Profile_Edit.Rows)
                    {
                        dr["strMode"] = "edit";
                    }
                    sp06069OMClientContractYearBillingProfileEditBindingSource.EndEdit();  // Commit any changes from grid to underlying datasource //
                }
                catch (System.Exception ex)
                {
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    XtraMessageBox.Show("An error occurred while saving the linked Contract Year Billing Profile changes [" + ex.Message + "]!\n\nTry saving again - if the problem persists, contact Technical Support.", "Save Changes", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }

            // Notify any open instances of Parent Manager they will need to refresh their data on activating //
            if (this.ParentForm != null)
            {
                foreach (Form frmChild in this.ParentForm.MdiChildren)
                {
                    if (frmChild.Name == "frm_OM_Client_Contract_Manager")
                    {
                        var fParentForm = (frm_OM_Client_Contract_Manager)frmChild;
                        fParentForm.UpdateFormRefreshStatus(1, Utils.enmFocusedGrid.Contract, strNewIDs);
                    }
                }
            }
            this.dataSet_OM_Contract.AcceptChanges();

            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }

            if (checkEdit2.Checked)  // Open Site Contract Wizard //
            {
                if (this.ParentForm != null)
                {
                    var fChildForm = new frm_OM_Site_Contract_Wizard();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.strCaller;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager2;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.i_str_PassedInClientContractIDs = intClientContractID.ToString() + ",";
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
                else
                {
                    XtraMessageBox.Show("Unable to open another form from this form... The form must be docked inside the main container form first.", "Open Site Contract Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else if (checkEdit3.Checked)  // Re-open Client Contract Wizard to create another Contract //
            {
                if (this.ParentForm != null)
                {
                    var fChildForm = new frm_OM_Client_Contract_Wizard();
                    fChildForm.MdiParent = this.MdiParent;
                    fChildForm.GlobalSettings = this.GlobalSettings;
                    fChildForm.strRecordIDs = "";
                    fChildForm.strFormMode = "add";
                    fChildForm.strCaller = this.strCaller;
                    fChildForm.intRecordCount = 0;
                    fChildForm.FormPermissions = this.FormPermissions;
                    DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager2 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                    fChildForm.splashScreenManager = splashScreenManager2;
                    fChildForm.splashScreenManager.ShowWaitForm();
                    fChildForm.Show();

                    System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                    if (method != null) method.Invoke(fChildForm, new object[] { null });
                }
                else
                {
                    XtraMessageBox.Show("Unable to open another form from this form... The form must be docked inside the main container form first.", "Open Client Contract Wizard", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }


            this.Close();
        }

        #endregion








    }
}

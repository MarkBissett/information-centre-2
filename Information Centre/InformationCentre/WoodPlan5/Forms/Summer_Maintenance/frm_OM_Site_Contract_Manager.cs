using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;  // Required for Path Statement //
using System.Xml;
using System.Linq;
using System.Xml.Linq;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraBars;
using DevExpress.XtraTab;
using DevExpress.XtraBars.Docking;

using DevExpress.Utils.Menu;  // Required to disable Column Chooser on Grids //
using DevExpress.XtraGrid.Localization;  // Required to disable Column Chooser on Grids //

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;
using LocusEffects;
using WoodPlan5.Classes.Operations;
using Utilities;  // Used by Datasets //

namespace WoodPlan5
{
    public partial class frm_OM_Site_Contract_Manager : BaseObjects.frmBase
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        bool isRunning = false;
        GridHitInfo downHitInfo = null;
        Boolean iBoolDontFireGridGotFocusOnDoubleClick = false;

        bool iBool_AllowDelete = false;
        bool iBool_AllowAdd = false;
        bool iBool_AllowEdit = false;
        bool iBool_TemplateManager = false;
        bool iBool_BillingRequirementTemplateManager = false;

        private Utils.enmFocusedGrid _enmFocusedGrid = Utils.enmFocusedGrid.Jobs;

        bool iBool_AddWizardButtonEnabled = false;
        bool iBool_ImportButtonEnabled = false;
        bool iBool_SetInactiveButtonEnabled = false;
        bool iBool_SetOnHoldButtonEnabled = false;
        bool iBool_SetOffHoldButtonEnabled = false;
        bool iBool_SetExpiryReasonEnabled = false;
        bool iBool_EnableGridColumnChooser = true;

        public int UpdateRefreshStatus = 0; // Controls if grid needs to refresh itself on activate when a child screen has updated it's data //

        public RefreshGridState RefreshGridViewStateContract;  // Used by Grid View State Facilities //
        public RefreshGridState RefreshGridViewStateYear;
        public RefreshGridState RefreshGridViewStateProfile;
        public RefreshGridState RefreshGridViewStateResponsibility;
        public RefreshGridState RefreshGridViewStateLabour;
        public RefreshGridState RefreshGridViewStateEquipment;
        public RefreshGridState RefreshGridViewStateMaterial;
        public RefreshGridState RefreshGridViewStateClientPO;
        public RefreshGridState RefreshGridViewStateJobRate;
        public RefreshGridState RefreshGridViewStateBillingRequirement;

        RepositoryItem emptyEditor;  // Used to conditionally hide the hyperlink editor //

        string i_str_AddedRecordIDsContract = "";  // Used to store the IDs of any newly added records so they can be selected when the data is reloaded //
        string i_str_AddedRecordIDsYear = "";
        string i_str_AddedRecordIDsProfile = "";
        string i_str_AddedRecordIDsResponsibility = "";
        string i_str_AddedRecordIDsLabour = "";
        string i_str_AddedRecordIDsEquipment = "";
        string i_str_AddedRecordIDsMaterial = ""; 
        string i_str_AddedRecordIDsClientPO = "";
        string i_str_AddedRecordIDsJobRate = "";
        string i_str_AddedRecordIDsBillingRequirement = "";

        string i_str_selected_ClientContract_ids = "";
        string i_str_selected_ClientContract_descriptions = "";

        string i_str_selected_CM_ids = "";
        string i_str_selected_CM_names = "";
        BaseObjects.GridCheckMarksSelection selection5;

        string i_str_selected_Construction_Manager_ids = "";
        string i_str_selected_Construction_Manager_names = "";
        BaseObjects.GridCheckMarksSelection selection3;

        Default_Screen_Settings default_screen_settings; // Last used settings for current user for the screen //
        private string strDefaultPath = "";  // Holds the first part of the path, retrieved from the System_Settings table //

        public string strPassedInDrillDownIDs = "";

        Bitmap bmpBlank = null;
        Bitmap bmpAlert = null;
        
        private DateTime i_dtStart = DateTime.MinValue;
        private DateTime i_dtEnd = DateTime.MaxValue;

        private LocusEffects.LocusEffectsProvider locusEffectsProvider1;
        private ArrowLocusEffect m_customArrowLocusEffect1 = null;

        SuperToolTip superToolTipClientContractFilter = null;
        SuperToolTipSetupArgs superToolTipSetupArgsClientContractFilter = null;

        private decimal _decDefaultVatRate = (decimal)0.00;

        private int intBillingRequirementLinkedRecordTypeID = 2;  // 2 = SiteContracts //

        #endregion

        public frm_OM_Site_Contract_Manager()
        {
            InitializeComponent();
        }

        private void frm_OM_Site_Contract_Manager_Load(object sender, EventArgs e)
        {
            this.LockThisWindow(); // ***** Unlocked in PostOpen event ***** //
            this.FormID = 7007;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            // Get Form Permissions //
            sp00039GetFormPermissionsForUserTableAdapter.Connection.ConnectionString = strConnectionString;
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, FormID, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);
            ProcessPermissionsForForm();
            if (fProgress != null) fProgress.UpdateProgress(20); // Update Progress Bar //

            bmpBlank = new Bitmap(16, 16);
            bmpAlert = new Bitmap(imageCollection1.Images[6], 16, 16);

            try
            {
                var GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                _decDefaultVatRate = Convert.ToDecimal(GetSetting.sp00043_RetrieveSingleSystemSetting(3, "GrittingDefaultVatRate"));
            }
            catch (Exception)
            {
                _decDefaultVatRate = (decimal)0.00;
            }

            try
            {
                DataSet_ATTableAdapters.QueriesTableAdapter GetSetting = new DataSet_ATTableAdapters.QueriesTableAdapter();
                GetSetting.ChangeConnectionString(strConnectionString);
                strDefaultPath = GetSetting.sp00043_RetrieveSingleSystemSetting(7, "OM_PictureFilesFolder").ToString();
            }
            catch (Exception)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while trying to obtain the Folder Path for Job Pictures (from the System Configuration Screen).\n\nPlease close this screen then try again. If the problem persists, contact Technical Support.", "Get Linked Job Pictures Folder Path", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            
            sp06088_OM_Site_Contract_ManagerTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateContract = new RefreshGridState(gridViewContract, "SiteContractID");

            i_dtStart = DateTime.Today.AddMonths(-6);
            i_dtEnd = DateTime.Today.AddMonths(6);
            dateEditFromDate.DateTime = i_dtStart;
            dateEditToDate.DateTime = i_dtEnd;
            popupContainerEditDateRange.EditValue = PopupContainerEditDateRange_Get_Selected();

            sp06345_OM_Site_Contract_Manager_Linked_Job_RatesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateJobRate = new RefreshGridState(gridViewJobRate, "SiteContractJobRateID");

            sp06110_OM_Site_Contract_Manager_Linked_YearsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateYear = new RefreshGridState(gridViewYear, "SiteContractYearID");
            
            sp06111_OM_Site_Contract_Manager_Linked_ProfilesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateProfile = new RefreshGridState(gridViewProfile, "SiteContractProfileID");
            
            sp06112_OM_Site_Contract_Manager_Linked_ResponsibilitiesTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateResponsibility = new RefreshGridState(gridViewResponsibility, "PersonResponsibilityID");


            sp06113_OM_Site_Contract_Manager_Linked_LabourTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateLabour = new RefreshGridState(gridViewLabour, "SiteContractLabourCostID");

            sp06114_OM_Site_Contract_Manager_Linked_EquipmentTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateEquipment = new RefreshGridState(gridViewEquipment, "SiteContractEquipmentCostID");

            sp06115_OM_Site_Contract_Manager_Linked_MaterialsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateMaterial = new RefreshGridState(gridViewMaterial, "SiteContractMaterialCostID");

            sp06326_OM_Client_POs_Linked_To_ParentTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateClientPO = new RefreshGridState(gridViewClientPO, "ClientPurchaseOrderLinkID");

            sp01037_Core_Billing_Requirement_ItemsTableAdapter.Connection.ConnectionString = strConnectionString;
            RefreshGridViewStateBillingRequirement = new RefreshGridState(gridViewBillingRequirement, "BillingRequirementID");

            // Add record selection checkboxes to popup KAM Filter grid control //
            selection5 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl5.MainView);
            selection5.CheckMarkColumn.VisibleIndex = 0;
            selection5.CheckMarkColumn.Width = 30;
            sp06020_OM_Job_Manager_CMs_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06020_OM_Job_Manager_CMs_FilterTableAdapter.Fill(dataSet_GC_Summer_Core.sp06020_OM_Job_Manager_CMs_Filter);
            gridControl5.ForceInitialize();

            // Add record selection checkboxes to popup Construction Manager Filter grid control //
            selection3 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl3.MainView);
            selection3.CheckMarkColumn.VisibleIndex = 0;
            selection3.CheckMarkColumn.Width = 30;
            sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06388_OM_Client_Contract_Manager_Construction_Manager_FilterTableAdapter.Fill(dataSet_OM_Contract.sp06388_OM_Client_Contract_Manager_Construction_Manager_Filter);
            gridControl3.ForceInitialize();

            if (strPassedInDrillDownIDs != "")  // Opened in drill-down mode //
            {
                popupContainerEditDateRange.EditValue = "Custom Filter";
                buttonEditClientFilter.EditValue = "Custom Filter";
                popupContainerEditCMFilter.Text = "Custom Filter";
                popupContainerEditConstructionManager.Text = "Custom Filter";
                Load_Data();  // Load records //
            }
            popupContainerControlDateRange.Size = new System.Drawing.Size(200, 107);
            popupContainerControlCMFilter.Size = new System.Drawing.Size(270, 400);
            popupContainerControlConstructionManagerFilter.Size = new System.Drawing.Size(270, 400);

            // Create a SuperToolTip //
            superToolTipClientContractFilter = new SuperToolTip();
            superToolTipSetupArgsClientContractFilter = new SuperToolTipSetupArgs();
            superToolTipSetupArgsClientContractFilter.Title.Text = "Client Contract Filter - Information";
            superToolTipSetupArgsClientContractFilter.Title.Image = global::WoodPlan5.Properties.Resources.Info_32x32;
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client ContractFilter.\n\nNo Filter Set.";
            superToolTipSetupArgsClientContractFilter.ShowFooterSeparator = false;
            superToolTipSetupArgsClientContractFilter.Footer.Text = "";
            superToolTipClientContractFilter.Setup(superToolTipSetupArgsClientContractFilter);
            superToolTipClientContractFilter.AllowHtmlText = DefaultBoolean.True;
            buttonEditClientFilter.SuperTip = superToolTipClientContractFilter;

            emptyEditor = new RepositoryItem();
        }

        public override void PostOpen(object objParameter)
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            _KeepWaitFormOpen = false;
            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so load last saved screen settings for current user //
            {
                Application.DoEvents();  // Allow Form time to repaint itself //
                LoadLastSavedUserScreenSettings();
            }
        }

        private void frm_OM_Site_Contract_Manager_Activated(object sender, EventArgs e)
        {
            if (UpdateRefreshStatus > 0)
            {
                if (UpdateRefreshStatus == 1 || !string.IsNullOrEmpty(i_str_AddedRecordIDsContract))
                {
                    Load_Data();
                }
                if (UpdateRefreshStatus == 2 || !string.IsNullOrEmpty(i_str_AddedRecordIDsJobRate) || !string.IsNullOrEmpty(i_str_AddedRecordIDsYear) || !string.IsNullOrEmpty(i_str_AddedRecordIDsResponsibility) || !string.IsNullOrEmpty(i_str_AddedRecordIDsLabour) || !string.IsNullOrEmpty(i_str_AddedRecordIDsEquipment) || !string.IsNullOrEmpty(i_str_AddedRecordIDsMaterial) || !string.IsNullOrEmpty(i_str_AddedRecordIDsClientPO) || !string.IsNullOrEmpty(i_str_AddedRecordIDsBillingRequirement))
                {
                    LoadLinkedRecords();
                }
                if (UpdateRefreshStatus == 3 || !string.IsNullOrEmpty(i_str_AddedRecordIDsProfile))
                {
                    LoadLinkedRecords2();
                }
            }
            SetMenuStatus();
        }

        private void frm_OM_Site_Contract_Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (strPassedInDrillDownIDs == "")  // Not opened in drill-down mode so save screen settings for current user //
            {
                if (Control.ModifierKeys != Keys.Control)  // Only save settings if user is not holding down Ctrl key //
                {
                    // Store last used screen settings for current user //
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ClientContractFilter", i_str_selected_ClientContract_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ActiveOnly", checkEditActiveOnly.EditValue.ToString());
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "CMFilter", i_str_selected_CM_ids);
                    default_screen_settings.UpdateInMemoryDefaultScreenSetting(this.FormID, this.GlobalSettings.UserID, "ConstructionManagerFilter", i_str_selected_Construction_Manager_ids);
                    default_screen_settings.SaveDefaultScreenSettings();
                }
            }
        }


        public void LoadLastSavedUserScreenSettings()
        {
            // Load last used settings for current user for the screen //
            default_screen_settings = new Default_Screen_Settings();
            if (default_screen_settings.LoadDefaultScreenSettings(this.FormID, this.GlobalSettings.UserID, strConnectionString) == 1)
            {
                if (Control.ModifierKeys == Keys.Control) return;  // Only load settings if user is not holding down Ctrl key //

                int intFoundRow = 0;
                string strItemFilter = "";

                // Active Only //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ActiveOnly");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    checkEditActiveOnly.EditValue = (strItemFilter == "1" ? 1 : 0);
                }

                // Client Contract Filter //
                strItemFilter = default_screen_settings.RetrieveSetting("ClientContractFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    i_str_selected_ClientContract_ids = strItemFilter;
                    var ResolveIDs = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter();
                    ResolveIDs.ChangeConnectionString(strConnectionString);
                    try
                    {
                        //i_str_selected_ClientContract_ids = buttonEditClientFilter.EditValue.ToString();
                        i_str_selected_ClientContract_descriptions = ResolveIDs.sp06087_OM_Get_ClientsContracts_From_ClientContractIDs(strItemFilter).ToString();
                        buttonEditClientFilter.EditValue = i_str_selected_ClientContract_descriptions;
                        
                        // Update Filter control's tooltip //
                        string strTooltipText = i_str_selected_ClientContract_descriptions.Replace(", ", "\n");
                        superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Contract Filter.\n\n" + strTooltipText;
                    }
                    catch (Exception) { }
                }

                // CM Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("CMFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl5.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditCMFilter.Text = PopupContainerEditCMFilter_Get_Selected();
                }

                // Construction Manager Filter //
                intFoundRow = 0;
                strItemFilter = default_screen_settings.RetrieveSetting("ConstructionManagerFilter");
                if (!string.IsNullOrEmpty(strItemFilter))
                {
                    Array arrayItems = strItemFilter.Split(',');  // Single quotes because char expected for delimeter //
                    GridView viewFilter = (GridView)gridControl3.MainView;
                    viewFilter.BeginUpdate();
                    intFoundRow = 0;
                    foreach (string strElement in arrayItems)
                    {
                        if (strElement == "") break;
                        intFoundRow = viewFilter.LocateByValue(0, viewFilter.Columns["ID"], Convert.ToInt32(strElement));
                        if (intFoundRow != GridControl.InvalidRowHandle)
                        {
                            viewFilter.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                            viewFilter.MakeRowVisible(intFoundRow, false);
                        }
                    }
                    viewFilter.EndUpdate();
                    popupContainerEditConstructionManager.Text = PopupContainerEditConstructionManagerFilter_Get_Selected();
                }

                Load_Data();


                // Prepare LocusEffects and add custom effect //
                locusEffectsProvider1 = new LocusEffectsProvider();
                locusEffectsProvider1.Initialize();
                locusEffectsProvider1.FramesPerSecond = 30;
                m_customArrowLocusEffect1 = new ArrowLocusEffect();
                m_customArrowLocusEffect1.Name = "CustomeArrow1";
                m_customArrowLocusEffect1.AnimationStartColor = Color.Orange;
                m_customArrowLocusEffect1.AnimationEndColor = Color.Red;
                m_customArrowLocusEffect1.MovementMode = MovementMode.OneWayAlongVector;
                m_customArrowLocusEffect1.MovementCycles = 20;
                m_customArrowLocusEffect1.MovementAmplitude = 200;
                m_customArrowLocusEffect1.MovementVectorAngle = 45; //degrees
                m_customArrowLocusEffect1.LeadInTime = 0; //msec
                m_customArrowLocusEffect1.LeadOutTime = 1000; //msec
                m_customArrowLocusEffect1.AnimationTime = 2000; //msec
                locusEffectsProvider1.AddLocusEffect(m_customArrowLocusEffect1);

                Point location = dockPanelFilters.PointToScreen(Point.Empty);

                System.Drawing.Point screenPoint = new System.Drawing.Point(location.X + 10, location.Y + 5);
                locusEffectsProvider1.ShowLocusEffect(this, screenPoint, m_customArrowLocusEffect1.Name);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            Load_Data();
        }

        private void Load_Data()
        {
            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            if (!splashScreenManager.IsSplashFormVisible)
            {
                this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                splashScreenManager.ShowWaitForm();
                splashScreenManager.SetWaitFormDescription("Loading Site Contracts...");
            }

            if (i_str_selected_ClientContract_ids == null) i_str_selected_ClientContract_ids = "";
            int intActive = Convert.ToInt32(checkEditActiveOnly.EditValue);
            RefreshGridViewStateContract.SaveViewInfo();
            GridView view = (GridView)gridControlContract.MainView;
            view.BeginUpdate();
            try
            {
                if (buttonEditClientFilter.EditValue.ToString() == "Custom Filter" && strPassedInDrillDownIDs != "")  // Load passed in Callouts //
                {
                    sp06088_OM_Site_Contract_ManagerTableAdapter.Fill(this.dataSet_OM_Contract.sp06088_OM_Site_Contract_Manager, strPassedInDrillDownIDs, i_dtStart, i_dtEnd, "", 0, "", "");
                    this.RefreshGridViewStateContract.LoadViewInfo();  // Reload any expanded groups and selected rows //
                    view.ExpandAllGroups();
                }
                else // Load users selection //
                {
                    sp06088_OM_Site_Contract_ManagerTableAdapter.Fill(this.dataSet_OM_Contract.sp06088_OM_Site_Contract_Manager, "", i_dtStart, i_dtEnd, i_str_selected_ClientContract_ids, intActive, i_str_selected_CM_ids, i_str_selected_Construction_Manager_ids);
                    this.RefreshGridViewStateContract.LoadViewInfo();  // Reload any expanded groups and selected rows //
                }
            }
            catch (Exception ex)
            {
                 DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while Loading Site Contract Data.\n\nMessage = [" + ex.Message + "].\n\nIf the error is a result of a query timeout, try adjusting the parameters at the top of the screen to reduce the amount of data returned then try again. If the problem persists, contact Technical Support.", "Load Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);

            }
            view.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsContract != "")
            {
                char[] delimiters = new char[] { ';' };
                string[] strArray = i_str_AddedRecordIDsContract.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                int intID = 0;
                int intRowHandle = 0;
                //GridView view = (GridView)gridControl1.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SiteContractID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsContract = "";
                view.EndSelection();
            }

            if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
        }

        private void LoadLinkedRecords()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlContract.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["SiteContractID"])) + ',';
            }
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            gridControlYear.MainView.BeginUpdate();
            RefreshGridViewStateYear.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06110_OM_Site_Contract_Manager_Linked_Years.Clear();
            }
            else
            {
                sp06110_OM_Site_Contract_Manager_Linked_YearsTableAdapter.Fill(dataSet_OM_Contract.sp06110_OM_Site_Contract_Manager_Linked_Years, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateYear.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlYear.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsYear != "")
            {
                strArray = i_str_AddedRecordIDsYear.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlYear.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SiteContractYearID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsYear = "";
                view.EndSelection();
            }


            gridControlResponsibility.MainView.BeginUpdate();
            RefreshGridViewStateResponsibility.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06112_OM_Site_Contract_Manager_Linked_Responsibilities.Clear();
            }
            else
            {
                sp06112_OM_Site_Contract_Manager_Linked_ResponsibilitiesTableAdapter.Fill(dataSet_OM_Contract.sp06112_OM_Site_Contract_Manager_Linked_Responsibilities, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateResponsibility.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlResponsibility.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsResponsibility != "")
            {
                strArray = i_str_AddedRecordIDsResponsibility.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlResponsibility.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["PersonResponsibilityID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsResponsibility = "";
                view.EndSelection();
            }


            gridControlLabour.MainView.BeginUpdate();
            RefreshGridViewStateLabour.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06113_OM_Site_Contract_Manager_Linked_Labour.Clear();
            }
            else
            {
                sp06113_OM_Site_Contract_Manager_Linked_LabourTableAdapter.Fill(dataSet_OM_Contract.sp06113_OM_Site_Contract_Manager_Linked_Labour, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateLabour.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlLabour.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsLabour != "")
            {
                strArray = i_str_AddedRecordIDsLabour.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlLabour.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SiteContractLabourCostID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsLabour = "";
                view.EndSelection();
            }


            gridControlEquipment.MainView.BeginUpdate();
            RefreshGridViewStateEquipment.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06114_OM_Site_Contract_Manager_Linked_Equipment.Clear();
            }
            else
            {
                sp06114_OM_Site_Contract_Manager_Linked_EquipmentTableAdapter.Fill(dataSet_OM_Contract.sp06114_OM_Site_Contract_Manager_Linked_Equipment, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateEquipment.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlEquipment.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsEquipment != "")
            {
                strArray = i_str_AddedRecordIDsEquipment.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlEquipment.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SiteContractEquipmentCostID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsEquipment = "";
                view.EndSelection();
            }


            gridControlMaterial.MainView.BeginUpdate();
            RefreshGridViewStateMaterial.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06115_OM_Site_Contract_Manager_Linked_Materials.Clear();
            }
            else
            {
                sp06115_OM_Site_Contract_Manager_Linked_MaterialsTableAdapter.Fill(dataSet_OM_Contract.sp06115_OM_Site_Contract_Manager_Linked_Materials, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateMaterial.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlMaterial.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsMaterial != "")
            {
                strArray = i_str_AddedRecordIDsMaterial.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlMaterial.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SiteContractMaterialCostID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsMaterial = "";
                view.EndSelection();
            }


            gridControlClientPO.MainView.BeginUpdate();
            RefreshGridViewStateClientPO.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Client_PO.sp06326_OM_Client_POs_Linked_To_Parent.Clear();
            }
            else
            {
                sp06326_OM_Client_POs_Linked_To_ParentTableAdapter.Fill(dataSet_OM_Client_PO.sp06326_OM_Client_POs_Linked_To_Parent, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), 1);
                RefreshGridViewStateClientPO.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlClientPO.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsClientPO != "")
            {
                strArray = i_str_AddedRecordIDsClientPO.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlClientPO.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["ClientPurchaseOrderLinkID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsClientPO = "";
                view.EndSelection();
            }


            gridControlJobRate.MainView.BeginUpdate();
            RefreshGridViewStateJobRate.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06345_OM_Site_Contract_Manager_Linked_Job_Rates.Clear();
            }
            else
            {
                sp06345_OM_Site_Contract_Manager_Linked_Job_RatesTableAdapter.Fill(dataSet_OM_Contract.sp06345_OM_Site_Contract_Manager_Linked_Job_Rates, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateJobRate.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlJobRate.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsJobRate != "")
            {
                strArray = i_str_AddedRecordIDsJobRate.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlJobRate.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SiteContractJobRateID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsJobRate = "";
                view.EndSelection();
            }


            gridControlBillingRequirement.MainView.BeginUpdate();
            RefreshGridViewStateBillingRequirement.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_Common_Functionality.sp01037_Core_Billing_Requirement_Items.Clear();
            }
            else
            {
                sp01037_Core_Billing_Requirement_ItemsTableAdapter.Fill(dataSet_Common_Functionality.sp01037_Core_Billing_Requirement_Items, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), intBillingRequirementLinkedRecordTypeID);
                RefreshGridViewStateBillingRequirement.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlBillingRequirement.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsBillingRequirement != "")
            {
                strArray = i_str_AddedRecordIDsBillingRequirement.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlBillingRequirement.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["BillingRequirementID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsBillingRequirement = "";
                view.EndSelection();
            }

        }

        private void LoadLinkedRecords2()
        {
            if (splitContainerControl1.Collapsed) return;  // Don't bother loading related data as the parent panel is collapsed so the grids are invisible //

            if (UpdateRefreshStatus > 0) UpdateRefreshStatus = 0;
            GridView view = (GridView)gridControlYear.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            string strSelectedIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strSelectedIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, view.Columns["SiteContractYearID"])) + ',';
            }
            char[] delimiters = new char[] { ';' };
            string[] strArray = null;
            int intID = 0;

            gridControlProfile.MainView.BeginUpdate();
            RefreshGridViewStateProfile.SaveViewInfo();  // Store expanded groups and selected rows //
            if (intCount == 0)
            {
                dataSet_OM_Contract.sp06111_OM_Site_Contract_Manager_Linked_Profiles.Clear();
            }
            else
            {
                sp06111_OM_Site_Contract_Manager_Linked_ProfilesTableAdapter.Fill(dataSet_OM_Contract.sp06111_OM_Site_Contract_Manager_Linked_Profiles, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs));
                RefreshGridViewStateProfile.LoadViewInfo();  // Reload any expanded groups and selected rows //
            }
            gridControlProfile.MainView.EndUpdate();

            // Highlight any recently added new rows //
            if (i_str_AddedRecordIDsProfile != "")
            {
                strArray = i_str_AddedRecordIDsProfile.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                intID = 0;
                int intRowHandle = 0;
                view = (GridView)gridControlProfile.MainView;
                view.BeginSelection();
                view.ClearSelection(); // Clear any current selection so just the new record is selected //
                foreach (string strElement in strArray)
                {
                    intID = Convert.ToInt32(strElement);
                    intRowHandle = view.LocateByValue(0, view.Columns["SiteContractProfileID"], intID);
                    if (intRowHandle != GridControl.InvalidRowHandle)
                    {
                        view.SelectRow(intRowHandle);
                        view.MakeRowVisible(intRowHandle, false);
                    }
                }
                i_str_AddedRecordIDsProfile = "";
                view.EndSelection();
            }

        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            Load_Data();
        }

        public void UpdateFormRefreshStatus(int status, Utils.enmFocusedGrid grid, string newIds)
        {
            // Called by child edit screens to notify parent of a required refresh to underlying data //
            UpdateRefreshStatus = status;
            switch (grid)
            {
                case Utils.enmFocusedGrid.Contract:
                    i_str_AddedRecordIDsContract = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsContract : newIds);
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    i_str_AddedRecordIDsJobRate = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsJobRate : newIds);
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    i_str_AddedRecordIDsYear = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsYear : newIds);
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    i_str_AddedRecordIDsProfile = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsProfile : newIds);
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    i_str_AddedRecordIDsResponsibility = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsResponsibility : newIds);
                    break;
                case Utils.enmFocusedGrid.Labour:
                    i_str_AddedRecordIDsLabour = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsLabour : newIds);
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    i_str_AddedRecordIDsEquipment = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsEquipment : newIds);
                    break;
                case Utils.enmFocusedGrid.Materials:
                    i_str_AddedRecordIDsMaterial = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsMaterial : newIds);
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    i_str_AddedRecordIDsClientPO = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsClientPO : newIds);
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    i_str_AddedRecordIDsBillingRequirement = (string.IsNullOrWhiteSpace(newIds) ? i_str_AddedRecordIDsBillingRequirement : newIds);
                    break;
                default:
                    break;
            }
        }


        private void ProcessPermissionsForForm()
        {
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        if (sfpPermissions.blCreate)
                        {
                            iBool_AllowAdd = true;
                        }
                        if (sfpPermissions.blUpdate)
                        {
                            iBool_AllowEdit = true;
                        }
                        if (sfpPermissions.blDelete)
                        {
                            iBool_AllowDelete = true;
                        }
                        break;
                    case 1:  // Add Wizard Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_AddWizardButtonEnabled = true;
                        }
                        break;
                    case 2:  // Import Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_ImportButtonEnabled = true;
                        }
                        break;
                    case 3:  // Set In-Active Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_SetInactiveButtonEnabled = true;
                        }
                        break;
                    case 4:  // Set In-Active Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_SetOnHoldButtonEnabled = true;
                        }
                        break;
                    case 5:  // Set In-Active Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_SetOffHoldButtonEnabled = true;
                        }
                        break;
                    case 6:  // Set Site Contract Expiry Reason Button //    
                        if (sfpPermissions.blRead)
                        {
                            iBool_SetExpiryReasonEnabled = true;
                        }
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7008, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7008 //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_BillingRequirementTemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
            sp00039GetFormPermissionsForUserTableAdapter.Fill(dataSet_AT.sp00039GetFormPermissionsForUser, 7018, GlobalSettings.UserID, 0, GlobalSettings.ViewedPeriodID);  // 7018 //
            for (int i = 0; i < this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows.Count; i++)
            {
                stcFormPermissions sfpPermissions = new stcFormPermissions();  // Hold permissions in array //
                sfpPermissions.intFormID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["PartID"]);
                sfpPermissions.intSubPartID = Convert.ToInt32(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["SubPartID"]);
                sfpPermissions.blCreate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["CreateAccess"]);
                sfpPermissions.blRead = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["ReadAccess"]);
                sfpPermissions.blUpdate = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["UpdateAccess"]);
                sfpPermissions.blDelete = Convert.ToBoolean(this.dataSet_AT.sp00039GetFormPermissionsForUser.Rows[i]["DeleteAccess"]);

                this.FormPermissions.Add(sfpPermissions);
                switch (sfpPermissions.intSubPartID)
                {
                    case 0:  // Whole Form //    
                        iBool_TemplateManager = sfpPermissions.blRead;
                        break;
                }
            }
        }

        public void SetMenuStatus()
        {
            ArrayList alItems = new ArrayList();

            bsiAdd.Enabled = false;
            bbiSingleAdd.Enabled = false;
            bbiBlockAdd.Enabled = false;
            bsiEdit.Enabled = false;
            bbiSingleEdit.Enabled = false;
            bbiBlockEdit.Enabled = false;
            bbiDelete.Enabled = false;
            bbiSave.Enabled = false;

            bbiSelectAll.Enabled = false;
            bbiUndo.Enabled = false;
            bbiRedo.Enabled = false;
            bbiCut.Enabled = false;
            bbiCopy.Enabled = false;
            bbiPaste.Enabled = false;
            bbiClear.Enabled = false;
            bbiSpellChecker.Enabled = false;

            bsiDataset.Enabled = false;
            bbiDatasetSelection.Enabled = false;
            bbiDatasetSelectionInverted.Enabled = false;
            bsiDataset.Enabled = false;
            bbiDatasetCreate.Enabled = false;
            bbiDatasetManager.Enabled = false;

            alItems.AddRange(new string[] { "sbiSelectAll", "iDefaultView", "iSelectGroup", "iSelectAll", "sbiFind", "iFind", "iPreview", "iPrint" });
            GridView view = (GridView)gridControlContract.MainView;
            GridView ParentView = (GridView)gridControlContract.MainView;
            GridView ChildParentView = (GridView)gridControlYear.MainView;

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    view = (GridView)gridControlContract.MainView;
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    view = (GridView)gridControlJobRate.MainView;
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    view = (GridView)gridControlYear.MainView;
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    view = (GridView)gridControlProfile.MainView;
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    view = (GridView)gridControlResponsibility.MainView;
                    break;
                case Utils.enmFocusedGrid.Labour:
                    view = (GridView)gridControlLabour.MainView;
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    view = (GridView)gridControlEquipment.MainView;
                    break;
                case Utils.enmFocusedGrid.Materials:
                    view = (GridView)gridControlMaterial.MainView;
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    view = (GridView)gridControlClientPO.MainView;
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    view = (GridView)gridControlBillingRequirement.MainView;
                    break;
                default: 
                    break;
            }
            int[] intRowHandles = view.GetSelectedRows();
            int[] intParentRowHandles = ParentView.GetSelectedRows();
            int[] intChildParentRowHandles = ChildParentView.GetSelectedRows();

            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                case Utils.enmFocusedGrid.JobRate:
                case Utils.enmFocusedGrid.ContractYear:
                case Utils.enmFocusedGrid.ContractYearProfile:
                case Utils.enmFocusedGrid.PersonResponsibility:
                case Utils.enmFocusedGrid.Labour:
                case Utils.enmFocusedGrid.Equipment:
                case Utils.enmFocusedGrid.Materials:
                case Utils.enmFocusedGrid.ClientPOLink:
                case Utils.enmFocusedGrid.BillingRequirement:
                    if (iBool_AllowAdd)
                    {
                        alItems.AddRange(new string[] { "iAdd", "sbiAdd" });
                        bsiAdd.Enabled = true;
                        bbiSingleAdd.Enabled = true;
                    }
                    if (iBool_AllowEdit && intRowHandles.Length >= 1)
                    {
                        alItems.AddRange(new string[] { "iEdit", "sbiEdit" });
                        bsiEdit.Enabled = true;
                        bbiSingleEdit.Enabled = true;
                        if (intRowHandles.Length >= 2)
                        {
                            alItems.Add("iBlockEdit");
                            bbiBlockEdit.Enabled = true;
                        }
                    }
                    if (iBool_AllowDelete && intRowHandles.Length >= 1)
                    {
                        alItems.Add("iDelete");
                        bbiDelete.Enabled = true;
                    }
                    break;
                default:
                    break;
            }
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        bbiBlockAdd.Enabled = true;
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        bbiBlockAdd.Enabled = (iBool_AllowAdd && intParentRowHandles.Length > 1);
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                case Utils.enmFocusedGrid.ContractYear:
                case Utils.enmFocusedGrid.PersonResponsibility:
                case Utils.enmFocusedGrid.Labour:
                case Utils.enmFocusedGrid.Equipment:
                case Utils.enmFocusedGrid.Materials:
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        bbiBlockAdd.Enabled = (iBool_AllowAdd && intParentRowHandles.Length > 1);
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        bbiBlockAdd.Enabled = (iBool_AllowAdd && intChildParentRowHandles.Length > 1);
                    }
                    break;
            }
            bsiAuditTrail.Enabled = (intRowHandles.Length > 0);
            bbiViewAuditTrail.Enabled = (intRowHandles.Length > 0);


            frmMain2 frmParent = (frmMain2)this.ParentForm;
            if (frmParent != null) frmParent.PermissionsHandler(alItems);

            // Set enabled status of navigator custom buttons //
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intParentRowHandles.Length > 0);
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intParentRowHandles.Length > 0);
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intParentRowHandles.Length > 0);
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intParentRowHandles.Length == 1);
            gridControlContract.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intParentRowHandles.Length > 0);
            bbiInActive.Enabled = (iBool_SetInactiveButtonEnabled && intParentRowHandles.Length > 0);
            bbiOnHoldSiteContract.Enabled = (iBool_SetOnHoldButtonEnabled && intParentRowHandles.Length > 0);
            bbiOffHoldSiteContract.Enabled = (iBool_SetOffHoldButtonEnabled && intParentRowHandles.Length > 0);
            bbiSetContractExpiryReason.Enabled = (iBool_SetExpiryReasonEnabled && intParentRowHandles.Length > 0);
            

            view = (GridView)gridControlJobRate.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlJobRate.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlYear.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowAdd && intParentRowHandles.Length > 0);
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlYear.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlProfile.MainView;
            intRowHandles = view.GetSelectedRows();
            ChildParentView = (GridView)gridControlYear.MainView;
            intChildParentRowHandles = ChildParentView.GetSelectedRows();
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowAdd && intChildParentRowHandles.Length > 0);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowAdd && intChildParentRowHandles.Length > 0);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_TemplateManager);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlProfile.EmbeddedNavigator.Buttons.CustomButtons[6].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlResponsibility.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlResponsibility.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlLabour.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = iBool_AllowAdd;
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);
            gridControlLabour.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlEquipment.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = iBool_AllowAdd;
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlEquipment.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlMaterial.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = iBool_AllowAdd;
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlMaterial.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlClientPO.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlClientPO.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlClientPO.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlClientPO.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlClientPO.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);

            view = (GridView)gridControlBillingRequirement.MainView;
            intRowHandles = view.GetSelectedRows();
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = iBool_AllowAdd;
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = (iBool_AllowEdit && intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[2].Enabled = (iBool_AllowDelete && intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[3].Enabled = (intRowHandles.Length > 0);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[4].Enabled = (iBool_AllowAdd);
            gridControlBillingRequirement.EmbeddedNavigator.Buttons.CustomButtons[5].Enabled = (iBool_BillingRequirementTemplateManager);

            // Set Enabled Status of Main Toolbar Buttons //
            bsiWizard.Enabled = iBool_AddWizardButtonEnabled;
            bbiAddWizard.Enabled = iBool_AddWizardButtonEnabled;
            bbiAddVisitWizard.Enabled = iBool_AddWizardButtonEnabled;
            bbiImport.Enabled = iBool_ImportButtonEnabled;
        }

        public override void OnAddEvent(object sender, EventArgs e)
        {
            Add_Record();
        }

        public override void OnBlockAddEvent(object sender, EventArgs e)
        {
            Block_Add();
        }

        public override void OnBlockEditEvent(object sender, EventArgs e)
        {
            Block_Edit();
        }

        public override void OnEditEvent(object sender, EventArgs e)
        {
            Edit_Record();
        }

        public override void OnDeleteEvent(object sender, EventArgs e)
        {
            Delete_Record();
        }

        private void Add_Record()
        {
            GridView view = null;
            GridView ParentView = null;
            int[] intRowHandles;
            System.Reflection.MethodInfo method = null;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Site_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlJobRate.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Site_Contract_Job_Rate_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06088_OM_Site_Contract_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.SiteContractID;
                            fChildForm.strLinkedToRecordDesc = row.SiteName;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(ParentView.GetRowCellValue(intRowHandles[0], "LinkedToParent").ToString())
                                                                + ", Site: " + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString());
                            fChildForm.dtPassedInContractStartDate = (DBNull.Value.Equals(row["StartDate"]) ? new DateTime(2000, 1, 1) : row.StartDate);
                            fChildForm.dtPassedInContractEndDate = (DBNull.Value.Equals(row["EndDate"]) ? new DateTime(2500, 12, 31) : row.EndDate); 
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Site_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06088_OM_Site_Contract_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.SiteContractID;
                            fChildForm.strLinkedToRecordDesc = row.SiteName;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(ParentView.GetRowCellValue(intRowHandles[0], "LinkedToParent").ToString())
                                                                + ", Site: " + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString());
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Site_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlYear.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06110_OM_Site_Contract_Manager_Linked_YearsRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.SiteContractYearID;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(row.LinkedToParent) + ", Year: "
                                                                + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "YearDescription").ToString()) ? "Unknown Year" : ParentView.GetRowCellValue(intRowHandles[0], "YearDescription").ToString());

                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "21,22,41,42,901,902";
                        fChildForm.i_intPassedInRecordTypeID = 1;  // Site //
                        fChildForm.i_intPassedInPersonTypeID = 0;
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06088_OM_Site_Contract_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.SiteContractID;
                            fChildForm.strLinkedToRecordDesc = row.ClientName;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(ParentView.GetRowCellValue(intRowHandles[0], "LinkedToParent").ToString())
                                                                + ", Site: " + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString());
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Site_Contract_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06088_OM_Site_Contract_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.SiteContractID;
                            fChildForm.strLinkedToRecordDesc = row.SiteName;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(ParentView.GetRowCellValue(intRowHandles[0], "LinkedToParent").ToString())
                                                                + ", Site: " + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString());
                            fChildForm.strPassedInSitePostcode = (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SitePostcode").ToString());
                            fChildForm.dblPassedInSiteX = (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteLocationX").ToString()) ? (double)0.00 : Convert.ToDouble(ParentView.GetRowCellValue(intRowHandles[0], "SiteLocationX")));
                            fChildForm.dblPassedInSiteY = (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteLocationY").ToString()) ? (double)0.00 : Convert.ToDouble(ParentView.GetRowCellValue(intRowHandles[0], "SiteLocationY")));
                            fChildForm.intPassedInClientContractID = (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "ClientContractID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientContractID")));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Site_Contract_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06088_OM_Site_Contract_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.SiteContractID;
                            fChildForm.strLinkedToRecordDesc = row.SiteName;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(ParentView.GetRowCellValue(intRowHandles[0], "LinkedToParent").ToString())
                                                                + ", Site: " + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString());
                            fChildForm.intPassedInClientContractID = (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "ClientContractID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientContractID")));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Site_Contract_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06088_OM_Site_Contract_ManagerRow)rowView.Row;
                            fChildForm.intLinkedToRecordID = row.SiteContractID;
                            fChildForm.strLinkedToRecordDesc = row.SiteName;
                            fChildForm.strLinkedToRecordDesc = HtmlRemoval.StripTagsCharArray(ParentView.GetRowCellValue(intRowHandles[0], "LinkedToParent").ToString())
                                                                + ", Site: " + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString());
                            fChildForm.intPassedInClientContractID = (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "ClientContractID").ToString()) ? 0 : Convert.ToInt32(ParentView.GetRowCellValue(intRowHandles[0], "ClientContractID")));
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 1;
                        fChildForm._LinkedToRecordType = "Site Contract";

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06088_OM_Site_Contract_ManagerRow)rowView.Row;
                            fChildForm._ClientID = row.ClientID;
                            fChildForm._LinkedToRecordID = row.SiteContractID;
                            fChildForm._LinkedToRecord = HtmlRemoval.StripTagsCharArray(row.LinkedToParent) + ", Site: " + row.SiteName;
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowAdd) return;

                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();

                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = "";
                        fChildForm.strFormMode = "add";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = 0;
                        fChildForm.LinkedToRecordTypeID = 2;
                        fChildForm.LinkedToRecordType = "Site Contract";
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();

                        ParentView = (GridView)gridControlContract.MainView;
                        intRowHandles = ParentView.GetSelectedRows();
                        if (intRowHandles.Length == 1)
                        {
                            var rowView = (DataRowView)ParentView.GetRow(intRowHandles[0]);
                            var row = (DataSet_OM_Contract.sp06088_OM_Site_Contract_ManagerRow)rowView.Row;
                            fChildForm.ParentRecordId = row.SiteContractID;
                            fChildForm.ParentRecordDescription = "Client: " + ParentView.GetRowCellValue(intRowHandles[0], "ClientName").ToString()
                                                               + ", Contract: " + ParentView.GetRowCellValue(intRowHandles[0], "ContractDescription").ToString()
                                                               + ", Site: " + (string.IsNullOrWhiteSpace(ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString()) ? "" : ParentView.GetRowCellValue(intRowHandles[0], "SiteName").ToString());
                        }
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Add()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();

                        // Get parent Client ontract //
                        var fChildForm = new frm_OM_Select_Client_Contract();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.intOriginalSelectedID = 0;
                        if (fChildForm.ShowDialog() != DialogResult.OK) return;

                        int intSelectedClientContract = fChildForm.intSelectedID;
                        if (intSelectedClientContract <= 0) return;

                        // Get Sites //
                        var fChildForm2 = new frm_OM_Select_Client_Contract_Site();
                        fChildForm2.GlobalSettings = this.GlobalSettings;
                        fChildForm2.intOriginalID = 0;
                        fChildForm2.strPassedInClientContractIDs = intSelectedClientContract.ToString() + ",";
                        fChildForm2._Mode = "Multiple";
                        if (fChildForm2.ShowDialog() != DialogResult.OK) return;

                        string strSelectedSiteIDs = fChildForm2.strSelectedIDs;
                        if (string.IsNullOrWhiteSpace(strSelectedSiteIDs)) return;

                        // Open Edit Site Contract screen in BlockAdd mode //
                        var fChildForm1 = new frm_OM_Site_Contract_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strSelectedSiteIDs;
                        fChildForm1.intLinkedToRecordID = intSelectedClientContract;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Site_Contract_Job_Rate_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Site_Contract_Year_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractYearID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Site_Contract_Year_Billing_Profile_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.i_strPassedInRepsonsibilityTypes = "21,22,41,42,901,902";
                        fChildForm1.i_intPassedInRecordTypeID = 1;  // Site //
                        fChildForm1.i_intPassedInPersonTypeID = 0;
                        fChildForm1.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Site_Contract_Labour_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Site_Contract_Equipment_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm1 = new frm_OM_Site_Contract_Material_Edit();
                        fChildForm1.MdiParent = this.MdiParent;
                        fChildForm1.GlobalSettings = this.GlobalSettings;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.strFormMode = "blockadd";
                        fChildForm1.strCaller = this.Name;
                        fChildForm1.intRecordCount = intCount;
                        fChildForm1.strRecordIDs = strRecordIDs;
                        fChildForm1.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm1.splashScreenManager = splashScreenManager1;
                        fChildForm1.splashScreenManager.ShowWaitForm();
                        fChildForm1.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm1, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one parent Site Contract record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        int intClientID = 0;
                        string strClientName = "";
                        int intLastClientID = 0;
                        bool boolMultipleClients = false;
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                            intClientID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientID"));
                            strClientName = Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientName"));
                            if (intLastClientID == 0) intLastClientID = intClientID;
                            if (intLastClientID != intClientID) boolMultipleClients = true;
                        }

                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 1;
                        fChildForm._LinkedToRecordType = "Site Contract";
                        fChildForm._ClientID = (!boolMultipleClients ? intClientID : 0);
                        fChildForm._ClientName = (!boolMultipleClients ? strClientName : "");
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowAdd) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select at least one parent Site Contract record to block add to before proceeding.", "Block Add Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockadd";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.LinkedToRecordTypeID = 2;
                        fChildForm.LinkedToRecordType = "Site Contract";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Block_Edit()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJobRate.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractJobRateID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Job_Rate_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractYearID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractProfileID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PersonResponsibilityID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "21,22,41,42,901,902";
                        fChildForm.i_intPassedInRecordTypeID = 1;  // Site //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientPurchaseOrderLinkID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 1;
                        fChildForm._LinkedToRecordType = "Site Contract";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractLabourCostID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractEquipmentCostID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractMaterialCostID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 1)
                        {
                            XtraMessageBox.Show("Select more than one record to block edit before proceeding.", "Block Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BillingRequirementID")) + ',';
                        }

                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "blockedit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.LinkedToRecordTypeID = 2;
                        fChildForm.LinkedToRecordType = "Site Contract";
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Edit_Record()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJobRate.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractJobRateID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Job_Rate_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractYearID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractProfileID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PersonResponsibilityID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "21,22,41,42,901,902";
                        fChildForm.i_intPassedInRecordTypeID = 1;  // Site //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientPurchaseOrderLinkID")) + ',';
                        }

                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 1;
                        fChildForm._LinkedToRecordType = "Site Contract";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;

                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractLabourCostID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractEquipmentCostID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractMaterialCostID")) + ',';
                        }

                        var fChildForm = new frm_OM_Site_Contract_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to edit before proceeding.", "Edit Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BillingRequirementID")) + ',';
                        }

                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "edit";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.LinkedToRecordTypeID = 2;
                        fChildForm.LinkedToRecordType = "Site Contract";
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }

        private void Delete_Record()
        {
            int[] intRowHandles;
            int intCount = 0;
            GridView view = null;
            string strMessage = "";
            string strRecordIDs = "";
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlContract.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Contracts to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Site Contract" : Convert.ToString(intRowHandles.Length) + " Site Contracts") + " selected for delete!\n\nProceed?\n\n<color=red>WARNING, WARNING, WARNING: If you proceed " + (intCount == 1 ? "this Site Contract" : "these Site Contracts") + " will no longer be available for selection and any related records will also be deleted!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("site_contract", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            Load_Data();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;

                case Utils.enmFocusedGrid.JobRate:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlJobRate.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Site Contract Job Rates to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Site Contract Job Rate" : Convert.ToString(intRowHandles.Length) + " Linked Site Contract Job Rates") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Site Contract Job Rate" : "these Linked Site Contract Job Rates") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractJobRateID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("site_contract_job_rate", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;

                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlYear.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Site Contract Years to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Site Contract Year" : Convert.ToString(intRowHandles.Length) + " Linked Site Contract Years") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Site Contract Year" : "these Linked Site Contract Years") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractYearID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("site_contract_year", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlProfile.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Billing Profiles to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Billing Profile" : Convert.ToString(intRowHandles.Length) + " Linked Billing Profiles") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Billing Profile" : "these Linked Billing Profiles") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractProfileID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("site_contract_year_billing_profile", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords2();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Person Responsibilities to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Person Responsibility" : Convert.ToString(intRowHandles.Length) + " Linked Person Responsibilities") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Person Responsibility" : "these Linked Person Responsibilities") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "PersonResponsibilityID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("person_responsibility", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlClientPO.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Client Purchase Orders to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Client Purchase Order" : Convert.ToString(intRowHandles.Length) + " Linked Client Purchase Orders") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Client Purchase Order" : "these Linked Client Purchase Orders") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "ClientPurchaseOrderLinkID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("client_po_link", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;

                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlLabour.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Labour to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Labour record" : Convert.ToString(intRowHandles.Length) + " Linked Labour records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Labour record" : "these Linked Labour records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractLabourCostID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("site_contract_labour", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlEquipment.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Equipment to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Equipment record" : Convert.ToString(intRowHandles.Length) + " Linked Equipment records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Equipment record" : "these Linked Equipment records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractEquipmentCostID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("site_contract_equipment", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlMaterial.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Materials to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Material record" : Convert.ToString(intRowHandles.Length) + " Linked Material records") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Material record" : "these Linked Material records") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractMaterialCostID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp06000_OM_Delete("site_contract_material", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        if (!iBool_AllowDelete) return;
                        view = (GridView)gridControlBillingRequirement.MainView;
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more Linked Billing Requirements to delete by clicking on them then try again.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        // Checks passed so delete selected record(s) //
                        strMessage = "You have " + (intCount == 1 ? "1 Linked Billing Requirement" : Convert.ToString(intRowHandles.Length) + " Linked Billing Requirements") + " selected for delete!\n\nProceed?\n\n<color=red>Warning: If you proceed " + (intCount == 1 ? "this Linked Billing Requirement" : "these Linked Billing Requirements") + " will no longer be available for selection!</color>";
                        if (XtraMessageBox.Show(strMessage, "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, DefaultBoolean.True) == DialogResult.Yes)
                        {
                            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager1.ShowWaitForm();
                            splashScreenManager1.SetWaitFormDescription("Deleting...");

                            foreach (int intRowHandle in intRowHandles)
                            {
                                strRecordIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "BillingRequirementID")) + ",";
                            }

                            using (var RemoveRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                            {
                                RemoveRecords.ChangeConnectionString(strConnectionString);
                                try
                                {
                                    RemoveRecords.sp01000_Core_Delete("billing_requirement", strRecordIDs);  // Remove the records from the DB in one go //
                                }
                                catch (Exception) { }
                            }
                            LoadLinkedRecords();

                            if (splashScreenManager1.IsSplashFormVisible)
                            {
                                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                                splashScreenManager1.CloseWaitForm();
                            }
                            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show(intCount + " record(s) deleted.", "Delete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void View_Record()
        {
            GridView view = null;
            MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            //switch (i_int_FocusedGrid)
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }
                        var fChildForm = new frm_OM_Site_Contract_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractYearID")) + ',';
                        }
                        var fChildForm = new frm_OM_Site_Contract_Year_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        view = (GridView)gridControlJobRate.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractJobRateID")) + ',';
                        }
                        var fChildForm = new frm_OM_Site_Contract_Job_Rate_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractProfileID")) + ',';
                        }
                        var fChildForm = new frm_OM_Site_Contract_Year_Billing_Profile_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PersonResponsibilityID")) + ',';
                        }
                        var fChildForm = new frm_OM_Client_Contract_Person_Responsibility_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.i_strPassedInRepsonsibilityTypes = "21,22,41,42,901,902";
                        fChildForm.i_intPassedInRecordTypeID = 1;  // Site //
                        fChildForm.i_intPassedInPersonTypeID = 0;  // Staff //
                        fChildForm.i_strPassedInPersonTypeDescription = "Staff";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientPurchaseOrderLinkID")) + ',';
                        }
                        var fChildForm = new frm_OM_Client_PO_Link_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm._LinkedToRecordTypeID = 1;
                        fChildForm._LinkedToRecordType = "Site Contract";
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                
                case Utils.enmFocusedGrid.Labour:
                    {
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractLabourCostID")) + ',';
                        }
                        var fChildForm = new frm_OM_Site_Contract_Labour_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractEquipmentCostID")) + ',';
                        }
                        var fChildForm = new frm_OM_Site_Contract_Equipment_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractMaterialCostID")) + ',';
                        }
                        var fChildForm = new frm_OM_Site_Contract_Material_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.BillingRequirement:
                    {
                        view = (GridView)gridControlBillingRequirement.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view before proceeding.", "View Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "BillingRequirementID")) + ',';
                        }
                        var fChildForm = new frm_Core_Billing_Requirement_Edit();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strRecordIDs = strRecordIDs;
                        fChildForm.strFormMode = "view";
                        fChildForm.strCaller = this.Name;
                        fChildForm.intRecordCount = intCount;
                        fChildForm.LinkedToRecordTypeID = 2;
                        fChildForm.LinkedToRecordType = "Site Contract";
                        fChildForm.FormPermissions = this.FormPermissions;
                        var splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                default:
                    break;
            }
        }


        public override void OnViewAuditTrail(object sender, EventArgs e)
        {
            View_Audit_Trail();
        }
        private void View_Audit_Trail()
        {
            GridView view = null;
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            int[] intRowHandles;
            int intCount = 0;
            switch (_enmFocusedGrid)
            {
                case Utils.enmFocusedGrid.Contract:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Site_Contract";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.JobRate:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlJobRate.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractJobRateID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Site_Contract_Job_Rate";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYear:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlYear.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractYearID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Site_Contract_Year";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ContractYearProfile:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlProfile.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractProfileID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Site_Contract_Year_Billing_Profile";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.PersonResponsibility:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlResponsibility.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "PersonResponsibilityID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Person_Responsibility";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Labour:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlLabour.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractLabourCostID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Site_Contract_Labour_Cost";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Equipment:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlEquipment.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractEquipmentCostID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Site_Contract_Equipment_Cost";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.Materials:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlMaterial.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractMaterialCostID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Site_Contract_Material_Cost";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
                case Utils.enmFocusedGrid.ClientPOLink:
                    {
                        if (!iBool_AllowEdit) return;
                        view = (GridView)gridControlClientPO.MainView;
                        view.PostEditor();
                        intRowHandles = view.GetSelectedRows();
                        intCount = intRowHandles.Length;
                        if (intCount <= 0)
                        {
                            XtraMessageBox.Show("Select one or more records to view the audit trail for before proceeding.", "View Audit Trail", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        foreach (int intRowHandle in intRowHandles)
                        {
                            strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientPurchaseOrderLinkID")) + ',';
                        }

                        var fChildForm = new frm_Core_Audit_Trail_Viewer();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strPassedInDrillDownIDs = strRecordIDs;
                        fChildForm.strPassedInDrillDownTypeID = "dbo.OM_Client_Purchase_Order_Link";
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();

                        method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    break;
            }
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridControlContract":
                    message = "No Site Contracts Available - Adjust any filters and click Refresh button";
                    break;
                case "gridControlJobRate":
                    message = "No Linked Job Rates Available - Select one or more Site Contracts to view Linked Job Rates";
                    break;
                case "gridViewYear":
                    message = "No Linked Years Available - Select one or more Site Contracts to view Linked Years";
                    break;
                case "gridViewProfile":
                    message = "No Linked Billing Profiles Available - Select one or more Years to view Linked Billing Profiles";
                    break;
                case "gridViewResponsibility":
                    message = "No Linked Person Responsibilities Available - Select one or more Site Contracts to view Linked Person Responsibilities";
                    break;
                case "gridViewClientPO":
                    message = "No Linked CLient POs Available - Select one or more Client Contracts to view Linked Client POs";
                    break;
                case "gridViewLabour":
                    message = "No Linked Labour Available - Select one or more Site Contracts to view Linked Labour";
                    break;
                case "gridViewEquipment":
                    message = "No Linked Equipment Available - Select one or more Site Contracts to view Linked Equipment";
                    break;
                case "gridViewMaterial":
                    message = "No Linked Materials Available - Select one or more Client Contracts to view Linked Materials";
                    break;
                case "gridControlBillingRequirement":
                    message = "No Linked Billing Requirements Available - Select one or more Site Contracts to view Linked Billing Requirements";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_MouseDown_Standard(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_Standard(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        private void GridView_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.SelectionChanged(sender, e, ref isRunning);

            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridViewContract":
                    LoadLinkedRecords();

                    view = (GridView)gridControlJobRate.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlYear.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlResponsibility.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlLabour.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlEquipment.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlMaterial.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlClientPO.MainView;
                    view.ExpandAllGroups();

                    view = (GridView)gridControlBillingRequirement.MainView;
                    view.ExpandAllGroups();

                    SetMenuStatus();

                    int intSelectedCount = view.GetSelectedRows().Length;
                    if (intSelectedCount == 0)
                    {
                        bsiSelectedCount.Visibility = BarItemVisibility.Never;
                    }
                    else
                    {
                        bsiSelectedCount.Visibility = BarItemVisibility.Always;
                        bsiSelectedCount.Caption = (intSelectedCount == 1 ? "1 Site Contract Selected" : "<color=red>" + intSelectedCount.ToString() + " Site Contracts</Color> Selected");
                    }
                    break;
                case "gridViewYear":
                    LoadLinkedRecords2();
                    view = (GridView)gridControlProfile.MainView;
                    view.ExpandAllGroups();
                    break;
                default:
                    break;
            }
            SetMenuStatus();
        }

        #endregion


        #region GridView - Contract

        private void gridControlContract_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        // Drilldown to Linked Document Manager //
                        GridView view = gridViewContract;
                        int intRecordType = 51;  // Site Contract //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteContractID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() + 
                                                        ", Duration: " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date") + 
                                                        " - " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date") +
                                                        ", Site: " + (string.IsNullOrWhiteSpace(view.GetRowCellValue(view.FocusedRowHandle, "SiteName").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "SiteName").ToString());
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
                    }
                    else if ("copy_forward".Equals(e.Button.Tag))
                    {
                        Copy_Site_Contract();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewContract_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            switch (e.Column.FieldName)
            {
                case "LinkedLabourCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedLabourCount")) <= 0)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "LinkedEquipmentCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedEquipmentCount")) <= 0)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "LinkedMaterialCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedMaterialCount")) <= 0)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                case "LinkedPersonResponsibilityCount":
                    {
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedPersonResponsibilityCount")) <= 0)
                        {
                            e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                            e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                            e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewContract_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedVisitCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedVisitCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewContract_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }
       
        private void gridViewContract_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            SetMenuStatus();
        }

        private void gridViewContract_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Contract;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewContract_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedVisitCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedVisitCount")) <= 0) e.Cancel = true;
                    break;
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }

        private void repositoryItemHyperLinkEdit1_OpenLink(object sender, OpenLinkEventArgs e)
        {
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intSiteContractID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteContractID"));
            if (intSiteContractID <= 0) return;
            string strRecordIDs = "";

            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedVisitCount":
                    {
                        try
                        {
                            string strParameter = intSiteContractID.ToString() + ",";
                            var GetValue = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter();
                            GetValue.ChangeConnectionString(strConnectionString);
                            strRecordIDs = GetValue.sp06256_OM_Get_Visits_From_SiteContractIDs(intSiteContractID.ToString() + ",").ToString();
                        }
                        catch (Exception ex)
                        {
                            DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred while attempting to view visits linked to this site contract [" + ex.Message + "].\n\nPlease try again. If the problem persists contact Technical Support.", "View Linked Visits", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                        Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "operations_visit");
                    }
                    break;
                default:
                    break;
            }         
        }

        private void repositoryItemHyperLinkEditLinkedDocumentsClientContract_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 51;  // Site Contract //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteContractID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "ClientName").ToString() + 
                                            ", Duration: " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "StartDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date") + 
                                            " - " + (Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "EndDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date") +
                                            ", Site: " + (string.IsNullOrWhiteSpace(view.GetRowCellValue(view.FocusedRowHandle, "SiteName").ToString()) ? "" : view.GetRowCellValue(view.FocusedRowHandle, "SiteName").ToString());
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Copy_Site_Contract()
        {
            if (!iBool_AllowEdit) return;
            GridView view = (GridView)gridControlContract.MainView;
            string strRecordIDs = "";
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to Copy Forwards before proceeding.", "Copy Forward Site Contract(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ',';
            }

            var fChildForm = new frm_OM_Site_Contract_Copy_Forward();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm._SelectedCount = intCount;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User clicked OK on child form //
            {
                var fProgress = new frmProgress(0);
                fProgress.UpdateCaption("Copying...");
                fProgress.Show();
                Application.DoEvents();
                int intUpdateProgressThreshhold = intCount / 10;
                int intUpdateProgressTempCount = 0;

                int _NewContractStart = (fChildForm._NewContractStart ? 1 : 0);
                DateTime? _NewContractStartDate = fChildForm._NewContractStartDate;
                DateTime? _NewContractEndDate = fChildForm._NewContractEndDate;
                int? _ContractAddStartUnits = fChildForm._ContractAddStartUnits;
                int _ContractAddStartUnitDescriptor = 0;
                switch (fChildForm._ContractAddStartUnitDescriptor)
	            {
                    case "Days":
                        _ContractAddStartUnitDescriptor = 1;
                        break;
                    case "Weeks":
                        _ContractAddStartUnitDescriptor = 2;
                        break;
                    case "Months":
                        _ContractAddStartUnitDescriptor = 3;
                        break;
                    case "Years":
                        _ContractAddStartUnitDescriptor = 4;
                        break;
		            default:
                         _ContractAddStartUnitDescriptor = 4;
                        break;
	              }
                
                int _ApplyNewContractValue = (fChildForm._ApplyNewContractValue ? 1 : 0);
                decimal? _NewContractValue = fChildForm._NewContractValue;
                
                int _ApplyAddUpliftPercentage = (fChildForm._ApplyAddUpliftPercentage ? 1 : 0);             
                decimal? _NewContractUpliftPercentage = fChildForm._NewContractUpliftPercentage;
                
                int _ApplyUpliftValue = (fChildForm._ApplyUpliftValue ? 1 : 0);
                decimal? _NewContractUpliftValue = fChildForm._NewContractUpliftValue;

                int _KeepYearlyPercentageIncrease = (fChildForm._KeepYearlyPercentageIncrease ? 1 : 0);
                bool _NewYearlyPerecentageIncrease = fChildForm._NewYearlyPerecentageIncrease;
                decimal? _NewContractYearlyPerecentageIncrease = fChildForm._NewContractYearlyPerecentageIncrease;

                int _NewContractActive = (fChildForm._NewContractActive ? 1 : 0);
                int _ExistingContractInactive = (fChildForm._ExistingContractInactive ? 1 : 0);

                int _SiteInstructionsAppend = (fChildForm._SiteInstructionsAppend ? 1 : 0);
                int _SiteInstructionsNew = (fChildForm._SiteInstructionsNew ? 1 : 0);
                string _SiteInstructions = fChildForm._SiteInstructions;

                int _YearAndBillingProfile = (fChildForm._YearAndBillingProfile ? 1 : 0);
                int _PersonResponsibilities = (fChildForm._PersonResponsibilities ? 1 : 0);
                int _PreferredLabour = (fChildForm._PreferredLabour ? 1 : 0);
                int _DefaultEquipmentCosts = (fChildForm._DefaultEquipmentCosts ? 1 : 0);
                int _DefaultMaterialCosts = (fChildForm._DefaultMaterialCosts ? 1 : 0);
                int _SiteContractBillingRequirements = (fChildForm._SiteContractBillingRequirements ? 1 : 0);
                
                int _VisitStructure = (fChildForm._VisitStructure ? 1 : 0);
                int _HoldDayOfWeek = (fChildForm._HoldDayOfWeek ? 1 : 0);
                int _VisitPersonResponsibilities = (fChildForm._VisitPersonResponsibilities ? 1 : 0);
                int _VisitBillingRequirements = (fChildForm._VisitBillingRequirements ? 1 : 0);                
                
                int _JobStructure = (fChildForm._JobStructure ? 1 : 0);
                int _JobLabour = (fChildForm._JobLabour ? 1 : 0);
                int _JobEquipment = (fChildForm._JobEquipment ? 1 : 0);
                int _JobMaterials = (fChildForm._JobMaterials ? 1 : 0);
                int _JobHealthAndSafety = (fChildForm._JobHealthAndSafety ? 1 : 0);

                // Create Site Contracts //
                int intSiteContractID = 0;
                i_str_AddedRecordIDsContract = "";
                using (var CopyRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    CopyRecords.ChangeConnectionString(strConnectionString);
                    foreach (int intRowHandle in intRowHandles)
                    {
                        intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteContractID"));

                        try
                        {
                            i_str_AddedRecordIDsContract += CopyRecords.sp06299_OM_Site_Contract_Copy_Forwards(intSiteContractID,
                                                                                                            _NewContractStart,
                                                                                                            _NewContractStartDate,
                                                                                                            _NewContractEndDate,
                                                                                                            _ContractAddStartUnits,
                                                                                                            _ContractAddStartUnitDescriptor,
                                                                                                            _ApplyNewContractValue,
                                                                                                            _NewContractValue,
                                                                                                            _ApplyAddUpliftPercentage,
                                                                                                            _NewContractUpliftPercentage,
                                                                                                            _KeepYearlyPercentageIncrease,
                                                                                                            _NewContractYearlyPerecentageIncrease,
                                                                                                            _NewContractActive,
                                                                                                            _ExistingContractInactive,
                                                                                                            _SiteInstructionsAppend,
                                                                                                            _SiteInstructionsNew,
                                                                                                            _SiteInstructions,
                                                                                                            _YearAndBillingProfile,
                                                                                                            _PersonResponsibilities,
                                                                                                            _PreferredLabour,
                                                                                                            _DefaultEquipmentCosts,
                                                                                                            _DefaultMaterialCosts,
                                                                                                            _SiteContractBillingRequirements,
                                                                                                            _VisitStructure,
                                                                                                            _HoldDayOfWeek,
                                                                                                            _VisitPersonResponsibilities,
                                                                                                            _VisitBillingRequirements,
                                                                                                            _JobStructure,
                                                                                                            _JobLabour,
                                                                                                            _JobEquipment,
                                                                                                            _JobMaterials,
                                                                                                            _JobHealthAndSafety).ToString() + ";";
                        }
                        catch (Exception ex) { }
                        
                        intUpdateProgressTempCount++;
                        if (intUpdateProgressTempCount >= intUpdateProgressThreshhold)
                        {
                            intUpdateProgressTempCount = 0;
                            fProgress.UpdateProgress(10);
                        }

                    }
                }
                if (fProgress != null)
                {
                    fProgress.Close();
                    fProgress = null;
                }
                Load_Data();
            }
        }

        #endregion


        #region GridView - Job Rate

        private void gridControlJobRate_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("apply_uplift".Equals(e.Button.Tag))
                    {
                        Apply_Job_Rate_Uplift();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewJobRate_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewJobRate_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.JobRate;
            SetMenuStatus();
        }

        private void gridViewJobRate_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.JobRate;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Apply_Job_Rate_Uplift()
        {
            if (!iBool_AllowEdit) return;
            GridView view = (GridView)gridControlJobRate.MainView;
            string strRecordIDs = "";
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Job Rates to Apply a Rate Uplift to before proceeding.", "Apply Job Rate Uplift", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractJobRateID")) + ',';
            }

            var fChildForm = new frm_OM_Site_Contract_Job_Rate_Apply_Uplift();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = strRecordIDs;
            fChildForm.intRecordCount = intCount;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User clicked OK on child form //
            {
                i_str_AddedRecordIDsJobRate = fChildForm.strReturnedNewIDs;
                LoadLinkedRecords();
            }
        }

        #endregion


        #region GridView - Year

        private void gridControlYear_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("copy from client contract".Equals(e.Button.Tag))
                    {
                        Copy_Client_Contract_Years_To_Site_Contract();
                    }
                    break;
                default:
                    break;
            }
        }
       
        private void gridViewYear_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle < 0) return;
            try
            {
                GridView view = (GridView)sender;
                switch (e.Column.FieldName)
                {
                    case "LinkedDocumentCount":
                        if (Convert.ToInt32(view.GetRowCellValue(e.RowHandle, "LinkedDocumentCount")) <= 0) e.RepositoryItem = emptyEditor;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception) { }
        }

        private void gridViewYear_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewYear_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.ContractYear;
            SetMenuStatus();
        }

        private void gridViewYear_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.ContractYear;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void gridViewYear_ShowingEditor(object sender, CancelEventArgs e)
        {
            // Prevent hyperlink firing if row had no associated linked records [value = 0]//
            GridView view = (GridView)sender;
            switch (view.FocusedColumn.FieldName)
            {
                case "LinkedDocumentCount":
                    if (Convert.ToInt32(view.GetFocusedRowCellValue("LinkedDocumentCount")) <= 0) e.Cancel = true;
                    break;
                default:
                    break;
            }
        }
        
        private void repositoryItemHyperLinkEditLinkedDocumentsClientContractYear_OpenLink(object sender, OpenLinkEventArgs e)
        {
            // Drilldown to Linked Document Manager //
            int intRecordType = 42;  // Client Contract Year //
            int intRecordSubType = 0;  // Not Used //
            GridView view = ((sender as BaseEdit).Parent as GridControl).FocusedView as GridView;
            int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "SiteContractYearID"));
            string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "LinkedToParent").ToString() + ", Contract Year: " + view.GetRowCellValue(view.FocusedRowHandle, "YearDescription").ToString();
            Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void Copy_Client_Contract_Years_To_Site_Contract()
        {
            GridView viewSites = (GridView)gridControlContract.MainView;
            int[] intRowHandles = viewSites.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Sites to copy the parent Client Contract's Contract Years to before proceeding.", "Copy Client Contract Years to Site Contracts", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to copy the parent Client Contract's Contract Years to the selected Site Contracts.\n\nIf you proceed one or more new Contract Years will be created for each of the selected Site Contracts.\n\nProceed?", "Copy Client Contract Years to Site Contracts", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;

            SqlDataAdapter sdaYears = new SqlDataAdapter();
            DataSet dsYears = new DataSet("NewDataSet");

            GridView view = (GridView)gridControlYear.MainView;
            view.PostEditor();
            int intSiteContractID = 0;
            decimal decContractValue = (decimal)0.00;
            decimal decValue = (decimal)0.00;
            string strSiteName = "";
            string strClientContractID = "";
            string strYearDescription = "";
            decimal? decYearlyPercentageIncrease = (decimal)0.00;
            DateTime? dtStartDate = null;
            DateTime? dtEndDate = null;
            int intActive = 0;
            int intClientContractID = 0;
            int intClientContractYearID = 0;
            string strRemarks = "";
            string strNewIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                intClientContractID = Convert.ToInt32(viewSites.GetRowCellValue(intRowHandle, "ClientContractID"));
                strClientContractID = intClientContractID.ToString() + ",";
                try
                {
                    dsYears.Clear();
                    SqlConnection SQlConn = new SqlConnection(strConnectionString);
                    SqlCommand cmd = null;
                    cmd = new SqlCommand("sp06094_OM_Client_Contract_Years_Linked_To_Client_Contract", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientContractIDs", strClientContractID));
                    sdaYears = new SqlDataAdapter(cmd);
                    sdaYears.Fill(dsYears, "Table");
                    SQlConn = null;
                    cmd = null;
                }
                catch (Exception Ex) { return; }

                intSiteContractID = Convert.ToInt32(viewSites.GetRowCellValue(intRowHandle, "SiteContractID"));
                decContractValue = Convert.ToDecimal(viewSites.GetRowCellValue(intRowHandle, "ContractValue"));  // Contract Value //
                strSiteName = viewSites.GetRowCellValue(intRowHandle, "SiteName").ToString();
                foreach (DataRow dr in dsYears.Tables[0].Rows)
                {
                    strYearDescription = dr["YearDescription"].ToString();
                    dtStartDate = (string.IsNullOrWhiteSpace(dr["StartDate"].ToString()) ? null : (DateTime?)dr["StartDate"]);
                    dtEndDate = (string.IsNullOrWhiteSpace(dr["EndDate"].ToString()) ? null : (DateTime?)dr["EndDate"]);
                    intActive = (string.IsNullOrWhiteSpace(dr["Active"].ToString()) ? 0 : Convert.ToInt32(dr["Active"]));
                    decValue = (Convert.ToDecimal(dr["ContractValuePercentage"]) > (decimal)0.00 ? decContractValue * Convert.ToDecimal(dr["ContractValuePercentage"]) : (decimal)0.00);
                    decYearlyPercentageIncrease = (string.IsNullOrWhiteSpace(dr["YearlyPercentageIncrease"].ToString()) ? null : (decimal?)dr["YearlyPercentageIncrease"]);
                    strRemarks = dr["Remarks"].ToString();
                    intClientContractYearID = Convert.ToInt32(dr["ClientContractYearID"]);

                    using (var AddRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                    {
                        AddRecords.ChangeConnectionString(strConnectionString);
                        try
                        {
                            strNewIDs += AddRecords.sp06118_OM_Site_Contract_Year_Add(intSiteContractID, strYearDescription, dtStartDate, dtEndDate, intActive, decValue, intClientContractYearID, strRemarks, decYearlyPercentageIncrease).ToString() + ";";
                        }
                        catch (Exception) { return; }
                    }
                }
            }
            i_str_AddedRecordIDsYear = strNewIDs;
            LoadLinkedRecords();
        }
        
        #endregion


        #region GridView - Billing Profile

        private void gridControlProfile_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("linked_document".Equals(e.Button.Tag))
                    {
                        /*// Drilldown to Linked Document Manager //
                        GridView view = (GridView)gridControlBonuses.MainView;
                        int intRecordType = 11;  // Bonuses //
                        int intRecordSubType = 0;  // Not Used //
                        int intRecordID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "BonusID"));
                        string strRecordDescription = view.GetRowCellValue(view.FocusedRowHandle, "EmployeeName").ToString() + " - " + Convert.ToDateTime(view.GetRowCellValue(view.FocusedRowHandle, "BonusDueDate")).ToString("dd/MM/yyyy HH:mm") ?? "Unknown Date";
                        Linked_Document_Drill_Down(intRecordType, intRecordSubType, intRecordID, strRecordDescription); */
                    }
                     else if ("add_from_template".Equals(e.Button.Tag))
                    {
                        Add_From_Billing_Profile();
                    }
                    else if ("template_manager".Equals(e.Button.Tag))
                    {
                        var fChildForm = new frm_OM_Template_Billing_Profile_Manager();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    else if ("copy_from_client".Equals(e.Button.Tag))
                    {
                        Copy_From_Client_Billing_Profile();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewProfile_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewProfile_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.ContractYearProfile;
            SetMenuStatus();
        }

        private void gridViewProfile_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.ContractYearProfile;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Add_From_Billing_Profile()
        {
            GridView parentView = (GridView)gridControlYear.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contract Years to add the Billing Profile Template to by clicking on them then try again.", "Add Billing Profile From Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Site Contract Years selected. If you proceed a new Billing Profile will be created for each of these records from the chosen Template.\n\nProceed?", "Add Billing Profile From Template", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControlProfile.MainView;
            view.PostEditor();

            var fChildForm = new frm_OM_Select_Template_Billing_Profile_Header();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                int intTemplateID = fChildForm.intSelectedTemplateHeaderID;
                if (intTemplateID <= 0) return;

                GridControl gridControlTemp = fChildForm.gridControl2;
                GridView viewTemp = (GridView)gridControlTemp.MainView;
                int intRowCount = viewTemp.DataRowCount;
                if (intCount <= 0) return;

                i_str_AddedRecordIDsProfile = "";
                using (var AddRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    AddRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        decimal decValuePercentage = (decimal)0.00;
                        decimal decEstimatedBillAmount = (decimal)0.00;
                        DateTime dtInvoiceDateDue = DateTime.Now; ;
                        int intUnits = 0;
                        int intUnitDescriptor = 0;
                        int intSiteContractYearID = 0;

                        foreach (int i in intRowHandles)  // Selected Years //
                        {
                            for (int j = 0; j < intRowCount; j++)  // Template Items //
                            {
                                intSiteContractYearID = Convert.ToInt32(parentView.GetRowCellValue(i, "SiteContractYearID"));

                                if (!string.IsNullOrWhiteSpace(parentView.GetRowCellValue(i, "StartDate").ToString()))
                                {
                                    intUnits = Convert.ToInt32(viewTemp.GetRowCellValue(j, "UnitsFromStart"));
                                    intUnitDescriptor = Convert.ToInt32(viewTemp.GetRowCellValue(j, "UnitsFromStartDescriptorID"));
                                    if (intUnits > 0 && intUnitDescriptor > 0)
                                    {
                                        dtInvoiceDateDue = Convert.ToDateTime(parentView.GetRowCellValue(i, "StartDate"));
                                        switch (intUnitDescriptor)
                                        {
                                            case 1:  // Days //
                                                {
                                                    dtInvoiceDateDue = dtInvoiceDateDue.AddDays(intUnits);
                                                }
                                                break;
                                            case 2:  // Weeks //
                                                {
                                                    dtInvoiceDateDue = dtInvoiceDateDue.AddDays(intUnits * 7);
                                                }
                                                break;
                                            case 3:  // Month //
                                                {
                                                    dtInvoiceDateDue = dtInvoiceDateDue.AddMonths(intUnits);
                                                }
                                                break;
                                            case 4:  // Years //
                                                {
                                                    dtInvoiceDateDue = dtInvoiceDateDue.AddYears(intUnits);
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                                decValuePercentage = Convert.ToDecimal(viewTemp.GetRowCellValue(j, "ValuePercentage"));
                                decEstimatedBillAmount = (decValuePercentage > (decimal)0.00 ? Convert.ToDecimal(parentView.GetRowCellValue(i, "ClientValue")) * (decValuePercentage / 100) : (decimal)0.00);

                                i_str_AddedRecordIDsProfile += AddRecords.sp06120_OM_Site_Contract_Year_Billing_Profile_Update(intSiteContractYearID, dtInvoiceDateDue, null, decEstimatedBillAmount, (decimal)0.00, 0, null).ToString() + ";";
                            }
                        }
                    }
                    catch (Exception) { }
                }
                LoadLinkedRecords2();
            }
        }

        private void Copy_From_Client_Billing_Profile()
        {
            GridView viewSiteYears = (GridView)gridControlYear.MainView;
            int[] intRowHandles = viewSiteYears.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Sites Years to copy the parent Client Contract Year's Billing Profile to before proceeding.", "Copy Client Contract Year Billing Profile to Site Contract Years", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // De-select any years which have not been created from a Client Contract Year // 
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(viewSiteYears.GetRowCellValue(intRowHandle, "CreatedFromClientContractYearID")) == 0) viewSiteYears.UnselectRow(intRowHandle);
            }

            intRowHandles = viewSiteYears.GetSelectedRows();
            if (intRowHandles.Length <= 0)
            {
                XtraMessageBox.Show("Select one or more Sites Years to copy the parent Client Contract Year's Billing Profile to before proceeding.\n\nNote: This process automatically de-selects any rows which were not created from a parent Client Contract Year.", "Copy Client Contract Year Billing Profile to Site Contract Years", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            if (DevExpress.XtraEditors.XtraMessageBox.Show("You are about to copy the parent Client Contract Year's Billing Profile to the selected Site Contract Years.\n\nIf you proceed one or more new Contract Year Billing Profiles will be created for each of the selected Site Contract Years.\n\nProceed?", "Copy Client Contract Year Billing Profile to Site Contract Years", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;

            SqlDataAdapter sdaYearProfiles = new SqlDataAdapter();
            DataSet dsYearProfiles = new DataSet("NewDataSet");
            SqlConnection SQlConn = new SqlConnection(strConnectionString);
            SqlCommand cmd = null;
            string strClientContractYearIDs = "";

            GridView view = (GridView)gridControlProfile.MainView;
            view.PostEditor();
            int intCreatedFromClientContractYearID = 0;

            int intSiteContractYearID = 0;
            DateTime? dtInvoiceDateDue = null;
            DateTime? dtInvoiceDateActual = null;
            decimal decEstimatedBillAmount = (decimal)0.00;
            decimal decActualBillAmount = (decimal)0.00;
            int intBilledByPersonID = 0;
            int intClientContractProfileID = 0;
            string strRemarks = "";
            string strNewIDs = "";

            decimal decValue = (decimal)0.00;

            foreach (int intRowHandle in intRowHandles)
            {
                intCreatedFromClientContractYearID = Convert.ToInt32(viewSiteYears.GetRowCellValue(intRowHandle, "CreatedFromClientContractYearID"));
                strClientContractYearIDs = intCreatedFromClientContractYearID.ToString() + ",";
                try
                {
                    cmd = null;
                    dsYearProfiles.Clear();  // Clear any prior result set //
                    cmd = new SqlCommand("sp06097_OM_Client_Contract_Billing_Profiles_Linked_To_Client_Contract_Year", SQlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ClientContractYearIDs", strClientContractYearIDs));
                    sdaYearProfiles = new SqlDataAdapter(cmd);

                    sdaYearProfiles.Fill(dsYearProfiles, "Table");
                }
                catch (Exception Ex)
                {
                    view.EndUpdate();  // UnLock view //
                    return;
                }

                decValue = Convert.ToDecimal(viewSiteYears.GetRowCellValue(intRowHandle, "ClientValue"));
                foreach (DataRow dr in dsYearProfiles.Tables[0].Rows)
                {
                    intSiteContractYearID = Convert.ToInt32(viewSiteYears.GetRowCellValue(intRowHandle, "SiteContractYearID"));
                    dtInvoiceDateDue = (string.IsNullOrWhiteSpace(dr["InvoiceDateDue"].ToString()) ? null : (DateTime?)dr["InvoiceDateDue"]);
                    dtInvoiceDateActual = (string.IsNullOrWhiteSpace(dr["InvoiceDateActual"].ToString()) ? null : (DateTime?)dr["InvoiceDateActual"]);
                    decEstimatedBillAmount = (Convert.ToDecimal(dr["YearValuePercentage"]) > (decimal)0.00 ? decValue * Convert.ToDecimal(dr["YearValuePercentage"]) : (decimal)0.00);
                    decActualBillAmount = (decimal)0.00;
                    intBilledByPersonID = 0;
                    intClientContractProfileID = Convert.ToInt32(dr["ClientContractProfileID"]);
                    strRemarks = dr["Remarks"].ToString();
                 
                    using (var AddRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                    {
                        AddRecords.ChangeConnectionString(strConnectionString);
                        try
                        {
                            strNewIDs += AddRecords.sp06121_OM_Site_Contract_Year_Billing_Profile_Add(intSiteContractYearID, dtInvoiceDateDue, dtInvoiceDateActual, decEstimatedBillAmount, decActualBillAmount, intBilledByPersonID, intClientContractProfileID, strRemarks).ToString() + ";";
                        }
                        catch (Exception) { return; }
                    }

                }
            }
            SQlConn = null;
            cmd = null;
            i_str_AddedRecordIDsProfile = strNewIDs;
            LoadLinkedRecords2();

        }
        #endregion


        #region GridView - Person Responsibility

        private void gridControlResponsibility_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewResponsibility_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewResponsibility_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.PersonResponsibility;
            SetMenuStatus();
        }

        private void gridViewResponsibility_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.PersonResponsibility;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Client POs

        private void gridControlClientPO_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewClientPO_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            if (e.RowHandle < 0) return;
            if (e.Column.FieldName == "StartingValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "StartingValue"));
                if (decWarningValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "WarningValue")
            {
                decimal decWarningValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "WarningValue"));
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decWarningValue > decRemainingValue && decWarningValue > (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
            else if (e.Column.FieldName == "RemainingValue")
            {
                decimal decRemainingValue = Convert.ToDecimal(view.GetRowCellValue(e.RowHandle, "RemainingValue"));
                if (decRemainingValue <= (decimal)0.00)
                {
                    e.Appearance.BackColor = Color.FromArgb(0xBD, 0xFD, 0x80, 0xA0);
                    e.Appearance.BackColor2 = Color.FromArgb(0xC0, 0xFD, 0x02, 0x02);
                    e.Appearance.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
                }
            }
        }

        private void gridViewClientPO_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewClientPO_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.ClientPOLink;
            SetMenuStatus();
        }

        private void gridViewClientPO_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.ClientPOLink;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        #endregion


        #region GridView - Labour

        private void gridControlLabour_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("block_add".Equals(e.Button.Tag))
                    {
                        Block_Add_Labour();
                    }
                    else if ("reassign_labour".Equals(e.Button.Tag))
                    {
                        Reassign_Labour();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewLabour_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewLabour_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            SetMenuStatus();
        }

        private void gridViewLabour_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Labour;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Block_Add_Labour()
        {
            if (strFormMode == "view") return;
            GridView parentView = (GridView)gridControlContract.MainView;
            int[] intSiteListRowHandles = parentView.GetSelectedRows();
            int intCount = intSiteListRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contracts to block add the Preferred Labour to by clicking on them then try again.", "Block Add Preferred Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Select_Contractor();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intOriginalContractorID = 0;
            fChildForm._Mode = "multiple";
            fChildForm._PassedInLatitude = (double)0.00;
            fChildForm._PassedInLongitude = (double)0.00;
            fChildForm._PassedInPostcode = "";
            GridView ContractorScreenView = null;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                ContractorScreenView = (GridView)fChildForm.gridControl1.MainView;
                int intContractorRowCount = ContractorScreenView.DataRowCount;

                int intSiteContractID = 0;
                int intContractorID = 0;
                int intCostUnitDescriptorID = 1; // Hours //
                int intSellUnitDescriptorID = 1; // Hours //
                decimal decCostPerUnitExVat = (decimal)0.00;
                decimal decCostPerUnitVatRate = _decDefaultVatRate;
                decimal decSellPerUnitExVat = (decimal)0.00;
                decimal decSellPerUnitVatRate = _decDefaultVatRate;
                decimal decCISPercentage = (decimal)0.00;
                decimal decCISValue = (decimal)0.00;
                double dblPostcodeSiteDistance = (double)0.00;
                double dblLatLongSiteDistance = (double)0.00;
                string strRemarks = "";
                string strNewIDs = "";
                
                using (var AddRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    AddRecords.ChangeConnectionString(strConnectionString);

                    for (int i = 0; i < intContractorRowCount; i++)
                    {
                        if (Convert.ToBoolean(ContractorScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            foreach (int intSiteListRowHandle in intSiteListRowHandles)
                            {

                                try
                                {
                                    intSiteContractID = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                    intContractorID = Convert.ToInt32(ContractorScreenView.GetRowCellValue(i, "ContractorID"));

                                    strNewIDs += AddRecords.sp06122_OM_Site_Contract_Labour_Cost_Add(intSiteContractID, intContractorID, intCostUnitDescriptorID, intSellUnitDescriptorID, decCostPerUnitExVat, decCostPerUnitVatRate, decSellPerUnitExVat, decSellPerUnitVatRate, decCISPercentage, decCISValue, dblPostcodeSiteDistance, dblLatLongSiteDistance, strRemarks).ToString() + ";";
                                }
                                catch (Exception Ex) { }
                            }
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(strNewIDs))
                {
                    i_str_AddedRecordIDsLabour = strNewIDs;
                    LoadLinkedRecords();
                }
            }
        }

        private void Reassign_Labour()
        {
            if (strFormMode == "view") return;
            GridView view = (GridView)gridControlLabour.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Preferred Labour records to reassign by clicking on them then try again.", "Reassign Preferred Labour", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            System.Reflection.MethodInfo method = null;
            string strRecordIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strRecordIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractLabourCostID")) + ',';
            }

            var fChildForm = new frm_OM_Site_Contract_Labour_Reassign();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.i_str_PassedInIDs = strRecordIDs;
            fChildForm.strFormMode = "edit";
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }
        
        #endregion


        #region GridView - Equipment

        private void gridControlEquipment_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                     else if ("block_add".Equals(e.Button.Tag))
                    {
                        Block_Add_Equipment();
                    }
                   break;
                default:
                    break;
            }
        }

        private void gridViewEquipment_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewEquipment_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Equipment;
            SetMenuStatus();
        }

        private void gridViewEquipment_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Equipment;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Block_Add_Equipment()
        {
            if (strFormMode == "view") return;
            GridView parentView = (GridView)gridControlContract.MainView;
            int[] intSiteListRowHandles = parentView.GetSelectedRows();
            int intCount = intSiteListRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contracts to block add the Default Equipment Costs to by clicking on them then try again.", "Block Add Default Equipment Costs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Select_Equipment();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intOriginalID = 0;
            fChildForm._Mode = "multiple";
            GridView EquipmentScreenView = null;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                EquipmentScreenView = (GridView)fChildForm.gridControl1.MainView;
                int intContractorRowCount = EquipmentScreenView.DataRowCount;

                int intSiteContractID = 0;
                int intEquipmentID = 0;
                int intCostUnitDescriptorID = 1; // Hours //
                int intSellUnitDescriptorID = 1; // Hours //
                decimal decCostPerUnitExVat = (decimal)0.00;
                decimal decCostPerUnitVatRate = _decDefaultVatRate;
                decimal decSellPerUnitExVat = (decimal)0.00;
                decimal decSellPerUnitVatRate = _decDefaultVatRate;
                string strRemarks = "";
                string strNewIDs = "";

                using (var AddRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    AddRecords.ChangeConnectionString(strConnectionString);

                    for (int i = 0; i < intContractorRowCount; i++)
                    {
                        if (Convert.ToBoolean(EquipmentScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            foreach (int intSiteListRowHandle in intSiteListRowHandles)
                            {

                                try
                                {
                                    intSiteContractID = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                    intEquipmentID = Convert.ToInt32(EquipmentScreenView.GetRowCellValue(i, "EquipmentID"));

                                    strNewIDs += AddRecords.sp06123_OM_Site_Contract_Equipment_Cost_Add(intSiteContractID, intEquipmentID, intCostUnitDescriptorID, intSellUnitDescriptorID, decCostPerUnitExVat, decCostPerUnitVatRate, decSellPerUnitExVat, decSellPerUnitVatRate, strRemarks).ToString() + ";";
                                }
                                catch (Exception Ex) { }
                            }
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(strNewIDs))
                {
                    i_str_AddedRecordIDsEquipment = strNewIDs;
                    LoadLinkedRecords();
                }
            }
        }
        
        #endregion


        #region GridView - Material

        private void gridControlMaterial_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("block_add".Equals(e.Button.Tag))
                    {
                        Block_Add_Materials();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridViewMaterial_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridViewMaterial_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.Materials;
            SetMenuStatus();
        }

        private void gridViewMaterial_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.Materials;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Block_Add_Materials()
        {
            if (strFormMode == "view") return;
            GridView parentView = (GridView)gridControlContract.MainView;
            int[] intSiteListRowHandles = parentView.GetSelectedRows();
            int intCount = intSiteListRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contracts to block add the Default Material Costs to by clicking on them then try again.", "Block Add Default Material Costs", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            var fChildForm = new frm_OM_Select_Material();
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.intOriginalID = 0;
            fChildForm._Mode = "multiple";
            GridView MaterialScreenView = null;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                MaterialScreenView = (GridView)fChildForm.gridControl1.MainView;
                int intContractorRowCount = MaterialScreenView.DataRowCount;

                int intSiteContractID = 0;
                int intEquipmentID = 0;
                int intCostUnitDescriptorID = 1; // Units //
                int intSellUnitDescriptorID = 1; // Units //
                decimal decCostPerUnitExVat = (decimal)0.00;
                decimal decCostPerUnitVatRate = _decDefaultVatRate;
                decimal decSellPerUnitExVat = (decimal)0.00;
                decimal decSellPerUnitVatRate = _decDefaultVatRate;
                string strRemarks = "";
                string strNewIDs = "";

                using (var AddRecords = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
                {
                    AddRecords.ChangeConnectionString(strConnectionString);

                    for (int i = 0; i < intContractorRowCount; i++)
                    {
                        if (Convert.ToBoolean(MaterialScreenView.GetRowCellValue(i, "CheckMarkSelection")))
                        {
                            foreach (int intSiteListRowHandle in intSiteListRowHandles)
                            {

                                try
                                {
                                    intSiteContractID = Convert.ToInt32(parentView.GetRowCellValue(intSiteListRowHandle, "SiteContractID"));
                                    intEquipmentID = Convert.ToInt32(MaterialScreenView.GetRowCellValue(i, "MaterialID"));

                                    strNewIDs += AddRecords.sp06124_OM_Site_Contract_Material_Cost_Add(intSiteContractID, intEquipmentID, intCostUnitDescriptorID, intSellUnitDescriptorID, decCostPerUnitExVat, decCostPerUnitVatRate, decSellPerUnitExVat, decSellPerUnitVatRate, strRemarks).ToString() + ";";
                                }
                                catch (Exception Ex) { }
                            }
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(strNewIDs))
                {
                    i_str_AddedRecordIDsMaterial = strNewIDs;
                    LoadLinkedRecords();
                }
            }
        }
        
        #endregion


        #region GridView - Billing Requirement

        private void gridControlBillingRequirement_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Custom:
                    if ("add".Equals(e.Button.Tag))
                    {
                        Add_Record();
                    }
                    else if ("edit".Equals(e.Button.Tag))
                    {
                        Edit_Record();
                    }
                    else if ("delete".Equals(e.Button.Tag))
                    {
                        Delete_Record();
                    }
                    else if ("view".Equals(e.Button.Tag))
                    {
                        View_Record();
                    }
                    else if ("add_from_template".Equals(e.Button.Tag))
                    {
                        Add_From_Billing_Requirement_Template();
                    }
                    else if ("template_manager".Equals(e.Button.Tag))
                    {
                        if (!iBool_BillingRequirementTemplateManager) return;
                        var fChildForm = new frm_Core_Billing_Requirement_Template_Manager();
                        fChildForm.MdiParent = this.MdiParent;
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                        fChildForm.splashScreenManager = splashScreenManager1;
                        fChildForm.splashScreenManager.ShowWaitForm();
                        fChildForm.Show();
                        System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
                        if (method != null) method.Invoke(fChildForm, new object[] { null });
                    }
                    else if ("add_from_parent".Equals(e.Button.Tag))
                    {
                        GridView view = (GridView)gridControlContract.MainView;
                        view.PostEditor();
                        int[] intRowHandles;
                        if (!iBool_AllowEdit) return;
                        intRowHandles = view.GetSelectedRows();
                        if (intRowHandles.Length <= 0)
                        {
                            XtraMessageBox.Show("Select at least one Site Contract to re-create the Billing Requirements for before proceeding.", "Re-Create Billing Requirements", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            return;
                        }
                        if (XtraMessageBox.Show("WARNING... You are about to re-create the Billing Requirements for " + intRowHandles.Length.ToString() + " Site Contract(s).\n\nIf you proceed, any existing Billing Requirements for the selected Site Contract(s) will be deleted first.", "Re-Create Billing Requirements", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes) return;

                        if (!splashScreenManager.IsSplashFormVisible)
                        {
                            this.splashScreenManager = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
                            splashScreenManager.ShowWaitForm();
                            splashScreenManager.SetWaitFormDescription("Re-Creating Billing Requirements...");
                        }

                        try
                        {
                            using (var ReCreateRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                            {
                                ReCreateRecords.ChangeConnectionString(strConnectionString);

                                foreach (int intRowHandle in intRowHandles)
                                {
                                    int intClientContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "ClientContractID"));
                                    int intSiteContractID = Convert.ToInt32(view.GetRowCellValue(intRowHandle, "SiteContractID"));
                                    i_str_AddedRecordIDsBillingRequirement += ReCreateRecords.sp01043_Core_Billing_Requirement_Re_Add_From_Parent(intClientContractID, intBillingRequirementLinkedRecordTypeID-1, intSiteContractID, intBillingRequirementLinkedRecordTypeID).ToString() + ";";
                                }
                            }
                        }
                        catch (Exception) { }
                        LoadLinkedRecords();
                        if (splashScreenManager.IsSplashFormVisible) splashScreenManager.CloseWaitForm();
                    }
                    break;
                default:
                    break;
            }
        }

        private void gridControlBillingRequirement_DoubleClick(object sender, EventArgs e)
        {
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            GridView view = (GridView)sender;
            GridHitInfo hitInfo = view.CalcHitInfo(args.Location);
            if (!hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                if (iBool_AllowEdit)
                {
                    iBoolDontFireGridGotFocusOnDoubleClick = true;
                    Edit_Record();
                }
            }
        }

        private void gridControlBillingRequirement_GotFocus(object sender, EventArgs e)
        {
            if (iBoolDontFireGridGotFocusOnDoubleClick)
            {
                // Stops this event adjusting the menu when double clicking from the grid to open an edit screen as focus seems to come back after edit code is fired. //
                iBoolDontFireGridGotFocusOnDoubleClick = false;
                return;
            }
            _enmFocusedGrid = Utils.enmFocusedGrid.BillingRequirement;
            SetMenuStatus();
        }

        private void gridControlBillingRequirement_MouseUp(object sender, MouseEventArgs e)
        {
            _enmFocusedGrid = Utils.enmFocusedGrid.BillingRequirement;
            if (e.Clicks < 2)  // Don't fire if double clicking //
            {
                //SetMenuStatus();
            }

            GridView view = sender as GridView;
            //view.GridControl.Focus();
            downHitInfo = null;
            GridHitInfo hitInfo = view.CalcHitInfo(new Point(e.X, e.Y));
            if (Control.ModifierKeys != Keys.None) return;
            if (e.Button == MouseButtons.Left && hitInfo.InRow && hitInfo.HitTest != GridHitTest.RowIndicator)
            {
                downHitInfo = hitInfo;
            }
            DXMouseEventArgs args = (DXMouseEventArgs)e;
            if (e.Button == MouseButtons.Right && !hitInfo.InColumnPanel && !hitInfo.InFilterPanel && !hitInfo.InGroupPanel && hitInfo.HitTest != GridHitTest.Footer)
            {
                try
                {
                    i_GridViewFocused = view;
                    i_intCopyRowHandle = hitInfo.RowHandle;
                    i_strCopyColumnName = (hitInfo.HitTest.ToString() != "RowIndicator" && i_intCopyRowHandle >= 0 ? hitInfo.Column.FieldName : "");
                    bbiCopyToClipboard.Enabled = (i_intCopyRowHandle >= 0 && !string.IsNullOrWhiteSpace(i_strCopyColumnName));
                }
                catch (Exception) { }

                pmDataContextMenu.ShowPopup(new Point(MousePosition.X, MousePosition.Y));
            }
        }

        private void Add_From_Billing_Requirement_Template()
        {
            GridView parentView = (GridView)gridControlContract.MainView;
            int[] intRowHandles = parentView.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Site Contracts to add the Billing Requirement Template to by clicking on them then try again.", "Add Billing Requirements From Template", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            else if (intCount > 1)
            {
                if (DevExpress.XtraEditors.XtraMessageBox.Show("You have " + intCount.ToString() + " Contracts selected. If you proceed new Billing Requirements will be created for each of these records from the chosen Template.\n\nProceed?", "Add Billing Requirements From Template", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No) return;
            }
            GridView view = (GridView)gridControlBillingRequirement.MainView;
            view.PostEditor();

            var fChildForm = new frm_Core_Select_Billing_Requirement_Template();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child form //
            {
                int intTemplateID = fChildForm.intSelectedTemplateHeaderID;
                if (intTemplateID <= 0) return;

                StringBuilder sb = new StringBuilder();
                foreach (int i in intRowHandles)  // Selected Contracts //
                {
                    sb.Append(parentView.GetRowCellValue(i, "SiteContractID").ToString() + ",");
                }
                if (string.IsNullOrWhiteSpace(sb.ToString())) return;

                i_str_AddedRecordIDsBillingRequirement = "";
                using (var AddRecords = new DataSet_Common_FunctionalityTableAdapters.QueriesTableAdapter())
                {
                    AddRecords.ChangeConnectionString(strConnectionString);
                    try
                    {
                        i_str_AddedRecordIDsBillingRequirement += AddRecords.sp01042_Core_Billing_Requirement_Item_Add_From_Template(sb.ToString(), intBillingRequirementLinkedRecordTypeID, intTemplateID).ToString();
                    }
                    catch (Exception) { }
                }
                LoadLinkedRecords();
            }
        }
        
        #endregion


        #region Client Contract Filter Panel

        private void buttonEditClientFilter_ButtonClick(object sender, ButtonPressedEventArgs e)
        {
            switch (e.Button.Tag.ToString().ToLower())
            {
                case "clear":
                    {
                        i_str_selected_ClientContract_ids = "";
                        i_str_selected_ClientContract_descriptions = "";
                        buttonEditClientFilter.EditValue = "No Client Contract Filter";
                        
                        // Update Filter control's tooltip //
                        superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Contract Filter.\n\nNo Filter Set.";
                    }
                    break;
                case "choose":
                    {
                        var fChildForm = new frm_OM_Select_Client_Contract();
                        fChildForm.GlobalSettings = this.GlobalSettings;
                        fChildForm.strOriginalIDs = i_str_selected_ClientContract_ids;
                        fChildForm._Mode = "multiple";
                        if (fChildForm.ShowDialog() == DialogResult.OK)  // User Clicked OK on child Form //
                        {
                            if (i_str_selected_ClientContract_ids == fChildForm.strSelectedIDs) return;
                            i_str_selected_ClientContract_ids = fChildForm.strSelectedIDs;
                            i_str_selected_ClientContract_descriptions = fChildForm.strSelectedDescription;
                            buttonEditClientFilter.EditValue = fChildForm.strSelectedDescription;
                            
                            // Update Filter control's tooltip //
                            string strTooltipText = i_str_selected_ClientContract_descriptions.Replace(", ", "\n");
                            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Contract Filter.\n\n" + strTooltipText;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion


        #region Callout Date Range Filter Panel

        private void btnDateRangeOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            string strValue = "";
            if (dateEditFromDate.DateTime == DateTime.MinValue)
            {
                i_dtStart = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtStart = dateEditFromDate.DateTime;
            }
            if (dateEditToDate.DateTime == DateTime.MinValue)
            {
                i_dtEnd = new DateTime(1900, 1, 1);
            }
            else
            {
                i_dtEnd = dateEditToDate.DateTime;
            }
            if (dateEditFromDate.DateTime == DateTime.MinValue && dateEditToDate.DateTime == DateTime.MinValue)
            {
                strValue = "No Date Range Filter";
            }
            else
            {
                strValue = (i_dtStart == new DateTime(1900, 1, 1) ? "No Start" : i_dtStart.ToString("dd/MM/yyyy HH:mm")) + " - " + (i_dtEnd == new DateTime(1900, 1, 1) ? "No End" : i_dtEnd.ToString("dd/MM/yyyy HH:mm"));
            }
            return strValue;
        }

        #endregion


        #region CM Filter Panel

        private void popupContainerEditCMFilter_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditCMFilter_Get_Selected();
        }

        private void btnCMFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditCMFilter_Get_Selected()
        {
            i_str_selected_CM_ids = "";    // Reset any prior values first //
            i_str_selected_CM_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl5.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_CM_ids = "";
                return "All CMs";
            }
            else if (selection5.SelectedCount <= 0)
            {
                i_str_selected_CM_ids = "";
                return "All CMs";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_CM_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_CM_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_CM_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_CM_names;
        }

        #endregion


        #region Construction Manager Filter Panel

        private void popupContainerEditConstructionManager_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditConstructionManagerFilter_Get_Selected();
        }

        private void btnConstructionManagerFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private string PopupContainerEditConstructionManagerFilter_Get_Selected()
        {
            i_str_selected_Construction_Manager_ids = "";    // Reset any prior values first //
            i_str_selected_Construction_Manager_names = "";  // Reset any prior values first //
            GridView view = (GridView)gridControl3.MainView;
            if (view.DataRowCount <= 0)
            {
                i_str_selected_Construction_Manager_ids = "";
                return "All Construction Managers";
            }
            else if (selection3.SelectedCount <= 0)
            {
                i_str_selected_Construction_Manager_ids = "";
                return "All Construction Managers";
            }
            else
            {
                int intCount = 0;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        i_str_selected_Construction_Manager_ids += Convert.ToString(view.GetRowCellValue(i, "ID")) + ",";
                        if (intCount == 0)
                        {
                            i_str_selected_Construction_Manager_names = Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        else if (intCount >= 1)
                        {
                            i_str_selected_Construction_Manager_names += ", " + Convert.ToString(view.GetRowCellValue(i, "Surname")) + ": " + Convert.ToString(view.GetRowCellValue(i, "Forename"));
                        }
                        intCount++;
                    }
                }
            }
            return i_str_selected_Construction_Manager_names;
        }

        #endregion


        private void btnClearAllFilters_Click(object sender, EventArgs e)
        {
            i_str_selected_ClientContract_ids = "";
            i_str_selected_ClientContract_descriptions = "";
            buttonEditClientFilter.Text = "";
            superToolTipSetupArgsClientContractFilter.Contents.Text = "I store the currently applied Client Filter.\n\nNo Filter Set.";
            
            selection5.ClearSelection();
            popupContainerEditCMFilter.Text = PopupContainerEditCMFilter_Get_Selected();

            selection3.ClearSelection();
            popupContainerEditConstructionManager.Text = PopupContainerEditConstructionManagerFilter_Get_Selected();
        }

        private void btnLoad_Click_1(object sender, EventArgs e)
        {
            Load_Data();
        }


        #region Buttons

        private void bbiAddWizard_ItemClick(object sender, ItemClickEventArgs e)
        {
            var fChildForm = new frm_OM_Site_Contract_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.i_str_PassedInClientContractIDs = "";  // No passed  in Client Contract ID //
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiAddVisitWizard_ItemClick(object sender, ItemClickEventArgs e)
        {
            string strClientContractIDs = ",";
            string strCurrentClientContractID = "";
            string strSiteContractIDs = ",";

            GridView view = (GridView)gridControlContract.MainView;
            int[] intRowHandles = view.GetSelectedRows(); ;
            int intCount = intRowHandles.Length;
            if (intCount > 0)
            {
                foreach (int intRowHandle in intRowHandles)
                {
                    strSiteContractIDs += Convert.ToString(view.GetRowCellValue(intRowHandle, "SiteContractID")) + ",";
                    strCurrentClientContractID = Convert.ToString(view.GetRowCellValue(intRowHandle, "ClientContractID"));
                    if (!strClientContractIDs.Contains("," + strCurrentClientContractID + ",")) strClientContractIDs += strCurrentClientContractID + ",";
                }
            }
            strClientContractIDs = strClientContractIDs.TrimStart(',');
            strSiteContractIDs = strSiteContractIDs.TrimStart(',');
      
            var fChildForm = new frm_OM_Visit_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.i_str_PassedInClientContractIDs = strClientContractIDs;
            fChildForm.i_str_PassedInSiteContractIDs = strSiteContractIDs;
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }


        private void bbiViewSchedule_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void bbiSendSchedule_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void bbiInActive_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlContract.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to set as In-Active by clicking on them then try again.", "Set Site Contract(s) as In-Active", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "Active")) == 0)  // Already In-active so de-select it //
                {
                    view.UnselectRow(intRowHandle);
                }
            }
            view.EndSelection();
            // Check there is at least one row still selected //
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to set as In-Active by clicking on them then try again.\n\nOnly Site Contracts not already in-active can be selected for setting as In-Active. If in-active Site Contracts are selected this process will automatically de-select them.", "Set Site Contract(s) as In-Active", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            // Ok to proceed //
            string strIDs = "";
            foreach (int intRowHandle in intRowHandles)
            {
                strIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, "SiteContractID")) + ",";
            }
            int intCancelData = 0;
            /*DialogResult dr = XtraMessageBox.Show("<color=red>You are about to set the selected Site Contract(s) as In-Active!</color>\n\n<b>Do you want to set any existing outstanding linked visits and jobs as Cancelled</b>.\n\nClick <b>Yes</b> to set linked outstanding visits and jobs as <b>Cancelled</b>.\n\nClick <b>No</b> to leave linked outstanding visits and jobs <b>unchanged</b>.\n\nClick <b>Cancel</b> to <b>abort</b> the process.", "Set Site Contract(s) as In-Active", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, DefaultBoolean.True);
            if (dr == DialogResult.Cancel) return;
            if (dr == DialogResult.Yes) intCancelData = 1;*/

            var fChildForm = new frm_OM_Site_Contract_Utilities_InActive();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            intCancelData = fChildForm.intCancelVisits;
            DateTime dtEndDate = fChildForm.dtEndDate;
            string strRemarks = fChildForm.strRemarks;

            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            splashScreenManager1.ShowWaitForm();
            splashScreenManager1.SetWaitFormDescription("Updating Data...");

            using (var UpdateData = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
            {
                UpdateData.ChangeConnectionString(strConnectionString);
                try
                {
                    UpdateData.sp06395_OM_SIte_Contract_Set_InActive(strIDs, intCancelData, dtEndDate, strRemarks);
                }
                catch (Exception ex)
                {
                    if (splashScreenManager1.IsSplashFormVisible)
                    {
                        splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                        splashScreenManager1.CloseWaitForm();
                    }
                    XtraMessageBox.Show("An error occurred while attempting to set selected Site Contracts as In-Active.\n\n Error: " + ex.Message, "Set Site Contract(s) as In-Active", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            if (splashScreenManager1.IsSplashFormVisible)
            {
                splashScreenManager1.SetWaitFormDescription("Loading...");  // Reset caption //
                splashScreenManager1.CloseWaitForm();
            }
            Load_Data();
            if (this.GlobalSettings.ShowConfirmations == 1) XtraMessageBox.Show("Site Contract(s) set as In-Active Successfully.", "Set Site Contract(s) as In-Active", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void bbiOnHoldSiteContract_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlContract.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to set as On-Hold by clicking on them then try again.", "Set Site Contracts On-Hold", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "OnHold")) > 0)  // Already On-Hold so de-select it //
                {
                    view.UnselectRow(intRowHandle);
                }
            }
            view.EndSelection();
            // Check there is at least one row still selected //
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to set as On-Hold by clicking on them then try again.\n\nOnly Site Contracts not already On-Hold can be selected for setting to On-Hold. If On-Hold Site Contracts are selected this process will automatically de-select them.", "Set Site Contracts On-Hold", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Ok to proceed //
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellDisplayText(intRowHandle, "SiteContractID").ToString() + ",");
            }

            var fChildForm = new frm_OM_Site_Contract_On_Hold_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.i_str_PassedInSiteContractIDs = sb.ToString();
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });            
        }

        private void bbiOffHoldSiteContract_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlContract.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to set as Off-Hold by clicking on them then try again.", "Set Site Contracts Off-Hold", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            view.BeginSelection();
            foreach (int intRowHandle in intRowHandles)
            {
                if (Convert.ToInt32(view.GetRowCellValue(intRowHandle, "OnHold")) == 0)  // Already Off-Hold so de-select it //
                {
                    view.UnselectRow(intRowHandle);
                }
            }
            view.EndSelection();
            // Check there is at least one row still selected //
            intRowHandles = view.GetSelectedRows();
            intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to set as Off-Hold by clicking on them then try again.\n\nOnly Site Contracts On-Hold can be selected for setting to Off-Hold. If Off-Hold Site Contracts are selected this process will automatically de-select them.", "Set Site Contracts Off-Hold", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Ok to proceed //
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellDisplayText(intRowHandle, "SiteContractID").ToString() + ",");
            }

            var fChildForm = new frm_OM_Site_Contract_Off_Hold_Wizard();
            fChildForm.MdiParent = this.MdiParent;
            fChildForm.GlobalSettings = this.GlobalSettings;
            fChildForm.strRecordIDs = "";
            fChildForm.strFormMode = "add";
            fChildForm.strCaller = this.Name;
            fChildForm.intRecordCount = 0;
            fChildForm.i_str_PassedInSiteContractIDs = sb.ToString();
            fChildForm.FormPermissions = this.FormPermissions;
            DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::WoodPlan5.WaitForm1), true, true);
            fChildForm.splashScreenManager = splashScreenManager1;
            fChildForm.splashScreenManager.ShowWaitForm();
            fChildForm.Show();

            System.Reflection.MethodInfo method = typeof(frmBase).GetMethod("PostOpen");  // Invoke Post Open event on Base form //
            if (method != null) method.Invoke(fChildForm, new object[] { null });
        }

        private void bbiSetContractExpiryReason_ItemClick(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlContract.MainView;
            int[] intRowHandles = view.GetSelectedRows();
            int intCount = intRowHandles.Length;
            if (intCount <= 0)
            {
                XtraMessageBox.Show("Select one or more Site Contracts to set Contract Expiry Reasons by clicking on them then try again.", "Set Site Contract Expiry Reason", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            // Ok to proceed //
            StringBuilder sb = new StringBuilder();
            foreach (int intRowHandle in intRowHandles)
            {
                sb.Append(view.GetRowCellDisplayText(intRowHandle, "SiteContractID").ToString() + ",");
            }

            string strMessage = "You have " + (intCount == 1 ? "1 Site Contract" : Convert.ToString(intRowHandles.Length) + " Site Contracts") + " selected for setting Expiry Reason.\n\nProceed?";
            if (XtraMessageBox.Show(strMessage, "Set Site Contract Expiry Reason", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            // Ok to proceed so get further info //
            var fChildForm = new frm_OM_Site_Contract_Get_Expiry_Reason();
            fChildForm.GlobalSettings = this.GlobalSettings;
            if (fChildForm.ShowDialog() != DialogResult.OK) return;
            int intExpiryReasonID = fChildForm.intExpiryReasonID;

            string strSiteContractIDs = sb.ToString();

            // Cancel Visits and linked unstarted jobs //
            using (var SetExpiryReason = new DataSet_OM_ContractTableAdapters.QueriesTableAdapter())
            {
                SetExpiryReason.ChangeConnectionString(strConnectionString);
                try
                {
                    SetExpiryReason.sp06542_OM_Set_Site_contract_Expiry_Reason(strSiteContractIDs, intExpiryReasonID);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("An error occurred while attempting to set the Expiry Reason for the selected site contract(s) - error: " + ex.Message + "\n\nPlease try again. If problems persist contact Technical Support.", "Set Site Contract Expiry Reason", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }
            }
            // Merge just changed rows back in to dataset - no need to reload the full dataset //
            Load_Data();

            XtraMessageBox.Show("Expiry Reasons set Successfully.", "Set Site Contract Expiry Reason", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        #endregion


        private void splitContainerControl1_SplitGroupPanelCollapsed(object sender, SplitGroupPanelCollapsedEventArgs e)
        {
            if (!e.Collapsed) LoadLinkedRecords();
        }

        private void barManager1_HighlightedLinkChanged(object sender, DevExpress.XtraBars.HighlightedLinkChangedEventArgs e)
        {
            // This event is required by the ToolTipControl object [it handles firing the Tooltips on BarSubItem objects on the toolbar because they don't show Tooltips] //
            toolTipController1.HideHint();
            if (e.Link == null) return;

            BarSubItemLink link = e.PrevLink as BarSubItemLink;
            if (link != null) link.CloseMenu();

            if (e.Link.Item is BarLargeButtonItem) return;

            var Info = new ToolTipControlInfo {Object = e.Link.Item, SuperTip = e.Link.Item.SuperTip};

            toolTipController1.ShowHint(Info);
        }


        private void Linked_Document_Drill_Down(int intRecordType, int intRecordSubType, int intRecordID, string strRecordDescription)
        {
            // Drilldown to Linked Document Manager //
            string strRecordIDs = "";
            DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter GetSetting = new DataSet_GC_Summer_CoreTableAdapters.QueriesTableAdapter();
            GetSetting.ChangeConnectionString(strConnectionString);
            try
            {
                strRecordIDs = GetSetting.sp06049_OM_Linked_Docs_Drilldown_Get_IDs(intRecordID, intRecordType, intRecordSubType).ToString();
            }
            catch (Exception) { }
            if (string.IsNullOrWhiteSpace(strRecordIDs)) strRecordIDs = "-1,";  // Make sure the form starts in drilldown mode [use a dummy id] //
            Data_Manager_Drill_Down.Drill_Down(this, this.GlobalSettings, strRecordIDs, "om_linked_documents", intRecordType, intRecordSubType, intRecordID, strRecordDescription);
        }

        private void bciFilterContractsSelected_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            GridView view = (GridView)gridControlContract.MainView;
            if (bciFilterContractsSelected.Checked)  // Filter Selected rows //
            {
                try
                {
                    int[] intRowHandles = view.GetSelectedRows();
                    int intCount = intRowHandles.Length;
                    DataRow dr = null;
                    if (intCount <= 0)
                    {
                        XtraMessageBox.Show("Select one or more Site Contract records to filter by before proceeding.", "Filter Selected Site Contract Record(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                    gridControlContract.BeginUpdate();
                    foreach (int intRowHandle in intRowHandles)
                    {
                        dr = view.GetDataRow(intRowHandle);
                        if (dr != null) dr["Selected"] = 1;
                    }
                }
                catch (Exception) { }
                view.ActiveFilter.Clear();
                view.ActiveFilter.NonColumnFilter = "[Selected] = 1";
                gridControlContract.EndUpdate();
            }
            else  // Clear Filter //
            {
                gridControlContract.BeginUpdate();
                try
                {
                    view.ActiveFilter.Clear();
                    foreach (DataRow dr in dataSet_OM_Contract.sp06088_OM_Site_Contract_Manager.Rows)
                    {
                        if (Convert.ToInt32(dr["Selected"]) == 1) dr["Selected"] = 0;
                    }
                }
                catch (Exception) { }
            }
            gridControlContract.EndUpdate();

        }



 





    }
}



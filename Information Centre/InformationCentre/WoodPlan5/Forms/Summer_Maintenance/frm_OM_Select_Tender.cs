using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Menu;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

using System.Reflection;  // Required by GridViewFiltering //
using DevExpress.XtraEditors.Repository;  // Required by GridViewFiltering //
using System.Collections.Generic;    // Required by GridViewFiltering - List command //

using BaseObjects;
using WoodPlan5.Properties;

namespace WoodPlan5
{
    public partial class frm_OM_Select_Tender : WoodPlan5.frmBase_Modal
    {
        #region Instance Variables...

        private Settings set = Settings.Default;
        private string strConnectionString = "";
        GridHitInfo downHitInfo = null;
        bool internalRowFocusing;

        public string _Mode = "single";  // single or multiple //
        public int intExcludeTenderLinkedToTenderGroup = 0;

        public string strOriginalParentIDs = "";
        public int intOriginalParentID = 0;
        public string strOriginalChildIDs = "";
        public int intOriginalChildID = 0;

        public string strSelectedParentIDs = "";
        public int intSelectedParentID = 0;
        public string strSelectedChildIDs = "";
        public int intSelectedChildID = 0;
        public string strSelectedParentDescriptions = "";
        public string strSelectedChildDescriptions = "";
        public string strSelectedChildDescriptionFull = "";

        public string _strExcludeIDs = "";
        public DateTime? i_dt_FromDate = null;
        public DateTime? i_dt_ToDate = null;
        private string i_str_date_range = "";

        public int _SelectedCount1 = 0;
        public int _SelectedCount2 = 0;
        BaseObjects.GridCheckMarksSelection selection1;
        BaseObjects.GridCheckMarksSelection selection2;

        #endregion

        public frm_OM_Select_Tender()
        {
            InitializeComponent();
        }

        private void frm_OM_Select_Tender_Load(object sender, EventArgs e)
        {
            this.LockThisWindow();
            this.FormID = 500277;  // *** IMPORTANT: THIS VALUE SHOULD BE UNIQUE FOR EACH FORM - CONTROLS LINKING TO SAVED LAYOUTS ETC: USE PARENT FORM ID + 1/2/3... *** // 
            strConnectionString = GlobalSettings.ConnectionString;

            Set_Grid_Highlighter_Transparent(this.Controls);

            sp06508_OM_Select_Extra_Works_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            sp06497_OM_Tenders_For_Client_ContractTableAdapter.Connection.ConnectionString = strConnectionString;
            
            if (i_dt_FromDate != null) dateEditFromDate.DateTime = Convert.ToDateTime(i_dt_FromDate);
            if (i_dt_ToDate != null) dateEditToDate.DateTime = Convert.ToDateTime(i_dt_ToDate);
            beiDateFilter.EditValue = PopupContainerEditDateRange_Get_Selected();

            GridView view = (GridView)gridControl1.MainView;
            gridControl1.ForceInitialize();
            LoadData();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection1 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl1.MainView);
                selection1.CheckMarkColumn.Fixed = FixedStyle.Left;
                selection1.CheckMarkColumn.VisibleIndex = 0;
                selection1.CheckMarkColumn.Width = 30;

                Array arrayRecords = strOriginalParentIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalParentID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], intOriginalParentID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }
            bbiRefreshChildData.Enabled = (_Mode != "single");

            view = (GridView)gridControl2.MainView;
            gridControl2.ForceInitialize();
            LoadLinkedData();

            if (_Mode != "single")
            {
                // Add record selection checkboxes to popup grid control //
                selection2 = new BaseObjects.GridCheckMarksSelection((GridView)gridControl2.MainView);
                selection2.CheckMarkColumn.Fixed = FixedStyle.Left;
                selection2.CheckMarkColumn.VisibleIndex = 0;
                selection2.CheckMarkColumn.Width = 30;

                Array arrayRecords = strOriginalChildIDs.Split(',');  // Single quotes because char expected for delimeter //
                view.BeginUpdate();
                int intFoundRow = 0;
                foreach (string strElement in arrayRecords)
                {
                    if (strElement == "") break;
                    intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], Convert.ToInt32(strElement));
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.SetRowCellValue(intFoundRow, "CheckMarkSelection", 1);
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
                view.EndUpdate();
            }
            else  // Single selection mode //
            {
                if (intOriginalChildID != 0)  // Record selected so try to find and highlight //
                {
                    int intFoundRow = view.LocateByValue(0, view.Columns["ClientContractID"], intOriginalChildID);
                    if (intFoundRow != GridControl.InvalidRowHandle)
                    {
                        view.FocusedRowHandle = intFoundRow;
                        view.MakeRowVisible(intFoundRow, false);
                    }
                }
            }

            PostOpen();
        }

        public void PostOpen()
        {
            // Load any default view //
            Load_Saved_Layout LoadSavedLayout = new Load_Saved_Layout();
            int intReturn = LoadSavedLayout.Load_Default_Screen_Layout(strConnectionString, this, this.GlobalSettings);
            this.UnlockThisWindow();  // ***** Locked in Load event ***** //

            if (fProgress != null)
            {
                fProgress.UpdateProgress(10); // Update Progress Bar //
                fProgress.Close();
                fProgress = null;
            }
            Application.DoEvents();  // Allow Form time to repaint itself //
        }
        
        public override void PostLoadView(object objParameter)
        {
            bbiRefreshChildData.Enabled = (_Mode != "single");
        }

        private void LoadData()
        {
            GridView view = (GridView)gridControl1.MainView;
            view.BeginUpdate();
            DateTime dtFromDate = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2001, 01, 01) : Convert.ToDateTime(i_dt_FromDate));
            DateTime dtToDate = (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2900, 12, 31) : Convert.ToDateTime(i_dt_ToDate));

            sp06508_OM_Select_Extra_Works_Client_ContractTableAdapter.Fill(dataSet_OM_Tender.sp06508_OM_Select_Extra_Works_Client_Contract, _strExcludeIDs, dtFromDate, dtToDate);
            view.EndUpdate();
        }

        private void LoadLinkedData()
        {
            GridView view = (GridView)gridControl1.MainView;
            int intCount = 0;
            string strSelectedIDs = "";
            if (_Mode != "single")
            {
                if (selection1.SelectedCount <= 0) return;
                intCount = selection1.SelectedCount;
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedIDs += Convert.ToString(view.GetRowCellValue(i, "ClientContractID")) + ",";
                    }
                }
            }
            else
            {
                int[] intRowHandles = view.GetSelectedRows();
                intCount = intRowHandles.Length;
                foreach (int intRowHandle in intRowHandles)
                {
                    strSelectedIDs += Convert.ToString(view.GetRowCellDisplayText(intRowHandle, view.Columns["ClientContractID"])) + ',';
                }
            }

            gridControl2.MainView.BeginUpdate();
            if (intCount == 0)
            {
                this.dataSet_OM_Tender.sp06497_OM_Tenders_For_Client_Contract.Clear();
            }
            else
            {
                DateTime dtFromDate = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2001, 01, 01) : Convert.ToDateTime(i_dt_FromDate));
                DateTime dtToDate = (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? new DateTime(2900, 12, 31) : Convert.ToDateTime(i_dt_ToDate));
                try
                {
                    sp06497_OM_Tenders_For_Client_ContractTableAdapter.Fill(dataSet_OM_Tender.sp06497_OM_Tenders_For_Client_Contract, (string.IsNullOrEmpty(strSelectedIDs) ? "" : strSelectedIDs), dtFromDate, dtToDate, intExcludeTenderLinkedToTenderGroup);
                }
                catch (Exception Ex)
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("An error occurred [" + Ex.Message + "] while loading the related Tenders.\n\nTry selecting a Client Contract again. If the problem persists, contact Technical Support.", "Load Data", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            gridControl2.MainView.EndUpdate();
            
        }


        #region Grid View Generic Events

        private void GridView_CustomDrawEmptyForeground(object sender, CustomDrawEventArgs e)
        {
            GridView view = (GridView)sender;
            string message = "";
            switch (view.Name)
            {
                case "gridView1":
                    message = "No Client Contacts Available - Try adjusting any Date Filters and click Refresh Data button";
                    break;
                case "gridView2":
                    message = "No Tenders Available - Select a Client Contract to view linked Tenders";
                    break;
                default:
                    message = "No Records Available";
                    break;
            }
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomDrawEmptyForeground(sender, e, message);
        }

        private void GridView_CustomFilterDialog(object sender, CustomFilterDialogEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.CustomFilterDialog(sender, e);
        }

        private void GridView_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FilterEditorCreated(sender, e);
        }

        public void GridView_FocusedRowChanged_NoGroupSelection(object sender, FocusedRowChangedEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.FocusedRowChanged_NoGroupSelection(sender, e, ref internalRowFocusing);
            GridView view = (GridView)sender;
            switch (view.Name)
            {
                case "gridView1":
                    if (_Mode == "single") LoadLinkedData();
                    break;
                default:
                    break;
            }
        }

        public void GridView_MouseDown_NoGroupSelection(object sender, MouseEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.MouseDown_NoGroupSelection(sender, e);
        }

        private void GridView_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            ExtendedGridViewFunctions extGridViewFunction = new ExtendedGridViewFunctions();
            extGridViewFunction.PopupMenuShowing(sender, e);
        }

        #endregion


        #region Date Range Filter Panel

        private void btnDateRangeFilterOK_Click(object sender, EventArgs e)
        {
            // Close the dropdown accepting the user's choice //
            Control button = sender as Control;
            (button.Parent as PopupContainerControl).OwnerEdit.ClosePopup();
        }

        private void repositoryItemPopupContainerEditDateRange_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            e.Value = PopupContainerEditDateRange_Get_Selected();
        }

        private string PopupContainerEditDateRange_Get_Selected()
        {
            i_dt_FromDate = null;    // Reset any prior values first //
            i_dt_ToDate = null;  // Reset any prior values first //
            i_str_date_range = "";
            GridView view = (GridView)gridControl1.MainView;

            i_dt_FromDate = dateEditFromDate.DateTime;
            i_dt_ToDate = dateEditToDate.DateTime;
            i_str_date_range = (i_dt_FromDate == null || i_dt_FromDate == Convert.ToDateTime("01/01/0001") ? "No Start" : Convert.ToDateTime(i_dt_FromDate).ToString("dd/MM/yyyy"));
            i_str_date_range += " - " + (i_dt_ToDate == null || i_dt_ToDate == Convert.ToDateTime("01/01/0001") ? "No End" : Convert.ToDateTime(i_dt_ToDate).ToString("dd/MM/yyyy"));
            return i_str_date_range;
        }

        #endregion


        private void btnOK_Click(object sender, EventArgs e)
        {
            GetSelectedDetails();
            if (string.IsNullOrEmpty(strSelectedChildDescriptions))
            {
                if (_Mode != "single")
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select one or more Tenders by ticking them before proceeding.", "Select Tender(s)", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    DevExpress.XtraEditors.XtraMessageBox.Show("Select a Tender by clicking on it before proceeding.", "Select Tender", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                return;
            }

            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void GetSelectedDetails()
        {
            strSelectedParentIDs = "";    // Reset any prior values first //
            strSelectedParentDescriptions = "";    // Reset any prior values first //

            GridView view = (GridView)gridControl2.MainView;
            if (!ReferenceEquals(view.ActiveFilter.Criteria, null)) view.ActiveFilter.Clear();  // Ensure there are no fiters switched on //
            if (!string.IsNullOrEmpty(view.FindFilterText)) view.ApplyFindFilter("");  // Clear any Find in place //  
            if (_Mode != "single")
            {
                if (selection2.SelectedCount <= 0) return;
                strSelectedParentDescriptions = ",";
                int intCount = 0;
                string strTempParentID = "";
                for (int i = 0; i < view.DataRowCount; i++)
                {
                    if (Convert.ToBoolean(view.GetRowCellValue(i, "CheckMarkSelection")))
                    {
                        strSelectedChildIDs += Convert.ToString(view.GetRowCellValue(i, "TenderID")) + ",";
                        if (intCount == 0)
                        {
                            strSelectedChildDescriptions = view.GetRowCellValue(view.FocusedRowHandle, "TenderID").ToString();
                        }
                        else if (intCount >= 1)
                        {
                            strSelectedChildDescriptions += ", " + view.GetRowCellValue(view.FocusedRowHandle, "TenderID").ToString();
                        }
                        strTempParentID = Convert.ToString(view.GetRowCellValue(i, "ClientContractID"));
                        if (!strSelectedParentIDs.Contains("," + strTempParentID + ","))
                        {
                            strSelectedParentIDs += strTempParentID + ",";
                            strSelectedParentDescriptions += Convert.ToString(view.GetRowCellValue(i, "ClientName")) + ",";
                        }

                        intCount++;
                    }
                }
                strSelectedParentIDs = strSelectedParentIDs.Remove(0, 1);  // Remove preceeding comma //
            }
            else  // Single Selection //
            {
                if (view.FocusedRowHandle != GridControl.InvalidRowHandle)
                {
                    intSelectedParentID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "ClientContractID"));
                    intSelectedChildID = Convert.ToInt32(view.GetRowCellValue(view.FocusedRowHandle, "TenderID"));
                    strSelectedChildDescriptions = (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")))
                                                        + ", Tender: " + view.GetRowCellValue(view.FocusedRowHandle, "TenderID").ToString();
                    strSelectedChildDescriptionFull = "Client: " + (String.IsNullOrEmpty(Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName"))) ? "Unknown Client" : Convert.ToString(view.GetRowCellValue(view.FocusedRowHandle, "ClientName")))
                                                        + ", Tender: " + view.GetRowCellValue(view.FocusedRowHandle, "TenderID").ToString();
                }
            }

        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadData();
        }

        private void bbiRefreshChildData_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadLinkedData();
        }



    }
}

